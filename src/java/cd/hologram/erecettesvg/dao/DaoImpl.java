/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.dao;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.sql.SQLQueryRecouvrement;
import cd.hologram.erecettesvg.util.CustumException;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.math.BigDecimal;
import java.util.*;
import java.util.HashMap;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.ejb.*;
import javax.persistence.*;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 *
 * @author tony.lundja
 */
@Stateless(mappedName = "DaoErecettesDGRK")
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class DaoImpl implements IDaoLocal, IDaoRemote {

    @PersistenceContext(unitName = "erecettesvgPU")
    private EntityManager em;

    @Resource
    private EJBContext contextEJB;

    UserTransaction userTransaction;

    private EntityManager getEntityManager() {

        return em;
    }

    private final static String INVALID_ARGUMENT_TEXT = "Argument invalid";

    private boolean verifyQuery(String query, Class<Object> entity) {
        return query == null || entity == null;
    }

    private boolean verifyLimit(Integer limit) {
        return limit == null || limit < 0;
    }

    private static void setParameters(Query query, boolean like, Object... params) {

        int i = 1;
        for (Object object : params) {
            if (object instanceof Date) {
                query.setParameter(i++, (Date) object, TemporalType.DATE);
            } else {
                if (like) {
                    query.setParameter(i++, "%" + object + "%");
                } else {
                    query.setParameter(i++, object);
                }
            }
        }
    }

    private void setStoreProcedureParameters(StoredProcedureQuery query, HashMap<String, Object> params) {

        Iterator<String> keys = params.keySet().iterator();

        while (keys.hasNext()) {

            String key = keys.next();
            query.registerStoredProcedureParameter(key, Object.class, ParameterMode.IN);
            if (params.get(key) instanceof Date) {
                query.setParameter(key, (Date) params.get(key), TemporalType.DATE);
            }
            query.setParameter(key, params.get(key));

        }

    }

    private boolean isBulkQueryValid(HashMap<String, Object[]> params) {

        boolean isValid = true;

        Iterator<String> keys = params.keySet().iterator();

        while (keys.hasNext()) {

            String key = keys.next();

            if (!key.contains(":")) {
                isValid = false;
            }
        }

        return isValid;
    }

    private void setBulkInsertion(HashMap<String, Object[]> params) {

        int paramSize = params.size() + 1;

        for (int i = 0; i <= paramSize; i++) {

            Iterator<String> keys = params.keySet().iterator();

            while (keys.hasNext()) {

                String key = keys.next();
                String[] splitKey = key.split(":");
                int index = Integer.parseInt(splitKey[0]);
                if (index == i) {
                    Query query = (Query) getEntityManager().createNativeQuery(splitKey[1]);
                    setParameters(query, false, params.get(key));
                    query.executeUpdate();
                    break;
                }

            }

        }
    }

    @Override
    public Object find(Class<Object> entity, Object id) {
        Object object = null;
        try {
            if (entity == null || id == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            object = getEntityManager().find(entity, id);
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return object;
    }

    @Override
    public Object find(String query, Class<Object> entity, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (verifyQuery(query, entity) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query nativeQuery = getEntityManager().createNativeQuery(query, entity);
            setParameters(nativeQuery, like, params);
            objectList = nativeQuery.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    public Object find2(String query, Class<Object> entity, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (verifyQuery(query, entity) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query nativeQuery = getEntityManager().createNativeQuery(query);
            setParameters(nativeQuery, like, params);
            objectList = nativeQuery.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    @Override
    public Object find(String query, Class<Object> entity, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (verifyQuery(query, entity) || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            Query nativeQuery = getEntityManager().createNativeQuery(query, entity);
            nativeQuery.setFirstResult(0);
            nativeQuery.setMaxResults(limit);
            setParameters(nativeQuery, like, params);
            objectList = nativeQuery.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    @Override
    public Object findJPQL(String queryJPQL, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (queryJPQL == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createQuery(queryJPQL);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    @Override
    public Object findJPQL(String queryJPQL, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (queryJPQL == null || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createQuery(queryJPQL);
            query.setFirstResult(0);
            query.setMaxResults(limit);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    @Override
    public Object findNamedJPQL(String namedQuery, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (namedQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createNamedQuery(namedQuery);
            setParameters(query, like, params);
            objectList = query.getResultList();
        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return objectList;
    }

    @Override
    public Object findNamedJPQL(String namedQuery, Integer limit, boolean like, Object... params) {
        Object objectList = Collections.EMPTY_LIST;
        try {
            if (namedQuery == null || verifyLimit(limit) || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query query = getEntityManager().createNamedQuery(namedQuery);

            query.setFirstResult(0);
            query.setMaxResults(limit);

            setParameters(query, like, params);

            objectList = query.getResultList();

        } catch (Exception e) {

            CustumException.LogException(e);
            throw e;

        }

        return objectList;

    }

    @Override
    public boolean executeNativeQuery(String NativeQuery, Object... params) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result = false;

        try {

            if (NativeQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            Query query = (Query) getEntityManager().createNativeQuery(NativeQuery);

            setParameters(query, false, params);

            result = query.executeUpdate() != 0;

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.LogException(ex);
            }
            result = false;
            try {
                CustumException.LogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.LogException(ex);
            }
        }

        return result;
    }

    @Override
    public Integer executeNativeQueryV2(String NativeQuery, Object... params) {

        userTransaction = contextEJB.getUserTransaction();

        int result;

        try {

            if (NativeQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            Query query = (Query) getEntityManager().createNativeQuery(NativeQuery);

            setParameters(query, false, params);

            result = query.executeUpdate();

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.LogException(ex);
            }
            result = 0;
            try {
                CustumException.LogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.LogException(ex);
            }
        }

        return result;
    }

    @Override
    public Object getSingleResult(String Nativequery, Object... params) {

        Object object = null;

        try {

            if (Nativequery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            Query query = getEntityManager().createNativeQuery(Nativequery);
            object = (Object) query.getSingleResult();
            setParameters(query, false, params);

        } catch (Exception e) {

            CustumException.LogException(e);
            throw e;
        }

        return object;
    }

    @Override
    public boolean create(Object... entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (object != null) {
                    getEntityManager().persist(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            success = false;
            CustumException.LogException(e);
        } catch (Exception e) {
            success = false;
            CustumException.LogException(e);
            throw e;
        }
        return success;
    }

    @Override
    public boolean create(List<Object> entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (object != null) {
                    getEntityManager().persist(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            success = false;
            CustumException.LogException(e);

        } catch (Exception e) {
            success = false;
            CustumException.LogException(e);
            throw e;
        }
        return success;
    }

    @Override
    public boolean createAndUpdate(Object... entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (entityExist(object)) {
                    getEntityManager().merge(object);
                } else {
                    getEntityManager().persist(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            success = false;
            CustumException.LogException(e);
        } catch (Exception e) {
            success = false;
            CustumException.LogException(e);
            throw e;
        }
        return success;
    }

    @Override
    public boolean createAndUpdate(List<Object> entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (entityExist(object)) {
                    getEntityManager().merge(object);
                } else {
                    getEntityManager().persist(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            success = false;
            CustumException.LogException(e);
        } catch (Exception e) {
            success = false;
            CustumException.LogException(e);
            throw e;
        }
        return success;
    }

    @Override
    public boolean entityExist(Object entity) {
        boolean success = false;
        try {
            Field[] tab = entity.getClass().getDeclaredFields();
            if (tab != null && tab.length > 0) {
                for (Field field : tab) {
                    Annotation[] annotations = field.getAnnotations();
                    if (annotations != null && annotations.length > 0) {
                        for (Annotation annotation : annotations) {
                            if (annotation.toString().equals("@javax.persistence.Id()")) {
                                String fieldName = field.getName();
                                Method method = entity.getClass().getDeclaredMethod("get" + String.valueOf(fieldName.charAt(0)).toUpperCase() + fieldName.substring(1));
                                if (method != null) {
                                    Object object = method.invoke(entity);
                                    Class entityClass = Class.forName(entity.getClass().getCanonicalName());
                                    try {
                                        if (find(entityClass, object) != null) {
                                            success = true;
                                        } else {
                                            success = false;
                                        }
                                    } catch (Exception e) {
                                        CustumException.LogException(e);
                                        throw e;
                                    }
                                }
                                break;
                            }
                            if (annotation.toString().equals("@javax.persistence.EmbeddedId()")) {
                                Method method = entity.getClass().getDeclaredMethod("get" + entity.getClass().getSimpleName() + "PK");

                                if (method != null) {
                                    Object object = method.invoke(entity);
                                    Class entityClass = Class.forName(entity.getClass().getCanonicalName());
                                    if (find(entityClass, object) != null) {
                                        success = true;
                                    } else {
                                        success = false;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);

        }
        return success;
    }

    @Override
    public boolean update(Object... entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (object != null) {
                    getEntityManager().merge(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            CustumException.LogException(e);
            success = false;
        } catch (Exception e) {
            CustumException.LogException(e);
            success = false;
            throw e;
        }
        return success;
    }

    @Override
    public boolean update(List<Object> entities) {
        boolean success;
        try {
            for (Object object : entities) {
                if (object != null) {
                    em.merge(object);
                }
            }
            success = true;
        } catch (javax.validation.ConstraintViolationException e) {
            success = false;
            CustumException.LogException(e);
        } catch (Exception e) {
            success = false;
            CustumException.LogException(e);
            throw e;
        }
        return success;
    }

    @Override
    public String generatekey(String table, String idColumn, String prefix, int nbrOrdrSize, String sifix) {
        String key = "";
        try {
            int taillePrefix = prefix.length();
            int intCode = 0;
            try {
                String[] infos = null;

                String queryStr = "SELECT TOP(1) " + infos[1] + "  FROM " + infos[0] + " WHERE SUBSTRING(" + infos[1] + ",1," + taillePrefix + ") = ? ORDER BY " + infos[1] + " DESC";
                Query requette = getEntityManager().createNativeQuery(queryStr);
                requette.setParameter(1, prefix);
                try {
                    String resultat = requette.getSingleResult().toString();
                    resultat = resultat.replace("[", "");
                    resultat = resultat.replace("]", "");
                    intCode = Integer.parseInt(resultat.substring(taillePrefix));
                    intCode += 1;
                    String zero = "";
                    int taille = String.valueOf(intCode).length();
                    for (int i = 0; i < 6 - taille; i++) {
                        zero += "0";
                    }
                    key = prefix + zero + intCode;
                } catch (NumberFormatException e) {
                    key = prefix + "000001";
                    CustumException.LogException(e);
                }
            } catch (Exception e) {
                CustumException.LogException(e);
            }
        } catch (SecurityException e) {
            CustumException.LogException(e);
            throw e;
        }
        return key;
    }

    @Override
    public Object executeProcedure(String query, Object... params) {
        Object object = null;

        try {
            if (query == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            StoredProcedureQuery storedProcedure = getEntityManager().createNamedStoredProcedureQuery(query);

            setParameters(storedProcedure, false, params);

            object = storedProcedure.getSingleResult();

        } catch (Exception e) {

            CustumException.LogException(e);
            throw e;
        }

        return object;
    }

    @Override
    public Float getSingleResultToBigDecimal(String query, Object... params) {
        Float result = Float.valueOf("0");
        try {
            Query requette = em.createNativeQuery(query);
            setParameters(requette, false, params);
            result = Float.valueOf(requette.getSingleResult().toString());

        } catch (SecurityException ex) {
            CustumException.LogException(ex);
        }
        return result;
    }

    @Override
    public String getSingleResultToString(String query, Object... params) {
        String result = "";
        try {
            Query requette = em.createNativeQuery(query);
            setParameters(requette, false, params);
            result = requette.getSingleResult().toString();

        } catch (SecurityException ex) {
            CustumException.LogException(ex);
        }
        return result;
    }

    @Override
    public String getStoredProcedureOutput(String StoredProcedureName, String outputName, HashMap<String, Object> params) {
        StoredProcedureQuery storedProcedure = getEntityManager().createStoredProcedureQuery(StoredProcedureName);

        storedProcedure.registerStoredProcedureParameter(outputName, String.class, ParameterMode.OUT);

        setStoreProcedureParameters(storedProcedure, params);

        storedProcedure.execute();

        String value = String.valueOf(storedProcedure.getOutputParameterValue(outputName));

        return value;

    }

    @Override
    public Integer getSingleResultToInteger(String query) {
        Integer result = Integer.valueOf("0");
        try {
            if (query == null || query.isEmpty()) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }
            Query requette = em.createNativeQuery(query);
            result = Integer.valueOf(requette.getSingleResult().toString());

        } catch (Exception e) {
            CustumException.LogException(e);
            throw e;
        }
        return result;
    }

    @Override
    public boolean executeBulkQuery(HashMap<String, Object[]> params) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(params)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsertion(params);

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.LogException(ex);
            }
            result = false;
            try {
                CustumException.LogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.LogException(ex);
            }
        }

        return result;

    }

    @Override
    public boolean executeStoredProcedure(String NativeQuery, Object... params) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result = false;

        try {

            if (NativeQuery == null || params == null) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            Query query = (Query) getEntityManager().createNativeQuery(NativeQuery);

            setParameters(query, false, params);

            result = query.executeUpdate() != 0;

            userTransaction.commit();

        } catch (Exception e) {

            try {
                userTransaction.setRollbackOnly();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.LogException(ex);
            }
            result = false;
            try {
                CustumException.LogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.LogException(ex);
            }
        }

        return result;
    }

    public boolean execBulkQuery(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsert(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    public String execBulkQueryWithOneLevel(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = GeneralConst.EMPTY_STRING;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            result = setBulkInsertionWithOneLevel(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private String setBulkInsertionWithOneLevel(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour;

        try {

            valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

            if (!valueRetour.isEmpty()) {

                int paramSize = bulkQuery.size() + 1;

                for (int i = 0; i <= paramSize; i++) {

                    Iterator<String> keys = bulkQuery.keySet().iterator();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        String[] splitKey = key.split(":");
                        int index = Integer.parseInt(splitKey[0]);
                        String req = splitKey[1];

                        if (index == i) {

                            if (req.contains("%s")) {

                                req = req.replaceAll("%s", valueRetour);
                            }

                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQuery.get(key));
                            query.executeUpdate();
                            break;
                        }
                    }
                }
            }
        } catch (Exception exeption) {
            throw exeption;
        }

        return valueRetour;

    }

    private void setBulkInsert(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

        int paramSize = bulkQuery.size() + 1;

        for (int i = 0; i <= paramSize; i++) {

            Iterator<String> keys = bulkQuery.keySet().iterator();

            while (keys.hasNext()) {

                String key = keys.next();
                String[] splitKey = key.split(":");
                int index = Integer.parseInt(splitKey[0]);
                String req = splitKey[1];

                if (index == i) {

                    if (req.contains("%s")) {
                        req = String.format(req, valueRetour);
                    }

                    Query query = (Query) getEntityManager().createNativeQuery(req);
                    setParameters(query, false, bulkQuery.get(key));
                    query.executeUpdate();
                    break;
                }

            }

        }

    }

    @Override
    public BigDecimal getSingleResultToBigDecimal2(String query, Object... params) {
        BigDecimal result = new BigDecimal("0");
        try {
            Query requette = em.createNativeQuery(query);
            setParameters(requette, false, params);
            result = BigDecimal.valueOf(Double.parseDouble(requette.getSingleResult().toString()));

        } catch (SecurityException ex) {
            //CustumException.LogExceptionEJB(ex);
        }
        return result;
    }

    public String execBulkQueryv4(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        userTransaction = contextEJB.getUserTransaction();

        String result;

        try {

            if (!isBulkQueryValid(bulkQuery)) {
                result = GeneralConst.EMPTY_STRING;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            result = setBulkInsertionv4(fistQuery, returnValue, params, bulkQuery);

            userTransaction.commit();

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private String setBulkInsertionv4(
            String fistQuery,
            String returnValue,
            HashMap<String, Object> params,
            HashMap<String, Object[]> bulkQuery) {

        String valueRetour;

        try {

            valueRetour = getStoredProcedureOutput(fistQuery, returnValue, params);

            if (!valueRetour.isEmpty()) {

                int paramSize = bulkQuery.size() + 1;

                for (int i = 0; i <= paramSize; i++) {

                    Iterator<String> keys = bulkQuery.keySet().iterator();

                    while (keys.hasNext()) {

                        String key = keys.next();
                        String[] splitKey = key.split(":");
                        int index = Integer.parseInt(splitKey[0]);
                        String req = splitKey[1];

                        if (index == i) {

                            if (req.contains("%s")) {

                                req = req.replaceAll("%s", valueRetour);
                            }

                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQuery.get(key));
                            query.executeUpdate();
                            break;
                        }
                    }
                }
            }
        } catch (Exception exeption) {
            throw exeption;
        }

        return valueRetour;

    }

    public String execBulkQueryv5(
            HashMap<String, Object> firstparams,
            HashMap<String, Object> secondparams,
            HashMap<String, Object[]> firstQuery,
            HashMap<String, Object[]> secondQuery,
            String firstStoredProcedure,
            String secondStoredProcedure,
            String firstStoredReturnValueKey,
            String secondStrotedReturnValueKey,
            String secondParamskey) {

        userTransaction = contextEJB.getUserTransaction();

        String firstResult, secondResult = GeneralConst.EMPTY_STRING;

        try {

            if (!isBulkQueryValid(firstQuery) && !isBulkQueryValid(secondQuery)) {
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            firstResult = setBulkInsertionv4(firstStoredProcedure, firstStoredReturnValueKey, firstparams, firstQuery);

            if (!firstResult.isEmpty()) {

                secondparams.put(secondParamskey, firstResult);

                secondResult = setBulkInsertionv4(secondStoredProcedure, secondStrotedReturnValueKey, secondparams, secondQuery);

            }

            if (!firstResult.isEmpty() && !secondResult.isEmpty()) {
                userTransaction.commit();
            } else {
                userTransaction.rollback();
            }

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            firstResult = GeneralConst.EMPTY_STRING;
            try {
                userTransaction.rollback();
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return firstResult;

    }

    public boolean execBulkQueryV2(
            String queryRole,
            String returnValueRole,
            HashMap<String, Object> paramsRole,
            HashMap<String, Object[]> bulkQueryDetailRole,
            List<String> listAssujetti,
            String userId,
            String articleRole) {

        userTransaction = contextEJB.getUserTransaction();

        boolean result;

        try {

            if (!isBulkQueryValid(bulkQueryDetailRole)) {
                result = false;
                throw new IllegalArgumentException(INVALID_ARGUMENT_TEXT);
            }

            userTransaction.begin();

            setBulkInsertV2(queryRole, returnValueRole, paramsRole, bulkQueryDetailRole,
                    listAssujetti, userId, articleRole.trim());

            userTransaction.commit();

            result = true;

        } catch (Exception e) {
            try {
                userTransaction.rollback();
            } catch (IllegalStateException | SystemException ex) {
                CustumException.BasicLogException(ex);
            }
            result = false;
            try {
                CustumException.BasicLogException(e);
                throw e;
            } catch (Exception ex) {
                CustumException.BasicLogException(ex);
            }
        }

        return result;

    }

    private void setBulkInsertV2(
            String queryRole,
            String returnValueRole,
            HashMap<String, Object> paramsRole,
            HashMap<String, Object[]> bulkQueryDetailRole,
            List<String> listAssujetti,
            String userId,
            String articleRoleOfRole) {

        String roleId = getStoredProcedureOutput(queryRole, returnValueRole, paramsRole);
        String articleRoleOfxtrait = GeneralConst.EMPTY_STRING;

        int i = 0;
        String position = GeneralConst.EMPTY_STRING;

        for (String codeAssujetti : listAssujetti) {
            i++;
            position = org.apache.commons.lang.StringUtils.leftPad(i + GeneralConst.EMPTY_STRING, 5,
                    GeneralConst.Number.ZERO);

            HashMap<String, Object> bulkQueryExtraitRole = new HashMap<String, Object>();

            String returnValueExtrait = RecouvrementConst.ParamQuery.EXTRAIT_ROLE_ID;
            String queryExtraitRole = SQLQueryRecouvrement.Role.F_NEW_EXTRAIT_DE_ROLE;

            bulkQueryExtraitRole.put(RecouvrementConst.ParamQuery.AGENT_CREAT, userId);
            bulkQueryExtraitRole.put(RecouvrementConst.ParamQuery.PERSONNE, codeAssujetti);
            bulkQueryExtraitRole.put(RecouvrementConst.ParamQuery.FK_ROLE, roleId);
            articleRoleOfxtrait = GeneralConst.EMPTY_STRING;
            articleRoleOfxtrait = articleRoleOfRole.concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE).concat(position);

            bulkQueryExtraitRole.put(RecouvrementConst.ParamQuery.ARTICLE_ROLE, articleRoleOfxtrait.trim());

            String extraitId = getStoredProcedureOutput(queryExtraitRole, returnValueExtrait, bulkQueryExtraitRole);

            String[] tabExtraitId = extraitId.split(";");

            int paramSize = bulkQueryDetailRole.size() + 1;

            for (int j = 0; j <= paramSize; j++) {

                Iterator<String> keys = bulkQueryDetailRole.keySet().iterator();

                while (keys.hasNext()) {

                    String key = keys.next();
                    String[] splitKey = key.split(":");
                    int index = Integer.parseInt(splitKey[0]);
                    String req = splitKey[1];
                    String assujetti = splitKey[2];

                    if (index == j) {

                        if (assujetti.equals(tabExtraitId[1])) {

                            req = String.format(req, roleId, tabExtraitId[0]);
                            Query query = (Query) getEntityManager().createNativeQuery(req);
                            setParameters(query, false, bulkQueryDetailRole.get(key));
                            query.executeUpdate();
                        }
                        break;
                    }
                }
            }
        }
    }


}
