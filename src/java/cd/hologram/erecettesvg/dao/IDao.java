
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.dao;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author tony.lundja
 */
public interface IDao<T> {

  public T find(Class<T> entity, T id);

    public T find(String query, Class<T> entity, boolean like, T... params);

    public T find(String query, Class<T> entity, Integer limit, boolean like, T... params);

    public T findJPQL(String queryJPQL, boolean like, T... params);

    public T findJPQL(String queryJPQL, Integer limit, boolean like, T... params);

    public T findNamedJPQL(String namedQuery, boolean like, T... params);

    public T findNamedJPQL(String namedQuery, Integer limit, boolean like, T... params);

    public boolean executeNativeQuery(String requeteNative, Object... params);
    
    public Integer executeNativeQueryV2(String requeteNative, Object... params);
    
    public String getStoredProcedureOutput(String procedureName,String outputName,HashMap<String,Object> params);

    public Object getSingleResult(String query, Object... params);

    public boolean create(T... entities);

    public boolean create(List<T> entities);

    public boolean createAndUpdate(T... entities);

    public boolean createAndUpdate(List<T> entities);

    public boolean entityExist(T entity);

    public boolean update(T... entities);

    public boolean update(List<T> entities);

    public String generatekey(String table, String idColumn, String prefix, int nbrOrdrSize, String sifix);

    public Object executeProcedure(String queryString, T... params);
    
    public Float getSingleResultToBigDecimal(String query,Object... params);
    
    public String getSingleResultToString(String query,Object... params);
    
    public Integer getSingleResultToInteger(String query);
    
    public boolean executeBulkQuery(HashMap<String, Object[]> params);
    
    public boolean executeStoredProcedure(String requeteNative, Object... params);
   
    public BigDecimal getSingleResultToBigDecimal2(String query,Object... params);

}