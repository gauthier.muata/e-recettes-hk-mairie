/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.dao;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author moussa.toure
 */
public class Dao {

    DaoImpl daoImpl = lookupDaoImplBean();

    public DaoImpl getDaoImpl() {
        return daoImpl;
    }

    private DaoImpl lookupDaoImplBean() {
        try {
            Context c = new InitialContext();
            return (DaoImpl) c.lookup("java:global/erecettesvg/DaoImpl!cd.hologram.erecettesvg.dao.DaoImpl");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
