/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.AbComplementBien;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.CategorieTypeComplement;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.ComplementForme;
import cd.hologram.erecettesvg.models.TypeComplement;
import cd.hologram.erecettesvg.models.TypeComplementBien;
import cd.hologram.erecettesvg.sql.SQLQueryGestionArticleBudgetaire;
import cd.hologram.erecettesvg.sql.SQLQueryGestionComplement;
import cd.hologram.erecettesvg.util.Casting;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class GestionComplementBusiness {

    private static final Dao myDao = new Dao();

    public static List<ComplementForme> getComplementFormeByType(String codeType) {

        try {
            String query = SQLQueryGestionComplement.GET_COMPLEMENT_FORME_BY_TYPE;
            List<ComplementForme> complementFormeList = (List<ComplementForme>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementForme.class), false, codeType);
            return complementFormeList;

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<TypeComplement> getTypeComplementByIntitule(String valueSearch, String codeForme) {

        String sqlQueryMaster;
        boolean checks = false;
        String secondQuery = GeneralConst.EMPTY_STRING;
        String primaryQuery = GeneralConst.EMPTY_STRING;

        primaryQuery = "WHERE CF.FORME_JURIDIQUE = '" + codeForme + "' AND CF.ETAT = 1) AND TC.ETAT = 1 AND TC.TYPE IN ('01')";

        if (!valueSearch.equals("")) {

            secondQuery = "AND TC.INTITULE LIKE '%" + valueSearch + "%'";
            checks = true;
        }

        sqlQueryMaster = SQLQueryGestionComplement.SELECT_TYPE_COMPLEMENT_BY_INTITULE;

        sqlQueryMaster = String.format(sqlQueryMaster, primaryQuery, secondQuery);

        List<TypeComplement> listComplement = (List<TypeComplement>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(TypeComplement.class), checks, valueSearch.trim());
        return listComplement;
    }

    public static CategorieTypeComplement getCategorieTypeComplementByCode(String code) {

        try {
            String query = SQLQueryGestionComplement.GET_CATEGORIE_TYPE_COMPLEMENT_BY_CODE;
            List<CategorieTypeComplement> categorieTypeComplemList = (List<CategorieTypeComplement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CategorieTypeComplement.class), false, code);
            return categorieTypeComplemList.isEmpty() ? null : categorieTypeComplemList.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean affecterComplementForme(List<ComplementForme> complementFormeList) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {
            for (ComplementForme complForme : complementFormeList) {
                bulkQuery.put(counter + SQLQueryGestionComplement.AFFECTER_COMPLEMENT_FORME, new Object[]{
                    complForme.getFormeJuridique().getCode().trim(),
                    complForme.getComplement().getCode().trim(),
                    complForme.getEtat()
                });
                counter++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean desactiverComplementForme(ComplementForme cpl) {

        boolean result;
        String query = SQLQueryGestionComplement.DESACTIVER_COMPLEMENT_FORME;

        try {
            result = myDao.getDaoImpl().executeStoredProcedure(query, cpl.getEtat(), cpl.getCode());
            return result;

        } catch (Exception e) {
            throw e;
        }
    }

    public static AbComplementBien getArticleComplementByCode(String code) {

        try {
            String query = SQLQueryGestionComplement.SELECT_ARTICLE_COMPLEMENT_BY_CODE;
            List<AbComplementBien> articlComplBien = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, code);
            return articlComplBien.isEmpty() ? null : articlComplBien.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getListArticleNonComplementByIntitule(String valueSearch) {

        String sqlQuery = SQLQueryGestionComplement.SELECT_ARTICLE_NON_COMPLEMENT_BY_INTITULE;

        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), true, valueSearch.trim());
        return listArticleBudgetaire;
    }

    public static List<TypeComplementBien> getComplementBienByType(String codeType) {

        try {
            String query = SQLQueryGestionComplement.GET_COMPLEMENT_FORME_BY_TYPE;
            List<TypeComplementBien> complementBienList = (List<TypeComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeComplementBien.class), false, codeType);
            return complementBienList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<TypeComplement> getTypeComplementBien(String valueSearch, String codeTypeBien) {

        String sqlQueryMaster;
        boolean checks = false;
        String secondQuery = GeneralConst.EMPTY_STRING;
        String primaryQuery = GeneralConst.EMPTY_STRING;

        primaryQuery = "WHERE TCB.TYPE_BIEN = '" + codeTypeBien + "' AND TCB.ETAT = 1) AND TC.ETAT = 1 AND TC.TYPE IN ('02')";

        if (!valueSearch.equals("")) {

            secondQuery = "AND TC.INTITULE LIKE '%" + valueSearch + "%'";
            checks = true;
        }

        sqlQueryMaster = SQLQueryGestionComplement.SELECT_TYPE_COMPLEMENT_BIEN;

        sqlQueryMaster = String.format(sqlQueryMaster, primaryQuery, secondQuery);

        List<TypeComplement> listComplement = (List<TypeComplement>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(TypeComplement.class), checks, valueSearch.trim());
        return listComplement;
    }

    public static boolean affecterComplementBien(List<TypeComplementBien> typeComplementBienList) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {
            for (TypeComplementBien typeComplBien : typeComplementBienList) {
                bulkQuery.put(counter + SQLQueryGestionComplement.F_NEW_COMPLEMENT_TYPE_BIEN, new Object[]{
                    typeComplBien.getTypeBien().getCode().trim(),
                    typeComplBien.getComplement().getCode().trim(),
                    null
                });
                counter++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean desactiverComplementBien(String codeTypeComplementBien) {
        boolean result;
        String query = SQLQueryGestionComplement.DESACTIVER_TYPE_COMPLEMENT_BIEN;

        try {
            result = myDao.getDaoImpl().executeStoredProcedure(query, codeTypeComplementBien);
            return result;

        } catch (Exception e) {
            throw e;
        }
    }

    public static AbComplementBien getArticleComplementByTypeComplement(String codeTypeCompl) {

        try {
            String query = SQLQueryGestionComplement.SELECT_ARTICLE_COMPLEMENT_BY_TYPE_COMPLEMENT;
            List<AbComplementBien> articlComplBien = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, codeTypeCompl);
            return articlComplBien.isEmpty() ? null : articlComplBien.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> loadArticleNonAffecterComplement(
            String valueSearch, String codeComplenet, String codeArticle) {

        String sqlQueryMaster;
        boolean checks = false;
        String secondQuery = GeneralConst.EMPTY_STRING;
        String primaryQuery = GeneralConst.EMPTY_STRING;

        if (!codeArticle.equals("")) {
            primaryQuery = "AND AB.CODE NOT IN (SELECT ABC.ARTICLE_BUDGETAIRE FROM T_AB_COMPLEMENT_BIEN ABC WITH "
                    + "(READPAST) WHERE ABC.ARTICLE_BUDGETAIRE = '" + codeArticle + "' "
                    + "AND ABC.TYPE_COMPLEMENT_BIEN = '" + codeComplenet + "') ";
        }

        if (!valueSearch.equals("")) {

            secondQuery = "AND AB.INTITULE LIKE '%" + valueSearch + "%'";
            checks = true;
        }

        sqlQueryMaster = SQLQueryGestionComplement.SELECT_ARTICLE_NON_AFFECTER_COMPLEMENT_BIEN;

        sqlQueryMaster = String.format(sqlQueryMaster, primaryQuery, secondQuery);

        List<ArticleBudgetaire> listArticle = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), checks, valueSearch.trim());
        return listArticle;
    }
    
     public static boolean affecterArticleComplement(AbComplementBien abCompl) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {
            if (abCompl.getCode() != null){
                bulkQuery.put(counter + SQLQueryGestionComplement.UPDATE_AB_COMPLEMENT_BIEN, new Object[]{
                    abCompl.getTypeComplementBien().getCode().trim(),
                    abCompl.getArticleBudgetaire().getCode().trim(),
                    abCompl.getCode().trim()
                });
            }else{
                 bulkQuery.put(counter + SQLQueryGestionComplement.F_NEW_AB_COMPLEMENT_BIEN, new Object[]{
                    abCompl.getTypeComplementBien().getCode().trim(),
                    abCompl.getArticleBudgetaire().getCode().trim()
                });
            }
               

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

}
