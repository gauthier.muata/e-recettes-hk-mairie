/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.NatureArticleBudgetaire;
import cd.hologram.erecettesvg.models.PalierTauxPenalite;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.TypePenalite;
import cd.hologram.erecettesvg.util.Casting;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class GestionPenaliteBackendBusiness {

    private static final Dao myDao = new Dao();

    public static List<Penalite> getListPenalites() {

        String sqlQuery = "select p.* from T_PENALITE p WITH (READPAST) ORDER BY p.INTITULE";

        List<Penalite> listPenalite = (List<Penalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Penalite.class), false);
        return listPenalite;
    }

    public static List<Penalite> getListPenalitesV2(String valueSearch) {

        String sqlQuery = "select p.* from T_PENALITE p WITH (READPAST) WHERE p.INTITULE LIKE ?1  ORDER BY p.INTITULE";

        List<Penalite> listPenalite = (List<Penalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Penalite.class), true, valueSearch.trim());
        return listPenalite;
    }

    public static List<Penalite> getListPenalitesV3() {

        String sqlQuery = "select p.code,p.intitule from T_PENALITE p WITH (READPAST) where p.etat = 1 ORDER BY p.INTITULE";

        List<Penalite> listPenalite = (List<Penalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Penalite.class), false);
        return listPenalite;
    }

    public static List<TypePenalite> getListTypePenalites() {

        String sqlQuery = "select * from T_Type_Penalite with (readpast) where etat = 1 order by intitule";

        List<TypePenalite> listTypePenalite = (List<TypePenalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(TypePenalite.class), false);
        return listTypePenalite;
    }

    public static boolean savePenalite(
            String intitule,
            String type,
            boolean palier,
            boolean visibility,
            int state,
            String userId,
            boolean applyMonth) {

        boolean result;

        String sqlQuery = "EXEC F_NEW_PENALITE ?1,?2,?3,?4,?5,?6,?7";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                intitule.trim(),
                type.trim(),
                palier,
                visibility,
                state,
                userId,
                applyMonth);
        return result;
    }

    public static boolean updatePenalite(
            String code,
            String intitule,
            String type,
            boolean palier,
            boolean visibility,
            int state,
            String userId,
            boolean applyMonth) {

        boolean result;

        String sqlQuery = "UPDATE T_PENALITE SET INTITULE = ?1, TYPE_PENALITE = ?2, PALIER = ?3, "
                + " VISIBILTE_UTILISATEUR = ?4, ETAT = ?5, APPLIQUER_MOIS_RETARD = ?6, DATE_MAJ = GETDATE(),"
                + " AGENT_MAJ = ?7 WHERE CODE = ?8";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                intitule.trim(),
                type.trim(),
                palier,
                visibility,
                state,
                applyMonth,
                userId,
                code.trim());
        return result;
    }

    public static List<PalierTauxPenalite> getListPalierTauxPenalites(String penaliteCode) {

        String sqlQuery = "select p.* from T_PALIER_TAUX_PENALITE p with (readpast) where p.etat = 1 and p.penalite = ?1";

        List<PalierTauxPenalite> listPalierTauxPenalite = (List<PalierTauxPenalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(PalierTauxPenalite.class), false, penaliteCode.trim());
        return listPalierTauxPenalite;
    }

    public static List<PalierTauxPenalite> getListPalierTauxPenalitesV2(String valueSearch, String penaliteCode) {

        String sqlQuery = "select p.* from T_PALIER_TAUX_PENALITE p with (readpast) where p.etat = 1 "
                + " and p.penalite in (select code from t_penalite with (readpast) where intitule LIKE ?1 and etat = 1)"
                + " and p.penalite = '%s'";

        sqlQuery = String.format(sqlQuery, penaliteCode);

        List<PalierTauxPenalite> listPalierTauxPenalite = (List<PalierTauxPenalite>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(PalierTauxPenalite.class), true, valueSearch.trim());
        return listPalierTauxPenalite;
    }

    public static NatureArticleBudgetaire getNatureArticleBudgetaireByCode(String code) {
        String sqlQuery = "select * from dbo.T_NATURE_ARTICLE_BUDGETAIRE where code = ?1";
        List<NatureArticleBudgetaire> natureArticleBudgetaires = (List<NatureArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NatureArticleBudgetaire.class), false, code.trim());
        return natureArticleBudgetaires.isEmpty() ? null : natureArticleBudgetaires.get(0);
    }

    public static List<NatureArticleBudgetaire> getListNatureArticleBudgetaires() {

        String sqlQuery = "select * from dbo.T_NATURE_ARTICLE_BUDGETAIRE order by intitule";

        List<NatureArticleBudgetaire> listNatureArticleBudgetaire = (List<NatureArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NatureArticleBudgetaire.class), false);
        return listNatureArticleBudgetaire;
    }

    public static boolean saveTauxPenalite(
            String penalite,
            String nature,
            String typePersonne,
            BigDecimal taux,
            BigDecimal bornMin,
            BigDecimal bornMax,
            String typeTaux,
            boolean isPercent,
            boolean recidive,
            boolean state) {

        boolean result;

        String sqlQuery = "INSERT INTO T_PALIER_TAUX_PENALITE (PENALITE,BORNE_INFERIEURE,"
                + " BORNE_SUPERIEURE,NATURE_ARTICLE_BUDGETAIRE,FORME_JURIDIQUE,RECIDIVE,"
                + " VALEUR,TYPE_VALEUR,EST_POURCENTAGE,NATURE,ETAT) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11)";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                penalite.trim(),
                bornMin,
                bornMax,
                nature.trim(),
                typePersonne,
                recidive,
                taux,
                typeTaux,
                isPercent,
                nature.trim(),
                state);
        return result;
    }

    public static boolean updateTauxPenalite(
            int id,
            String penalite,
            String nature,
            String typePersonne,
            BigDecimal taux,
            BigDecimal bornMin,
            BigDecimal bornMax,
            String typeTaux,
            boolean isPercent,
            boolean recidive,
            boolean state) {

        boolean result;

        String sqlQuery = "UPDATE T_PALIER_TAUX_PENALITE SET "
                + " PENALITE = ?1,BORNE_INFERIEURE = ?2,"
                + " BORNE_SUPERIEURE = ?3,NATURE_ARTICLE_BUDGETAIRE = ?4,"
                + " FORME_JURIDIQUE = ?5, RECIDIVE = ?6, VALEUR = ?7,"
                + " TYPE_VALEUR = ?8,EST_POURCENTAGE = ?9,"
                + " NATURE = ?10,ETAT = ?11 WHERE ID = ?12";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                penalite.trim(),
                bornMin,
                bornMax,
                nature.trim(),
                typePersonne,
                recidive,
                taux,
                typeTaux,
                isPercent,
                nature.trim(),
                state, id);
        return result;
    }

    public static boolean deleteTauxPenalite(int id) {

        boolean result;

        String sqlQuery = "UPDATE T_PALIER_TAUX_PENALITE SET ETAT = 0 WHERE ID = ?1";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, id);
        return result;
    }

    public static boolean deleteTypePenalite(String code, int userId) {

        boolean result;

        String sqlQuery = "UPDATE t_type_penalite SET ETAT = 0, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE Code = ?2";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, userId, code);
        return result;
    }

    public static boolean saveTypePenalite(String code, String intitule, int userId) {

        boolean result;
        String sqlQuery = GeneralConst.EMPTY_STRING;

        if (code.isEmpty()) {
            sqlQuery = " EXEC F_NEW_TYPE_PENALITE ?1,?2";
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, intitule.trim(), userId);
        } else {
            sqlQuery = "UPDATE t_type_penalite SET INTITULE = ?1, DATE_MAJ = GETDATE(), AGENT_MAJ = ?2 WHERE Code = ?3";
            result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, intitule.trim(), userId, code.trim());
        }

        return result;
    }
}
