/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Jdossier;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.util.Casting;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class CompteCourantFiscalBusiness {
    
    private static final Dao myDao = new Dao();

    public static List<NoteCalcul> getListNoteCalculByAssujetti(String assujetti) {


        String sqlQuery = "SELECT * from T_NOTE_CALCUL nc WITH (READPAST) WHERE nc.PERSONNE = ?1 ORDER BY nc.DATE_CREAT";

        List<NoteCalcul> listNoteCalcul = (List<NoteCalcul>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, assujetti.trim());
        return listNoteCalcul;
    }
    
    public static List<Jdossier> getListJDossierByAssujetti(String assujetti) {


        String sqlQuery = "SELECT j.* from T_JDOSSIER j With (READPAST) WHERE j.DOSSIER = ?1";

        List<Jdossier> listJdossier = (List<Jdossier>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Jdossier.class), false, assujetti.trim());
        return listJdossier;
    }
}
