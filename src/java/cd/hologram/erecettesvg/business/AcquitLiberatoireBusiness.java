/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.EtatSoldePaiement;
import cd.hologram.erecettesvg.sql.*;
import cd.hologram.erecettesvg.util.*;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author emmanuel.tsasa
 */
public class AcquitLiberatoireBusiness {

    private static final Dao myDao = new Dao();

    public static List<NoteCalcul> getListNoteCalculForEditionCpi(
            String libelle,
            boolean searchByName,
            boolean allSite,
            boolean allService,
            String idUser,
            String codeService,
            String codeSite) {

        String secondaryQuery_One = GeneralConst.EMPTY_STRING,
                secondaryQuery_Two = GeneralConst.EMPTY_STRING,
                secondaryQuery_Three = GeneralConst.EMPTY_STRING,
                primaryQuery = GeneralConst.EMPTY_STRING;

        boolean like = false;

        if (searchByName) {
            secondaryQuery_One = SQLQueryAcquitLiberatoire.AND_SEARCH_BY_NAME;
            like = true;
        } else {
            secondaryQuery_One = SQLQueryAcquitLiberatoire.AND_SEARCH_BY_NP;
            like = false;
        }

        secondaryQuery_Two = (!allSite)
                ? String.format(SQLQueryAcquitLiberatoire.AND_SEARCH_BY_SITE, codeSite, idUser)
                : GeneralConst.EMPTY_STRING;

        secondaryQuery_Three = (!allService)
                ? String.format(SQLQueryAcquitLiberatoire.AND_SEARCH_BY_SERVICE, codeService)
                : GeneralConst.EMPTY_STRING;

        primaryQuery = SQLQueryAcquitLiberatoire.LIST_NOTES_CALCUL_FOR_EDIT;

        primaryQuery = String.format(primaryQuery, secondaryQuery_One,
                secondaryQuery_Two, secondaryQuery_Three);

        List<NoteCalcul> listNoteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), like, libelle.trim());
        return listNoteCalculs;
    }

    public static List<NoteCalcul> getListAdvancedNoteCalculForEditionCpi(String codeSite, String codeService,
            String dateDebut, String dateFin) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two = "";
        String primaryQuery = "";

        if (codeSite.equals("*")) {

            secondaryQuery_One = " AND NC.NUMERO IN (SELECT DISTINCT NP.NOTE_CALCUL FROM T_NOTE_PERCEPTION NP "
                    + " WHERE DBO.FDATE(NP.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "'"
                    + " AND NP.ETAT > 0)";
        } else {
            secondaryQuery_One = " AND NC.SITE = '" + codeSite.trim() + "' AND "
                    + " NC.NUMERO IN (SELECT DISTINCT NP.NOTE_CALCUL FROM T_NOTE_PERCEPTION NP "
                    + " WHERE DBO.FDATE(NP.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "' "
                    + " AND NP.ETAT > 0)";
        }

        if (!codeService.equals("*")) {
            secondaryQuery_Two = " AND NC.SERVICE = '" + codeService.trim() + "'";
        } else {
            secondaryQuery_Two = "";
        }

        primaryQuery = "SELECT NC.* FROM T_NOTE_CALCUL NC WITH (READPAST) JOIN T_PERSONNE P WITH (READPAST) ON NC.PERSONNE = P.CODE WHERE NC.ETAT = 1"
                + " AND NOT NC.AGENT_CLOTURE IS NULL AND NOT NC.DATE_CLOTURE IS NULL AND NC.AGENT_VALIDATION IS NOT NULL "
                + " %s %s AND NC.NUMERO IN (SELECT DISTINCT NP.NOTE_CALCUL FROM T_NOTE_PERCEPTION NP "
                + " WHERE NP.NUMERO NOT IN (SELECT DISTINCT CPI.NOTE_PERCEPTION FROM T_ACQUIT_LIBERATOIRE CPI) "
                + " AND NP.COMPTE_BANCAIRE IS NOT NULL AND NP.NBR_IMPRESSION > 0) ORDER BY P.NOM";

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two);

        List<NoteCalcul> listNoteCalculs = (List<NoteCalcul>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(NoteCalcul.class), false, "");
        return listNoteCalculs;
    }

    public static ArticleBudgetaire getArticleBudgetaireByNc(String numeroNC) {
        String query = SQLQueryAcquitLiberatoire.GET_ARTICLE_BUDGETAIRE_BY_NC;
        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, numeroNC.trim());
        return listArticleBudgetaire.isEmpty() ? null : listArticleBudgetaire.get(0);
    }

    public static List<Periodicite> getListPeriodicites() {
        String query = SQLQueryAcquitLiberatoire.GET_LIST_PERIODICITE;
        List<Periodicite> listPeriodicites = (List<Periodicite>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Periodicite.class), false);
        return listPeriodicites;
    }

    public static NotePerception getNotePerceptionByNoteCalcul(String numero) {
        String query = SQLQueryAcquitLiberatoire.GET_NOTE_PERCEPTION_BY_NC;
        List<NotePerception> listNotePerception = (List<NotePerception>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero.trim());
        return listNotePerception.isEmpty() ? null : listNotePerception.get(0);
    }

    public static EtatSoldePaiement getEtatSoldePaiementByNc(String noteCalcul) {
        String query = SQLQueryAcquitLiberatoire.GET_ETAT_SOLDE_PAIEMENT_BY_NC;
        List<EtatSoldePaiement> listEtatSoldePaiements = (List<EtatSoldePaiement>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(EtatSoldePaiement.class), false, noteCalcul.trim());
        return listEtatSoldePaiements.isEmpty() ? null : listEtatSoldePaiements.get(0);
    }

    public static CompteBancaire getCompteBancaireByCode(String codeCompteBancaire) {
        String query = SQLQueryAcquitLiberatoire.GET_COMPTE_BANCAIRE_BY_CODE;
        List<CompteBancaire> listCompteBancaire = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(CompteBancaire.class), false, codeCompteBancaire.trim());
        return listCompteBancaire.isEmpty() ? null : listCompteBancaire.get(0);
    }

    public static NotePerception getNotePerceptionMereByNC(String numero) {
        String query = SQLQueryAcquitLiberatoire.GET_NOTE_PERCEPTION_MERE_BY_NC;
        List<NotePerception> listNotePerception = (List<NotePerception>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero.trim());
        return listNotePerception.isEmpty() ? null : listNotePerception.get(0);
    }

    public static List<Journal> getListJournalByDocumentApure(String numeroDocument) {
        String query = SQLQueryAcquitLiberatoire.LIST_JOURNAL_BY_DOCUMENT_APURE;
        List<Journal> listJournals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, numeroDocument.trim());
        return listJournals;
    }

    public static FichePriseCharge getFichePriseChargeByNc(String numero, boolean isOK) {

        String query = GeneralConst.EMPTY_STRING;

        if (isOK) {
            query = SQLQueryAcquitLiberatoire.GET_FICHE_PRISE_CHARGE_BY_NC_1;
        } else {
            query = SQLQueryAcquitLiberatoire.GET_FICHE_PRISE_CHARGE_BY_NC_2;
        }

        List<FichePriseCharge> listFichePriseCharges = (List<FichePriseCharge>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false, numero.trim());
        return listFichePriseCharges.isEmpty() ? null : listFichePriseCharges.get(0);
    }

    public static Amr getAmrByNc(String numero, boolean isOK) {

        String query = GeneralConst.EMPTY_STRING;

        if (isOK) {
            query = SQLQueryAcquitLiberatoire.GET_AMR_BY_NC_1;
        } else {
            query = SQLQueryAcquitLiberatoire.GET_AMR_BY_NC_2;
        }

        List<Amr> listAmrs = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, numero.trim());
        return listAmrs.isEmpty() ? null : listAmrs.get(0);
    }

    public static Journal getJournalByDocumentApure(String numeroDocument) {
        String query = SQLQueryAcquitLiberatoire.JOURNAL_BY_DOCUMENT_APURE;
        List<Journal> listJournals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, numeroDocument.trim());
        return listJournals.isEmpty() ? null : listJournals.get(0);
    }
    public static Journal getJournalByDocumentApureVolet(String numeroDocument, String volet) {
        String query = SQLQueryAcquitLiberatoire.JOURNAL_BY_DOCUMENT_APURE;
        List<Journal> listJournals = (List<Journal>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Journal.class), false, numeroDocument.trim(), volet.trim());
        return listJournals.isEmpty() ? null : listJournals.get(0);
    }

    public static List<SuiviComptableDeclaration> getListSuiviComptableDeclarationByNc(String numeroNc) {
        String query = SQLQueryAcquitLiberatoire.LIST_SUIVI_COMPTABLE_BY_NC;
        List<SuiviComptableDeclaration> listSuiviComptableDeclaration = (List<SuiviComptableDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(SuiviComptableDeclaration.class), false, numeroNc.trim());
        return listSuiviComptableDeclaration;
    }

    public static String getGenerateAcquitLiberatoire(AcquitLiberatoire acquitLiberatoire) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "KEYS";

        String query = "F_NEW_ACQUIT_LIBERATOIRE_WEB";

        params.put("NUMERO_PAPIER", acquitLiberatoire.getNumeroPapier().trim());
        params.put("NUMERO_TIMBRE", acquitLiberatoire.getNumeroTimbre().trim());
        params.put("NOTE_PERCEPTION", acquitLiberatoire.getNotePerception().getNumero().trim());
        params.put("AGENT_CREAT", acquitLiberatoire.getAgentCreat());
        params.put("SITE", acquitLiberatoire.getSite().trim());
        params.put("RECEPTIONNISTE", acquitLiberatoire.getReceptionniste() == null
                ? null
                : acquitLiberatoire.getReceptionniste().getCode().trim());
        params.put("OBSERVATION", acquitLiberatoire.getObservation());

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (!result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static Archive getIdArchiveByReference(String refence) {
        String query = "SELECT * FROM  T_ARCHIVE WITH (READPAST)  WHERE  REF_DOCUMENT = ?1";
        List<Archive> listArchive = (List<Archive>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Archive.class), false, refence.trim());
        return listArchive.isEmpty() ? null : listArchive.get(0);
    }

    public static NotePerception getNotePerceptionByNumero(String numero) {
        String query = SQLQueryAcquitLiberatoire.GET_NOTE_PERCEPTION_BY_NUMERO;
        List<NotePerception> listNotePerception = (List<NotePerception>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, numero.trim());
        return listNotePerception.isEmpty() ? null : listNotePerception.get(0);
    }

    public static List<AcquitLiberatoire> getListAcquitLiberatoiresPrint(
            String index,
            int typeSearch,
            boolean allSite,
            boolean allService,
            String codeSite,
            String idUser,
            String codeService) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two = "";
        String secondaryQuery_Three = "";
        String primaryQuery = "";

        boolean like = false;

        if (typeSearch == 0) {
            secondaryQuery_One = " AND A.NOTE_PERCEPTION IN (SELECT NP.NUMERO FROM T_NOTE_PERCEPTION NP WITH (READPAST)"
                    + " WHERE NP.NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST)"
                    + " WHERE NC.PERSONNE IN (SELECT P.CODE FROM T_PERSONNE P WITH (READPAST)"
                    + " WHERE (P.NOM LIKE ?1 OR P.POSTNOM LIKE ?1 OR P.PRENOMS LIKE ?1))))";
            like = true;
        } else if (typeSearch == 1) {
            secondaryQuery_One = " AND A.NOTE_PERCEPTION = ?1";
            like = false;
        } else if (typeSearch == 2) {
            secondaryQuery_One = " AND A.CODE = ?1";
            like = false;
        }

        if (!allSite) {
            secondaryQuery_Two = " AND A.SITE = '" + codeSite.trim() + "'"
                    + " AND A.SITE IN (SELECT DISTINCT AST.CODESITE FROM T_AGENT_SITE AST WITH (READPAST)"
                    + " WHERE AST.CODEAGENT = " + idUser + " AND AST.ETAT = 1)";
        } else {
            secondaryQuery_Two = "";
        }

        if (!allService) {
            secondaryQuery_Three = " AND A.NOTE_PERCEPTION IN (SELECT NP.NUMERO FROM T_NOTE_PERCEPTION NP WITH (READPAST)"
                    + " WHERE NP.NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST) "
                    + " WHERE NC.SERVICE = '" + codeService.trim() + "'))";
        } else {
            secondaryQuery_Three = "";
        }

        primaryQuery = "SELECT A.* FROM T_ACQUIT_LIBERATOIRE A WITH (READPAST) WHERE A.ETAT = 1 %s %s %s "
                + " ORDER BY A.DATE_CREAT DESC, A.CODE DESC";

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, secondaryQuery_Three);

        List<AcquitLiberatoire> listAcquitLiberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), like, index.trim());
        return listAcquitLiberatoires;
    }

    public static AcquitLiberatoire getAcquitLiberatoireByNumero(String code) {
        String query = "SELECT * FROM T_ACQUIT_LIBERATOIRE WHERE CODE = ?1 AND ETAT = 1";
        List<AcquitLiberatoire> liberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), false, code.trim());
        return liberatoires.isEmpty() ? null : liberatoires.get(0);
    }

    public static List<AcquitLiberatoire> getListAdvancedAcquitLiberatoiresPrint(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin) {

        String secondaryQuery_One = "";
        String secondaryQuery_Two = "";
        String primaryQuery = "";

        if (codeSite.equals("*")) {

            secondaryQuery_One = " AND DBO.FDATE(A.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "'";
        } else {
            secondaryQuery_One = " AND A.SITE = '" + codeSite.trim() + "' AND "
                    + " DBO.FDATE(A.DATE_CREAT) BETWEEN '" + dateDebut.trim() + "' AND '" + dateFin.trim() + "'";
        }

        if (!codeService.equals("*")) {
            secondaryQuery_Two = " AND A.NOTE_PERCEPTION IN (SELECT NP.NUMERO FROM T_NOTE_PERCEPTION NP WITH (READPAST) "
                    + " WHERE NP.NOTE_CALCUL IN (SELECT NC.NUMERO FROM T_NOTE_CALCUL NC WITH (READPAST) "
                    + " WHERE NC.SERVICE = '" + codeService.trim() + "'))";
        } else {
            secondaryQuery_Two = "";
        }

        primaryQuery = "SELECT A.* FROM T_ACQUIT_LIBERATOIRE A WITH (READPAST) WHERE A.ETAT = 1 %s %s "
                + " ORDER BY A.DATE_CREAT DESC, A.CODE DESC";

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two);

        List<AcquitLiberatoire> listAcquitLiberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), false, "");
        return listAcquitLiberatoires;
    }

    public static AcquitLiberatoire checkCPIFiche(String personne, String article, String cpi) {

        String primaryQuery = "SELECT * from T_ACQUIT_LIBERATOIRE AQ WITH (READPAST) WHERE CODE NOT IN (SELECT CPI_FICHE FROM T_NOTE_CALCUL NC WHERE NC.CPI_FICHE = AQ.CODE AND ETAT = 1) AND CODE = ?1 AND "
                + "NOTE_PERCEPTION IN (SELECT NUMERO from T_NOTE_PERCEPTION NP where NUMERO = AQ.NOTE_PERCEPTION AND NP.NOTE_CALCUL IN (SELECT NUMERO FROM T_NOTE_CALCUL NC WHERE NC.NUMERO = NP.NOTE_CALCUL AND NC.PERSONNE = ?2 AND NC.NUMERO IN (SELECT DNC.NOTE_CALCUL FROM T_DETAILS_NC DNC WHERE DNC.NOTE_CALCUL = NC.NUMERO AND DNC.ARTICLE_BUDGETAIRE = ?3 )))";

        List<AcquitLiberatoire> listAcquitLiberatoires;

        try {

            listAcquitLiberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), false, cpi, personne, article);

        } catch (Exception e) {
            throw e;
        }

        return listAcquitLiberatoires.isEmpty() ? null : listAcquitLiberatoires.get(0);
    }

    public static AcquitLiberatoire getAcquitLiberatoireByNumero(String code, String paramTaxe) {

        String query = "SELECT A.* FROM T_ACQUIT_LIBERATOIRE A WITH (READPAST) WHERE A.CODE = ?1 \n"
                + "AND A.ETAT = 1 AND A.NOTE_PERCEPTION IN (SELECT NP.NUMERO FROM T_NOTE_PERCEPTION NP WITH (READPAST)\n"
                + "WHERE NP.NOTE_CALCUL IN (SELECT DNC.NOTE_CALCUL FROM T_DETAILS_NC DNC WITH (READPAST)\n"
                + "WHERE ARTICLE_BUDGETAIRE IN (%s)))";

        query = String.format(query, paramTaxe);

        List<AcquitLiberatoire> liberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), false, code.trim());
        return liberatoires.isEmpty() ? null : liberatoires.get(0);
    }

    public static LogCpiCarteConducteurMoto getLogCpiByCpi(String codeCpi) {

        String query = "SELECT * FROM dbo.T_LOG_CPI_CARTE_CONDUCTEUR_MOTO WITH (READPAST) WHERE CPI = ?1 AND ETAT = 1";

        List<LogCpiCarteConducteurMoto> logCpiCarteConducteurMotos = (List<LogCpiCarteConducteurMoto>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(LogCpiCarteConducteurMoto.class), false, codeCpi.trim());
        return logCpiCarteConducteurMotos.isEmpty() ? null : logCpiCarteConducteurMotos.get(0);
    }
    
    public static FichePriseCharge getFichePriseChargeByReference(String reference) {
        String query = "select * from t_fiche_prise_charge fpc with (readpast) where fpc.numero_reference = ?1";
        List<FichePriseCharge> listFichePriseCharge = (List<FichePriseCharge>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(FichePriseCharge.class), false, reference.trim());
        return listFichePriseCharge.isEmpty() ? null : listFichePriseCharge.get(0);
    }
}
