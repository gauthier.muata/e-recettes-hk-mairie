/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.sql.SQLQueryLogin;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.util.*;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class ConnexionBusiness {

    private static final Dao myDao = new Dao();

    public static boolean updatePassword(String newPassword, int agentCode) {

        boolean updated;
        String masterQuery = String.format(SQLQueryLogin.UPDATE_AGENT_PASSWORD, newPassword, agentCode);

        try {
            updated = myDao.getDaoImpl().executeNativeQuery(masterQuery);
        } catch (Exception e) {
            throw e;
        }
        return updated;
    }
    
    public static boolean updatePassword(String login, String password) {
        boolean saved = false;
        try {
            String query = "UPDATE T_AGENT SET MDP = HASHBYTES ('MD5','" + password.trim() + "') WHERE CODE = ?1";
            saved = myDao.getDaoImpl().executeNativeQuery(query, login);
            return saved;
        } catch (Exception e) {
            return saved;
        }
    }

    public static Agent authentification(String login, String password) {
        try {
            String sqlPart = "AND MDP =  HASHBYTES ('MD5','" + password.trim() + "') AND ETAT = 1";
            String query = SQLQueryLogin.LOGIN_AGENT.concat(GeneralConst.SPACE).concat(sqlPart);
            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, login.trim());
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<AgentGu> getListGroupUserByAgent(int codeAgent) {
        try {
            String query = SQLQueryLogin.LIST_GROUP_USER_SELECT_BY_AGENT;
            List<AgentGu> agentGus = (List<AgentGu>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AgentGu.class), false, codeAgent);
            return agentGus;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<GuAcces> getListRightGroupUserByGroup(int codeGroup, int codeAgent) {
        try {
            String query = SQLQueryLogin.LIST_RIGHTS_GROUP_USER_SELECT_BY_GROUP;
            List<GuAcces> guAcceses = (List<GuAcces>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(GuAcces.class), false, codeGroup, codeAgent);
            return guAcceses;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<AccesUser> getListRightsUserByAgent(int codeAgent) {
        try {
            String query = SQLQueryLogin.LIST_RIGHTS_USER_SELECT_BY_AGENT;
            List<AccesUser> accesUsers = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, codeAgent);
            return accesUsers;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getAgentByCode(int codeAgent) {
        try {
            String query = SQLQueryLogin.GET_AGENT_BY_CODE;
            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeAgent);
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
    
    public static Agent getAgentByCode(String code) {
        try {
            String query = SQLQueryLogin.AGENT_BY_CODE;

            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code.trim());
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean updateSignature(String signature, int agentCode) {

        boolean updated;
        String masterQuery = String.format(SQLQueryLogin.UPDATE_AGENT_SIGNATURE, signature, agentCode);

        try {
            updated = myDao.getDaoImpl().executeNativeQuery(masterQuery);
        } catch (Exception e) {
            throw e;
        }
        return updated;
    }
    
    public static List<UtilisateurDivision> getUserDivision(int codeAgent) {
        try {
            String query = SQLQueryLogin.USER_DIVISION_BY_CODE;
            List<UtilisateurDivision> divisions = (List<UtilisateurDivision>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UtilisateurDivision.class), false, codeAgent);
            return divisions;
        } catch (Exception e) {
            throw e;
        }
    }

}
