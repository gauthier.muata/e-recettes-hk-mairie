/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.AssujettissementBusiness.propertiesConfig;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DernierAvertissement;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionMairie;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Role;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.pojo.DetailRolePrint;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.sql.SQLQueryAssujettissement;
import cd.hologram.erecettesvg.sql.SQLQueryPaiement;
import cd.hologram.erecettesvg.sql.SQLQueryRecouvrement;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.Tools;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONException;

/**
 *
 * @author bonheur.muntasomo
 */
public class RecouvrementBusiness {

    private static final Dao myDao = new Dao();
    static HashMap<String, Object[]> bulkDernierAvert;

    public static List<PeriodeDeclaration> getListDefaillantDeclarationByAdvancedSearch(String codeAB, String sqlQueryParam) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V1;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQueryParam);

        List<PeriodeDeclaration> listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAB.trim());
        return listPeriodeDeclarations;
    }

    public static List<PeriodeDeclaration> getListDefaillantDeclarationBySimpleSearch(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_MASTER_LIST_MED_V2;

        List<PeriodeDeclaration> listPeriodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codeAssujetti.trim());
        return listPeriodeDeclarations;
    }

    /*public static Med getMedByPerideDeclaration(int periodeID) {
     String query = SQLQueryRecouvrement.SELECT_MED_BY_PERIODE_DECLARATION;
     List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
     Casting.getInstance().convertIntoClassType(Med.class), false, periodeID);
     return meds.isEmpty() ? null : meds.get(0);
     }*/
    public static Med getMedByPerideDeclaration(int periodeID) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_PERIODE_DECLARATION;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Med getMedByPerideDeclaration(int periodeID, String typMed) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_PERIODE_DECLARATION_2;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID, typMed);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static String checkExistArticleRole(String articleRole) {
        String sqlQuery = SQLQueryRecouvrement.F_CHECK_EXIST_ARTICLE_ROLE.concat("('" + articleRole.trim() + "')");
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static Med getMedByNotePerception(String np) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_NOTE_PERCEPTION;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, np);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Med getMedById(String id) {
        String query = SQLQueryRecouvrement.SELECT_MED_BY_ID;
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, id);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static Boolean saveMed(
            String codeArticleBudgetaire,
            String assujettiCode,
            String periodeDeclaration,
            String echeanceDeclaration,
            String adresseCode,
            String agentCreat,
            float amountPeriodeDeclaration,
            int periodeId,
            String typeDoc,
            String numeroNp,
            String numeroNc,
            float amountPenalite
    ) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            counter++;

            switch (typeDoc) {
                case "DECLARATION":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, null, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;
                case "INVITATION_PAIEMENT":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, null, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;
                case "SERVICE":
                case "RELANCE":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED_SERVICE, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, null,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        amountPenalite,
                        new Date()
                    });
                    break;
                case "PAIEMENT":
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, numeroNp,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        new Date()
                    });
                    break;

                case "INVITATION_SERVICE":
                    Double amountPenalite1 = amountPenalite * 0.7;
                    Double amountPenalite2 = amountPenalite * 0.3;

                    String accountBankProvince = propertiesConfig.getProperty("VALEUR_COMPTE_BANCAIRE_PENALITE_PROVINCE");
                    String accountBankDRHKAT = propertiesConfig.getProperty("VALEUR_COMPTE_BANCAIRE_PENALITE_DRHKAT");

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_MED_SERVICE, new Object[]{
                        agentCreat,
                        assujettiCode,
                        periodeId, numeroNc, numeroNp,
                        typeDoc,
                        codeArticleBudgetaire,
                        adresseCode,
                        periodeDeclaration,
                        amountPeriodeDeclaration,
                        amountPenalite,
                        new Date()
                    });

                    counter++;

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_NOTE_PERCEPTION_PENALITE, new Object[]{
                        numeroNc,
                        amountPenalite1,
                        0, amountPenalite1,
                        periodeDeclaration,
                        echeanceDeclaration,
                        numeroNp,
                        agentCreat,
                        new Date(),
                        3,
                        accountBankDRHKAT
                    });

                    counter++;

                    bulkQuery.put(counter + SQLQueryRecouvrement.F_NEW_NOTE_PERCEPTION_PENALITE, new Object[]{
                        numeroNc,
                        amountPenalite2,
                        0, amountPenalite2,
                        periodeDeclaration,
                        echeanceDeclaration,
                        numeroNp,
                        agentCreat,
                        new Date(),
                        3,
                        accountBankProvince
                    });
                    break;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static LoginWeb getLoginWebByAssujetti(String assujettiCode) {
        String query = SQLQueryRecouvrement.SELECT_LOGIN_WEB_BY_ASSUJETTI;
        List<LoginWeb> loginWebs = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(LoginWeb.class), false, assujettiCode);
        return loginWebs.isEmpty() ? null : loginWebs.get(0);
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByAssujetti(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_V1;

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, codeAssujetti.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByAssujettiRole(String codeAssujetti) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_ROLE;

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, codeAssujetti.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByService_SiteAndPeriode(
            String codeService, String codeSite, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlParamService = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }
        if (codeService != null && codeSite != null) {
            if (!codeService.equals("*") && !codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "' AND NC.SITE = '" + codeSite.trim() + "'";
            } else if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            } else if (!codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
            }
        } else if (codeService != null) {
            if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            }
        } else if (!codeSite.equals("*")) {
            sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
        }

        String sqlParamDate = " AND DBO.FDATE(NP.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDate = String.format(sqlParamDate, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_V3;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamService, sqlParamDate);

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false,
                codeSite.trim(), codeService.trim());
        return notePerceptions;
    }

    public static List<NotePerception> getListNotePerceptionNotPaidByService_SiteAndPeriodeRole(
            String codeService, String codeSite, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlParamService = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }
        if (codeService != null && codeSite != null) {
            if (!codeService.equals("*") && !codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "' AND NC.SITE = '" + codeSite.trim() + "'";
            } else if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            } else if (!codeSite.equals("*")) {
                sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
            }
        } else if (codeService != null) {
            if (!codeService.equals("*")) {
                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            }
        } else if (!codeSite.equals("*")) {
            sqlParamService = " WHERE NC.SITE = '" + codeSite.trim() + "'";
        }

        String sqlParamDate = " AND DBO.FDATE(NP.DATE_CREAT) BETWEEN '%s' AND '%s'";
        sqlParamDate = String.format(sqlParamDate, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_NOT_PAID_ROLE2;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamService, sqlParamDate);

        List<NotePerception> notePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false,
                codeSite.trim(), codeService.trim());
        return notePerceptions;
    }

    public static Boolean saveRole(Role role, LogUser logUser,
            List<DetailRolePrint> listDetailsRole,
            List<String> listAssujettiClean, String userId, String articleRole) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> bulkQueryRole = new HashMap<>();
        HashMap<String, Object[]> bulkQueryDetailRole = new HashMap<>();

        try {

            String returnValueRole = RecouvrementConst.ParamQuery.ROLE_ID;
            String queryRole = SQLQueryRecouvrement.Role.F_NEW_ROLE;

            bulkQueryRole.put(RecouvrementConst.ParamQuery.PERIODE_DEBUT, role.getPeriodeDebut());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.PERIODE_FIN, role.getPeriodeFin());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.AGENT_CREAT, role.getAgentCreat().getCode());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.DESCRIPTION, role.getObservationReceveur());
            bulkQueryRole.put(RecouvrementConst.ParamQuery.ARTICLE_ROLE, role.getArticleRole().trim());
//            bulkQueryRole.put(RecouvrementConst.ParamQuery.MAC_ADRESSE, "");
//            bulkQueryRole.put(RecouvrementConst.ParamQuery.IP_ADRESSE, "");
//            bulkQueryRole.put(RecouvrementConst.ParamQuery.HOST_NAME, "");
//            bulkQueryRole.put(RecouvrementConst.ParamQuery.CONTEXT_BROWSER, "");

            if (listDetailsRole != null && !listDetailsRole.isEmpty()) {

                for (DetailRolePrint detailsRole : listDetailsRole) {
                    counter++;
                    bulkQueryDetailRole.put(counter + SQLQueryRecouvrement.Role.F_NEW_DETAILS_ROLE.concat(
                            detailsRole.getAssujetti().trim()), new Object[]{
                                detailsRole.getDocumentReference().trim(),
                                detailsRole.getTypeDocument().trim()
                            });

                }
            }

            result = myDao.getDaoImpl().execBulkQueryV2(
                    queryRole, returnValueRole, bulkQueryRole,
                    bulkQueryDetailRole, listAssujettiClean, userId, articleRole);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Taux getTauxByDevise(String devise) {
        String sqlQuery = SQLQueryRecouvrement.SELECT_TAUX_BY_DEVISE;
        List<Taux> tauxs = (List<Taux>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Taux.class), false, devise.trim());
        return tauxs.isEmpty() ? null : tauxs.get(0);
    }

    public static List<Role> loadProjetRoles(
            int typeSearch,
            String articleRoleValue,
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String codeEntite,
            String codeAgent) throws JSONException {

        if (dateDebut != null) {
            if (dateDebut.contains("-")) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains("-")) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        String sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_ArticleRole = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_DateCreate = GeneralConst.EMPTY_STRING;
        String sqlQueryMaster;
        String agentList = GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {

            agentList = Tools.getJsonListToString(codeAgent, "codeAgent");

            sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
//                    String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
//                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
//                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE),
//                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
//                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE));

            sqlQuerySecondary_By_ArticleRole = SQLQueryRecouvrement.Role.SELECT_ROLE_PART_QUERY_1_BY_ARTICLE_ROLE;
            sqlQuerySecondary_By_ArticleRole = String.format(sqlQuerySecondary_By_ArticleRole, articleRoleValue.trim());

        } else if (typeSearch == 2) {

            agentList = Tools.getListToString(codeAgent);

            sqlQuerySecondary_By_Site_Service = String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
                    Tools.getListToString(codeSite),
                    Tools.getListToString(codeService),
                    Tools.getListToString(codeSite),
                    Tools.getListToString(codeService));

            sqlQuerySecondary_By_DateCreate = SQLQueryRecouvrement.Role.SELECT_ROLE_PART_QUERY_4_BY_DATE_CREATE;
            sqlQuerySecondary_By_DateCreate = String.format(sqlQuerySecondary_By_DateCreate,
                    dateDebut, dateFin);

        }

        String paramAgent = GeneralConst.EMPTY_STRING; //" AND R.AGENT_CREAT IN (%s)";
//        paramAgent = String.format(paramAgent, agentList);

        sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_PROJET_ROLE;

        /*if (!codeProvince.equals(GeneralConst.ALL_2)) {

         if (!codeEntite.equals(GeneralConst.ALL_2)) {
         //sqlQuerySecondary_By_Site_Service = "";
         } else {
         sqlQuerySecondary_By_Site_Service = String.format(
         SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE_ALL,
         provinceList);
         }
         } else {
         sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
         }*/
        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlQuerySecondary_By_Site_Service,
                sqlQuerySecondary_By_ArticleRole,
                sqlQuerySecondary_By_DateCreate, paramAgent);

        try {

            List<Role> listRoles = (List<Role>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(Role.class), false);
            return listRoles;
        } catch (Exception e) {
            throw e;
        }

    }

    public static ExtraitDeRole getExtraitRoleByRole(String roleId) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_EXTRAIT_ROLE_BY_ROLE;
        List<ExtraitDeRole> extraitDeRoles = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, roleId.trim());
        return extraitDeRoles.isEmpty() ? null : extraitDeRoles.get(0);
    }

    public static boolean executeQueryBulkInsertRole(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static List<ExtraitDeRole> loadExtraitRoles(
            int typeSearch,
            String valueSearch,
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeRegister,
            String critereSearch,
            String codeEntite,
            String codeAgent) throws JSONException {

        if (dateDebut != null) {
            if (dateDebut.contains("-")) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains("-")) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }

        String sqlQuerySecondary_By_Critere = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_Site_Service = GeneralConst.EMPTY_STRING;
        String sqlQuerySecondary_By_DateCreate = GeneralConst.EMPTY_STRING;
        String sqlQueryMaster = GeneralConst.EMPTY_STRING;

        String agentList = GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {

            agentList = Tools.getJsonListToString(codeAgent, "codeAgent");

            switch (critereSearch.trim()) {
                case GeneralConst.Number.ONE:
                    sqlQuerySecondary_By_Critere = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_BY_CODE;
                    sqlQuerySecondary_By_Critere = String.format(sqlQuerySecondary_By_Critere, valueSearch);
                    break;
                case GeneralConst.Number.TWO:
                    sqlQuerySecondary_By_Critere = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_BY_ARTICLE_ROLE;
                    break;
            }

            sqlQuerySecondary_By_Site_Service = "";
            String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE),
                    Tools.getJsonListToString(codeSite, GeneralConst.SearchCode.SITE),
                    Tools.getJsonListToString(codeService, GeneralConst.SearchCode.SERVICE));

        } else if (typeSearch == 2) {

            agentList = Tools.getListToString(codeAgent);

            sqlQuerySecondary_By_DateCreate = SQLQueryRecouvrement.Role.SELECT_EXTRAIT_ROLE_PART_QUERY_4_BY_DATE_CREATE;
            sqlQuerySecondary_By_DateCreate = String.format(sqlQuerySecondary_By_DateCreate,
                    dateDebut, dateFin);

            sqlQuerySecondary_By_Site_Service = "";
//                    String.format(SQLQueryRecouvrement.Role.SELECT_DETAIL_ROLE_BY_SITE_SERVICE,
//                    Tools.getListToString(codeSite),
//                    Tools.getListToString(codeService),
//                    Tools.getListToString(codeSite),
//                    Tools.getListToString(codeService));
        }

        String sqlParamAgent = GeneralConst.EMPTY_STRING;
//                " AND EXR.AGENT_CREAT IN (%s)";
//        sqlParamAgent = String.format(sqlParamAgent, agentList);

        switch (typeRegister.trim()) {
            case GeneralConst.Number.ONE:
                sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_EXTRAIT_ROLE_A_ORDONNANCER;
                break;
            case GeneralConst.Number.TWO:
                sqlQueryMaster = SQLQueryRecouvrement.Role.SELECT_LIST_EXTRAIT_ROLE_ORDONNANCER;
                break;
        }

        sqlQueryMaster = String.format(sqlQueryMaster,
                sqlQuerySecondary_By_Site_Service,
                sqlQuerySecondary_By_Critere,
                sqlQuerySecondary_By_DateCreate,
                sqlParamAgent);

        try {

            List<ExtraitDeRole> listExtraitRole = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, valueSearch.trim());
            return listExtraitRole;
        } catch (Exception e) {
            throw e;
        }

    }

    public static String getNiveauExtraitRole(String extraitRoleId) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.F_GET_EXTRAIT_ROLE_NIVEAU.concat("('" + extraitRoleId.trim() + "')");
        String code = (String) myDao.getDaoImpl().getSingleResult(sqlQuery);
        return code;
    }

    public static List<NotePerception> getListNotePerceptionDependancy(String npMere) {
        String sqlQuery = SQLQueryRecouvrement.SELECT_NOTE_PERCEPTION_DEPENDANCY;
        List<NotePerception> listNotePerceptions = (List<NotePerception>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(NotePerception.class), false, npMere.trim());
        return listNotePerceptions;
    }

    public static String saveContrainte(
            String agentCreate,
            String assujettiCode,
            String dernierAvertissementId,
            LogUser logUser,
            String idHuissier) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "ID_CONTRAINTE";

        String query = "F_NEW_CONTRAINTE";

        params.put("AGENT_CREAT", agentCreate);
        params.put("PERSONNE", assujettiCode.trim());
        params.put("FK_DERNIER_AVERTISSEMENT", dernierAvertissementId.trim());
        params.put("HUISSIER_ID", idHuissier.trim());
        params.put("MAC_ADRESSE", logUser.getMacAddress().trim());
        params.put("IP_ADRESSE", logUser.getIpAddress().trim());
        params.put("HOST_NAME", logUser.getHostName().trim());
        params.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (result != null && !result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static String saveCommandement(
            String agentCreate,
            String assujettiCode,
            double fraisPoursuite,
            String contrainteId,
            LogUser logUser) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = "ID_COMMANDEMENT";

        String query = "F_NEW_COMMANDEMENT";

        params.put("AGENT_CREAT", agentCreate);
        params.put("PERSONNE", assujettiCode.trim());
        params.put("FK_CONTRAINTE", contrainteId.trim());
        params.put("FRAIS_POURSUITE", fraisPoursuite);
        params.put("MAC_ADRESSE", logUser.getMacAddress().trim());
        params.put("IP_ADRESSE", logUser.getIpAddress().trim());
        params.put("HOST_NAME", logUser.getHostName().trim());
        params.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

        if (result != null && !result.isEmpty()) {
            returnValue = result;
        }
        return returnValue;
    }

    public static Contrainte getContrainteByID(String iD) {
        String sqlQuery = SQLQueryRecouvrement.Contrainte.SELECT_CONTRAINTE_BY_ID;
        List<Contrainte> contrainte = (List<Contrainte>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Contrainte.class
                ), false, iD);
        return contrainte.isEmpty()
                ? null : contrainte.get(0);

    }

    public static Commandement getCommandementById(String idCommandement) {
        String sqlQuery = SQLQueryRecouvrement.Commandement.SELECT_COMMANDEMENT_BY_ID;
        List<Commandement> commandements = (List<Commandement>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Commandement.class
                ), false, idCommandement.trim());
        return commandements.isEmpty()
                ? null : commandements.get(0);
    }

    public static List<DetailsRole> getListDetailsRoleByExtrait(String extraitRole) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_LIST_DETAILS_ROLE_BY_EXTRAIT;
        List<DetailsRole> listDetailsRoles = (List<DetailsRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(DetailsRole.class
                ), false, extraitRole.trim());
        return listDetailsRoles;
    }

    public static DernierAvertissement getDernierAvertissementByExtraitRoleAndAssujetti(
            String extraitRoleId, String assujettiCode) {
        String sqlQuery = SQLQueryRecouvrement.DernierAvertissement.SELECT_DERNIER_AVERTISSEMENT_BY_EXTRAIT_AND_ASSUJETTI;
        List<DernierAvertissement> dernierAv = (List<DernierAvertissement>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(DernierAvertissement.class
                ), false,
                extraitRoleId.trim(), assujettiCode.trim());
        return dernierAv.isEmpty()
                ? null : dernierAv.get(0);

    }

    public static boolean saveDernierAvertissement(
            HttpServletRequest request,
            String fkExtraitRole,
            String fkPersonne,
            List<DetailsRole> listDetailsRole) {

        bulkDernierAvert = new HashMap<>();
        int counter = 0;
        Object[] valueObject = null;
        boolean result = false;

        try {

            String idUser = request.getParameter(GeneralConst.ID_USER);
            String operation = request.getParameter(GeneralConst.OPERATION);
            LogUser logUser = new LogUser(request, operation);

            counter++;
            bulkDernierAvert.put(counter + SQLQueryRecouvrement.DernierAvertissement.F_NEW_DERNIER_AVERTISSEMENT,
                    new Object[]{
                        idUser,
                        fkPersonne,
                        fkExtraitRole,
                        logUser.getMacAddress(),
                        logUser.getIpAddress(),
                        logUser.getHostName(),
                        logUser.getBrowserContext()
                    });

            /*if (!listDetailsRole.isEmpty()) {

             for (DetailsRole detailsRole : listDetailsRole) {

             switch (detailsRole.getFkTypeDocument().getCode().trim()) {
             case DocumentConst.DocumentCode.NP:

             NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(
             detailsRole.getDocumentRef().trim());

             if (np != null) {
             counter++;
             bulkDernierAvert.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
             np.getNoteCalcul().getNumero().trim(),
             np.getNumero().trim(),
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE").concat(np.getNumero()),
             GeneralConst.Numeric.ZERO,
             GeneralConst.Numeric.ZERO,
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE").concat(np.getNumero()),
             np.getDevise().getCode().trim()
             });
             }
             break;
             case DocumentConst.DocumentCode.BP:
             BonAPayer bap = PaiementBusiness.getBonAPayerByCode(
             detailsRole.getDocumentRef().trim());

             if (bap != null) {
             counter++;
             bulkDernierAvert.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
             bap.getFkNoteCalcul().getNumero().trim(),
             bap.getCode().trim(),
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE"),
             GeneralConst.Numeric.ZERO,
             GeneralConst.Numeric.ZERO,
             propertiesMessage.getProperty("TXT_GENERATION_DA_SUIVI_COMPTABLE"),
             propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")
             });
             }
             break;
             }

             }
             }*/
            result = executeQueryBulkInsert(bulkDernierAvert);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        try {
            return myDao.getDaoImpl().executeBulkQuery(params);
        } catch (Exception e) {
            throw e;
        }

    }

    public static ExtraitDeRole getExtraitRoleById(String idExtraitRole) {
        String sqlQuery = SQLQueryRecouvrement.ExtraitRole.SELECT_EXTRAIT_ROLE_BY_ID;
        List<ExtraitDeRole> extraitDeRoles = (List<ExtraitDeRole>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(ExtraitDeRole.class), false, idExtraitRole.trim());
        return extraitDeRoles.isEmpty() ? null : extraitDeRoles.get(0);
    }

    public static Role getRoleById(String idRole) {
        String sqlQuery = SQLQueryRecouvrement.Role.SELECT_ROLE_BY_ID;
        List<Role> roles = (List<Role>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(Role.class), false, idRole.trim());
        return roles.isEmpty() ? null : roles.get(0);
    }

    public static Amr getAMRByRetrait(Integer fkRetrait) {
        String query = "SELECT * FROM T_AMR WHERE FK_RETRAIT_DECLARATION = ?1 AND ETAT = 1";
        List<Amr> amrs = (List<Amr>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Amr.class), false, fkRetrait);
        return amrs.isEmpty() ? null : amrs.get(0);
    }

    public static PeriodeDeclaration getPeriodeDeclarationById(int periodeId) {
        String query = "SELECT * FROM T_PERIODE_DECLARATION WHERE ID = ?1 AND ETAT = 1";
        List<PeriodeDeclaration> declarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, periodeId);
        return declarations.isEmpty() ? null : declarations.get(0);
    }

    public static Med getMedRelanceByPerideDeclaration(int periodeID, String typMed) {
        String query = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.PERIODE_DECLARATION = (SELECT TOP(1) FK_PERIODE FROM T_RETRAIT_DECLARATION "
                + " WHERE FK_PERIODE = ?1) AND M.ETAT > 0 AND M.TYPE_MED = ?2";
        List<Med> meds = (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, periodeID, typMed);
        return meds.isEmpty() ? null : meds.get(0);
    }

    public static List<Med> getListMedAvance(String codeAB, String sqlQueryParam, String typMed) {
        String query = "SELECT M.* FROM T_MED M INNER JOIN T_PERIODE_DECLARATION PD ON M.PERIODE_DECLARATION = PD.ID WHERE M.ARTICLE_BUDGETAIRE = ?1 AND M.TYPE_MED = ?2 "
                + "AND M.ETAT = 1 AND PD.NOTE_CALCUL IS NULL AND (PD.DATE_LIMITE < GETDATE() OR PD.FIN < GETDATE()) %s";

        query = String.format(query, sqlQueryParam);

        return (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, codeAB, typMed);
    }

    public static RetraitDeclaration getRetraitDeclarationFilsByPeriode(String periodeId) {
        String query = "SELECT * FROM T_RETRAIT_DECLARATION WHERE FK_PERIODE = ?1 AND FK_RETRAIT_PERE IS NOT NULL"
                + " AND EST_PENALISE = 1 AND EST_TAXATION_OFFICE = 1 AND ETAT = 1";
        List<RetraitDeclaration> retraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periodeId);
        return retraitDeclarations.isEmpty() ? null : retraitDeclarations.get(0);
    }

    public static Boolean saveTaxationOffice(RetraitDeclaration retraitDeclaration, float penaliteInit, float penaliteCalculate,
            String numeroMed, String observation, String periodeDeclarationName, String impotName) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_2, new Object[]{
                retraitDeclaration.getRequerant(),
                retraitDeclaration.getFkAssujetti(),
                retraitDeclaration.getFkAb(),
                retraitDeclaration.getFkPeriode(),
                retraitDeclaration.getFkAgentCreate(),
                null,
                retraitDeclaration.getMontant(),
                retraitDeclaration.getDevise(),
                retraitDeclaration.getCodeDeclaration(),
                retraitDeclaration.getEstPenalise(),
                penaliteInit,
                retraitDeclaration.getFkBanque(),
                retraitDeclaration.getFkCompteBancaire(),
                penaliteCalculate,
                retraitDeclaration.getTauxRemise(),
                observation});

            counter++;

            String motif = "Absence déclaration".concat(GeneralConst.SPACE).concat(impotName).concat(GeneralConst.SPACE).concat(periodeDeclarationName.toUpperCase());

            bulkQuery.put(counter + ":EXEC F_NEW_AMR_RELANCE ?1,?2,?3,?4", new Object[]{
                numeroMed,
                retraitDeclaration.getFkAgentCreate(),
                motif, motif
            });

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Med> getListMedByPersonne(String assujettiCode, String typeMed) {
        String query = "SELECT M.* FROM T_MED M WHERE M.PERSONNE = ?1 AND M.TYPE_MED = ?2 AND ETAT = 1";
        return (List<Med>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(Med.class), false, assujettiCode, typeMed);
    }

    public static BanqueAb getBanqueByImpot(String codeImpot) {
        String query = "SELECT * FROM T_BANQUE_AB WITH (READPAST) WHERE FK_AB = ?1 AND ETAT = 1";
        List<BanqueAb> banqueAbs = (List<BanqueAb>) myDao.getDaoImpl().find(query,
                Casting.getInstance().convertIntoClassType(BanqueAb.class), false, codeImpot);
        return banqueAbs.isEmpty() ? null : banqueAbs.get(0);
    }

    public static List<NotePerceptionMairie> getListNotePerceptionNotMairiePaid(String assujettiCode) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        List<NotePerceptionMairie> npMairieList = null;

        if (assujettiCode.equals(GeneralConst.EMPTY_STRING)) {
            sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_MAIRIE_NOT_PAID;
            npMairieList = (List<NotePerceptionMairie>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(NotePerceptionMairie.class), false);
        } else {
            sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_MAIRIE_NOT_PAID_BY_ASSUJETTI;
            npMairieList = (List<NotePerceptionMairie>) myDao.getDaoImpl().find(sqlQueryMaster,
                    Casting.getInstance().convertIntoClassType(NotePerceptionMairie.class), false, assujettiCode.trim());
        }

        return npMairieList;
    }

    public static List<NotePerceptionMairie> getListNotePerceptionNotMairiePaidBySite(
            String codeService, String codeSite, String dateDebut, String dateFin) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlParamService = GeneralConst.EMPTY_STRING;

        if (dateDebut != null) {
            if (dateDebut.contains(GeneralConst.DASH_SEPARATOR)) {
                dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            }
        }

        if (dateFin != null) {
            if (dateFin.contains(GeneralConst.DASH_SEPARATOR)) {
                dateFin = ConvertDate.getValidFormatDate(dateFin);
            }
        }
        if (codeService != null && codeSite != null) {
            if (!codeService.equals("*") && !codeSite.equals("*")) {
//                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "' AND NC.SITE = '" + codeSite.trim() + "'";
                sqlParamService = " WHERE J.SITE = '" + codeSite.trim() + "'";
            } else if (!codeService.equals("*")) {
//                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            } else if (!codeSite.equals("*")) {
                sqlParamService = " WHERE J.SITE = '" + codeSite.trim() + "'";
            }
        } else if (codeService != null) {
            if (!codeService.equals("*")) {
//                sqlParamService = " WHERE NC.SERVICE = '" + codeService.trim() + "'";
            }
        } else if (!codeSite.equals("*")) {
            sqlParamService = " WHERE J.SITE = '" + codeSite.trim() + "'";
        }

        String sqlParamDate = " AND DBO.FDATE(NP.DATE_CREATE) BETWEEN '%s' AND '%s'";
        sqlParamDate = String.format(sqlParamDate, dateDebut, dateFin);

        sqlQueryMaster = SQLQueryRecouvrement.SELECT_LIST_NOTE_PERCEPTION_MAIRIE_NOT_PAID_BY_SITE;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamService, sqlParamDate);

        List<NotePerceptionMairie> notePerceptionMairies = (List<NotePerceptionMairie>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(NotePerceptionMairie.class), false,
                codeSite.trim(), codeService.trim());
        return notePerceptionMairies;
    }
    
    public static NotePerceptionMairie getNotePerceptionMairieByCode(String codeNpMairie) {
        try {

            String query = SQLQueryRecouvrement.SELECT_NOTE_PERCEPTION_MAIRIE_BY_CODE;

            List<NotePerceptionMairie> npM = (List<NotePerceptionMairie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(NotePerceptionMairie.class), false, codeNpMairie);
            return npM.isEmpty() ? null : npM.get(0);
        } catch (Exception e) {
            throw e;
        }
    }
}
