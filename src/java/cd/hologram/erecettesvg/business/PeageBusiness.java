/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.IdentificationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujettissement;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.DetailAssujettissement;
import cd.hologram.erecettesvg.models.DetailPrevisionCredit;
import cd.hologram.erecettesvg.models.DetailsPrevisionCredit;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.PersonneExemptee;
import cd.hologram.erecettesvg.models.PrevisionCredit;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.TarifSite;
import cd.hologram.erecettesvg.models.TicketPeage;
import cd.hologram.erecettesvg.servlets.Peage;
import cd.hologram.erecettesvg.sql.SQLQueryIdentification;
import cd.hologram.erecettesvg.sql.SQLQueryPeage;
import cd.hologram.erecettesvg.util.Casting;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author WILLY KASHALA
 */
public class PeageBusiness {

    private static final Dao myDao = new Dao();

    public static DetailAssujettissement getDetailAssujettissementByID(int id) {
        try {
            String query = "SELECT * FROM T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) WHERE da.ID = ?1";
            List<DetailAssujettissement> detailAssujettissements = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, id);
            return detailAssujettissements.isEmpty() ? null : detailAssujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static TarifSite getTarifSiteBySiteAndTarif(String codeSite, String codeTatif) {
        try {
            String query = "select * from T_TARIF_SITE WITH (READPAST) WHERE FK_SITE_PROVENANCE = ?1 AND FK_SITE_DESTINANTION = ?1 AND FK_TARIF = ?2 AND ETAT = 1";
            List<TarifSite> detailAssujettissements = (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, codeSite, codeTatif);
            return detailAssujettissements.isEmpty() ? null : detailAssujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getAssujettiByName(String libelle) {
        try {
            String query = SQLQueryPeage.SELECT_ASSUJETTI_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getAssujettiByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryPeage.SELECT_ASSUJETTI_BY_CODE
                    : SQLQueryPeage.SELECT_ASSUJETTI_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getArticleBudgetaireOfAssujetti(String codeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_ARTICLE_OF_ASSUJETTI;
            List<ArticleBudgetaire> articleList = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeAssujetti);
            return articleList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujettissement> getAssujettissementOfAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT a.* FROM T_ASSUJETTISSEMENT a WITH (READPAST) WHERE a.PERSONNE = ?1 AND a.ETAT = 1";
            List<Assujettissement> assujettissements = (List<Assujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujettissement.class), false, codeAssujetti);
            return assujettissements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailAssujettissementOfAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT da.* FROM T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) JOIN T_BIEN b WITH (READPAST) "
                    + " ON da.BIEN = b.ID WHERE da.ASSUJETTISSEMENT IN "
                    + " (SELECT a.CODE FROM T_ASSUJETTISSEMENT a WITH (READPAST) WHERE a.PERSONNE = ?1 AND a.ETAT = 1) "
                    + " AND da.FK_PERSONNE = ?1 AND da.ETAT IN (1,2) ORDER BY b.INTITULE";
            List<DetailAssujettissement> detailAssujettissements = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeAssujetti);
            return detailAssujettissements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PrevisionCredit> getPrepaiementListByAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT p.* FROM T_PREVISION_CREDIT p WITH (READPAST) WHERE p.FK_PERSONNE = ?1 AND p.ETAT > 0 "
                    + " AND p.CODE IN (SELECT dp.FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) WHERE dp.TYPE = 1)";
            List<PrevisionCredit> prepaiements = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, codeAssujetti);
            return prepaiements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PrevisionCredit> getCreditListByAssujetti(String codeAssujetti) {
        try {
            String query = "SELECT p.* FROM T_PREVISION_CREDIT p WITH (READPAST) WHERE p.FK_PERSONNE = ?1 AND p.ETAT > 0 "
                    + " AND p.CODE IN (SELECT dp.FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) WHERE dp.TYPE = 2)";
            List<PrevisionCredit> prepaiements = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, codeAssujetti);
            return prepaiements;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailsAssujettissementByArticle(String codeArticle, String codeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_DETAIL_ASSUJETTISSEMENT_BY_ARTICLE;
            List<DetailAssujettissement> dtlAssujettissement = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeArticle, codeAssujetti);
            return dtlAssujettissement;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailAssujettissement> getDetailsAssujettissementByPersonne(String codeAssujetti) {
        try {
            String query = "SELECT * from T_DETAIL_ASSUJETTISSEMENT da WITH (READPAST) WHERE da.FK_PERSONNE = ?1 AND da.ETAT = 1 "
                    + " AND da.ID NOT IN (SELECT dpc.FK_DETAIL_ASSUJETTISSEMENT FROM T_DETAILS_PREVISION_CREDIT dpc WITH (READPAST) WHERE dpc.ETAT = 1)";
            List<DetailAssujettissement> dtlAssujettissement = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, codeAssujetti);
            return dtlAssujettissement;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsPrevisionCredit> getDetailsPrePaiements(String codePrepaiement) {
        try {
            String query = "SELECT * from T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) "
                    + "WHERE dp.FK_PREVISION_CREDIT = ?1 AND dp.TYPE = 1 AND dp.ETAT > 0 ORDER BY dp.FK_BIEN";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, codePrepaiement);
            return detailsPrevisionCredits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsPrevisionCredit> getDetailsCredits(String codeCredit) {
        try {
            String query = "SELECT * from T_DETAILS_PREVISION_CREDIT dp WITH (READPAST) "
                    + "WHERE dp.FK_PREVISION_CREDIT = ?1 AND dp.TYPE = 2 AND dp.ETAT > 0 ORDER BY dp.FK_BIEN";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, codeCredit);
            return detailsPrevisionCredits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String savePrevisionCredit(
            List<DetailsPrevisionCredit> listDetailPrevisionCredit, PrevisionCredit previsionCredit) {

        HashMap<String, Object[]> firstBulk = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;
        HashMap<String, Object> paramsNC = new HashMap<>();

        String firstStoredProcedure = "F_NEW_PREVISION_CREDIT";
        String firstStoredReturnValueKey = "ID";

        paramsNC.put("FK_PERSONNE", previsionCredit.getFkPersonne().trim());
        paramsNC.put("AGENT_CREATE", previsionCredit.getAgentCreate());

        for (DetailsPrevisionCredit detailsPrevisionCredit : listDetailPrevisionCredit) {

            counter++;

            firstBulk.put(counter + SQLQueryPeage.SAVE_DETAILS_PREVISION_CREDIT_V2, new Object[]{
                detailsPrevisionCredit.getFkBien(),
                detailsPrevisionCredit.getTaux(),
                detailsPrevisionCredit.getType(),
                detailsPrevisionCredit.getNbreTourSouscrit(),
                detailsPrevisionCredit.getTotalTaux(),
                detailsPrevisionCredit.getFkDevise(),
                detailsPrevisionCredit.getFkDetailAssujettissement()
            });

        }

        String result = myDao.getDaoImpl().execBulkQueryv4(
                firstStoredProcedure, firstStoredReturnValueKey, paramsNC, firstBulk);

        return result;
    }

    public static Assujettissement getAssujettissementByArticleAndAssujetti(String codeArticle, String CodeAssujetti) {
        try {
            String query = SQLQueryPeage.SELECT_ASSUJETTISSEMENT_BY_ARTICLE_AND_ASSUJETTI;

            List<Assujettissement> assujettissement = (List<Assujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujettissement.class), false, codeArticle, CodeAssujetti);
            return assujettissement.isEmpty() ? null : assujettissement.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getPersonnesByName(String libelle) {
        try {
            String query = SQLQueryPeage.SELECT_PERSONNES_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryPeage.SELECT_PERSONNE_BY_CODE
                    : SQLQueryPeage.SELECT_PERSONNE_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean exempteAssujettis(PersonneExemptee personneExempte) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            bulkQuery.put(counter + SQLQueryPeage.EXEMPTE_ASSUJETTI, new Object[]{
                personneExempte.getPersonne().getCode().trim(),
                personneExempte.getAgentCreat(),
                personneExempte.getDateCreate(),
                personneExempte.getEtat()
            });

            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static PersonneExemptee checkPersonneExemptes(String codePersonne) {
        try {
            String query = SQLQueryPeage.SELECT_PERSONNE_EXEMPTE;

            List<PersonneExemptee> personnes = (List<PersonneExemptee>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PersonneExemptee.class), false, codePersonne);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disabledPersonneExemptes(String codePersonne) {

        boolean result = false;
        int idCodePer = Integer.valueOf(codePersonne);
        try {
            String query = SQLQueryPeage.DISABLED_PERSONNE_EXEMPTE;

            result = myDao.getDaoImpl().executeStoredProcedure(query, idCodePer);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketPeages(String codeTicket, String codeSite, int percepteur) {

        try {

            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1 AND SITE = ?2 %s";

            String sqlParam = GeneralConst.EMPTY_STRING;

            if (percepteur > 0) {
                sqlParam = " AND AGENT_CREAT = %s";
                sqlParam = String.format(sqlParam, percepteur);
            }

            query = String.format(query, sqlParam);

            return (List<TicketPeage>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(TicketPeage.class),
                    false, codeTicket.trim(), codeSite.trim());

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketCanceledPeages(String codeTicket, String codeSite, int percepteur) {

        try {

            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 2 AND SITE = ?2 %s";

            String sqlParam = GeneralConst.EMPTY_STRING;

            if (percepteur > 0) {
                sqlParam = " AND AGENT_CREAT = %s";
                sqlParam = String.format(sqlParam, percepteur);
            }

            query = String.format(query, sqlParam);

            return (List<TicketPeage>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(TicketPeage.class),
                    false, codeTicket.trim(), codeSite.trim());

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketPeages(String siteCode, String typePaiement, String dateDebut, String dateFin,
            String tarif, String percepteur) {

        try {

            String query = "SELECT t.* FROM T_TICKET_PEAGE t WITH (READPAST) WHERE t.ETAT = 1 %s %s %s %s %s ORDER BY t.CODE";

            String sqlParam_1 = GeneralConst.EMPTY_STRING;
            String sqlParam_2 = GeneralConst.EMPTY_STRING;
            String sqlParam_3 = GeneralConst.EMPTY_STRING;
            String sqlParam_4 = GeneralConst.EMPTY_STRING;
            String sqlParam_5 = GeneralConst.EMPTY_STRING;

            if (!siteCode.equals("*")) {
                sqlParam_1 = " AND t.SITE = '%s'";
                sqlParam_1 = String.format(sqlParam_1, siteCode.trim());
            }

            if (!typePaiement.equals("*")) {
                sqlParam_2 = " AND t.TYPE_PAIEMENT = '%s'";
                sqlParam_2 = String.format(sqlParam_2, typePaiement.trim());
            }

            if (!tarif.equals("*")) {
                sqlParam_3 = " AND t.FK_TARIF = '%s'";
                sqlParam_3 = String.format(sqlParam_3, tarif.trim());
            }

            if (!percepteur.equals("*")) {
                sqlParam_4 = " AND t.AGENT_CREAT = %s";
                sqlParam_4 = String.format(sqlParam_4, Integer.valueOf(percepteur.trim()));
            }

            sqlParam_5 = " AND DBO.FDATE(DATE_PROD) BETWEEN '%s' AND '%s'";
            sqlParam_5 = String.format(sqlParam_5, dateDebut, dateFin);

            query = String.format(query, sqlParam_1, sqlParam_2, sqlParam_3, sqlParam_4, sqlParam_5);

            List<TicketPeage> ticketPeageList = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false);

            return ticketPeageList;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TicketPeage> getTicketCanceledPeages(String siteCode, String typePaiement, String dateDebut, String dateFin,
            String tarif, String percepteur) {

        try {

            String query = "SELECT t.* FROM T_TICKET_PEAGE t WITH (READPAST) WHERE t.ETAT = 2 %s %s %s %s %s ORDER BY t.CODE";

            String sqlParam_1 = GeneralConst.EMPTY_STRING;
            String sqlParam_2 = GeneralConst.EMPTY_STRING;
            String sqlParam_3 = GeneralConst.EMPTY_STRING;
            String sqlParam_4 = GeneralConst.EMPTY_STRING;
            String sqlParam_5 = GeneralConst.EMPTY_STRING;

            if (!siteCode.equals("*")) {
                sqlParam_1 = " AND t.SITE = '%s'";
                sqlParam_1 = String.format(sqlParam_1, siteCode.trim());
            }

            if (!typePaiement.equals("*")) {
                sqlParam_2 = " AND t.TYPE_PAIEMENT = '%s'";
                sqlParam_2 = String.format(sqlParam_2, typePaiement.trim());
            }

            if (!tarif.equals("*")) {
                sqlParam_3 = " AND t.FK_TARIF = '%s'";
                sqlParam_3 = String.format(sqlParam_3, tarif.trim());
            }

            if (!percepteur.equals("*")) {
                sqlParam_4 = " AND t.AGENT_CREAT = %s";
                sqlParam_4 = String.format(sqlParam_4, Integer.valueOf(percepteur.trim()));
            }

            sqlParam_5 = " AND DBO.FDATE(DATE_PROD) BETWEEN '%s' AND '%s'";
            sqlParam_5 = String.format(sqlParam_5, dateDebut, dateFin);

            query = String.format(query, sqlParam_1, sqlParam_2, sqlParam_3, sqlParam_4, sqlParam_5);

            List<TicketPeage> ticketPeageList = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false);

            return ticketPeageList;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBienOfImm(String idBien, String tcImm) {
        try {
            String query = "SELECT * FROM T_COMPLEMENT_BIEN WHERE BIEN = ?1 AND TYPE_COMPLEMENT IN "
                    + "(SELECT CODE FROM T_TYPE_COMPLEMENT_BIEN WHERE COMPLEMENT = ?2 AND ETAT = 1) AND ETAT = 1";

            List<ComplementBien> complementBienList = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, idBien, tcImm);
            return complementBienList.isEmpty() ? null : complementBienList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByBienAndAssuj(String idBien, String ab, String codePers) {
        try {
            String query = "SELECT * FROM T_DETAIL_ASSUJETTISSEMENT WHERE BIEN = ?1 AND ASSUJETTISSEMENT = "
                    + "(SELECT TOP(1) CODE FROM T_ASSUJETTISSEMENT WHERE ARTICLE_BUDGETAIRE = ?2 AND PERSONNE = ?3 AND ETAT = 1) AND ETAT = 1";

            List<DetailAssujettissement> complementBienList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, idBien, ab, codePers);
            return complementBienList.isEmpty() ? null : complementBienList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBienByBienAndPlaque(String plaque, String bien) {
        try {
            String query = "SELECT cb.* from T_COMPLEMENT_BIEN cb WITH (READPAST) "
                    + " WHERE cb.TYPE_COMPLEMENT IN (SELECT  tcb.COMPLEMENT FROM T_TYPE_COMPLEMENT_BIEN tcb WITH (READPAST) "
                    + " WHERE tcb.COMPLEMENT = ?1) "
                    + " AND cb.BIEN = ?2";
            List<ComplementBien> complementBiens = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, plaque, bien);
            return complementBiens.isEmpty() ? null : complementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean editPrepaiement(String code, int userId, String state) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            switch (state) {
                case "1":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 3, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });

                    bulkQuery.put(1 + ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 3, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE FK_PREVISION_CREDIT = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
                case "2":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
                case "3":
                    bulkQuery.put(0 + ":UPDATE T_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2 ", new Object[]{
                        userId,
                        code.trim()
                    });
                    bulkQuery.put(1 + ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 1, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE FK_PREVISION_CREDIT = ?2 AND ETAT = 1", new Object[]{
                        userId,
                        code.trim()
                    });
                    break;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveRenouvellementSouProvision(int userId, int numberTour,
            BigDecimal inputTotalAPayer, int type, String devsie, BigDecimal taux, String bien,
            int detailAssujId, String prePaiementCode) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":INSERT INTO T_DETAILS_PREVISION_CREDIT (FK_PREVISION_CREDIT,FK_BIEN,TAUX,TYPE,NBRE_TOUR_SOUSCRIT,TOTAL_TAUX,"
                    + "FK_DEVISE,FK_DETAIL_ASSUJETTISSEMENT,AGENT_CREATE,ETAT) VALUES (?1,?2,?3,?4,?5,?6,?7,?8,?9,2)";

            bulkQuery.put(0 + query, new Object[]{
                prePaiementCode, bien,
                taux, type, numberTour, inputTotalAPayer,
                devsie, detailAssujId, userId

            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateDP(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 1,AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean suspendreBienPrevisionCredit(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET ETAT = 3,AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateExemption(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 2,AGENT_CREATE_EXEMPTION = ?1, DATE_CREATE_EXEMPTION = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateAllExemption(int userId, List<String> listId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String id : listId) {

                query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 2,AGENT_CREATE_EXEMPTION = ?1, DATE_CREATE_EXEMPTION = GETDATE() WHERE ID = ?2";

                bulkQuery.put(count + query, new Object[]{
                    userId, Integer.valueOf(id)
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean canceledTicket(int userId, List<String> listTicket, String observation, String codeTicket) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String ticket : listTicket) {

                query = ":UPDATE T_TICKET_PEAGE SET ETAT = 2,DATE_MAJ = GETDATE(), AGENT_MAJ = ?1, OBSERVATION = ?2,TICKET_DOCUMENT = ?3 WHERE CODE = ?4";

                bulkQuery.put(count + query, new Object[]{
                    userId, observation, ticket, codeTicket
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desactivateAllExemption(int userId, List<String> listId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;
        int count = 0;

        try {

            String query = GeneralConst.EMPTY_STRING;

            for (String id : listId) {

                query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 1,AGENT_MAJ_EXEMPTION = ?1, DATE_MAJ_EXEMPTION = GETDATE() WHERE ID = ?2";

                bulkQuery.put(count + query, new Object[]{
                    userId, Integer.valueOf(id)
                });

                count++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desactivateExemption(int userId, int id) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAIL_ASSUJETTISSEMENT SET ETAT = 1,AGENT_MAJ_EXEMPTION = ?1, DATE_MAJ_EXEMPTION = GETDATE() WHERE ID = ?2";

            bulkQuery.put(0 + query, new Object[]{
                userId, id
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean remplacerBienPrevisionCredit(int userId, String codePersonne, String bien) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        Boolean result = false;

        try {

            String query = ":UPDATE T_DETAILS_PREVISION_CREDIT SET AGENT_MAJ = ?1, FK_BIEN = ?2, DATE_MAJ = GETDATE() "
                    + " WHERE FK_PREVISION_CREDIT IN (SELECT CODE FROM T_PREVISION_CREDIT WITH (READPAST) WHERE FK_PERSONNE = ?3) AND ETAT = 1";

            bulkQuery.put(0 + query, new Object[]{
                userId, bien, codePersonne
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Bien> getListBiensRemplacementByAssujetti(String codeAssujtti, String codeBien, String codeTarif) {

        try {

            String query = "SELECT B.* FROM T_BIEN B WITH (READPAST) "
                    + " WHERE B.ID IN (SELECT DA.BIEN FROM T_DETAIL_ASSUJETTISSEMENT DA WITH (READPAST) "
                    + " WHERE DA.FK_PERSONNE = ?1 AND DA.BIEN <> ?2 AND DA.TARIF = ?3 AND DA.ETAT = 1) "
                    + " AND B.ETAT = 1 ORDER BY B.INTITULE";
            return (List<Bien>) myDao.getDaoImpl().find(query, Casting.getInstance().convertIntoClassType(Bien.class), false,
                    codeAssujtti.trim(), codeBien.trim(), codeTarif.trim());

        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailsPrevisionCredit getDetailsPrevisionCreditById(String id) {
        try {
            String query = "SELECT * FROM T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE ID = ?1";

            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, id);
            return detailsPrevisionCredits.isEmpty() ? null : detailsPrevisionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static PrevisionCredit getPrevisionCreditByDetailId(String detailId) {
        try {
            String query = "SELECT * FROM T_PREVISION_CREDIT WITH (READPAST) "
                    + " WHERE CODE IN (SELECT FK_PREVISION_CREDIT FROM T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE ID = ?1)";

            List<PrevisionCredit> proCredits = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, detailId);
            return proCredits.isEmpty() ? null : proCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Site> getAllSitePeage() {
        try {
            String query = "SELECT s.* FROM T_SITE s WITH (READPAST) WHERE s.PEAGE = 1 AND s.ETAT = 1 ORDER BY s.INTITULE";
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static TicketPeage getTicketPeageByCode(String code) {
        try {
            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE = ?1";

            List<TicketPeage> ticketPeages = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false, code);
            return ticketPeages.isEmpty() ? null : ticketPeages.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

}
