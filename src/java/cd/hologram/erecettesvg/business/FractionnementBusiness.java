/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

//import cd.hologram.erecettesvg.constants.EchelonnementConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import static cd.hologram.erecettesvg.constants.TaxationOfficeConst.ParamName.codeService;
import static cd.hologram.erecettesvg.constants.TaxationOfficeConst.ParamName.codeSite;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.DemandeEchelonnement;
//import cd.hologram.erecettesvg.models.DemandeEchelonnement;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.sql.SQLQueryFractionnement;
import cd.hologram.erecettesvg.sql.SQLQueryMed;
//import cd.hologram.erecettesvg.sql.SQLQueryEchelonnement;
//import cd.hologram.erecettesvg.sql.SQLQueryMed;
import cd.hologram.erecettesvg.util.Casting;
import cd.hologram.erecettesvg.util.ConvertDate;
import java.util.HashMap;
import java.util.List;
//import cd.hologram.erecettesvg.business.AdministrationBusiness;

/**
 *
 * @author moussa.toure
 */
public class FractionnementBusiness {

    private static final Dao myDao = new Dao();

//    public static boolean ProrogerEcheance(JSONArray jsonNpFilleProrogationArray,
//            String typeTitre,
//            List<String> documentList, String fkDocument,
//            String observation,
//            String codeEchelonnement) {
//
//        HashMap<String, Object[]> bulkQuery = new HashMap<>();
//        int counter = GeneralConst.Numeric.ZERO;
//
//        for (int i = 0; i < jsonNpFilleProrogationArray.length(); i++) {
//
//            try {
//
//                counter++;
//                JSONObject jsonObjectNp = jsonNpFilleProrogationArray.getJSONObject(i);
//
//                if (typeTitre.equals(DocumentConst.DocumentCode.NP) || typeTitre.equals(DocumentConst.DocumentCode.NP_P)) {
//
//                    bulkQuery.put(counter + SQLQueryEchelonnement.UPDATE_ECHEANCE_PROROGATION_NP, new Object[]{
//                        jsonObjectNp.getString(EchelonnementConst.ParamName.EcheanceNpFilleProrogee),
//                        jsonObjectNp.getString(EchelonnementConst.ParamName.NumeroNpFille)
//                    });
//
//                } else {
//
//                    bulkQuery.put(counter + SQLQueryEchelonnement.UPDATE_ECHEANCE_PROROGATION_BP, new Object[]{
//                        jsonObjectNp.getString(EchelonnementConst.ParamName.EcheanceNpFilleProrogee),
//                        jsonObjectNp.getString(EchelonnementConst.ParamName.NumeroNpFille)
//                    });
//                }
//
//            } catch (JSONException e) {
//                Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, e);
//            }
//
//        }
//
//        for (String document : documentList) {
//            counter++;
//            bulkQuery.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION,
//                    new Object[]{
//                        codeEchelonnement,
//                        typeTitre,
//                        document,
//                        fkDocument, observation
//                    });
//        }
//
//        try {
//            return myDao.getDaoImpl().executeBulkQuery(bulkQuery);
//        } catch (Exception e) {
//            throw e;
//        }
//
//    }
//    public static String saveDemandeEchelonnement(
//            DemandeEchelonnement demandeEchelonnement, JSONArray detailEchelonnement, LogUser logUser,
//            List<String> documentList, String fkDocument, String observationDoc) {
//
//        HashMap<String, Object[]> bulkQuery = new HashMap<>();
//        int counter = GeneralConst.Numeric.ZERO;
//
//        HashMap<String, Object> paramsStored = new HashMap<>();
//
//        String storedProcedure = "F_NEW_DEMANDE_ECHELONNEMENT";
//        String storedReturnValueKey = "CODE";
//
//        paramsStored.put("REFERENCE_LETTRE", demandeEchelonnement.getReferenceLettre());
//        paramsStored.put("OBSERVATION", demandeEchelonnement.getObservation());
//        paramsStored.put("AGENT_CREAT", demandeEchelonnement.getAgentCreat());
//        paramsStored.put("MAC_ADRESSE", logUser.getMacAddress().trim());
//        paramsStored.put("IP_ADRESSE", logUser.getIpAddress().trim());
//        paramsStored.put("HOST_NAME", logUser.getHostName().trim());
//        paramsStored.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());
//        paramsStored.put("FK_EVENEMENT", logUser.getEvent());
//
//        PropertiesMessage propertiesMessage = null;
//
//        try {
//            propertiesMessage = new PropertiesMessage();
//        } catch (IOException ex) {
//            Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        for (int i = 0; i < detailEchelonnement.length(); i++) {
//
//            try {
//
//                JSONObject jsonObject = detailEchelonnement.getJSONObject(i);
//
//                String typDoc = jsonObject.getString(NotePerceptionConst.ParamName.TYPE_DOCUMENT);
//                String refDoc = jsonObject.getString(ContentieuxConst.ParamName.REFERENCE_DOCUMENT);
//
//                counter++;
//                bulkQuery.put(counter + SQLQueryEchelonnement.SAVE_DETAIL_ECHELONNEMENT, new Object[]{
//                    refDoc,
//                    typDoc});
//
//                if (!typDoc.equals(DocumentConst.DocumentCode.BP)) {
//
//                    NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(refDoc);
//
//                    if (np != null) {
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                            np.getNoteCalcul().getNumero().trim(),
//                            np.getNumero().trim(),
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT),
//                            GeneralConst.Numeric.ZERO,
//                            GeneralConst.Numeric.ZERO,
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT) + GeneralConst.TWO_POINTS + np.getNumero().trim(),
//                            np.getDevise().getCode()
//                        });
//                    }
//
//                } else {
//
//                    BonAPayer bonAPayer = PaiementBusiness.getBonAPayerByBpManuel(refDoc.trim());
//
//                    if (bonAPayer != null) {
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                            bonAPayer.getFkNoteCalcul().getNumero().trim(),
//                            bonAPayer.getCode().trim(),
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT),
//                            GeneralConst.Numeric.ZERO,
//                            GeneralConst.Numeric.ZERO,
//                            propertiesMessage.getContent(PropertiesConst.Message.DEMANDE_ECHELONNEMENT) + GeneralConst.TWO_POINTS + bonAPayer.getCode(),
//                            bonAPayer.getDevise()
//                        });
//                    }
//
//                }
//
//            } catch (JSONException ex) {
//                Logger.getLogger(FractionnementBusiness.class.getName()).log(Level.SEVERE, null, ex);
//            }
//
//        }
//
//        if (documentList.size() > 0) {
//            for (String docu : documentList) {
//                counter++;
//                bulkQuery.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION_v2,
//                        new Object[]{
//                            DocumentConst.DocumentCode.NP,
//                            docu, fkDocument, observationDoc
//                        });
//            }
//        }
//
//        String result = GeneralConst.EMPTY_STRING;
//
//        try {
//            result = myDao.getDaoImpl().execBulkQueryv4(storedProcedure, storedReturnValueKey, paramsStored, bulkQuery);
//        } catch (Exception e) {
//            throw e;
//        }
//
//        return result;
//    }
//    public static boolean validerDemandeEchelonnement(
//            DemandeEchelonnement demandeEchelonnement,
//            LogUser logUser) {
//        String query = SQLQueryEchelonnement.VALIDER_DEMANDE_ECHELONNEMENT;
//        boolean result = myDao.getDaoImpl().executeStoredProcedure(
//                query,
//                demandeEchelonnement.getCode().trim(),
//                demandeEchelonnement.getEtat(),
//                demandeEchelonnement.getObservationValidation(),
//                demandeEchelonnement.getAgentValidation(),
//                logUser.getMacAddress(),
//                logUser.getIpAddress(),
//                logUser.getHostName().trim(),
//                logUser.getBrowserContext());
//        return result;
//    }
    public static List<DemandeEchelonnement> getListDemandeFractionnements(
            String typeSearch,
            String valueSearch) {

        String sqlQueryMaster = GeneralConst.EMPTY_STRING;
        String sqlParamFilter = GeneralConst.EMPTY_STRING;

        switch (typeSearch) {
            case GeneralConst.Number.ONE:

                sqlParamFilter = " AND E.PERSONNE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);

                break;

            case GeneralConst.Number.TWO:

                sqlParamFilter = " AND E.CODE IN (SELECT de.FK_DEMANDE_ECHELONNEMENT FROM "
                        + " t_detail_echelonnement de WITH (READPAST) WHERE de.NUMERO_TITRE = '%s')";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);

                break;

            case GeneralConst.Number.THREE:

                sqlParamFilter = " AND E.REFERENCE_LETTRE = '%s'";
                sqlParamFilter = String.format(sqlParamFilter, valueSearch);
                break;
        }

        sqlQueryMaster = "SELECT E.* FROM T_DEMANDE_ECHELONNEMENT E WITH (READPAST) WHERE E.ETAT > 0 %s";
        sqlQueryMaster = String.format(sqlQueryMaster, sqlParamFilter);

        List<DemandeEchelonnement> listDemandeEchelonnements = (List<DemandeEchelonnement>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(DemandeEchelonnement.class), false);

        return listDemandeEchelonnements;
    }

    public static List<DemandeEchelonnement> getListDemandeFractionnementsAdvanced(
            String service, String site, String dateDebut, String dateFin) {

        dateDebut = ConvertDate.getValidFormatDate(dateDebut);
        dateFin = ConvertDate.getValidFormatDate(dateFin);

        String sqlParamService = GeneralConst.EMPTY_STRING;
        String sqlParamSite = GeneralConst.EMPTY_STRING;
        String sqlMaster = GeneralConst.EMPTY_STRING;

        if (service != null) {
            if (!service.equals(GeneralConst.ALL)) {

                sqlParamService = " AND DE.CODE IN (SELECT DE.FK_DEMANDE_ECHELONNEMENT FROM t_detail_echelonnement "
                        + " DE WITH (READPAST) WHERE DE.SERVICE = '%s')";
                sqlParamService = String.format(sqlParamService, service);
            }
        }

        if (site != null) {
            if (!site.equals(GeneralConst.ALL)) {
                sqlParamSite = " AND DE.CODE IN (SELECT DE.FK_DEMANDE_ECHELONNEMENT FROM t_detail_echelonnement "
                        + " DE WITH (READPAST) WHERE DE.SITE = '%s')";
                sqlParamSite = String.format(sqlParamSite, site);
            }
        }

        sqlMaster = "SELECT E.* FROM T_DEMANDE_ECHELONNEMENT E WITH (READPAST) "
                + " WHERE DBO.FDATE(E.DATE_CREAT) BETWEEN '" + dateDebut + "' AND '" + dateFin + "' AND E.ETAT > 0 %s %s ORDER BY E.DATE_CREAT DESC";
        sqlMaster = String.format(sqlMaster, sqlParamService, sqlParamSite);

        try {
            List<DemandeEchelonnement> listDemandeEchelonnements = (List<DemandeEchelonnement>) myDao.getDaoImpl().find(sqlMaster,
                    Casting.getInstance().convertIntoClassType(DemandeEchelonnement.class), false);
            return listDemandeEchelonnements;
        } catch (Exception e) {
            throw e;
        }
    }

    public static DemandeEchelonnement getDemandeEchelonnementBytitre(String titre) {
        String sqlQuery = SQLQueryFractionnement.SELECT_DEMANDE_ECHELONNEMENT_BY_TITRE;
        try {
            List<DemandeEchelonnement> demandeEchelonnements = (List<DemandeEchelonnement>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(DemandeEchelonnement.class), false,
                    titre);
            return demandeEchelonnements.isEmpty() ? null : demandeEchelonnements.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

//    public static String revocation(NewNoteCalcul newNoteCalcul,
//            HashMap<String, Object[]> bulkQuery,
//            HashMap<String, Object[]> secondBulk,
//            LogUser logUser,
//            BigDecimal interetMoratoire,
//            BigDecimal montantDu,
//            String devise) {
//
//        HashMap<String, Object> params = new HashMap<>();
//        HashMap<String, Object> paramsFPC = new HashMap<>();
//
//        String returnValue = "NC_RETURN";
//
//        String firstStoredProcedure = "F_NEW_NOTE_CALCUL";
//        String secondStoredProcedure = "F_NEW_FPC";
//
//        String secondStoredReturnValueKey = "CODE_FPC";
//        String secondParamskey = "NUMERO_REFERENCE";
//
//        params.put("PERSONNE", newNoteCalcul.getCodePersonne().trim());
//        params.put("ADRESSE", newNoteCalcul.getCodeAdresse().trim());
//        params.put("EXERCICE", newNoteCalcul.getExerciceFiscal().trim());
//        params.put("SITE", newNoteCalcul.getCodeSite().trim());
//        params.put("SERVICE", newNoteCalcul.getCodeService().trim());
//        params.put("AGENT_CREAT", newNoteCalcul.getAgentCreate());
//        params.put("DATE_CREAT", newNoteCalcul.getDateCreate().trim());
//        params.put("EST_TAXATION_OFFICE", newNoteCalcul.isIsTaxationOffice());
//        params.put("DEPOT_DECLARATION", newNoteCalcul.getCodeDepot());
//        params.put("EST_TAXATION_REGULARISATION", newNoteCalcul.isIsTaxationRegularisation());
//        params.put("EST_TAXATION_OFFICE", newNoteCalcul.isIsTaxationOffice());
//        params.put("MAC_ADRESSE", logUser.getMacAddress().trim());
//        params.put("IP_ADRESSE", logUser.getIpAddress().trim());
//        params.put("HOST_NAME", logUser.getHostName().trim());
//        params.put("FK_EVENEMENT", Integer.valueOf(TaxationConst.Operation.SAVE_NOTE_CALCUL));
//        params.put("CONTEXT_BROWSER", logUser.getBrowserContext().trim());
//        params.put("FK_NC_SOURCE", GeneralConst.EMPTY_STRING);
//
//        if (secondBulk.size() > 0) {
//
//            FichePriseCharge fichePriseCharge = new FichePriseCharge();
//            fichePriseCharge.setTotalPenalitedu(interetMoratoire);
//            fichePriseCharge.setTotalPrincipaldu(montantDu);
//            fichePriseCharge.setPersonne(new Personne(newNoteCalcul.getCodePersonne()));
//            fichePriseCharge.setDetailMotif("Interêt moratoire");
//            fichePriseCharge.setDevise(new Devise(devise));
//
//            paramsFPC.put("TOTAL_PRINCIPAL_DU", fichePriseCharge.getTotalPrincipaldu().floatValue());
//            paramsFPC.put("TOTAL_PENALITE_DU ", fichePriseCharge.getTotalPenalitedu().floatValue());
//            paramsFPC.put("PERSONNE", fichePriseCharge.getPersonne().getCode().trim());
//            paramsFPC.put("DEATAILS_MOTIF", fichePriseCharge.getDetailMotif());
//            paramsFPC.put("DEVISE", fichePriseCharge.getDevise().getCode().trim());
//
//        }
//
//        String result;
//
//        try {
//
//            if (secondBulk.size() > 0) {
//
//                result = myDao.getDaoImpl().execBulkQueryv5(
//                        params,
//                        paramsFPC,
//                        bulkQuery,
//                        secondBulk,
//                        firstStoredProcedure,
//                        secondStoredProcedure,
//                        returnValue,
//                        secondStoredReturnValueKey,
//                        secondParamskey);
//
//            } else {
//
//                result = myDao.getDaoImpl().execBulkQueryv4(firstStoredProcedure, returnValue,
//                        params, bulkQuery);
//            }
//
//        } catch (Exception e) {
//            throw e;
//        }
//
//        return result;
//    }
//    public static List<ArchiveAccuseReception> getListArchiveByReference(String reference) {
//        String sqlQuery = SQLQueryEchelonnement.SELECT_LIST_ARCHIVE_BY_REFERENCE;
//        List<ArchiveAccuseReception> listArchiveAccuseReception = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class), false, reference.trim());
//        return listArchiveAccuseReception;
//    }
//    public static BonAPayer getBonAPayerFromFractionByNoteCalcul(String noteCalcul) {
//
//        String sqlQuery = SQLQueryEchelonnement.SELECT_BON_A_PAYER_FROM_FRACTION_BY_NOTE_CALCUL;
//
//        List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(BonAPayer.class), false,
//                noteCalcul.trim());
//        return bonAPayers.isEmpty() ? null : bonAPayers.get(0);
//    }
//    public static BonAPayer getBonAPayerFromFractionByNoteCalculNotPaid(String noteCalcul) {
//
//        String sqlQuery = SQLQueryEchelonnement.SELECT_BON_A_PAYER_FROM_FRACTION_BY_NOTE_CALCUL_NOT_PAYED;
//
//        List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(BonAPayer.class), false,
//                noteCalcul.trim());
//        return bonAPayers.isEmpty() ? null : bonAPayers.get(0);
//    }
//    public static BonAPayer getBonAPayerInteretByNP(String npMere) {
//
//        String sqlQuery = SQLQueryEchelonnement.SELECT_BON_A_PAYER_INTERET_BY_NP_NOT_PAYED;
//
//        List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(BonAPayer.class), false,
//                npMere.trim());
//        return bonAPayers.isEmpty() ? null : bonAPayers.get(0);
//    }
//    public static BonAPayer getBonAPayerInteretByBP(String BMere) {
//
//        String sqlQuery = SQLQueryEchelonnement.SELECT_BON_A_PAYER_INTERET_BY_BP_NOT_PAYED;
//
//        List<BonAPayer> bonAPayers = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(BonAPayer.class), false,
//                BMere.trim());
//        return bonAPayers.isEmpty() ? null : bonAPayers.get(0);
//    }
//    public static List<BonAPayer> getListBonAPayerByNpMere(String npMere) {
//        String sqlQuery = SQLQueryMed.SELECT_BON_A_PAYER_BY_NP_MERE;
//        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
//                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, npMere.trim());
//        return listBonAPayer;
//    }
//
    public static boolean validerDemandeEchelonnement(DemandeEchelonnement demandeEchelonnement, LogUser logUser) {
        String query = SQLQueryFractionnement.VALIDER_DEMANDE_ECHELONNEMENT;
        boolean result = myDao.getDaoImpl().executeStoredProcedure(
                query,
                demandeEchelonnement.getCode().trim(),
                demandeEchelonnement.getEtat(),
                demandeEchelonnement.getObservationValidation(),
                demandeEchelonnement.getAgentValidation(),
                logUser.getMacAddress(),
                logUser.getIpAddress(),
                logUser.getHostName().trim(),
                logUser.getBrowserContext());
        return result;
    }

    public static List<BonAPayer> getListBonAPayerByNpMere(String npMere) {
        String sqlQuery = SQLQueryMed.SELECT_BON_A_PAYER_BY_NP_MERE;
        List<BonAPayer> listBonAPayer = (List<BonAPayer>) myDao.getDaoImpl().find(sqlQuery,
                Casting.getInstance().convertIntoClassType(BonAPayer.class), false, npMere.trim());
        return listBonAPayer;
    }
}
