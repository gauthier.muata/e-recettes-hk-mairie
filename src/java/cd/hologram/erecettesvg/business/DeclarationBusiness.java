/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.ContentieuxBusiness.propertiesMessage;
import cd.hologram.erecettesvg.constants.DeclarationConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.ArchiveAccuseReception;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.DetailBordereau;
import cd.hologram.erecettesvg.models.DetailDepotDeclaration;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.ParamTauxVignette;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.TypeDocument;
import cd.hologram.erecettesvg.models.ValeurPredefinie;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.NoteCalculData;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import cd.hologram.erecettesvg.sql.SQLQueryDeclaration;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.util.Casting;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author WILLY KASHALA Tel : 00243 81 27 20 560
 */
public class DeclarationBusiness {

    private static final Dao myDao = new Dao();

    public static Bordereau getBordereau(String numero) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_BORDEREAU_BY_DECLARATION;

            List<Bordereau> bordereau = (List<Bordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bordereau.class), false, numero);
            return bordereau.size() > 0 ? bordereau.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static ArticleBudgetaire getArticleBudgetaireByCode(String codeAB) {

        try {
            String query = SQLQueryDeclaration.SELECT_ARTICLE_BUDGETAIRE_BY_CODE;

            List<ArticleBudgetaire> articleBudg = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false, codeAB);
            return articleBudg.size() > 0 ? articleBudg.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Personne getPersonneByCode(String code) {

        try {
            String query = SQLQueryDeclaration.SELECT_PERSONNE_BY_CODE;

            List<Personne> person = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, code);
            return person.size() > 0 ? person.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraiteclarationByNumero(String numero) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_RETRAIT_DECLARATION_BY_NUMERO;

            List<RetraitDeclaration> retrDcl = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, numero);
            return retrDcl.size() > 0 ? retrDcl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraiteclarationByID(int id) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_RETRAIT_DECLARATION_BY_ID;

            List<RetraitDeclaration> retrDcl = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);
            return retrDcl.size() > 0 ? retrDcl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraiteclarationByNumeroDeclaration(String numero) {
        String query = null;
        try {

            query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE CODE_DECLARATION = ?1 AND ETAT = 1";

            List<RetraitDeclaration> retrDcl = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, numero);
            return retrDcl.size() > 0 ? retrDcl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static DepotDeclaration getDepotDeclarationByCode(String code) {

        try {
            String query = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_CODE;

            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, code);
            return depotDecl.size() > 0 ? depotDecl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean saveDepotDeclaration(
            LogUser logUser,
            DepotDeclaration depotDeclar,
            List<DetailDepotDeclaration> listsDetailsDepotDeclarat,
            List<String> documentList,
            String observation,
            String numDepotDecl,
            List<NoteCalculData> listNcData) throws IOException {

        int counter = GeneralConst.Numeric.ZERO;

        Boolean result = false;
        HashMap<String, Object> depotDeclarationParams = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        propertiesMessage = new PropertiesMessage();

        String returnValue = DeclarationConst.ParamQuery.DEPOT_DECLARATION_CODE;
        String quetyDepotDeclaration = SQLQueryDeclaration.SAVE_DEPOT_DECLARATION;

        try {

            depotDeclarationParams.put(DeclarationConst.ParamQuery.NUMERO_DEPOT, depotDeclar.getNumeroDepot().trim());
            depotDeclarationParams.put(DeclarationConst.ParamQuery.PERSONNE, depotDeclar.getPersonne().getCode().trim());
            depotDeclarationParams.put(DeclarationConst.ParamQuery.BORDEREAU, depotDeclar.getBordereau());
            depotDeclarationParams.put(DeclarationConst.ParamQuery.AGENT, depotDeclar.getAgent().getCode());
            depotDeclarationParams.put(DeclarationConst.ParamQuery.SERVICE, depotDeclar.getService().getCode().trim());
            depotDeclarationParams.put(DeclarationConst.ParamQuery.NUMERO_ATTESTATION_PAIEMENT, null);
//            depotDeclarationParams.put(DeclarationConst.ParamQuery.HOST_NAME, logUser.getHostName().trim());
//            depotDeclarationParams.put(DeclarationConst.ParamQuery.ADRESSE_MAC, logUser.getMacAddress().trim());
//            depotDeclarationParams.put(DeclarationConst.ParamQuery.ADRESSE_IP, logUser.getIpAddress().trim());

            for (DetailDepotDeclaration detailsDepot : listsDetailsDepotDeclarat) {

                counter++;
                bulkQuery.put(counter + SQLQueryDeclaration.F_NEW_DETAILS_RECLAMATION, new Object[]{
                    detailsDepot.getArticleBudgetaire().getCode().trim(),
                    detailsDepot.getTaux(),
                    detailsDepot.getValeurBase(),
                    detailsDepot.getDevise().getCode().trim(),
                    detailsDepot.getTarif().getCode().trim(),
                    detailsDepot.getFkPeriode().trim()});
            }

            if (documentList.size() > 0) {
                for (String docu : documentList) {
                    counter++;
                    bulkQuery.put(counter + SQLQueryDeclaration.ARCHIVE_ACCUSER_RECEPTION,
                            new Object[]{
                                DocumentConst.DocumentCode.DEPOT_DECLARATION, docu, numDepotDecl, observation
                            });
                }
            }

            for (NoteCalculData ncData : listNcData) {

                counter++;
                bulkQuery.put(counter + SQLQueryDeclaration.P_NEW_NOTE_CALCUL_TO_ALL, new Object[]{
                    ncData.getCodePersonne().trim(),
                    ncData.getAgentCreate(),
                    ncData.getCodeService().trim(),
                    ncData.getCodeAdresse().trim(),
                    ncData.getExerciceFiscal(),
                    ncData.getCodeSite().trim(),
                    ncData.getCodeArticleBudgetaire().trim(),
                    ncData.getTauxArticleBudgetaire(),
                    ncData.getMontantDu(),
                    ncData.getPenaliser(),
                    ncData.getPeriodeDeclaration(),
                    ncData.getTarif().trim(),
                    ncData.getDevise().trim(),
                    ncData.getCodeBien().trim(),
                    ncData.getQuatite(),
                    ncData.getValeurBase(),
                    ncData.getTypeTaux(),
                    ncData.getCompteBancaire(),
                    ncData.getNumeroBordereau(),
                    ncData.getDatePaiementBordereau(),
                    ncData.getNumeroDepotDeclaration()

                });
            }

            result = myDao.getDaoImpl().execBulkQuery(quetyDepotDeclaration, returnValue, depotDeclarationParams, bulkQuery);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static Assujeti getAssujettissementByPeriode(String codePeriode) {

        try {
            String query = SQLQueryDeclaration.SELECT_ASSUJETTI_BY_PERIODE;

            List<Assujeti> assujeti = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false, codePeriode);
            return assujeti.size() > 0 ? assujeti.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static DepotDeclaration getDepotDeclarationByNumDepot(String numero) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_NUM_DEPOT;

            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, numero);
            return depotDecl.size() > 0 ? depotDecl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<DepotDeclaration> getListDepotDeclaration(
            String valueSearch,
            int typeSearch,
            String typeRegister,
            boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String codeAgent) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Three = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String primaryQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        boolean isLike = false;

        if (typeSearch == 1) {
            secondaryQuery_One = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_ONE_REGISTER_DEPOT_DECLARATION;
            isLike = true;
        } else if (typeSearch == 2) {
            secondaryQuery_One = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_TWO_REGISTER_DEPOT_DECLARATION;
            isLike = false;
        }

        if (!allSite) {
//            secondaryQuery_Two = SQLQueryTaxation.SecondaryQuery.SQL_QUERY_SECONDARY_THREE_REGISTER_NOTE_CALCUL;
//            secondaryQuery_Two = String.format(secondaryQuery_Two, codeSite.trim(), codeAgent.trim());
//        } else {
            secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        if (!allService) {
            secondaryQuery_Three = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_FOURT_REGISTER_DEPOT_DECLARATION;
            secondaryQuery_Three = String.format(secondaryQuery_Three, codeService.trim());
        } else {
            secondaryQuery_Three = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        switch (typeRegister.trim()) {
            case DeclarationConst.ConfigCodeRegister.REGISTRE_DEPOT_DECLATION: //Registre des dépôts de déclaration
                primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_DEPOT_DECLARATION;
                break;
        }

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, secondaryQuery_Three);

        List<DepotDeclaration> listDepotDeclations = (List<DepotDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(DepotDeclaration.class), isLike, valueSearch.trim());
        return listDepotDeclations;
    }

    public static List<DepotDeclaration> getListDepotDeclarationBySearchAvanced2(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeRegister) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String sqlFirstParam = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        switch (codeSite.trim()) {
            case "*":
                secondaryQuery_One = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_ONE_DEPOT_DECLARATION_ADVANCED;
                secondaryQuery_One = String.format(secondaryQuery_One, dateDebut.trim(), dateFin.trim());
                break;
            default:

//                secondaryQuery_One = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_THREE_DEPOT_DECLARATION_ADVANCED;
//                secondaryQuery_One = String.format(secondaryQuery_One, codeSite.trim(), dateDebut.trim(), dateFin.trim());
                break;
        }

        if (!codeService.equals("*")) {
            secondaryQuery_Two = SQLQueryDeclaration.SecondaryQuery.SQL_QUERY_SECONDARY_TWO_DEPOT_DECLARATION_ADVANCED;
            secondaryQuery_Two = String.format(secondaryQuery_Two, codeService.trim());
        } else {
            secondaryQuery_Two = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        }

        switch (typeRegister) {
            case DeclarationConst.ConfigCodeRegister.REGISTRE_DEPOT_DECLATION:
                sqlFirstParam = " WHERE DDCL.ETAT = 1 ";
                break;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_DEPOT_DECLARATION_ADVANCED_2;

        primaryQuery = String.format(primaryQuery, sqlFirstParam, secondaryQuery_One, secondaryQuery_Two);

        List<DepotDeclaration> listDepotDeclarations = (List<DepotDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false);
        return listDepotDeclarations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclaration(
            String valueSearch,
            int typeSearch,
            String typeRegister,
            boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String codeAgent) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = " AND RDCL.FK_AGENT_CREATE <> 37";

        String primaryQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {
            secondaryQuery_One = " AND RDCL.FK_ASSUJETTI = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);

        } else if (typeSearch == 2) {
            secondaryQuery_One = " AND RDCL.CODE_DECLARATION = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);
        }

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION;

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationDefaillant(
            String valueSearch,
            int typeSearch,
            String typeRegister,
            boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String codeAgent) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = " AND RDCL.FK_AGENT_CREATE <> 37";

        String primaryQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {
            secondaryQuery_One = " AND RDCL.FK_ASSUJETTI = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);

        } else if (typeSearch == 2) {
            secondaryQuery_One = " AND RDCL.CODE_DECLARATION = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);
        }

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT;

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationDefaillantPaidRetard(
            String valueSearch,
            int typeSearch,
            String typeRegister,
            boolean allSite,
            boolean allService,
            String codeSite,
            String codeService,
            String codeAgent) {

        String secondaryQuery_One = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;
        String secondaryQuery_Two = " AND RDCL.FK_AGENT_CREATE <> 37";

        String primaryQuery = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        if (typeSearch == 1) {
            secondaryQuery_One = " AND RDCL.FK_ASSUJETTI = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);

        } else if (typeSearch == 2) {
            secondaryQuery_One = " AND RDCL.CODE_DECLARATION = '%s'";
            secondaryQuery_One = String.format(secondaryQuery_One, valueSearch);
        }

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_PAID_RETARD;

        primaryQuery = String.format(primaryQuery, secondaryQuery_One, secondaryQuery_Two, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclations;
    }

    public static PeriodeDeclaration getPeriodeDeclarationById(String idPeriode) {

        try {
            String query = SQLQueryDeclaration.SELECT_PERIODE_DECLARATION_BY_ID;

            List<PeriodeDeclaration> periodeDeclarat = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, idPeriode);
            return periodeDeclarat.size() > 0 ? periodeDeclarat.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static ArchiveAccuseReception getArchivePreuvePaiementByDeclaration(String declarationCode) {

        try {
            String query = "SELECT * FROM T_ARCHIVE_ACCUSE_RECEPTION WITH (READPAST) "
                    + " WHERE DOCUMENT_REFERENCE = ?1 AND TYPE_DOCUMENT = 'PP'";

            List<ArchiveAccuseReception> archiveAccuseReceptions = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class), false, declarationCode);
            return archiveAccuseReceptions.size() > 0 ? archiveAccuseReceptions.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static TypeDocument getTypeDocumentByCode(String code) {

        try {
            String query = "SELECT * FROM T_TYPE_DOCUMENT WHERE CODE = ?1";

            List<TypeDocument> typeDocuments = (List<TypeDocument>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeDocument.class), false, code);
            return typeDocuments.size() > 0 ? typeDocuments.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArchiveAccuseReception> getListArchivePreuvePaiementByDeclaration(String declarationCode) {

        try {
            String query = "SELECT * FROM T_ARCHIVE_ACCUSE_RECEPTION WITH (READPAST) "
                    + " WHERE DOCUMENT_REFERENCE = ?1 AND TYPE_DOCUMENT = 'PP'";

            List<ArchiveAccuseReception> archiveAccuseReceptions = (List<ArchiveAccuseReception>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArchiveAccuseReception.class), false, declarationCode);
            return archiveAccuseReceptions;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeDeclaration,
            int codeAgent) {

        String SQL_Filter_Service = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            /*SQL_Filter_Service = " AND RDCL.FK_AB IN (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)\n"
             + "WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)\n"
             + "WHERE AG.SERVICE_ASSIETTE = '%s'))";*/
            SQL_Filter_Service = " AND RDCL.FK_AB IN (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)\n"
                    + "WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)\n"
                    + "WHERE AG.SERVICE_ASSIETTE IN (select FK_SERVICE from T_AGENT_SERVICE WITH (READPAST) WHERE FK_AGENT = %s AND ETAT = 1)))";

            //SQL_Filter_Service = String.format(SQL_Filter_Service, codeService);
            SQL_Filter_Service = String.format(SQL_Filter_Service, codeAgent);
        }

        String SQL_Filter_Date = " AND (DBO.FDATE(RDCL.DATE_CREATE) BETWEEN '%s' AND '%s' OR DBO.FDATE(RDCL.DATE_ECHEANCE_PAIEMENT ) BETWEEN '%s' AND '%s')";
        SQL_Filter_Date = String.format(SQL_Filter_Date, dateDebut, dateFin, dateDebut, dateFin);

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE = 37";
                break;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_ADVANCED_2;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Service, SQL_Filter_Date, SQL_Filter_Type_Declaration, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclarations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationDefaillantBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeDeclaration,
            int codeAgent) {

        String SQL_Filter_Service = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            SQL_Filter_Service = " AND RDCL.FK_AB IN (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)\n"
                    + "WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)\n"
                    + "WHERE AG.SERVICE_ASSIETTE IN (select FK_SERVICE from T_AGENT_SERVICE WITH (READPAST) WHERE FK_AGENT = %s AND ETAT = 1)))";

            SQL_Filter_Service = String.format(SQL_Filter_Service, codeAgent);
        }

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        String SQL_Filter_Date = "";

        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE = 37";
                break;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_ADVANCED_2;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Service, SQL_Filter_Date, SQL_Filter_Type_Declaration, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclarations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationDefaillantPaidBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeDeclaration,
            int codeAgent) {

        String SQL_Filter_Service = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            SQL_Filter_Service = " AND RDCL.FK_AB IN (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)\n"
                    + "WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)\n"
                    + "WHERE AG.SERVICE_ASSIETTE IN (select FK_SERVICE from T_AGENT_SERVICE WITH (READPAST) WHERE FK_AGENT = %s AND ETAT = 1)))";

            SQL_Filter_Service = String.format(SQL_Filter_Service, codeAgent);
        }

        String SQL_Filter_Site = "";

        if (!codeSite.equals(GeneralConst.ALL)) {
            SQL_Filter_Site = " AND RDCL.FK_SITE = '%s'";
            SQL_Filter_Site = String.format(SQL_Filter_Site, codeSite);
        }

        String SQL_Filter_Date = "";

        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE = 37";
                break;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_DEFAILLANT_ADVANCED_3;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Service, SQL_Filter_Date, SQL_Filter_Type_Declaration, SQL_Filter_Site);

        List<RetraitDeclaration> listRetraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclarations;
    }

    public static DepotDeclaration checkDepotDeclaration(String numero, String typeReseach) {
        String query = null;
        try {

            switch (typeReseach) {
                case DocumentConst.DocumentCode.BORDEREAU:
                    query = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_NUMERO_BORDEREAU;
                    break;
                case DocumentConst.DocumentCode.DEPOT_DECLARATION:
                    query = SQLQueryDeclaration.SELECT_CHECK_DECLARATION_TWO;
                    break;
            }

            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, numero);
            return depotDecl.size() > 0 ? depotDecl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static DepotDeclaration getDepotDeclarationByBordereau(String bordereau) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_NUMERO_BORDEREAU;

            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, bordereau);
            return depotDecl.size() > 0 ? depotDecl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static LoginWeb getLoginWebByPersonne(String codePersonne) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_LOGINWEB_PERSONNE;

            List<LoginWeb> loginW = (List<LoginWeb>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(LoginWeb.class), false, codePersonne);
            return loginW.size() > 0 ? loginW.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Palier getTauxAppliquer(String articleBudg, String tarif, String formeJurid) {

        try {

            String query = query = SQLQueryDeclaration.SELECT_TAUX_APPLIQUER_ALL;

            List<Palier> palier = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, articleBudg, tarif, formeJurid);
            return palier.size() > 0 ? palier.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Bordereau getBordereauByCode(String numero) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_BORDEREAU_BY_CODE;

            List<Bordereau> bordereau = (List<Bordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bordereau.class), false, numero);
            return bordereau.size() > 0 ? bordereau.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getListArticleBudgetaireAssujettisable(
            String valueInput) {

        boolean like;
        String secondaryQuery_One = GeneralConst.EMPTY_STRING;
        String primaryQuery;

        if (!valueInput.equals(GeneralConst.EMPTY_STRING)) {
            secondaryQuery_One = " AND INTITULE LIKE ?1";
            like = true;
        } else {
            like = false;
        }

        primaryQuery = SQLQueryDeclaration.SELECT_ARTICLE_BUDGETAIRE_ASSUJETTISABLE;

        primaryQuery = String.format(primaryQuery, secondaryQuery_One);

        List<ArticleBudgetaire> listArticleBudgetaire = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), like, valueInput);
        return listArticleBudgetaire;
    }

    public static DetailBordereau getDetailBordereauByPeriodicite(String numero) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_DETAIL_BORDEREAU_BY_PERIODE;

            List<DetailBordereau> detailBordereau = (List<DetailBordereau>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailBordereau.class), false, numero);
            return detailBordereau.size() > 0 ? detailBordereau.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static DepotDeclaration getDepotDeclarationByArticleBudgetaire(String codeAB) {
        String query = null;
        try {

            query = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_ARTICLE;

            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, codeAB);
            return depotDecl.size() > 0 ? depotDecl.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<ArticleBudgetaire> getArticlesBudgetairesAssujettissableByService(
            String codeService) {
        try {

            String sqlQuery;

            sqlQuery = SQLQueryDeclaration.LIST_ARTICLE_BUDGETAIRE_ASSUJETISSABLE_BY_SERVICE;

            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class),
                    false, codeService.trim());

            return articleBudgetaires;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DepotDeclaration> loadDepotDeclarationByArticleBudgetaire(String codeAB,
            String codePeriodicite, String annee, String mois) {

        String secondaryQuery_One = GeneralConst.EMPTY_STRING;
        String primaryQuery = GeneralConst.EMPTY_STRING;

        try {

//            if (!codePeriodicite.equals(GeneralConst.EMPTY_STRING)) {
//                secondaryQuery_One = "AND DDT.FK_PERIODE IN (SELECT PD.ID FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ID = ?2)";
//            }
            primaryQuery = SQLQueryDeclaration.SELECT_DEPOT_DECLARATION_BY_ARTICLE_AND_PERIODE;

//            primaryQuery = String.format(primaryQuery, secondaryQuery_One);
            List<DepotDeclaration> depotDecl = (List<DepotDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, codeAB, codePeriodicite);
            return depotDecl;
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationBySearchAvanced(
            String codeSite,
            String codeService,
            String dateDebut,
            String dateFin,
            String typeDeclaration) {

        String SQL_Filter_Service = GeneralConst.EMPTY_STRING;

        if (!codeService.equals(GeneralConst.ALL)) {

            SQL_Filter_Service = " AND RDCL.FK_AB IN (SELECT AB.CODE FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)\n"
                    + "WHERE AB.ARTICLE_GENERIQUE IN (SELECT AG.CODE FROM T_ARTICLE_GENERIQUE AG WITH (READPAST)\n"
                    + "WHERE AG.SERVICE_ASSIETTE = '%s'))";

            SQL_Filter_Service = String.format(SQL_Filter_Service, codeService);
        }

        String SQL_Filter_Date = " AND DBO.FDATE(RDCL.DATE_CREATE) BETWEEN '%s' AND '%s'";
        SQL_Filter_Date = String.format(SQL_Filter_Date, dateDebut, dateFin);

        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = " AND RDCL.FK_AGENT_CREATE = 37";
                break;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_REGISTER_RETRAIT_DECLARATION_ADVANCED_2;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Service, SQL_Filter_Date, SQL_Filter_Type_Declaration);

        List<RetraitDeclaration> listRetraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false);
        return listRetraitDeclarations;
    }

    public static List<RetraitDeclaration> getListRetraitDeclarationFromSearchAvanced(
            String codeSite,
            String codeAB,
            String mois,
            String annee,
            boolean isMensuel,
            String dateDebut,
            String dateFin,
            String typeDeclaration) {

        String queryPart2 = GeneralConst.EMPTY_STRING;
        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = "AND FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = "AND FK_AGENT_CREATE = 37";
                break;
        }

        if (isMensuel) {
            queryPart2 = SQLQueryDeclaration.MasterQuery.SELECT_RETRAIT_DECLARATION__ADVANCED_SEARCH_PART2;
            queryPart2 = String.format(queryPart2, mois);
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_RETRAIT_DECLARATION_FROM_ADVANCED_SEARCH_V2;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Type_Declaration, queryPart2);

        List<RetraitDeclaration> listRetraitDeclarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, codeAB, codeSite, annee, dateDebut, dateFin);
        return listRetraitDeclarations;
    }

    public static List<DepotDeclaration> getListDepotDeclarationBySearchAvanced(
            String codeSite,
            String codeAB,
            String mois,
            String annee,
            boolean isMensuel,
            String typeDeclaration) {

        String queryPart2 = GeneralConst.EMPTY_STRING;
        String SQL_Filter_Type_Declaration = GeneralConst.EMPTY_STRING;

        switch (typeDeclaration) {
            case GeneralConst.Number.ZERO:
                SQL_Filter_Type_Declaration = "AND FK_AGENT_CREATE <> 37";
                break;
            case GeneralConst.Number.ONE:
                SQL_Filter_Type_Declaration = "AND FK_AGENT_CREATE = 37";
                break;
        }

        if (isMensuel) {
            queryPart2 = SQLQueryDeclaration.MasterQuery.SELECT_DEPOT_DECLARATION__ADVANCED_SEARCH_PART2;
        }

        String primaryQuery = SQLQueryDeclaration.MasterQuery.SELECT_MASTER_DEPOT_DECLARATION_FROM_ADVANCED_SEARCH;

        primaryQuery = String.format(primaryQuery, SQL_Filter_Type_Declaration, queryPart2);

        List<DepotDeclaration> listDepotDeclarations = (List<DepotDeclaration>) myDao.getDaoImpl().find(primaryQuery,
                Casting.getInstance().convertIntoClassType(DepotDeclaration.class), false, codeAB, codeSite, annee, mois);
        return listDepotDeclarations;
    }

    public static ComplementBien getComplementBien(String codeTypeBien) {
        String query = null;
        try {

            query = "SELECT * FROM T_COMPLEMENT_BIEN WITH (READPAST) WHERE TYPE_COMPLEMENT IN(SELECT CODE FROM T_TYPE_COMPLEMENT_BIEN WHERE TYPE_BIEN = ?1)";

            List<ComplementBien> cbs = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, codeTypeBien);
            return cbs.size() > 0 ? cbs.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static ValeurPredefinie getValeurPredefinie(String code) {
        String query = null;
        try {

            query = "SELECT * FROM T_VALEUR_PREDEFINIE WITH (READPAST) WHERE CODE = ?1)";

            List<ValeurPredefinie> vps = (List<ValeurPredefinie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ValeurPredefinie.class), false, code);
            return vps.size() > 0 ? vps.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

    public static ParamTauxVignette getTauxVignette(String fkTarif, String fkFormeJuridique) {

        try {

            String query = "SELECT * FROM T_PARAM_TAUX_VIGNETTE WITH (READPAST) WHERE TARIF = ?1 AND FORME_JURIDIQUE = ?2";

            List<ParamTauxVignette> ptvs = (List<ParamTauxVignette>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ParamTauxVignette.class), false, fkTarif, fkFormeJuridique);
            return ptvs.size() > 0 ? ptvs.get(0) : null;
        } catch (Exception e) {
            throw e;
        }
    }

}
