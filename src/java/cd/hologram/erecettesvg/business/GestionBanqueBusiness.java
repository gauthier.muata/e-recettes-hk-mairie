/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.util.Casting;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class GestionBanqueBusiness {

    private static final Dao myDao = new Dao();

    public static List<Banque> getBanqueList() {
        try {
            String query = "SELECT B.* FROM T_BANQUE B WITH (READPAST) WHERE B.ETAT = 1 "
                    + " AND B.CODE NOT IN ('B0028','B0020') ORDER BY B.INTITULE";
            List<Banque> banques = (List<Banque>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Banque.class), false);
            return banques;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveBank(
            String codeBank,
            String intitule,
            String sigle,
            String codeSwift,
            int userId) {

        boolean result;

        String sqlQuery = "EXEC F_NEW_BANQUE_WEB ?1,?2,?3,?4,?5";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                codeBank,
                intitule.trim(),
                sigle,
                codeSwift,
                userId);
        return result;
    }

    public static boolean deleteBank(
            String codeBank,
            int userId) {

        boolean result;

        String sqlQuery = "UPDATE T_BANQUE SET ETAT = 0, AGENT_MAJ = ?1, DATE_MAJ = GETDATE() WHERE CODE = ?2";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                userId, codeBank);
        return result;
    }

    public static List<CompteBancaire> getCompteBancaireList() {
        try {
            String query = "select cb.* from t_compte_bancaire cb with (readpast) where cb.code <> '*' "
                    + " and cb.etat = 1 order by cb.banque,cb.devise";
            List<CompteBancaire> compteBancaires = (List<CompteBancaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CompteBancaire.class), false);
            return compteBancaires;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean editAccountBank(
            String code,
            String libelle,
            String devise,
            String banque,
            int userId) {

        boolean result;

        String sqlQuery = "EXEC F_NEW_COMPTE_BANCAIRES ?1,?2,?3,?4,?5";
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery,
                code,
                libelle.trim(),
                devise,
                banque,
                userId);
        return result;
    }

    public static List<Devise> loadDevises() {
        try {
            String query = "SELECT * FROM T_DEVISE WITH (READPAST) WHERE ETAT = 1";
            List<Devise> deviseList = (List<Devise>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Devise.class), false);
            return deviseList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Taux> loadTauxByDevises(String codeDevise) {
        try {
            String query = "SELECT * FROM T_TAUX WITH (READPAST) WHERE DEVISE = ?1";
            List<Taux> tauxList = (List<Taux>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Taux.class), false, codeDevise.trim());
            return tauxList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Devise loadDevisesByCode(String codedevise) {
        try {
            String query = "SELECT * FROM T_DEVISE WITH (READPAST) WHERE ETAT = 1 AND CODE = ?1";
            List<Devise> deviseList = (List<Devise>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Devise.class), false, codedevise.trim());
            return deviseList.size() > 0 ? deviseList.get(0) : null;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveTaux(Taux taux) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (taux.getId() > 0) {

                bulkQuery.put(counter + SQLQueryGeneral.UPDATE_TAUX_CHANGE, new Object[]{
                    taux.getDevise().getCode().trim(),
                    taux.getDeviseDest().trim(),
                    taux.getTaux(),
                    taux.getAgentCreat(),
                    taux.getId()});

                counter++;

            } else {

                bulkQuery.put(counter + SQLQueryGeneral.SAVE_TAUX_CHANGE, new Object[]{
                    taux.getDevise().getCode().trim(),
                    taux.getDeviseDest().trim(),
                    taux.getTaux(),
                    taux.getAgentCreat()});

                counter++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }
}
