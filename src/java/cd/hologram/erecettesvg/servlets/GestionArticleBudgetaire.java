 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.CleRepartitionBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.CleRepartition;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.DocumentOfficiel;
import cd.hologram.erecettesvg.models.ExerciceFiscale;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.NatureArticleBudgetaire;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.Periodicite;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Unite;
import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.ProrogationEcheanceLegaleDeclaration;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.TarifSite;
import cd.hologram.erecettesvg.models.TypeBien;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.*;
import java.math.BigDecimal;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "GestionArticleBudgetaire", urlPatterns = {"/articleBudgetaire_servlet"})
public class GestionArticleBudgetaire extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    List<ArticleGenerique> listArticleGenerique = new ArrayList<>();
    List<DocumentOfficiel> listDocumentOfficiel = new ArrayList<>();
    List<Tarif> listTarif = new ArrayList<>();

    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case GestionArticleBudgetaireConst.Operation.LOAD_EXERCICE_FISCAL:
                result = loadExerciceFiscal();
                break;
            case GestionArticleBudgetaireConst.Operation.ADD_EXERCICE_FISCAL:
                result = addExerciceFiscal(request);
                break;
            case GestionArticleBudgetaireConst.Operation.DELETE_EXERCICE_FISCAL:
                result = deleteExerciceFiscal(request);
                break;
            case GestionArticleBudgetaireConst.Operation.UPDATE_EXERCICE_FISCAL:
                result = updateExerciceFiscal(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_MINISTERE:
                result = loadMinistere(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_SERVICES:
                result = loadServices(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_FAITS_GENERATEURS_SERVICE:
                result = loadFaitsGenerateurs(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_FORME_JURIDIQUE:
                result = loadFormeJuridiques();
                break;
            case GestionArticleBudgetaireConst.Operation.SAVE_FAIT_GENERATEUR:
                result = saveFaitGenerateur(request);
                break;
            case GestionArticleBudgetaireConst.Operation.ADD_SERVICE:
                result = addService(request);
                break;
            case GestionArticleBudgetaireConst.Operation.UPDATE_SERVICE:
                result = updateService(request);
                break;
            case GestionArticleBudgetaireConst.Operation.ACTIVATE_DEACTIVATE_SERVICE:
                result = activeAndDeactiveService(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_DOCUMENT_OFFICIEL:
                result = loadDocumentOfficiel();
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_NATURE_ARTICLE_BUDGETAIRE:
                result = loadNatureArticleBudgetaire();
                break;

            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_BUDGETAIRE:
                result = loadArticleBudgetaires(request);
                break;

            case GestionArticleBudgetaireConst.Operation.ADD_CLE_REPARTITION:
                result = savecleRepartition(request);
                break;

            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_BUDGETAIRE_CLEREP_BY_MINISTERE:
                result = loadAritcleBudgetaireCleRepByMinistere(request);
                break;

            case GestionArticleBudgetaireConst.Operation.DELETE_CLE_REPARTITION:
                result = DeleteCleRepartition(request);
                break;

            case GestionArticleBudgetaireConst.Operation.LOAD_CLE_REPARTITIION:
                result = LoadCleRepartition(request);
                break;

            case GestionArticleBudgetaireConst.Operation.LOAD_SERVICES_ASSIETTES:
                result = LoadServiceAssiettes(request);
                break;
            case GestionArticleBudgetaireConst.Operation.SAVE_DOCUMENT_OFFICIEL:
                result = saveDocumentOfficiel(request);
                break;
            case GestionArticleBudgetaireConst.Operation.GET_DOCUMENT_OFFICIEL:
                result = getDocumentsOfficiels(request);
                break;
            case GestionArticleBudgetaireConst.Operation.GET_ARTICLE_GENERIQUE:
                result = loadArticleGenerique(request);
                break;
            case GestionArticleBudgetaireConst.Operation.GET_DOCUMENT_OFFICIELS:
                result = loadDocumentOfficiels(request);
                break;
            case GestionArticleBudgetaireConst.Operation.INIT_DATA_ARTICLE:
                result = initDataArticle();
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_TARIF:
                result = loadTarif(request);
                break;
            case GestionArticleBudgetaireConst.Operation.SAVE_ARTICLE_BUDGETAIRE:
                result = saveArticleBudgetaire(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_BANQUE:
                result = loadBanque(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_COMPTE_BANCAIRE:
                result = loadCompteBancaire(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_AFFECTRE:
                result = loadArticleAffecterBanque(request);
                break;
            case GestionArticleBudgetaireConst.Operation.GET_SERVICE_ASSIETTE:
                result = getServiceAssiettes(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_SERVICE:
                result = loadArticleBudgetairesByService(request);
                break;
            case GestionArticleBudgetaireConst.Operation.AFFECTER_ARTICLE_COMPTE:
                result = affecterArticleCompte(request);
                break;
            case GestionArticleBudgetaireConst.Operation.DESAFFECTER_ARTICLE_COMPTE:
                result = desaffecterArticleCompte(request);
                break;
            case "loadTarifs":
                result = loadTarifs(request);
                break;
            case "saveTarif":
                result = saveTarif(request);
                break;
            case "disableTarif":
                result = disableTarif(request);
                break;
            case "updateTarif":
                result = updateTarif(request);
                break;
            case "loadPeriodicite":
                result = loadPeriodicite();
                break;
            case "savePeriodicite":
                result = savePeriodicite(request);
                break;
            case "updatePeriodicite":
                result = updatePeriodicte(request);
                break;
            case "disablePeriodicite":
                result = disablePeriodicite(request);
                break;
            case "loadTarifSites":
                result = getListTarifsBySite(request);
                break;
            case "initDataTarifSite":
                result = getInitDataTarifSite(request);
                break;
            case "saveTarifSite":
                result = saveTarifSite(request);
                break;
            case "deleteTarifSite":
                result = deleteTarifSite(request);
                break;
            case "loadUnite":
                result = loadUnite();
                break;
            case "saveUnite":
                result = saveUnite(request);
                break;
            case "updateUnite":
                result = updateUnite(request);
                break;
            case "disableUnite":
                result = disableUnite(request);
                break;
            case "loadInfoArticleBudgetaire":
                result = loadInfosArticleBudgetaire(request);
                break;
            case "initDataProrogationEcheanceLegale":
                result = initDataProrogationEcheanceLegale(request);
                break;
            case "loadProrogations":
                result = loadProrogations(request);
                break;
            case "saveProrogation":
                result = saveProrogation(request);
                break;
            case "deleteProrogation":
                result = deleteProrogation(request);
                break;
            case "getArticleBudgetaireByService":
                result = getArticleBudgetaireByService(request);
                break;
            case "getArticleBudgetaireByIntitule":
                result = getArticleBudgetaireByIntitule(request);
                break;
            case "loadEntiteAdministrativeFille":
                result = loadEntiteAdministrativeFille2(request);
                break;
            case "loadEntiteAdministrativeBytype":
                result = loadEntiteAdministrativeByType(request);
                break;
            case "getArticleBudgetaireImpot":
                result = getArticleBudgetaireImpot(request);
                break;
        }
        out.print(result);
    }

    public String deleteTarifSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String tarifSiteID = request.getParameter("id");
            String userId = request.getParameter("userId");

            if (GestionArticleBudgetaireBusiness.deleteTarifSite(Integer.valueOf(tarifSiteID), Integer.valueOf(userId))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveTarifSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String tarifSiteID = request.getParameter("id");
            String siteProvenance = request.getParameter("siteProvenance");
            String siteDestination = request.getParameter("siteDestination");
            String tarif = request.getParameter("tarif");
            String taux = request.getParameter("taux");
            String devise = request.getParameter("devise");
            String userId = request.getParameter("userId");

            if (tarifSiteID == null) {
                tarifSiteID = GeneralConst.EMPTY_STRING;
            }

            int id = tarifSiteID.equals(GeneralConst.EMPTY_STRING) ? 0 : Integer.valueOf(tarifSiteID);
            BigDecimal tauxTarif = new BigDecimal("0");

            tauxTarif = tauxTarif.add(BigDecimal.valueOf(Double.valueOf(taux)));

            TarifSite tarifSite = new TarifSite();

            tarifSite.setId(id);
            tarifSite.setFkSiteProvenance(siteProvenance.trim());
            tarifSite.setFkSiteDestination(siteDestination.trim());
            tarifSite.setFkTarif(tarif.trim());
            tarifSite.setTaux(tauxTarif);
            tarifSite.setFkDevise(devise.trim());
            tarifSite.setAgentCreate(Integer.valueOf(userId));

            if (GestionArticleBudgetaireBusiness.saveTarifSite(tarifSite)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadExerciceFiscal() {
        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<ExerciceFiscale> efs = new ArrayList<>();

        try {
            efs = GestionArticleBudgetaireBusiness.getListExerciceFiscal();
            if (!efs.isEmpty()) {
                for (ExerciceFiscale ef : efs) {
                    JSONObject object = new JSONObject();
                    object.put("code", ef.getCode());
                    object.put("intitule", ef.getIntitule() == null ? GeneralConst.EMPTY_STRING : ef.getIntitule());
                    object.put("debut", ef.getDebut() == null ? GeneralConst.EMPTY_STRING : Tools.formatDateToString(ef.getDebut()));
                    object.put("fin", ef.getFin() == null ? GeneralConst.EMPTY_STRING : Tools.formatDateToString(ef.getFin()));
                    object.put("etat", ef.getEtat());
                    object.put("agent_create", ef.getAgentCreat() == null ? GeneralConst.EMPTY_STRING : agent(ef.getAgentCreat()));
                    object.put("date_create", ef.getDebut() == null ? GeneralConst.EMPTY_STRING : Tools.formatDateToString(ef.getDateCreat()));
                    object.put("agent_maj", ef.getAgentMaj() == null ? GeneralConst.EMPTY_STRING : agent(ef.getAgentMaj()));
                    object.put("date_maj", ef.getDateMaj() == null ? GeneralConst.EMPTY_STRING : Tools.formatDateToString(ef.getDateMaj()));
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String deleteExerciceFiscal(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String code = request.getParameter("annee");
        try {
            if (GestionArticleBudgetaireBusiness.deleteExerciceFiscal(code)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String DeleteCleRepartition(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String code = request.getParameter("code");
        try {
            if (CleRepartitionBusiness.DeleteCleRepartition(code)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String addExerciceFiscal(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        String userId = request.getParameter("userId");
        String code = request.getParameter("annee");
        String intitule = request.getParameter("intitule");
        String dateDebut = Tools.getValidFormat(request.getParameter("dateDebut"));
        String dateFin = Tools.getValidFormat(request.getParameter("dateFin"));

        try {
            ExerciceFiscale ef = new ExerciceFiscale();
            ef.setCode(code);
            ef.setIntitule(intitule);
            ef.setAgentCreat(userId);

            if (GestionArticleBudgetaireBusiness.saveExerciceFiscal(ef, dateDebut, dateFin)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String updateExerciceFiscal(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        String userId = request.getParameter("userId");
        String code = request.getParameter("annee");
        String intitule = request.getParameter("intitule");
        String dateDebut = Tools.getValidFormat(request.getParameter("dateDebut"));
        String dateFin = Tools.getValidFormat(request.getParameter("dateFin"));

        try {
            ExerciceFiscale ef = new ExerciceFiscale();
            ef.setCode(code);
            ef.setIntitule(intitule);
            ef.setAgentMaj(userId);

            if (GestionArticleBudgetaireBusiness.updateExerciceFiscal(ef, dateDebut, dateFin)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    private String agent(String code) {
        String result = GeneralConst.EMPTY_STRING;
        Agent agent = GestionArticleBudgetaireBusiness.getAgentByCode(code);
        if (agent != null) {
            result = agent.toString();
        } else {
            result = GeneralConst.EMPTY_STRING;
        }
        return result;
    }

    private String loadMinistere(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<Service> services = new ArrayList<>();
        String indice = request.getParameter("code");

        try {
            services = GestionArticleBudgetaireBusiness.loadMinistere(indice);
            if (!services.isEmpty() || services.size() > 0) {
                for (Service service : services) {

                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule() == null ? GeneralConst.EMPTY_STRING : service.getIntitule());
                    object.put("etat", service.getEtat());
                    object.put("fkService", service.getService() == null ? GeneralConst.Number.ZERO : service.getService().getCode());
                    jSONObject.add(object);

                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String LoadCleRepartition(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<CleRepartition> clerepart = new ArrayList<>();
        String code = request.getParameter("code");

        try {
            clerepart = CleRepartitionBusiness.loadCleRepartition(code);
            if (!clerepart.isEmpty()) {
                for (CleRepartition service : clerepart) {

                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("taux", service.getTaux() == null ? GeneralConst.EMPTY_STRING : service.getTaux());
                    object.put("entitebeneficiaire", service.getEntiteBeneficiaire());
                    jSONObject.add(object);

                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadAritcleBudgetaireCleRepByMinistere(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<ArticleBudgetaire> articlebudgetaire = new ArrayList<>();
        String codeministere = request.getParameter("codeministere");

        try {
            articlebudgetaire = GestionArticleBudgetaireBusiness.ArticleBugdetaireRepartiByMinistere(codeministere);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire articleclerepartie : articlebudgetaire) {

                    JSONObject object = new JSONObject();
                    object.put("code", articleclerepartie.getCode());
                    object.put("intitule", articleclerepartie.getIntitule() == null ? GeneralConst.EMPTY_STRING : articleclerepartie.getIntitule());
                    object.put("etat", articleclerepartie.getEtat());
                    jSONObject.add(object);

                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadServices(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<Service> services = new ArrayList<>();
        String code_ministere = request.getParameter("code_ministere");
        String indice = request.getParameter("code");

        try {

            services = GestionArticleBudgetaireBusiness.loadServices(code_ministere, indice);
            if (!services.isEmpty()) {
                for (Service service : services) {

                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule());
                    object.put("fkService", service.getService() == null ? GeneralConst.Number.ZERO : service.getService().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String LoadServiceAssiettes(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<Service> services = new ArrayList<>();
        String code = request.getParameter("code");

        try {

            services = GestionArticleBudgetaireBusiness.loadServicesAssiettes(code);
            if (!services.isEmpty()) {
                for (Service service : services) {

                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadFormeJuridiques() {

        List<JSONObject> jsonFormeJuridiques = new ArrayList<>();

        try {

            List<FormeJuridique> formeJuridiques = GestionArticleBudgetaireBusiness.getFormeJuridiques();

            for (FormeJuridique formeJuridique : formeJuridiques) {
                JSONObject jsonFormeJuridique = new JSONObject();
                jsonFormeJuridique.put(GestionArticleBudgetaireConst.Operation.CODE_FORME_JURIDIQUE,
                        formeJuridique == null
                                ? GeneralConst.EMPTY_STRING
                                : formeJuridique.getCode());
                jsonFormeJuridique.put(GestionArticleBudgetaireConst.Operation.LIBELLE_FORME_JURIDIQUE,
                        formeJuridique == null
                                ? GeneralConst.EMPTY_STRING
                                : formeJuridique.getIntitule());
                jsonFormeJuridiques.add(jsonFormeJuridique);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonFormeJuridiques.toString();
    }

    private String loadFaitsGenerateurs(HttpServletRequest request) {

        String result;
        String code_service = request.getParameter("code_service");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ArticleGenerique> faitGenerateurs = GestionArticleBudgetaireBusiness.loadFaitGenerateurByService(code_service);
            if (!faitGenerateurs.isEmpty()) {
                for (ArticleGenerique articleGenerique : faitGenerateurs) {

                    FormeJuridique formeJuridique = articleGenerique.getFormeJuridique();
                    Service service = articleGenerique.getServiceAssiette();

                    JSONObject object = new JSONObject();
                    object.put("code", articleGenerique.getCode());
                    object.put("intitule", articleGenerique.getIntitule());
                    object.put("codeForme", formeJuridique == null
                            ? "" : formeJuridique.getCode());
                    object.put("intituleForme", formeJuridique == null
                            ? "" : formeJuridique.getIntitule().toUpperCase());
                    object.put("service", service == null ? "" : service.getIntitule());
                    object.put("ministere", service == null ? "" : service.getService() == null
                            ? ""
                            : service.getService().getIntitule());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadArticleBudgetaires(HttpServletRequest request) {

        String result;
        String code_article = request.getParameter("code_article");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.loadArticleBudgetaire(code_article);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {

                    JSONObject object = new JSONObject();

                    object.put("intitule", article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel());
                    object.put("faitgenerateur", article.getArticleGenerique() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getArticleGenerique().getIntitule());
                    object.put("intituleperiode", article.getPeriodicite() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getPeriodicite().getIntitule());
                    object.put("codeArticle", article.getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveFaitGenerateur(HttpServletRequest request) {
        String result;
        String code = request.getParameter("code");
        String intitule = request.getParameter("intitule");
        String codeService = request.getParameter("codeService");
        String codeForme = request.getParameter("codeForme");
        String etat = request.getParameter("etat");

        try {
            ArticleGenerique at = new ArticleGenerique();
            at.setCode(code);
            at.setIntitule(intitule);
            at.setServiceAssiette(new Service(codeService));
            at.setFormeJuridique(new FormeJuridique(codeForme));
            at.setEtat(etat.equals("1"));

            if (GestionArticleBudgetaireBusiness.saveFaitGenerateur(at)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    // cle de repartition
    public String savecleRepartition(HttpServletRequest request) {
        String result;
//        String fk_article_budgetaire = request.getParameter("article_bugdetaire");
//        double taux = Double.valueOf(request.getParameter("taux"));
//        String entitebeneficiaire = request.getParameter("entitebeneficiaire");
//        boolean etat = Boolean.parseBoolean(request.getParameter("etat"));
////        int valtresorPart = Integer.valueOf(request.getParameter("tresorpart"));
//        String is_tresor_part = request.getParameter("tresorpart");

        String repartitionList = request.getParameter("repartitionList");

        List<CleRepartition> cleRepartitionList = new ArrayList<>();
        try {

            JSONArray repartitionArray = new JSONArray(repartitionList);

            if (repartitionArray.length() > 0) {
                for (int i = 0; i < repartitionArray.length(); i++) {
                    JSONObject data = repartitionArray.getJSONObject(i);
                    CleRepartition clerepart = new CleRepartition();
                    BigDecimal taux = new BigDecimal(0);
                    taux = BigDecimal.valueOf(Integer.valueOf(data.getString("tauxactegenerateur")));
                    clerepart.setFkArticleBudgetaire(new ArticleBudgetaire(data.getString("article_bugdetaire")));
                    clerepart.setEntiteBeneficiaire(data.getString("entitearticle"));
                    clerepart.setEtat(Boolean.valueOf(data.getString("etat")));
                    clerepart.setIsTresorPart(Boolean.valueOf(data.getString("cbtresorpart")));
                    clerepart.setTaux(taux);
                    cleRepartitionList.add(clerepart);
                }
            }

            result = GeneralConst.ResultCode.FAILED_OPERATION;
            if (CleRepartitionBusiness.savecleRepartition(cleRepartitionList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String addService(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String userId = request.getParameter("userId");
        String intitule = request.getParameter("intitule");
        String fkService = request.getParameter("code_ministere");

        try {

            if (GestionArticleBudgetaireBusiness.saveService(intitule, fkService, userId)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String updateService(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String intitule = request.getParameter("intitule");
        String code_service = request.getParameter("code_service");
        String fkService = request.getParameter("code_ministere");

        try {

            if (GestionArticleBudgetaireBusiness.updateService(intitule, code_service, fkService)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String activeAndDeactiveService(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String code = request.getParameter("code_service");
        int etat = Integer.valueOf(request.getParameter("etat"));

        try {

            if (GestionArticleBudgetaireBusiness.activeAndDeactiveService(etat, code)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    private String loadDocumentOfficiel() {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<DocumentOfficiel> documentOfficiels = GestionArticleBudgetaireBusiness.loadDocumentOfficiel();
            if (!documentOfficiels.isEmpty()) {
                for (DocumentOfficiel documentOfficiel : documentOfficiels) {

                    JSONObject object = new JSONObject();
                    object.put("code", documentOfficiel.getCode());
                    object.put("intitule", documentOfficiel.getIntitule());
                    object.put("numero", documentOfficiel.getNumero());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadNatureArticleBudgetaire() {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<NatureArticleBudgetaire> nabs = GestionArticleBudgetaireBusiness.loadNatureArticleBudgetaire();
            if (!nabs.isEmpty()) {
                for (NatureArticleBudgetaire nab : nabs) {

                    JSONObject object = new JSONObject();
                    object.put("codeNatureArticle", nab.getCode());
                    object.put("intitule", nab.getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String saveDocumentOfficiel(HttpServletRequest request) {

        String result = null;

        try {
            String intituleDocument = request.getParameter(GestionArticleBudgetaireConst.ParamName.INTITULE_DOCUMENT_OFFICIEL);
            String numeroArrete = request.getParameter(GestionArticleBudgetaireConst.ParamName.NUMERO_ARRETE);
            String dateArrete = request.getParameter(GestionArticleBudgetaireConst.ParamName.DATE_ARRETE);

            DocumentOfficiel document = new DocumentOfficiel();

            document.setIntitule(intituleDocument);
            document.setNumero(numeroArrete);
            document.setDateArrete(dateArrete);

            if (GestionArticleBudgetaireBusiness.createDocumentOfficiel(document)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String getDocumentsOfficiels(HttpServletRequest request) {

        List<JSONObject> jsonDocumentOfficiel = new ArrayList<>();
        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);

        try {

            List<DocumentOfficiel> documentOfficiels = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                documentOfficiels = GestionArticleBudgetaireBusiness.getDocumentOfficielByIntitule(libelle);
            } else {
                DocumentOfficiel documentOff = GestionArticleBudgetaireBusiness.getDocumentOfficielByNumeroArrete(libelle);
                if (documentOff != null) {
                    documentOfficiels.add(documentOff);
                }
            }

            for (DocumentOfficiel documentOfficiel : documentOfficiels) {
                JSONObject jsonDocument = new JSONObject();

                jsonDocument.put(GestionArticleBudgetaireConst.ParamName.CODE,
                        documentOfficiel == null
                                ? GeneralConst.EMPTY_STRING
                                : documentOfficiel.getCode() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getCode());
                jsonDocument.put(GestionArticleBudgetaireConst.ParamName.NUMERO_ARRETE,
                        documentOfficiel == null
                                ? GeneralConst.EMPTY_STRING
                                : documentOfficiel.getNumero() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getNumero());
                jsonDocument.put(GestionArticleBudgetaireConst.ParamName.INTITULE_DOCUMENT_OFFICIEL,
                        documentOfficiel == null
                                ? GeneralConst.EMPTY_STRING
                                : documentOfficiel.getIntitule() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getIntitule());
                jsonDocument.put(GestionArticleBudgetaireConst.ParamName.DATE_ARRETE,
                        documentOfficiel == null
                                ? GeneralConst.EMPTY_STRING
                                : documentOfficiel.getDateArrete() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getDateArrete());
                jsonDocument.put(GestionArticleBudgetaireConst.ParamName.ETAT,
                        documentOfficiel == null
                                ? GeneralConst.EMPTY_STRING
                                : documentOfficiel.getEtat() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getEtat());

                jsonDocumentOfficiel.add(jsonDocument);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDocumentOfficiel.toString();
    }

    public String loadArticleGenerique(HttpServletRequest request) {

        String valueSearch, typeSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            if (valueSearch != null && !valueSearch.isEmpty()) {

                listArticleGenerique = GestionArticleBudgetaireBusiness.getListArticleGenerique(valueSearch.trim());

                if (listArticleGenerique != null && !listArticleGenerique.isEmpty()) {

                    for (ArticleGenerique articleGenerique : listArticleGenerique) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE_GENERIQUE,
                                articleGenerique.getCode() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getCode());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE_GENERIQUE,
                                articleGenerique.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getIntitule());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_SERVICE,
                                articleGenerique.getServiceAssiette() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getServiceAssiette().getCode() == null ? GeneralConst.EMPTY_STRING
                                                : articleGenerique.getServiceAssiette().getCode());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_SERVICE,
                                articleGenerique.getServiceAssiette() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getServiceAssiette().getIntitule() == null ? GeneralConst.EMPTY_STRING
                                                : articleGenerique.getServiceAssiette().getIntitule());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_FORME_JURIDIQUE,
                                articleGenerique.getFormeJuridique() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getFormeJuridique().getCode() == null ? GeneralConst.EMPTY_STRING
                                                : articleGenerique.getFormeJuridique().getCode());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_FORME_JURIDIQUE,
                                articleGenerique.getFormeJuridique() == null ? GeneralConst.EMPTY_STRING
                                        : articleGenerique.getFormeJuridique().getIntitule() == null ? GeneralConst.EMPTY_STRING
                                                : articleGenerique.getFormeJuridique().getIntitule());

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String loadDocumentOfficiels(HttpServletRequest request) {

        String valueSearch, typeSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            if (valueSearch != null && !valueSearch.isEmpty()) {

                listDocumentOfficiel = GestionArticleBudgetaireBusiness.getListDocumentOfficiel(valueSearch.trim());

                if (listDocumentOfficiel != null && !listDocumentOfficiel.isEmpty()) {

                    for (DocumentOfficiel documentOfficiel : listDocumentOfficiel) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_DOCUMENT_OFFICIEL,
                                documentOfficiel.getCode() == null ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getCode());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_DOCUMENT_OFFICIEL,
                                documentOfficiel.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getIntitule());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.NUMERO_ARRETE,
                                documentOfficiel.getNumero() == null ? GeneralConst.EMPTY_STRING
                                        : documentOfficiel.getNumero());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.DATE_ARRETE,
                                documentOfficiel.getDateArrete() == null ? GeneralConst.EMPTY_STRING
                                        : ConvertDate.getValidFormatDatePrint(documentOfficiel.getDateArrete()));

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String initDataArticle() {

        JSONObject jsonResult = new JSONObject();
        List<EntiteAdministrative> villeList = new ArrayList<>();

        try {
            List<Periodicite> periodiciteList = GestionArticleBudgetaireBusiness.getPeriodiciteList();
            List<FormeJuridique> formeJuridiqueList = GestionArticleBudgetaireBusiness.getFormeJuridiqueList();
            List<Devise> deviseList = GestionArticleBudgetaireBusiness.getDeviseList();
            List<Unite> uniteList = GestionArticleBudgetaireBusiness.getUniteList();
            String codeProvince = propertiesConfig.getProperty("CODE_HAUT_KATANGA");
            String natureArticleBudgetaire = propertiesConfig.getProperty("NATURE_ARTICLE");

            EntiteAdministrative objetProvince = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByCode(codeProvince);

            if (objetProvince != null) {
                villeList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeFille(objetProvince.getCode());
            }

            if (!periodiciteList.isEmpty()) {

                List<JSONObject> jsonPeriodiciteList = new ArrayList<>();

                for (Periodicite periodicite : periodiciteList) {

                    JSONObject jsonPeriodicite = new JSONObject();

                    jsonPeriodicite.put(GestionArticleBudgetaireConst.ParamName.CODE_PERIODICITE, periodicite.getCode());
                    jsonPeriodicite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_PERIODICITE,
                            periodicite.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                    : periodicite.getIntitule());
                    jsonPeriodiciteList.add(jsonPeriodicite);
                }
                jsonResult.put(GestionArticleBudgetaireConst.ParamName.PERIODICITE, jsonPeriodiciteList.toString());
            }

            if (!formeJuridiqueList.isEmpty()) {

                List<JSONObject> jsonFormeJuridiqueList = new ArrayList<>();

                for (FormeJuridique formeJuridique : formeJuridiqueList) {

                    JSONObject jsonFormeJuridique = new JSONObject();

                    jsonFormeJuridique.put(GestionArticleBudgetaireConst.ParamName.CODE_FORME_JURIDIQUE, formeJuridique.getCode());
                    jsonFormeJuridique.put(GestionArticleBudgetaireConst.ParamName.INTITULE_FORME_JURIDIQUE,
                            formeJuridique.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                    : formeJuridique.getIntitule());
                    jsonFormeJuridiqueList.add(jsonFormeJuridique);
                }
                jsonResult.put(GestionArticleBudgetaireConst.ParamName.FORME_JURIDIQUE, jsonFormeJuridiqueList.toString());
            }

            if (!deviseList.isEmpty()) {

                List<JSONObject> jsonDeviseList = new ArrayList<>();

                for (Devise devise : deviseList) {

                    JSONObject jsonDevise = new JSONObject();

                    jsonDevise.put(GestionArticleBudgetaireConst.ParamName.CODE_DEVISE, devise.getCode());
                    jsonDevise.put(GestionArticleBudgetaireConst.ParamName.INTITULE_DEVISE,
                            devise.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                    : devise.getIntitule());
                    jsonDeviseList.add(jsonDevise);
                }
                jsonResult.put(GestionArticleBudgetaireConst.ParamName.DEVISE, jsonDeviseList.toString());
            }

            if (!uniteList.isEmpty()) {

                List<JSONObject> jsonUniteList = new ArrayList<>();

                for (Unite unite : uniteList) {

                    JSONObject jsonUnite = new JSONObject();

                    jsonUnite.put(GestionArticleBudgetaireConst.ParamName.CODE_UNITE, unite.getCode());
                    jsonUnite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_UNITE,
                            unite.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                    : unite.getIntitule());
                    jsonUniteList.add(jsonUnite);
                }
                jsonResult.put(GestionArticleBudgetaireConst.ParamName.UNITE, jsonUniteList.toString());
            }

            if (!villeList.isEmpty()) {

                List<JSONObject> jsonVilleList = new ArrayList<>();

                for (EntiteAdministrative entite : villeList) {

                    JSONObject jsonVille = new JSONObject();

                    jsonVille.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, entite.getCode());
                    jsonVille.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ENTITE, entite.getIntitule());
                    jsonVilleList.add(jsonVille);
                }
                jsonResult.put(GestionArticleBudgetaireConst.ParamName.ENTITE_ADMINISTRATIVE, jsonVilleList.toString());
            }

            jsonResult.put(GestionArticleBudgetaireConst.ParamName.CHECK_NATURE_ARTICLE, natureArticleBudgetaire);

        } catch (JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonResult.toString();
    }

    public String loadTarif(HttpServletRequest request) {

        String valueSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            String userId = request.getParameter("userId");

            Agent agent = GeneralBusiness.getAgentByCode(userId);

            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            if (valueSearch != null && !valueSearch.isEmpty()) {

                listTarif = GestionArticleBudgetaireBusiness.getListTarif(valueSearch.trim(), agent.getSite().getPeage());

                if (listTarif != null && !listTarif.isEmpty()) {

                    for (Tarif tarif : listTarif) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_TARIF,
                                tarif.getCode() == null ? GeneralConst.EMPTY_STRING
                                        : tarif.getCode());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_TARIF,
                                tarif.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                        : tarif.getIntitule());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.TYPE_VALEUR_TARIF,
                                tarif.getTypeValeur() == null ? GeneralConst.EMPTY_STRING
                                        : tarif.getTypeValeur());
                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.VALEUR_TARIF,
                                tarif.getValeur() == null ? GeneralConst.Numeric.ZERO
                                        : tarif.getValeur());

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String saveArticleBudgetaire(HttpServletRequest request) {

        String result = null;

        try {

            String codeArticleGenerique = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE_GENERIQUE);
            String intituleArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE);
            String codeUniteArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_UNITE_ARTICLE);
            String codePeriodicite = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_PERIODICITE);
            Boolean checkedPalier = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_PALIER));
            Boolean checkedAssujettissable = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_ASSUJETTISSABLE));
            Boolean checkPeriodeVariable = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_PERIODE_VARIABLE));
            Boolean checkProprietaire = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_PROPRIETAIRE));
            Boolean checkTauxTransactionnel = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_TAUX_TRANSACTIONNEL));
            Boolean checkQteVariable = Boolean.valueOf(request.getParameter(GestionArticleBudgetaireConst.ParamName.CHECKED_TAUX_TRANSACTIONNEL));
            String codeTarifArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_TARIF_ARTICLE);
            String codeOfficiel = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_OFFICIEL);
            String codeNatureArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_NATURE_ARTICLE);
            String codeDocumentOfficiel = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_DOCUMENT_OFFICIEL);
            String priseChargeDeclaration = request.getParameter(GestionArticleBudgetaireConst.ParamName.PRISE_CHARGE_DECLARATION);
            String priseChargePenalite = request.getParameter(GestionArticleBudgetaireConst.ParamName.PRISE_CHARGE_PENALITE);
            String nbrJourLimite = request.getParameter(GestionArticleBudgetaireConst.ParamName.NBR_JOUR_LIMITE);
            String agentCreat = request.getParameter(GestionArticleBudgetaireConst.ParamName.AGENT_CREAT);
            String codeArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE);

            JSONArray jsonPalierList = new JSONArray(request.getParameter(GestionArticleBudgetaireConst.ParamName.PALIER_LIST));

            ArticleBudgetaire articleBudget = new ArticleBudgetaire();

            articleBudget.setArticleGenerique(new ArticleGenerique(codeArticleGenerique));
            articleBudget.setIntitule(intituleArticle);
            articleBudget.setUnite(new Unite(codeUniteArticle));
            articleBudget.setPalier(checkedPalier);
            articleBudget.setPeriodicite(new Periodicite(codePeriodicite));
            articleBudget.setAssujetissable(checkedAssujettissable);
            articleBudget.setArticleMere(null);
            articleBudget.setAgentCreat(agentCreat);
            articleBudget.setDateCreat(new Date());

            if (checkPeriodeVariable) {
                articleBudget.setPeriodiciteVariable(Short.parseShort(GeneralConst.Number.ONE));
            } else {
                articleBudget.setPeriodiciteVariable(Short.parseShort(GeneralConst.Number.ZERO));
            }

            if (checkProprietaire) {
                articleBudget.setProprietaire(Short.parseShort(GeneralConst.Number.ONE));
            } else {
                articleBudget.setProprietaire(Short.parseShort(GeneralConst.Number.ZERO));
            }

            if (checkTauxTransactionnel) {
                articleBudget.setTransactionnel(Short.parseShort(GeneralConst.Number.ONE));
            } else {
                articleBudget.setTransactionnel(Short.parseShort(GeneralConst.Number.ZERO));
            }

            articleBudget.setQuantiteVariable(checkQteVariable);
            articleBudget.setTarif(new Tarif(codeTarifArticle));
            articleBudget.setCodeOfficiel(codeOfficiel);
            articleBudget.setNature(new NatureArticleBudgetaire(codeNatureArticle));
            articleBudget.setArrete(codeDocumentOfficiel);
            if (!priseChargeDeclaration.equals(GeneralConst.EMPTY_STRING)) {
                articleBudget.setEcheanceLegale(priseChargeDeclaration);
            } else {
                articleBudget.setEcheanceLegale(null);
            }
            if (!priseChargePenalite.equals(GeneralConst.EMPTY_STRING)) {
                articleBudget.setDateLimitePaiement(priseChargePenalite);
            } else {
                articleBudget.setDateLimitePaiement(null);
            }
            articleBudget.setNbrJourLimite(Integer.valueOf(nbrJourLimite));
            articleBudget.setArticleMere(null);
            articleBudget.setDebut(BigDecimal.ZERO);
            articleBudget.setFin(BigDecimal.ZERO);

            if (!codeArticle.equals(GeneralConst.EMPTY_STRING) || !codeArticle.isEmpty()) {
                articleBudget.setCode(codeArticle);
            } else {
                articleBudget.setCode(GeneralConst.EMPTY_STRING);
            }

//            loginWeb.setPassword(propertiesConfig.getProperty("DEFAULT_PASSWORD"));
            List<Palier> palierList = getPalier(jsonPalierList);

            if (GestionArticleBudgetaireBusiness.saveArticleBudgetairePaliers(articleBudget, palierList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<Palier> getPalier(JSONArray jsonPalier) {

        List<Palier> palierList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonPalier.length(); i++) {

                JSONObject jsonobject = jsonPalier.getJSONObject(i);

                Palier palier = new Palier();

                BigDecimal borneMinimale = BigDecimal.valueOf(Double.valueOf(jsonobject.getString(GestionArticleBudgetaireConst.ParamName.BORNE_MINIMALE)));
                BigDecimal borneMaximale = BigDecimal.valueOf(Double.valueOf(jsonobject.getString(GestionArticleBudgetaireConst.ParamName.BORNE_MAXIMALE)));
                BigDecimal taux = BigDecimal.valueOf(Double.valueOf(jsonobject.getString(GestionArticleBudgetaireConst.ParamName.TAUX)));
                String typeTaux = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.TYPE_TAUX);
                String codeTarif = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE_TARIF);
                String formeJuridique = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE_FORME_JURIDIQUE);
                String agentCreat = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.AGENT_CREAT);
                boolean multipliervalBase = Boolean.valueOf(jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CHECKED_VAL_BASE));
                String devise = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE_DEVISE);
                String codeunite = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE_UNITE);
                String codeEntite = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE_QUARTIER);
                String codePalier = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.CODE);
                String etatPalier = jsonobject.getString(GestionArticleBudgetaireConst.ParamName.ETAT);
                String codeNature = jsonobject.getString("codeNature");

                palier.setBorneInferieure(borneMinimale);
                palier.setBorneSuperieure(borneMaximale);
                palier.setTaux(taux);
                palier.setTypeTaux(typeTaux);
                palier.setTarif(new Tarif(codeTarif));
                palier.setTypePersonne(new FormeJuridique(formeJuridique));
                palier.setEtat(Short.parseShort(etatPalier));
                palier.setAgentCreat(agentCreat);
                palier.setDateCreat(new Date());
                palier.setFkTypeBien(codeNature);

                if (multipliervalBase) {
                    palier.setMultiplierValeurBase(Short.parseShort(GeneralConst.Number.ONE));
                } else {
                    palier.setMultiplierValeurBase(Short.parseShort(GeneralConst.Number.ZERO));
                }
                palier.setDevise(new Devise(devise));
                palier.setUnite(new Unite(codeunite));

                if (!codeEntite.equals("null") || !codeEntite.isEmpty()) {
                    palier.setFkEntiteAdministrative(codeEntite);
                    palier.setAgentMaj(agentCreat);
                    palier.setDateMaj(new Date());
                } else {
                    palier.setFkEntiteAdministrative(null);
                    palier.setAgentMaj(null);
                    palier.setDateMaj(null);
                }

                if (!codePalier.equals(GeneralConst.EMPTY_STRING) || !codePalier.isEmpty()) {
                    palier.setCode(Integer.valueOf(codePalier));
                } else {
                    palier.setCode(null);
                }

                palierList.add(palier);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return palierList;
    }

    public String loadBanque(HttpServletRequest request) {

        String codeSite = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_SITE);

        List<JSONObject> jsonBanqueList = new ArrayList<>();
        JSONObject jsonBanque = new JSONObject();

        try {

            List<Banque> banque = GestionArticleBudgetaireBusiness.getBanqueListBySite(codeSite);

            for (Banque bq : banque) {

                jsonBanque = new JSONObject();

                jsonBanque.put(IdentificationConst.ParamName.INTITULE_BANQUE,
                        bq == null
                                ? GeneralConst.EMPTY_STRING
                                : bq.getIntitule());
                jsonBanque.put(IdentificationConst.ParamName.CODE_BANQUE,
                        bq == null
                                ? GeneralConst.EMPTY_STRING
                                : bq.getCode());
                jsonBanque.put(IdentificationConst.ParamName.CODE_SUIFT_BANQUE,
                        bq == null
                                ? GeneralConst.EMPTY_STRING
                                : bq.getCodeSuift());
                jsonBanque.put(IdentificationConst.ParamName.SIGLE_BANQUE,
                        bq == null
                                ? GeneralConst.EMPTY_STRING
                                : bq.getSigle());

                jsonBanqueList.add(jsonBanque);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonBanqueList.toString();
    }

    public String loadCompteBancaire(HttpServletRequest request) {

        String codeBancaire = request.getParameter(IdentificationConst.ParamName.CODE_BANQUE);

        List<JSONObject> jsonCompteBancaireList = new ArrayList<>();
        JSONObject jsonCompteBancaire = new JSONObject();

        try {

            List<CompteBancaire> compteBancaire = GestionArticleBudgetaireBusiness.getCompteByBanque(codeBancaire);

            for (CompteBancaire cptB : compteBancaire) {

                jsonCompteBancaire = new JSONObject();

                jsonCompteBancaire.put(IdentificationConst.ParamName.INTITULE_COMPTE_BANQUE,
                        cptB == null
                                ? GeneralConst.EMPTY_STRING
                                : cptB.getIntitule());
                jsonCompteBancaire.put(IdentificationConst.ParamName.CODE_COMPTE_BANQUE,
                        cptB == null
                                ? GeneralConst.EMPTY_STRING
                                : cptB.getCode());

                jsonCompteBancaire.put(IdentificationConst.ParamName.DEVISE_COMPTE_BANQUE,
                        cptB == null
                                ? GeneralConst.EMPTY_STRING
                                : cptB.getDevise() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : cptB.getDevise().getCode());

                jsonCompteBancaireList.add(jsonCompteBancaire);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonCompteBancaireList.toString();
    }

    public String loadArticleAffecterBanque(HttpServletRequest request) {

        String codeCompteBancaire = request.getParameter(IdentificationConst.ParamName.CODE_COMPTE_BANQUE);

        List<JSONObject> jsonArticleList = new ArrayList<>();
        JSONObject jsonArticle = new JSONObject();

        try {

            List<BanqueAb> articleList = GestionArticleBudgetaireBusiness.getArticleBanqueByCompteBancaire(codeCompteBancaire.trim());

            for (BanqueAb banqAb : articleList) {

                jsonArticle = new JSONObject();

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.BANQUE_ARTICLE_CODE,
                        banqAb == null
                                ? GeneralConst.EMPTY_STRING
                                : banqAb.getId());
                jsonArticle.put(IdentificationConst.ParamName.CODE_BANQUE,
                        banqAb == null
                                ? GeneralConst.EMPTY_STRING
                                : banqAb.getFkBanque());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_COMPTE_BANCAIRE,
                        banqAb == null
                                ? GeneralConst.EMPTY_STRING
                                : banqAb.getFkCompte() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : banqAb.getFkCompte().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE,
                        banqAb == null
                                ? GeneralConst.EMPTY_STRING
                                : banqAb.getFkAb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : banqAb.getFkAb().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE,
                        banqAb == null
                                ? GeneralConst.EMPTY_STRING
                                : banqAb.getFkAb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : banqAb.getFkAb().getCode());

                jsonArticleList.add(jsonArticle);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticleList.toString();
    }

    private String getServiceAssiettes(HttpServletRequest request) {
        String result = GeneralConst.EMPTY_STRING;
        List<JSONObject> jSONObject = new ArrayList<>();
        List<Service> services = new ArrayList<>();

        try {
            String code = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE);

            services = GestionArticleBudgetaireBusiness.loadServiceByMinistre(code);
            if (!services.isEmpty()) {
                for (Service service : services) {

                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadArticleBudgetairesByService(HttpServletRequest request) {

        String result;
        String codeService = request.getParameter("codeService");
        String compteBancaire = request.getParameter("codeCompteBancaire");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.loadArticleBudgetaireByService(codeService, compteBancaire);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {

                    JSONObject object = new JSONObject();

                    object.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE,
                            article == null
                                    ? GeneralConst.EMPTY_STRING
                                    : article.getIntitule());

                    object.put(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE,
                            article == null
                                    ? GeneralConst.EMPTY_STRING
                                    : article.getCode());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String affecterArticleCompte(HttpServletRequest request) {

        String codeCompteBancaire = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_COMPTE_BANCAIRE);
        String codeBanque = request.getParameter(IdentificationConst.ParamName.CODE_BANQUE);
        String articleList = request.getParameter(IdentificationConst.ParamName.DROIT_LIST);
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(articleList);
            List<BanqueAb> listBanqueAb = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                BanqueAb banqueab = new BanqueAb();
                String codeArticle = jsonObject.getString(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE);

                banqueab.setFkAb(new ArticleBudgetaire(codeArticle));
                banqueab.setAgentCreat(idUser);
                banqueab.setDateCreat(new Date());
                banqueab.setEtat(true);
                banqueab.setFkCompte(new CompteBancaire(codeCompteBancaire));
                banqueab.setFkBanque(codeBanque);

                listBanqueAb.add(banqueab);
            }

            boolean result = GestionArticleBudgetaireBusiness.affecterArticleCompte(listBanqueAb);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String desaffecterArticleCompte(HttpServletRequest request) {

        String articleList = request.getParameter(IdentificationConst.ParamName.DROIT_LIST);
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(articleList);
            List<BanqueAb> listBanqueAb = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                BanqueAb banqueab = new BanqueAb();
                String codeArticleBanque = jsonObject.getString(GestionArticleBudgetaireConst.ParamName.BANQUE_ARTICLE_CODE);

                banqueab.setId(Integer.valueOf(codeArticleBanque));

                listBanqueAb.add(banqueab);
            }

            boolean result = GestionArticleBudgetaireBusiness.desaffecterArticleCompte(listBanqueAb);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadTarifs(HttpServletRequest request) {

        List<JSONObject> jsonTarifList = new ArrayList<>();

        try {
            
            String userId = request.getParameter("userId");

            Agent agent = GeneralBusiness.getAgentByCode(userId);
            
            List<Tarif> tarifs = GestionArticleBudgetaireBusiness.getTarif(agent.getSite().getPeage());

            for (Tarif tarif : tarifs) {
                JSONObject jsonTarif = new JSONObject();

                jsonTarif.put(AssujettissementConst.ParamName.CODE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getCode());

                jsonTarif.put(AssujettissementConst.ParamName.INTITULE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getIntitule());

                jsonTarif.put(AssujettissementConst.ParamName.ETAT,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getEtat());

                jsonTarif.put(AssujettissementConst.ParamName.VALEUR_BASE,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getValeur());

                jsonTarif.put("TYPE_VALEUR_NAME",
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : (tarif.getTypeValeur().equals("%"))
                                        ? "En pourcentage"
                                        : "Fixe");

                jsonTarif.put("TYPE_VALEUR_CODE",
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : (tarif.getTypeValeur().equals("%"))
                                        ? "(%)"
                                        : "");

                jsonTarif.put(AssujettissementConst.ParamName.AGENT_CREAT,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getAgentCreat());

                jsonTarif.put(AssujettissementConst.ParamName.VALEUR_TYPE,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getTypeValeur());

                jsonTarif.put("TYPE_TARIF_CODE",
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getEstTarifPeage());

                jsonTarif.put("TYPE_TARIF_NAME",
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : (tarif.getEstTarifPeage() == 0)
                                        ? "Est tarif normal"
                                        : "Est tarif péage");

                jsonTarifList.add(jsonTarif);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonTarifList.toString();
    }

    public String saveTarif(HttpServletRequest request) {

        String libelleTarif = request.getParameter(AssujettissementConst.ParamName.INTITULE_TARIF);
        String valeur = request.getParameter(AssujettissementConst.ParamName.VALEUR_BASE);
        String typeValeur = request.getParameter(AssujettissementConst.ParamName.TYPE_VALEUR);
        String typeTarif = request.getParameter("typeTarif");
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            Tarif tarif = new Tarif();

            tarif.setIntitule(libelleTarif);
            tarif.setTypeValeur(typeValeur);
            tarif.setAgentCreat(idUser);
            tarif.setEtat(Short.parseShort(GeneralConst.Number.ONE));
            tarif.setDateCreat(new Date());
            tarif.setValeur(BigDecimal.valueOf(Double.valueOf(valeur)));
            tarif.setEstTarifPeage(Integer.valueOf(typeTarif));

            boolean result = GestionArticleBudgetaireBusiness.saveTarif(tarif);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String disableTarif(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeTarif = request.getParameter(AssujettissementConst.ParamName.CODE_TARIF);

            if (GestionArticleBudgetaireBusiness.disableTarif(codeTarif)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String updateTarif(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String libelleTarif = request.getParameter(AssujettissementConst.ParamName.INTITULE_TARIF);
            String valeur = request.getParameter(AssujettissementConst.ParamName.VALEUR_BASE);
            String typeValeur = request.getParameter(AssujettissementConst.ParamName.TYPE_VALEUR);
            String idUser = request.getParameter(GeneralConst.ID_USER);
            String codeTarif = request.getParameter(AssujettissementConst.ParamName.CODE_TARIF);
            String typeTarif = request.getParameter("typeTarif");

            Tarif tarif = new Tarif();

            tarif.setIntitule(libelleTarif);
            tarif.setTypeValeur(typeValeur);
            tarif.setAgentMaj(idUser);
            tarif.setDateMaj(new Date());
            tarif.setValeur(BigDecimal.valueOf(Double.valueOf(valeur)));
            tarif.setCode(codeTarif);
            tarif.setEstTarifPeage(Integer.valueOf(typeTarif));

            if (GestionArticleBudgetaireBusiness.updateTarif(tarif)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadPeriodicite() {

        List<JSONObject> jsonPeriodiciteList = new ArrayList<>();

        try {

            List<Periodicite> periodicites = GestionArticleBudgetaireBusiness.getPeriodiciteList();

            for (Periodicite period : periodicites) {

                JSONObject jsonPeriodicite = new JSONObject();

                jsonPeriodicite.put(AssujettissementConst.ParamName.CODE_PERIODICITE,
                        period == null
                                ? GeneralConst.EMPTY_STRING
                                : period.getCode());

                jsonPeriodicite.put(AssujettissementConst.ParamName.INTITULE_PERIODICITE,
                        period == null
                                ? GeneralConst.EMPTY_STRING
                                : period.getIntitule());

                jsonPeriodicite.put(AssujettissementConst.ParamName.ETAT,
                        period == null
                                ? GeneralConst.EMPTY_STRING
                                : period.getEtat());

                jsonPeriodicite.put(AssujettissementConst.ParamName.NOMBRE_JOUR,
                        period == null
                                ? GeneralConst.EMPTY_STRING
                                : period.getNbrJour());

                jsonPeriodicite.put(AssujettissementConst.ParamName.PERIODE_RECIDIVE,
                        period == null
                                ? GeneralConst.EMPTY_STRING
                                : period.getPeriodeRecidive());

                jsonPeriodiciteList.add(jsonPeriodicite);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPeriodiciteList.toString();
    }

    public String savePeriodicite(HttpServletRequest request) {

        String libellePeriodicite = request.getParameter(AssujettissementConst.ParamName.INTITULE_PERIODICITE);
        String nbrJour = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR);
        String periodeRecidive = request.getParameter(AssujettissementConst.ParamName.PERIODE_RECIDIVE);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            Periodicite period = new Periodicite();

            period.setIntitule(libellePeriodicite);
            period.setNbrJour(Integer.valueOf(nbrJour));
            period.setPeriodeRecidive(Integer.valueOf(periodeRecidive));

            boolean result = GestionArticleBudgetaireBusiness.savePeriodicite(period);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updatePeriodicte(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String libellePeriodicite = request.getParameter(AssujettissementConst.ParamName.INTITULE_PERIODICITE);
            String nbrJour = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR);
            String periodeRecidive = request.getParameter(AssujettissementConst.ParamName.PERIODE_RECIDIVE);
            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE);

            Periodicite period = new Periodicite();

            period.setIntitule(libellePeriodicite);
            period.setNbrJour(Integer.valueOf(nbrJour));
            period.setPeriodeRecidive(Integer.valueOf(periodeRecidive));
            period.setCode(codePeriodicite);

            if (GestionArticleBudgetaireBusiness.updatePeriodicite(period)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String disablePeriodicite(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE);

            if (GestionArticleBudgetaireBusiness.disablePeriodicite(codePeriodicite)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String getInitDataTarifSite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Site> listSite = TaxationBusiness.getListAllSitePeage();
            List<Tarif> listTarif = GestionArticleBudgetaireBusiness.getListTarifPeage();

            List<JsonObject> siteAJsonList = new ArrayList<>();
            List<JsonObject> siteBJsonList = new ArrayList<>();
            List<JsonObject> tarifJsonList = new ArrayList<>();

            JsonObject initDataJson = new JsonObject();

            if (!listSite.isEmpty()) {

                for (Site siteA : listSite) {

                    JsonObject siteAJson = new JsonObject();

                    siteAJson.addProperty("siteCode", siteA.getCode());
                    siteAJson.addProperty("siteName", siteA.getIntitule().toUpperCase());

                    siteAJsonList.add(siteAJson);
                }

                for (Site siteB : listSite) {

                    JsonObject siteBJson = new JsonObject();

                    siteBJson.addProperty("siteCode", siteB.getCode());
                    siteBJson.addProperty("siteName", siteB.getIntitule().toUpperCase());

                    siteBJsonList.add(siteBJson);
                }

                initDataJson.addProperty("siteProvenanceList", siteAJsonList.toString());
                initDataJson.addProperty("siteDestinationList", siteBJsonList.toString());

            } else {

                initDataJson.addProperty("siteProvenanceList", GeneralConst.EMPTY_STRING);
                initDataJson.addProperty("siteDestinationList", GeneralConst.EMPTY_STRING);
            }

            if (!listTarif.isEmpty()) {

                for (Tarif tarif : listTarif) {

                    JsonObject tarifJson = new JsonObject();

                    tarifJson.addProperty("tarifCode", tarif.getCode());
                    tarifJson.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                    tarifJsonList.add(tarifJson);
                }

                initDataJson.addProperty("tarifList", tarifJsonList.toString());

            } else {
                initDataJson.addProperty("tarifList", GeneralConst.EMPTY_STRING);
            }

            dataReturn = initDataJson.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListTarifsBySite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<TarifSite> listTarifSites = GestionArticleBudgetaireBusiness.getListTarifSite();

            List<JsonObject> tarifSiteJsonList = new ArrayList<>();

            if (!listTarifSites.isEmpty()) {

                for (TarifSite tarifSite : listTarifSites) {

                    Site siteA = new Site();
                    Site siteB = new Site();
                    Tarif tarif = new Tarif();

                    JsonObject tarifSiteJson = new JsonObject();

                    tarifSiteJson.addProperty("id", tarifSite.getId());

                    if (tarifSite.getFkSiteProvenance() != null && !tarifSite.getFkSiteProvenance().isEmpty()) {

                        siteA = TaxationBusiness.getSitePeageByCode(tarifSite.getFkSiteProvenance().trim());
                        tarifSiteJson.addProperty("siteProvenanaceCode", siteA.getCode());
                        tarifSiteJson.addProperty("siteProvenanaceName", siteA.getIntitule().toUpperCase());

                    } else {
                        tarifSiteJson.addProperty("siteProvenanaceCode", GeneralConst.EMPTY_STRING);
                        tarifSiteJson.addProperty("siteProvenanaceName", GeneralConst.EMPTY_STRING);
                    }

                    if (tarifSite.getFkSiteDestination() != null && !tarifSite.getFkSiteDestination().isEmpty()) {

                        siteB = TaxationBusiness.getSitePeageByCode(tarifSite.getFkSiteDestination().trim());
                        tarifSiteJson.addProperty("siteDestinantionCode", siteB.getCode());
                        tarifSiteJson.addProperty("siteDestinantionName", siteB.getIntitule().toUpperCase());

                    } else {
                        tarifSiteJson.addProperty("siteDestinantionCode", GeneralConst.EMPTY_STRING);
                        tarifSiteJson.addProperty("siteDestinantionName", GeneralConst.EMPTY_STRING);

                    }

                    if (tarifSite.getFkTarif() != null && !tarifSite.getFkTarif().isEmpty()) {

                        tarif = TaxationBusiness.getTarifByCode(tarifSite.getFkTarif());
                        tarifSiteJson.addProperty("tarifName", tarif.getIntitule().toUpperCase());
                        tarifSiteJson.addProperty("tarifCode", tarif.getCode());
                    } else {
                        tarifSiteJson.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                        tarifSiteJson.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                    }

                    if (tarifSite.getFkDevise() != null && !tarifSite.getFkDevise().isEmpty()) {
                        tarifSiteJson.addProperty("devise", tarifSite.getFkDevise());
                    } else {
                        tarifSiteJson.addProperty("devise", GeneralConst.EMPTY_STRING);
                    }

                    tarifSiteJson.addProperty("taux", tarifSite.getTaux());

                    tarifSiteJsonList.add(tarifSiteJson);

                }

                dataReturn = tarifSiteJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadUnite() {

        List<JSONObject> jsonUniteList = new ArrayList<>();

        try {

            List<Unite> unites = GestionArticleBudgetaireBusiness.getUnite();

            for (Unite unite : unites) {
                JSONObject jsonUnite = new JSONObject();

                jsonUnite.put(AssujettissementConst.ParamName.CODE_UNITE,
                        unite == null
                                ? GeneralConst.EMPTY_STRING
                                : unite.getCode());

                jsonUnite.put(AssujettissementConst.ParamName.INTITULE_UNITE,
                        unite == null
                                ? GeneralConst.EMPTY_STRING
                                : unite.getIntitule());

                jsonUnite.put(AssujettissementConst.ParamName.ETAT,
                        unite == null
                                ? GeneralConst.EMPTY_STRING
                                : unite.getEtat());

                jsonUniteList.add(jsonUnite);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonUniteList.toString();
    }

    public String saveUnite(HttpServletRequest request) {

        String libelleUnite = request.getParameter(AssujettissementConst.ParamName.INTITULE_UNITE);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String selectImpo = request.getParameter("selectImpot");
            String selectExercice = request.getParameter("selectExercice");
            String inputEcheanceLegaleDeclaration = request.getParameter("inputEcheanceLegaleDeclaration");
            String inputEcheanceLegaleDeclarationProrogation = request.getParameter("inputEcheanceLegaleDeclarationProrogation");
            String inputEcheanceLegalePaiement = request.getParameter("inputEcheanceLegalePaiement");
            String inputEcheanceLegalePaiementProrogation = request.getParameter("inputEcheanceLegalePaiementProrogation");
            String userId = request.getParameter("idUser");
            String inputMotifProrogation = request.getParameter("inputMotifProrogation");

            ProrogationEcheanceLegaleDeclaration prorogationEcheance = new ProrogationEcheanceLegaleDeclaration();

            boolean isSaved = false;

            if (id.isEmpty()) {
                isSaved = true;
            } else {
                prorogationEcheance.setId(Integer.valueOf(id));
            }

            prorogationEcheance.setAgentCreate(Integer.valueOf(userId));
            prorogationEcheance.setExercice(selectExercice);
            prorogationEcheance.setFkArticleBudgetaire(selectImpo);
            prorogationEcheance.setDateDeclarationLegale(inputEcheanceLegaleDeclaration);
            prorogationEcheance.setDateDeclarationProrogee(inputEcheanceLegaleDeclarationProrogation);
            prorogationEcheance.setDateLimitePaiement(inputEcheanceLegalePaiement);
            prorogationEcheance.setDateLimitePaiementProrogee(inputEcheanceLegalePaiementProrogation);
            prorogationEcheance.setMotif(inputMotifProrogation);

            boolean result = GestionArticleBudgetaireBusiness.saveProrogation(prorogationEcheance, isSaved);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveProrogation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String selectImpo = request.getParameter("selectImpot");
            String selectExercice = request.getParameter("selectExercice");
            String inputEcheanceLegaleDeclaration = request.getParameter("inputEcheanceLegaleDeclaration");
            String inputEcheanceLegaleDeclarationProrogation = request.getParameter("inputEcheanceLegaleDeclarationProrogation");
            String inputEcheanceLegalePaiement = request.getParameter("inputEcheanceLegalePaiement");
            String inputEcheanceLegalePaiementProrogation = request.getParameter("inputEcheanceLegalePaiementProrogation");
            String userId = request.getParameter("userId");
            String inputMotifProrogation = request.getParameter("inputMotifProrogation");

            ProrogationEcheanceLegaleDeclaration prorogationEcheance = new ProrogationEcheanceLegaleDeclaration();

            boolean isSaved = false;

            if (id.isEmpty()) {
                isSaved = true;
            } else {
                prorogationEcheance.setId(Integer.valueOf(id));
            }

            prorogationEcheance.setAgentCreate(Integer.valueOf(userId));
            prorogationEcheance.setAgentMaj(Integer.valueOf(userId));
            prorogationEcheance.setExercice(selectExercice);
            prorogationEcheance.setFkArticleBudgetaire(selectImpo);
            prorogationEcheance.setDateDeclarationLegale(inputEcheanceLegaleDeclaration);
            prorogationEcheance.setDateDeclarationProrogee(inputEcheanceLegaleDeclarationProrogation);
            prorogationEcheance.setDateLimitePaiement(inputEcheanceLegalePaiement);
            prorogationEcheance.setDateLimitePaiementProrogee(inputEcheanceLegalePaiementProrogation);
            prorogationEcheance.setMotif(inputMotifProrogation);

            boolean result = GestionArticleBudgetaireBusiness.saveProrogation(prorogationEcheance, isSaved);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteProrogation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String userId = request.getParameter("userId");
            String inputMotifProrogation = request.getParameter("inputMotifProrogation");
            String selectImpot = request.getParameter("selectImpot");
            String selectExercice = request.getParameter("selectExercice");

            ProrogationEcheanceLegaleDeclaration prorogationEcheance = new ProrogationEcheanceLegaleDeclaration();

            prorogationEcheance.setAgentMaj(Integer.valueOf(userId));
            prorogationEcheance.setExercice(selectExercice);
            prorogationEcheance.setFkArticleBudgetaire(selectImpot);
            prorogationEcheance.setMotif(inputMotifProrogation);
            prorogationEcheance.setId(Integer.valueOf(id));

            boolean result = GestionArticleBudgetaireBusiness.deleteProrogation(prorogationEcheance);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String updateUnite(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String libelleUnite = request.getParameter(AssujettissementConst.ParamName.INTITULE_UNITE);
            String codeUnite = request.getParameter(AssujettissementConst.ParamName.CODE_UNITE);

            Unite unite = new Unite();

            unite.setIntitule(libelleUnite);
            unite.setCode(codeUnite);

            if (GestionArticleBudgetaireBusiness.updateUnite(unite)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String disableUnite(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String codeUnite = request.getParameter(AssujettissementConst.ParamName.CODE_UNITE);

            if (GestionArticleBudgetaireBusiness.disableUnite(codeUnite)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadInfosArticleBudgetaire(HttpServletRequest request) {

        JSONObject jsonArticle = new JSONObject();

        String natureArticleBudgetaire;

        try {
            String codeArticle = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE);

            ArticleBudgetaire article = GestionArticleBudgetaireBusiness.loadInfosArticleBudgetaire(codeArticle);

            if (article != null) {

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE_GENERIQUE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getArticleGenerique().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE_GENERIQUE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getArticleGenerique().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_SERVICE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getArticleGenerique().getServiceAssiette() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : article.getArticleGenerique().getServiceAssiette().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_UNITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getUnite() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getUnite().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_UNITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getUnite() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getUnite().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.VALEUR_PALIER,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getPalier());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_PERIODICITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getPeriodicite() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getPeriodicite().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_PERIODICITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getPeriodicite() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getPeriodicite().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.ASSUJETTISABLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getAssujetissable());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE_MERE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getArticleMere());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_TARIF_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getTarif() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getTarif().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.VALEUR_TARIF_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getTarif() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getTarif().getValeur());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_TARIF_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getTarif() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getTarif().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.TARIF_VARIABLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getTarifVariable());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.ETAT,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getEtat());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.AGENT_CREAT,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getAgentCreat());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.DATE_CREAT,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : Tools.formatDateToStringV2(article.getDateCreat()));

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.AGENT_MAJ,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getAgentMaj());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.DATE_MAJ,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : Tools.formatDateToStringV2(article.getDateMaj()));

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PROPRIETAIRE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getProprietaire());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.TRANSACTIONNEL,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getTransactionnel());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PERIODE_DEBUT,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getDebut());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PERIODE_FIN,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getFin());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PERIODICITE_VARIABLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getPeriodiciteVariable());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.NBR_JOUR_LIMITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getNbrJourLimite());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_NATURE_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getNature() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getNature().getCode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_NATURE_ARTICLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getNature() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : article.getNature().getIntitule());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.QUANTITE_VARIABLE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getQuantiteVariable());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_DOCUMENT_OFFICIEL,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getArrete());

                if (article.getArrete() != null) {

                    DocumentOfficiel document = GestionArticleBudgetaireBusiness.loadDocumentOfficielByCode(article.getArrete());

                    if (document != null) {
                        jsonArticle.put(GestionArticleBudgetaireConst.ParamName.INTITULE_DOCUMENT_OFFICIEL,
                                document.getIntitule());

                        jsonArticle.put(GestionArticleBudgetaireConst.ParamName.NUMERO_ARRETE,
                                document.getNumero());
                    }
                }

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CODE_OFFICIEL,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getCodeOfficiel());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.DATE_DEBUT_PERIODE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getDateDebutPeriode());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.DATE_DEBUT_PENALITE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getDateDebutPenalite());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.ECHEANCE_LEGALE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getEcheanceLegale());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.DATE_LIMITE_PAIEMENT,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getDateLimitePaiement());

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PERIODE_ECHEANCE,
                        article == null
                                ? GeneralConst.EMPTY_STRING
                                : article.getPeriodeEcheance());

                natureArticleBudgetaire = propertiesConfig.getProperty("NATURE_ARTICLE");

                jsonArticle.put(GestionArticleBudgetaireConst.ParamName.CHECK_NATURE_ARTICLE, natureArticleBudgetaire);

                List<Palier> palierList = GestionArticleBudgetaireBusiness.loadPalierByArticle(codeArticle);

                if (palierList.size() > 0) {

                    List<JSONObject> jSONObjectPalierList = new ArrayList<>();

                    for (Palier palier : palierList) {

                        JSONObject objectPalier = new JSONObject();

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getCode());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.BORNE_MINIMALE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getBorneInferieure());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.BORNE_MAXIMALE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getBorneSuperieure());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.TAUX,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTaux());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.TYPE_TAUX,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTypeTaux());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.AGENT_CREAT,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getAgentCreat());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_TARIF,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTarif() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getTarif().getCode());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.TARIF,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTarif() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getTarif().getIntitule());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_FORME_JURIDIQUE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTypePersonne() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getTypePersonne().getIntitule());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_FORME_JURIDIQUE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getTypePersonne() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getTypePersonne().getCode());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CHECKED_VAL_BASE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getMultiplierValeurBase() == 0
                                                ? false
                                                : true);

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.DEVISE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getDevise() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getDevise().getIntitule());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_DEVISE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getDevise() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getDevise().getCode());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_UNITE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getUnite() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getUnite().getCode());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.UNITE,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getUnite() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getUnite().getIntitule());

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.ETAT,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getEtat());

                        if (palier.getFkTypeBien() != null) {

                            TypeBien typeBien = GestionArticleBudgetaireBusiness.getTypeBienByCode(palier.getFkTypeBien());
//                            String a;

                            if (typeBien != null) {
                                objectPalier.put("idNature", typeBien.getCode());
                                objectPalier.put("intituleNature", typeBien.getIntitule().toUpperCase());
                            } else {
                                objectPalier.put("idNature", GeneralConst.EMPTY_STRING);
                                objectPalier.put("intituleNature", GeneralConst.EMPTY_STRING);
                            }

                        } else {
                            objectPalier.put("idNature", GeneralConst.EMPTY_STRING);
                            objectPalier.put("intituleNature", GeneralConst.EMPTY_STRING);
                        }

                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_QUARTIER,
                                palier == null
                                        ? GeneralConst.EMPTY_STRING
                                        : palier.getFkEntiteAdministrative() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : palier.getFkEntiteAdministrative());

                        String checkEntite = palier.getFkEntiteAdministrative();

                        if (checkEntite == null) {
                            //checkEntite = null;
                        } else if (checkEntite.equals("null")) {
                            checkEntite = null;
                        } else if (checkEntite.isEmpty()) {
                            checkEntite = null;
                        }

                        if (checkEntite != null) {

                            EntiteAdministrative entiteAdmin = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByCode(palier.getFkEntiteAdministrative());

                            if (entiteAdmin != null) {
                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER,
                                        entiteAdmin == null
                                                ? GeneralConst.EMPTY_STRING
                                                : entiteAdmin.getIntitule());

                                if (entiteAdmin.getEntiteMere().getCode() != null) {
                                    objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE,
                                            entiteAdmin.getEntiteMere().getCode());

                                    EntiteAdministrative entiteDistrict = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByCode(entiteAdmin.getEntiteMere().getCode());

                                    if (entiteDistrict != null) {
                                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE,
                                                entiteDistrict == null
                                                        ? GeneralConst.EMPTY_STRING
                                                        : entiteDistrict.getIntitule());

                                        if (entiteDistrict.getEntiteMere().getCode() != null) {
                                            objectPalier.put("codeDistrict",
                                                    entiteDistrict.getEntiteMere().getCode());

                                            EntiteAdministrative entiteVille = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByCode(entiteDistrict.getEntiteMere().getCode());

                                            if (entiteVille != null) {
                                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE,
                                                        entiteVille.getEntiteMere().getCode());
                                            } else {
                                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                            }
                                        } else {
                                            objectPalier.put("codeDistrict", GeneralConst.EMPTY_STRING);
                                            objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                        }
                                    } else {
                                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, GeneralConst.EMPTY_STRING);
                                        objectPalier.put("codeDistrict", GeneralConst.EMPTY_STRING);
                                        objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE, GeneralConst.EMPTY_STRING);
                                    objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, GeneralConst.EMPTY_STRING);
                                    objectPalier.put("codeDistrict", GeneralConst.EMPTY_STRING);
                                    objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                }

                            } else {
                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER, GeneralConst.EMPTY_STRING);
                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE, GeneralConst.EMPTY_STRING);
                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, GeneralConst.EMPTY_STRING);
                                objectPalier.put("codeDistrict", GeneralConst.EMPTY_STRING);
                                objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                            }
                        } else {
                            objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER, GeneralConst.EMPTY_STRING);
                            objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE, GeneralConst.EMPTY_STRING);
                            objectPalier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, GeneralConst.EMPTY_STRING);
                            objectPalier.put("codeDistrict", GeneralConst.EMPTY_STRING);
                            objectPalier.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                        }

                        jSONObjectPalierList.add(objectPalier);

                    }

                    jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PALIER_LIST, jSONObjectPalierList.toString());

                } else {
                    jsonArticle.put(GestionArticleBudgetaireConst.ParamName.PALIER_LIST, GeneralConst.EMPTY_STRING);
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticle.toString();
    }

    private String getArticleBudgetaireByService(HttpServletRequest request) {

        String result;
        String codeService = request.getParameter("codeService");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.getArticleBudgetaireByService(codeService);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {

                    JSONObject object = new JSONObject();

                    object.put("intitule", article.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getCodeOfficiel());
                    object.put("faitgenerateur", article.getArticleGenerique() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getArticleGenerique().getIntitule());
                    object.put("intituleperiode", article.getPeriodicite() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getPeriodicite().getIntitule());
                    object.put("codeArticle", article.getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getArticleBudgetaireByIntitule(HttpServletRequest request) {

        String result;
        String intituleArticle = request.getParameter("intituleArticle");
        String codeMinistere = request.getParameter("codeMinistere");
        String codeService = request.getParameter("codeService");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.getArticleBudgetaireByIntitule(
                    intituleArticle, codeService, codeMinistere);

            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {

                    JSONObject object = new JSONObject();

                    object.put("intitule", article.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getCodeOfficiel());
                    object.put("faitgenerateur", article.getArticleGenerique() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getArticleGenerique().getIntitule());
                    object.put("intituleperiode", article.getPeriodicite() == null
                            ? GeneralConst.EMPTY_STRING
                            : article.getPeriodicite().getIntitule());
                    object.put("codeArticle", article.getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadEntiteAdministrativeFille(HttpServletRequest request) {

        String result = null;

        try {

            String codeMere = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE);
            String niveau = request.getParameter("niveau");

            List<EntiteAdministrative> communeList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeFille(codeMere);

            if (!communeList.isEmpty()) {

                List<JSONObject> jsonVilleList = new ArrayList<>();

                for (EntiteAdministrative entite : communeList) {

                    JSONObject jsonVille = new JSONObject();

                    if (niveau.equals("1")) {
                        jsonVille.put(GestionArticleBudgetaireConst.ParamName.CODE_COMMUNE, entite.getCode());
                        jsonVille.put(GestionArticleBudgetaireConst.ParamName.INTITULE_COMMUNE, entite.getIntitule());
                    } else {
                        jsonVille.put(GestionArticleBudgetaireConst.ParamName.CODE_QUARTIER, entite.getCode());
                        jsonVille.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER, entite.getIntitule());
                    }

                    jsonVilleList.add(jsonVille);
                }
                result = jsonVilleList.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String loadEntiteAdministrativeFille2(HttpServletRequest request) {

        String result = null;

        try {

            String codeMere = request.getParameter(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE);
            String niveau = request.getParameter("niveau");

            List<EntiteAdministrative> quartierList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeFilleV2(codeMere);

            if (!quartierList.isEmpty()) {

                List<JSONObject> jsonQuartierList = new ArrayList<>();

                for (EntiteAdministrative quartier : quartierList) {

                    JSONObject jsonQuartier = new JSONObject();

                    jsonQuartier.put(GestionArticleBudgetaireConst.ParamName.CODE_QUARTIER, quartier.getCode());
                    jsonQuartier.put(GestionArticleBudgetaireConst.ParamName.INTITULE_QUARTIER, quartier.getIntitule().toUpperCase());

                    jsonQuartierList.add(jsonQuartier);
                }

                result = jsonQuartierList.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String initDataProrogationEcheanceLegale(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<ArticleBudgetaire> listArticleBudgetaire = GestionArticleBudgetaireBusiness.getLstArticleBudgetaireAssujetti();
            List<ExerciceFiscale> listExerciceFiscale = GestionArticleBudgetaireBusiness.getLstExerciceFiscale();

            List<JsonObject> abJsonList = new ArrayList<>();
            List<JsonObject> exerciceJsonList = new ArrayList<>();

            if (!listArticleBudgetaire.isEmpty()) {

                for (ArticleBudgetaire ab : listArticleBudgetaire) {

                    JsonObject abJson = new JsonObject();

                    abJson.addProperty("codeAb", ab.getCode());
                    abJson.addProperty("libelleAb", ab.getIntitule().toUpperCase());

                    if (ab.getEcheanceLegale() != null) {
                        abJson.addProperty("echeanceLegaleDeclarationAb", ab.getEcheanceLegale());
                    } else {
                        abJson.addProperty("echeanceLegaleDeclarationAb", "Non définie");
                    }

                    if (ab.getDateLimitePaiement() != null) {
                        abJson.addProperty("echeanceLegalePaiementAb", ab.getDateLimitePaiement());
                    } else {
                        abJson.addProperty("echeanceLegalePaiementAb", "Non définie");
                    }

                    abJsonList.add(abJson);

                }
            }

            if (!listExerciceFiscale.isEmpty()) {

                for (ExerciceFiscale ex : listExerciceFiscale) {

                    JsonObject exJson = new JsonObject();

                    exJson.addProperty("code", ex.getCode());

                    exerciceJsonList.add(exJson);
                }
            }

            JsonObject jsonInitData = new JsonObject();

            if (!abJsonList.isEmpty()) {
                jsonInitData.addProperty("abList", abJsonList.toString());
            } else {
                jsonInitData.addProperty("abList", GeneralConst.EMPTY_STRING);
            }

            if (!exerciceJsonList.isEmpty()) {
                jsonInitData.addProperty("exerciceFiscaleList", exerciceJsonList.toString());
            } else {
                jsonInitData.addProperty("exerciceFiscaleList", GeneralConst.EMPTY_STRING);
            }

            List<JsonObject> jsonInitDataList = new ArrayList<>();

            jsonInitDataList.add(jsonInitData);

            dataReturn = jsonInitDataList.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadProrogations(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<ProrogationEcheanceLegaleDeclaration> prorogationEcheanceLegaleDeclarations = GestionArticleBudgetaireBusiness.getListProrogationEcheanceLegaleDeclaration();

            List<JsonObject> prorogationJsonList = new ArrayList<>();

            if (prorogationEcheanceLegaleDeclarations.size() > 0) {

                for (ProrogationEcheanceLegaleDeclaration prorogation : prorogationEcheanceLegaleDeclarations) {

                    JsonObject prorogationJson = new JsonObject();

                    ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(prorogation.getFkArticleBudgetaire());

                    prorogationJson.addProperty("codeAb", ab.getCode());
                    prorogationJson.addProperty("libelleAb", ab.getIntitule().toUpperCase());

                    if (!ab.getEcheanceLegale().isEmpty() && ab.getEcheanceLegale() != null) {
                        prorogationJson.addProperty("echeanceLegaleDeclarationAbF", ConvertDate.getValidFormatDatePrintV2(ab.getEcheanceLegale()));
                        prorogationJson.addProperty("echeanceLegaleDeclarationAb", ab.getEcheanceLegale());
                    } else {
                        prorogationJson.addProperty("echeanceLegaleDeclarationAb", GeneralConst.EMPTY_STRING);
                    }

                    if (!ab.getDateLimitePaiement().isEmpty() && ab.getDateLimitePaiement() != null) {
                        prorogationJson.addProperty("echeanceLegalePaiementAbF", ConvertDate.getValidFormatDatePrintV2(ab.getDateLimitePaiement()));
                        prorogationJson.addProperty("echeanceLegalePaiementAb", ab.getDateLimitePaiement());
                    } else {
                        prorogationJson.addProperty("echeanceLegalePaiementAb", GeneralConst.EMPTY_STRING);
                    }

                    prorogationJson.addProperty("echeanceLegaleDeclarationProrogeeF", ConvertDate.getValidFormatDatePrintV2(prorogation.getDateDeclarationProrogee()));
                    prorogationJson.addProperty("echeanceLegaleDeclarationProrogee", prorogation.getDateDeclarationProrogee());
                    prorogationJson.addProperty("echeanceLegalePaiementProrogeeF", ConvertDate.getValidFormatDatePrintV2(prorogation.getDateLimitePaiementProrogee()));
                    prorogationJson.addProperty("echeanceLegalePaiementProrogee", prorogation.getDateLimitePaiementProrogee());
                    prorogationJson.addProperty("motif", prorogation.getMotif());
                    prorogationJson.addProperty("exercice", prorogation.getExercice());
                    prorogationJson.addProperty("id", prorogation.getId());

                    prorogationJsonList.add(prorogationJson);

                }

                dataReturn = prorogationJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private String loadEntiteAdministrativeByType(HttpServletRequest request) {

        String result = null;

        try {

            String codeType = request.getParameter("codeType");

            List<EntiteAdministrative> entiteList = GestionArticleBudgetaireBusiness.loadEntiteAdministrativeByType(codeType);

            if (!entiteList.isEmpty()) {

                List<JSONObject> jsonEntiteList = new ArrayList<>();

                for (EntiteAdministrative entite : entiteList) {

                    JSONObject jsonEntite = new JSONObject();

                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.CODE_ENTITE, entite.getCode());
                    jsonEntite.put(GestionArticleBudgetaireConst.ParamName.INTITULE_ENTITE, entite.getIntitule());

                    jsonEntiteList.add(jsonEntite);
                }
                result = jsonEntiteList.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getArticleBudgetaireImpot(HttpServletRequest request) {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            JSONObject data = new JSONObject(request.getParameter("codes"));
            String code1 = data.getString("1");
            String code2 = data.getString("2");
            String code3 = data.getString("3");
            String code4 = data.getString("4");
            String code5 = data.getString("5");
            String code6 = data.getString("6");

            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.getArticleBudgetaireImpot(code1, code2, code3, code4, code5, code6);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {

                    JSONObject object = new JSONObject();

                    object.put("code", article.getCode() == null ? GeneralConst.EMPTY_STRING : article.getCode());
                    object.put("intitule", article.getIntitule() == null ? GeneralConst.EMPTY_STRING : article.getIntitule());
                    object.put("codePeriodicite", article.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : article.getPeriodicite().getCode());
                    object.put("intitulePeriodicite", article.getPeriodicite() == null ? GeneralConst.EMPTY_STRING : article.getPeriodicite().getIntitule());
                    jSONObject.add(object);

                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
