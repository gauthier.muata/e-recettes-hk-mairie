/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import static cd.hologram.erecettesvg.business.PaiementBusiness.getAgentByCode;
import static cd.hologram.erecettesvg.business.PaiementBusiness.getEtatByCode;
import static cd.hologram.erecettesvg.business.PaiementBusiness.getListPaymentByAdvencedJournal;
import static cd.hologram.erecettesvg.business.PaiementBusiness.getListPaymentJournal;
import static cd.hologram.erecettesvg.business.PaiementBusiness.getListPaymentNpMairieByAdvencedJournal;
import cd.hologram.erecettesvg.business.PoursuiteBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.PaiementConst;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DetailBordereau;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Etat;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionMairie;
import cd.hologram.erecettesvg.models.ParamAmr;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.SiteBanque;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import static cd.hologram.erecettesvg.util.Tools.getValidFormat;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author bonheur.muntasomo
 */
@WebServlet(name = "Paiement", urlPatterns = {"/paiement_servlet"})
public class Paiement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    List<Journal> listJournal = new ArrayList<>();

    JsonObject npJson;
    JsonObject amrJson;
    JsonObject bpJson;
    JsonObject declarationJson;
    AdressePersonne adressePersonne;
    CompteBancaire compteBancaire;
    Personne personne;
    Journal journal;
    String valueOperation = GeneralConst.EMPTY_STRING;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(PaiementConst.ParamName.OPERATION);

        switch (operation) {
            case NotePerceptionConst.Operation.LOAD_AMR:
                result = loadAmrByNumero(request);
                break;
            case NotePerceptionConst.Operation.LOAD_BON_A_PAYER:
                result = loadBonAPayerByCode(request);
                break;
            case NotePerceptionConst.Operation.LOAD_NOTE_PERCEPTION:
                result = loadNotePerceptionByNumero(request);
                break;
            case PaiementConst.Operation.SAVE_JOURNAL:
                valueOperation = PaiementConst.Operation.SAVE_JOURNAL;
                result = saveJournal(request);
                break;
            case NotePerceptionConst.Operation.LOAD_DECLARATION:
                result = loadDeclarationByNumero(request);
                break;
            case PaiementConst.Operation.SAVE_DECLARATION:
                valueOperation = PaiementConst.Operation.SAVE_DECLARATION;
                result = saveDeclaration(request);
                break;

            case PaiementConst.Operation.SEARCH_PAYMENT_FROM_BANK:
                //valueOperation = PaiementConst.Operation.SAVE_DECLARATION;
                result = loadPaiementByDocument(request);
                break;

            case PaiementConst.Operation.TRAITEMENT_PAIEMENT:
                //valueOperation = PaiementConst.Operation.SAVE_DECLARATION;
                result = traiterPaiement(request);
                break;
            case PaiementConst.Operation.SEARCH_PAYMENT_FROM_ADMINISTRATION:
            case PaiementConst.Operation.SEARCH_PAYMENT_ADVENCED:
                try {
                    result = getJournals(request);
                } catch (Exception ex) {
                    CustumException.LogException(ex);
                }
                break;
            case PaiementConst.Operation.VALIDATION_APUREMENT_ADMIN:
                try {
                    result = getApurementAdminValidation(request);
                } catch (Exception ex) {
                    CustumException.LogException(ex);
                }
                break;
            case PaiementConst.Operation.VALIDATION_APUREMENT_COMPT:
                try {
                    result = getApurementComptValidation(request);
                } catch (Exception ex) {
                    CustumException.LogException(ex);
                }
                break;

            case "loadVolet":
                result = loadVoletyNumeroNP(request);
                break;

            case "saveJournalVolet":
                valueOperation = "saveJournalVolet";
                result = saveVoletToJournal(request);
                break;

            /*case PaiementConst.Operation.SEARCH_PAYMENT:
             //valueOperation = PaiementConst.Operation.SAVE_DECLARATION;
             result = loadPaiementByDocument(request);
             break;*/
        }

        out.print(result);

    }

    public String getApurementAdminValidation(HttpServletRequest request) {

        String codeJournal, documentApure, agentApurement, montantApure, numeroReleveBancaire, dateReleveBancaire, page, operation,
                document = GeneralConst.EMPTY_STRING;

        operation = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.OPERATION);

        codeJournal = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.CODE_JOURNAL);
        documentApure = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.DOCUMENT_APURE);
        agentApurement = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.AGENT_APUREMENT);
        montantApure = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.MONTANT_APURE);
        numeroReleveBancaire = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE);
        dateReleveBancaire = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.DATE_RELEVE_BANCAIRE);
        page = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.VALUE_PAGE);

        String dataReturn;

        PaiementBusiness paiementBusiness = new PaiementBusiness();

        dataReturn = paiementBusiness.validationApurementAdmin(
                codeJournal,
                documentApure,
                agentApurement,
                montantApure,
                numeroReleveBancaire,
                dateReleveBancaire);

        return dataReturn;

    }

    public String getApurementComptValidation(HttpServletRequest request) {

        String codeJournal, etatApurement, observationAvisRejete, operation, page, document = GeneralConst.EMPTY_STRING;

        operation = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.OPERATION);
        codeJournal = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.CODE_JOURNAL);
        etatApurement = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.ETAT_APUREMENT);
        observationAvisRejete = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.OBSERVATION_APUREMENT);
        page = request.getParameter(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.VALUE_PAGE);

        String dataReturn;

        PaiementBusiness paiementBusiness = new PaiementBusiness();

        dataReturn = paiementBusiness.validationApurementCompt(codeJournal, observationAvisRejete, Integer.valueOf(etatApurement));

        return dataReturn;
    }

    public String getJournals(HttpServletRequest request) throws ParseException, JSONException {

        String typeSeach, valueSearch, dataReturn, page, operation, site, siteValueList, banque, compteBancaire, periodeDebut, periodeFin,
                document = GeneralConst.EMPTY_STRING;
        String myListCode = GeneralConst.EMPTY_STRING;
        boolean rigthUser = false;

        operation = request.getParameter(PaiementConst.ParamName.OPERATION);

        typeSeach = request.getParameter(PaiementConst.ParamName.TYPE_SEARCH);
        valueSearch = request.getParameter(PaiementConst.ParamName.VALUE_SEACH);
        page = request.getParameter(PaiementConst.ParamName.VALUE_PAGE);

        site = request.getParameter(PaiementConst.ParamName.SITE_VALUE);
        siteValueList = request.getParameter(PaiementConst.ParamName.SITE_VALUE_LIST);
        banque = request.getParameter(PaiementConst.ParamName.BANQUE_VALUE);
        compteBancaire = request.getParameter(PaiementConst.ParamName.COMPTE_BANCAIRE_VALUE);
        periodeDebut = getValidFormat(request.getParameter(PaiementConst.ParamName.PERIODE_DEBUT_VALUE));
        periodeFin = getValidFormat(request.getParameter(PaiementConst.ParamName.PERIODE_FIN_VALUE));

        if (!siteValueList.equals(GeneralConst.Number.ZERO)) {
            rigthUser = true;

            try {
                JSONArray jSONArray = new JSONArray(siteValueList);
                List<String> codeList = new ArrayList<>();
                String listCode;

                for (int i = 0; i < jSONArray.length(); i++) {

                    JSONObject object = jSONArray.getJSONObject(i);

                    String code = object.getString("SiteCode");

                    codeList.add(code);
                }
                if (codeList != null) {
                    for (int i = 0; i < jSONArray.length(); i++) {

                        if (i < jSONArray.length() - 1) {
                            listCode = codeList.get(i).toString() + GeneralConst.VIRGULE;
                        } else {
                            listCode = codeList.get(i).toString();
                        }
                        myListCode = myListCode + listCode;

                    }

                }
            } catch (JSONException ex) {
                System.out.println(ex.getMessage());
                Logger.getLogger(Paiement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        switch (operation) {
            case PaiementConst.Operation.SEARCH_PAYMENT_FROM_ADMINISTRATION:
                listJournal = getListPaymentJournal(Integer.valueOf(typeSeach), valueSearch, Integer.valueOf(page), rigthUser, myListCode);
                break;
            case PaiementConst.Operation.SEARCH_PAYMENT_ADVENCED:
//                listJournal = getListPaymentByAdvencedJournal(site, banque, compteBancaire, periodeDebut, periodeFin, Integer.valueOf(page));
                listJournal = getListPaymentNpMairieByAdvencedJournal(site, banque, compteBancaire, periodeDebut, periodeFin, Integer.valueOf(page));
                break;
        }

        List<JsonObject> listJsonObjects = new ArrayList<>();

        if (listJournal != null && !listJournal.isEmpty()) {

            for (Journal journal : listJournal) {

                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty(PaiementConst.ParamName.DATE_PAIEMENT,
                        Tools.formatDateToString(journal.getDateCreat()));
                String ab = GeneralConst.EMPTY_STRING;
                Journal jrnl;

                switch (journal.getTypeDocument().trim()) {
                    case PaiementConst.ParamNameDocument.NP:
                        document = PaiementConst.ParamNameDocument.NOTE_PERCEPTION;
                        NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(journal.getDocumentApure());
                        ab = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();
                        jsonObject.addProperty("codeArticleBudgetaire", np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode());
                        jsonObject.addProperty("intituleArticleBudgetaire", np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getIntitule());
                        jsonObject.addProperty("dateOrdonnancement", np.getDateCreat());
                        break;
                    case PaiementConst.ParamNameDocument.BAP:
                        document = PaiementConst.ParamNameDocument.BON_A_PAYE;
                        break;
                    case PaiementConst.ParamNameDocument.AMR_ONE:
                        document = PaiementConst.ParamNameDocument.AMR_ONE;
                        break;
                    case PaiementConst.ParamNameDocument.AMR_TWO:
                        document = PaiementConst.ParamNameDocument.AMR_TWO;
                        break;
                    case PaiementConst.ParamNameDocument.DEC:
                        document = PaiementConst.ParamNameDocument.DECLARATION;
                        RetraitDeclaration rdcl = DeclarationBusiness.getRetraiteclarationByNumero(journal.getDocumentApure());
                        ArticleBudgetaire artb = DeclarationBusiness.getArticleBudgetaireByCode(rdcl.getFkAb());
                        ab = artb.getCode();
                        jsonObject.addProperty("codeArticleBudgetaire", artb.getCode());
                        jsonObject.addProperty("intituleArticleBudgetaire", artb.getIntitule());
                        jsonObject.addProperty("dateOrdonnancement", ConvertDate.formatDateToString(rdcl.getDateCreate()));
                        break;
                }

                jsonObject.addProperty(PaiementConst.ParamName.DOCUMENT, document.trim());

                String requerant = String.format("%s %s %s", journal.getPersonne().getNom(), journal.getPersonne().getPostnom(), journal.getPersonne().getPrenoms());

                jsonObject.addProperty(PaiementConst.ParamName.REQUERANT, requerant);

                jrnl = PaiementBusiness.getJournalByDocument_V2(journal.getDocumentApure());

                jsonObject.addProperty(PaiementConst.ParamName.AMOUNT2, jrnl == null
                        ? GeneralConst.Numeric.ZERO
                        : jrnl.getMontantPercu());

                jsonObject.addProperty(PaiementConst.ParamName.CODE_JOURNAL_VOLET_2, jrnl == null
                        ? GeneralConst.EMPTY_STRING
                        : jrnl.getCode());

                jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                        journal == null
                                ? GeneralConst.EMPTY_STRING
                                : journal.getPersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : journal.getPersonne().getLoginWeb().getUsername() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : journal.getPersonne().getLoginWeb().getUsername());

                jsonObject.addProperty(PaiementConst.ParamName.REFERENCE, journal.getDocumentApure().trim());
                jsonObject.addProperty(PaiementConst.ParamName.BORDEREAU, journal.getBordereau().trim());
                Date dateBd = journal.getDateBordereau();

                jsonObject.addProperty(PaiementConst.ParamName.DATE_BORDERAU, ConvertDate.formatDateToStringOfFormat(dateBd, "dd/MM/yyyy"));
                jsonObject.addProperty(PaiementConst.ParamName.LIBELLE_BANQUE, journal.getCompteBancaire().getBanque().getIntitule().trim());
                jsonObject.addProperty(PaiementConst.ParamName.COMPTE_BANCAIRE, journal.getCompteBancaire().getCode().trim());
                jsonObject.addProperty(PaiementConst.ParamName.DEVISE, journal.getDevise().getCode() != null ? journal.getDevise().getCode() : GeneralConst.Devise.DEVISE_CDF);
                jsonObject.addProperty(PaiementConst.ParamName.MONTANT_PAYE, journal.getMontantPercu());
                jsonObject.addProperty(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.CODE_JOURNAL, journal.getCode().trim());
                jsonObject.addProperty(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.AGENT_APUREMENT, journal.getAgentApurement());
                jsonObject.addProperty(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.MONTANT_APURE, journal.getMontantPercu());

                jsonObject.addProperty(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE, (journal.getNumeroReleveBancaire() != null && !journal.getNumeroReleveBancaire().isEmpty() ? journal.getNumeroReleveBancaire().trim() : GeneralConst.EMPTY_STRING));
                jsonObject.addProperty(cd.hologram.erecettesvg.constants.PaiementConst.ParamName.DATE_RELEVE_BANCAIRE, (ConvertDate.formatDateToStringOfFormat(journal.getDateReleveBancaire(), "dd/MM/yyyy") != null && !ConvertDate.formatDateToStringOfFormat(journal.getDateReleveBancaire(), "dd/MM/yyyy").isEmpty() ? ConvertDate.formatDateToStringOfFormat(journal.getDateReleveBancaire(), "dd/MM/yyyy") : GeneralConst.EMPTY_STRING));

                Etat etat = getEtatByCode(Integer.valueOf(journal.getEtat()));
                jsonObject.addProperty(PaiementConst.ParamName.ETAT_PAIEMENT, etat.getLibelleEtat());

                Agent agent = getAgentByCode(Integer.valueOf(journal.getAgentCreat().trim()));
                jsonObject.addProperty(PaiementConst.ParamName.AGENT_PAIEMENT, (agent != null) ? agent.toString() : GeneralConst.EMPTY_STRING);

                jsonObject.addProperty(PaiementConst.ParamName.OBSERVATION, (journal != null) ? journal.getObservation() : GeneralConst.EMPTY_STRING);

                listJsonObjects.add(jsonObject);
            }

            return listJsonObjects.toString();
        } else {
            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
        }
        return dataReturn;
    }

    public String traiterPaiement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String recordId = GeneralConst.EMPTY_STRING;
        String recordIdVolet2 = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;
        String observation = GeneralConst.EMPTY_STRING;
        String typeDocument = GeneralConst.EMPTY_STRING;
        String value = GeneralConst.EMPTY_STRING;

        boolean result = false;

        try {

            recordId = request.getParameter(PaiementConst.ParamName.REFERENCE_RECORD);
            recordIdVolet2 = request.getParameter(PaiementConst.ParamName.REFERENCE_RECORD_VOLET_2);
            userId = request.getParameter(GeneralConst.ParamName.USER_ID);
            observation = request.getParameter(PaiementConst.ParamName.OBSERVATION);
            typeDocument = request.getParameter(PaiementConst.ParamName.TYPE_DOCUMENT_CODE);
            value = request.getParameter(PaiementConst.ParamName.VALUE_TRAITEMENT);

            switch (typeDocument) {
//                case "AMR":
//                case "BP":
                case "NP":

//                    result = PaiementBusiness.traiterPaiementJournal(recordId, observation, value, userId);
                    result = PaiementBusiness.traiterPaiementJournalVolet2(recordId, observation, value, userId, recordIdVolet2);

                    if (result) {
                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                    break;
                case "DEC":

                    result = PaiementBusiness.traiterPaiementBordereau(recordId, observation, value, userId);

                    if (result) {
                        dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadPaiementByDocument(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;
        String typeDocument = GeneralConst.EMPTY_STRING;
        String allSite = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String agentOrSite = GeneralConst.EMPTY_STRING;
        String periodeDebutValue = GeneralConst.EMPTY_STRING;
        String periodeFinValue = GeneralConst.EMPTY_STRING;
        String fromResearch = GeneralConst.EMPTY_STRING;
        String accountBank = GeneralConst.EMPTY_STRING;
        String statePaiement = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(PaiementConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(PaiementConst.ParamName.TYPE_SEARCH);
            typeDocument = request.getParameter(PaiementConst.ParamName.TYPE_DOCUMENT);
            allSite = request.getParameter(PaiementConst.ParamName.ALL_SITE);
            advancedSearch = request.getParameter(PaiementConst.ParamName.ADVANCED_SEARCH);
            agentOrSite = request.getParameter(PaiementConst.ParamName.AGENT_OR_SITE);
            periodeDebutValue = request.getParameter(PaiementConst.ParamName.PERIODE_DEBUT_VALUE);
            periodeFinValue = request.getParameter(PaiementConst.ParamName.PERIODE_FIN_VALUE);
            fromResearch = request.getParameter(PaiementConst.ParamName.FROM_RESEARCH_PAYMENT);
            accountBank = request.getParameter(PaiementConst.ParamName.COMPTE_BANCAIRE);
            statePaiement = request.getParameter(PaiementConst.ParamName.ID_ETAT_PAIEMENT);

            List<Journal> journals = new ArrayList<>();
            Journal journal = new Journal();
            Bordereau bordereau = new Bordereau();
            List<Bordereau> bordereaus = new ArrayList<>();

            switch (fromResearch) {
                case GeneralConst.Number.ONE:

                    switch (advancedSearch) {
                        case GeneralConst.Number.ZERO:

                            switch (typeDocument) {
//                                case "AMR":
//                                case "BP":
                                case "NP":

                                    if (typeSearch.equals(GeneralConst.Number.ONE)) {

                                        journals = PaiementBusiness.getListJournalByTitrePerceptionMairieAndDocument(
                                                valueSearch, typeDocument, allSite, agentOrSite);

                                        if (!journals.isEmpty()) {
                                            dataReturn = getInfosJournal(journals);
                                        } else {
                                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                        }

                                    } else if (typeSearch.equals(GeneralConst.Number.TWO)) {

                                        journal = PaiementBusiness.getJournalByTitrePerceptionMairieAndDocument(
                                                valueSearch, typeDocument, allSite, agentOrSite);

                                        if (journal != null) {
                                            journals.add(journal);
                                            dataReturn = getInfosJournal(journals);
                                        } else {
                                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                        }

                                    }

                                    break;

                                case "DEC":

                                    if (typeSearch.equals(GeneralConst.Number.ONE)) {

                                        bordereaus = PaiementBusiness.getListBordereauByNameAssujetti(
                                                valueSearch, allSite, agentOrSite, periodeDebutValue, periodeFinValue);

                                        if (!bordereaus.isEmpty()) {
                                            dataReturn = getInfosBordereau(bordereaus);
                                        } else {
                                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                        }

                                    } else {
                                        bordereau = PaiementBusiness.getBordereauByTitrePerceptionAndDocument(
                                                valueSearch, allSite, agentOrSite);

                                        if (bordereau != null) {
                                            bordereaus.add(bordereau);
                                            dataReturn = getInfosBordereau(bordereaus);
                                        } else {
                                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                        }

                                    }

                                    break;
                            }
                            break;
                        case GeneralConst.Number.ONE:
                            switch (typeDocument) {
//                                case "AMR":
//                                case "BP":
                                case "NP":

                                    journals = PaiementBusiness.getListJournalAdvancedSearchNpMairie(
                                            periodeDebutValue, periodeFinValue, allSite,
                                            agentOrSite, typeDocument, accountBank, statePaiement);

                                    if (!journals.isEmpty()) {
                                        dataReturn = getInfosJournal(journals);
                                    } else {
                                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                    }

                                    break;

                                case "DEC":

                                    bordereaus = PaiementBusiness.getListBordereauByTitrePerceptionAndDocumentAdvancedSearch(
                                            accountBank, allSite, agentOrSite, periodeDebutValue, periodeFinValue, statePaiement);

                                    if (!bordereaus.isEmpty()) {
                                        dataReturn = getInfosBordereau(bordereaus);
                                    } else {
                                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case GeneralConst.Number.ZERO:
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveVoletToJournal(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String responseSaveSuiviComptable;
        String responseSaveJournal;

        try {

            String referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            String referencePaiement = request.getParameter(PaiementConst.ParamName.REFERENCE_PAIEMENT);
            String typeDocument = request.getParameter(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE);
            String datePaiement = request.getParameter(PaiementConst.ParamName.DATE_PAIEMENT);
            String userId = request.getParameter(GeneralConst.ParamName.USER_ID);
            String accountBank = request.getParameter("accountBank");
            String bankCode = request.getParameter("bankCode");
            String montant = request.getParameter("montant");
            String volet = request.getParameter("volet");

            Journal journal = new Journal();
            NotePerceptionMairie notePerception = new NotePerceptionMairie();

            Agent agent = PaiementBusiness.getAgentByCode(Integer.valueOf(userId));

            String compteBancaire = GeneralConst.EMPTY_STRING;
            String NcFiche = GeneralConst.EMPTY_STRING;
            String codeAssujetti = GeneralConst.EMPTY_STRING;
            String deviseFiche = GeneralConst.EMPTY_STRING;

            BigDecimal montantPaye = new BigDecimal("0");
            montantPaye = montantPaye.add(BigDecimal.valueOf(Double.valueOf(montant)));

            CompteBancaire cb = AcquitLiberatoireBusiness.getCompteBancaireByCode(accountBank);

            if (cb != null) {
                deviseFiche = cb.getDevise().getCode();
            }

            notePerception = NotePerceptionBusiness.getNotePerceptionMarieByCode(referenceDocument);

            if (notePerception != null) {

                compteBancaire = notePerception.getFkCompteBancaire();

                if (PaiementBusiness.checkReferePaiementExists(
                        referencePaiement.trim(),
                        compteBancaire.trim(), typeDocument)) {

                    dataReturn = PaiementConst.ResultCode.BORDEREAU_EXISTING;

                } else {

                    journal.setDocumentApure(referenceDocument.trim());
                    journal.setSite(agent.getSite());
                    journal.setTypeDocument(typeDocument);
                    journal.setMontant(montantPaye);
                    journal.setCompteBancaire(new CompteBancaire(compteBancaire));
                    journal.setBordereau(referencePaiement.trim());
//                    journal.setDateBordereau(Tools.formatStringFullToDate(datePaiement));
                    journal.setDateBordereau(null);
                    journal.setMontantApurre(BigDecimal.ZERO);
                    journal.setNumeroReleveBancaire(GeneralConst.EMPTY_STRING);
                    journal.setDateReleveBancaire(null);
                    journal.setAgentCreat(agent.getCode() + GeneralConst.EMPTY_STRING);
                    journal.setAgentApurement(null);
                    journal.setPersonne(new Personne(notePerception.getFkPersonne()));
                    journal.setTaux(BigDecimal.ONE);
                    journal.setModePaiement("BANQUE");
                    journal.setMontantPercu(montantPaye);
                    journal.setNumeroAttestationPaiement(GeneralConst.EMPTY_STRING);
                    journal.setDevise(new Devise(notePerception.getFkDevise()));
                    journal.setVolet(volet);

                    responseSaveJournal = saveVoletInJournal(journal,
                            NotePerceptionConst.TypeDocument.NP_CODE);

                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    private String saveVoletInJournal(Journal journal, String typeDocument) {
        boolean result1;
        String result2;

        try {

            result1 = PaiementBusiness.saveVoletJournal(journal, typeDocument);

            if (result1) {
                result2 = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result2 = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result2;
    }

    private String getInfosJournal(List<Journal> journals) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            List<JsonObject> jsonObjList = new ArrayList<>();
            Journal jrnl = new Journal();

            for (Journal journal : journals) {

                JsonObject jsonObject = new JsonObject();
                Agent agent = new Agent();

                jsonObject.addProperty(PaiementConst.ParamName.DOCUMENT_APURE, journal.getDocumentApure().trim());
                jsonObject.addProperty(PaiementConst.ParamName.CODE_JOURNAL, journal.getCode());
                jsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                        journal.getPersonne() != null ? journal.getPersonne().toString().toUpperCase()
                                : GeneralConst.EMPTY_STRING);

                jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                        journal == null
                                ? GeneralConst.EMPTY_STRING
                                : journal.getPersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : journal.getPersonne().getLoginWeb() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : journal.getPersonne().getLoginWeb().getUsername());

                jsonObject.addProperty(PaiementConst.ParamName.DATE_PAIEMENT,
                        ConvertDate.formatDateToString(journal.getDateCreat()));

                if (journal.getDateMaj() != null) {

                    jsonObject.addProperty(PaiementConst.ParamName.DATE_VALIDATION_PAIEMENT,
                            ConvertDate.formatDateToString(journal.getDateMaj()));

                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.DATE_VALIDATION_PAIEMENT, GeneralConst.EMPTY_STRING);
                }

                if (journal.getAgentMaj() != null && !journal.getAgentMaj().isEmpty()) {
                    agent = getAgentByCode(Integer.valueOf(journal.getAgentMaj()));

                    if (agent != null) {
                        jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                                agent.toString().toUpperCase());
                    } else {
                        jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                                GeneralConst.EMPTY_STRING);
                    }
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                            GeneralConst.EMPTY_STRING);
                }

                switch (journal.getTypeDocument()) {

                    case NotePerceptionConst.TypeDocument.NP_CODE:

                        jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT_CODE,
                                NotePerceptionConst.TypeDocument.NP_CODE);

                        jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT,
                                NotePerceptionConst.TypeDocument.NP_LIBELLE);
                        break;
                    case NotePerceptionConst.TypeDocument.AMR_CODE:

                        jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT_CODE,
                                NotePerceptionConst.TypeDocument.AMR_CODE);

                        Amr amr = NotePerceptionBusiness.getAmrByCode(journal.getDocumentApure());

                        if (amr != null) {

                            switch (amr.getTypeAmr()) {
                                case "AMR1":
                                    jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT,
                                            NotePerceptionConst.TypeDocument.AMR_LIBELLE_1);
                                    break;
                                case "AMR2":
                                    jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT,
                                            NotePerceptionConst.TypeDocument.AMR_LIBELLE_2);
                                    break;
                            }

                        } else {
                            jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT,
                                    NotePerceptionConst.TypeDocument.AMR_LIBELLE);
                        }

                        break;

                    case NotePerceptionConst.TypeDocument.BP_CODE:

                        jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT_CODE,
                                NotePerceptionConst.TypeDocument.BP_CODE);

                        jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT,
                                NotePerceptionConst.TypeDocument.BP_LIBELLE);
                        break;

                }

                jsonObject.addProperty(PaiementConst.ParamName.BORDEREAU, journal.getBordereau());
                jsonObject.addProperty(PaiementConst.ParamName.DATE_BORDERAU, ConvertDate.formatDateToStringOfFormat(journal.getDateBordereau(), "dd/MM/yyyy"));
                jsonObject.addProperty(PaiementConst.ParamName.COMPTE_BANCAIRE, journal.getCompteBancaire()
                        != null ? journal.getCompteBancaire().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);
                jsonObject.addProperty(PaiementConst.ParamName.BANQUE_NAME, journal.getSite()
                        != null ? journal.getSite().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                jsonObject.addProperty(PaiementConst.ParamName.MONTANT_PAYE, journal.getMontantPercu());
                jsonObject.addProperty(PaiementConst.ParamName.DEVISE, journal.getDevise().getCode());

                jrnl = PaiementBusiness.getJournalByDocument_V2(journal.getDocumentApure());

                if (jrnl != null) {
                    jsonObject.addProperty(PaiementConst.ParamName.AMOUNT2, jrnl.getMontantPercu());
                    jsonObject.addProperty(PaiementConst.ParamName.CODE_JOURNAL_VOLET_2, jrnl.getCode());
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.AMOUNT2, GeneralConst.Number.ZERO);
                    jsonObject.addProperty(PaiementConst.ParamName.CODE_JOURNAL_VOLET_2, GeneralConst.EMPTY_STRING);
                }

                String stateLibelle = GeneralConst.EMPTY_STRING;

                switch (journal.getEtat()) {
                    case GeneralConst.Numeric.ONE: // Apurement comptable
                        stateLibelle = "APURER";

                        jsonObject.addProperty(PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE, journal.getNumeroReleveBancaire());
                        jsonObject.addProperty(PaiementConst.ParamName.DATE_RELEVE_BANCAIRE, ConvertDate.formatDateToStringOfFormat(journal.getDateReleveBancaire(), "dd/MM/yyyy"));

                        break;
                    case GeneralConst.Numeric.TWO: // Valider à la banque
                        stateLibelle = "VALIDER";

                        jsonObject.addProperty(PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PaiementConst.ParamName.DATE_RELEVE_BANCAIRE, GeneralConst.EMPTY_STRING);
                        break;
                    case GeneralConst.Numeric.THREE: // En attente d'une validation à la banque
                        stateLibelle = "EN ATTENTE DE VALIDATION";
                        jsonObject.addProperty(PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE, GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(PaiementConst.ParamName.DATE_RELEVE_BANCAIRE, GeneralConst.EMPTY_STRING);
                        break;
                    case GeneralConst.Numeric.FIVE: // Apurement administratif
                        stateLibelle = "APUREMENT ADMINISTRATIF";
                        jsonObject.addProperty(PaiementConst.ParamName.NUMERO_RELEVE_BANCAIRE, journal.getNumeroReleveBancaire());
                        jsonObject.addProperty(PaiementConst.ParamName.DATE_RELEVE_BANCAIRE, ConvertDate.formatDateToStringOfFormat(journal.getDateReleveBancaire(), "dd/MM/yyyy"));
                        break;
                    case GeneralConst.Numeric.FOUR: // Annuler à la banque
                        stateLibelle = "REJETER A LA BANQUE";
                        break;
                }

                jsonObject.addProperty(PaiementConst.ParamName.ETAT_PAIEMENT, stateLibelle);
                jsonObject.addProperty(PaiementConst.ParamName.ID_ETAT_PAIEMENT, journal.getEtat());

                if (journal.getObservationBanque() != null && !journal.getObservationBanque().isEmpty()) {
                    jsonObject.addProperty(PaiementConst.ParamName.OBSERVATION, journal.getObservationBanque());
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.OBSERVATION, GeneralConst.EMPTY_STRING);
                }

                agent = PaiementBusiness.getAgentByCode(Integer.valueOf(journal.getAgentCreat()));

                if (agent != null) {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_PAIEMENT, agent.toString().toUpperCase());
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_PAIEMENT, GeneralConst.EMPTY_STRING);
                }

                jsonObjList.add(jsonObject);
            }

            dataReturn = jsonObjList.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private String getInfosBordereau(List<Bordereau> bordereaus) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            List<JsonObject> jsonObjList = new ArrayList<>();

            for (Bordereau bordereau : bordereaus) {

                JsonObject jsonObject = new JsonObject();
                Personne personne = new Personne();
                Agent agent = new Agent();

                jsonObject.addProperty(PaiementConst.ParamName.DOCUMENT_APURE, bordereau.getNumeroDeclaration());
                jsonObject.addProperty(PaiementConst.ParamName.CODE_JOURNAL, bordereau.getCode());

                personne = IdentificationBusiness.getPersonneByCode(bordereau.getNomComplet().trim());

                if (personne != null) {

                    jsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            personne.toString().toUpperCase());

                    jsonObject.addProperty("userName",
                            personne.getLoginWeb().getUsername().toUpperCase());

                } else {
                    jsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty("userName",
                            GeneralConst.EMPTY_STRING);
                }

                jsonObject.addProperty(PaiementConst.ParamName.DATE_PAIEMENT,
                        ConvertDate.formatDateToString(bordereau.getDatePaiement()));

                jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT_CODE,
                        NotePerceptionConst.TypeDocument.DECLARATION_CODE);

                jsonObject.addProperty(PaiementConst.ParamName.TYPE_DOCUMENT, NotePerceptionConst.TypeDocument.DECLARATION_LIBELLE);
                jsonObject.addProperty(PaiementConst.ParamName.BORDEREAU, bordereau.getNumeroBordereau());
                jsonObject.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                        ConvertDate.formatDateToString(bordereau.getDatePaiement()));

                if (bordereau.getDateMaj() != null) {

                    jsonObject.addProperty(PaiementConst.ParamName.DATE_VALIDATION_PAIEMENT,
                            ConvertDate.formatDateToString(bordereau.getDateMaj()));

                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.DATE_VALIDATION_PAIEMENT, GeneralConst.EMPTY_STRING);
                }

                if (bordereau.getAgentMaj() > 0) {
                    agent = getAgentByCode(bordereau.getAgentMaj());

                    if (agent != null) {
                        jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                                agent.toString().toUpperCase());
                    } else {
                        jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                                GeneralConst.EMPTY_STRING);
                    }
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_VALIDATION_PAIEMENT,
                            GeneralConst.EMPTY_STRING);
                }

                jsonObject.addProperty(PaiementConst.ParamName.COMPTE_BANCAIRE, bordereau.getCompteBancaire()
                        != null ? bordereau.getCompteBancaire().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);
                jsonObject.addProperty(PaiementConst.ParamName.BANQUE_NAME, bordereau.getCompteBancaire()
                        != null ? bordereau.getCompteBancaire().getBanque().getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                jsonObject.addProperty(PaiementConst.ParamName.MONTANT_PAYE, bordereau.getTotalMontantPercu());
                jsonObject.addProperty(PaiementConst.ParamName.DEVISE, bordereau.getCompteBancaire()
                        != null ? bordereau.getCompteBancaire().getDevise().getCode() : GeneralConst.EMPTY_STRING);

                String stateLibelle = GeneralConst.EMPTY_STRING;

                switch (bordereau.getEtat().getCode()) {
                    case GeneralConst.Numeric.ONE: // Apurement comptable
                        stateLibelle = "APURER";
                        break;
                    case GeneralConst.Numeric.TWO: // Valider à la banque
                        stateLibelle = "VALIDER";
                        break;
                    case GeneralConst.Numeric.THREE: // En attente d'une validation à la banque
                        stateLibelle = "EN ATTENTE DE VALIDATION";
                        break;
                    case GeneralConst.Numeric.FIVE: // Apurement administratif
                        stateLibelle = "APUREMENT ADMINISTRATIF";
                        break;
                    case GeneralConst.Numeric.FOUR: // Annuler à la banque
                        stateLibelle = "REJETER A LA BANQUE";
                        break;
                }

                jsonObject.addProperty(PaiementConst.ParamName.ETAT_PAIEMENT, stateLibelle);
                jsonObject.addProperty(PaiementConst.ParamName.ID_ETAT_PAIEMENT, bordereau.getEtat().getCode());

                if (bordereau.getObservationBanque() != null && !bordereau.getObservationBanque().isEmpty()) {
                    jsonObject.addProperty(PaiementConst.ParamName.OBSERVATION, bordereau.getObservationBanque());
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.OBSERVATION, GeneralConst.EMPTY_STRING);
                }

                if (bordereau.getAgent() != null) {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_PAIEMENT, bordereau.getAgent().toString().toUpperCase());
                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.AGENT_PAIEMENT, GeneralConst.EMPTY_STRING);
                }

                if (!bordereau.getDetailBordereauList().isEmpty()) {

                    List<JsonObject> jsonDbList = new ArrayList<>();

                    for (DetailBordereau db : bordereau.getDetailBordereauList()) {

                        JsonObject jsonDB = new JsonObject();
                        PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                        jsonDB.addProperty(PaiementConst.ParamName.MONTANT_PERCU, db.getMontantPercu());
                        jsonDB.addProperty(PaiementConst.ParamName.DEVISE, db.getDevise());
                        jsonDB.addProperty(PaiementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                db.getArticleBudgetaire().getIntitule().toUpperCase().concat(
                                        GeneralConst.TWO_POINTS).concat(
                                        db.getArticleBudgetaire().getTarif().getIntitule()));

                        String periodeName = GeneralConst.EMPTY_STRING;

                        periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(
                                db.getPeriodicite());

                        if (periodeDeclaration != null) {
                            periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                    periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());
                        }

                        jsonDB.addProperty(PaiementConst.ParamName.BORDEREAU_ID, db.getBordereau().getCode());
                        jsonDB.addProperty(PaiementConst.ParamName.PERIODE_NAME, periodeName);
                        jsonDB.addProperty(PaiementConst.ParamName.PERIODE_NAME, periodeName);

                        jsonDbList.add(jsonDB);

                    }

                    jsonObject.addProperty(PaiementConst.ParamName.DETAIL_BORDEREAU_LIST, jsonDbList.toString());

                } else {
                    jsonObject.addProperty(PaiementConst.ParamName.DETAIL_BORDEREAU_LIST, GeneralConst.EMPTY_STRING);
                }

                jsonObjList.add(jsonObject);
            }

            dataReturn = jsonObjList.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadAmrByNumero(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String siteCode = GeneralConst.EMPTY_STRING;

        try {

            referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            siteCode = request.getParameter(NotePerceptionConst.ParamName.SITE_CODE);

            Amr amr = NotePerceptionBusiness.getAmrByCode(referenceDocument);

            if (amr != null) {

                amrJson = new JsonObject();
                adressePersonne = new AdressePersonne();
                compteBancaire = new CompteBancaire();
                personne = new Personne();
                journal = new Journal();
                //FichePriseCharge fichePriseCharge = new FichePriseCharge();
                ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();
                String codeBank = GeneralConst.EMPTY_STRING;

                boolean nonAutoriser = false;

                if (amr.getFractionner()) {
                    return dataReturn = GeneralConst.ResultCode.TITRE_PERCEPTION_FRACTIONNE;
                }

                if (amr.getEnContentieux()) {
                    return dataReturn = GeneralConst.ResultCode.TITRE_PERCEPTION_EN_CONTENTIEUX;
                }

                if (amr.getFichePriseCharge().getPersonne() != null) {
                    personne = amr.getFichePriseCharge().getPersonne();

                    amrJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                            amr.getFichePriseCharge().getDevise());

                    if (!amr.getFichePriseCharge().getFkArticleBudgetaire().isEmpty()
                            && amr.getFichePriseCharge().getFkArticleBudgetaire() != null) {

                        articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(
                                amr.getFichePriseCharge().getFkArticleBudgetaire());
                    }
                } else {
                    amrJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                            GeneralConst.EMPTY_STRING);
                }

                amrJson.addProperty(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT,
                        amr.getNumero().trim());

                if (personne != null) {
                    amrJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                            personne.getFormeJuridique().getIntitule());

                    amrJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_CODE,
                            personne.getCode());

                    amrJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            personne.toString().toUpperCase());

                    adressePersonne = IdentificationBusiness.getDefaultAdressByPersone(personne.getCode());

                    if (adressePersonne != null) {

                        amrJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                adressePersonne.getAdresse().toString().toUpperCase());

                    } else {
                        amrJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }
                } else {
                    amrJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                            GeneralConst.EMPTY_STRING);
                    amrJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            GeneralConst.EMPTY_STRING);
                }

                switch (amr.getTypeAmr()) {
                    case "AMR1":
                        amrJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                                NotePerceptionConst.TypeDocument.AMR_LIBELLE_1);
                        break;
                    case "AMR2":
                        amrJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                                NotePerceptionConst.TypeDocument.AMR_LIBELLE_2);
                        break;
                }

                amrJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE,
                        NotePerceptionConst.TypeDocument.AMR_CODE);

                //String articleName = Tools.getArticleByNC_V2(notePerception.getNoteCalcul());
                if (articleBudgetaire != null) {
                    amrJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            articleBudgetaire.getIntitule());
                } else {
                    amrJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            GeneralConst.EMPTY_STRING);
                }

                //compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                //notePerception.getCompteBancaire());
                compteBancaire = null;

                if (compteBancaire != null) {

                    amrJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                            compteBancaire.getCode());

                    amrJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                            compteBancaire.getBanque().getCode());

                } else {
                    amrJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                            GeneralConst.EMPTY_STRING);

                    amrJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                            GeneralConst.EMPTY_STRING);

                    ParamAmr paramAmr = PoursuiteBusiness.getCompteBancaireToParamAmr();

                    if (paramAmr != null) {

                        String accountCdf = paramAmr.getCompteDgrkCdf();
                        String accountUsd = paramAmr.getCompteDgrkUsd();

                        String paramAccount = "'" + accountCdf + "','" + accountUsd + "'";
                        List<CompteBancaire> compteBancaireList = new ArrayList<>();

                        compteBancaireList = PoursuiteBusiness.getListCompteBancaireToParamAmr(paramAccount);

                        if (!compteBancaireList.isEmpty()) {

                            List<JsonObject> cbJsonList = new ArrayList<>();
                            List<JsonObject> bankJsonList = new ArrayList<>();
                            codeBank = GeneralConst.EMPTY_STRING;

                            for (CompteBancaire cb : compteBancaireList) {

                                JsonObject cbJson = new JsonObject();
                                JsonObject bankJson = new JsonObject();

                                if (cb.getDevise().getCode().equals(amr.getFichePriseCharge().getDevise())) {

                                    cbJson.addProperty("CompteBancaireCode", cb.getCode());
                                    cbJson.addProperty("CompteBancaireLibelle", cb.getIntitule());

                                    if (codeBank.isEmpty()) {

                                        codeBank = cb.getBanque().getCode();

                                        bankJson.addProperty("banqueCode", cb.getBanque().getCode());
                                        bankJson.addProperty("banqueName", cb.getBanque().getIntitule());

                                        bankJsonList.add(bankJson);

                                    } else {

                                        if (!codeBank.equals(cb.getBanque().getCode())) {

                                            bankJson.addProperty("banqueCode", cb.getBanque().getCode());
                                            bankJson.addProperty("banqueName", cb.getBanque().getIntitule());

                                            bankJsonList.add(bankJson);
                                        }
                                    }

                                    cbJsonList.add(cbJson);

                                }

                            }

                            amrJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIST,
                                    cbJsonList.toString());

                            amrJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIST,
                                    bankJsonList.toString());

                        } else {
                            amrJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIST,
                                    GeneralConst.EMPTY_STRING);

                            amrJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIST,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        amrJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIST,
                                GeneralConst.EMPTY_STRING);
                        amrJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIST,
                                GeneralConst.EMPTY_STRING);
                    }
                }

                amrJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                        amr.getNetAPayer());

                amrJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                        amr.getPayer());

                amrJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                        amr.getSolde());

                journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(amr.getNumero());

                if (journal != null) {

                    amrJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                            GeneralConst.Number.ONE);

                    amrJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                            journal.getBordereau());

                    amrJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                            ConvertDate.formatDateToStringOfFormat(journal.getDateBordereau(), "dd/MM/yyyy"));

                    amrJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                            journal.getMontantPercu());

                } else {
                    amrJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                            GeneralConst.Number.ZERO);

                    amrJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                            GeneralConst.EMPTY_STRING);

                    amrJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                            GeneralConst.EMPTY_STRING);

                    amrJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                            GeneralConst.EMPTY_STRING);
                }

                dataReturn = amrJson.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadBonAPayerByCode(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String siteCode = GeneralConst.EMPTY_STRING;

        try {

            referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            siteCode = request.getParameter(NotePerceptionConst.ParamName.SITE_CODE);

            BonAPayer bonAPayer = PoursuiteBusiness.getBonAPayerByCode(referenceDocument);

            if (bonAPayer != null) {

                bpJson = new JsonObject();
                adressePersonne = new AdressePersonne();
                compteBancaire = new CompteBancaire();
                personne = new Personne();
                journal = new Journal();

                if (bonAPayer.getEstFraction() != null) {

                    if (bonAPayer.getEstFraction()) {
                        return dataReturn = GeneralConst.ResultCode.TITRE_PERCEPTION_FRACTIONNE;
                    }
                }

                if (bonAPayer.getEnContentieux()) {
                    return dataReturn = GeneralConst.ResultCode.TITRE_PERCEPTION_EN_CONTENTIEUX;
                }

                if (bonAPayer.getFkAmr() != null) {

                    personne = bonAPayer.getFkPersonne();

                    if (bonAPayer.getFkAmr().getFichePriseCharge() != null) {

                        bpJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                bonAPayer.getFkAmr().getFichePriseCharge().getDevise());
                    } else {
                        bpJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                GeneralConst.EMPTY_STRING);
                    }

                } else {
                    bpJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                            GeneralConst.EMPTY_STRING);
                }

                bpJson.addProperty(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT,
                        bonAPayer.getCode());

                if (personne != null) {
                    bpJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                            personne.getFormeJuridique().getIntitule());

                    bpJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_CODE,
                            personne.getCode());

                    bpJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            personne.toString().toUpperCase());

                    adressePersonne = IdentificationBusiness.getDefaultAdressByPersone(personne.getCode());

                    if (adressePersonne != null) {

                        bpJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                adressePersonne.getAdresse().toString().toUpperCase());

                    } else {
                        bpJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }
                } else {
                    bpJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                            GeneralConst.EMPTY_STRING);
                    bpJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                            GeneralConst.EMPTY_STRING);
                }

                bpJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE,
                        NotePerceptionConst.TypeDocument.BP_CODE);
                bpJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                        NotePerceptionConst.TypeDocument.BP_LIBELLE);

                if (!bonAPayer.getActeGenerateur().isEmpty() && bonAPayer.getActeGenerateur() != null) {
                    bpJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            bonAPayer.getActeGenerateur());
                } else {
                    bpJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            bonAPayer.getMotifPenalite());
                }

                compteBancaire = null;
                List<CompteBancaire> compteBancaireList = new ArrayList<>();

                if (bonAPayer.getFkCompte() != null) {
                    compteBancaire = bonAPayer.getFkCompte();
                    compteBancaireList.add(compteBancaire);
                }

                if (!compteBancaireList.isEmpty()) {

                    List<JsonObject> cbJsonList = new ArrayList<>();
                    List<JsonObject> bankJsonList = new ArrayList<>();

                    for (CompteBancaire cb : compteBancaireList) {

                        JsonObject cbJson = new JsonObject();
                        JsonObject bankJson = new JsonObject();

                        cbJson.addProperty("CompteBancaireCode", cb.getCode());
                        cbJson.addProperty("CompteBancaireLibelle", cb.getIntitule());

                        bankJson.addProperty("banqueCode", cb.getBanque().getCode());
                        bankJson.addProperty("banqueName", cb.getBanque().getIntitule());

                        bankJsonList.add(bankJson);
                        cbJsonList.add(cbJson);

                    }

                    bpJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIST,
                            cbJsonList.toString());

                    bpJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIST,
                            bankJsonList.toString());
                } else {
                    bpJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIST,
                            GeneralConst.EMPTY_STRING);

                    bpJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIST,
                            GeneralConst.EMPTY_STRING);
                }

                bpJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                        bonAPayer.getMontant());

                journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(bonAPayer.getCode());

                BigDecimal soldeBp = new BigDecimal("0");
                BigDecimal payeBp = new BigDecimal("0");

                if (journal != null) {

                    payeBp = payeBp.add(journal.getMontantPercu());
                    soldeBp = bonAPayer.getMontant().subtract(payeBp);

                    bpJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                            soldeBp);

                    bpJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                            GeneralConst.Number.ONE);

                    bpJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                            journal.getBordereau());

                    bpJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                            ConvertDate.formatDateToStringOfFormat(journal.getDateBordereau(), "dd/MM/yyyy"));

                    bpJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                            journal.getMontantPercu());

                    bpJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                            journal.getMontantPercu());

                } else {
                    bpJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                            GeneralConst.Number.ZERO);

                    bpJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                            GeneralConst.EMPTY_STRING);

                    bpJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                            GeneralConst.EMPTY_STRING);

                    bpJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                            GeneralConst.EMPTY_STRING);

                    bpJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                            GeneralConst.Numeric.ZERO);

                    bpJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                            bonAPayer.getMontant());
                }

                dataReturn = bpJson.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadNotePerceptionByNumero(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String siteCode = GeneralConst.EMPTY_STRING;

        try {

            referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            siteCode = request.getParameter(NotePerceptionConst.ParamName.SITE_CODE);

            NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(
                    referenceDocument);

            if (notePerception != null) {

                npJson = new JsonObject();
                adressePersonne = new AdressePersonne();
                compteBancaire = new CompteBancaire();
                personne = new Personne();
                journal = new Journal();

                if (notePerception.getFractionnee() == 1) {
                    return dataReturn = GeneralConst.ResultCode.TITRE_PERCEPTION_FRACTIONNE;
                }

                boolean nonAutoriser = false;

                SiteBanque siteBanque = GeneralBusiness.getSiteBanqueBySite(siteCode);

                if (siteBanque != null && siteBanque.getBanque() != null) {

                    if (!siteBanque.getBanque().getCompteBancaireList().isEmpty()) {

                        for (CompteBancaire compteBancaire : siteBanque.getBanque().getCompteBancaireList()) {

                            if (notePerception.getCompteBancaire().trim().equals(compteBancaire.getCode().trim())
                                    && compteBancaire.getEtat() == GeneralConst.Numeric.ONE) {

                                nonAutoriser = true;

                                //System.out.println("compt Np : " + notePerception.getCompteBancaire());
                                //System.out.println("compt SB : " + compteBancaire.getCode());
                            }
                        }
                    }

                }

                /*npJson.addProperty(NotePerceptionConst.ParamName.PAIEMENT_AUTORISE,
                 nonAutoriser == true ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);*/
                if (!nonAutoriser) {
                    dataReturn = GeneralConst.ResultCode.PAIEMENT_NON_AUTORISE;
                } else {

                    personne = IdentificationBusiness.getPersonneByCode(
                            notePerception.getNoteCalcul().getPersonne());

                    npJson.addProperty(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT,
                            notePerception.getNumero().trim());

                    if (personne != null) {
                        npJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                personne.getFormeJuridique().getIntitule());

                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_CODE,
                                personne.getCode());

                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                personne.toString().toUpperCase());

                        npJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                        adressePersonne = IdentificationBusiness.getDefaultAdressByPersone(personne.getCode());

                        if (adressePersonne != null) {

                            npJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    adressePersonne.getAdresse().toString().toUpperCase());

                        } else {
                            npJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        npJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);

                    }

                    npJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                            NotePerceptionConst.TypeDocument.NP_LIBELLE);

                    npJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE,
                            NotePerceptionConst.TypeDocument.NP_CODE);

                    //String articleName = Tools.getArticleByNC(notePerception.getNoteCalcul());
                    String articleName = Tools.getArticleByNC_V2(notePerception.getNoteCalcul());

                    npJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            articleName);

                    compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                            notePerception.getCompteBancaire());

                    if (compteBancaire != null) {

                        npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                                compteBancaire.getCode());

                        /*npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIBELLE,
                         compteBancaire.toString());*/
                        npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                                compteBancaire.getBanque().getCode());

                        /* npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIBELLE,
                         compteBancaire.getBanque().getIntitule());*/
                        npJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                compteBancaire.getDevise().getCode());

                    } else {
                        npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                                GeneralConst.EMPTY_STRING);

                        /*npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIBELLE,
                         GeneralConst.EMPTY_STRING);*/
                        npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                                GeneralConst.EMPTY_STRING);

                        /*npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIBELLE,
                         GeneralConst.EMPTY_STRING);*/
                        npJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                GeneralConst.EMPTY_STRING);
                    }

                    npJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                            notePerception.getNetAPayer());

                    npJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                            notePerception.getPayer());

                    npJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                            notePerception.getSolde());

                    journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(notePerception.getNumero());

                    if (journal != null) {

                        npJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ONE);

                        npJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                journal.getBordereau());

                        npJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                ConvertDate.formatDateToStringOfFormat(journal.getDateBordereau(), "dd/MM/yyyy"));

                        npJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                journal.getMontantPercu());

                        switch (journal.getEtat()) {

                            case GeneralConst.Numeric.TWO:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et valider à la banque)");
                                break;

                            case GeneralConst.Numeric.THREE:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et en attente de validation à la banque)");
                                break;

                            default:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et valider à la regie)");
                                break;
                        }

                    } else {
                        npJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ZERO);

                        npJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                GeneralConst.EMPTY_STRING);
                    }

                    dataReturn = npJson.toString();
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadDeclarationByNumero(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        try {

            referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            userId = request.getParameter(GeneralConst.ParamName.USER_ID);
            String siteCode = request.getParameter(NotePerceptionConst.ParamName.SITE_CODE);

            List<RetraitDeclaration> declarationList = new ArrayList<>();

            declarationList = NotePerceptionBusiness.getListRetraitDeclarationByCodeDeclaration(
                    referenceDocument);

            if (!declarationList.isEmpty()) {

                declarationJson = new JsonObject();
                adressePersonne = new AdressePersonne();
                compteBancaire = new CompteBancaire();
                personne = new Personne();
                journal = new Journal();

                ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(declarationList.get(0).getFkAb());

                boolean nonAutoriser = false;

                SiteBanque siteBanque = GeneralBusiness.getSiteBanqueBySite(siteCode);

                if (siteBanque != null && siteBanque.getBanque() != null) {

                    if (!siteBanque.getBanque().getCompteBancaireList().isEmpty()) {

                        for (CompteBancaire compteBancaire : siteBanque.getBanque().getCompteBancaireList()) {

                            if (declarationList.get(0).getFkCompteBancaire().trim().equals(compteBancaire.getCode().trim())
                                    && compteBancaire.getEtat() == GeneralConst.Numeric.ONE) {

                                nonAutoriser = true;

                            }
                        }
                    }

                }

                if (!nonAutoriser) {

                    dataReturn = GeneralConst.ResultCode.PAIEMENT_NON_AUTORISE;

                } else {

                    //String bankAgent = agent.getSite().getSiteBanqueList().get(0).getBanque().getCode();
                    //String codeDevise = declarationList.get(0).getDevise();
                    String tarifLibelle = GeneralConst.EMPTY_STRING;
                    String articleLibelle = articleBudgetaire.getIntitule();

                    String articleComposite = GeneralConst.EMPTY_STRING;

                    if (!GeneralConst.TOUT.equalsIgnoreCase(articleBudgetaire.getTarif().getIntitule())) {
                        tarifLibelle = GeneralConst.TWO_POINTS.concat(articleBudgetaire.getTarif().getIntitule());
                    }

                    if (!articleBudgetaire.getTarif().getIntitule().equals("All")) {
                        articleComposite = articleLibelle.concat(GeneralConst.TWO_POINTS).concat(tarifLibelle);
                    } else {
                        articleComposite = articleLibelle;
                    }

                    String codeDeclaration = declarationList.get(0).getCodeDeclaration().trim();

                    personne = IdentificationBusiness.getPersonneByCode(declarationList.get(0).getFkAssujetti().trim());

                    declarationJson.addProperty(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT,
                            declarationList.get(0).getCodeDeclaration().trim());

                    if (personne != null) {

                        declarationJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                personne.getFormeJuridique().getIntitule());

                        declarationJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_CODE,
                                personne.getCode());

                        declarationJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                personne.toString().toUpperCase());

                        declarationJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                        adressePersonne = IdentificationBusiness.getDefaultAdressByPersone(personne.getCode());

                        if (adressePersonne != null) {

                            declarationJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    adressePersonne.getAdresse().toString().toUpperCase());

                        } else {
                            declarationJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        declarationJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        declarationJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                GeneralConst.EMPTY_STRING);
                        declarationJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    declarationJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                            NotePerceptionConst.TypeDocument.DECLARATION_LIBELLE);

                    declarationJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE,
                            NotePerceptionConst.TypeDocument.DECLARATION_CODE);

                    declarationJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                            declarationList.get(0).getFkCompteBancaire() != null ? declarationList.get(0).getFkCompteBancaire() : "");

                    declarationJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                            declarationList.get(0).getFkBanque() != null ? declarationList.get(0).getFkBanque() : "");

                    declarationJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                            articleComposite.toUpperCase());

                    declarationJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                            declarationList.get(0).getDevise());

                    BigDecimal sumTotalDeclaration = new BigDecimal(0);

                    List<JsonObject> detailDeclarationJsonList = new ArrayList<>();

                    for (RetraitDeclaration retraitDeclaration : declarationList) {

                        JsonObject detailDeclarationJson = new JsonObject();

                        PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(
                                retraitDeclaration.getFkPeriode());

                        if (periodeDeclaration != null) {

                            detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.PERIODE_DECLARATION,
                                    Tools.getPeriodeIntitule(periodeDeclaration.getDebut(),
                                            periodeDeclaration.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                            if (periodeDeclaration.getAssujetissement().getBien() != null) {

                                Bien bien = periodeDeclaration.getAssujetissement().getBien();

                                String bienName = bien.getIntitule().toUpperCase();
                                String bienDesciption = bien.getDescription() != null ? bien.getDescription() : GeneralConst.EMPTY_STRING;
                                String bienType = bien.getTypeBien().getIntitule().toUpperCase();
                                String bienAddresse = bien.getFkAdressePersonne().getAdresse().toString().toUpperCase();

                                String bienComposite = "Type : ".concat(bienType).concat("<br/>").concat("Intitulé : ").concat(
                                        bienName).concat("<br/>").concat("Description : ").concat(
                                                bienDesciption).concat("<br/>").concat("Adresse : ").concat(
                                                bienAddresse);

                                detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.BIEN_DECLARATION, bienComposite);

                            } else {
                                detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.BIEN_DECLARATION,
                                        GeneralConst.EMPTY_STRING);

                                detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.PERIODE_DECLARATION,
                                        GeneralConst.EMPTY_STRING);
                            }

                        } else {
                            detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.BIEN_DECLARATION,
                                    GeneralConst.EMPTY_STRING);
                            detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.PERIODE_DECLARATION,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (retraitDeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

                            detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                                    retraitDeclaration.getMontant());

                            sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getMontant());

                        } else {

                            if (retraitDeclaration.getPenaliteRemise().floatValue() > 0) {

                                detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                                        retraitDeclaration.getPenaliteRemise());

                                sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getPenaliteRemise());

                            } else {
                                detailDeclarationJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                                        retraitDeclaration.getMontant());

                                sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getMontant());
                            }
                        }

                        detailDeclarationJsonList.add(detailDeclarationJson);

                    }

                    Bordereau bordereau = GeneralBusiness.getBordereauByDeclaration(codeDeclaration);

                    String mesageContent = GeneralConst.EMPTY_STRING;

                    if (bordereau != null) {

                        declarationJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ONE);

                        declarationJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                bordereau.getNumeroBordereau());

                        declarationJson.addProperty(PaiementConst.ParamName.ID_ETAT_PAIEMENT,
                                bordereau.getEtat().getCode());

                        switch (bordereau.getEtat().getCode()) {

                            case 3:
                                mesageContent = "Cette déclaration est déjà payée et reste en attente de traitement à la banque";
                                break;
                            case 2:
                                mesageContent = "Cette déclaration est déjà payée et confirmer à la banque";
                                break;
                            default:
                                mesageContent = "";
                                break;

                        }

                        declarationJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT, mesageContent);

                        declarationJson.addProperty(PaiementConst.ParamName.COMPTE_BANCAIRE_BORDEREAU,
                                declarationList.get(0).getFkCompteBancaire() == null
                                        ? bordereau.getCompteBancaire().getCode() : declarationList.get(0).getFkCompteBancaire());

                        declarationJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                ConvertDate.formatDateToStringOfFormat(bordereau.getDatePaiement(), "dd/MM/YYYY"));

                        declarationJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                sumTotalDeclaration);

                    } else {
                        declarationJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ZERO);

                        declarationJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                GeneralConst.EMPTY_STRING);

                        declarationJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                GeneralConst.EMPTY_STRING);

                        declarationJson.addProperty(PaiementConst.ParamName.ID_ETAT_PAIEMENT,
                                GeneralConst.EMPTY_STRING);

                        declarationJson.addProperty(PaiementConst.ParamName.COMPTE_BANCAIRE_BORDEREAU,
                                GeneralConst.EMPTY_STRING);

                        declarationJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                GeneralConst.EMPTY_STRING);

                        declarationJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                GeneralConst.EMPTY_STRING);
                    }

                    declarationJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                            sumTotalDeclaration);

                    declarationJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                            0);

                    declarationJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                            sumTotalDeclaration);

                    declarationJson.addProperty(NotePerceptionConst.ParamName.DETAIL_DECLARATION_LIST,
                            detailDeclarationJsonList.toString());

                    dataReturn = declarationJson.toString();
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadVoletyNumeroNP(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String siteCode = GeneralConst.EMPTY_STRING;
        String volet = GeneralConst.EMPTY_STRING;
        ArticleBudgetaire articleBudg;

        try {

            referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            siteCode = request.getParameter(NotePerceptionConst.ParamName.SITE_CODE);
            volet = request.getParameter("volet");

            NotePerceptionMairie notePerception = NotePerceptionBusiness.getNotePerceptionMarieByCodeVolet(
                    referenceDocument, volet);

            if (notePerception != null) {

                npJson = new JsonObject();
                adressePersonne = new AdressePersonne();
                compteBancaire = new CompteBancaire();
                personne = new Personne();
                journal = new Journal();

                boolean nonAutoriser = true;

                SiteBanque siteBanque = GeneralBusiness.getSiteBanqueBySite(siteCode);
                journal = AcquitLiberatoireBusiness.getJournalByDocumentApureVolet(notePerception.getNumeroNp(), volet);

                if (siteBanque != null && siteBanque.getBanque() != null) {

                    if (!siteBanque.getBanque().getCompteBancaireList().isEmpty()) {

                        for (CompteBancaire compteBancaire : siteBanque.getBanque().getCompteBancaireList()) {

                            if (notePerception.getFkCompteBancaire().trim().equals(compteBancaire.getCode().trim())
                                    && compteBancaire.getEtat() == GeneralConst.Numeric.ONE) {

                                nonAutoriser = true;

                            }
                        }
                    }

                }

                if (volet.equals(GeneralConst.Number.TWO)) {
                    if (journal != null && journal.getVolet().equals(GeneralConst.Number.ONE)) {
                        nonAutoriser = true;
                    } else {
                        nonAutoriser = false;
                    }
                }

                if (!nonAutoriser) {
                    dataReturn = GeneralConst.ResultCode.PAIEMENT_NON_AUTORISE;
                } else {

                    personne = IdentificationBusiness.getPersonneByCode(
                            notePerception.getFkPersonne());

                    npJson.addProperty(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT,
                            notePerception.getNumeroNp().trim());

                    if (personne != null) {
                        npJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                personne.getFormeJuridique().getIntitule());

                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_CODE,
                                personne.getCode());

                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                personne.toString().toUpperCase());

                        npJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                        adressePersonne = IdentificationBusiness.getDefaultAdressByPersone(personne.getCode());

                        if (adressePersonne != null) {

                            npJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    adressePersonne.getAdresse().toString().toUpperCase());

                        } else {
                            npJson.addProperty(NotePerceptionConst.ParamName.ADRESSE_PERSONNE,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        npJson.addProperty(NotePerceptionConst.ParamName.TYPE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(NotePerceptionConst.ParamName.ASSUJETTI_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);

                    }

                    npJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_LIBELLE,
                            NotePerceptionConst.TypeDocument.NP_LIBELLE);

                    npJson.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE,
                            NotePerceptionConst.TypeDocument.NP_CODE);

                     articleBudg = GestionArticleBudgetaireBusiness.getArticleBudgetaireByAG(notePerception.getFkAb());

                    if (articleBudg != null) {
                        npJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                                articleBudg.getIntitule());
                    } else {
                        npJson.addProperty(NotePerceptionConst.ParamName.ARTICLE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                            notePerception.getFkCompteBancaire());

                    if (compteBancaire != null) {

                        npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                                compteBancaire.getCode());

                        /*npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIBELLE,
                         compteBancaire.toString());*/
                        npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                                compteBancaire.getBanque().getCode());

                        /* npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIBELLE,
                         compteBancaire.getBanque().getIntitule());*/
                        npJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                compteBancaire.getDevise().getCode());

                    } else {
                        npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE,
                                GeneralConst.EMPTY_STRING);

                        /*npJson.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_LIBELLE,
                         GeneralConst.EMPTY_STRING);*/
                        npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_CODE,
                                GeneralConst.EMPTY_STRING);

                        /*npJson.addProperty(NotePerceptionConst.ParamName.BANQUE_LIBELLE,
                         GeneralConst.EMPTY_STRING);*/
                        npJson.addProperty(NotePerceptionConst.ParamName.DEVISE_CODE,
                                GeneralConst.EMPTY_STRING);
                    }

                    npJson.addProperty(NotePerceptionConst.ParamName.NET_A_PAYER,
                            notePerception.getMontant());
                    npJson.addProperty(NotePerceptionConst.ParamName.PAYER,
                            GeneralConst.Numeric.ZERO);

                    npJson.addProperty(NotePerceptionConst.ParamName.SOLDE,
                            GeneralConst.Numeric.ZERO);

                    if (journal != null && journal.getVolet().equals(volet)) {

                        npJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ONE);

                        npJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                journal.getBordereau());

                        npJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                ConvertDate.formatDateToStringOfFormat(journal.getDateBordereau(), "dd/MM/yyyy"));

                        npJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                journal.getMontantPercu());

                        switch (journal.getEtat()) {

                            case GeneralConst.Numeric.TWO:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et valider à la banque)");
                                break;

                            case GeneralConst.Numeric.THREE:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et en attente de validation à la banque)");
                                break;

                            default:

                                npJson.addProperty(PaiementConst.ParamName.MESSAGE_PAIEMENT,
                                        "(Paiement déjà effectuer et valider à la regie)");
                                break;
                        }

                    } else {
                        npJson.addProperty(PaiementConst.ParamName.PAIEMENT_EXISTING,
                                GeneralConst.Number.ZERO);

                        npJson.addProperty(PaiementConst.ParamName.BORDEREAU,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(PaiementConst.ParamName.DATE_BORDERAU,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(PaiementConst.ParamName.MONTANT_PAYE,
                                GeneralConst.EMPTY_STRING);
                    }

                    dataReturn = npJson.toString();
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String saveJournal(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String responseSaveSuiviComptable;
        String responseSaveJournal;

        try {

            String referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            String referencePaiement = request.getParameter(PaiementConst.ParamName.REFERENCE_PAIEMENT);
            String typeDocument = request.getParameter(NotePerceptionConst.ParamName.TYPE_DOCUMENT_CODE);
            String datePaiement = request.getParameter(PaiementConst.ParamName.DATE_PAIEMENT);
            String userId = request.getParameter(GeneralConst.ParamName.USER_ID);
            String accountBank = request.getParameter("accountBank");
            String bankCode = request.getParameter("bankCode");
            String montant = request.getParameter("montant");

            Journal journal = new Journal();
            SuiviComptableDeclaration suiviComptableDeclaration = new SuiviComptableDeclaration();
            NotePerception notePerception = new NotePerception();
            Amr amr = new Amr();
            BonAPayer bonAPayer = new BonAPayer();
            Agent agent = PaiementBusiness.getAgentByCode(Integer.valueOf(userId));

            String compteBancaire = GeneralConst.EMPTY_STRING;
            String NcFiche = GeneralConst.EMPTY_STRING;
            String codeAssujetti = GeneralConst.EMPTY_STRING;
            String deviseFiche = GeneralConst.EMPTY_STRING;

            BigDecimal montantPaye = new BigDecimal("0");
            montantPaye = montantPaye.add(BigDecimal.valueOf(Double.valueOf(montant)));

            CompteBancaire cb = AcquitLiberatoireBusiness.getCompteBancaireByCode(accountBank);

            if (cb != null) {
                deviseFiche = cb.getDevise().getCode();
            }

            switch (typeDocument) {

                case NotePerceptionConst.TypeDocument.NP_CODE:

                    notePerception = NotePerceptionBusiness.getNotePerceptionByCode(referenceDocument);

                    if (notePerception != null) {

                        compteBancaire = notePerception.getCompteBancaire();

                        if (PaiementBusiness.checkReferePaiementExists(
                                referencePaiement.trim(),
                                compteBancaire.trim(), typeDocument)) {

                            dataReturn = PaiementConst.ResultCode.BORDEREAU_EXISTING;

                        } else {

                            journal.setDocumentApure(referenceDocument.trim());
                            journal.setSite(agent.getSite());
                            journal.setTypeDocument(typeDocument);
                            journal.setMontant(montantPaye);
                            journal.setCompteBancaire(new CompteBancaire(compteBancaire));
                            journal.setBordereau(referencePaiement.trim());
                            journal.setDateBordereau(Tools.formatStringFullToDate(datePaiement));
                            journal.setMontantApurre(BigDecimal.ZERO);
                            journal.setNumeroReleveBancaire(GeneralConst.EMPTY_STRING);
                            journal.setDateReleveBancaire(Tools.formatStringFullToDate(""));
                            journal.setAgentCreat(agent.getCode() + GeneralConst.EMPTY_STRING);
                            journal.setAgentApurement(null);
                            journal.setPersonne(new Personne(notePerception.getNoteCalcul().getPersonne()));
                            journal.setTaux(BigDecimal.ONE);
                            journal.setModePaiement("BANQUE");
                            journal.setMontantPercu(montantPaye);
                            journal.setNumeroAttestationPaiement(GeneralConst.EMPTY_STRING);
                            journal.setDevise(new Devise(notePerception.getDevise()));

                            suiviComptableDeclaration.setNc(notePerception.getNoteCalcul().getNumero());
                            suiviComptableDeclaration.setDevise(notePerception.getDevise());

                            responseSaveJournal = saveInJournal(journal,
                                    NotePerceptionConst.TypeDocument.NP_CODE);

                            switch (responseSaveJournal.trim()) {

                                case GeneralConst.ResultCode.SUCCES_OPERATION:

                                    responseSaveSuiviComptable = saveSuiviComptable(
                                            suiviComptableDeclaration,
                                            typeDocument.trim(),
                                            referenceDocument);

                                    dataReturn = responseSaveSuiviComptable;

                                    break;
                                default:
                                    dataReturn = responseSaveJournal;
                                    break;
                            }
                        }
                    }
                    break;
                case NotePerceptionConst.TypeDocument.AMR_CODE:

                    amr = NotePerceptionBusiness.getAmrByCode(referenceDocument);

                    if (amr != null) {

                        if (!amr.getFichePriseCharge().getDetailFichePriseChargeList().isEmpty()) {

                            if (amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc() != null) {

                                NcFiche = amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                            }
                        }

                        if (amr.getFichePriseCharge() != null) {

                            if (amr.getFichePriseCharge().getPersonne() != null) {
                                codeAssujetti = amr.getFichePriseCharge().getPersonne().getCode();
                            }

                            if (deviseFiche.isEmpty()) {

                                if (!amr.getFichePriseCharge().getDevise().isEmpty()
                                        && amr.getFichePriseCharge().getDevise() != null) {

                                    deviseFiche = amr.getFichePriseCharge().getDevise();
                                }
                            }

                        }

                        if (PaiementBusiness.checkReferePaiementExists(
                                referencePaiement.trim(),
                                accountBank.trim(), typeDocument)) {

                            dataReturn = PaiementConst.ResultCode.BORDEREAU_EXISTING;

                        } else {

                            journal.setDocumentApure(referenceDocument.trim());
                            journal.setSite(agent.getSite());
                            journal.setTypeDocument(typeDocument);
                            journal.setMontant(amr.getNetAPayer());
                            journal.setCompteBancaire(new CompteBancaire(accountBank));
                            journal.setBordereau(referencePaiement.trim());
                            journal.setDateBordereau(Tools.formatStringFullToDate(datePaiement));
                            journal.setMontantApurre(BigDecimal.ZERO);
                            journal.setNumeroReleveBancaire(GeneralConst.EMPTY_STRING);
                            journal.setDateReleveBancaire(Tools.formatStringFullToDate(""));
                            journal.setAgentCreat(agent.getCode() + GeneralConst.EMPTY_STRING);
                            journal.setAgentApurement(null);
                            journal.setPersonne(new Personne(codeAssujetti));
                            journal.setTaux(BigDecimal.ONE);
                            journal.setModePaiement("BANQUE");
                            journal.setMontantPercu(amr.getNetAPayer());
                            journal.setNumeroAttestationPaiement(GeneralConst.EMPTY_STRING);
                            journal.setDevise(new Devise(deviseFiche));

                            suiviComptableDeclaration.setNc(NcFiche);
                            suiviComptableDeclaration.setDevise(deviseFiche);

                            responseSaveJournal = saveInJournal(journal,
                                    NotePerceptionConst.TypeDocument.AMR_CODE);

                            switch (responseSaveJournal.trim()) {

                                case GeneralConst.ResultCode.SUCCES_OPERATION:

                                    responseSaveSuiviComptable = saveSuiviComptable(
                                            suiviComptableDeclaration,
                                            typeDocument.trim(),
                                            referenceDocument);

                                    dataReturn = responseSaveSuiviComptable;

                                    break;
                                default:
                                    dataReturn = responseSaveJournal;
                                    break;
                            }
                        }
                    }

                    break;

                case NotePerceptionConst.TypeDocument.BP_CODE:

                    bonAPayer = PoursuiteBusiness.getBonAPayerByCode(referenceDocument);

                    if (bonAPayer != null) {

                        compteBancaire = bonAPayer.getFkCompte().getCode();

                        if (!bonAPayer.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().isEmpty()) {

                            if (!bonAPayer.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc().isEmpty()
                                    && bonAPayer.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc() != null) {

                                NcFiche = bonAPayer.getFkAmr().getFichePriseCharge().getDetailFichePriseChargeList().get(0).getNc();
                            }
                        }

                        if (deviseFiche.isEmpty()) {

                            if (bonAPayer.getFkAmr().getFichePriseCharge() != null) {

                                if (!bonAPayer.getFkAmr().getFichePriseCharge().getDevise().isEmpty()
                                        && bonAPayer.getFkAmr().getFichePriseCharge().getDevise() != null) {

                                    deviseFiche = bonAPayer.getFkAmr().getFichePriseCharge().getDevise();
                                }
                            }

                        }

                        if (PaiementBusiness.checkReferePaiementExists(
                                referencePaiement.trim(),
                                compteBancaire.trim(), typeDocument)) {

                            dataReturn = PaiementConst.ResultCode.BORDEREAU_EXISTING;

                        } else {

                            journal.setDocumentApure(referenceDocument.trim());
                            journal.setSite(agent.getSite());
                            journal.setTypeDocument(typeDocument);
                            journal.setMontant(bonAPayer.getMontant());
                            journal.setCompteBancaire(new CompteBancaire(compteBancaire));
                            journal.setBordereau(referencePaiement.trim());
                            journal.setDateBordereau(Tools.formatStringFullToDate(datePaiement));
                            journal.setMontantApurre(BigDecimal.ZERO);
                            journal.setNumeroReleveBancaire(GeneralConst.EMPTY_STRING);
                            journal.setDateReleveBancaire(Tools.formatStringFullToDate(""));
                            journal.setAgentCreat(agent.getCode() + GeneralConst.EMPTY_STRING);
                            journal.setAgentApurement(null);
                            journal.setPersonne(bonAPayer.getFkPersonne());
                            journal.setTaux(BigDecimal.ONE);
                            journal.setModePaiement("BANQUE");
                            journal.setMontantPercu(bonAPayer.getMontant());
                            journal.setNumeroAttestationPaiement(GeneralConst.EMPTY_STRING);
                            journal.setDevise(new Devise(deviseFiche));

                            suiviComptableDeclaration.setNc(NcFiche);
                            suiviComptableDeclaration.setDevise(deviseFiche);

                            responseSaveJournal = saveInJournal(journal,
                                    NotePerceptionConst.TypeDocument.BP_CODE);

                            switch (responseSaveJournal.trim()) {

                                case GeneralConst.ResultCode.SUCCES_OPERATION:

                                    responseSaveSuiviComptable = saveSuiviComptable(
                                            suiviComptableDeclaration,
                                            typeDocument.trim(),
                                            referenceDocument);

                                    dataReturn = responseSaveSuiviComptable;

                                    break;
                                default:
                                    dataReturn = responseSaveJournal;
                                    break;
                            }
                        }
                    }

                    break;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String saveDeclaration(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

            String referenceDocument = request.getParameter(NotePerceptionConst.ParamName.RERERENCE_DOCUMENT);
            String referencePaiement = request.getParameter(PaiementConst.ParamName.REFERENCE_PAIEMENT);
            String datePaiement = request.getParameter(PaiementConst.ParamName.DATE_PAIEMENT);
            String userId = request.getParameter(GeneralConst.ParamName.USER_ID);
            String accountBank = request.getParameter(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE);

            Bordereau bordereau = new Bordereau();
            Agent agent = PaiementBusiness.getAgentByCode(Integer.valueOf(userId));
            List<RetraitDeclaration> declarationList = new ArrayList<>();

            String assujettiCode = GeneralConst.EMPTY_STRING;
            String centreCode = GeneralConst.EMPTY_STRING;

            declarationList = NotePerceptionBusiness.getListRetraitDeclarationByCodeDeclaration(
                    referenceDocument);

            BigDecimal sumTotalDeclaration = new BigDecimal(0);

            if (!declarationList.isEmpty()) {

                assujettiCode = declarationList.get(0).getFkAssujetti();
                Agent agentDeclaration = PaiementBusiness.getAgentByCode(Integer.valueOf(declarationList.get(0).getFkAgentCreate()));

                if (agentDeclaration != null) {
                    centreCode = agentDeclaration.getSite().getCode();
                }

                for (RetraitDeclaration retraitDeclaration : declarationList) {

                    accountBank = retraitDeclaration.getFkCompteBancaire();

                    if (retraitDeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

                        sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getMontant());

                    } else {

                        if (retraitDeclaration.getPenaliteRemise().floatValue() > 0) {
                            sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getPenaliteRemise());
                        } else {
                            sumTotalDeclaration = sumTotalDeclaration.add(retraitDeclaration.getMontant());
                        }
                    }

                }
            }

            if (PaiementBusiness.checkReferePaiementExists(
                    referencePaiement.trim(),
                    accountBank, NotePerceptionConst.TypeDocument.DECLARATION_CODE)) {

                dataReturn = PaiementConst.ResultCode.BORDEREAU_EXISTING;

            } else {

                bordereau.setNumeroBordereau(referencePaiement);
                bordereau.setNomComplet(assujettiCode);
                bordereau.setDatePaiement(ConvertDate.formatDate(
                        ConvertDate.getValidFormatDateString(datePaiement)));
                bordereau.setTotalMontantPercu(sumTotalDeclaration);
                bordereau.setCompteBancaire(new CompteBancaire(accountBank));
                bordereau.setAgent(agent);
                bordereau.setDateCreate(new Date());
                bordereau.setEtat(new Etat(GeneralConst.Numeric.THREE));
                bordereau.setNumeroDeclaration(referenceDocument);
                bordereau.setCentre(centreCode);

                result = PaiementBusiness.createDeclaration(bordereau, declarationList);

                if (result) {
                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    private String saveSuiviComptable(
            SuiviComptableDeclaration scd,
            String typeDocument,
            String numeroDocument) {

        String valueReturn = GeneralConst.EMPTY_STRING;

        try {
            int counter = 0;
            boolean result = false;
            HashMap<String, Object[]> bulkQuery = new HashMap<>();

            switch (typeDocument.trim()) {

                case PaiementConst.ParamNameDocument.NP:
                case PaiementConst.ParamNameDocument.BAP:

                    counter++;
                    bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION,
                            new Object[]{
                                scd.getNc().trim(),
                                numeroDocument.trim(),
                                "Paiement à la banque",
                                Float.valueOf(GeneralConst.Number.ZERO),
                                Float.valueOf(GeneralConst.Number.ZERO),
                                "Paiement à la banque".concat(
                                        GeneralConst.TWO_POINTS).concat(numeroDocument.trim()),
                                scd.getDevise().trim()
                            });

                    result = TaxationBusiness.executeQueryBulkInsert(bulkQuery);

                    if (result) {
                        valueReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        valueReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
                //case PaiementConst.ParamNameDocument.AMR:
                //break;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return valueReturn;
    }

    private String saveInJournal(Journal journal, String typeDocument) {
        boolean result1;
        String result2;

        try {

            result1 = PaiementBusiness.saveVoletJournal(journal, typeDocument);

            if (result1) {
                result2 = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result2 = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result2;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
