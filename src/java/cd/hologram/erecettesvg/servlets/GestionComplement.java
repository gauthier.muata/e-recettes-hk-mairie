/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.GestionComplementBusiness;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.AbComplementBien;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.CategorieTypeComplement;
import cd.hologram.erecettesvg.models.ComplementForme;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.TypeBien;
import cd.hologram.erecettesvg.models.TypeComplement;
import cd.hologram.erecettesvg.models.TypeComplementBien;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.*;
import java.math.BigInteger;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "GestionComplement", urlPatterns = {"/complement_servlet"})
public class GestionComplement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "getComplementForme":
                result = getComplementFormeByCode(request);
                break;
            case "getComplementType":
                result = getComplementTypeByIntitule(request);
                break;
            case "affecterComplementFormeJuridique":
                result = affecterComplementForme(request);
                break;
            case "desactiverComplement":
                result = desactiverComplementForme(request);
                break;
            case "getComplementTypeBien":
                result = loadTypeComplementBiens(request);
                break;
            case "getArticleNonAffecterComplement":
                result = loadArticleNonComplement(request);
                break;
            case "affecterComplementTypeBien":
                result = affecterComplementTypeBien(request);
                break;
            case "getComplementNonAffected":
                result = getComplementNonAffected(request);
                break;
            case "affecterComplementBien":
                result = affecterComplementBien(request);
                break;
            case "desactiverComplementBien":
                result = desactiverComplementBien(request);
                break;
            case "getTypeComplementBien":
                result = loadTypesComplementsBien(request);
                break;
            case "affecterArticleComplement":
                result = affecterArticleComplement(request);
                break;
        }
        out.print(result);

    }

    private String getComplementFormeByCode(HttpServletRequest request) {

        String result;
        String codeComplement = request.getParameter("codeComplement");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<ComplementForme> complementForme = GestionComplementBusiness.getComplementFormeByType(codeComplement);
            if (!complementForme.isEmpty()) {
                for (ComplementForme cplForme : complementForme) {

                    JSONObject object = new JSONObject();

                    object.put("code", cplForme.getCode());

                    object.put("intitule", cplForme.getComplement() == null
                            ? GeneralConst.EMPTY_STRING
                            : cplForme.getComplement().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : cplForme.getComplement().getIntitule());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getComplementTypeByIntitule(HttpServletRequest request) {

        String result;
        String intituleComplementType = request.getParameter("intituleArticle");
        String codeFormeJuridique = request.getParameter("codeFormeJuridique");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<TypeComplement> typeComplement = GestionComplementBusiness.getTypeComplementByIntitule(
                    intituleComplementType, codeFormeJuridique);

            if (!typeComplement.isEmpty()) {
                for (TypeComplement typeCompl : typeComplement) {

                    JSONObject object = new JSONObject();

                    object.put("code", typeCompl.getCode() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getCode());

                    object.put("intitule", typeCompl.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getIntitule());

                    if (!typeCompl.getType().equals("")) {

                        CategorieTypeComplement objCateg = GestionComplementBusiness.getCategorieTypeComplementByCode(typeCompl.getType());

                        if (objCateg != null) {
                            object.put("intituleCategorie", objCateg.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : objCateg.getIntitule());
                        } else {
                            object.put("intituleCategorie", GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        object.put("intituleCategorie", GeneralConst.EMPTY_STRING);
                    }

                    object.put("type", typeCompl.getType() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getType());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String affecterComplementForme(HttpServletRequest request) {

        String result = null;

        try {

            JSONArray jsonComplementFormeList = new JSONArray(request.getParameter("complementObjetList"));

            List<ComplementForme> complementList = new ArrayList<>();

            for (int i = 0; i < jsonComplementFormeList.length(); i++) {

                JSONObject jsonobject = jsonComplementFormeList.getJSONObject(i);

                ComplementForme objetComplem = new ComplementForme();

                String code = jsonobject.getString("code");
                String codeComplementForme = jsonobject.getString("codeComplementForme");

                objetComplem.setFormeJuridique(new FormeJuridique(codeComplementForme));
                objetComplem.setComplement(new TypeComplement(code));
                objetComplem.setEtat(true);

                complementList.add(objetComplem);
            }

            if (GestionComplementBusiness.affecterComplementForme(complementList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String desactiverComplementForme(HttpServletRequest request) {

        String result = null;

        try {

            String codeTypeComplement = request.getParameter("codeTypeComplement");

            ComplementForme cpl = new ComplementForme();
            cpl.setCode(codeTypeComplement);
            cpl.setEtat(false);

            if (GestionComplementBusiness.desactiverComplementForme(cpl)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadTypeComplementBiens(HttpServletRequest request) {

        String result;
        String codeTypeBien = request.getParameter("codeComplement");

        List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

        try {

            List<TypeComplementBien> typeComplementBiens = AssujettissementBusiness
                    .getTypeComplementBienByTypeBien(codeTypeBien);

            if (typeComplementBiens != null) {
                int counter = 0;
                for (TypeComplementBien typeComplementBien : typeComplementBiens) {

                    JSONObject jsonTypeComplementBien = new JSONObject();
                    TypeComplement typeComplement = typeComplementBien.getComplement();

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getCode());

                    AbComplementBien artComplBien = GestionComplementBusiness.getArticleComplementByCode(typeComplement.getCode());

                    if (artComplBien != null) {
                        jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE,
                                artComplBien.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : artComplBien.getArticleBudgetaire().getIntitule());
                        
                        jsonTypeComplementBien.put("codeAbComplement",
                                artComplBien.getArticleBudgetaire() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : artComplBien.getCode());
                    } else {
                        jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE, GeneralConst.EMPTY_STRING);
                        jsonTypeComplementBien.put("codeAbComplement", GeneralConst.EMPTY_STRING);
                    }

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.OBJET_INTERACTION,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.VALEUR_PREDEFINIE,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getValeurPredefinie());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.INPUT_VALUE,
                            Assujettissement.createControl(typeComplementBien, counter));

                    jsonTypeComplementBiens.add(jsonTypeComplementBien);
                    counter++;
                }

                result = jsonTypeComplementBiens.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadArticleNonComplement(HttpServletRequest request) {

        String valueSearch, codeComplement, codeArticle = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            codeComplement = request.getParameter("codeComplement");
            codeArticle = request.getParameter("codeArticle");

JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

                List<ArticleBudgetaire> listArticleBudgetaire = GestionComplementBusiness.loadArticleNonAffecterComplement(
                        valueSearch, codeComplement, codeArticle);

                if (listArticleBudgetaire != null && !listArticleBudgetaire.isEmpty()) {

                    for (ArticleBudgetaire articleBudgetaire : listArticleBudgetaire) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE_GENERIQUE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getCode());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE_GENERIQUE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getIntitule());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_SERVICE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette() == null ? GeneralConst.EMPTY_STRING
                                                : articleBudgetaire.getArticleGenerique().getServiceAssiette().getCode());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_SERVICE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette() == null ? GeneralConst.EMPTY_STRING
                                                : articleBudgetaire.getArticleGenerique().getServiceAssiette().getIntitule());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_FORME_JURIDIQUE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getFormeJuridique() == null ? GeneralConst.EMPTY_STRING
                                                : articleBudgetaire.getArticleGenerique().getFormeJuridique().getCode());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_FORME_JURIDIQUE,
                                articleBudgetaire.getArticleGenerique() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getFormeJuridique() == null ? GeneralConst.EMPTY_STRING
                                                : articleBudgetaire.getArticleGenerique().getFormeJuridique().getIntitule());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.CODE_ARTICLE,
                                articleBudgetaire.getCode() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getCode());

                        jsonObject.addProperty(GestionArticleBudgetaireConst.ParamName.INTITULE_ARTICLE,
                                articleBudgetaire.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getIntitule());

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }


            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String affecterComplementTypeBien(HttpServletRequest request) {

        String result = null;

        try {

            JSONArray jsonComplementFormeList = new JSONArray(request.getParameter("complementObjetList"));

            List<ComplementForme> complementList = new ArrayList<>();

            for (int i = 0; i < jsonComplementFormeList.length(); i++) {

                JSONObject jsonobject = jsonComplementFormeList.getJSONObject(i);

                ComplementForme objetComplem = new ComplementForme();

                String code = jsonobject.getString("code");
                String codeComplementForme = jsonobject.getString("codeComplementForme");

                objetComplem.setFormeJuridique(new FormeJuridique(codeComplementForme));
                objetComplem.setComplement(new TypeComplement(code));
                objetComplem.setEtat(true);

                complementList.add(objetComplem);
            }

            if (GestionComplementBusiness.affecterComplementForme(complementList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    private String getComplementNonAffected(HttpServletRequest request) {
        String result;
        String intituleComplementType = request.getParameter("intituleComplement");
        String codeComplementBien = request.getParameter("codeComplementBien");

        List<JSONObject> jSONObject = new ArrayList<>();

        try {

            List<TypeComplement> typeComplement = GestionComplementBusiness.getTypeComplementBien(
                    intituleComplementType, codeComplementBien);

            if (!typeComplement.isEmpty()) {
                for (TypeComplement typeCompl : typeComplement) {

                    JSONObject object = new JSONObject();

                    object.put("code", typeCompl.getCode() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getCode());

                    object.put("intitule", typeCompl.getIntitule() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getIntitule());

                    if (!typeCompl.getType().equals("")) {

                        CategorieTypeComplement objCateg = GestionComplementBusiness.getCategorieTypeComplementByCode(typeCompl.getType());

                        if (objCateg != null) {
                            object.put("intituleCategorie", objCateg.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : objCateg.getIntitule());
                        } else {
                            object.put("intituleCategorie", GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        object.put("intituleCategorie", GeneralConst.EMPTY_STRING);
                    }

                    object.put("type", typeCompl.getType() == null
                            ? GeneralConst.EMPTY_STRING
                            : typeCompl.getType());

                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String affecterComplementBien(HttpServletRequest request) {

        String result = null;

        try {

            JSONArray jsonTypeComplementBienList = new JSONArray(request.getParameter("complementObjetListBien"));

            List<TypeComplementBien> typeComplementBienList = new ArrayList<>();

            for (int i = 0; i < jsonTypeComplementBienList.length(); i++) {

                JSONObject jsonobject = jsonTypeComplementBienList.getJSONObject(i);

                TypeComplementBien objetTypeComplemBien = new TypeComplementBien();

                String codeComplementT = jsonobject.getString("code");
                String codeTypeBien = jsonobject.getString("codeComplementTypeBien");

                objetTypeComplemBien.setComplement(new TypeComplement(codeComplementT));
                objetTypeComplemBien.setTypeBien(new TypeBien(codeTypeBien));
                objetTypeComplemBien.setEtat(true);

                typeComplementBienList.add(objetTypeComplemBien);
            }

            if (GestionComplementBusiness.affecterComplementBien(typeComplementBienList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String desactiverComplementBien(HttpServletRequest request) {

        String result = null;

        try {

            String codeTypeComplementBien = request.getParameter("codeTypeComplementBien");

            if (GestionComplementBusiness.desactiverComplementBien(codeTypeComplementBien)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadTypesComplementsBien(HttpServletRequest request) {

        String result;
        String codeTypeBien = request.getParameter("codeComplement");

        List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

        try {

            List<TypeComplementBien> typeComplementBiens = AssujettissementBusiness
                    .getTypeComplementBienByTypeBien(codeTypeBien);

            if (typeComplementBiens != null) {
                int counter = 0;
                for (TypeComplementBien typeComplementBien : typeComplementBiens) {

                    JSONObject jsonTypeComplementBien = new JSONObject();

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                            typeComplementBien == null ? GeneralConst.EMPTY_STRING : typeComplementBien.getCode());

                    if (typeComplementBien.getAbComplementBienList().size() > 0) {
                        AbComplementBien artComplBien = GestionComplementBusiness.getArticleComplementByCode(
                                typeComplementBien.getAbComplementBienList().get(0).getCode());

                        if (artComplBien != null) {
                            jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE,
                                    artComplBien.getArticleBudgetaire() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : artComplBien.getArticleBudgetaire().getIntitule());

                            jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                    artComplBien.getArticleBudgetaire() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : artComplBien.getArticleBudgetaire().getCode());
                             jsonTypeComplementBien.put("codeAbComplement",
                                artComplBien.getCode() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : artComplBien.getCode());
                        } else {
                            jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE, GeneralConst.EMPTY_STRING);
                            jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                            jsonTypeComplementBien.put("codeAbComplement",GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE, GeneralConst.EMPTY_STRING);
                        jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                        jsonTypeComplementBien.put("codeAbComplement",GeneralConst.EMPTY_STRING);
                    }

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                            typeComplementBien == null ? GeneralConst.EMPTY_STRING
                                    : typeComplementBien.getComplement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : typeComplementBien.getComplement().getIntitule());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.OBJET_INTERACTION,
                            typeComplementBien == null ? GeneralConst.EMPTY_STRING
                                    : typeComplementBien.getComplement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : typeComplementBien.getComplement().getObjetInteraction());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.VALEUR_PREDEFINIE,
                            typeComplementBien == null ? GeneralConst.EMPTY_STRING
                                    : typeComplementBien.getComplement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : typeComplementBien.getComplement().getValeurPredefinie());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.INPUT_VALUE,
                            Assujettissement.createControl(typeComplementBien, counter));

                    jsonTypeComplementBiens.add(jsonTypeComplementBien);
                    counter++;

                }

                result = jsonTypeComplementBiens.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
     public String affecterArticleComplement(HttpServletRequest request) {

        String result = null;

        try {

            String codeAbCompl = request.getParameter("codeAbCompl");
            String codeArticle = request.getParameter("codeArticle");
            String codeComplementArticle = request.getParameter("codeComplementArticle");

            AbComplementBien abCompl = new AbComplementBien();
            abCompl.setArticleBudgetaire(new ArticleBudgetaire(codeArticle)); 
            abCompl.setTypeComplementBien(new TypeComplementBien(codeComplementArticle));
            if (!codeAbCompl.equals("")){
               abCompl.setCode(codeAbCompl);  
            }else{
                abCompl.setCode(null); 
            }
            
            abCompl.setEtat(BigInteger.ONE); 
            abCompl.setDateCreat(new Date());

            if (GestionComplementBusiness.affecterArticleComplement(abCompl)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
