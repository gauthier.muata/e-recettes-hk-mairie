/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.IdentificationBusiness;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.*;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.util.*;
import static cd.hologram.erecettesvg.util.SMSSender.sendSMS;
import java.io.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "Identification", urlPatterns = {"/identification_servlet"})
public class Identification extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);
    Properties propertiesMail = Property.getProperties(Property.FileData.FR_MAIL);

    static List<Complement> infoComplementaires;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

//        StopWatch stopWatch = new StopWatch();
//        stopWatch.start();
//        System.out.println("------------------------------------------------------");
//        System.out.println(String.format("%s / début / %s", operation, stopWatch.toString()));
        switch (operation) {
            case IdentificationConst.Operation.LOAD_FORME_JURIDIQUE:
                result = loadFormeJuridiques();
                break;
            case IdentificationConst.Operation.LOAD_COMPLEMENT_FORME:
                result = loadComplementFormes(request);
                break;
            case IdentificationConst.Operation.LOAD_ADRESSES:
                result = loadAdresses(request);
                break;
            case IdentificationConst.Operation.SAVE_ASSUJETTI:
                result = saveAssujetti(request);
                break;
            case IdentificationConst.Operation.LOAD_ASSUJETTIS:
                result = loadPersonnes(request);
                break;
            case IdentificationConst.Operation.LOAD_INFO_ASSUJETTI:
                result = loadInfoPersonne(request);
                break;
            case IdentificationConst.Operation.DISABLED_ASSUJETTI:
                result = disabledAssujetti(request);
                break;
            case IdentificationConst.Operation.LOAD_ASSUJETTI_AVANCEE:
                result = loadAssujettiAvencee(request);
                break;
            case IdentificationConst.Operation.LOAD_ADRESSES_PERSONNE:
                result = loadAdressesPersonne(request);
                break;
            case IdentificationConst.Operation.LOAD_ASSUJETTIS_VALIDER:
                result = loadAssujettisValider(request);
                break;
            case IdentificationConst.Operation.VALIDER_ASSUJETTIS:
                result = validerAssujettis(request);
                break;
            case IdentificationConst.Operation.ASSUJETTIS_FROM:
                result = loadAssujettisValider(request);
                break;
            case "checkNifValue":
                result = checkNifValue(request);
                break;
            case "checkMailOrPhoneValue":
                result = checkMailOrPhoneValue(request);
                break;
            case "loadComplementByFormeJuridique":
                result = loadListComplementByFormeJurique(request);
                break;
        }

//        stopWatch.stop();
//        System.out.println(String.format("%s / fin / %s", operation, stopWatch.toString()));
//        System.out.println("------------------------------------------------------");
//
        out.print(result);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String checkNifValue(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String value = request.getParameter("value");

        try {

            dataReturn = IdentificationBusiness.getValueCheckNif(value);

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String checkMailOrPhoneValue(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String value = request.getParameter("value");
        String type = request.getParameter("type");

        try {

            dataReturn = IdentificationBusiness.getValueCheckMailOrPhone(value, Integer.valueOf(type));

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadFormeJuridiques() {

        List<JSONObject> jsonFormeJuridiques = new ArrayList<>();

        try {

            List<FormeJuridique> formeJuridiques = IdentificationBusiness.getFormeJuridiques();

            for (FormeJuridique formeJuridique : formeJuridiques) {
                JSONObject jsonFormeJuridique = new JSONObject();
                jsonFormeJuridique.put(IdentificationConst.ParamName.CODE_FORME_JURIDIQUE,
                        formeJuridique == null
                                ? GeneralConst.EMPTY_STRING
                                : formeJuridique.getCode());
                jsonFormeJuridique.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        formeJuridique == null
                                ? GeneralConst.EMPTY_STRING
                                : formeJuridique.getIntitule());
                jsonFormeJuridiques.add(jsonFormeJuridique);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonFormeJuridiques.toString();
    }

    public String loadAdressesPersonne(HttpServletRequest request) {

        List<JSONObject> jsonAdresses = new ArrayList<>();

        try {

            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            List<AdressePersonne> adressePersonnes = IdentificationBusiness.getAdressesPersonnesByPersonne(codePersonne);

            if (adressePersonnes.size() > 0) {

                for (AdressePersonne adressePersonne : adressePersonnes) {

                    JSONObject jsonAdresse = new JSONObject();

                    String chaine = GeneralConst.EMPTY_STRING;

                    if (adressePersonne != null) {

                        chaine = adressePersonne.getAdresse().toString().toUpperCase().replaceAll("'", GeneralConst.SPACE);
                    }

                    jsonAdresse.put(IdentificationConst.ParamName.CHAINE_ADRESSE, adressePersonne == null
                            ? GeneralConst.EMPTY_STRING : chaine);

                    jsonAdresse.put(IdentificationConst.ParamName.CODE_ADRESSE_PERSONNE, adressePersonne == null
                            ? GeneralConst.EMPTY_STRING : adressePersonne.getCode());

                    jsonAdresses.add(jsonAdresse);
                }
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAdresses.toString();
    }

    public String loadComplementFormes(HttpServletRequest request) {

        List<JSONObject> jsonComplementFormes = new ArrayList<>();

        try {

            infoComplementaires = null;

            String codeFormeJuridique = request.getParameter(IdentificationConst.ParamName.CODE_FORME_JURIDIQUE);

            jsonComplementFormes = getComplementsFormes(codeFormeJuridique);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonComplementFormes.toString();
    }

    public String loadAdresses(HttpServletRequest request) {

        List<JSONObject> jsonAdresses = new ArrayList<>();
        try {

            String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);
            String codeHautKatanga = propertiesConfig.getProperty("CODE_HAUT_KATANGA");
            List<EntiteAdministrative> avenues = IdentificationBusiness.getEntiteAdministratives(libelle == null
                    ? GeneralConst.EMPTY_STRING
                    : libelle);

            for (EntiteAdministrative avenue : avenues) {
                
                System.out.println("avenue : " + avenue.getIntitule());
                
                JSONObject jsonAdresse = new JSONObject();
                jsonAdresse.put(IdentificationConst.ParamName.CODE_AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getCode());

                jsonAdresse.put(IdentificationConst.ParamName.AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getIntitule());

                if (avenue != null) {
                    EntiteAdministrative quartier = avenue.getEntiteMere();
                    jsonAdresse.put(IdentificationConst.ParamName.CODE_QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getCode());

                    jsonAdresse.put(IdentificationConst.ParamName.QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getIntitule());

                    if (quartier != null) {
                        EntiteAdministrative commune = quartier.getEntiteMere();
                        jsonAdresse.put(IdentificationConst.ParamName.CODE_COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getCode());

                        jsonAdresse.put(IdentificationConst.ParamName.COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getIntitule());

                        if (commune != null) {
                            //EntiteAdministrative district = commune.getEntiteMere();
                            EntiteAdministrative ville = commune.getEntiteMere();
                            jsonAdresse.put(IdentificationConst.ParamName.CODE_VILLE, ville == null
                                    ? GeneralConst.EMPTY_STRING : ville.getCode());

                            jsonAdresse.put(IdentificationConst.ParamName.VILLE, ville == null
                                    ? GeneralConst.EMPTY_STRING : ville.getIntitule());

                            if (ville != null) {
                                EntiteAdministrative province = ville.getEntiteMere();
                                jsonAdresse.put(IdentificationConst.ParamName.CODE_PROVINCE, province == null
                                        ? GeneralConst.EMPTY_STRING : province.getCode());

                                jsonAdresse.put(IdentificationConst.ParamName.PROVINCE, province == null
                                        ? GeneralConst.EMPTY_STRING : province.getIntitule());

                                /*if (ville != null) {
                                    EntiteAdministrative province = ville.getEntiteMere();
                                    jsonAdresse.put(IdentificationConst.ParamName.CODE_PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getCode());

                                    jsonAdresse.put(IdentificationConst.ParamName.PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getIntitule());
                                }*/
                            }
                        }
                    }
                }
                jsonAdresses.add(jsonAdresse);
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAdresses.toString();
    }

    public String saveAssujetti(HttpServletRequest request) {

        String result;

        try {
            String codeFormeJuridique = request.getParameter(IdentificationConst.ParamName.CODE_FORME_JURIDIQUE);
            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            String nif = request.getParameter(IdentificationConst.ParamName.NIF);
            String nom = request.getParameter(IdentificationConst.ParamName.NOM);
            String postnom = request.getParameter(IdentificationConst.ParamName.POST_NOM);
            String prenom = request.getParameter(IdentificationConst.ParamName.PRENOM);
            String telephone = request.getParameter(IdentificationConst.ParamName.TELEPHONE);
            String email = request.getParameter(IdentificationConst.ParamName.EMAIL);
            String idUser = request.getParameter(GeneralConst.ID_USER);
            String stateDeclaration = request.getParameter(IdentificationConst.ParamName.SELECT_STATE_DECLARATION);
            JSONArray jsonAdresses = new JSONArray(request.getParameter(IdentificationConst.ParamName.ADRESSES));
            JSONArray jsonComplements = new JSONArray(request.getParameter(IdentificationConst.ParamName.COMPLEMENTS));

            Personne personne = new Personne();
            personne.setCode(codePersonne);
            personne.setFormeJuridique(new FormeJuridique(codeFormeJuridique));
            personne.setNif(nif);
            personne.setNom(nom);
            personne.setPostnom(postnom);
            personne.setPrenoms(prenom);
            personne.setAgentCreat(idUser);

            LoginWeb loginWeb = null;

            String motPasse = Tools.generateRandomValue(8);

            stateDeclaration = GeneralConst.Number.ONE; // Cette ligne de code n'existait pas

            if (stateDeclaration.equals(GeneralConst.Number.ONE)) {

                /*if (personne.getCode().isEmpty()) {

                 LoginWeb loginW = IdentificationBusiness.getLoginWebByTelephoneOrEmail(email.trim(), telephone.trim());

                 if (loginW != null) {

                 if (telephone.trim().equals(loginW.getTelephone())) {
                 return GeneralConst.ResultCode.PHONE_EXIST;
                 }

                 if (email.trim().equals(loginW.getMail())) {
                 return GeneralConst.ResultCode.MAIL_EXIST;
                 }
                 }

                 } else {

                 LoginWeb loginW = IdentificationBusiness.getLoginWebByPersonne(personne.getCode().trim());

                 if (loginW == null) {

                 LoginWeb loginW2 = IdentificationBusiness.getLoginWebByTelephoneOrEmail(email.trim(), telephone.trim());

                 if (loginW2 != null) {

                 if (telephone.trim().equals(loginW2.getTelephone())) {
                 return GeneralConst.ResultCode.PHONE_EXIST;
                 }

                 if (email.trim().equals(loginW2.getMail())) {
                 return GeneralConst.ResultCode.MAIL_EXIST;
                 }
                 }

                 } else {

                 if (!loginW.getTelephone().equals(telephone)) {
                 LoginWeb loginW1 = IdentificationBusiness.getLoginWebByTelephoneOrEmail("", telephone);

                 if (loginW1 != null) {

                 return GeneralConst.ResultCode.PHONE_EXIST;
                 }
                 }

                 if (!loginW.getMail().equals(email)) {
                 LoginWeb loginW1 = IdentificationBusiness.getLoginWebByTelephoneOrEmail(email.trim(), "");

                 if (loginW1 != null) {

                 return GeneralConst.ResultCode.MAIL_EXIST;
                 }
                 }
                 }

                 }*/
                loginWeb = new LoginWeb();
                loginWeb.setMail(email);
                loginWeb.setTelephone(telephone);
                loginWeb.setPassword(motPasse);

            }

            List<Complement> complements = getComplements(jsonComplements);
            List<AdressePersonne> adressePersonnes = getAdressePersonnes(jsonAdresses);

            if (savePersonne(personne, complements, adressePersonnes, loginWeb)) {

                /*String service_sms = propertiesConfig.getProperty(PropertiesConst.Config.SMS_SERVICE);
                 String smsURL;

                 String mailjetUser = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_USERNAME);
                 String mailjetPassword = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_PASSWORD);
                 String mailjetURL = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_URL);
                 String mailSmtp = propertiesConfig.getProperty(PropertiesConst.Config.MAIL_SMTP);

                 if (stateDeclaration.equals(GeneralConst.Number.ONE)) {

                 if (service_sms.equals(GeneralConst.Number.ONE)) {
                 smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL);
                 } else {
                 smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL2);
                 }

                 String smsSubscribe = propertiesMail.getProperty(PropertiesConst.Message.SMS_SUBSCRIBE);
                 String mailSubscribeContent = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_CONTENT);
                 String mailSubscribeSubjet = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_SUBJECT);

                 LoginWeb login = IdentificationBusiness.getLoginWebByTelephoneOrEmail(email.trim(), telephone.trim());

                 String mailContent = String.format(mailSubscribeContent, personne.toString(), login.getUsername(), motPasse);

                 MailSender mailSender = new MailSender();
                 mailSender.setDate(new Date());
                 mailSender.setHost(mailjetURL);
                 mailSender.setSubject(mailSubscribeSubjet);
                 mailSender.setMessage(mailContent);
                 mailSender.setReciever(email);
                 mailSender.setSender(mailSmtp);

                 mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);

                 String smsContent = String.format(smsSubscribe, personne.getNom(), email);

                 smsURL = String.format(smsURL, telephone, smsContent);

                 sendSMS(smsURL);
                    
                 } else {

                 if (!personne.getCode().isEmpty()) {

                 LoginWeb loginWe = IdentificationBusiness.getLoginWebByPersonne(personne.getCode().trim());

                 if (loginWe != null && loginWe.getFkPersonne() != null) {

                 if (service_sms.equals(GeneralConst.Number.ONE)) {
                 smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL);
                 } else {
                 smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL2);
                 }

                 String smsSubscribe = propertiesMail.getProperty(PropertiesConst.Message.SMS_SUBSCRIBE_NON);

                 String mailSubscribeContent = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_CONTENT_NON);
                 String mailSubscribeSubjet = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_SUBJECT_NON);

                 String mailContent = String.format(mailSubscribeContent, personne.toString(), GeneralConst.EMPTY_STRING, GeneralConst.EMPTY_STRING);

                 MailSender mailSender = new MailSender();
                 mailSender.setDate(new Date());
                 mailSender.setHost(mailjetURL);
                 mailSender.setSubject(mailSubscribeSubjet);
                 mailSender.setMessage(mailContent);
                 mailSender.setReciever(email);
                 mailSender.setSender(mailSmtp);

                 mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);

                 String smsContent = String.format(smsSubscribe, personne.getNom(), email);

                 smsURL = String.format(smsURL, telephone, smsContent);

                 sendSMS(smsURL);

                 }
                 }

                 }*/
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadPersonnes(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();
        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);

        try {

            List<Personne> personnes = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                personnes = IdentificationBusiness.getPersonnesByName(libelle);
            } else if (typeSearch.equals(GeneralConst.Number.ONE)) {
                Personne personne = getPersonneByCodeOrNif(libelle, false);
                if (personne != null) {
                    personnes.add(personne);
                }
            } else {
                Personne personne = getPersonneByNtdorTelephone(libelle, typeSearch);
                if (personne != null) {
                    personnes.add(personne);
                }
            }

            for (Personne personne : personnes) {
                JSONObject jsonPersonne = new JSONObject();

                jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getIntitule());

                jsonPersonne.put(IdentificationConst.ParamName.USER_NAME,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                jsonPersonne.put(IdentificationConst.ParamName.NIF,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                jsonPersonne.put(IdentificationConst.ParamName.NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getTelephone());

                jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getMail());

                jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                        getDefaultAdresse(personne).toUpperCase());

                jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                jsonPersonnes.add(jsonPersonne);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String loadInfoPersonne(HttpServletRequest request) {

        JSONObject jsonPersonne = new JSONObject();
        try {
            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);

            if (codePersonne != null) {

                Personne personne = getPersonneByCodeOrNif(codePersonne, true);

                jsonPersonne.put(IdentificationConst.ParamName.CODE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getCode());

                jsonPersonne.put(IdentificationConst.ParamName.NIF,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                jsonPersonne.put(IdentificationConst.ParamName.NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getTelephone());

                jsonPersonne.put(IdentificationConst.ParamName.TELE_DECLARATION_IS_EXISTE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getEtat());

                jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getMail());

                jsonPersonne.put(IdentificationConst.ParamName.ADRESSES,
                        getAdressesPersonne(personne));

                infoComplementaires = (personne == null) ? null : personne.getComplementList();

                jsonPersonne.put(IdentificationConst.ParamName.COMPLEMENTS,
                        getComplementsFormes(personne == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : personne.getFormeJuridique().getCode()).toString());

            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonne.toString();
    }

    public String disabledAssujetti(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {
            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            if (codePersonne != null) {
                if (disabledPersonne(codePersonne)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    //
    // MEthodes complementaires
    //
    public List<JSONObject> getComplementsFormes(String codeFormeJuridique) {

        List<JSONObject> jsonComplementFormes = new ArrayList<>();

        try {
            List<ComplementForme> complementFormes = IdentificationBusiness.getComplementFormeByForme(codeFormeJuridique);

            if (complementFormes != null) {
                int counter = 0;
                for (ComplementForme complementForme : complementFormes) {

                    JSONObject jsonComplementForme = new JSONObject();
                    TypeComplement typeComplement = complementForme.getComplement();

                    jsonComplementForme.put(IdentificationConst.ParamName.CODE_TYPE_COMPLEMENT,
                            typeComplement == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeComplement.getCode());

                    jsonComplementForme.put(IdentificationConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                            typeComplement == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeComplement.getIntitule());

                    jsonComplementForme.put(IdentificationConst.ParamName.OBJET_INTERACTION,
                            typeComplement == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeComplement.getObjetInteraction());

                    jsonComplementForme.put(IdentificationConst.ParamName.VALEUR_PREDEFINIE,
                            typeComplement == null
                                    ? GeneralConst.EMPTY_STRING
                                    : typeComplement.getValeurPredefinie());

                    jsonComplementForme.put(IdentificationConst.ParamName.INPUT_VALUE,
                            createControl(complementForme, counter));

                    jsonComplementFormes.add(jsonComplementForme);
                    counter++;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return jsonComplementFormes;
    }

    public String createControl(final ComplementForme complementForme, int counter) {

        String CONTROL_ID = GeneralConst.PREFIX_IDENTIFIANT_KEY.concat(String.valueOf(counter));
        String value = GeneralConst.EMPTY_STRING;

        TypeComplement typeComplement = complementForme.getComplement();

        final String OBJET_INTERACTION = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction();

        final String CONTROL_VALUE = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule();

        final boolean HAS_VALUES_LIST = (typeComplement == null)
                ? false : typeComplement.getValeurPredefinie();

        if (HAS_VALUES_LIST) {

            value = getControlList(
                    CONTROL_ID,
                    CONTROL_VALUE,
                    complementForme,
                    GeneralConst.EMPTY_STRING);

        } else {

            switch (OBJET_INTERACTION) {
                case IdentificationConst.ObjetInteraction.EMPTY:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            IdentificationConst.TypeData.TYPE_TEXT,
                            complementForme);
                    break;
                case IdentificationConst.ObjetInteraction.DATE:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            IdentificationConst.TypeData.TYPE_DATE,
                            complementForme);
                    break;
                case IdentificationConst.ObjetInteraction.MONTANT:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            IdentificationConst.TypeData.TYPE_NUMBER,
                            complementForme);
                    break;
                case IdentificationConst.ObjetInteraction.NOMBRE:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            IdentificationConst.TypeData.TYPE_NUMBER,
                            complementForme);
                    break;
                case IdentificationConst.ObjetInteraction.SEXE:

                    value = getControlList(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            complementForme,
                            IdentificationConst.ObjetInteraction.SEXE);

                    break;
            }
        }

        return value;
    }

    public static ComplementData getKeyNameComplement(ComplementForme complementForme) {

        ComplementData complementData = new ComplementData();

        complementData.complement = getCorrespondingComplement(complementForme);

        String idComplement = (complementData.complement == null)
                ? GeneralConst.Number.ZERO
                : complementData.complement.getId();

        TypeComplement typeComplement = complementForme.getComplement();

        complementData.keyName = idComplement
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(complementForme.getCode())
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplement == null
                                ? GeneralConst.Number.ZERO
                                : (complementForme.getComplement().getObligatoire()
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO));
        return complementData;
    }

    public static String getInputControl(String controlId, String controlValue, String type, ComplementForme complementForme) {

        ComplementData complementData = getKeyNameComplement(complementForme);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(IdentificationConst.ParamControl.INPUT_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()));
        return value;

    }

    public static String getControlList(String controlId, String controlValue, ComplementForme complementForme, final String objetInteraction) {

        ComplementData complementData = getKeyNameComplement(complementForme);

        String options = String.format(IdentificationConst.ParamControl.ITEM_OPTION,
                GeneralConst.Number.ZERO,
                GeneralConst.EMPTY_STRING,
                GeneralConst.TEXT_SELECTED_VALUE);

        switch (objetInteraction) {
            case IdentificationConst.ObjetInteraction.SEXE:
                options += String.format(IdentificationConst.ParamControl.ITEM_OPTION,
                        IdentificationConst.ObjetSexeData.CODE_SEXE_MASCULIN,
                        selectedOption(complementData == null
                                        ? null
                                        : complementData.complement, IdentificationConst.ObjetSexeData.CODE_SEXE_MASCULIN),
                        IdentificationConst.ObjetSexeData.LIBELLE_SEXE_MASCULIN);

                options += String.format(IdentificationConst.ParamControl.ITEM_OPTION,
                        IdentificationConst.ObjetSexeData.CODE_SEXE_FEMININ,
                        selectedOption(complementData == null
                                        ? null
                                        : complementData.complement, IdentificationConst.ObjetSexeData.CODE_SEXE_FEMININ),
                        IdentificationConst.ObjetSexeData.LIBELLE_SEXE_FEMININ
                );
                break;
            default:
                TypeComplement typeComplement = (complementForme == null) ? null : complementForme.getComplement();
                if (typeComplement != null) {
                    List<ValeurPredefinie> valeurPredefinies = typeComplement.getValeurPredefinieList();
                    if (valeurPredefinies != null) {
                        for (ValeurPredefinie valeurPredefinie : valeurPredefinies) {
                            options += String.format(IdentificationConst.ParamControl.ITEM_OPTION,
                                    valeurPredefinie.getCode(),
                                    selectedOption(complementData == null
                                                    ? null
                                                    : complementData.complement, valeurPredefinie.getCode()),
                                    valeurPredefinie.getValeur().toUpperCase());
                        }
                    }
                }
                break;
        }

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(IdentificationConst.ParamControl.COMPLEMENT_LIST,
                controlId,
                controlValue,
                requiredValue,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING : complementData.keyName,
                options);

        return value;
    }

    public static String selectedOption(Complement complement, String item) {

        String result = GeneralConst.EMPTY_STRING;
        if (complement != null) {
            if (complement.getValeur().trim().equals(item.trim())) {
                return IdentificationConst.ParamControl.SELECTED_OPTION_ATTR;
            }
        }
        return result;
    }

    public static String construstRequiredValue(String codeCF) {

        String requiredValue;
        String[] array = codeCF.split(GeneralConst.DASH_SEPARATOR);

        if (array.length > 1) {
            requiredValue = array[2].equals(GeneralConst.Number.ZERO)
                    ? GeneralConst.EMPTY_STRING
                    : IdentificationConst.ParamControl.SPAN_REQUIRED_VALUE;
        } else {
            requiredValue = GeneralConst.EMPTY_STRING;
        }
        return requiredValue;
    }

    public List<AdressePersonne> getAdressePersonnes(JSONArray jsonAdresses) {

        List<AdressePersonne> adressePersonnes = new ArrayList<>();
        try {
            for (int i = 0; i < jsonAdresses.length(); i++) {
                JSONObject jsonobject = jsonAdresses.getJSONObject(i);
                String codeAP = jsonobject.getString(IdentificationConst.ParamName.CODE_ADRESSE_PERSONNE);
                String code = jsonobject.getString(IdentificationConst.ParamName.CODE_ENTITE_ADMINISTRATIVE);
                String numero = jsonobject.getString(IdentificationConst.ParamName.NUMERO);
                String defaut = jsonobject.getString(IdentificationConst.ParamName.DEFAUT);
                String etat = jsonobject.getString(IdentificationConst.ParamName.ETAT);
                String chaine = jsonobject.getString(IdentificationConst.ParamName.CHAINE_ADRESSE);

                EntiteAdministrative avenue = getEntiteAdministrativeByCode(code);
                EntiteAdministrative quartier = avenue.getEntiteMere();
                EntiteAdministrative commune = quartier.getEntiteMere();
                EntiteAdministrative district = commune.getEntiteMere();
                EntiteAdministrative ville = district.getEntiteMere();
                EntiteAdministrative province = ville.getEntiteMere();

                Adresse adresse = new Adresse();
                adresse.setNumero(numero);
                adresse.setAvenue(avenue);
                adresse.setQuartier(quartier);
                adresse.setCommune(commune);
                adresse.setDistrict(district);
                adresse.setVille(ville);
                adresse.setProvince(province);
                adresse.setChaine(chaine.concat("n° ").concat(numero));

                AdressePersonne adressePersonne = new AdressePersonne();
                adressePersonne.setEtat(etat.equals(GeneralConst.Number.ONE));
                adressePersonne.setCode(codeAP);
                adressePersonne.setParDefaut(defaut.equals(GeneralConst.YES_VALUE));
                adressePersonne.setAdresse(adresse);
                adressePersonnes.add(adressePersonne);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return adressePersonnes;
    }

    public List<Complement> getComplements(JSONArray complementsArray) {

        List<Complement> complements = new ArrayList<>();
        try {
            for (int i = 0; i < complementsArray.length(); i++) {
                JSONObject jsonobject = complementsArray.getJSONObject(i);
                String id = jsonobject.getString(IdentificationConst.ParamName.ID_COMPLEMENT);
                String code = jsonobject.getString(IdentificationConst.ParamName.CODE_COMPLEMENT_FORME);
                String valeur = jsonobject.getString(IdentificationConst.ParamName.VALEUR_COMPLEMENT_FORME);

                Complement complement = new Complement();
                complement.setId(id.equals(GeneralConst.Number.ZERO) ? GeneralConst.EMPTY_STRING : id);
                complement.setFkComplementForme(new ComplementForme(code));
                complement.setValeur(valeur);
                complements.add(complement);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return complements;
    }

    public String getDefaultAdresse(Personne personne) {

        String adresse = GeneralConst.EMPTY_STRING;
        if (personne != null) {
            if (personne.getAdressePersonneList() != null) {
                for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {
                    if (adressePersonne.getParDefaut() == true && adressePersonne.getEtat() == true) {
                        adresse = adressePersonne.getAdresse().toString();
                        break;
                    }
                }
            }
        }
        return adresse;
    }

    public String getAdressesPersonne(Personne personne) {

        List<JSONObject> adresses = new ArrayList<>();

        try {
            if (personne != null) {
                if (personne.getAdressePersonneList() != null) {
                    for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {

                        if (adressePersonne.getEtat() == true) {

                            JSONObject adresse = new JSONObject();
                            adresse.put(IdentificationConst.ParamName.CODE_ENTITE_ADMINISTRATIVE,
                                    adressePersonne.getAdresse().getAvenue().getCode());

                            adresse.put(IdentificationConst.ParamName.DEFAUT,
                                    adressePersonne.getParDefaut()
                                            ? GeneralConst.YES_VALUE
                                            : GeneralConst.EMPTY_STRING);

                            adresse.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                                    adressePersonne.getAdresse().toString());

                            adresse.put(IdentificationConst.ParamName.NUMERO,
                                    adressePersonne.getAdresse().getNumero());

                            adresse.put(IdentificationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                    adressePersonne.getCode());

                            adresses.add(adresse);
                        }

                    }
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return adresses.toString();
    }

    public static Complement getCorrespondingComplement(ComplementForme complementForme) {

        if (infoComplementaires != null) {
            for (Complement complement : infoComplementaires) {
                if (complement.getFkComplementForme().equals(complementForme)) {
                    return complement;
                }
            }
        }

        return null;
    }

    public static class ComplementData {

        public String keyName;
        public Complement complement;
    }

    public String loadAssujettiAvencee(HttpServletRequest request) {

        String valueSearch = GeneralConst.EMPTY_STRING,
                codeFormeJuridique = GeneralConst.EMPTY_STRING,
                codeService = GeneralConst.EMPTY_STRING;

        List<JSONObject> jsonAssujettiAvancee = new ArrayList<>();

        try {
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            codeFormeJuridique = request.getParameter(TaxationConst.ParamName.CODE_FORME_JURIDIQUE);
            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);

            List<Personne> listAssujetti = IdentificationBusiness.getListPersonneBySearchAvance(codeFormeJuridique,
                    codeService,
                    valueSearch);

            if (listAssujetti.size() > 0) {

                for (Personne pers : listAssujetti) {
                    JSONObject jsonPersonne = new JSONObject();
                    jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                            pers == null
                                    ? GeneralConst.EMPTY_STRING
                                    : pers.getFormeJuridique() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : pers.getFormeJuridique().getIntitule().trim());

                    jsonPersonne.put(IdentificationConst.ParamName.NIF,
                            pers == null ? GeneralConst.EMPTY_STRING : pers.getNif());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, pers.toString().toUpperCase());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM,
                            pers == null ? GeneralConst.EMPTY_STRING : pers.getNom());

                    jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                            pers == null ? GeneralConst.EMPTY_STRING : pers.getPostnom());

                    jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                            pers == null ? GeneralConst.EMPTY_STRING : pers.getPrenoms());

                    jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                            pers == null
                                    ? GeneralConst.EMPTY_STRING
                                    : pers.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : pers.getLoginWeb().getTelephone());

                    jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                            pers == null
                                    ? GeneralConst.EMPTY_STRING
                                    : pers.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : pers.getLoginWeb().getMail());

                    jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                            getDefaultAdresse(pers).toUpperCase());

                    jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                            pers == null ? GeneralConst.EMPTY_STRING : pers.getCode());

                    jsonAssujettiAvancee.add(jsonPersonne);
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonAssujettiAvancee.toString();
    }

    public String loadAssujettisValider(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();

        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);

        try {

            List<Personne> personnes = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                personnes = IdentificationBusiness.getPersonnesByName(libelle);
            } else if (typeSearch.equals(GeneralConst.Number.ONE)) {
                Personne personne = getPersonneByCodeOrNif(libelle, false);
                if (personne != null) {
                    personnes.add(personne);
                }
            } else {
                Personne personne = getPersonneByNtdorTelephone(libelle, typeSearch);
                if (personne != null) {
                    personnes.add(personne);
                }
            }

            if (personnes.size() > 0) {

                for (Personne personne : personnes) {
                    JSONObject jsonPersonne = new JSONObject();

                    jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getFormeJuridique() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getFormeJuridique().getIntitule());

                    jsonPersonne.put(IdentificationConst.ParamName.NIF,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                    jsonPersonne.put(IdentificationConst.ParamName.USER_NAME,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getUsername());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                    jsonPersonne.put(IdentificationConst.ParamName.NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                    jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                    jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                    jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getTelephone());

                    jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getMail());

                    jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                            getDefaultAdresse(personne).toUpperCase());

                    jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                    jsonPersonnes.add(jsonPersonne);
                }

            }else{
                
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String validerAssujettis(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {
            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            if (codePersonne != null) {
                if (IdentificationBusiness.validerAssujettis(codePersonne)) {

                    LoginWeb login = IdentificationBusiness.getLoginWebByPersonne(codePersonne.trim());

                    if (login != null) {

                        String service_sms = propertiesConfig.getProperty(PropertiesConst.Config.SMS_SERVICE);
                        String smsURL;

                        String mailjetUser = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_USERNAME);
                        String mailjetPassword = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_PASSWORD);
                        String mailjetURL = propertiesConfig.getProperty(PropertiesConst.Config.MAILJET_URL);
                        String mailSmtp = propertiesConfig.getProperty(PropertiesConst.Config.MAIL_SMTP);

                        if (service_sms.equals(GeneralConst.Number.ONE)) {
                            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL);
                        } else {
                            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL2);
                        }

                        String smsSubscribe = propertiesMail.getProperty(PropertiesConst.Message.SMS_SUBSCRIBE);
                        String mailSubscribeContent = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_CONTENT);
                        String mailSubscribeSubjet = propertiesMail.getProperty(PropertiesConst.Message.MAIL_SUBSCRIBE_CLIENT_SUBJECT);

                        String motPasse = Tools.generateRandomValue(8);

                        Personne personne = IdentificationBusiness.getPersonneByCode(codePersonne);

                        String mailContent = String.format(mailSubscribeContent, personne.toString(), login.getUsername(), motPasse);
                        String mail = GeneralConst.EMPTY_STRING;
                        String telephone = GeneralConst.EMPTY_STRING;

                        MailSender mailSender = new MailSender();
                        mailSender.setDate(new Date());
                        mailSender.setHost(mailjetURL);
                        mailSender.setSubject(mailSubscribeSubjet);
                        mailSender.setMessage(mailContent);

                        if (login.getMail() != null) {
                            mail = login.getMail().trim();
                        } else {
                            mail = null;
                        }
                        mailSender.setReciever(mail);

                        mailSender.setSender(mailSmtp);

                        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);

                        String smsContent = String.format(smsSubscribe, personne.getNom(), mail);

                        if (login.getTelephone() != null) {
                            telephone = login.getTelephone().trim();
                        } else {
                            telephone = null;
                        }

                        smsURL = String.format(smsURL, telephone, smsContent);

                        sendSMS(smsURL);

                        if (IdentificationBusiness.updatePassWordLoginWeb(codePersonne, motPasse)) {

                            result = GeneralConst.ResultCode.SUCCES_OPERATION;
                        } else {
                            result = GeneralConst.ResultCode.PASSWORD_NON_CHANGE;
                        }
                    }

                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadListComplementByFormeJurique(HttpServletRequest request) {

        List<JSONObject> jsonComplementList = new ArrayList<>();

        try {

            String codeForme = request.getParameter("codeFormeJuridique");

            List<Complement> complements = IdentificationBusiness.getListComplementByFormeJuridique(codeForme);

            for (Complement complement : complements) {

                JSONObject jsonComplement = new JSONObject();

                jsonComplement.put("id", complement.getId());
                jsonComplement.put("codeComplement", complement.getFkComplementForme().getCode());
                jsonComplement.put("value", complement.getValeur() != null ? complement.getValeur() : GeneralConst.EMPTY_STRING);

                jsonComplementList.add(jsonComplement);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonComplementList.toString();
    }
}
