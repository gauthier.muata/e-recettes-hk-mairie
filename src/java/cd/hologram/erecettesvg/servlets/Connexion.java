/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.ConnexionBusiness;
import static cd.hologram.erecettesvg.business.ConnexionBusiness.*;
import static cd.hologram.erecettesvg.business.GeneralBusiness.*;
import cd.hologram.erecettesvg.business.GestionPenaliteBackendBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.util.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.pojo.RightUsers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "Connexion", urlPatterns = {"/connexion_servlet"})
public class Connexion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG),
            propertiesMail = Property.getProperties(Property.FileData.FR_MAIL);

    List<AgentGu> listAgentGu;
    List<GuAcces> listGuAccess;
    List<AccesUser> listAccesUsers;
    List<RightUsers> listRightUsers;
    List<AgentSite> listSitesUser;
    List<SiteBanque> listBanqueSite;
    List<Site> listSite;
    List<Service> listService;
    List<Avis> listAvis;
    List<Taux> listTaux;
    List<Fonction> listFonction;
    List<TypePenalite> listTypePenalite;
    List<Penalite> listPenalite;
    List<FormeJuridique> listFormeJuridique;

    List<JsonObject> listUserSiteJson;
    List<JsonObject> listAllSiteJson;
    List<JsonObject> listAllServiceJson;
    List<JsonObject> listbanqueJson;
    List<JsonObject> listAccountJson;
    List<JsonObject> listAVisJson;
    List<JsonObject> listTauxJson;
    List<JsonObject> listFonctionJson;
    List<JsonObject> listTypePenaliteJson;
    List<JsonObject> listPenaliteJson;
    List<JsonObject> listFormeJuridiqueJson;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

//        StopWatch stopWatch = new StopWatch();
//        stopWatch.start();
//        System.out.println("------------------------------------------------------");
//        System.out.println(String.format("%s / début / %s", operation, stopWatch.toString()));
        switch (operation) {
            case LoginConst.Operation.LOGIN:
                result = authentificationUser(request);
                break;
            case LoginConst.Operation.CHECK_SESSION:
                result = checkSession(request);
                break;
            case LoginConst.Operation.UPDATE_PASSWORD:
                result = updateUserPassword(request);
                break;
            case LoginConst.Operation.GET_SIGNATURE_AGENT:
                result = getSignatureAgent(request);
                break;
            case LoginConst.Operation.SAVE_SIGNATURE_AGENT:
                result = saveSignatureAgent(request);
                break;
            case LoginConst.Operation.LAST_DECONEXION:
                result = lastDeconnexion(request);
                break;
        }

//        stopWatch.stop();
//        System.out.println(String.format("%s / fin / %s", operation, stopWatch.toString()));
//        System.out.println("------------------------------------------------------");
        out.print(result);

    }

    public String updateUserPassword(HttpServletRequest request) {

        String result;

        HttpSession session = request.getSession();
        Agent connectedAgent = (Agent) session.getAttribute(GeneralConst.KEY_SESSION_AGENT);

        String userdID = request.getParameter(LoginConst.ParamName.ID_USER);
        String newPassword = request.getParameter(LoginConst.ParamName.NEW_PASSWORD);
        String currentPassword = request.getParameter(LoginConst.ParamName.CURRENT_PASSWORD);

        try {
            if (!currentPassword.equals(GeneralConst.EMPTY_STRING)) {
                Agent user = authentification(connectedAgent.getLogin(), currentPassword);
                if (user == null) {
                    return GeneralConst.ResultCode.INVALID_PASSWORD;
                }
            }

            boolean updated = ConnexionBusiness.updatePassword(newPassword, Integer.valueOf(userdID));

            if (updated) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (NumberFormatException numberFormatException) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String checkSession(HttpServletRequest request) {

        HttpSession session = request.getSession();

        Agent agent = (Agent) session.getAttribute(GeneralConst.KEY_SESSION_AGENT);

        if (agent != null) {

            return GeneralConst.ResultCode.SUCCES_OPERATION;
        } else {

            return GeneralConst.ResultCode.FAILED_OPERATION;
        }

    }

    public String authentificationUser(HttpServletRequest request) {

        listAgentGu = new ArrayList<>();
        listGuAccess = new ArrayList<>();
        listAccesUsers = new ArrayList<>();
        listRightUsers = new ArrayList<>();
        listSitesUser = new ArrayList<>();
        listBanqueSite = new ArrayList<>();
        listSite = new ArrayList<>();
        listService = new ArrayList<>();
        listAvis = new ArrayList<>();
        listTaux = new ArrayList<>();
        listFonction = new ArrayList<>();
        listTypePenalite = new ArrayList<>();
        listPenalite = new ArrayList<>();
        listFormeJuridique = new ArrayList<>();
        List<UtilisateurDivision> listUserDivision = new ArrayList<>();

        listUserSiteJson = new ArrayList<>();
        listAllSiteJson = new ArrayList<>();
        listAllServiceJson = new ArrayList<>();
        listbanqueJson = new ArrayList<>();
        listAccountJson = new ArrayList<>();
        listAVisJson = new ArrayList<>();
        listTauxJson = new ArrayList<>();
        listFonctionJson = new ArrayList<>();
        listTypePenaliteJson = new ArrayList<>();
        listPenaliteJson = new ArrayList<>();
        listFormeJuridiqueJson = new ArrayList<>();
        List<JsonObject> listUserDivisionJson = new ArrayList<>();

        String login, mdp;

        try {

            login = request.getParameter(LoginConst.ParamName.LOGIN_USER);
            mdp = request.getParameter(LoginConst.ParamName.PASSWORD);

            String[] array = login.split(LoginConst.ParamName.SUFFIXE_LOGIN);
            if (array.length == 1) {
                login = array[0];
            }

            Agent agent = authentification(login, mdp);

            JsonObject jsonUserObject = new JsonObject();

            if (agent != null) {

                RightUsers rightUsers;

                boolean isExpired = false;

                if (agent.getCompteExpire() != null) {

                    if (agent.getCompteExpire() != 0) {

                        if (agent.getDateExpiration().before(new Date())) {

                            isExpired = true;
                        }

                    }

                }

                jsonUserObject.addProperty("isConnected", true);
                jsonUserObject.addProperty("dateLastConnexion", ConvertDate.formatDateHeureToStringV2(new Date()));

                if (agent.getDateDerniereDeconnexion() != null) {
                    jsonUserObject.addProperty("dateLastDeconnexion", ConvertDate.formatDateHeureToStringV2(agent.getDateDerniereDeconnexion()));
                } else {
                    jsonUserObject.addProperty("dateLastDeconnexion", "");
                }

                if (agent.getSite() != null) {
                    jsonUserObject.addProperty("isSitePeage", agent.getSite().getPeage());
                } else {
                    jsonUserObject.addProperty("isSitePeage", false);
                }

                jsonUserObject.addProperty(LoginConst.ParamName.SESSION, true);
                jsonUserObject.addProperty(LoginConst.ParamName.EXPIRED, isExpired);
                jsonUserObject.addProperty(LoginConst.ParamName.ID_USER, agent.getCode());
                jsonUserObject.addProperty(LoginConst.ParamName.MATRICULE, agent.getMatricule() != null ? agent.getMatricule().trim() : GeneralConst.EMPTY_STRING);
                jsonUserObject.addProperty(LoginConst.ParamName.LOGIN_USER, agent.getLogin().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.NOM_COMPLET, agent.toString().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.CHANGE_PWD, agent.getChangePwd().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.LIBELLE_SITE, agent.getSite() != null ? agent.getSite().getIntitule().trim() : GeneralConst.EMPTY_STRING);
                jsonUserObject.addProperty(GeneralConst.ParamName.SITE_CODE, agent.getSite() != null ? agent.getSite().getCode().trim() : GeneralConst.EMPTY_STRING);
                jsonUserObject.addProperty(GeneralConst.ParamName.COMMUNE_USER,
                        agent.getSite() == null ? GeneralConst.EMPTY_STRING
                                : agent.getSite().getAdresse() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : agent.getSite().getAdresse().getCommune() == null
                                                ? GeneralConst.EMPTY_STRING
                                                : agent.getSite().getAdresse().getCommune().getIntitule());

                if (agent.getSite() != null) {

                    if (agent.getSite().getDivision() != null) {
                        jsonUserObject.addProperty("divisionCode", agent.getSite().getDivision().getCode());
                        jsonUserObject.addProperty("divisionName", agent.getSite().getDivision().getIntitule().toUpperCase());
                    } else {
                        jsonUserObject.addProperty("divisionCode", GeneralConst.EMPTY_STRING);
                        jsonUserObject.addProperty("divisionName", GeneralConst.EMPTY_STRING);
                    }

                } else {
                    jsonUserObject.addProperty("divisionCode", GeneralConst.EMPTY_STRING);
                    jsonUserObject.addProperty("divisionName", GeneralConst.EMPTY_STRING);
                }

                jsonUserObject.addProperty(GeneralConst.ParamName.DISTRICT_USER, GeneralConst.EMPTY_STRING);

                if (agent.getSite() != null && agent.getSite().getAdresse() != null) {
                    jsonUserObject.addProperty(LoginConst.ParamName.ADRESSE_SITE, agent.getSite().getAdresse().toString().trim());
                } else {
                    jsonUserObject.addProperty(LoginConst.ParamName.ADRESSE_SITE, GeneralConst.EMPTY_STRING);
                }

                jsonUserObject.addProperty(LoginConst.ParamName.LIBELLE_FONCTION, agent.getFonction().getIntitule().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.CODE_FONCTION, agent.getFonction().getCode().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.LIBELLE_SERVICE, agent.getService().getIntitule().trim());
                jsonUserObject.addProperty(GeneralConst.ParamName.SERVICE_CODE, agent.getService().getCode().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.LIBELLE_GRADE, agent.getGrade().trim());
                jsonUserObject.addProperty(LoginConst.ParamName.LIBELLE_GRADE, agent.getGrade().trim());

                listAgentGu = getListGroupUserByAgent(agent.getCode());

                if (listAgentGu != null && !listAgentGu.isEmpty()) {

                    listRightUsers.clear();

                    for (AgentGu agentGu : listAgentGu) {

                        listGuAccess = getListRightGroupUserByGroup(agentGu.getFkGu().getId(),
                                agent.getCode());

                        if (listGuAccess != null && !listGuAccess.isEmpty()) {

                            for (GuAcces guAcces : listGuAccess) {

                                rightUsers = new RightUsers();
                                rightUsers.setDroit(guAcces.getDroits().trim());
                                listRightUsers.add(rightUsers);
                            }
                        }
                    }
                } else {
                    listRightUsers.clear();
                }

                listAccesUsers = getListRightsUserByAgent(agent.getCode());

                listSitesUser = agent.getAgentSiteList();

                if (agent.getSite() != null) {

                    listBanqueSite = agent.getSite().getSiteBanqueList();
                    listSite = getSite(agent.getSite().getPeage());
                }

                listFonction = getFonction();

                listService = getServiceAssiete();

                listAvis = TaxationBusiness.getListAllAvis();

                if (!listAccesUsers.isEmpty()) {
                    for (AccesUser accesUser : listAccesUsers) {

                        rightUsers = new RightUsers();
                        rightUsers.setDroit(accesUser.getDroit().trim());
                        listRightUsers.add(rightUsers);
                    }
                }

                JsonObject jsonAgentObject = new JsonObject();

                if (agent.getSite() != null) {

                    jsonAgentObject.addProperty(GeneralConst.ParamName.SITE_CODE, agent.getSite().getCode());
                    jsonAgentObject.addProperty(GeneralConst.ParamName.SITE_NAME, agent.getSite().getIntitule());

                    listUserSiteJson.add(jsonAgentObject);
                }

                if (!listSitesUser.isEmpty()) {
                    for (AgentSite agentSite : listSitesUser) {

                        if (agentSite.getEtat()) {
                            JsonObject jsonSiteObject = new JsonObject();

                            jsonSiteObject.addProperty(GeneralConst.ParamName.SITE_CODE, agentSite.getCodesite().getCode());
                            jsonSiteObject.addProperty(GeneralConst.ParamName.SITE_NAME, agentSite.getCodesite().getIntitule());
                            jsonSiteObject.addProperty(GeneralConst.ParamName.PEAGE, agentSite.getCodesite().getPeage() == null
                                    ? GeneralConst.Number.ZERO
                                    : agentSite.getCodesite().getPeage()
                                            ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                            listUserSiteJson.add(jsonSiteObject);
                        }

                    }
                }

                if (!listSite.isEmpty() && listSite.size() > 0) {
                    for (Site site : listSite) {

                        JsonObject jsonSiteObject = new JsonObject();

                        jsonSiteObject.addProperty(GeneralConst.ParamName.SITE_CODE, site.getCode());
                        jsonSiteObject.addProperty(GeneralConst.ParamName.SITE_NAME, site.getIntitule());
                        jsonSiteObject.addProperty(GeneralConst.ParamName.PEAGE, site.getPeage() == null
                                ? GeneralConst.Number.ZERO
                                : site.getPeage()
                                        ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                        listAllSiteJson.add(jsonSiteObject);
                    }

                }
                if (!listFonction.isEmpty() && listFonction.size() > 0) {
                    for (Fonction fonction : listFonction) {

                        JsonObject jsonFonctionObject = new JsonObject();

                        jsonFonctionObject.addProperty(GeneralConst.ParamName.FONCTION_CODE, fonction.getCode());
                        jsonFonctionObject.addProperty(GeneralConst.ParamName.FONCTION_NAME, fonction.getIntitule());

                        listFonctionJson.add(jsonFonctionObject);
                    }

                }

                if (!listService.isEmpty()) {
                    for (Service site : listService) {

                        JsonObject jsonServiceObject = new JsonObject();

                        jsonServiceObject.addProperty(GeneralConst.ParamName.SERVICE_CODE, site.getCode());
                        jsonServiceObject.addProperty(GeneralConst.ParamName.SERVICE_NAME, site.getIntitule());

                        listAllServiceJson.add(jsonServiceObject);

                    }
                }

                for (SiteBanque siteBanque : listBanqueSite) {

                    if (siteBanque.getEtat()) {

                        JsonObject jsonBanqnueObject = new JsonObject();

                        jsonBanqnueObject.addProperty(GeneralConst.ParamName.BANQUE_CODE, siteBanque.getBanque().getCode());
                        jsonBanqnueObject.addProperty(GeneralConst.ParamName.BANQUE_NAME, siteBanque.getBanque().getIntitule());

                        List<CompteBancaire> compteBancaires = siteBanque.getBanque().getCompteBancaireList();
                        if (compteBancaires != null) {
                            for (CompteBancaire account : compteBancaires) {

                                if (account.getEtat() == 1) {

                                    JsonObject jsonAccountObject = new JsonObject();

                                    jsonAccountObject.addProperty(GeneralConst.ParamName.ACCOUNT_CODE, account.getCode());
                                    jsonAccountObject.addProperty(GeneralConst.ParamName.ACCOUNT_NAME, account.getIntitule());
                                    jsonAccountObject.addProperty(GeneralConst.ParamName.ACCOUNT_DEVISE, account.getDevise().getCode());
                                    jsonAccountObject.addProperty(GeneralConst.ParamName.ACCOUNT_BANQUE_CODE, siteBanque.getBanque().getCode());
                                    jsonAccountObject.addProperty(GeneralConst.ParamName.ACCOUNT_BANQUE_NAME, siteBanque.getBanque().getIntitule());
                                    listAccountJson.add(jsonAccountObject);
                                }

                            }
                        }
                        listbanqueJson.add(jsonBanqnueObject);
                    }

                }

                for (Avis avis : listAvis) {

                    JsonObject jsonAvisObject = new JsonObject();
                    jsonAvisObject.addProperty(TaxationConst.ParamName.ID_AVIS, avis.getId().trim());
                    jsonAvisObject.addProperty(TaxationConst.ParamName.CODE_AVIS, avis.getCode().trim());
                    jsonAvisObject.addProperty(TaxationConst.ParamName.LIBELLE_AVIS, avis.getIntitule().trim());

                    listAVisJson.add(jsonAvisObject);
                }

                listTaux = getTaux();

                if (!listTaux.isEmpty()) {
                    for (Taux taux : listTaux) {

                        JsonObject jsonTauxObject = new JsonObject();

                        jsonTauxObject.addProperty(GeneralConst.ParamName.TAUX_CODE, taux.getId());
                        jsonTauxObject.addProperty(GeneralConst.ParamName.TAUX_DEVIDE_SOURCE, taux.getDevise().getCode());
                        jsonTauxObject.addProperty(GeneralConst.ParamName.TAUX_DEVIDE_DEST, taux.getDeviseDest());
                        jsonTauxObject.addProperty(GeneralConst.ParamName.TAUX_VALUE, taux.getTaux());

                        listTauxJson.add(jsonTauxObject);
                    }
                }

                listTypePenalite = GestionPenaliteBackendBusiness.getListTypePenalites();

                if (!listTypePenalite.isEmpty()) {

                    for (TypePenalite typePenalite : listTypePenalite) {

                        JsonObject jsonTypePenalite = new JsonObject();

                        jsonTypePenalite.addProperty("typePenaliteCode", typePenalite.getCode());
                        jsonTypePenalite.addProperty("typePenaliteName", typePenalite.getIntitule());

                        listTypePenaliteJson.add(jsonTypePenalite);
                    }
                }

                listPenalite = GestionPenaliteBackendBusiness.getListPenalitesV3();

                if (!listPenalite.isEmpty()) {

                    for (Penalite penalite : listPenalite) {

                        JsonObject jsonPenalite = new JsonObject();

                        jsonPenalite.addProperty("penaliteCode", penalite.getCode());
                        jsonPenalite.addProperty("penaliteName", penalite.getIntitule());

                        listPenaliteJson.add(jsonPenalite);
                    }
                }

                listFormeJuridique = IdentificationBusiness.getFormeJuridiques();

                if (!listFormeJuridique.isEmpty()) {

                    for (FormeJuridique formeJuridique : listFormeJuridique) {

                        JsonObject jsonformeJuridique = new JsonObject();

                        jsonformeJuridique.addProperty("formeJuridiqueCode", formeJuridique.getCode());

                        if (formeJuridique.getCode().equals(GeneralConst.ALL)) {
                            jsonformeJuridique.addProperty("formeJuridiqueName", "Tous type de personne");
                        } else {
                            jsonformeJuridique.addProperty("formeJuridiqueName", formeJuridique.getIntitule());
                        }

                        listFormeJuridiqueJson.add(jsonformeJuridique);
                    }
                }

                listUserDivision = ConnexionBusiness.getUserDivision(agent.getCode());

                JsonObject jsonUserDivision;

                if (!listUserDivision.isEmpty()) {

                    for (UtilisateurDivision ud : listUserDivision) {

                        jsonUserDivision = new JsonObject();
                        jsonUserDivision.addProperty("code", ud.getCode());
                        jsonUserDivision.addProperty("codeDivision", ud.getDivision().getCode());
                        jsonUserDivision.addProperty("intituleDivision", ud.getDivision().getIntitule());
                        jsonUserDivision.addProperty("codeBureau", ud.getSite().getCode());
                        jsonUserDivision.addProperty("intituleBureau", ud.getSite().getIntitule());
                        listUserDivisionJson.add(jsonUserDivision);
                    }
                } else {

                    if (agent.getSite().getDivision() != null) {

                        jsonUserDivision = new JsonObject();

                        jsonUserDivision.addProperty("code", 1);
                        jsonUserDivision.addProperty("codeDivision", agent.getSite().getDivision().getCode());
                        jsonUserDivision.addProperty("intituleDivision", agent.getSite().getDivision().getIntitule().toUpperCase());
                        jsonUserDivision.addProperty("codeBureau", agent.getSite().getCode());
                        jsonUserDivision.addProperty("intituleBureau", agent.getSite().getIntitule().toUpperCase());

                        listUserDivisionJson.add(jsonUserDivision);
                    }

                }

                Gson gsonBuilder = new GsonBuilder().create();

                jsonUserObject.addProperty(GeneralConst.ParamName.SITE_USER_LIST, listUserSiteJson.toString());
                jsonUserObject.addProperty(GeneralConst.ParamName.SITE_ALL_LIST, listAllSiteJson.toString());
                jsonUserObject.addProperty(GeneralConst.ParamName.SERVICE_ALL_LIST, listAllServiceJson.toString());
                jsonUserObject.addProperty(GeneralConst.ParamName.BANQUE_USER_LIST, listbanqueJson.toString());
                jsonUserObject.addProperty(GeneralConst.ParamName.ACCOUNT_LIST, listAccountJson.toString());
                jsonUserObject.addProperty(LoginConst.ParamName.RIGHTS_USER_LIST, gsonBuilder.toJson(listRightUsers));
                jsonUserObject.addProperty(GeneralConst.ParamName.AVIS_LIST, listAVisJson.toString());
                jsonUserObject.addProperty(GeneralConst.ParamName.TAUX_LIST, listTauxJson.toString());
                jsonUserObject.addProperty("typePenaliteList", listTypePenaliteJson.toString());
                jsonUserObject.addProperty("penaliteList", listPenaliteJson.toString());
                jsonUserObject.addProperty("formeJuridiqueList", listFormeJuridiqueJson.toString());
                jsonUserObject.addProperty("utilisateurDivisionList", listUserDivisionJson.toString());

                HttpSession session = request.getSession();
                session.setAttribute(GeneralConst.KEY_SESSION_AGENT, agent);
                session.setAttribute(mdp, agent);

                String sqlQuery = "UPDATE T_AGENT SET CONNECTE = 1, DATE_DERNIERE_CONNEXION = GETDATE() WHERE CODE = %s";
                sqlQuery = String.format(sqlQuery, agent.getCode());

                TaxationBusiness.updateEcheanePayment(sqlQuery);

            } else {
                jsonUserObject.addProperty(LoginConst.ParamName.SESSION, false);
            }
            return jsonUserObject.toString();
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public void StringTest() {

    }

    public String getSignatureAgent(HttpServletRequest request) {

        String result;
        String idUser = request.getParameter(LoginConst.ParamName.ID_USER);

        try {

            Agent agent = ConnexionBusiness.getAgentByCode(Integer.valueOf(idUser));
            if (agent == null) {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            } else {
                result = agent.getSignature() == null ? GeneralConst.EMPTY_STRING : agent.getSignature();
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String saveSignatureAgent(HttpServletRequest request) {

        String idUser = request.getParameter(LoginConst.ParamName.ID_USER);
        String signature = request.getParameter(LoginConst.ParamName.SIGNATURE);

        try {

            boolean result = ConnexionBusiness.updateSignature(signature, Integer.valueOf(idUser));
            if (result) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String lastDeconnexion(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            String idUser = request.getParameter("idUser");

            String sqlQuery = "UPDATE T_AGENT SET CONNECTE = 0, DATE_DERNIERE_DECONNEXION = GETDATE() WHERE CODE = %s";
            sqlQuery = String.format(sqlQuery, idUser);

            if (TaxationBusiness.updateEcheanePayment(sqlQuery)) {
                dataReturn = "1";
            } else {
                dataReturn = "0";
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
