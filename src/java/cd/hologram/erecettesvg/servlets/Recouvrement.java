/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.PaiementConst;
import cd.hologram.erecettesvg.constants.PoursuiteConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DernierAvertissement;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionMairie;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.PalierTauxPenalite;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Role;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.pojo.AssujettiListPrint;
import cd.hologram.erecettesvg.pojo.DetailRolePrint;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.ProjectRolePrint;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryRecouvrement;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.Compare;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import static cd.hologram.erecettesvg.util.Tools.generateQRCode;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "Recouvrement", urlPatterns = {"/recouvrement_servlet"})
public class Recouvrement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    List<PeriodeDeclaration> periodeDeclarationList;
    Med med;
    Archive archive;
    Adresse adresse;
    List<NotePerception> notePerceptionList;
    List<NotePerceptionMairie> notePerceptionMairieList;
    String valueOperation = GeneralConst.EMPTY_STRING;
    Role role;
    List<Role> listRoles;
    JsonObject roleJson;
    JsonObject extraitRoleJson;
    List<JsonObject> roleJsonList;
    List<JsonObject> extraitRoleJsonList;
    List<JsonObject> jsonTitrePerceptionList;
    JsonObject jsonNPPenalite;
    JsonObject jsonBP;
    JsonObject detailRoleJson;
    List<JsonObject> detailRoleJsonList;
    LogUser logUser;
    List<ExtraitDeRole> listExtraitDeRole;
    List<DetailRolePrint> listDetailRolePrint;
    List<DetailRolePrint> listDetailRolePrintTemp;

    String niveauExtraitRole = GeneralConst.EMPTY_STRING;
    String codeNc = GeneralConst.EMPTY_STRING;
    String lblArticleBudgetaireDisplay = GeneralConst.EMPTY_STRING;
    String lblArticleBudgetaire = GeneralConst.EMPTY_STRING;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(TaxationConst.ParamName.OPERATION);

        switch (operation) {
            case RecouvrementConst.Operation.LOAD_DEFAILLANT_DECLARATION:
                result = getMed(request);
                break;
            case RecouvrementConst.Operation.LOAD_DEFAILLANT_PAIEMENT:
                result = getMep(request);
                break;
            case RecouvrementConst.Operation.LOAD_DEFAILLANT_PAIEMENT_ROLE:
                result = getMepRole(request);
                break;
            case RecouvrementConst.Operation.PRINT_MED:
                result = saveAndPrintMed(request);
                break;
            case RecouvrementConst.Operation.PRINT_MEP:
                result = saveAndPrintMep(request);
                break;
            case RecouvrementConst.Operation.PRINT_INVITATION_SERVICE:
                result = saveAndPrintInvitationService(request);
                break;
            case RecouvrementConst.Operation.SAVE_ROLE:
                result = saveRole(request);
                break;
            case RecouvrementConst.Operation.LOAD_ROLE:
                result = loadListProjetRole(request);
                break;
            case RecouvrementConst.Operation.VALIDER_ROLE:
                result = validerRole(request);
                break;
            case RecouvrementConst.Operation.ORDONNANCER_EXTRAIT_ROLE:
                valueOperation = RecouvrementConst.Operation.ORDONNANCER_EXTRAIT_ROLE;
                result = ordonnancerExtraitRole2(request);
                break;
            case RecouvrementConst.Operation.LOAD_EXTRAIT_ROLE:
                result = loadExtraitRol2(request);
                break;
            case RecouvrementConst.Operation.LOAD_EXTRAIT_ROLE_BY_ROLE:
                break;
            case RecouvrementConst.Operation.SAVE_DERNIER_AVERTISSEMENT:
                result = saveDernierAvertissement(request);
                break;
            case PoursuiteConst.Operation.LOAD_PENALITE_LIST:
                result = getPenalitiesList();
                break;
            case RecouvrementConst.Operation.SAVE_CONTRAINTE:
                result = saveContrainte(request);
                break;
            case RecouvrementConst.Operation.SAVE_COMMANDEMENT:
                result = saveCommandement(request);
                break;

            case RecouvrementConst.Operation.PRINTER_PROJECT_ROLE:
                result = printerProjectRole(request);
                break;
            case RecouvrementConst.Operation.LOAD_DEFAILLANT_PAIEMENTS:
                result = getNotePerceptionNotMairiePaid(request);
                break;
        }

        out.print(result);
    }

    public String saveAndPrintMep(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String numeroNp,
                assujettiCode,
                assujettiName,
                adresseCode,
                adresseName,
                dateOrdonnancement,
                articleBudgetaireCode,
                articleBudgetaireName,
                exerciceNp,
                printExist,
                dateEcheanceNp,
                codeAgent,
                numeroMed,
                amountNp1,
                amountNp2,
                numeroNc, agentCreat = GeneralConst.EMPTY_STRING;

        try {

            numeroNp = request.getParameter(RecouvrementConst.ParamName.NUMERO_NP);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            adresseCode = request.getParameter(RecouvrementConst.ParamName.ADRESSE_CODE);
            dateOrdonnancement = request.getParameter(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC);
            articleBudgetaireCode = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            exerciceNp = request.getParameter(RecouvrementConst.ParamName.EXERCICE_NP);
            printExist = request.getParameter(RecouvrementConst.ParamName.PRINT_EXISTS);
            dateEcheanceNp = request.getParameter(RecouvrementConst.ParamName.DATE_ECHEANCE_NP);
            numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);
            amountNp1 = request.getParameter(RecouvrementConst.ParamName.AMOUNT_NP_1);
            amountNp2 = request.getParameter(RecouvrementConst.ParamName.AMOUNT_NP_2);
            numeroNc = request.getParameter(RecouvrementConst.ParamName.NUMERO_NC);
            codeAgent = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            String nbreMoisRetard = request.getParameter("nbreMoisRetard");

            articleBudgetaireName = GeneralConst.EMPTY_STRING;
            adresseName = GeneralConst.EMPTY_STRING;
            BigDecimal amountPrincipal = new BigDecimal(0);

            amountPrincipal = amountPrincipal.add(BigDecimal.valueOf(Double.valueOf(amountNp2)));

            float amountPenalite = ((amountPrincipal.floatValue() * 2 / 100) * Integer.valueOf(nbreMoisRetard));

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    if (RecouvrementBusiness.saveMed(
                            articleBudgetaireCode,
                            assujettiCode,
                            exerciceNp,
                            dateEcheanceNp,
                            adresseCode,
                            codeAgent,
                            amountPrincipal.floatValue(),
                            0, "INVITATION_SERVICE",
                            numeroNp,
                            numeroNc, amountPenalite)) {

                        Med med = RecouvrementBusiness.getMedByNotePerception(numeroNp);

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();

                            dataReturn = printDocument.createMep(
                                    med.getId(), articleBudgetaireName, assujettiName,
                                    adresseName, dateEcheanceNp, dateOrdonnancement, numeroNp,
                                    amountNp1, assujettiCode, codeAgent);

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAndPrintInvitationService(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String numeroNp,
                assujettiCode,
                assujettiName,
                adresseCode,
                adresseName,
                dateOrdonnancement,
                articleBudgetaireCode,
                articleBudgetaireName,
                exerciceNp,
                printExist,
                dateEcheanceNp,
                codeAgent,
                numeroMed,
                amountNp1,
                amountNp2,
                numeroNc, agentCreat = GeneralConst.EMPTY_STRING;

        try {

            numeroNp = request.getParameter(RecouvrementConst.ParamName.NUMERO_NP);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            adresseCode = request.getParameter(RecouvrementConst.ParamName.ADRESSE_CODE);
            dateOrdonnancement = request.getParameter(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC);
            articleBudgetaireCode = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            exerciceNp = request.getParameter(RecouvrementConst.ParamName.EXERCICE_NP);
            printExist = request.getParameter(RecouvrementConst.ParamName.PRINT_EXISTS);
            dateEcheanceNp = request.getParameter(RecouvrementConst.ParamName.DATE_ECHEANCE_NP);
            numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);
            amountNp1 = request.getParameter(RecouvrementConst.ParamName.AMOUNT_NP_1);
            amountNp2 = request.getParameter(RecouvrementConst.ParamName.AMOUNT_NP_2);
            numeroNc = request.getParameter(RecouvrementConst.ParamName.NUMERO_NC);
            codeAgent = request.getParameter(RecouvrementConst.ParamName.USER_ID);

            articleBudgetaireName = GeneralConst.EMPTY_STRING;
            adresseName = GeneralConst.EMPTY_STRING;

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    if (RecouvrementBusiness.saveMed(
                            articleBudgetaireCode,
                            assujettiCode,
                            exerciceNp,
                            dateEcheanceNp,
                            adresseCode,
                            codeAgent,
                            Float.valueOf(amountNp2),
                            0, "PAIEMENT",
                            numeroNp,
                            numeroNc, 0)) {

                        Med med = RecouvrementBusiness.getMedByNotePerception(numeroNp);

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();
//                            
                            dataReturn = printDocument.createInvitationService(
                                    med.getId(), articleBudgetaireName, assujettiName,
                                    adresseName, dateEcheanceNp, dateOrdonnancement, numeroNp,
                                    amountNp1, assujettiCode, codeAgent);

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAndPrintMed(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String codeArticleBudgetaire = GeneralConst.EMPTY_STRING;
        String libelleArticleBudgetaire = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;
        String assujettiName = GeneralConst.EMPTY_STRING;
        String periodeDeclaration = GeneralConst.EMPTY_STRING;
        String echeanceDeclaration = GeneralConst.EMPTY_STRING;
        String adresseCode = GeneralConst.EMPTY_STRING;
        String adresseName = GeneralConst.EMPTY_STRING;
        String amountPeriodeDeclaration = GeneralConst.EMPTY_STRING;
        //String amountPeriodeDeclarationToString = GeneralConst.EMPTY_STRING;
        String agentCreat = GeneralConst.EMPTY_STRING;
        String periodeDeclarationId = GeneralConst.EMPTY_STRING;
        String printExist = GeneralConst.EMPTY_STRING;
        String numeroMed = GeneralConst.EMPTY_STRING;

        try {

            codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            libelleArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            periodeDeclaration = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION);
            echeanceDeclaration = request.getParameter(RecouvrementConst.ParamName.ECHEANCE_DECLARATION);
            adresseCode = request.getParameter(RecouvrementConst.ParamName.ADRESSE_CODE);
            adresseName = request.getParameter(RecouvrementConst.ParamName.ADRESSE_NAME);
            printExist = request.getParameter(RecouvrementConst.ParamName.PRINT_EXISTS);
            amountPeriodeDeclaration = request.getParameter(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION);
            agentCreat = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            periodeDeclarationId = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID);
            numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    PeriodeDeclaration pd = DeclarationBusiness.getPeriodeDeclarationById(periodeDeclarationId);

                    if (RecouvrementBusiness.saveMed(
                            codeArticleBudgetaire,
                            assujettiCode,
                            periodeDeclaration,
                            echeanceDeclaration,
                            adresseCode, agentCreat,
                            pd == null ? 0 : pd.getAssujetissement().getValeur().floatValue(),
                            Integer.valueOf(periodeDeclarationId), "DECLARATION",
                            GeneralConst.EMPTY_STRING, GeneralConst.EMPTY_STRING, 0)) {

                        Med med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(periodeDeclarationId));

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();

                            dataReturn = printDocument.createMed(
                                    med, libelleArticleBudgetaire, assujettiName,
                                    adresseName, periodeDeclaration,
                                    echeanceDeclaration);

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getMep(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;

        String codeService = GeneralConst.EMPTY_STRING;
        String codeSite = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        try {

            advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);

            codeService = request.getParameter(RecouvrementConst.ParamName.CODE_SERVICE);
            codeSite = request.getParameter(RecouvrementConst.ParamName.CODE_SITE);
            dateDebut = request.getParameter(RecouvrementConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(RecouvrementConst.ParamName.DATE_FIN);

            notePerceptionList = new ArrayList<>();

            advancedSearch = advancedSearch == null ? "1" : advancedSearch;

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    notePerceptionList = RecouvrementBusiness.getListNotePerceptionNotPaidByAssujetti(assujettiCode);

                    break;
                case GeneralConst.Number.ONE:

                    notePerceptionList = RecouvrementBusiness.getListNotePerceptionNotPaidByService_SiteAndPeriode(
                            codeService, codeSite, dateDebut, dateFin);
                    break;
            }

            if (!notePerceptionList.isEmpty()) {

                List<JsonObject> npJsonList = new ArrayList<>();

                for (NotePerception np : notePerceptionList) {

                    JsonObject npJson = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Med med = new Med();
                    Agent agent = new Agent();

                    String dateOrd = GeneralConst.EMPTY_STRING;
                    Date dateEcheance;

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NP, np.getNumero());

                    med = RecouvrementBusiness.getMedByNotePerception(np.getNumero());

                    if (med != null) {

                        npJson.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ONE);

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());

                        npJson.addProperty(RecouvrementConst.ParamName.AMOUNT_PENALITE,
                                med.getMontantPenalite());

                        if (med.getDateEcheance() != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                    ConvertDate.formatDateToString(med.getDateEcheance()));
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (med.getDateReception() != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                    ConvertDate.formatDateToString(med.getDateReception()));
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        npJson.addProperty(RecouvrementConst.ParamName.DATE_CREATION_MED_OR_MEP,
                                ConvertDate.formatDateToString(med.getDateCreat()));

                        agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                        if (agent != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                    agent.toString().toUpperCase());
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }
                        archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                        if (archive != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    archive.getDocumentString());
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ZERO);
                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_MED, GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_CREATION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    dateEcheance = Tools.formatStringFullToDate(np.getDateEcheancePaiement());

                    npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_NP,
                            ConvertDate.formatDateToString(dateEcheance));

                    npJson.addProperty("nbreMoisRetard", TaxationBusiness.getDateDiffBetwenTwoDates(
                            ConvertDate.formatDateToString(dateEcheance)));
                    //npJson.addProperty("nbreMoisRetard", "");

                    npJson.addProperty(RecouvrementConst.ParamName.AMOUNT_NP, np.getSolde());
                    npJson.addProperty(RecouvrementConst.ParamName.DEVISE_NP, np.getDevise());

                    if (np.getNoteCalcul() != null) {

                        dateOrd = ConvertDate.formatDateToString(np.getNoteCalcul().getDateValidation());

                        if (dateOrd != null && !dateOrd.isEmpty()) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                    dateOrd);
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                    GeneralConst.EMPTY_STRING);
                        }

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NC, np.getNoteCalcul().getNumero());

                        if (np.getNoteCalcul().getExercice() != null
                                && !np.getNoteCalcul().getExercice().isEmpty()) {

                            npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                    np.getNoteCalcul().getExercice());

                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        ArticleBudgetaire ab = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire();

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                ab.getCode());

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                Tools.getArticleByNC_V2(np.getNoteCalcul()));

                        if (np.getNoteCalcul().getPersonne() != null
                                && !np.getNoteCalcul().getPersonne().isEmpty()) {

                            personne = IdentificationBusiness.getPersonneByCode(np.getNoteCalcul().getPersonne());

                            if (personne != null) {

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        personne.getCode());

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                        personne.toString().toUpperCase());

                                typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                        personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                                assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                        personne.toString().toUpperCase()).concat("</span>"));

                                adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                                if (adresse != null) {

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                            adresse.getId());

                                    adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                            "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                                    "</span>"));

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                            adresse.toString().toUpperCase());

                                } else {

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                            GeneralConst.EMPTY_STRING);

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                            GeneralConst.EMPTY_STRING);
                                }

                                assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                        assujettiNameComposite);

                            } else {
                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        GeneralConst.EMPTY_STRING);

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        GeneralConst.EMPTY_STRING);
                                npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);

                                npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                        GeneralConst.EMPTY_STRING);
                            }
                        } else {

                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);

                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);
                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    GeneralConst.EMPTY_STRING);

                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    GeneralConst.EMPTY_STRING);
                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NC,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);

                    }

                    npJsonList.add(npJson);
                }

                dataReturn = npJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getMepRole(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;

        String codeService = GeneralConst.EMPTY_STRING;
        String codeSite = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        try {

            advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);

            codeService = request.getParameter(RecouvrementConst.ParamName.CODE_SERVICE);
            codeSite = request.getParameter(RecouvrementConst.ParamName.CODE_SITE);
            dateDebut = request.getParameter(RecouvrementConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(RecouvrementConst.ParamName.DATE_FIN);

            notePerceptionList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    notePerceptionList = RecouvrementBusiness.getListNotePerceptionNotPaidByAssujettiRole(assujettiCode);

                    break;
                case GeneralConst.Number.ONE:

                    notePerceptionList = RecouvrementBusiness.getListNotePerceptionNotPaidByService_SiteAndPeriodeRole(
                            codeService, codeSite, dateDebut, dateFin);
                    break;
            }

            if (!notePerceptionList.isEmpty()) {

                List<JsonObject> npJsonList = new ArrayList<>();

                for (NotePerception np : notePerceptionList) {

                    JsonObject npJson = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Med med = new Med();
                    Agent agent = new Agent();

                    String dateOrd = GeneralConst.EMPTY_STRING;
                    Date dateEcheance;

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NP, np.getNumero());

                    med = RecouvrementBusiness.getMedByNotePerception(np.getNumero());

                    if (med != null) {

                        npJson.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ONE);

                        npJson.addProperty(RecouvrementConst.ParamName.AMOUNT_PENALITE,
                                med.getMontantPenalite());

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());

                        if (med.getDateEcheance() != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                    ConvertDate.formatDateToString(med.getDateEcheance()));
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (med.getDateReception() != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                    ConvertDate.formatDateToString(med.getDateReception()));
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        npJson.addProperty(RecouvrementConst.ParamName.DATE_CREATION_MED_OR_MEP,
                                ConvertDate.formatDateToString(med.getDateCreat()));

                        agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                        if (agent != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                    agent.toString().toUpperCase());
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                    GeneralConst.EMPTY_STRING);
                        }
                        archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                        if (archive != null) {
                            npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    archive.getDocumentString());
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ZERO);
                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_MED, GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DATE_CREATION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    dateEcheance = Tools.formatStringFullToDate(np.getDateEcheancePaiement());

                    npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_NP,
                            ConvertDate.formatDateToString(dateEcheance));

                    npJson.addProperty(RecouvrementConst.ParamName.AMOUNT_NP, np.getSolde());
                    npJson.addProperty(RecouvrementConst.ParamName.DEVISE_NP, np.getDevise());

                    if (np.getNoteCalcul() != null) {

                        dateOrd = ConvertDate.formatDateToString(np.getNoteCalcul().getDateValidation());

                        if (dateOrd != null && !dateOrd.isEmpty()) {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                    dateOrd);
                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                    GeneralConst.EMPTY_STRING);
                        }

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NC, np.getNoteCalcul().getNumero());

                        if (np.getNoteCalcul().getExercice() != null
                                && !np.getNoteCalcul().getExercice().isEmpty()) {

                            npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                    np.getNoteCalcul().getExercice());

                        } else {
                            npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                    GeneralConst.EMPTY_STRING);
                        }

                        ArticleBudgetaire ab = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire();

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                ab.getCode());

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                Tools.getArticleByNC_V2(np.getNoteCalcul()));

                        if (np.getNoteCalcul().getPersonne() != null
                                && !np.getNoteCalcul().getPersonne().isEmpty()) {

                            personne = IdentificationBusiness.getPersonneByCode(np.getNoteCalcul().getPersonne());

                            if (personne != null) {

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        personne.getCode());

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                        personne.toString().toUpperCase());

                                typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                        personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                                assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                        personne.toString().toUpperCase()).concat("</span>"));

                                adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                                if (adresse != null) {

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                            adresse.getId());

                                    adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                            "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                                    "</span>"));

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                            adresse.toString().toUpperCase());

                                } else {

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                            GeneralConst.EMPTY_STRING);

                                    npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                            GeneralConst.EMPTY_STRING);
                                }

                                assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                        assujettiNameComposite);

                            } else {
                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        GeneralConst.EMPTY_STRING);

                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                        GeneralConst.EMPTY_STRING);
                                npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);

                                npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                                npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                        GeneralConst.EMPTY_STRING);
                            }
                        } else {

                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);

                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);
                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    GeneralConst.EMPTY_STRING);

                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    GeneralConst.EMPTY_STRING);
                            npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {

                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NC,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                GeneralConst.EMPTY_STRING);

                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);

                    }

                    npJsonList.add(npJson);
                }

                dataReturn = npJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getMed(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String sqlQueryParam = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String codeArticleBudgetaire = GeneralConst.EMPTY_STRING;
        String codePeriodicite = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;
        String periodValueMonth = GeneralConst.EMPTY_STRING;
        String periodValueYear = GeneralConst.EMPTY_STRING;

        try {

            advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            codePeriodicite = request.getParameter(RecouvrementConst.ParamName.CODE_PERIODICITE);
            periodValueMonth = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_MONTH);
            periodValueYear = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_YEAR);

            periodeDeclarationList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    periodeDeclarationList = RecouvrementBusiness.getListDefaillantDeclarationBySimpleSearch(
                            assujettiCode);
                    break;

                case GeneralConst.Number.ONE:

                    switch (codePeriodicite) {

                        case "PR0032015": // MENSUEL
                            sqlQueryParam = " AND MONTH(PD.DEBUT) = %s AND YEAR(PD.DEBUT) = %s";
                            sqlQueryParam = String.format(sqlQueryParam, periodValueMonth, periodValueYear);
                            break;
                        case "PR0042015": // ANNUEL
                            sqlQueryParam = " AND YEAR(PD.DEBUT) = %s";
                            sqlQueryParam = String.format(sqlQueryParam, periodValueYear);
                            break;
                    }

                    periodeDeclarationList = RecouvrementBusiness.getListDefaillantDeclarationByAdvancedSearch(
                            codeArticleBudgetaire, sqlQueryParam);

                    break;
            }

            if (!periodeDeclarationList.isEmpty()) {

                List<JsonObject> pdJsonList = new ArrayList<>();

                for (PeriodeDeclaration pd : periodeDeclarationList) {

                    JsonObject pdJsonObject = new JsonObject();
                    med = new Med();
                    Palier palier = new Palier();
                    Agent agent = new Agent();

                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;
                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;

                    adresse = new Adresse();

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID, pd.getId());

                    med = RecouvrementBusiness.getMedByPerideDeclaration(pd.getId());

                    if (med != null) {

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ONE);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());

                        /*if (med.getDateEcheance() != null) {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                         ConvertDate.formatDateToString(med.getDateEcheance()));
                         } else {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);
                         }

                         if (med.getDateReception() != null) {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                         ConvertDate.formatDateToString(med.getDateReception()));
                         } else {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);
                         }

                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_CREATION_MED_OR_MEP,
                         ConvertDate.formatDateToString(med.getDateCreat()));

                         agent = GeneralBusiness.getAgentByCode(med.getAgentCreat());

                         if (agent != null) {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                         agent.toString().toUpperCase());
                         } else {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);
                         }*/
                        archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                        if (archive != null) {
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    archive.getDocumentString());
                        } else {
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                    GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS,
                                GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED,
                                GeneralConst.EMPTY_STRING);
                        /*pdJsonObject.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_MED_OR_MEP,
                         GeneralConst.EMPTY_STRING);*/
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (pd.getAssujetissement() != null) {

                        if (advancedSearch.equals(GeneralConst.Number.ZERO)) {
                            codePeriodicite = pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode();
                        }

                        float amountPeriode = 0;
                        float taux = 0;
                        float valeurBase = 0;

                        String devisePeriode = GeneralConst.EMPTY_STRING;

                        palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonne(
                                pd.getAssujetissement().getArticleBudgetaire().getCode(),
                                pd.getAssujetissement().getTarif().getCode(),
                                pd.getAssujetissement().getPersonne().getFormeJuridique().getCode());

                        if (palier != null) {

                            if (palier.getMultiplierValeurBase() == GeneralConst.Numeric.ONE) {

                                taux = palier.getTaux().floatValue();
                                valeurBase = pd.getAssujetissement().getValeur().floatValue();

                                amountPeriode = (taux * valeurBase);

                            } else {
                                amountPeriode = pd.getAssujetissement().getValeur().floatValue();
                            }

                            devisePeriode = palier.getDevise().getCode();

                        } else {
                            amountPeriode = 0;
                        }

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION,
                                amountPeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION,
                                devisePeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                pd.getAssujetissement().getArticleBudgetaire().getCode());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                pd.getAssujetissement().getArticleBudgetaire().getIntitule());

                        if (pd.getAssujetissement().getPersonne() != null) {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    pd.getAssujetissement().getPersonne().getCode());

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    pd.getAssujetissement().getPersonne().toString().toUpperCase());

                            typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    pd.getAssujetissement().getPersonne().getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                    pd.getAssujetissement().getPersonne().getCode());

                            if (adresse != null) {
                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat("</span>"));

                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        adresse.getId());
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        adresse.toString().toUpperCase());

                            } else {
                                adresseAssujetti = GeneralConst.EMPTY_STRING;
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(pd.getAssujetissement().getPersonne().toString().toUpperCase()).concat("</span>"));

                            assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    assujettiNameComposite);

                        } else {
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    GeneralConst.EMPTY_STRING);

                        }
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.ECHEANCE_DECLARATION,
                            ConvertDate.formatDateToString(pd.getDateLimite()));

                    String echeance = Tools.getPeriodeIntitule(pd.getDebut(), codePeriodicite);

                    if (!echeance.isEmpty()) {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, echeance.toUpperCase());
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    pdJsonList.add(pdJsonObject);

                }

                dataReturn = pdJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveRole(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String periode = GeneralConst.EMPTY_STRING;
        String userId = request.getParameter(RecouvrementConst.ParamName.USER_ID);
        String description = request.getParameter(RecouvrementConst.ParamName.DESCRIPTION_ROLE);
        String detailRoles = request.getParameter(RecouvrementConst.ParamName.DETAIL_ROLE);
        String monthDebut = request.getParameter(RecouvrementConst.ParamName.MONTH_SEARCH_START);
        String monthFin = request.getParameter(RecouvrementConst.ParamName.MONTH_SEARCH_END);
        String year = request.getParameter(RecouvrementConst.ParamName.YEAR_SEARCH);
        String articleRole = request.getParameter(RecouvrementConst.ParamName.ARTICLE_ROLE);

        try {

            String exists = RecouvrementBusiness.checkExistArticleRole(articleRole.trim());
            int day = 0;
            String dayToStr = GeneralConst.EMPTY_STRING;
            Date datePeriodeStart = null;
            Date datePeriodeEnd = null;

            List<String> listAssujettiFull = new ArrayList<>();
            List<String> listAssujettiClean = new ArrayList<>();

            if (exists != null && !exists.isEmpty()) {

                switch (exists.trim()) {
                    case GeneralConst.Number.ONE:
                        dataReturn = GeneralConst.ResultCode.VALUE_EXIST;
                        break;
                    case GeneralConst.Number.ZERO:

                        if (monthDebut != null && !monthDebut.isEmpty()) {
                            day = Tools.getDayOfMonthByYear(Integer.valueOf(year), Integer.valueOf(monthDebut));

                            if (day > GeneralConst.Numeric.ZERO && day < GeneralConst.Numeric.TEN) {
                                dayToStr = GeneralConst.Number.ZERO.concat(day + GeneralConst.EMPTY_STRING);
                            } else if (day > GeneralConst.Numeric.NINE) {
                                dayToStr = (day + GeneralConst.EMPTY_STRING);
                            }

                            if (!dayToStr.isEmpty()) {
                                monthDebut = monthDebut.length() == GeneralConst.Numeric.ONE ? GeneralConst.Number.ZERO.concat(monthDebut) : monthDebut;
                                periode = dayToStr.concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE)
                                        .concat(monthDebut).concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE).concat(year);
                                datePeriodeStart = ConvertDate.formatDate(periode.trim());
                            }
                        }

                        if (monthFin != null && !monthFin.isEmpty()) {
                            day = Tools.getDayOfMonthByYear(Integer.valueOf(year), Integer.valueOf(monthFin));

                            if (day > GeneralConst.Numeric.ZERO && day < GeneralConst.Numeric.TEN) {
                                dayToStr = GeneralConst.Number.ZERO.concat(day + GeneralConst.EMPTY_STRING);
                            } else if (day > GeneralConst.Numeric.NINE) {
                                dayToStr = (day + GeneralConst.EMPTY_STRING);
                            }

                            if (!dayToStr.isEmpty()) {
                                monthDebut = monthFin.length() == GeneralConst.Numeric.ONE ? GeneralConst.Number.ZERO.concat(monthFin) : monthFin;
                                periode = dayToStr.concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE)
                                        .concat(monthFin).concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE).concat(year);
                                datePeriodeEnd = ConvertDate.formatDate(periode.trim());
                            }
                        }

                        role = new Role();

                        role.setPeriodeDebut(datePeriodeStart);
                        role.setPeriodeFin(datePeriodeEnd);

                        role.setAgentCreat(new Agent(Integer.valueOf(userId)));
                        role.setObservationReceveur(description);
                        role.setArticleRole(articleRole);

                        JSONArray jsonArray = new JSONArray(detailRoles);
                        listDetailRolePrint = new ArrayList<>();
                        listDetailRolePrintTemp = new ArrayList<>();

                        for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DetailRolePrint detailRolePrint = new DetailRolePrint();

                            detailRolePrint.setDocumentReference(
                                    jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE));

                            detailRolePrint.setTypeDocument(
                                    jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT));
                            detailRolePrint.setAssujetti(jsonObject.getString(
                                    NotePerceptionConst.ParamName.CODE_ASSUJETTI));

                            listDetailRolePrint.add(detailRolePrint);

                            listAssujettiFull.add(jsonObject.getString(NotePerceptionConst.ParamName.CODE_ASSUJETTI));

                        }

                        if (!listAssujettiFull.isEmpty()) {
                            listAssujettiClean = listAssujettiFull.stream().distinct().collect(Collectors.toList());
                        }

                        //saveLogUserInfo(request);
                        boolean result = RecouvrementBusiness.saveRole(
                                role, logUser, listDetailRolePrint,
                                listAssujettiClean, userId, articleRole.trim());

                        if (result) {
                            dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                        break;
                }
            } else {
                if (monthDebut != null && !monthDebut.isEmpty()) {
                    day = Tools.getDayOfMonthByYear(Integer.valueOf(year), Integer.valueOf(monthDebut));

                    if (day > GeneralConst.Numeric.ZERO && day < GeneralConst.Numeric.TEN) {
                        dayToStr = GeneralConst.Number.ZERO.concat(day + GeneralConst.EMPTY_STRING);
                    } else if (day > GeneralConst.Numeric.NINE) {
                        dayToStr = (day + GeneralConst.EMPTY_STRING);
                    }

                    if (!dayToStr.isEmpty()) {
                        monthDebut = monthDebut.length() == GeneralConst.Numeric.ONE ? GeneralConst.Number.ZERO.concat(monthDebut) : monthDebut;
                        periode = dayToStr.concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE)
                                .concat(monthDebut).concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE).concat(year);
                        datePeriodeStart = ConvertDate.formatDate(periode.trim());
                    }
                }

                if (monthFin != null && !monthFin.isEmpty()) {
                    day = Tools.getDayOfMonthByYear(Integer.valueOf(year), Integer.valueOf(monthFin));

                    if (day > GeneralConst.Numeric.ZERO && day < GeneralConst.Numeric.TEN) {
                        dayToStr = GeneralConst.Number.ZERO.concat(day + GeneralConst.EMPTY_STRING);
                    } else if (day > GeneralConst.Numeric.NINE) {
                        dayToStr = (day + GeneralConst.EMPTY_STRING);
                    }

                    if (!dayToStr.isEmpty()) {
                        monthDebut = monthFin.length() == GeneralConst.Numeric.ONE ? GeneralConst.Number.ZERO.concat(monthFin) : monthFin;
                        periode = dayToStr.concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE)
                                .concat(monthFin).concat(GeneralConst.SEPARATOR_SLASH_NO_SPACE).concat(year);
                        datePeriodeEnd = ConvertDate.formatDate(periode.trim());
                    }
                }

                role = new Role();
                role.setPeriodeDebut(datePeriodeStart);
                role.setPeriodeFin(datePeriodeEnd);
                role.setAgentCreat(new Agent(Integer.valueOf(userId)));
                role.setObservationReceveur(description);
                role.setArticleRole(articleRole);

                JSONArray jsonArray = new JSONArray(detailRoles);
                listDetailRolePrint = new ArrayList<>();
                listDetailRolePrintTemp = new ArrayList<>();

                for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    DetailRolePrint detailRolePrint = new DetailRolePrint();

                    detailRolePrint.setDocumentReference(
                            jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE));

                    detailRolePrint.setTypeDocument(
                            jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT));
                    detailRolePrint.setAssujetti(jsonObject.getString(NotePerceptionConst.ParamName.CODE_ASSUJETTI));

                    listDetailRolePrint.add(detailRolePrint);

                    listAssujettiFull.add(jsonObject.getString(NotePerceptionConst.ParamName.CODE_ASSUJETTI));

                }

                if (!listAssujettiFull.isEmpty()) {
                    listAssujettiClean = listAssujettiFull.stream().distinct().collect(Collectors.toList());
                }

                saveLogUserInfo(request);

                boolean result = RecouvrementBusiness.saveRole(
                        role, logUser, listDetailRolePrint,
                        listAssujettiClean, userId, articleRole.trim());

                if (result) {
                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (NumberFormatException | JSONException e) {
            //Integer value = Tools.getCodeAgentFromSession(request);
            //CustumException.LogException(e, RecouvrementConst.Operation.SAVE_ROLE, value);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    HashMap<String, Object[]> bulkUpdateNP;
    int counter = 0;

    HashMap<String, Object[]> bulkQuery = new HashMap<>();

    private String validerRole(HttpServletRequest request) {

        String dataReturn;
        String numRole = request.getParameter(RecouvrementConst.ParamName.ROLE_ID);
        String observationRole = request.getParameter(RecouvrementConst.ParamName.DESCRIPTION_ROLE);
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String operation = request.getParameter(GeneralConst.OPERATION);
        String stateRole = request.getParameter(RecouvrementConst.ParamName.STATE_ROLE);

        bulkUpdateNP = new HashMap<>();
        counter = GeneralConst.Numeric.ZERO;
        boolean result;

        logUser = new LogUser(request, operation);
        try {

            counter++;

            switch (stateRole) {

                case GeneralConst.Number.TWO:

                    bulkUpdateNP.put(counter + SQLQueryRecouvrement.Role.VALIDATION_ROLE_BY_DIRECTION,
                            new Object[]{
                                observationRole,
                                numRole});

//                    logUser.setEvent(RecouvrementConst.Operation.VALIDER_ROLE_BY_DIRECTION);
                    break;

                case GeneralConst.Number.THREE:

                    bulkUpdateNP.put(counter + SQLQueryRecouvrement.Role.VALIDATION_ROLE_BY_RECEVEUR,
                            new Object[]{
                                observationRole,
                                numRole});

//                    logUser.setEvent(RecouvrementConst.Operation.VALIDER_ROLE_BY_RECEVEUR);
                    break;
            }

//            counter++;
//            logUser.setReferenceOccurence(numRole);
//
//            bulkUpdateNP.put(counter + SQLQueryGeneral.EXEC_F_NEW_LOG,
//                    new Object[]{
//                        idUser,
//                        logUser.getMacAddress(),
//                        logUser.getIpAddress(),
//                        logUser.getHostName(),
//                        logUser.getReferenceOccurence(),
//                        logUser.getEvent(),
//                        logUser.getBrowserContext(),
//                        GeneralConst.TablesNames.T_ROLE
//                    });
            result = RecouvrementBusiness.executeQueryBulkInsertRole(bulkUpdateNP);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadExtraitRol2(HttpServletRequest request) {

        String dataReturn;

        String codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
        String codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
        String valueSearch = request.getParameter(RecouvrementConst.ParamName.ARTICLE_ROLE);
        String dateDebut = request.getParameter(GeneralConst.ParamName.DATE_DEBUT);
        String dateFin = request.getParameter(GeneralConst.ParamName.DATE_FIN);
        String typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
        String typeRegister = request.getParameter(RecouvrementConst.ParamName.TYPE_REGISTER);
        String critereSearch = request.getParameter(RecouvrementConst.ParamName.CRITERE_SEARCH);
        String codeEntite = request.getParameter(GeneralConst.ParamName.CODE_ENTITE);
        String codeAgent = request.getParameter(GeneralConst.ParamName.CODE_AGENT);

        try {

            listExtraitDeRole = new ArrayList<>();

            listExtraitDeRole = RecouvrementBusiness.loadExtraitRoles(
                    Integer.valueOf(typeSearch),
                    valueSearch, codeSite,
                    codeService, dateDebut, dateFin,
                    typeRegister, critereSearch,
                    codeEntite, codeAgent);

            List<JsonObject> deatailstRoleJsonList;
            List<NotePerception> listNpOfExtraitRole;

            if (!listExtraitDeRole.isEmpty()) {

                extraitRoleJsonList = new ArrayList<>();

                ArticleBudgetaire fkAbAmende;

                for (ExtraitDeRole extraitDeRole : listExtraitDeRole) {

                    extraitRoleJson = new JsonObject();
                    jsonTitrePerceptionList = new ArrayList<>();

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.ARTICLE_ROLE,
                            extraitDeRole.getArticleRole().toUpperCase().trim());

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                            extraitDeRole.getId());

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.AssujettiName,
                            extraitDeRole.getFkPersonne().toString().toUpperCase());
                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                            extraitDeRole.getFkPersonne().getCode());

//                    extraitRoleJson.addProperty(EchelonnementConst.ParamName.LegalFormName,
//                            extraitDeRole.getFkPersonne().getFormeJuridique().getIntitule().toUpperCase());
                    String converDateToString;

                    converDateToString = Tools.formatDateToString(extraitDeRole.getDateCreat());
                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.DATE_CREATE_EXTRAIT_ROLE,
                            converDateToString);

                    niveauExtraitRole = GeneralConst.EMPTY_STRING;

                    niveauExtraitRole = "N1"; //RecouvrementBusiness.getNiveauExtraitRole(extraitDeRole.getId().trim());

                    if (!niveauExtraitRole.isEmpty()) {

                        extraitRoleJson.addProperty(RecouvrementConst.ParamName.NIVEAU_EXTRAIT_ROLE,
                                niveauExtraitRole);

                        JsonObject levelJsonData;
                        List<JsonObject> levelJsonDataList = new ArrayList<>();

                        switch (niveauExtraitRole.trim()) {

                            case GeneralConst.Level.LEVEL_ZERO:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                break;

                            case GeneralConst.Level.LEVEL_ONE:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                if (!extraitDeRole.getDernierAvertissementList().isEmpty()) {

                                    for (DernierAvertissement da : extraitDeRole.getDernierAvertissementList()) {

                                        levelJsonData = new JsonObject();

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                                extraitDeRole.getId());
                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                                GeneralConst.Level.LEVEL_ONE);

                                        converDateToString = Tools.formatDateToString(da.getDateCreat());

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                                converDateToString);

                                        levelJsonDataList.add(levelJsonData);
                                    }
                                }

                                break;

                            case GeneralConst.Level.LEVEL_TWO:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                if (!extraitDeRole.getDernierAvertissementList().isEmpty()) {

                                    for (DernierAvertissement da : extraitDeRole.getDernierAvertissementList()) {

                                        levelJsonData = new JsonObject();

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                                extraitDeRole.getId());
                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                                GeneralConst.Level.LEVEL_ONE);

                                        converDateToString = Tools.formatDateToString(da.getDateCreat());

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                                converDateToString);

                                        levelJsonDataList.add(levelJsonData);
                                    }
                                }

//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().isEmpty()) {
//
//                                    for (Contrainte contrainte
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_TWO);
//
//                                        converDateToString = Tools.formatDateToString(contrainte.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
                                break;
                            case GeneralConst.Level.LEVEL_THREE:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                if (!extraitDeRole.getDernierAvertissementList().isEmpty()) {

                                    for (DernierAvertissement da : extraitDeRole.getDernierAvertissementList()) {

                                        levelJsonData = new JsonObject();

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                                extraitDeRole.getId());
                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                                GeneralConst.Level.LEVEL_ONE);

                                        converDateToString = Tools.formatDateToString(da.getDateCreat());

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                                converDateToString);

                                        levelJsonDataList.add(levelJsonData);
                                    }
                                }

//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().isEmpty()) {
//
//                                    for (Contrainte contrainte
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_TWO);
//
//                                        converDateToString = Tools.formatDateToString(contrainte.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().isEmpty()) {
//
//                                    for (Commandement commandement
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_THREE);
//
//                                        converDateToString = Tools.formatDateToString(commandement.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
                                break;
                            case GeneralConst.Level.LEVEL_FOUR:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                if (!extraitDeRole.getDernierAvertissementList().isEmpty()) {

                                    for (DernierAvertissement da : extraitDeRole.getDernierAvertissementList()) {

                                        levelJsonData = new JsonObject();

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                                extraitDeRole.getId());
                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                                GeneralConst.Level.LEVEL_ONE);

                                        converDateToString = Tools.formatDateToString(da.getDateCreat());

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                                converDateToString);

                                        levelJsonDataList.add(levelJsonData);
                                    }
                                }

//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().isEmpty()) {
//
//                                    for (Contrainte contrainte
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_TWO);
//
//                                        converDateToString = Tools.formatDateToString(contrainte.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().isEmpty()) {
//
//                                    for (Commandement commandement
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_THREE);
//
//                                        converDateToString = Tools.formatDateToString(commandement.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getLettreSaisieList().isEmpty()) {
//
//                                    for (LettreSaisie lettreSaisie
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getLettreSaisieList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_FOUR);
//
//                                        converDateToString = Tools.formatDateToString(lettreSaisie.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
                                break;
                            case GeneralConst.Level.LEVEL_FIVE:

                                levelJsonData = new JsonObject();

                                levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                        extraitDeRole.getId());
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                        GeneralConst.Level.LEVEL_ZERO);
                                levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                        converDateToString);

                                levelJsonDataList.add(levelJsonData);

                                if (!extraitDeRole.getDernierAvertissementList().isEmpty()) {

                                    for (DernierAvertissement da : extraitDeRole.getDernierAvertissementList()) {

                                        levelJsonData = new JsonObject();

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                                extraitDeRole.getId());
                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
                                                GeneralConst.Level.LEVEL_ONE);

                                        converDateToString = Tools.formatDateToString(da.getDateCreat());

                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
                                                converDateToString);

                                        levelJsonDataList.add(levelJsonData);
                                    }
                                }

//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().isEmpty()) {
//
//                                    for (Contrainte contrainte
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_TWO);
//
//                                        converDateToString = Tools.formatDateToString(contrainte.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().isEmpty()) {
//
//                                    for (Commandement commandement
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_THREE);
//
//                                        converDateToString = Tools.formatDateToString(commandement.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getLettreSaisieList().isEmpty()) {
//
//                                    for (LettreSaisie lettreSaisie
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getLettreSaisieList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_FOUR);
//
//                                        converDateToString = Tools.formatDateToString(lettreSaisie.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
//                                if (!extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getAtdList().isEmpty()) {
//
//                                    for (Atd atd
//                                            : extraitDeRole.getDernierAvertissementList().get(0).getContrainteList().get(0).getCommandementList().get(0).getAtdList()) {
//
//                                        levelJsonData = new JsonObject();
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
//                                                extraitDeRole.getId());
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_VALUE,
//                                                GeneralConst.Level.LEVEL_FIVE);
//
//                                        converDateToString = Tools.formatDateToString(atd.getDateCreat());
//
//                                        levelJsonData.addProperty(RecouvrementConst.ParamName.LEVEL_DATE,
//                                                converDateToString);
//
//                                        levelJsonDataList.add(levelJsonData);
//                                    }
//                                }
                                break;
                        }

                        extraitRoleJson.addProperty(RecouvrementConst.ParamName.TRACKER_LINE_LIST,
                                levelJsonDataList.toString());

                    } else {
                        extraitRoleJson.addProperty(RecouvrementConst.ParamName.NIVEAU_EXTRAIT_ROLE,
                                GeneralConst.EMPTY_STRING);
                    }

                    //fin niveau
                    converDateToString = Tools.formatDateToString(extraitDeRole.getDateReception());
                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.DATE_RECEPTION_EXTRAIT_ROLE,
                            converDateToString);

                    converDateToString = GeneralConst.EMPTY_STRING;

                    if (extraitDeRole.getDateEcheance() != null) {
                        converDateToString = Tools.formatDateToString(extraitDeRole.getDateEcheance());

                        if (Compare.before(extraitDeRole.getDateEcheance(), new Date())) {
                            extraitRoleJson.addProperty(RecouvrementConst.ParamName.ECHEANCE_EXTRAIT_ROLE_DEPASSE,
                                    GeneralConst.Number.ONE);
                        } else {
                            extraitRoleJson.addProperty(RecouvrementConst.ParamName.ECHEANCE_EXTRAIT_ROLE_DEPASSE,
                                    GeneralConst.Number.ZERO);
                        }
                    } else {
                        extraitRoleJson.addProperty(RecouvrementConst.ParamName.ECHEANCE_EXTRAIT_ROLE_DEPASSE,
                                GeneralConst.Number.ZERO);
                    }

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_EXTRAIT_ROLE,
                            converDateToString);

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.STATE_EXTRAIT_ROLE, extraitDeRole.getEtat());

                    BigDecimal sumFraisDePoursuite = BigDecimal.ZERO;

                    sumFraisDePoursuite = extraitDeRole.getFraisPoursuiteCmd() != null ? sumFraisDePoursuite.add(extraitDeRole.getFraisPoursuiteCmd()) : BigDecimal.ZERO;
                    sumFraisDePoursuite = extraitDeRole.getFraisPoursuiteAtdPv() != null ? sumFraisDePoursuite.add(extraitDeRole.getFraisPoursuiteAtdPv()) : BigDecimal.ZERO;

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.FRAIS_POURSUITE, sumFraisDePoursuite);

                    NotePerception np;
                    BonAPayer bap;
                    BonAPayer bpDependacy;
                    NotePerception npDependacyBp;
                    ArticleBudgetaire ab;
                    JsonObject titreDependacyJson;
                    String typeDocumentText;
                    String deviseTitre;
                    boolean isBp = true;
                    boolean isPenality;
                    String echeance;
                    String codeBudgetaires;
                    int size;
                    List<JsonObject> titreDependacyJsonList;

                    Taux tauxchange = RecouvrementBusiness.getTauxByDevise(GeneralConst.Devise.DEVISE_USD);
                    BigDecimal taux = new BigDecimal(GeneralConst.Number.ZERO);

                    if (tauxchange != null) {
                        taux = tauxchange.getTaux();
                    }

                    deatailstRoleJsonList = new ArrayList<>();

                    List<NotePerception> listNpDependancy;

                    for (DetailsRole detailsRole : extraitDeRole.getDetailsRoleList()) {

                        detailRoleJson = new JsonObject();
                        Adresse adresse;
                        ab = new ArticleBudgetaire();

                        detailRoleJson.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID,
                                detailsRole.getFkExtraitRole().getId());

                        BigDecimal sumPenalite = new BigDecimal(BigInteger.ZERO);
                        BigDecimal sumPayeePenalite = new BigDecimal(BigInteger.ZERO);

                        switch (detailsRole.getFkTypeDocument().trim()) {

                            case DocumentConst.DocumentCode.NP:

                                np = NotePerceptionBusiness.getNotePerceptionByCode(
                                        detailsRole.getDocumentRef().trim());

                                titreDependacyJsonList = new ArrayList<>();

                                if (np != null) {

                                    isBp = false;
                                    deviseTitre = np.getDevise() != null ? np.getDevise() : GeneralConst.Devise.DEVISE_CDF;

//                                    if (np.getFkTypeTitrePerception().getCode() == GeneralConst.Numeric.THREE) {
//                                        typeDocumentText = GeneralConst.TypeNPFils.NP_P.toUpperCase();
//                                        isPenality = true;
//                                    } else {
//                                        typeDocumentText = GeneralConst.TypeNPFils.NP.toUpperCase();
//                                        isPenality = false;
//                                    }
                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TYPE,
                                            "NP");

                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.SERVICE,
                                            np.getNoteCalcul().getService().toUpperCase().trim());

                                    detailRoleJson.addProperty(GeneralConst.ParamName.CODE_SERVICE,
                                            np.getNoteCalcul().getService());

                                    detailRoleJson.addProperty(GeneralConst.ParamName.CODE_SITE,
                                            np.getNoteCalcul().getSite());

                                    detailRoleJson.addProperty(GeneralConst.ParamName.CODE_PROVINCE,
                                            "");

                                    detailRoleJson.addProperty(GeneralConst.ParamName.CODE_ENTITE,
                                            "");

                                    detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                            np.getNoteCalcul().getService());

                                    detailRoleJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE,
                                            np.getNumero().trim());

//                                    detailRoleJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE_MANUEL,
//                                            np.getNpManuel() == null ? GeneralConst.EMPTY_STRING : np.getNpManuel().trim());
                                    detailRoleJson.addProperty(RecouvrementConst.ParamName.TYPE_DOCUMENT,
                                            DocumentConst.DocumentCode.NP);

                                    detailRoleJson.addProperty(RecouvrementConst.ParamName.AssujettiCode,
                                            np.getNoteCalcul().getPersonne().trim());

                                    detailRoleJson.addProperty(RecouvrementConst.ParamName.AssujettiName,
                                            np.getNoteCalcul().getPersonne().toString().toUpperCase().trim());

                                    adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                            np.getNoteCalcul().getPersonne().trim());

                                    if (adresse != null) {
                                        detailRoleJson.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                                adresse.toString().toUpperCase().trim());
                                    } else {
                                        detailRoleJson.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                    detailRoleJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                            Tools.getExerciceFiscal(np.getNoteCalcul()));

                                    detailRoleJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                            ConvertDate.getValidFormatDateString(np.getDateCreat().trim()));

                                    echeance = ConvertDate.getValidFormatDateString(np.getDateEcheancePaiement().trim());

                                    detailRoleJson.addProperty(TaxationConst.ParamName.ECHEANCE, echeance);

                                    if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {

                                        BigDecimal amountConvert = Tools.convertAmountToCDF(np.getNetAPayer(), taux);
                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amountConvert);
                                        detailRoleJson.addProperty(TaxationConst.ParamName.DEVISE, GeneralConst.Devise.DEVISE_CDF);

                                    } else {

                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, np.getNetAPayer());
                                        detailRoleJson.addProperty(TaxationConst.ParamName.DEVISE, deviseTitre);
                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.DEVISE, deviseTitre);
                                    }

                                    Journal journal = PaiementBusiness.getJournalByDocument(np.getNumero().trim(), false);

                                    if (journal != null) {

                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, true);
                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal.getEtat() == GeneralConst.Numeric.ONE);
                                        detailRoleJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)
                                                ? Tools.convertAmountToCDF(journal.getMontant(), taux) : journal.getMontant());
                                        long nombreMois = Math.abs(ConvertDate.getMonthsBetween(Tools.formatStringFullToDateV2(echeance), journal.getDateCreat()));
                                        detailRoleJson.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD, nombreMois);

                                    } else {

                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, false);
                                        detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, false);
                                        detailRoleJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, GeneralConst.Numeric.ZERO);
                                        long nombreMois = Math.abs(ConvertDate.getMonthsBetween(Tools.formatStringFullToDateV2(echeance), new Date()));
                                        detailRoleJson.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD, nombreMois);
                                    }

                                    size = np.getNoteCalcul().getDetailsNcList().size();

//                                    if (isPenality) {
//
//                                        fkAbAmende = TaxationBusiness.getArticleBudgetaireByCode(
//                                                np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode().trim());
//
//                                        if (fkAbAmende != null) {
//
//                                            lblArticleBudgetaire = fkAbAmende.getCodeOfficiel().concat(
//                                                    GeneralConst.SEPARATOR_SLASH).concat(fkAbAmende.getIntitule());
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
//                                                    lblArticleBudgetaire);
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.BUDGET_ARTICLE_CODE,
//                                                    fkAbAmende != null ? fkAbAmende.getCode() : GeneralConst.EMPTY_STRING);
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
//                                                    fkAbAmende.getCodeOfficiel().toUpperCase());
//                                        } else {
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
//                                                    GeneralConst.EMPTY_STRING);
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.BUDGET_ARTICLE_CODE, GeneralConst.EMPTY_STRING);
//
//                                            detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
//                                                    GeneralConst.EMPTY_STRING);
//                                        }
//
//                                    } else {
                                    codeNc = GeneralConst.EMPTY_STRING;
                                    lblArticleBudgetaire = GeneralConst.EMPTY_STRING;

                                    //lblArticleBudgetaireDisplay = Tools.getArticleByNCV3(np.getNoteCalcul());
                                    //lblArticleBudgetaireDisplay = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getIntitule();
                                    detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                            np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getIntitule().trim());

                                    codeBudgetaires = Tools.getCodeBudgetaireByNC(np.getNoteCalcul());

                                    detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                            codeBudgetaires.toUpperCase());

                                    detailRoleJson.addProperty(TaxationConst.ParamName.BUDGET_ARTICLE_CODE,
                                            np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode().trim());
                                    //}

                                    listNpDependancy = RecouvrementBusiness.getListNotePerceptionDependancy(
                                            np.getNumero().trim());

                                    for (NotePerception npDependacy : listNpDependancy) {

                                        titreDependacyJson = new JsonObject();

                                        typeDocumentText = GeneralConst.TypeNPFils.NP_P.toUpperCase();

                                        titreDependacyJson.addProperty(NotePerceptionConst.ParamName.TYPE,
                                                typeDocumentText);

                                        titreDependacyJson.addProperty(NotePerceptionConst.ParamName.NP_MERE,
                                                np.getNumero().trim());

                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_SERVICE,
                                                npDependacy.getNoteCalcul().getService().trim());

                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_SITE,
                                                npDependacy.getNoteCalcul().getSite());

                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_PROVINCE,
                                                "");

                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_ENTITE,
                                                "");

                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE,
                                                npDependacy.getNumero().trim());

//                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE_MANUEL,
//                                                npDependacy.getNpManuel() == null ? GeneralConst.EMPTY_STRING
//                                                        : npDependacy.getNpManuel().trim());
                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.TYPE_DOCUMENT,
                                                DocumentConst.DocumentCode.NP);

                                        echeance = GeneralConst.EMPTY_STRING;

                                        if (npDependacy.getDateEcheancePaiement() != null
                                                && !npDependacy.getDateEcheancePaiement().isEmpty()) {

                                            echeance = ConvertDate.getValidFormatDateString(
                                                    npDependacy.getDateEcheancePaiement().trim());
                                        }

                                        titreDependacyJson.addProperty(TaxationConst.ParamName.ECHEANCE, echeance);

                                        titreDependacyJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                                ConvertDate.getValidFormatDateString(npDependacy.getDateCreat().trim()));

                                        if (npDependacy.getDevise() != null) {
                                            titreDependacyJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                    npDependacy.getDevise().toUpperCase());
                                        } else {
                                            titreDependacyJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                    GeneralConst.Devise.DEVISE_CDF);
                                        }

                                        if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {
                                            BigDecimal amountConvert = Tools.convertAmountToCDF(npDependacy.getNetAPayer(), taux);
                                            sumPenalite = sumPenalite.add(amountConvert);
                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amountConvert);
                                        } else {
                                            sumPenalite = sumPenalite.add(npDependacy.getNetAPayer());
                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, npDependacy.getNetAPayer());
                                        }

                                        journal = PaiementBusiness.getJournalByDocument(npDependacy.getNumero().trim(), false);

                                        if (journal != null) {

                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, true);
                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal.getEtat() == GeneralConst.Numeric.ONE);
                                            titreDependacyJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)
                                                    ? Tools.convertAmountToCDF(journal.getMontant(), taux) : journal.getMontant());

                                            sumPayeePenalite = sumPayeePenalite.add(deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)
                                                    ? Tools.convertAmountToCDF(journal.getMontant(), taux) : journal.getMontant());

                                        } else {

                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, false);
                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, false);
                                            titreDependacyJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, GeneralConst.Numeric.ZERO);

                                        }

                                        titreDependacyJsonList.add(titreDependacyJson);
                                    }

                                    //Récupération des bons à payer dépendantes à la note enrôlés
                                    titreDependacyJson = new JsonObject();

//                                    bap = RecouvrementBusiness.getBonAPayerByNpMere(np.getNumero().trim());
//
//                                    if (bap != null) {
//
//                                        typeDocumentText = GeneralConst.TypeNPFils.NP_BP.toUpperCase();
//
//                                        titreDependacyJson.addProperty(NotePerceptionConst.ParamName.TYPE,
//                                                typeDocumentText);
//
//                                        titreDependacyJson.addProperty(NotePerceptionConst.ParamName.NP_MERE,
//                                                np.getNumero().trim());
//
//                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_SERVICE,
//                                                bap.getFkNoteCalcul().getService().getCode().trim());
//
//                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_SITE,
//                                                bap.getFkNoteCalcul().getSite().getCode());
//
//                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_PROVINCE,
//                                                bap.getFkNoteCalcul().getSite().getFkEntite().getFkProvince().getId());
//
//                                        titreDependacyJson.addProperty(GeneralConst.ParamName.CODE_ENTITE,
//                                                bap.getFkNoteCalcul().getSite().getFkEntite().getId());
//
//                                        titreDependacyJson.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL,
//                                                np.getNpManuel() == null ? GeneralConst.EMPTY_STRING
//                                                        : np.getNpManuel().trim());
//
//                                        titreDependacyJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
//                                                bap.getFkNoteCalcul().getService().getIntitule().toUpperCase().trim());
//
//                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE,
//                                                bap.getCode().trim());
//
//                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE_MANUEL,
//                                                bap.getBpManuel() == null ? GeneralConst.EMPTY_STRING : bap.getBpManuel().trim());
//
//                                        titreDependacyJson.addProperty(RecouvrementConst.ParamName.TYPE_DOCUMENT,
//                                                DocumentConst.DocumentCode.BP);
//
//                                        echeance = GeneralConst.EMPTY_STRING;
//
//                                        if (bap.getDateEcheance() != null) {
//
//                                            echeance = ConvertDate.formatDateToString(bap.getDateEcheance());
//                                        }
//
//                                        titreDependacyJson.addProperty(TaxationConst.ParamName.ECHEANCE, echeance);
//
//                                        titreDependacyJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
//                                                ConvertDate.formatDateToString(bap.getDateCreat()));
//
//                                        if (bap.getDevise() != null && !bap.getDevise().isEmpty()) {
//                                            titreDependacyJson.addProperty(TaxationConst.ParamName.DEVISE,
//                                                    bap.getDevise().toUpperCase());
//                                        } else {
//                                            titreDependacyJson.addProperty(TaxationConst.ParamName.DEVISE,
//                                                    GeneralConst.Devise.DEVISE_CDF);
//                                        }
//
//                                        if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {
//                                            BigDecimal amountConvert = Tools.convertAmountToCDF(bap.getMontant(), taux);
//                                            sumPenalite = sumPenalite.add(amountConvert);
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amountConvert);
//
//                                        } else {
//                                            sumPenalite = sumPenalite.add(bap.getMontant());
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, bap.getMontant());
//                                        }
//
//                                        journal = PaiementBusiness.getJournalByDocument(bap.getBpManuel().trim(), false);
//
//                                        if (journal != null) {
//
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, true);
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal.getEtat() == GeneralConst.Numeric.ONE);
//                                            titreDependacyJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)
//                                                    ? Tools.convertAmountToCDF(journal.getMontant(), taux) : journal.getMontant());
//
//                                            sumPayeePenalite = sumPayeePenalite.add(deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)
//                                                    ? Tools.convertAmountToCDF(journal.getMontant(), taux) : journal.getMontant());
//
//                                        } else {
//
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, false);
//                                            titreDependacyJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, false);
//                                            titreDependacyJson.addProperty(TaxationConst.ParamName.MONTANT_PAYE, GeneralConst.Numeric.ZERO);
//
//                                        }
//
//                                        titreDependacyJsonList.add(titreDependacyJson);
//                                    }
                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TITRE_PERCEPTION_DEPENDACY,
                                            titreDependacyJsonList.toString());

                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TOTAL_PENALITE, sumPenalite);
                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TOTAL_PAYEE_PENALITE, sumPayeePenalite);

                                } else {

                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TITRE_PERCEPTION_DEPENDACY,
                                            GeneralConst.EMPTY_STRING);
                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TOTAL_PENALITE, GeneralConst.Numeric.ZERO);
                                    detailRoleJson.addProperty(NotePerceptionConst.ParamName.TOTAL_PAYEE_PENALITE, GeneralConst.Numeric.ZERO);
                                }

                                break;

                        }

                        deatailstRoleJsonList.add(detailRoleJson);
                    }

                    extraitRoleJson.addProperty(RecouvrementConst.ParamName.DETAIL_ROLE_LIST,
                            deatailstRoleJsonList.toString());

                    extraitRoleJsonList.add(extraitRoleJson);

                }

                dataReturn = extraitRoleJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

//    public String saveDernierAvertissement(HttpServletRequest request) {
//
//        String dataReturn;
//        boolean result;
//
//        try {
//
//            String fkPersonne = request.getParameter(RecouvrementConst.ParamName.CODE_ASSUJETTI);
//            String fkExtraitRole = request.getParameter(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID);
//
//            List<DetailsRole> listDetailsRole = new ArrayList<>();
//
//            listDetailsRole = RecouvrementBusiness.getListDetailsRoleByExtrait(fkExtraitRole.trim());
//
//            result = RecouvrementBusiness.saveDernierAvertissement(
//                    request, fkExtraitRole.trim(), fkPersonne.trim(), listDetailsRole);
//
//            if (result) {
//
//                DernierAvertissement da = RecouvrementBusiness.getDernierAvertissementByExtraitRoleAndAssujetti(
//                        fkExtraitRole.trim(), fkPersonne.trim());
//
//                if (da != null) {
//                    PrintDocument printDocument = new PrintDocument();
//                    dataReturn = printDocument.createDernierAvertissement(da);
//                } else {
//                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//                }
//            } else {
//                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//            }
//
//        } catch (Exception e) {
//            Integer value = Tools.getCodeAgentFromSession(request);
//            CustumException.LogException(e, RecouvrementConst.Operation.SAVE_DERNIER_AVERTISSEMENT, value);
//            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//
//        return dataReturn;
//    }
//    public String saveContrainte(HttpServletRequest request) {
//
//        String dataReturn;
//        String idContraintReturn;
//
//        try {
//
//            String idUser = request.getParameter(GeneralConst.ID_USER);
//            String fkPersonne = request.getParameter(RecouvrementConst.ParamName.CODE_ASSUJETTI);
//            String fkDernierAvertissementId = request.getParameter(RecouvrementConst.ParamName.DERNIER_AVERTISSEMENT_ID);
//            String operation = request.getParameter(GeneralConst.OPERATION);
//            String idHuissier = request.getParameter(RecouvrementConst.ParamName.HUISSIER_ID);
//
//            logUser = new LogUser(request, operation);
//
//            idContraintReturn = RecouvrementBusiness.saveContrainte(idUser, fkPersonne,
//                    fkDernierAvertissementId, logUser, idHuissier);
//
//            if (!idContraintReturn.isEmpty()) {
//
//                PrintDocument printDocument = new PrintDocument();
//                Contrainte contrainte = RecouvrementBusiness.getContrainteByID(idContraintReturn.trim());
//
//                if (contrainte != null) {
//                    dataReturn = printDocument.createContrainte(contrainte);
//                } else {
//                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//                }
//
//            } else {
//                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//            }
//
//        } catch (NumberFormatException | InvocationTargetException e) {
//            Integer value = Tools.getCodeAgentFromSession(request);
//            CustumException.LogException(e, RecouvrementConst.Operation.SAVE_CONTRAINTE, value);
//            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//
//        return dataReturn;
//    }
    public String loadListProjetRole(HttpServletRequest request) {

        String dataReturn;

        String codeSite = "";// request.getParameter(TaxationConst.ParamName.CODE_SITE);
        String codeService = ""; // request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
        String articleRole = request.getParameter(RecouvrementConst.ParamName.ARTICLE_ROLE);
        String dateDebut = request.getParameter(GeneralConst.ParamName.DATE_DEBUT);
        String dateFin = request.getParameter(GeneralConst.ParamName.DATE_FIN);
        String typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
        String codeEntite = ""; // request.getParameter(GeneralConst.ParamName.CODE_ENTITE);
        String codeAgent = request.getParameter(GeneralConst.ParamName.CODE_AGENT);

        try {

            listRoles = new ArrayList<>();

            listRoles = RecouvrementBusiness.loadProjetRoles(
                    Integer.valueOf(typeSearch),
                    articleRole,
                    codeSite,
                    codeService,
                    dateDebut, dateFin,
                    codeEntite,
                    codeAgent);

            if (!listRoles.isEmpty()) {

                String convertDateToString;

                roleJsonList = new ArrayList<>();
                ArticleBudgetaire fkAbAmende;
                lblArticleBudgetaire = GeneralConst.EMPTY_STRING;
                lblArticleBudgetaireDisplay = GeneralConst.EMPTY_STRING;

                for (Role role : listRoles) {

                    roleJson = new JsonObject();

                    BigDecimal sumPayeDetailRole = new BigDecimal(BigInteger.ZERO);

                    roleJson.addProperty(RecouvrementConst.ParamName.ROLE_ID, role.getId().trim());
                    roleJson.addProperty(RecouvrementConst.ParamName.ARTICLE_ROLE, role.getArticleRole().trim());
                    roleJson.addProperty(RecouvrementConst.ParamName.STATE_ROLE, role.getEtat());

                    if (role.getObservationReceveur() != null) {
                        roleJson.addProperty(RecouvrementConst.ParamName.OBSERVATION_RECEVEUR,
                                role.getObservationReceveur());
                    } else {
                        roleJson.addProperty(RecouvrementConst.ParamName.OBSERVATION_RECEVEUR,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (role.getObservationDirection() != null) {
                        roleJson.addProperty(RecouvrementConst.ParamName.OBSERVATION_DIRECTION,
                                role.getObservationDirection());
                    } else {
                        roleJson.addProperty(RecouvrementConst.ParamName.OBSERVATION_DIRECTION,
                                GeneralConst.EMPTY_STRING);
                    }

                    roleJson.addProperty(RecouvrementConst.ParamName.FRAIS_POURSUITE_GLOBAL,
                            role.getFraisPoursuiteGlobal());

                    roleJson.addProperty(RecouvrementConst.ParamName.OBSERVATION_LIST,
                            Tools.getListObservations(role.getId().trim(),
                                    GeneralConst.Module.MOD_RECOUVREMENT));

                    convertDateToString = Tools.formatDateToString(role.getDateCreat());
                    roleJson.addProperty(RecouvrementConst.ParamName.DATE_CREATE_ROLE, convertDateToString);

                    BigDecimal tauxChange = new BigDecimal(GeneralConst.Number.ZERO);

                    Taux taux = RecouvrementBusiness.getTauxByDevise(GeneralConst.Devise.DEVISE_USD);

                    if (taux != null) {
                        tauxChange = taux.getTaux();
                    }

                    String deviseTitrePerception = GeneralConst.EMPTY_STRING;

                    if (!role.getDetailsRoleList().isEmpty()) {

                        detailRoleJsonList = new ArrayList();

                        for (DetailsRole detailsRole : role.getDetailsRoleList()) {

                            detailRoleJson = new JsonObject();
                            NotePerception np = new NotePerception();
                            BonAPayer bonAPayer = new BonAPayer();
                            String echeance;
                            boolean isBp = true;

                            detailRoleJson.addProperty(RecouvrementConst.ParamName.ROLE_ID,
                                    detailsRole.getFkRole().getId().trim());

                            switch (detailsRole.getFkTypeDocument().trim().toUpperCase()) {

                                case DocumentConst.DocumentCode.NP:

                                    np = NotePerceptionBusiness.getNotePerceptionByCode(
                                            detailsRole.getDocumentRef().trim());

                                    if (np != null) {

                                        Date dateCreate = Tools.formatStringFullToDate(np.getDateCreat());

                                        isBp = false;

                                        if (np.getDevise() != null) {
                                            deviseTitrePerception = np.getDevise().toUpperCase();
                                        } else {
                                            deviseTitrePerception = Tools.getDeviseByNC(np.getNoteCalcul());
                                        }

                                        detailRoleJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                                Tools.formatDateToString(dateCreate));
                                    }

                                    break;
//                                case DocumentConst.DocumentCode.BP:
//
//                                    bonAPayer = PaiementBusiness.getBonAPayerByCode(
//                                            detailsRole.getDocumentRef().trim());
//                                    if (bonAPayer != null) {
//                                        isBp = true;
//                                        detailRoleJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
//                                                Tools.formatDateToString(bonAPayer.getDateCreat()));
//                                        deviseTitrePerception = bonAPayer.getDevise() == null ? GeneralConst.EMPTY_STRING : bonAPayer.getDevise();
//                                    }
//                                    break;
                            }

//                            if (isBp) {
//                            } else {
//                                detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
//                                        np.getNoteCalcul().getService().getIntitule().toUpperCase().trim());
                            detailRoleJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE,
                                    np.getNumero().trim());
//                                detailRoleJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_REFERENCE_MANUEL,
//                                        np.getNpManuel() == null ? GeneralConst.EMPTY_STRING : np.getNpManuel().trim());

                            detailRoleJson.addProperty(RecouvrementConst.ParamName.TYPE_DOCUMENT,
                                    DocumentConst.DocumentCode.NP);

                            detailRoleJson.addProperty(RecouvrementConst.ParamName.AssujettiCode,
                                    np.getNoteCalcul().getPersonne().trim());
//
                            detailRoleJson.addProperty(RecouvrementConst.ParamName.AssujettiName,
                                    np.getNoteCalcul().getPersonne().toString().toUpperCase().trim());
                            detailRoleJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                    Tools.getExerciceFiscal(np.getNoteCalcul()));

                            echeance = ConvertDate.getValidFormatDateString(np.getDateEcheancePaiement().trim());

                            detailRoleJson.addProperty(TaxationConst.ParamName.ECHEANCE, echeance);

                            if (deviseTitrePerception.equals(
                                    GeneralConst.Devise.DEVISE_USD)) {

                                BigDecimal amountConvert = Tools.convertAmountToCDF(np.getNetAPayer(), tauxChange);

                                detailRoleJson.addProperty(TaxationConst.ParamName.NET_A_PAYE, amountConvert);
                                detailRoleJson.addProperty(TaxationConst.ParamName.DEVISE, GeneralConst.Devise.DEVISE_CDF);

                            } else {

                                detailRoleJson.addProperty(TaxationConst.ParamName.NET_A_PAYE, np.getNetAPayer());
                                detailRoleJson.addProperty(TaxationConst.ParamName.DEVISE, deviseTitrePerception);

                            }

                            Journal journal = PaiementBusiness.getJournalByDocument(np.getNumero(), false);

                            if (journal != null) {

                                detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, true);
                                detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal.getEtat() == GeneralConst.Numeric.ONE);
                                long NombreMois;

                                NombreMois = Math.abs(ConvertDate.getMonthsBetween(
                                        Tools.formatStringFullToDate(np.getDateEcheancePaiement()),
                                        journal.getDateCreat()));
                                detailRoleJson.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD, NombreMois);

                                sumPayeDetailRole = sumPayeDetailRole.add(np.getDevise().equals(GeneralConst.Devise.DEVISE_USD)
                                        ? Tools.convertAmountToCDF(journal.getMontant(), tauxChange) : journal.getMontant());

                            } else {

                                detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_PAID, false);
                                detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_APURED, false);
                                long NombreMois;
                                NombreMois = Math.abs(ConvertDate.getMonthsBetween(
                                        Tools.formatStringFullToDate(np.getDateEcheancePaiement()),
                                        new Date()));
                                detailRoleJson.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD, NombreMois);
                            }

                            String codeBudgetaire;

                            int resultRecidiviste = GeneralConst.Numeric.ZERO;

//                                if (np.getFkTypeTitrePerception().getCode() == GeneralConst.Numeric.THREE) {
//
                            fkAbAmende = TaxationBusiness.getArticleBudgetaireByCode(
                                    np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode());
                            if (fkAbAmende != null) {
                                lblArticleBudgetaire = fkAbAmende.getCodeOfficiel().concat(
                                        GeneralConst.SEPARATOR_SLASH).concat(fkAbAmende.getIntitule());
                                detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        lblArticleBudgetaire);

                                if (fkAbAmende.getCodeOfficiel() != null && !fkAbAmende.getCodeOfficiel().isEmpty()) {
                                    detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                            fkAbAmende.getCodeOfficiel().toUpperCase());
                                } else {
                                    detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                            GeneralConst.EMPTY_STRING);
                                }
                            } else {

                                detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);

                                detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                        GeneralConst.EMPTY_STRING);
                            }
//                                } else {
//
//                                    codeNc = GeneralConst.EMPTY_STRING;
//
//                                    lblArticleBudgetaireDisplay = Tools.getArticleByNCV3(np.getNoteCalcul());
//
//                                    codeBudgetaire = Tools.getCodeBudgetaireByNC(np.getNoteCalcul());
//
//                                    detailRoleJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
//                                            lblArticleBudgetaireDisplay.trim());
//
//                                    detailRoleJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
//                                            codeBudgetaire.toUpperCase());
//
//                                }
                            //a modifier par rapport au paiment
//                                if (!np.getNoteCalcul().getDetailsNcList().isEmpty()) {
//
//                                    Integer size = np.getNoteCalcul().getDetailsNcList().size();
//
//                                    for (int i = GeneralConst.Numeric.ZERO; i < size; i++) {
//
//                                        ArticleBudgetaire articleBudgetaire = np.getNoteCalcul().getDetailsNcList().get(i).getArticleBudgetaire();
//
//                                        if (resultRecidiviste == GeneralConst.Numeric.ZERO) {
//                                            resultRecidiviste = PoursuiteBusiness.checkRecidiviste(
//                                                    articleBudgetaire.getCode(),
//                                                    np.getNoteCalcul().getPersonne().getCode());
//                                        }
//                                    }
//                                }
//                                if (detailsRole.getOrdonnance() == 1) {
//
//                                    List<NotePerception> listNpDependancy = RecouvrementBusiness.getListNotePerceptionDependancy(
//                                            np.getNumero().trim());
//
//                                    if (!listNpDependancy.isEmpty()) {
//
//                                        NotePerception npPenalite = listNpDependancy.get(GeneralConst.Numeric.ZERO);
//
//                                        journal = PaiementBusiness.getJournalByDocument(npPenalite.getNpManuel().trim(), false);
//
//                                        if (journal != null) {
//
//                                            sumPayeDetailRole = sumPayeDetailRole.add(npPenalite.getDevise().getCode().equals(GeneralConst.Devise.DEVISE_USD)
//                                                    ? Tools.convertAmountToCDF(journal.getMontant(), tauxChange) : journal.getMontant());
//                                        }
//
//                                    }
//
//                                    BonAPayer bap = RecouvrementBusiness.getBonAPayerByNpMere(np.getNumero().trim());
//
//                                    if (bap != null) {
//
//                                        journal = PaiementBusiness.getJournalByDocument(bap.getBpManuel(), false);
//
//                                        if (journal != null) {
//
//                                            sumPayeDetailRole = sumPayeDetailRole.add(bap.getDevise().equals(GeneralConst.Devise.DEVISE_USD)
//                                                    ? Tools.convertAmountToCDF(journal.getMontant(), tauxChange) : journal.getMontant());
//                                        }
//                                    }
//
//                                }
//                                detailRoleJson.addProperty(NotePerceptionConst.ParamName.IS_RECIDIVISTE,
//                                        resultRecidiviste);
                            //}
                            detailRoleJsonList.add(detailRoleJson);
                        }

                        roleJson.addProperty(RecouvrementConst.ParamName.DETAIL_ROLE_LIST, detailRoleJsonList.toString());
                        roleJson.addProperty(TaxationConst.ParamName.DEVISE, GeneralConst.Devise.DEVISE_CDF);
                        roleJson.addProperty(TaxationConst.ParamName.MONTANT_TOTAL_PAYER, sumPayeDetailRole);
                    }

                    ExtraitDeRole extraitDeRole = RecouvrementBusiness.getExtraitRoleByRole(
                            role.getId().trim());

                    if (extraitDeRole != null) {

                        switch (extraitDeRole.getEtat()) {
                            case GeneralConst.Numeric.ONE:
                            case GeneralConst.Numeric.THREE:

                                roleJson.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ORDONNANCE,
                                        GeneralConst.Number.ONE);

                                break;
                            case GeneralConst.Numeric.TWO:
                                roleJson.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ORDONNANCE,
                                        GeneralConst.Number.ZERO);
                                break;
                        }
                    } else {
                        roleJson.addProperty(RecouvrementConst.ParamName.EXTRAIT_ROLE_ORDONNANCE,
                                GeneralConst.Number.ZERO);
                    }

                    roleJsonList.add(roleJson);
                }

                dataReturn = roleJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (NumberFormatException | JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private void saveLogUserInfo(HttpServletRequest request) {
        logUser = new LogUser(request, valueOperation);
    }

    public String getPenalitiesList() {

        try {

            List<JsonObject> jsonPenalitiesList = new ArrayList<>();
            List<Penalite> penalites = GeneralBusiness.getPenalite();

            for (Penalite penalite : penalites) {

                JsonObject jsonObjectPenalite = new JsonObject();

                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_CODE,
                        penalite.getCode() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getCode());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_INTITULE,
                        penalite.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getIntitule());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_TYPE_PENALITE,
                        penalite.getTypePenalite() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getTypePenalite().getIntitule());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_APPLIQUER_MOIS_RETARD, penalite.getAppliquerMoisRetard());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_PALIER, penalite.getPalier());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_IS_VISIBLE_UTILISATEUR, penalite.getVisibilteUtilisateur());

                jsonPenalitiesList.add(jsonObjectPenalite);

                List<JsonObject> jsonTauxPenaliteList = new ArrayList<>();

                for (PalierTauxPenalite palierTauxPenalite : penalite.getPalierTauxPenaliteList()) {

                    if (palierTauxPenalite.getEtat()) {

                        JsonObject jsonObjecttauxPenalite = new JsonObject();

                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_ID, palierTauxPenalite.getId());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_PENALITE, palierTauxPenalite.getPenalite().getCode());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_BORNE_INFERIEURE, palierTauxPenalite.getBorneInferieure());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_BORNE_SUPERIEURE, palierTauxPenalite.getBorneSuperieure());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_FORME_JURIDIQUE, palierTauxPenalite.getFormeJuridique().getCode());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_RECIDIVE, palierTauxPenalite.getRecidive());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_VALEUR, palierTauxPenalite.getValeur());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_TYPE_VALEUR, palierTauxPenalite.getTypeValeur());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_EST_POURCENTAGE, palierTauxPenalite.getEstPourcentage());

                        jsonTauxPenaliteList.add(jsonObjecttauxPenalite);
                    }

                }

                jsonObjectPenalite.addProperty(GeneralConst.ParamName.LIST_TAUX_PANALIE, jsonTauxPenaliteList.toString());
            }

            return jsonPenalitiesList.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String saveContrainte(HttpServletRequest request) {

        String dataReturn;
        String idContraintReturn;

        try {

            String idUser = request.getParameter(GeneralConst.ID_USER);
            String fkPersonne = request.getParameter(RecouvrementConst.ParamName.CODE_ASSUJETTI);
            String fkDernierAvertissementId = request.getParameter(RecouvrementConst.ParamName.DERNIER_AVERTISSEMENT_ID);
            String operation = request.getParameter(GeneralConst.OPERATION);
            String idHuissier = request.getParameter(RecouvrementConst.ParamName.HUISSIER_ID);
            String medId = request.getParameter("medId");

            logUser = new LogUser(request, operation);

            idContraintReturn = RecouvrementBusiness.saveContrainte(idUser, fkPersonne,
                    fkDernierAvertissementId, logUser, idHuissier);

            if (!idContraintReturn.isEmpty()) {

                PrintDocument printDocument = new PrintDocument();
                Contrainte contrainte = RecouvrementBusiness.getContrainteByID(idContraintReturn.trim());
                Med med = RecouvrementBusiness.getMedById(medId);

                if (contrainte != null) {
                    dataReturn = printDocument.createContrainteV2(contrainte, idUser, "", med);
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException e) {
            Integer value = Tools.getCodeAgentFromSession(request);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveCommandement(HttpServletRequest request) {

        String dataReturn;
        String idCommandementReturn;

        try {

            Double fraisPoursuite = null;

            String idUser = request.getParameter(GeneralConst.ID_USER);
            String fkPersonne = request.getParameter(RecouvrementConst.ParamName.CODE_ASSUJETTI);
            String fkContrainteId = request.getParameter(RecouvrementConst.ParamName.CONTRAINTE_ID);
            String operation = request.getParameter(GeneralConst.OPERATION);
            String montantDu = request.getParameter(NotePerceptionConst.ParamName.MONTANT_DU);
            String nomPersonneTiers = request.getParameter(RecouvrementConst.ParamName.NOM_PERSONNE_TIERS);
            String fonctionPersonneTiers = request.getParameter(RecouvrementConst.ParamName.FONCTION_PERSONNE_TIERS);
            String parquetName = "";

            if (montantDu != null && !montantDu.isEmpty()) {
                Double frCmd = Double.valueOf(propertiesConfig.getProperty(
                        RecouvrementConst.ParamName.FRAIS_POURSUITE_COMMANDEMENT));

                fraisPoursuite = (Double.valueOf(montantDu) * frCmd);
            }

            logUser = new LogUser(request, operation);

            idCommandementReturn = RecouvrementBusiness.saveCommandement(
                    idUser.trim(), fkPersonne.trim(),
                    fraisPoursuite, fkContrainteId.trim(), logUser);

            if (idCommandementReturn != null && !idCommandementReturn.isEmpty()) {
                PrintDocument printDocument = new PrintDocument();
                Commandement commandement = RecouvrementBusiness.getCommandementById(
                        idCommandementReturn.trim());

                if (commandement != null) {
                    dataReturn = printDocument.createCommandementV2(commandement,
                            idUser, parquetName, 1);
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException e) {
            Integer value = Tools.getCodeAgentFromSession(request);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveDernierAvertissement(HttpServletRequest request) {

        String dataReturn;
        boolean result;

        try {

            String fkPersonne = request.getParameter(RecouvrementConst.ParamName.CODE_ASSUJETTI);
            String fkExtraitRole = request.getParameter(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID);

            List<DetailsRole> listDetailsRole = new ArrayList<>();

            listDetailsRole = RecouvrementBusiness.getListDetailsRoleByExtrait(fkExtraitRole.trim());

            result = RecouvrementBusiness.saveDernierAvertissement(
                    request, fkExtraitRole.trim(), fkPersonne.trim(), listDetailsRole);

            if (result) {

                DernierAvertissement da = RecouvrementBusiness.getDernierAvertissementByExtraitRoleAndAssujetti(
                        fkExtraitRole.trim(), fkPersonne.trim());

                if (da != null) {
                    PrintDocument printDocument = new PrintDocument();
                    dataReturn = printDocument.createDernierAvertissement(da);
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            Integer value = Tools.getCodeAgentFromSession(request);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String ordonnancerExtraitRole2(HttpServletRequest request) {

        boolean result;
        bulkQuery = new HashMap<>();
        int counter = 0;
        int nbre = 0;

        String dataReturn = GeneralConst.EMPTY_STRING;

        String extraitRoleId = request.getParameter(RecouvrementConst.ParamName.EXTRAIT_ROLE_ID);
        String userId = request.getParameter("userId");
        String siteCode = request.getParameter(GeneralConst.ParamName.SITE_CODE);
        String detailRoleList = request.getParameter(RecouvrementConst.ParamName.DETAIL_ROLE_LIST);
        String receveurName = GeneralConst.EMPTY_STRING;
        String idDispatch = request.getParameter(TaxationConst.ParamName.ID_DISPATCH_SERIE);

        BigDecimal tauxChange = new BigDecimal("0");
        Taux taux = RecouvrementBusiness.getTauxByDevise(GeneralConst.Devise.DEVISE_USD);
        if (taux != null) {
            tauxChange = taux.getTaux();
        }

        try {

            if (!detailRoleList.isEmpty()) {

                saveLogUserInfo(request);

                Agent agent = PaiementBusiness.getAgentByCode(Integer.valueOf(userId.trim()));
                if (agent != null) {
                    receveurName = agent.toString().toUpperCase().trim();
                }

                JSONArray jsonArray = new JSONArray(detailRoleList);
                listDetailRolePrint = new ArrayList<>();

                for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {
                    nbre++;

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    BigDecimal amount;
                    BigDecimal amountNp = new BigDecimal(GeneralConst.Number.ZERO);
                    BigDecimal amountBap = new BigDecimal(GeneralConst.Number.ZERO);
                    BigDecimal divisor = new BigDecimal(GeneralConst.Number.TWO);

                    amount = BigDecimal.valueOf(Double.valueOf(
                            jsonObject.getString(RecouvrementConst.ParamName.AMOUNT_PENALITE)));

                    amountNp = amountNp.add(amount.divide(divisor));
                    amountBap = amountBap.add(amountNp);

                    String nc = GeneralConst.EMPTY_STRING;
                    String amende = GeneralConst.EMPTY_STRING;
                    String devise = GeneralConst.EMPTY_STRING;
                    String titrePerception = GeneralConst.EMPTY_STRING;
                    ArticleBudgetaire abAmende;

                    NotePerception np;
                    BonAPayer bap;

                    DetailRolePrint detailRolePrint = new DetailRolePrint();

                    detailRolePrint.setDocumentReference(
                            jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE));

                    detailRolePrint.setTypeDocument(
                            jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT));

                    detailRolePrint.setAmountPenalite(
                            jsonObject.getString(RecouvrementConst.ParamName.AMOUNT_PENALITE));

                    detailRolePrint.setAmountPrincipal(
                            jsonObject.getString(TaxationConst.ParamName.NET_A_PAYE));

                    detailRolePrint.setBudgetArticleCode(
                            jsonObject.getString(TaxationConst.ParamName.BUDGET_ARTICLE_CODE));

                    detailRolePrint.setAssujettiCode(
                            jsonObject.getString(RecouvrementConst.ParamName.AssujettiCode));

                    detailRolePrint.setBudgetArticleLibelle(
                            jsonObject.getString(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE));

                    detailRolePrint.setDevise(
                            jsonObject.getString(TaxationConst.ParamName.DEVISE));

                    detailRolePrint.setService(
                            jsonObject.getString(TaxationConst.ParamName.LIBELLE_SERVICE));

                    detailRolePrint.setDateCreate(
                            jsonObject.getString(TaxationConst.ParamName.DATE_CREATE));

//                    detailRolePrint.setNpPenaliteManuel(
//                            jsonObject.getString(TaxationConst.ParamName.NP_PENALITE_MANUEL));
//
//                    String npPenaliteManuel = jsonObject.getString(TaxationConst.ParamName.NP_PENALITE_MANUEL);
                    listDetailRolePrint.add(detailRolePrint);

                    switch (jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT)) {

                        case DocumentConst.DocumentCode.NP:

                            np = NotePerceptionBusiness.getNotePerceptionByCode(jsonObject.getString(
                                    RecouvrementConst.ParamName.DOCUMENT_REFERENCE));

                            if (np != null) {

                                nc = np.getNoteCalcul().getNumero().trim();
                                titrePerception = np.getNumero().trim();
                                devise = np.getDevise() != null ? np.getDevise().trim() : GeneralConst.Devise.DEVISE_CDF;

                                abAmende = Tools.getAmendeBySecteur(np.getNoteCalcul().getService().trim(),
                                        GeneralConst.NatureArticleBudgetaire.AMENDE);

                                if (devise.equals(GeneralConst.Devise.DEVISE_USD)) {

                                    amountNp = amountNp.divide(tauxChange);
                                    amountBap = amountBap.divide(tauxChange);

                                }

                                counter++;
                                bulkQuery.put(counter + SQLQueryNotePerception.F_NEW_NOTE_PERCEPTION_PENALITE,
                                        new Object[]{
                                            nc,
                                            amountNp,
                                            0,
                                            amountNp,
                                            np.getNoteCalcul().getExercice(),
                                            siteCode,
                                            null,
                                            Integer.valueOf(userId.trim()),
                                            titrePerception,
                                            devise,
                                            logUser.getMacAddress(),
                                            logUser.getIpAddress(),
                                            logUser.getHostName(),
                                            Integer.valueOf("1409"),
                                            logUser.getBrowserContext(),
                                            abAmende != null ? abAmende.getCode() : GeneralConst.EMPTY_STRING,
                                            ""
                                        });

//                                counter++;
//                                bulkQuery.put(counter + SQLQueryNotePerception.F_NEW_BON_A_PAYER_PENALITEV3,
//                                        new Object[]{
//                                            amountBap, null, Integer.valueOf(userId.trim()),
//                                            propertiesMessage.getProperty("TXT_50_POURCENT_INTERETXT_INTERET_MORATOIRE"),
//                                            np.getNoteCalcul().getPersonne(),
//                                            nc,
//                                            np.getCompteBancaire().trim(),
//                                            amende,
//                                            devise,
//                                            titrePerception});
                            }
                            break;
//                        case DocumentConst.DocumentCode.BP:
//
//                            bap = PaiementBusiness.getBonAPayerByCode(jsonObject.getString(
//                                    RecouvrementConst.ParamName.DOCUMENT_REFERENCE));
//
//                            if (bap != null) {
//
//                                nc = bap.getFkNoteCalcul().getNumero().trim();
//                                titrePerception = bap.getCode().trim();
//                                devise = bap.getDevise() != null ? bap.getDevise().trim() : GeneralConst.Devise.DEVISE_CDF;
//
//                                abAmende = Tools.getAmendeBySecteur(bap.getFkNoteCalcul().getService().getCode().trim(),
//                                        GeneralConst.NatureArticleBudgetaire.AMENDE);
//
//                                if (devise.equals(GeneralConst.Devise.DEVISE_USD)) {
//
//                                    amountNp = amountNp.divide(tauxChange);
//                                    amountBap = amountBap.divide(tauxChange);
//
//                                }
//
//                                counter++;
//                                bulkQuery.put(counter + SQLQueryNotePerception.F_NEW_NOTE_PERCEPTION_PENALITEV3,
//                                        new Object[]{
//                                            nc,
//                                            amountNp,
//                                            0,
//                                            amountNp,
//                                            bap.getFkNoteCalcul().getExercice().getCode(),
//                                            siteCode,
//                                            null,
//                                            Integer.valueOf(userId.trim()),
//                                            null,
//                                            devise,
//                                            logUser.getMacAddress(),
//                                            logUser.getIpAddress(),
//                                            logUser.getHostName(),
//                                            Integer.valueOf("1409"),
//                                            logUser.getBrowserContext(),
//                                            abAmende != null ? abAmende.getCode() : GeneralConst.EMPTY_STRING,
//                                            titrePerception.trim(),
//                                            npPenaliteManuel.trim()
//                                        });
//
//                                counter++;
//                                bulkQuery.put(counter + SQLQueryNotePerception.F_NEW_BON_A_PAYER_PENALITEV2,
//                                        new Object[]{
//                                            amountBap, null, Integer.valueOf(userId.trim()),
//                                            propertiesMessage.getProperty("TXT_50_POURCENT_INTERETXT_INTERET_MORATOIRE"),
//                                            bap.getFkNoteCalcul().getPersonne().getCode().trim(),
//                                            nc,
//                                            bap.getFkCompte().getCode().trim(),
//                                            amende,
//                                            devise,
//                                            titrePerception});
//
//                            }
//                            break;

                    }

//                    counter++;
//                    bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                        nc,
//                        jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE),
//                        propertiesConfig.getProperty("TXT_NOTE_PENALITE_RECOUVREMENT"),
//                        amountNp,
//                        GeneralConst.Numeric.ZERO,
//                        propertiesMessage.getProperty("TXT_50_POURCENT_INTERETXT_INTERET_MORATOIRE").concat(titrePerception),
//                        devise
//                    });
                    counter++;
                    bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                        nc,
                        jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE),
                        propertiesConfig.getProperty("TXT_NOTE_PENALITE_RECOUVREMENT"),
                        0,
                        GeneralConst.Numeric.ZERO,
                        propertiesMessage.getProperty("TXT_50_POURCENT_INTERETXT_INTERET_MORATOIRE").concat(titrePerception),
                        devise
                    });

                    counter++;
                    bulkQuery.put(counter + SQLQueryNotePerception.F_UPDATE_EXTRAIT_ROLE,
                            new Object[]{
                                extraitRoleId});

                    counter++;
                    bulkQuery.put(counter + SQLQueryNotePerception.F_UPDATE_DETAIL_ROLE,
                            new Object[]{
                                extraitRoleId});
                }

                counter++;
                String queryUpdate = SQLQueryTaxation.ExecuteQuery.F_UPDATE_QTE_UTILISEE_DISPATCH;
                queryUpdate = String.format(queryUpdate, nbre++);

                bulkQuery.put(counter + queryUpdate, new Object[]{
                    idDispatch
                });

                result = executeQueryBulkInsert(bulkQuery);

                if (result) {
                    resetBulkQuery();
                    ExtraitDeRole extraitDeRole = RecouvrementBusiness.getExtraitRoleById(
                            extraitRoleId.trim());

                    PrintDocument printDocument = new PrintDocument();

                    if (extraitDeRole != null) {
                        dataReturn = printDocument.createExtraitRolePrint(extraitDeRole, listDetailRolePrint, receveurName);
                    }
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    private void resetBulkQuery() {
        bulkQuery = new HashMap<>();
        counter = GeneralConst.Numeric.ZERO;
    }

    public String printerProjectRole(HttpServletRequest request) {

        String dataReturn;

        try {

            String roleId = request.getParameter(RecouvrementConst.ParamName.ROLE_ID);
            String userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            String detailRoleList = request.getParameter(RecouvrementConst.ParamName.DETAIL_ROLE_LIST);

            Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(roleId.trim());

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                List<String> listAssujettiFull = new ArrayList<>();
                List<String> listAssujettiClean;
                List<AssujettiListPrint> listAssujettiListPrints = new ArrayList<>();

                ProjectRolePrint projectRolePrint = new ProjectRolePrint();

                String nameReceveur = GeneralConst.EMPTY_STRING;

                Agent agent = PaiementBusiness.getAgentByCode(Integer.valueOf(userId.trim()));

                if (agent != null) {
                    nameReceveur = agent.toString().toUpperCase();
                    projectRolePrint.setLieuSite("");

                    if (agent.getService() != null) {

                        projectRolePrint.setNameSector(agent.getService().getIntitule().toUpperCase());

                        if (agent.getService().getService() != null) {
                            projectRolePrint.setNameMinistrie(agent.getService().getService().getIntitule().toUpperCase());
                        } else {
                            projectRolePrint.setNameMinistrie(GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        projectRolePrint.setNameSector(GeneralConst.EMPTY_STRING);
                    }

                    if (agent.getSite() != null) {
                        projectRolePrint.setNameSite(agent.getSite().getIntitule().toUpperCase());
                    } else {
                        projectRolePrint.setNameSite(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    projectRolePrint.setLieuSite(GeneralConst.EMPTY_STRING);
                }

                projectRolePrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

                Role role = RecouvrementBusiness.getRoleById(roleId.trim());

                if (role != null) {

                    projectRolePrint.setArticleRole(role.getArticleRole().toUpperCase());

                    projectRolePrint.setRoleId(role.getId().trim());

                    if (propertiesConfig.getProperty("DISPLAY_CODE_QR_TO_DOCUMENT").equals(
                            GeneralConst.Number.ONE)) {

                        String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC").concat(
                                role.getArticleRole().toUpperCase().trim()));

                        if (qrCode != null && !qrCode.isEmpty()) {
                            projectRolePrint.setCodeQr(qrCode);
                        } else {
                            projectRolePrint.setCodeQr(GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        projectRolePrint.setCodeQr(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    projectRolePrint.setArticleRole(GeneralConst.EMPTY_STRING);
                }

                projectRolePrint.setNomReceveur(nameReceveur);
                projectRolePrint.setDateImpression(Tools.formatDateToString(new Date()));

                listDetailRolePrint = new ArrayList<>();

                if (!detailRoleList.isEmpty()) {

                    JSONArray jsonArray = new JSONArray(detailRoleList);

                    for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        DetailRolePrint detailRolePrint = new DetailRolePrint();

                        NotePerception np;
                        BonAPayer bp;

                        switch (jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT)) {
                            case "NP":
                                np = NotePerceptionBusiness.getNotePerceptionByCode(
                                        jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE));

                                if (np != null) {
                                    detailRolePrint.setDocumentReference(np.getNumero().trim());
                                } else {
                                    detailRolePrint.setDocumentReference(GeneralConst.EMPTY_STRING);
                                }
                                break;
//                            case "BP":
//                                bp = NotePerceptionBusiness.getBonAPayerByCode(
//                                        jsonObject.getString(RecouvrementConst.ParamName.DOCUMENT_REFERENCE));
//
//                                if (bp != null) {
//                                    detailRolePrint.setDocumentReference(bp.getBpManuel().trim());
//                                } else {
//                                    detailRolePrint.setDocumentReference(GeneralConst.EMPTY_STRING);
//                                }
//                                break;
                        }

                        detailRolePrint.setTypeDocument(
                                jsonObject.getString(RecouvrementConst.ParamName.TYPE_DOCUMENT));

                        detailRolePrint.setAmountPenalite(
                                jsonObject.getString(RecouvrementConst.ParamName.AMOUNT_PENALITE));

                        detailRolePrint.setAmountPrincipal(
                                jsonObject.getString(TaxationConst.ParamName.NET_A_PAYE));

                        detailRolePrint.setBudgetArticleCode(
                                jsonObject.getString(TaxationConst.ParamName.CODE_OFFICIEL));

                        detailRolePrint.setAssujettiCode(
                                jsonObject.getString(RecouvrementConst.ParamName.AssujettiCode));

                        detailRolePrint.setBudgetArticleLibelle(
                                jsonObject.getString(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE));

                        detailRolePrint.setDevise(
                                jsonObject.getString(TaxationConst.ParamName.DEVISE));

//                        detailRolePrint.setService(
//                                jsonObject.getString(TaxationConst.ParamName.LIBELLE_SERVICE));
//                        detailRolePrint.setDateCreate(
//                                jsonObject.getString(TaxationConst.ParamName.DATE_CREATE));
                        detailRolePrint.setIdRole(
                                jsonObject.getString(RecouvrementConst.ParamName.ROLE_ID));

                        listDetailRolePrint.add(detailRolePrint);

                        listAssujettiFull.add(detailRolePrint.getAssujettiCode().trim());
                    }

                    if (!listAssujettiFull.isEmpty()) {

                        listAssujettiClean = listAssujettiFull.stream().distinct().collect(Collectors.toList());

                        if (!listAssujettiClean.isEmpty()) {

                            listAssujettiListPrints = new ArrayList<>();

                            for (String assujettiCode : listAssujettiClean) {

                                AssujettiListPrint assujettiListPrint = new AssujettiListPrint();

                                Personne personne;
                                Adresse adresse;

                                personne = IdentificationBusiness.getPersonneByCode(assujettiCode.trim());

                                if (personne != null) {

                                    assujettiListPrint.setCodeAssujetti(personne.getCode().trim());
                                    assujettiListPrint.setRaison_Sociale(personne.toString().toUpperCase().trim());
                                    assujettiListPrint.setType(personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                                    if (personne.getNif() != null && !personne.getNif().isEmpty()) {
                                        assujettiListPrint.setNif(personne.getNif().toUpperCase());
                                    } else {
                                        assujettiListPrint.setNif(GeneralConst.EMPTY_STRING);
                                    }

                                    if (personne.getLoginWeb() != null) {
                                        assujettiListPrint.setTelephone(personne.getLoginWeb().getTelephone().trim());
                                    } else {
                                        assujettiListPrint.setTelephone(GeneralConst.EMPTY_STRING);
                                    }

                                    adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode().trim());

                                    if (adresse != null) {
                                        assujettiListPrint.setSiege_Social(adresse.toString().toUpperCase());
                                    } else {
                                        assujettiListPrint.setSiege_Social(GeneralConst.EMPTY_STRING);
                                    }
                                }

                                listAssujettiListPrints.add(assujettiListPrint);
                            }
                        }
                    }

                    System.out.println("assujetti : " + listAssujettiListPrints.size());
                    System.out.println("detail : " + listDetailRolePrint.size());

                    dataReturn = Tools.getDataTableToHtmlV3(listAssujettiListPrints, listDetailRolePrint);

                    if (dataReturn != null && !dataReturn.isEmpty()) {
                        projectRolePrint.setDetailRole(dataReturn);
                    } else {
                        projectRolePrint.setDetailRole(GeneralConst.EMPTY_STRING);
                    }

                    PrintDocument printDocument = new PrintDocument();
                    dataReturn = printDocument.createProjectRole(projectRolePrint);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;

    }

    public String getNotePerceptionNotMairiePaid(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;

        String codeService = GeneralConst.EMPTY_STRING;
        String codeSite = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        try {

            advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);

            codeService = request.getParameter(RecouvrementConst.ParamName.CODE_SERVICE);
            codeSite = request.getParameter(RecouvrementConst.ParamName.CODE_SITE);
            dateDebut = request.getParameter(RecouvrementConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(RecouvrementConst.ParamName.DATE_FIN);

            notePerceptionMairieList = new ArrayList<>();

            advancedSearch = advancedSearch == null ? "1" : advancedSearch;

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    notePerceptionMairieList = RecouvrementBusiness.getListNotePerceptionNotMairiePaid(assujettiCode);

                    break;
                case GeneralConst.Number.ONE:

                    notePerceptionMairieList = RecouvrementBusiness.getListNotePerceptionNotMairiePaidBySite(
                            codeService, codeSite, dateDebut, dateFin);
                    break;
            }

            if (!notePerceptionMairieList.isEmpty()) {

                List<JsonObject> npJsonList = new ArrayList<>();

                for (NotePerceptionMairie np : notePerceptionMairieList) {

                    JsonObject npJson = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Med med = new Med();
                    Agent agent = new Agent();
                    NotePerceptionMairie npMairie = new NotePerceptionMairie();

                    String dateOrd = GeneralConst.EMPTY_STRING;
                    Date dateEcheance;

                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;
                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                    npJson.addProperty(RecouvrementConst.ParamName.NUMERO_NP, np.getNumeroNp());
                    npJson.addProperty(RecouvrementConst.ParamName.ID_NP_MAIRIE, np.getId());

                    npJson.addProperty(RecouvrementConst.ParamName.DATE_ECHEANCE_NP,
                            ConvertDate.formatDateToString(np.getDateEcheance()));

                    npJson.addProperty("nbreMoisRetard", TaxationBusiness.getDateDiffBetwenTwoDates(
                            ConvertDate.formatDateToString(np.getDateEcheance())));
                    //npJson.addProperty("nbreMoisRetard", "");
                    npJson.addProperty(RecouvrementConst.ParamName.EXERCICE_NP,
                            propertiesConfig.getProperty("EXERCICE_FISCAL"));

                    npJson.addProperty(RecouvrementConst.ParamName.DATE_ORDONNANCEMENT_NC,
                            GeneralConst.EMPTY_STRING);

                    npJson.addProperty(RecouvrementConst.ParamName.AMOUNT_NP, np.getMontant());
                    npJson.addProperty(RecouvrementConst.ParamName.DEVISE_NP, np.getFkDevise());

                    npMairie = RecouvrementBusiness.getNotePerceptionMairieByCode(np.getNumeroNp());

                    if (npMairie != null) {
                        npJson.addProperty(PaiementConst.ParamName.AMOUNT2, npMairie.getMontant());
                        npJson.addProperty(RecouvrementConst.ParamName.ID_NP_MAIRIE, npMairie.getId());
                    } else {
                        npJson.addProperty(PaiementConst.ParamName.AMOUNT2, GeneralConst.Number.ZERO);
                        npJson.addProperty(RecouvrementConst.ParamName.ID_NP_MAIRIE, GeneralConst.EMPTY_STRING);
                    }

                    agent = GeneralBusiness.getAgentByCode(String.valueOf(np.getAgentCreate()));

                    if (agent != null) {
                        npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                agent.toString().toUpperCase());
                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.AGENT_CREATION_MED_OR_MEP,
                                GeneralConst.EMPTY_STRING);
                    }

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(String.valueOf(np.getId()));

                    if (archive != null) {
                        npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                archive.getDocumentString());
                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT,
                                GeneralConst.EMPTY_STRING);
                    }

                    ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(np.getFkAb());

                    if (ab != null) {
                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                ab.getCode());
                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                ab.getIntitule());
                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    personne = IdentificationBusiness.getPersonneByCode(np.getFkPersonne());

                    if (personne != null) {
                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                personne.getCode());

                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                personne.toString().toUpperCase());

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    adresse.getId());

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    adresse.toString().toUpperCase());

                        } else {

                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    GeneralConst.EMPTY_STRING);
                            npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    GeneralConst.EMPTY_STRING);
                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        npJson.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                    } else {
                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                GeneralConst.EMPTY_STRING);
                        npJson.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    npJsonList.add(npJson);
                }

                dataReturn = npJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
