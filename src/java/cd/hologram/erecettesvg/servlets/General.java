/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AccuserReceptionBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.util.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.constants.*;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.apache.commons.lang.time.StopWatch;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "General", urlPatterns = {"/general_servlet"})
public class General extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG),
            propertiesMail = Property.getProperties(Property.FileData.FR_MAIL);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        switch (operation) {
            case GeneralConst.Operation.LOAD_ENTITIES:
                result = getAllEntities();
                break;
            case GeneralConst.Operation.CREATE_ENTITY:
                result = saveEntity(request);
                break;
            case GeneralConst.Operation.UPLOADING_TYPE_DOCUMENT:
                result = uploadingTypeDocument(request);
                break;
            case GeneralConst.Operation.UPDATE_DATE_ECHEANCE:
                result = accuserReception(request);
                break;
            case GeneralConst.Operation.LOAD_PENALITE_LIST:
                result = getPenalitiesList();
                break;
            case GeneralConst.Operation.GET_ENTITE_CODE:
                result = getEntitiesByCode(request);
                break;
            case GeneralConst.Operation.GET_ENTITE_BY_MERE:
                result = getEntiteAdministrativeByMere(request);
                break;
        }

        //stopWatch.stop();
        //System.out.println("Time : " + stopWatch.toString());
        out.print(result);

    }

    public String getPenalitiesList() {

        try {

            List<JsonObject> jsonPenalitiesList = new ArrayList<>();
            List<Penalite> penalites = GeneralBusiness.getPenalite();

            for (Penalite penalite : penalites) {

                JsonObject jsonObjectPenalite = new JsonObject();

                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_CODE,
                        penalite.getCode() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getCode());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_INTITULE,
                        penalite.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getIntitule());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_TYPE_PENALITE,
                        penalite.getTypePenalite() == null ? GeneralConst.EMPTY_STRING
                                : penalite.getTypePenalite().getIntitule());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_APPLIQUER_MOIS_RETARD, penalite.getAppliquerMoisRetard());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_PALIER, penalite.getPalier());
                jsonObjectPenalite.addProperty(GeneralConst.ParamName.PENALITE_IS_VISIBLE_UTILISATEUR, penalite.getVisibilteUtilisateur());

                jsonPenalitiesList.add(jsonObjectPenalite);

                List<JsonObject> jsonTauxPenaliteList = new ArrayList<>();

                for (PalierTauxPenalite palierTauxPenalite : penalite.getPalierTauxPenaliteList()) {

                    if (palierTauxPenalite.getEtat()) {

                        JsonObject jsonObjecttauxPenalite = new JsonObject();

                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_ID, palierTauxPenalite.getId());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_PENALITE, palierTauxPenalite.getPenalite().getCode());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_BORNE_INFERIEURE, palierTauxPenalite.getBorneInferieure());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_BORNE_SUPERIEURE, palierTauxPenalite.getBorneSuperieure());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_FORME_JURIDIQUE, palierTauxPenalite.getFormeJuridique().getCode());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_RECIDIVE, palierTauxPenalite.getRecidive());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_VALEUR, palierTauxPenalite.getValeur());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_TYPE_VALEUR, palierTauxPenalite.getTypeValeur());
                        jsonObjecttauxPenalite.addProperty(GeneralConst.ParamName.T_PALIER_TAUX_PENALITE_EST_POURCENTAGE, palierTauxPenalite.getEstPourcentage());

                        jsonTauxPenaliteList.add(jsonObjecttauxPenalite);
                    }

                }

                jsonObjectPenalite.addProperty(GeneralConst.ParamName.LIST_TAUX_PANALIE, jsonTauxPenaliteList.toString());
            }

            return jsonPenalitiesList.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String accuserReception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            String typeDocument = request.getParameter(GeneralConst.AccuserReception.TYPE_DOCUMENT);

            String DAY_DOCUMENT_ECHEANCE = GeneralConst.EMPTY_STRING;

            switch (typeDocument) {
                case DocumentConst.DocumentCode.NP:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_NP_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.AMR:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_AMR_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.MEP_2:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_MEP_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.MED_2:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_MED_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.AMR1:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_AMR1_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.AMR2:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_AMR2_ECHEANCE;
                    break;
                case DocumentConst.DocumentCode.CTR:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_CONTRAINTE;
                    break;
                case DocumentConst.DocumentCode.CMDT:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_COMMANDEMENT;
                    break;
                case DocumentConst.DocumentCode.BP:
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_ECHEANCE_BP;
                    break;
                case "INVITATION_SERVICE":
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_ECHEANCE_INVITATION_SERVICE;
                    break;
                case "INVITATION_PAIEMENT":
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_ECHEANCE_INVITATION_PAIEMENT;
                    break;
                case "RELANCE":
                    DAY_DOCUMENT_ECHEANCE = NotePerceptionConst.ParamName.NBRE_DAY_ECHEANCE_RELANCE;
                    break;
            }

            boolean result = AccuserReceptionBusiness.AccuserReception(Integer.valueOf(DAY_DOCUMENT_ECHEANCE), request);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String saveEntity(HttpServletRequest request) {

        String results = GeneralConst.EMPTY_STRING;
        String dataReturn = null;
        String typeEntity = null;

        try {
            String operation = request.getParameter(GeneralConst.OPERATION);
            String type = request.getParameter(GeneralConst.ParamName.ENTITE_TYPE);
            String mere = request.getParameter(GeneralConst.ParamName.ENTITE_MERE);
            String idUser = request.getParameter(LoginConst.ParamName.ID_USER);
            String intitule = request.getParameter(GeneralConst.ParamName.ENTITE_INTITULE);

            EntiteAdministrative entity = new EntiteAdministrative();

            if (!mere.isEmpty()) {
                entity.setEntiteMere(new EntiteAdministrative(mere));
            } else {
                entity.setEntiteMere(new EntiteAdministrative("null"));
            }

            entity.setIntitule(intitule);
            entity.setEtat(Short.valueOf(GeneralConst.Number.ONE));
            entity.setTypeEntite(new TypeEntite(Integer.valueOf(type)));

            results = GeneralBusiness.saveEntitys(entity, idUser);

            if (results.equals("0")) {
                dataReturn = GeneralConst.ResultCode.PERSONNE_NON_EXIST;
            } else {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String getAllEntities() {

        try {

            List<JsonObject> listEntitiesJson = new ArrayList<>();
            List<EntiteAdministrative> entiteAdministratives = GeneralBusiness.getAllEntiteAdministrative();

            for (EntiteAdministrative entiteAdministrative : entiteAdministratives) {

                JsonObject jsonEntiteObject = new JsonObject();

                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_CODE, entiteAdministrative.getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_INTITULE, entiteAdministrative.getIntitule());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_TYPE, entiteAdministrative.getTypeEntite().getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_MERE, entiteAdministrative.getEntiteMere() != null ? entiteAdministrative.getEntiteMere().getCode() : GeneralConst.EMPTY_STRING);

                listEntitiesJson.add(jsonEntiteObject);
            }

            return listEntitiesJson.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String uploadingTypeDocument(HttpServletRequest request) {
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            List<TypeDocument> listTypeDocuments = new ArrayList<>();
            listTypeDocuments = GeneralBusiness.getListTypeDocumentUploading();

            if (!listTypeDocuments.isEmpty()) {

                List<JsonObject> jsonList = new ArrayList<>();
                JsonObject jsonObject;

                for (TypeDocument typeDocument : listTypeDocuments) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(GeneralConst.ParamName.CODE_TYPE_DOCUMENT,
                            typeDocument.getCode() == null ? GeneralConst.EMPTY_STRING
                                    : typeDocument.getCode().trim());
                    jsonObject.addProperty(GeneralConst.ParamName.LIBELLE_TYPE_DOCUMENT,
                            typeDocument.getIntitule() == null ? GeneralConst.EMPTY_STRING
                                    : typeDocument.getIntitule().toUpperCase().trim());

                    jsonList.add(jsonObject);
                }

                dataReturn = jsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String getEntitiesByCode(HttpServletRequest request) {

        try {

            List<JsonObject> listEntitiesJson = new ArrayList<>();

            String codeEntite = request.getParameter(GeneralConst.ParamName.ENTITE_CODE);

            List<EntiteAdministrative> entiteAdministratives = GeneralBusiness.getEntiteAdministrativeByCode(codeEntite);

            for (EntiteAdministrative entiteAdministrative : entiteAdministratives) {

                JsonObject jsonEntiteObject = new JsonObject();

                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_CODE, entiteAdministrative.getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_INTITULE, entiteAdministrative.getIntitule());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_TYPE, entiteAdministrative.getTypeEntite().getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_MERE, entiteAdministrative.getEntiteMere() != null ? entiteAdministrative.getEntiteMere().getCode() : GeneralConst.EMPTY_STRING);

                listEntitiesJson.add(jsonEntiteObject);
            }

            return listEntitiesJson.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String getEntiteAdministrativeByMere(HttpServletRequest request) {

        try {

            List<JsonObject> listEntitiesJson = new ArrayList<>();

            String codeEntiteMere = request.getParameter(GeneralConst.ParamName.ENTITE_MERE);
            String from = request.getParameter("from");

            EntiteAdministrative ea = GeneralBusiness.getEntiteAdministrativeByCodeV2(codeEntiteMere);

            List<EntiteAdministrative> entiteAdministratives = new ArrayList<>();

            int type = ea.getTypeEntite().getCode();

            /*if (type == 5) {
                type = 3;
            }*/

            switch (from) {
                case "0":
                    entiteAdministratives = GeneralBusiness.getEntiteAdministrativeByMereV2(codeEntiteMere, type);
                    break;
                case "1":
                    type = 3;
                    entiteAdministratives = GeneralBusiness.getEntiteAdministrativeByMereV2(codeEntiteMere, type);
                    break;
            }

            for (EntiteAdministrative entiteAdministrative : entiteAdministratives) {

                JsonObject jsonEntiteObject = new JsonObject();

                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_CODE, entiteAdministrative.getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_INTITULE, entiteAdministrative.getIntitule());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_TYPE, entiteAdministrative.getTypeEntite().getCode());
                jsonEntiteObject.addProperty(GeneralConst.ParamName.ENTITE_MERE, entiteAdministrative.getEntiteMere() != null ? entiteAdministrative.getEntiteMere().getCode() : GeneralConst.EMPTY_STRING);

                listEntitiesJson.add(jsonEntiteObject);
            }

            return listEntitiesJson.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public void StringTest() {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
