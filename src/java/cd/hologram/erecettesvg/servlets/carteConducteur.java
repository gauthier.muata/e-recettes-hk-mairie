/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import static cd.hologram.erecettesvg.business.AssujettissementBusiness.getComplements;
import cd.hologram.erecettesvg.business.ConnexionBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.business.TrackingCarteBusiness;
import cd.hologram.erecettesvg.constants.AcquitLiberatoireConst;
import cd.hologram.erecettesvg.constants.AssujettissementConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.GeneralConst.FormatComplement;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.LoginConst;
import cd.hologram.erecettesvg.etats.Anchor;
import cd.hologram.erecettesvg.models.Acquisition;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Carte;
import cd.hologram.erecettesvg.models.Complement;
import cd.hologram.erecettesvg.models.ConducteurVehicule;
import cd.hologram.erecettesvg.models.LogCpiCarteConducteurMoto;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.Property;
import static cd.hologram.erecettesvg.util.Tools.generateQRCode;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author bonheur.muntasomo
 */
@WebServlet(name = "carteConducteur", urlPatterns = {"/carteConducteur_servlet"})
public class carteConducteur extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {

            case "printCarte":
                result = imprimerCarte(request);
                break;
            case AcquitLiberatoireConst.Operation.CHECK_FICHE_CPI:
                result = checkCPI(request);
                break;
            case AcquitLiberatoireConst.Operation.SEARCH_CPI:
                result = searchCPI(request);
                break;
            case IdentificationConst.Operation.SAVE_CARTE:
                result = saveCarte(request);
                break;
            case AssujettissementConst.Operation.LOAD_BIENS_PERSONNE2:
                result = loadBiensPersonne2(request);
                break;
            case "loadBiensMoto":
                result = loadBiensPersonne2(request);
                break;
            case "loadCarte":
                result = registreCarte(request);
                break;
            case "dissociateContrat":
                result = dissocierBien(request);
                break;
            case "checkValidate":
                result = checkValidate(request);
                break;
        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String checkValidate(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String value = GeneralConst.EMPTY_STRING;
        String sw = GeneralConst.EMPTY_STRING;

        try {

            value = request.getParameter("value");
            sw = request.getParameter("sw");

            dataReturn = TrackingCarteBusiness.checkValidateValue(value, sw);

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String checkCPI(HttpServletRequest request) {

        String personne = request.getParameter(AcquitLiberatoireConst.ParamName.PERSONNE);
        String ab = request.getParameter(AcquitLiberatoireConst.ParamName.ARTICLE_BUDGETAIRE);
        String cpi = request.getParameter(AcquitLiberatoireConst.ParamName.CPI);

        String result;
        try {

            cd.hologram.erecettesvg.models.AcquitLiberatoire acquitLiberatoire = AcquitLiberatoireBusiness.checkCPIFiche(personne, ab, cpi);

            if (acquitLiberatoire != null) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String imprimerCarte(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String documentHtml;

        try {

            String numeroReference = request.getParameter("reference");
            String isToTracking = request.getParameter("isToTracking");
            String idUser = request.getParameter(LoginConst.ParamName.ID_USER);

            Carte carte = AssujettissementBusiness.getCarteImpression(numeroReference);
            documentHtml = getDocumentHtml(carte, idUser);

            if (isToTracking.equals(GeneralConst.Number.ZERO)) {
                documentHtml = documentHtml.replace("hidden=\"true\"", "");
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return documentHtml;
    }

    public String searchCPI(HttpServletRequest request) {

        JSONObject jsonCPI = new JSONObject();

        String response = GeneralConst.EMPTY_STRING;

        try {

            String codeCPI = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_CPI);

            cd.hologram.erecettesvg.models.AcquitLiberatoire acquitLiberatoire
                    = new cd.hologram.erecettesvg.models.AcquitLiberatoire();
            Personne personne = new Personne();

            String taxeParam = propertiesConfig.getProperty("SQL_PARAM_AB_CONDUCTEUR_MOTO");

            acquitLiberatoire = AcquitLiberatoireBusiness.getAcquitLiberatoireByNumero(codeCPI, taxeParam);

            if (acquitLiberatoire != null) {

                LogCpiCarteConducteurMoto carteConducteurMoto = AcquitLiberatoireBusiness.getLogCpiByCpi(
                        acquitLiberatoire.getCode());

                if (carteConducteurMoto != null) {

                    response = GeneralConst.Number.TWO;

                } else {

                    personne = IdentificationBusiness.getPersonneByCode(
                            acquitLiberatoire.getNotePerception().getNoteCalcul().getPersonne());

                    jsonCPI.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getFormeJuridique() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getFormeJuridique().getIntitule());

                    jsonCPI.put("nbreTaxe", acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList().size());

                    jsonCPI.put(IdentificationConst.ParamName.NIF,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                    jsonCPI.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                    jsonCPI.put(IdentificationConst.ParamName.NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                    jsonCPI.put(IdentificationConst.ParamName.POST_NOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                    jsonCPI.put(IdentificationConst.ParamName.PRENOM,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                    jsonCPI.put(IdentificationConst.ParamName.TELEPHONE,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getTelephone());

                    jsonCPI.put(IdentificationConst.ParamName.EMAIL,
                            personne == null
                                    ? GeneralConst.EMPTY_STRING
                                    : personne.getLoginWeb() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : personne.getLoginWeb().getMail());

                    jsonCPI.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                            getDefaultAdresse(personne).toUpperCase());

                    jsonCPI.put(IdentificationConst.ParamName.CODE_PERSONNE,
                            personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());
                    jsonCPI.put("categorie", personne == null ? GeneralConst.EMPTY_STRING : personne.getFormeJuridique().getIntitule());
                    jsonCPI.put("observation", acquitLiberatoire.getObservation() == null ? GeneralConst.EMPTY_STRING : acquitLiberatoire.getObservation());

                    jsonCPI.put(AcquitLiberatoireConst.ParamName.CODE_CPI, acquitLiberatoire.getCode().trim());

                    response = jsonCPI.toString();
                }

            } else {
                response = GeneralConst.Number.ZERO;

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return response;
    }

    public String getDefaultAdresse(Personne personne) {

        String adresse = GeneralConst.EMPTY_STRING;
        if (personne != null) {
            if (personne.getAdressePersonneList() != null) {
                for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {
                    if (adressePersonne.getParDefaut() == true && adressePersonne.getEtat() == true) {
                        adresse = adressePersonne.getAdresse().toString();
                        break;
                    }
                }
            }
        }
        return adresse;
    }

    String getDocumentHtml(Carte carte, String idUser) {

        String documentHtml = GeneralConst.EMPTY_STRING;

        String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC_CARTE").concat(
                carte.getId().toUpperCase().trim()));

        try {

            documentHtml = propertiesConfig.getProperty("DOCUMENT_HTML");

            Anchor anchor = new Anchor();

            List<Anchor> listAnchor = anchor.getAnchorList(documentHtml);

            for (Anchor monAnchor : listAnchor) {

                //String resultatTraitement = Traitement(monAnchor, carte, qrCode);
                String resultatTraitement = Traitement_V2(monAnchor, carte, qrCode, "", idUser);

                documentHtml = documentHtml.replace(monAnchor.getAnchorTxt(), resultatTraitement);
            }

        } catch (Exception e) {
            throw e;
        }

        return documentHtml;

    }

    public String Traitement(Anchor anchor, Carte carte, String qrCode) {

        String textRetourner = GeneralConst.EMPTY_STRING;

        String logoHVK = propertiesConfig.getProperty("LOGO_HVK");
        String complementLieuNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_LIEU_NAISSANCE");
        String complementDateNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_DATE_NAISSANCE");

//        List<Complement> listComplements = carte.getFkConducteurVehicule().getConducteur().getComplementList();
        String lieuNaissance = GeneralConst.EMPTY_STRING;
        String dateNaissance = GeneralConst.EMPTY_STRING;

        switch (anchor.getField().toLowerCase().trim()) {
            case FormatComplement.LOGO:
                textRetourner = logoHVK;
                break;
            case FormatComplement.AVATAR:
                if (carte.getPhotoProfil() != null) {
                    textRetourner = carte.getPhotoProfil();
                }
                break;
            case FormatComplement.NOM:
                textRetourner = carte.getFkConducteurVehicule().getConducteur().getNom();
                break;
            case FormatComplement.PRENOM:
                textRetourner = carte.getFkConducteurVehicule().getConducteur().getPrenoms();
                break;
            case FormatComplement.LIEU_NAISSANCE:

                List<Complement> complements = getComplements(carte.getFkConducteurVehicule().getConducteur().getCode());
                if (!complements.isEmpty()) {
                    for (Complement complement : complements) {
                        if (complement.getFkComplementForme().getCode().equals(complementLieuNaissance)) {
                            lieuNaissance = complement.getValeur();
                            break;
                        }
                    }
                }
                textRetourner = lieuNaissance;
                break;
            case FormatComplement.DATE_NAISSANCE:
                List<Complement> complements_ = getComplements(carte.getFkConducteurVehicule().getConducteur().getCode());
                if (!complements_.isEmpty()) {
                    for (Complement complement : complements_) {
                        if (complement.getFkComplementForme().getCode().equals(complementDateNaissance)) {

                            if (complement.getValeur().contains("-")) {
                                dateNaissance = ConvertDate.getValidFormatDatePrint(complement.getValeur());
                            } else {
                                dateNaissance = complement.getValeur();
                            }

                            break;
                        }
                    }
                }
                textRetourner = dateNaissance;
                break;
            case FormatComplement.DISTRICT:
                if (carte.getDistrict() != null) {
                    textRetourner = carte.getDistrict();
                } else {
                    textRetourner = GeneralConst.DASH_SEPARATOR;
                }
                break;
            case FormatComplement.NOM_COMMUNE:
                if (carte.getCommune() != null) {
                    textRetourner = carte.getCommune();
                } else {
                    textRetourner = GeneralConst.DASH_SEPARATOR;
                }
                break;
            case FormatComplement.SIGNATURE:
                textRetourner = "";
                break;
            case FormatComplement.NUMERO_CARTE:
                textRetourner = carte.getId();
                break;
            case FormatComplement.DATE_LIVRAISON:
                textRetourner = carte.getDateLivraison() != null
                        ? ConvertDate.formatDateToString(carte.getDateLivraison())
                        : GeneralConst.EMPTY_STRING;
                break;
            case FormatComplement.DATE_FIN_VALIDITE:
                textRetourner = carte.getDateFinValidite() != null
                        ? ConvertDate.formatDateToString(carte.getDateFinValidite())
                        : GeneralConst.EMPTY_STRING;
                break;
            case FormatComplement.QRCODE:
                textRetourner = qrCode;
                break;

        }

        if (!textRetourner.isEmpty()) {
            return textRetourner;
        }

        return textRetourner;
    }

    public String Traitement_V2(Anchor anchor, Carte carte, String qrCode, String signature, String idUser) {

        String textRetourner = GeneralConst.EMPTY_STRING;

        String logoHVK = propertiesConfig.getProperty("LOGO_HVK");
        String complementLieuNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_LIEU_NAISSANCE");
        String complementDateNaissance = propertiesConfig.getProperty("CODE_COMPLEMENT_FORME_DATE_NAISSANCE");

        Agent agent = ConnexionBusiness.getAgentByCode(Integer.valueOf(idUser));

//        List<Complement> listComplements = carte.getFkConducteurVehicule().getConducteur().getComplementList();
        String lieuNaissance = GeneralConst.EMPTY_STRING;
        String dateNaissance = GeneralConst.EMPTY_STRING;

        switch (anchor.getField().toLowerCase().trim()) {
            case FormatComplement.LOGO:
                textRetourner = logoHVK;
                break;
            case FormatComplement.AVATAR:
                if (carte.getPhotoProfil() != null) {
                    textRetourner = carte.getPhotoProfil();
                }
                break;
            case FormatComplement.NOM:
                textRetourner = carte.getFkConducteurVehicule().getConducteur().getNom();
                break;
            case FormatComplement.PRENOM:
                textRetourner = carte.getFkConducteurVehicule().getConducteur().getPrenoms();
                break;
            case FormatComplement.LIEU_NAISSANCE:

                List<Complement> complements = getComplements(carte.getFkConducteurVehicule().getConducteur().getCode());
                if (!complements.isEmpty()) {
                    for (Complement complement : complements) {
                        if (complement.getFkComplementForme().getCode().equals(complementLieuNaissance)) {
                            lieuNaissance = complement.getValeur();
                            break;
                        }
                    }
                }
                textRetourner = lieuNaissance;
                break;
            case FormatComplement.DATE_NAISSANCE:
                List<Complement> complements_ = getComplements(carte.getFkConducteurVehicule().getConducteur().getCode());
                if (!complements_.isEmpty()) {
                    for (Complement complement : complements_) {
                        if (complement.getFkComplementForme().getCode().equals(complementDateNaissance)) {

                            if (complement.getValeur().contains("-")) {
                                dateNaissance = ConvertDate.getValidFormatDatePrint(complement.getValeur());
                            } else {
                                dateNaissance = complement.getValeur();
                            }

                            break;
                        }
                    }
                }
                textRetourner = dateNaissance;
                break;
            case FormatComplement.DISTRICT:
                if (carte.getDistrict() != null) {
                    textRetourner = carte.getDistrict();
                } else {
                    textRetourner = GeneralConst.DASH_SEPARATOR;
                }
                break;
            case FormatComplement.NOM_COMMUNE:
                //if (carte.getCommune() != null) {
                if (agent.getSite().getAdresse().getCommune() != null) {
                    textRetourner = agent.getSite().getAdresse().getCommune().getIntitule().toUpperCase();
                } else {
                    textRetourner = GeneralConst.DASH_SEPARATOR;
                }
                break;
            case FormatComplement.SIGNATURE: // Here
                Personne personne = IdentificationBusiness.getPersonneByCode(agent.getSite().getPersonne());
                textRetourner = personne.getSignature();
                break;
            case FormatComplement.NUMERO_CARTE:
                textRetourner = carte.getId();
                break;
            case FormatComplement.DATE_LIVRAISON:
                textRetourner = carte.getDateLivraison() != null
                        ? ConvertDate.formatDateToString(carte.getDateLivraison())
                        : GeneralConst.EMPTY_STRING;
                break;
            case FormatComplement.DATE_FIN_VALIDITE:
                textRetourner = carte.getDateFinValidite() != null
                        ? ConvertDate.formatDateToString(carte.getDateFinValidite())
                        : GeneralConst.EMPTY_STRING;
                break;
            case FormatComplement.QRCODE:
                textRetourner = qrCode;
                break;

        }

        if (!textRetourner.isEmpty()) {
            return textRetourner;
        }

        return textRetourner;
    }

    public String saveCarte(HttpServletRequest request) {

        String result;

        try {
            String numAutocollant = request.getParameter("numAutocollant");
            String numAutorisation = request.getParameter("numAutorisation");
            String proprietaire = request.getParameter("proprietaire");
            String conducteur = request.getParameter("conducteur");
            String bien = request.getParameter("bien");
            String photo = request.getParameter("photo");
            String numCPI = request.getParameter("numCPI");
            String district = request.getParameter("district");
            String commune = request.getParameter("commune");
            String userId = request.getParameter("userId");

            if (IdentificationBusiness.saveCarte(numAutocollant, numAutorisation, proprietaire,
                    conducteur, bien, photo, numCPI, commune, district)) {

                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String registreCarte(HttpServletRequest request) {

        List<JSONObject> jsonCarteList = new ArrayList<>();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String libelle = request.getParameter(AcquitLiberatoireConst.ParamName.LIBELLE);
            String typeSearch = request.getParameter(AcquitLiberatoireConst.ParamName.TYPE_SEARCH);

            List<Carte> cartes = new ArrayList<>();

            cartes = AssujettissementBusiness.getCarte(libelle, typeSearch);

            if (!cartes.isEmpty()) {

                for (Carte carte : cartes) {

                    JSONObject jsonCarte = new JSONObject();
                    Adresse adresse = new Adresse();

                    jsonCarte.put("numAutocollant",
                            carte.getNumeroAutocollan() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : carte.getNumeroAutocollan());

                    jsonCarte.put("stateCard", carte.getEtat());

                    jsonCarte.put("numAutorisation",
                            carte.getNumeroAutorisation() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : carte.getNumeroAutorisation());

                    //jsonCarte.put("nomComplet", carte.getFkConducteurVehicule().getConducteur().getNom());
                    String nomProprietaire = GeneralConst.EMPTY_STRING;
                    String nomCompositeProprietaire = GeneralConst.EMPTY_STRING;
                    String adresseProprietaire = GeneralConst.EMPTY_STRING;

                    if (carte.getFkConducteurVehicule() != null) {

                        nomProprietaire = "Nom : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getFkConducteurVehicule().getProprietaire().toString().toUpperCase().concat("</span>")));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                carte.getFkConducteurVehicule().getProprietaire().getCode());

                        if (adresse != null) {

                            adresseProprietaire = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    adresse.toString().toUpperCase().concat("</span>")));
                        }

                        nomCompositeProprietaire = nomProprietaire.concat("<br/>").concat(adresseProprietaire);

                        jsonCarte.put("nomProprietaire", nomCompositeProprietaire);
                    } else {
                        jsonCarte.put("nomProprietaire", GeneralConst.EMPTY_STRING);
                    }

                    String nomConducteur = GeneralConst.EMPTY_STRING;
                    String nomCompositeConducteur = GeneralConst.EMPTY_STRING;
                    String adresseConducteur = GeneralConst.EMPTY_STRING;

                    if (carte.getFkConducteurVehicule() != null) {

                        nomConducteur = "Nom : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getFkConducteurVehicule().getConducteur().toString().toUpperCase().concat("</span>")));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                carte.getFkConducteurVehicule().getConducteur().getCode());

                        if (adresse != null) {

                            adresseConducteur = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    adresse.toString().toUpperCase().concat("</span>")));
                        }

                        nomCompositeConducteur = nomConducteur.concat("<br/>").concat(adresseConducteur);

                        jsonCarte.put("nomConducteur", nomCompositeConducteur);
                    } else {
                        jsonCarte.put("nomConducteur", GeneralConst.EMPTY_STRING);
                    }

                    String typeBien = GeneralConst.EMPTY_STRING;
                    String nomBien = GeneralConst.EMPTY_STRING;
                    String description = GeneralConst.EMPTY_STRING;
                    String autocollant = GeneralConst.EMPTY_STRING;
                    String autorisation = GeneralConst.EMPTY_STRING;
                    String nomCompositeBien = GeneralConst.EMPTY_STRING;

                    if (carte.getFkConducteurVehicule().getBien() != null) {

                        typeBien = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getFkConducteurVehicule().getBien().getTypeBien().getIntitule().toUpperCase().concat("</span>")));

                        nomBien = "Marque : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getFkConducteurVehicule().getBien().getIntitule().toUpperCase().concat("</span>")));

                        description = "Description : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getFkConducteurVehicule().getBien().getDescription().toUpperCase().concat("</span>")));

                        autocollant = "N° autocollant : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getNumeroAutocollan().concat("</span>")));

                        autorisation = "N° autorisation : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                carte.getNumeroAutorisation().concat("</span>")));

                        nomCompositeBien = typeBien.concat("<br/>").concat(nomBien).concat("<br/>").concat(description).concat(
                                "<br/><br/>").concat(autocollant).concat("<br/>").concat(autorisation);

                        jsonCarte.put("nomCompositeBien", nomCompositeBien);

                    } else {
                        jsonCarte.put("nomCompositeBien", GeneralConst.EMPTY_STRING);
                    }

                    jsonCarte.put("numeroCPI",
                            carte.getNumeroCpi() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : carte.getNumeroCpi());

                    jsonCarte.put("dateCreation",
                            carte.getDateGeneration() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : ConvertDate.formatDateToString(carte.getDateGeneration()));

                    jsonCarte.put("idCarte", carte.getId());

                    jsonCarte.put("dateExpiration",
                            carte.getDateFinValidite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : ConvertDate.formatDateToString(carte.getDateFinValidite()));

                    if (carte.getDateFinValidite().before(new Date())) {
                        jsonCarte.put("cardExpired", GeneralConst.Number.ONE);
                    } else {
                        jsonCarte.put("cardExpired", GeneralConst.Number.TWO);
                    }

                    jsonCarteList.add(jsonCarte);
                }

                dataReturn = jsonCarteList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String dissocierBien(HttpServletRequest request) {

        String returnValue = GeneralConst.EMPTY_STRING;

        try {

            String contraitId = request.getParameter("contratId");
            String userId = request.getParameter("userId");

            boolean response = AssujettissementBusiness.disociateMotoWithConducteur(contraitId, userId);

            if (response) {
                returnValue = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                returnValue = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return returnValue;
    }

    public String loadBiensPersonne2(HttpServletRequest request) {

        List<JSONObject> jsonBiens = new ArrayList<>();
        List<JSONObject> jsonAssujettis = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();

        try {

            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String sqlParam = propertiesConfig.getProperty("CODE_TYPE_BIEN_MOTO");

            List<Acquisition> acquisitions = AssujettissementBusiness.getBiensOfPersonneV2(codePersonne, sqlParam);

            for (Acquisition acquisition : acquisitions) {
                JSONObject jsonBien = new JSONObject();

                jsonBien.put(AssujettissementConst.ParamName.ID_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getId());

                jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getId());

                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getIntitule());

                jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getDescription());

                jsonBien.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne().getAdresse().toString().toUpperCase());

                String bienComposite = GeneralConst.EMPTY_STRING;
                String typeBien = GeneralConst.EMPTY_STRING;
                String intituleBien = GeneralConst.EMPTY_STRING;
                String descriptionBien = GeneralConst.EMPTY_STRING;
                String chaineAdresse = GeneralConst.EMPTY_STRING;

                typeBien = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                        acquisition.getBien().getTypeBien().getIntitule().toUpperCase().concat("</span>")));

                intituleBien = "Intitulé : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                        acquisition.getBien().getIntitule().toUpperCase().concat("</span>")));

                descriptionBien = "Description : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                        acquisition.getBien().getDescription().toUpperCase().concat("</span>")));

                chaineAdresse = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                        acquisition.getBien().getFkAdressePersonne().getAdresse().toString().toUpperCase().concat("</span>")));

                bienComposite = typeBien.concat("<br/>").concat(intituleBien).concat("<br/>").concat(descriptionBien).concat("<br/>").concat(chaineAdresse);

                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN_COMPOSITE, bienComposite);

                jsonBien.put(AssujettissementConst.ParamName.CODE_PERSONNE,
                        acquisition.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getPersonne().getCode());

                ConducteurVehicule conducteurVehicule = AssujettissementBusiness.getMotoSousContraitById(
                        acquisition.getBien().getId());

                if (conducteurVehicule != null) {
                    jsonBien.put(AssujettissementConst.ParamName.CONTRAT_ID, conducteurVehicule.getId());
                    jsonBien.put(AssujettissementConst.ParamName.CONTRAT_EXIST, GeneralConst.Number.ONE);

                    String cpiValue = GeneralConst.EMPTY_STRING;

                    Carte carte = TrackingCarteBusiness.getCarteByFkConducteurVehicule(conducteurVehicule.getId());

                    if (carte != null) {
                        jsonBien.put("numeroCpi", carte.getNumeroCpi());
                    } else {
                        jsonBien.put("numeroCpi", GeneralConst.EMPTY_STRING);
                    }

                } else {
                    jsonBien.put(AssujettissementConst.ParamName.CONTRAT_ID, GeneralConst.EMPTY_STRING);
                    jsonBien.put(AssujettissementConst.ParamName.CONTRAT_EXIST, GeneralConst.Number.ZERO);
                    jsonBien.put("numeroCpi", GeneralConst.EMPTY_STRING);
                }

                jsonBiens.add(jsonBien);
            }

            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_BIENS, jsonBiens);
            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_ARTICLE_BUDGETAIRES, jsonAssujettis);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAssujettissement.toString();
    }

}
