/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.PoursuiteBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.*;
import cd.hologram.erecettesvg.constants.AssujettissementConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.*;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "Taxation", urlPatterns = {"/taxation_servlet"})
public class Taxation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    List<Personne> listPersonneAssujetti = new ArrayList<>();
    List<ArticleBudgetaire> listArticleBudgetaire = new ArrayList<>();
    List<Tarif> listTarifs = new ArrayList<>();
    List<NoteCalcul> listNoteCalculs;
    List<Avis> listAvis;
    List<DetailPenaliteMock> listDetailPernalite;
    DetailPernalite detailPernalite;
    String nbreTax = GeneralConst.EMPTY_STRING;
    List<ArchiveAccuseReception> listDeclaration;

    JsonObject jsonObjectDeclaration;
    List<JsonObject> listJsonObjectDeclaration;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(TaxationConst.ParamName.OPERATION);

        switch (operation) {
            case TaxationConst.Operation.RESEARCH_RESPONSIBLE:
                result = loadResponsibles(request);
                break;
            case TaxationConst.Operation.RESEARCH_BUDGET_ARTICLE:
                result = loadBudgetArticle(request);
                break;
            case TaxationConst.Operation.RESEARCH_RATE_BY_ARTICLE_BUDGET:
                result = loadTarifByBudgetArticle(request);
                break;
            case TaxationConst.Operation.SAVE_NOTE_CALCUL:
                result = saveNoteCalcul(request);
                break;
            case "saveNoteCalcul2":
                result = saveNoteCalculConducteurMoto(request);
                break;
            case TaxationConst.Operation.GET_TARIF:
                result = getTarifByCode(request);
                break;
            case TaxationConst.Operation.CHECK_IS_NUMERIC:
                result = checkValuesTaxationIsNumeric(request);
                break;
            case TaxationConst.Operation.RESEARCH_TAXATION:
                result = loadNotePerctionMairie(request);
                break;
            case TaxationConst.Operation.GET_LIST_AVIS:
                result = getListAllAvis(request);
                break;
            case TaxationConst.Operation.CLOSED_TAXATION:
                result = closedNoteTaxation(request);
                break;
            case TaxationConst.Operation.RESEARCH_ADVANCED_TAXATION:
                result = loadTaxationBySearchAvanced(request);
                break;
            case TaxationConst.Operation.ORDONNANCER_TAXATION:
                result = ordonnancerNoteTaxation(request);
                break;
            case TaxationConst.Operation.RESEARCH_IMPOT:
                result = loadImpots(request);
                break;
            case TaxationConst.Operation.RESEARCH_AB_CONDUCTEUR_MOTO:
                result = getListArticleBudgetaireConducteurMoto(request);
                break;
            case TaxationConst.Operation.RESEARCH_TAXATION_REPETITIVE:
                result = getTaxationRepetitive(request);
                break;
            case "loadMotoCycle":
                result = getMotoByPersonne(request);
                break;
            case "updateEcheanePayment":
                result = updateEcheanePayment(request);
                break;
            case "loadBiensTaxation":
                result = loadBiensTaxation(request);
                break;
            case "printDocument":
                result = printerDocumentGenerique(request);
                break;

        }

        out.print(result);
        //System.out.println("response : " + result);
    }

    public String updateEcheanePayment(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String documentCode = request.getParameter("documentCode");
            String reference = request.getParameter("reference");
            String dateEcheance = request.getParameter("dateEcheance");

            Date echeance = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(dateEcheance));

            if (echeance.after(new Date())) {

                dataReturn = "-2";

            } else {

                dateEcheance = ConvertDate.getValidFormatDatePrint(dateEcheance);

                String sqlQuery = GeneralConst.EMPTY_STRING;

                switch (documentCode.trim()) {
                    case "AMR":
                        sqlQuery = "UPDATE T_AMR SET DATE_ECHEANCE = '%s' WHERE NUMERO = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;
                    case "COMMANDEMENT":
                        sqlQuery = "UPDATE T_COMMANDEMENT SET DATE_ECHEANCE = '%s' WHERE ID = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;
                    case "CONTRAINTE":
                        sqlQuery = "UPDATE T_CONTRAINTE SET DATE_ECHEANCE = '%s' WHERE ID = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;
                    case "BP":
                        sqlQuery = "UPDATE T_BON_A_PAYER SET DATE_ECHEANCE = '%s' WHERE CODE = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;
                    case "MED":
                        sqlQuery = "UPDATE T_MED SET DATE_ECHEANCE = '%s' WHERE ID = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;
                    case "NP":
                        sqlQuery = "UPDATE T_NOTE_PERCEPTION SET DATE_ECHEANCE_PAIEMENT = '%s' WHERE NUMERO = '%s'";
                        sqlQuery = String.format(sqlQuery, dateEcheance, reference);
                        break;

                }

                if (TaxationBusiness.updateEcheanePayment(sqlQuery)) {
                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    List<PeriodeDeclaration> listPeriodeDeclarations;

    public String getMotoByPersonne(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");
            List<Bien> biens = new ArrayList<>();

            biens = TaxationBusiness.getListMotoCycleByPersonne(codePersonne);

            if (!biens.isEmpty()) {

                List<JsonObject> bienJsonList = new ArrayList<>();

                for (Bien bien : biens) {

                    JsonObject bienJson = new JsonObject();

                    bienJson.addProperty(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                    bienJson.addProperty(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule());
                    bienJson.addProperty("description", bien.getDescription());
                    bienJson.addProperty("etat", GeneralConst.EMPTY_STRING);

                    if (bien.getFkAdressePersonne() != null) {
                        bienJson.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                bien.getFkAdressePersonne().getAdresse().toString());
                    } else {
                        bienJson.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (!bien.getComplementBienList().isEmpty()) {

                        for (ComplementBien cb : bien.getComplementBienList()) {

                            TypeComplement typeComplement;
                            ValeurPredefinie valeurPredefinie = new ValeurPredefinie();

                            typeComplement = cb.getTypeComplement().getComplement();

                            if (typeComplement != null) {

                                switch (typeComplement.getCode()) {
                                    case "00000000000000062015": // IMMATRICULATION
                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                            bienJson.addProperty("IMMATRICULATION", cb.getValeur());

                                        } else {
                                            bienJson.addProperty("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                                        }

                                        break;
                                    case "00000000000000132015": // COULEUR

                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                            valeurPredefinie = AssujettissementBusiness.getValeurComplement(cb.getValeur());

                                            if (valeurPredefinie != null) {
                                                bienJson.addProperty("COULEUR", valeurPredefinie.getValeur());
                                            } else {
                                                bienJson.addProperty("COULEUR", GeneralConst.EMPTY_STRING);
                                            }

                                        } else {
                                            bienJson.addProperty("COULEUR", GeneralConst.EMPTY_STRING);
                                        }

                                        break;

                                    case "00000000000001482015": // MARQUE

                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                            valeurPredefinie = AssujettissementBusiness.getValeurComplement(cb.getValeur());

                                            if (valeurPredefinie != null) {
                                                bienJson.addProperty("MARQUE", valeurPredefinie.getValeur());
                                            } else {
                                                bienJson.addProperty("MARQUE", GeneralConst.EMPTY_STRING);
                                            }

                                        } else {
                                            bienJson.addProperty("MARQUE", GeneralConst.EMPTY_STRING);
                                        }

                                        break;

                                    case "00000000000001562015": // NUMERO CHASIS

                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                            bienJson.addProperty("NUMEROCHASIS", cb.getValeur());

                                        } else {
                                            bienJson.addProperty("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                                        }

                                        break;
                                    case "00000000000000052015": // CV

                                        if (cb.getValeur() != null && !cb.getValeur().isEmpty()) {

                                            bienJson.addProperty("CV", cb.getValeur());

                                        } else {
                                            bienJson.addProperty("CV", GeneralConst.EMPTY_STRING);
                                        }
                                        break;

                                }

                            } else {
                                bienJson.addProperty("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                                bienJson.addProperty("COULEUR", GeneralConst.EMPTY_STRING);
                                bienJson.addProperty("MARQUE", GeneralConst.EMPTY_STRING);
                                bienJson.addProperty("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                                bienJson.addProperty("CV", GeneralConst.EMPTY_STRING);
                            }
                        }
                    } else {
                        bienJson.addProperty("IMMATRICULATION", GeneralConst.EMPTY_STRING);
                        bienJson.addProperty("COULEUR", GeneralConst.EMPTY_STRING);
                        bienJson.addProperty("MARQUE", GeneralConst.EMPTY_STRING);
                        bienJson.addProperty("NUMEROCHASIS", GeneralConst.EMPTY_STRING);
                        bienJson.addProperty("CV", GeneralConst.EMPTY_STRING);
                    }

                    bienJsonList.add(bienJson);

                }

                dataReturn = bienJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getTaxationRepetitive(HttpServletRequest request) {

        String dataReturn;

        try {

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listPeriodeDeclarationJson = new ArrayList<>();
            List<JsonObject> listAssujettissementJson = new ArrayList<>();

            listPeriodeDeclarations = new ArrayList<>();

            String codeAssujettissement = request.getParameter(TaxationConst.ParamName.ID_ASSUJETTISSEMENT);
            String isCorrection = request.getParameter(TaxationConst.ParamName.IS_CORRECTION);

            Assujeti assujeti = TaxationBusiness.getAssujetiId(codeAssujettissement.trim(),
                    isCorrection.equals(GeneralConst.Number.ONE));

            if (assujeti != null) {

                jsonObject.addProperty(TaxationConst.ParamName.ID_ASSUJETTISSEMENT, assujeti.getId().trim());
                jsonObject.addProperty(TaxationConst.ParamName.NOM_COMPLET_ASSUJETTI,
                        assujeti.getPersonne().toString().toUpperCase().trim());
                jsonObject.addProperty(TaxationConst.ParamName.CODE_RESPONSIBLE,
                        assujeti.getPersonne().getCode().trim());
                jsonObject.addProperty(TaxationConst.ParamName.NIF,
                        assujeti.getPersonne().getNif().trim());
                jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                        assujeti.getPersonne().getFormeJuridique().getCode().trim());
                jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        assujeti.getPersonne().getFormeJuridique().getIntitule().toUpperCase().trim());

                jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                        assujeti.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : assujeti.getPersonne().getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : assujeti.getPersonne().getLoginWeb().getUsername());

                AdressePersonne adressePersonne = assujeti.getFkAdressePersonne();

                jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                        (adressePersonne == null) ? GeneralConst.EMPTY_STRING : adressePersonne.getAdresse().toString().toUpperCase().trim());

                jsonObject.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI,
                        adressePersonne == null ? GeneralConst.EMPTY_STRING : adressePersonne.getAdresse().getId().trim());

                String tarif;

                String codeBudgetaire;
                String codeAB;
                long moisRetard;

                if (!assujeti.getTarif().getIntitule().equalsIgnoreCase(GeneralConst.TOUT) && !assujeti.getTarif().getIntitule().equalsIgnoreCase(GeneralConst.UNIQE)) {
                    tarif = GeneralConst.TWO_POINTS.concat(assujeti.getTarif().getIntitule());

                    if (tarif.equals(GeneralConst.ALL)) {
                        tarif = GeneralConst.EMPTY_STRING;
                    }
                } else {
                    tarif = GeneralConst.EMPTY_STRING;
                }

                if (assujeti.getArticleBudgetaire() != null) {

                    if (assujeti.getArticleBudgetaire().getCodeOfficiel() != null
                            && !assujeti.getArticleBudgetaire().getCodeOfficiel().isEmpty()) {

                        codeBudgetaire = assujeti.getArticleBudgetaire().getCodeOfficiel().toUpperCase();
                    } else {
                        codeBudgetaire = GeneralConst.EMPTY_STRING;
                    }

                } else {
                    codeBudgetaire = GeneralConst.EMPTY_STRING;
                }

                String articleBudgetaire = assujeti.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(tarif);

                codeAB = assujeti.getArticleBudgetaire().getCode();

                if (assujeti.getBien() != null) {

                    String bienValue = "";

                    if (assujeti.getBien().getIntitule() != null) {
                        bienValue = "Panneau/Affiche : ".concat(GeneralConst.SPACE).concat("<span style='color:red;font-style: italic'>".concat(
                                assujeti.getBien().getIntitule().toUpperCase().concat("</span>")));
                    }

                    articleBudgetaire = articleBudgetaire.concat(" " + GeneralConst.BRAKET_OPEN.concat(bienValue).concat(GeneralConst.BRAKET_CLOSE));
                }

                jsonObject.addProperty(TaxationConst.ParamName.CODE_BUDGETAIRE,
                        codeBudgetaire);

                jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                        articleBudgetaire.toUpperCase());

                jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE_NAME,
                        assujeti.getArticleBudgetaire().getPeriodicite().getIntitule().toUpperCase());

                boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                Palier palier = getPalierByAbTarifAndFJuridique(
                        assujeti.getArticleBudgetaire().getCode().trim(),
                        assujeti.getTarif().getCode().trim(),
                        assujeti.getPersonne().getFormeJuridique().getCode().trim(),
                        assujeti.getValeur().floatValue(), isPalier);

                if (palier != null) {

                    jsonObject.addProperty(TaxationConst.ParamName.TAUX_PALIER, palier.getTaux());
                    jsonObject.addProperty(TaxationConst.ParamName.TYPE_TAUX_PALIER, palier.getTypeTaux());
                    jsonObject.addProperty(TaxationConst.ParamName.MULTIPLIER_PAR_VALEUR_BASE, palier.getMultiplierValeurBase());
                    jsonObject.addProperty(TaxationConst.ParamName.DEVIE_PALIER, palier.getDevise().getCode().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.UNITE_PALIER, palier.getUnite().getIntitule());
                    jsonObject.addProperty(TaxationConst.ParamName.VALEUR_BASE, assujeti.getBien() != null
                            ? assujeti.getValeur() : GeneralConst.Numeric.ZERO);

                }

                String periodiciteAb = assujeti.getArticleBudgetaire().getPeriodicite().getCode();

                boolean periodePenalise, periodePaimentPenalise;

                int count = GeneralConst.Numeric.ZERO;

                listPeriodeDeclarations = getListPeriodeDeclarationByAssujettissement(
                        codeAssujettissement, isCorrection.equals(GeneralConst.Number.ONE));

                if (!listPeriodeDeclarations.isEmpty()) {

                    for (PeriodeDeclaration periodeDeclaration : listPeriodeDeclarations) {

                        periodePenalise = false;
                        periodePaimentPenalise = false;

                        JsonObject jsonPeriodeDeclarationObject = new JsonObject();

                        jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.NC_SOURCE, periodeDeclaration.getNoteCalcul() != null
                                ? periodeDeclaration.getNoteCalcul() : GeneralConst.EMPTY_STRING);

                        if (count == 0) {

                            int resultRecidiviste = PoursuiteBusiness.checkRecidivisteDeclaration(
                                    periodeDeclaration.getAssujetissement().getId(), periodeDeclaration.getId());

                            if (resultRecidiviste > GeneralConst.Numeric.ZERO) {
                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_RECIDIVISTE,
                                        GeneralConst.Number.ONE);
                                count++;
                            } else {

                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_RECIDIVISTE,
                                        GeneralConst.Number.ZERO);
                            }

                        } else {

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_RECIDIVISTE,
                                    GeneralConst.Number.ONE);
                        }

                        if (periodeDeclaration.getDateLimite() != null) {

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.ECHEANCE,
                                    Tools.formatDateToString(periodeDeclaration.getDateLimite()));

                            if (Compare.before(periodeDeclaration.getDateLimite(), new Date())) {
                                periodePenalise = true;
                                count++;
                            }

                        } else {

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.ECHEANCE, periodeDeclaration.getFin() == null
                                    ? GeneralConst.EMPTY_STRING : Tools.formatDateToString(periodeDeclaration.getFin()));
                            periodePenalise = false;
                        }

                        moisRetard = 0;

                        if (periodeDeclaration.getDateLimitePaiement() != null) {

                            jsonObject.addProperty(TaxationConst.ParamName.ECHEANCE_PAIEMENT_EXIST,
                                    GeneralConst.Number.ONE);

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.ECHEANCE_PAIEMENT,
                                    Tools.formatDateToString(periodeDeclaration.getDateLimitePaiement()));

                            if (Compare.before(periodeDeclaration.getDateLimitePaiement(), new Date())) {

                                periodePaimentPenalise = true;

                                if (periodeDeclaration.getDateLimitePaiement() != null) {

                                    moisRetard = ConvertDate.getMonthsBetween(periodeDeclaration.getDateLimitePaiement(), new Date());

                                } else {

                                    moisRetard = ConvertDate.getMonthsBetween(periodeDeclaration.getDateLimite(), new Date());

                                }

                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.NBRE_MOIS,
                                        Math.abs(moisRetard));

                            } else {

                                if (Compare.before(periodeDeclaration.getDateLimite(), new Date())) {

                                    jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE_PAIEMENT, GeneralConst.Number.ONE);
                                } else {
                                    jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE_PAIEMENT, GeneralConst.Number.ZERO);
                                    jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, GeneralConst.Numeric.ZERO);
                                }

                            }

                        } else {

                            jsonObject.addProperty(TaxationConst.ParamName.ECHEANCE_PAIEMENT_EXIST,
                                    GeneralConst.Number.ZERO);

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.ECHEANCE_PAIEMENT,
                                    GeneralConst.EMPTY_STRING);

                            String echeanePaiement = GeneralConst.EMPTY_STRING;
                            String toDay = GeneralConst.EMPTY_STRING;

                            if (periodeDeclaration.getDateLimite() != null) {

                                echeanePaiement = ConvertDate.formatDateToStringOfFormat(periodeDeclaration.getDateLimite(), "dd/MM/yyyy");
                                toDay = ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/yyyy");

                                if (echeanePaiement.equals(toDay)) {
                                    moisRetard = 0;
                                } else {
                                    moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanePaiement);
                                }

                            } else {
                                echeanePaiement = ConvertDate.formatDateToStringOfFormat(periodeDeclaration.getFin(), "dd/MM/yyyy");
                                moisRetard = moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanePaiement);
                            }

                            if (moisRetard <= 0) {
                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, 0);
                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE, GeneralConst.Number.ZERO);
                            } else {
                                jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);
                                //jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE, GeneralConst.Number.ONE);
                            }

                        }

                        String libellePeriode = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), periodiciteAb);

                        jsonPeriodeDeclarationObject.addProperty(
                                TaxationConst.ParamName.PERIODE_DECLARATION,
                                libellePeriode);

                        jsonPeriodeDeclarationObject.addProperty(
                                TaxationConst.ParamName.ID_PERIODE_DECLARATION,
                                periodeDeclaration.getId());

                        if (moisRetard > 0) {
                            periodePenalise = true;
                        } else {
                            periodePenalise = false;
                        }

                        if (periodePenalise) {

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE,
                                    GeneralConst.Number.ONE);

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.TEXT_OBSERVATION,
                                    propertiesMessage.getProperty("TXT_OBSERVATION_TAXATION_REPETITIVE"));

                        } else {

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.EST_PENALISE,
                                    GeneralConst.Number.ZERO);

                            jsonPeriodeDeclarationObject.addProperty(TaxationConst.ParamName.TEXT_OBSERVATION,
                                    GeneralConst.EMPTY_STRING);
                        }

                        listPeriodeDeclarationJson.add(jsonPeriodeDeclarationObject);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.PERIODE_DECLARATION_LIST,
                            listPeriodeDeclarationJson.toString());
                } else {
                    jsonObject.addProperty(TaxationConst.ParamName.PERIODE_DECLARATION_LIST,
                            GeneralConst.EMPTY_STRING);
                }

                listAssujettissementJson.add(jsonObject);
                return listAssujettissementJson.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    List<ArticleBudgetaireTaxation> listArticleBudgetaireTaxation = new ArrayList<>();
    HashMap<String, Object[]> bulkQuery = new HashMap<>();
    int counter = 0;

    public String getListArticleBudgetaireConducteurMoto(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        List<JsonObject> listJsonObject = new ArrayList<>();
        List<ArticleBudgetaire> listArticleBudgetaire = new ArrayList<>();

        try {

            String paramSQL = GeneralConst.EMPTY_STRING;
            nbreTax = request.getParameter("nbreTaxe");

            switch (nbreTax) {
                case "1":
                    paramSQL = propertiesConfig.getProperty("SQL_PARAM_AB_CONDUCTEUR_MOTO_WITH_1_TAX");
                    break;
                case "2":
                    paramSQL = propertiesConfig.getProperty("SQL_PARAM_AB_CONDUCTEUR_MOTO_WITH_2_TAX");
                    break;
                case "3":
                    paramSQL = propertiesConfig.getProperty("SQL_PARAM_AB_CONDUCTEUR_MOTO_WITH_3_TAX");
                    break;
            }

            listArticleBudgetaire = TaxationBusiness.getListArticleBudgetaireConducteurMoto(paramSQL);

            if (!listArticleBudgetaire.isEmpty()) {

                for (ArticleBudgetaire ab : listArticleBudgetaire) {

                    JsonObject jsonObject = new JsonObject();

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            ab.getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                            ab.getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getCode().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getIntitule().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_UNITE,
                            ab.getUnite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_UNITE,
                            ab.getUnite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PALIER,
                            ab.getPalier() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_PERIODICITE,
                            ab.getPeriodicite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE,
                            ab.getPeriodicite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.ASSUJETISSABLE,
                            ab.getAssujetissable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_TARIF,
                            ab.getTarif().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.TARIF_VARIABLE,
                            ab.getTarifVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.PROPRIETAIRE,
                            ab.getProprietaire());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL,
                            ab.getTransactionnel());

                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MINIMUM,
                            ab.getDebut());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MAXIMUM,
                            ab.getFin());

                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE_VARIABLE,
                            ab.getPeriodiciteVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.NBRE_JOUR_LIMITE,
                            ab.getNbrJourLimite());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_NATURE,
                            ab.getNature().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.QUANTITE_VARIABLE,
                            ab.getQuantiteVariable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARRETE,
                            ab.getArrete().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_DOCUMENT_OFFICIEL,
                            ab.getCodeOfficiel() != null ? ab.getCodeOfficiel().trim() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(TaxationConst.ParamName.ADD_N_ARTICLE_PANIER,
                            propertiesConfig.getProperty("ACCEPT_N_ARTICLE_PANIER"));

                    if (!ab.getPalierList().isEmpty()) {

                        jsonObject.addProperty(TaxationConst.ParamName.PALIER_EXIST, GeneralConst.Number.ONE);

                        List<JsonObject> jsonPalierList = new ArrayList<>();

                        for (Palier palier : ab.getPalierList()) {

                            JsonObject jsonPalierObj = new JsonObject();

                            jsonPalierObj.addProperty("codeArticleBudgetaire", palier.getArticleBudgetaire().getCode());
                            jsonPalierObj.addProperty("codeTarif", palier.getTarif().getCode());
                            jsonPalierObj.addProperty("libelleTarif", palier.getTarif().getIntitule().toUpperCase());
                            jsonPalierObj.addProperty("taux", palier.getTaux());
                            jsonPalierObj.addProperty("devise", palier.getDevise().getCode());
                            jsonPalierObj.addProperty("typeTaux", palier.getTypeTaux());

                            jsonPalierList.add(jsonPalierObj);
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.PALIER_LIST,
                                jsonPalierList.toString());

                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.PALIER_EXIST, GeneralConst.Number.ZERO);
                        jsonObject.addProperty(TaxationConst.ParamName.PALIER_LIST,
                                GeneralConst.EMPTY_STRING);
                    }

                    listJsonObject.add(jsonObject);

                }

                dataReturn = listJsonObject.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListAllAvis(HttpServletRequest request) {
        try {

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObject = new ArrayList<>();

            listAvis = new ArrayList<>();
            listAvis = cd.hologram.erecettesvg.business.TaxationBusiness.getListAllAvis();

            if (listAvis != null && !listAvis.isEmpty()) {

                for (Avis avis : listAvis) {
                    jsonObject = new JsonObject();

                    jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                            avis.getId().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_AVIS,
                            avis.getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_AVIS,
                            avis.getIntitule().toUpperCase().trim());

                    listJsonObject.add(jsonObject);
                }
            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, Boolean.FALSE);
                listJsonObject.add(jsonObject);
            }

            return listJsonObject.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String getTarifByCode(HttpServletRequest request) {
        String tarifCode, dataReturn = GeneralConst.EMPTY_STRING;
        try {

            tarifCode = request.getParameter(TaxationConst.ParamName.CODE_TARIF);

            if (tarifCode != null || !tarifCode.isEmpty()) {

                Tarif tarif = cd.hologram.erecettesvg.business.TaxationBusiness.getTarifByCode(tarifCode.trim());

                JsonObject jsonObject = new JsonObject();

                if (tarif != null) {
                    jsonObject.addProperty(TaxationConst.ParamName.SESSION, true);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_TARIF, tarif.getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_TARIF, tarif.getIntitule().toUpperCase().trim());
                    dataReturn = jsonObject.toString();
                } else {
                    jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
                    dataReturn = jsonObject.toString();
                }

            } else {
                dataReturn = GeneralConst.EMPTY_STRING;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String checkValuesTaxationIsNumeric(HttpServletRequest request) {

        String base, quantity, dataReturn = GeneralConst.EMPTY_STRING;
        try {

            base = request.getParameter(TaxationConst.ParamName.BASE_CALCUL);
            quantity = request.getParameter(TaxationConst.ParamName.QUANTITY);

            float baseCalcul = Float.valueOf(base);
            float qte = Float.valueOf(quantity);

            dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
        }

        return dataReturn;
    }

    public List<ArchiveData> getArchiveDataList(String userId, String archives, JSONArray jsonDocumentArray) throws IOException {

        String directory = "";//DirectoryData.createDirectoryIfDoesntExist(userId);

        /*if (directory.isEmpty()) {
         return null;
         }*/
        List<ArchiveData> documentList = new ArrayList<>();

        try {

            for (int i = 0; i < jsonDocumentArray.length(); i++) {
                JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                String pvDocument = jsonObject.getString("pvDocument");
                String base64 = pvDocument;
                base64 = base64.split(";")[1].split(",")[1];
                String fileName = null;//Tools.createFileFromBase64(base64, directory);
                ArchiveData archiveData = new ArchiveData();
                archiveData.setData(pvDocument);
                archiveData.setFile(fileName);
                documentList.add(archiveData);
            }
        } catch (JSONException e) {
            return null;
        }
        return documentList;
    }

    public String saveNoteCalcul(HttpServletRequest request) {

        String codeResponsible, codeBien, codeSite, ab, montant, devise,
                userId, dataReturn;

        String noteCalculReturn;

        try {

            codeResponsible = request.getParameter(TaxationConst.ParamName.CODE_RESPONSIBLE);
            codeBien = request.getParameter(TaxationConst.ParamName.CODE_BIEN);
            ab = request.getParameter(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            montant = request.getParameter(TaxationConst.ParamName.AMOUNT);
            devise = request.getParameter(TaxationConst.ParamName.DEVISE);

            dataReturn = TaxationBusiness.createTaxationMairie(codeResponsible, codeBien, ab, montant, devise, codeSite, userId);

            return dataReturn;

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    private List<DetailPenaliteMock> callProcessPenalites(JSONArray jsonPenalite, PeriodeDeclaration pd) throws JSONException {

        List<DetailPenaliteMock> listPenalites = new ArrayList<>();

        for (int i = 0; i < jsonPenalite.length(); i++) {

            JSONObject jsonObjectPenalite = jsonPenalite.getJSONObject(i);

            DetailPenaliteMock detailPenaliteMock = new DetailPenaliteMock();

            detailPenaliteMock.setCodePenalite(jsonObjectPenalite.getString("id"));
            detailPenaliteMock.setTypeTaux(jsonObjectPenalite.getString("type"));
            detailPenaliteMock.setReference(jsonObjectPenalite.getString("referenceNumber"));
            detailPenaliteMock.setNumberMonth(jsonObjectPenalite.getInt("numberMonth"));
            detailPenaliteMock.setTaux(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("rate"))));
            detailPenaliteMock.setAmountPenalite(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("value"))));
            detailPenaliteMock.setAmountPrincipal(BigDecimal.valueOf(Double.valueOf(jsonObjectPenalite.getString("principal"))));
            String periodeName = GeneralConst.EMPTY_STRING;

            if (pd != null) {

                periodeName = Tools.getPeriodeIntitule(pd.getDebut(),
                        pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());

                detailPenaliteMock.setExercice(periodeName);

            } else {
                detailPenaliteMock.setExercice(periodeName);
            }

            listPenalites.add(detailPenaliteMock);

        }

        return listPenalites;
    }

    private void resetBulkQuery() {
        bulkQuery = new HashMap<>();
        counter = 0;
    }

    public String loadTarifByBudgetArticle(HttpServletRequest request) {

        String codeArticle, codeFormeJuridique, baseCalcul, isPalier = GeneralConst.EMPTY_STRING;
        List<Palier> listpaliers = new ArrayList<>();

        try {

            codeArticle = request.getParameter(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            codeFormeJuridique = request.getParameter(TaxationConst.ParamName.CODE_FORME_JURIDIQUE);
            baseCalcul = request.getParameter(TaxationConst.ParamName.BASE_CALCUL);

            baseCalcul = request.getParameter(
                    TaxationConst.ParamName.BASE_CALCUL);

            isPalier = request.getParameter(
                    TaxationConst.ParamName.PALIER);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listpaliers = TaxationBusiness.getListPalierByArticleBudgetaire(codeArticle, codeFormeJuridique, Integer.valueOf(baseCalcul), Integer.valueOf(isPalier));

            for (Palier palier : listpaliers) {

                jsonObject = new JsonObject();

                jsonObject.addProperty(
                        TaxationConst.ParamName.CODE_TARIF, palier.getTarif().getCode().trim());
                jsonObject.addProperty(
                        TaxationConst.ParamName.LIBELLE_TARIF,
                        palier.getTarif().getIntitule().trim());
                jsonObject.addProperty(
                        TaxationConst.ParamName.VALEUR_TARIF, palier.getTarif().getValeur());
                jsonObject.addProperty(
                        TaxationConst.ParamName.TYPE_VALEUR_TARIF, palier.getTarif().getTypeValeur().trim());

                jsonObject.addProperty(
                        TaxationConst.ParamName.CODE_PALIER, palier.getCode());
                jsonObject.addProperty(
                        TaxationConst.ParamName.TYPE_TAUX_PALIER, palier.getTypeTaux());

                jsonObject.addProperty(
                        TaxationConst.ParamName.TAUX_PALIER_TO_FLOAT, palier.getTaux());

                jsonObject.addProperty(
                        TaxationConst.ParamName.TAUX_PALIER, palier.getTaux());

                jsonObject.addProperty(TaxationConst.ParamName.BORNE_INFERIEUR_PALIER,
                        palier.getBorneInferieure());

                jsonObject.addProperty(TaxationConst.ParamName.BORNE_SUPERIEUR_PALIER,
                        palier.getBorneSuperieure());

                jsonObject.addProperty(TaxationConst.ParamName.DEVIE_PALIER,
                        palier.getDevise().getCode().toUpperCase().trim());

                if (palier.getUnite() != null) {

                    jsonObject.addProperty(TaxationConst.ParamName.UNITE_PALIER,
                            palier.getUnite().getCode().trim());

                    Unite unite = TaxationBusiness.getUnitebyCode(palier.getUnite().getCode().trim());

                    if (unite != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.UNITE_LIBELLE_PALIER,
                                unite.getIntitule()
                        );
                    }

                } else {
                    jsonObject.addProperty(TaxationConst.ParamName.UNITE_PALIER,
                            GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(TaxationConst.ParamName.UNITE_LIBELLE_PALIER,
                            GeneralConst.EMPTY_STRING);
                }

                jsonObject.addProperty(TaxationConst.ParamName.MULTIPLIER_PAR_VALEUR_BASE,
                        palier.getMultiplierValeurBase());

                listJsonObjects.add(jsonObject);
            }
            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    String lblArticleBudgetaire = GeneralConst.EMPTY_STRING;
    String lblArticleBudgetaireForDetailNc = GeneralConst.EMPTY_STRING;
    String amount = GeneralConst.EMPTY_STRING;
    float amountToFloat, amountToFloatForDetailNc = 0;
    String lblDevise = GeneralConst.EMPTY_STRING;

    public String loadNotePerctionMairie(HttpServletRequest request) {

        String valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
        String dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
        String dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);

        dateDebut = ConvertDate.getValidFormatDate(dateDebut);
        dateFin = ConvertDate.getValidFormatDate(dateFin);

        List<NotePerceptionMairie> NPs;
        List<JsonObject> listNps = new ArrayList<>();

        try {

            if (!valueSearch.isEmpty()) {

                NPs = TaxationBusiness.getNotePerceptionMairie(valueSearch);

            } else {

                NPs = TaxationBusiness.getNotePerceptionMairie(dateDebut, dateFin);

            }

            NPs.stream().map((NP) -> {

                JsonObject jsonNp = new JsonObject();
                Personne personne = IdentificationBusiness.getPersonneByCode(NP.getFkPersonne());
                jsonNp.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                        personne != null
                                ? personne.toString().toUpperCase().trim()
                                : GeneralConst.EMPTY_STRING);
                jsonNp.addProperty(TaxationConst.ParamName.CODE_RESPONSIBLE,
                        personne != null
                                ? personne.getCode()
                                : GeneralConst.EMPTY_STRING);
                BigDecimal amoutVot1 = NP.getMontant();
                BigDecimal amoutVot2;
                NotePerceptionMairie npm = TaxationBusiness.getNotePerceptionMairieByMere(NP.getId());
                amoutVot2 = npm.getMontant();
                jsonNp.addProperty(TaxationConst.ParamName.AMOUNT_V1, amoutVot1);
                jsonNp.addProperty(TaxationConst.ParamName.AMOUNT_V2, amoutVot2);
                jsonNp.addProperty(TaxationConst.ParamName.DEVISE, NP.getFkDevise());
                jsonNp.addProperty(TaxationConst.ParamName.NUMERO, NP.getNumeroNp());
                jsonNp.addProperty(TaxationConst.ParamName.DATE_CREATE, ConvertDate.formatDateToString(NP.getDateCreate()));
                jsonNp.addProperty(TaxationConst.ParamName.DATE_ECHEANCE, ConvertDate.formatDateToString(NP.getDateEcheance()));
                ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(NP.getFkAb());
                jsonNp.addProperty(TaxationConst.ParamName.ARTICLE_BUDGETAIRE, ab.getIntitule());
                Bien bien = AssujettissementBusiness.getBienByCode(NP.getFkBien());
                jsonNp.addProperty(TaxationConst.ParamName.BIEN, bien.getIntitule());
                return jsonNp;
            }).forEach((jsonNp) -> {
                listNps.add(jsonNp);
            });

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        System.out.println(listNps.toString());

        return listNps.toString();
    }

    public String loadTaxations(HttpServletRequest request) {

        String valueSearch, typeSearch, typeRegister, allSite, allService,
                codeSite, codeService, userId = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            allSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            allService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);

            JsonObject jsonObject = new JsonObject();
            JsonObject jsonObjectDetailNc = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();
            List<JsonObject> listJsonObjectDetailNcs = new ArrayList<>();

            listNoteCalculs = new ArrayList<>();

            listNoteCalculs = getListTaxationNonFiscalNotClotured(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    allSite.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            if (listNoteCalculs != null && !listNoteCalculs.isEmpty()) {

                for (NoteCalcul noteCalcul : listNoteCalculs) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Site site = new Site();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    BienNoteCalcul binBienNoteCalcul = TaxationBusiness.getBienByNoteCalcul(noteCalcul.getNumero());

                    if (binBienNoteCalcul != null) {
                        jsonObject.addProperty("bienTaxationExist", GeneralConst.Numeric.ONE);
                    } else {
                        jsonObject.addProperty("bienTaxationExist", GeneralConst.Numeric.ZERO);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                            noteCalcul.getExercice().trim());

                    if (noteCalcul.getObservation() != null && !noteCalcul.getObservation().isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_TAXATION,
                                noteCalcul.getObservation().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_TAXATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNC(noteCalcul.getNumero());

                    if (complementInfoTaxe != null) {

                        jsonObject.addProperty("complementExist", GeneralConst.Number.ONE);
                        jsonObject.addProperty("transporteur", complementInfoTaxe.getTransporteur().toUpperCase());
                        jsonObject.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                        jsonObject.addProperty("plaque", complementInfoTaxe.getNumeroPlaque().toUpperCase());
                        jsonObject.addProperty("modePaiement", complementInfoTaxe.getModePaiement().toUpperCase());

                    } else {
                        jsonObject.addProperty("complementExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("transporteur", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("produit", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("plaque", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("modePaiement", GeneralConst.EMPTY_STRING);
                    }

                    listDeclaration = TaxationBusiness.getListArchiveDocumentTaxationByNc(
                            noteCalcul.getNumero().trim());

                    if (!listDeclaration.isEmpty()) {

                        jsonObject.addProperty("declarationExist",
                                GeneralConst.Number.ONE);

                        listJsonObjectDeclaration = new ArrayList<>();

                        for (ArchiveAccuseReception at : listDeclaration) {

                            jsonObjectDeclaration = new JsonObject();

                            jsonObjectDeclaration.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT,
                                    at.getArchive());

                            jsonObjectDeclaration.addProperty(TaxationConst.ParamName.NUMERO_NC,
                                    at.getDocumentReference());

                            jsonObjectDeclaration.addProperty("observation",
                                    at.getObservation() == null ? GeneralConst.EMPTY_STRING : at.getObservation().trim());

                            jsonObjectDeclaration.addProperty("libelleDocument", "DOCUMENT TAXATION");

                            listJsonObjectDeclaration.add(jsonObjectDeclaration);
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                listJsonObjectDeclaration.toString());

                    } else {
                        jsonObject.addProperty("declarationExist",
                                GeneralConst.Number.ZERO);
                        jsonObject.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (noteCalcul.getObservationOrdonnancement() != null && !noteCalcul.getObservationOrdonnancement().isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT,
                                noteCalcul.getObservationOrdonnancement().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT,
                                GeneralConst.EMPTY_STRING);
                    }

                    switch (noteCalcul.getEtat()) {
                        case 0:
                            jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                                    TaxationConst.TAXATION_NON_CONFORME);
                            break;
                        case 1:
                            jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                                    TaxationConst.TAXATION_CONFORME);
                            break;
                    }

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(noteCalcul.getDateCreat(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }
                    service = getServiceByCode(noteCalcul.getService().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            noteCalcul.getService().trim());
                    if (service != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.NUMERO_NC,
                            noteCalcul.getNumero().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_RESPONSIBLE,
                            noteCalcul.getPersonne());

                    personne = IdentificationBusiness.getPersonneByCode(noteCalcul.getPersonne().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SITE,
                            noteCalcul.getSite().trim());

                    site = getSiteByCode(noteCalcul.getSite().trim());

                    if (site != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SITE,
                                site.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (noteCalcul.getFkAdressePersonne() != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI,
                                noteCalcul.getFkAdressePersonne().trim());
                        adresse = getAdressByCode(noteCalcul.getFkAdressePersonne().trim());
                        if (adresse != null) {
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                    adresse.toString().toUpperCase().trim());
                        } else {
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                    }

                    lblArticleBudgetaire = GeneralConst.EMPTY_STRING;
                    lblArticleBudgetaireForDetailNc = GeneralConst.EMPTY_STRING;
                    amount = GeneralConst.EMPTY_STRING;
                    lblDevise = GeneralConst.EMPTY_STRING;
                    amountToFloat = 0;
                    amountToFloatForDetailNc = 0;

                    if (noteCalcul.getDetailsNcList() != null && !noteCalcul.getDetailsNcList().isEmpty()) {

                        listJsonObjectDetailNcs = new ArrayList<>();

                        for (DetailsNc detailsNc : noteCalcul.getDetailsNcList()) {

                            if (detailsNc.getDevise() != null) {
                                lblDevise = detailsNc.getDevise().toUpperCase();
                            } else {
                                lblDevise = GeneralConst.Devise.DEVISE_CDF;
                            }

                            String intituleTartif = GeneralConst.EMPTY_STRING;
                            Tarif tarif = new Tarif();
                            jsonObjectDetailNc = new JsonObject();

                            tarif = cd.hologram.erecettesvg.business.TaxationBusiness.getTarifByCode(detailsNc.getTarif().trim());

                            if (tarif != null) {
                                intituleTartif = tarif.getIntitule().toUpperCase().trim();
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                    detailsNc.getArticleBudgetaire().getCode().trim());

                            lblArticleBudgetaireForDetailNc = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                    " : ").concat(intituleTartif);

                            amountToFloatForDetailNc = detailsNc.getMontantDu().floatValue();

                            if (noteCalcul.getDetailsNcList().size() == 1) {
                                amountToFloat = detailsNc.getMontantDu().floatValue();

                                lblArticleBudgetaire
                                        = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                                " : ").concat(intituleTartif);

                            } else if (noteCalcul.getDetailsNcList().size() > 1) {

                                amountToFloat += detailsNc.getMontantDu().floatValue();

                                if (lblArticleBudgetaire.isEmpty()) {
                                    lblArticleBudgetaire
                                            = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                                    " : ").concat(intituleTartif);
                                } else {
                                    lblArticleBudgetaire += lblArticleBudgetaire = "; ".concat(detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                            " : ").concat(intituleTartif));
                                }
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                    lblArticleBudgetaireForDetailNc);

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_TARIF,
                                    detailsNc.getTarif().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.BASE_CALCUL,
                                    Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                            detailsNc.getValeurBase()).concat(GeneralConst.Format.ZERO_FORMAT));

                            if (detailsNc.getTypeTaux() != null && !detailsNc.getTypeTaux().isEmpty()) {

                                switch (detailsNc.getTypeTaux().trim()) {
                                    case "F":
                                        jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                                Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                        detailsNc.getTauxArticleBudgetaire()).concat(
                                                        GeneralConst.SPACE));
                                        break;
                                    case "%":
                                        jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                                Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                        detailsNc.getTauxArticleBudgetaire()).concat(
                                                        GeneralConst.SPACE).concat("(".concat(
                                                                detailsNc.getTypeTaux().trim()).concat(")")));
                                        break;
                                }
                            } else {
                                jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                        Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                detailsNc.getTauxArticleBudgetaire()).concat(
                                                GeneralConst.SPACE));
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TYPE_TAUX_PALIER,
                                    detailsNc.getTypeTaux().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.QUANTITY,
                                    detailsNc.getQte());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_UNITE,
                                    detailsNc.getArticleBudgetaire().getUnite().getCode().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.LIBELLE_UNITE,
                                    detailsNc.getArticleBudgetaire().getUnite().getIntitule().toUpperCase().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.AMOUNT, amountToFloatForDetailNc);
                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.DEVIE_PALIER, lblDevise);

                            listJsonObjectDetailNcs.add(jsonObjectDetailNc);
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.LIST_DETAIL_NC,
                                listJsonObjectDetailNcs.toString());
                    }

                    float totalAmount = getMontantTotalDuDetailNoteCalcul(noteCalcul.getNumero().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.TOTAL_AMOUNT2, totalAmount);

                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, lblArticleBudgetaire);
                    jsonObject.addProperty(TaxationConst.ParamName.AMOUNT, amountToFloat);
                    jsonObject.addProperty(TaxationConst.ParamName.DEVIE_PALIER, lblDevise);

                    Agent agent = new Agent();
                    String dateTraitement = GeneralConst.EMPTY_STRING;

                    agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentCreat());
                    dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateCreat());

                    jsonObject.addProperty("agentTaxateur", agent != null
                            ? agent.toString().toUpperCase().concat(
                                    GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                            : GeneralConst.EMPTY_STRING);

                    if (noteCalcul.getDateCloture() != null) {

                        dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateCloture());

                        agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentCloture());

                        jsonObject.addProperty("agentCloture", agent != null
                                ? agent.toString().toUpperCase().concat(
                                        GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                                : GeneralConst.EMPTY_STRING);

                    } else {
                        jsonObject.addProperty("agentCloture", "NON CLÔTUREE");
                    }

                    if (noteCalcul.getDateValidation() != null) {

                        dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateValidation());

                        agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentValidation());

                        jsonObject.addProperty("agentOrdonnateur", agent != null
                                ? agent.toString().toUpperCase().concat(
                                        GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                                : GeneralConst.EMPTY_STRING);

                    } else {
                        jsonObject.addProperty("agentOrdonnateur", "NON ORDONNANCEE");
                    }

                    listJsonObjects.add(jsonObject);

                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String loadTaxationBySearchAvanced(HttpServletRequest request) {

        String codeSite, codeService, dateDebut, dateFin, typeRegister = GeneralConst.EMPTY_STRING;

        lblArticleBudgetaire = GeneralConst.EMPTY_STRING;
        lblArticleBudgetaireForDetailNc = GeneralConst.EMPTY_STRING;
        amount = GeneralConst.EMPTY_STRING;
        amountToFloat = 0;
        amountToFloatForDetailNc = 0;
        lblDevise = GeneralConst.EMPTY_STRING;

        try {

            codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            codeService = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SERVICE_CODE);
            dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

            JsonObject jsonObject = new JsonObject();
            JsonObject jsonObjectDetailNc = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();
            List<JsonObject> listJsonObjectDetailNcs = new ArrayList<>();

            listNoteCalculs = new ArrayList<>();

            listNoteCalculs = getListTaxationNonFiscalNotCloturedBySearchAvanced(
                    codeSite.trim(),
                    codeService,
                    dateDebut, dateFin, typeRegister);

            if (!listNoteCalculs.isEmpty()) {

                for (NoteCalcul noteCalcul : listNoteCalculs) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Site site = new Site();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    BienNoteCalcul binBienNoteCalcul = TaxationBusiness.getBienByNoteCalcul(noteCalcul.getNumero());

                    if (binBienNoteCalcul != null) {
                        jsonObject.addProperty("bienTaxationExist", GeneralConst.Numeric.ONE);
                    } else {
                        jsonObject.addProperty("bienTaxationExist", GeneralConst.Numeric.ZERO);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                            noteCalcul.getExercice().trim());

                    if (noteCalcul.getObservation() != null && !noteCalcul.getObservation().isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_TAXATION,
                                noteCalcul.getObservation().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_TAXATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNC(noteCalcul.getNumero());

                    if (complementInfoTaxe != null) {

                        jsonObject.addProperty("complementExist", GeneralConst.Number.ONE);
                        jsonObject.addProperty("transporteur", complementInfoTaxe.getTransporteur().toUpperCase());
                        jsonObject.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                        jsonObject.addProperty("plaque", complementInfoTaxe.getNumeroPlaque().toUpperCase());
                        jsonObject.addProperty("modePaiement", complementInfoTaxe.getModePaiement().toUpperCase());

                    } else {
                        jsonObject.addProperty("complementExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("transporteur", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("produit", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("plaque", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("modePaiement", GeneralConst.EMPTY_STRING);
                    }

                    listDeclaration = TaxationBusiness.getListArchiveDocumentTaxationByNc(
                            noteCalcul.getNumero().trim());

                    if (!listDeclaration.isEmpty()) {

                        jsonObject.addProperty("declarationExist",
                                GeneralConst.Number.ONE);

                        listJsonObjectDeclaration = new ArrayList<>();

                        for (ArchiveAccuseReception at : listDeclaration) {

                            jsonObjectDeclaration = new JsonObject();

                            jsonObjectDeclaration.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT,
                                    at.getArchive());

                            jsonObjectDeclaration.addProperty(TaxationConst.ParamName.NUMERO_NC,
                                    at.getDocumentReference());

                            jsonObjectDeclaration.addProperty("observation",
                                    at.getObservation() == null ? GeneralConst.EMPTY_STRING : at.getObservation().trim());

                            jsonObjectDeclaration.addProperty("libelleDocument", "DOCUMENT TAXATION");

                            listJsonObjectDeclaration.add(jsonObjectDeclaration);
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                listJsonObjectDeclaration.toString());

                    } else {
                        jsonObject.addProperty("declarationExist",
                                GeneralConst.Number.ZERO);
                        jsonObject.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (noteCalcul.getObservationOrdonnancement() != null && !noteCalcul.getObservationOrdonnancement().isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT,
                                noteCalcul.getObservationOrdonnancement().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT,
                                GeneralConst.EMPTY_STRING);
                    }

                    switch (noteCalcul.getEtat()) {
                        case 0:
                            jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                                    TaxationConst.TAXATION_NON_CONFORME);
                            break;
                        case 1:
                            jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                                    TaxationConst.TAXATION_CONFORME);
                            break;
                    }

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(noteCalcul.getDateCreat(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }
                    service = getServiceByCode(noteCalcul.getService().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            noteCalcul.getService().trim());
                    if (service != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.NUMERO_NC,
                            noteCalcul.getNumero().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_RESPONSIBLE,
                            noteCalcul.getPersonne());

                    personne = IdentificationBusiness.getPersonneByCode(noteCalcul.getPersonne().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SITE,
                            noteCalcul.getSite().trim());

                    site = getSiteByCode(noteCalcul.getSite().trim());

                    if (site != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SITE,
                                site.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (noteCalcul.getFkAdressePersonne() != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI,
                                noteCalcul.getFkAdressePersonne().trim());
                        adresse = getAdressByCode(noteCalcul.getFkAdressePersonne().trim());
                        if (adresse != null) {
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                    adresse.toString().toUpperCase().trim());
                        } else {
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (noteCalcul.getDetailsNcList() != null && !noteCalcul.getDetailsNcList().isEmpty()) {

                        listJsonObjectDetailNcs = new ArrayList<>();

                        for (DetailsNc detailsNc : noteCalcul.getDetailsNcList()) {

                            if (detailsNc.getDevise() != null) {
                                lblDevise = detailsNc.getDevise().toUpperCase();
                            } else {
                                lblDevise = GeneralConst.Devise.DEVISE_CDF;
                            }

                            String intituleTartif = GeneralConst.EMPTY_STRING;
                            Tarif tarif = TaxationBusiness.getTarifByCode(detailsNc.getTarif().trim());
                            jsonObjectDetailNc = new JsonObject();

                            if (tarif != null) {
                                intituleTartif = tarif.getIntitule().toUpperCase().trim();
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.AMOUNT, amountToFloat);
                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.DEVIE_PALIER, lblDevise);

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                    detailsNc.getArticleBudgetaire().getCode().trim());

                            lblArticleBudgetaireForDetailNc = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                    " : ").concat(intituleTartif);

                            amountToFloatForDetailNc = detailsNc.getMontantDu().floatValue();

                            if (noteCalcul.getDetailsNcList().size() == 1) {
                                amountToFloat = detailsNc.getMontantDu().floatValue();

                                lblArticleBudgetaire
                                        = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                                " : ").concat(intituleTartif);

                            } else if (noteCalcul.getDetailsNcList().size() > 1) {

                                amountToFloat += detailsNc.getMontantDu().floatValue();

                                if (lblArticleBudgetaire.isEmpty()) {
                                    lblArticleBudgetaire
                                            = detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                                    " : ").concat(intituleTartif);
                                } else {
                                    lblArticleBudgetaire += lblArticleBudgetaire = "; ".concat(detailsNc.getArticleBudgetaire().getIntitule().toUpperCase().trim().concat(
                                            " : ").concat(intituleTartif));
                                }
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                    lblArticleBudgetaireForDetailNc);
                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.AMOUNT, amountToFloatForDetailNc);

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_TARIF,
                                    detailsNc.getTarif().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.BASE_CALCUL,
                                    Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                            detailsNc.getValeurBase()).concat(GeneralConst.Format.ZERO_FORMAT));

                            if (detailsNc.getTypeTaux() != null && !detailsNc.getTypeTaux().isEmpty()) {

                                switch (detailsNc.getTypeTaux().trim()) {
                                    case "F":
                                        jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                                Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                        detailsNc.getTauxArticleBudgetaire()).concat(
                                                        GeneralConst.SPACE));
                                        break;
                                    case "%":
                                        jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                                Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                        detailsNc.getTauxArticleBudgetaire()).concat(
                                                        GeneralConst.SPACE).concat("(".concat(
                                                                detailsNc.getTypeTaux().trim()).concat(")")));
                                        break;
                                }
                            } else {
                                jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TAUX_PALIER,
                                        Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                                detailsNc.getTauxArticleBudgetaire()).concat(
                                                GeneralConst.SPACE));
                            }

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.TYPE_TAUX_PALIER,
                                    detailsNc.getTypeTaux().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.QUANTITY,
                                    detailsNc.getQte());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.CODE_UNITE,
                                    detailsNc.getArticleBudgetaire().getUnite().getCode().trim());

                            jsonObjectDetailNc.addProperty(TaxationConst.ParamName.LIBELLE_UNITE,
                                    detailsNc.getArticleBudgetaire().getUnite().getIntitule().toUpperCase().trim());

                            listJsonObjectDetailNcs.add(jsonObjectDetailNc);
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.LIST_DETAIL_NC,
                                listJsonObjectDetailNcs.toString());
                    }

                    float totalAmount = getMontantTotalDuDetailNoteCalcul(noteCalcul.getNumero().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.TOTAL_AMOUNT2, totalAmount);
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, lblArticleBudgetaire);
                    jsonObject.addProperty(TaxationConst.ParamName.AMOUNT, amountToFloat);
                    jsonObject.addProperty(TaxationConst.ParamName.DEVIE_PALIER, lblDevise);

                    Agent agent = new Agent();
                    String dateTraitement = GeneralConst.EMPTY_STRING;

                    dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateCreat());

                    agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentCreat());

                    jsonObject.addProperty("agentTaxateur", agent != null
                            ? agent.toString().toUpperCase().concat(
                                    GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                            : GeneralConst.EMPTY_STRING);

                    if (noteCalcul.getDateCloture() != null) {

                        dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateCloture());

                        agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentCloture());

                        jsonObject.addProperty("agentCloture", agent != null
                                ? agent.toString().toUpperCase().concat(
                                        GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                                : GeneralConst.EMPTY_STRING);

                    } else {
                        jsonObject.addProperty("agentCloture", "NON CLÔTUREE");
                    }

                    if (noteCalcul.getDateValidation() != null) {

                        dateTraitement = ConvertDate.formatDateHeureToString(noteCalcul.getDateValidation());

                        agent = GeneralBusiness.getAgentByCode(noteCalcul.getAgentValidation());

                        jsonObject.addProperty("agentOrdonnateur", agent != null
                                ? agent.toString().toUpperCase().concat(
                                        GeneralConst.SPACE).concat("(Le " + dateTraitement + ")")
                                : GeneralConst.EMPTY_STRING);

                    } else {
                        jsonObject.addProperty("agentOrdonnateur", "NON ORDONNANCEE");
                    }

                    listJsonObjects.add(jsonObject);

                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String loadResponsibles(HttpServletRequest request) {

        String valueSearch, typeSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            if (valueSearch != null && !valueSearch.isEmpty()) {

                listPersonneAssujetti = getListAssujettisByCriterion(valueSearch.trim());

                if (listPersonneAssujetti != null && !listPersonneAssujetti.isEmpty()) {

                    for (Personne personne : listPersonneAssujetti) {

                        jsonObject = new JsonObject();

                        jsonObject.addProperty(TaxationConst.ParamName.CODE, personne.getCode().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.NIF, personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_COMPLET_ASSUJETTI, personne.toString().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());

                        if (personne.getLoginWeb() != null) {

                            jsonObject.addProperty("codeNTD", personne.getLoginWeb().getUsername() == null ? "" : personne.getLoginWeb().getUsername());

                        } else {
                            jsonObject.addProperty("codeNTD", GeneralConst.EMPTY_STRING);
                        }

                        Adresse adresse = getAdresseDefaultByAssujetti(personne.getCode().trim());

                        if (adresse != null) {
                            jsonObject.addProperty(TaxationConst.ParamName.ID_ADRESSE_ASSUJETTI, adresse.getId().trim());
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, adresse.toString().toUpperCase().trim());
                        } else {
                            jsonObject.addProperty(TaxationConst.ParamName.ID_ADRESSE_ASSUJETTI, GeneralConst.EMPTY_STRING);
                            jsonObject.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, GeneralConst.ADRESS_NOTHING);
                        }

                        listJsonObjects.add(jsonObject);
                    }

                } else {
                    return GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                jsonObject.addProperty(TaxationConst.ParamName.SESSION, false);
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String loadBudgetArticle(HttpServletRequest request) {

        String valueSearch, seeEverything, codeService = GeneralConst.EMPTY_STRING;

        listArticleBudgetaire = new ArrayList<>();

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            seeEverything = request.getParameter(TaxationConst.ParamName.SEE_EVERYTHING);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listArticleBudgetaire = getListArticleBudgetaires(
                    valueSearch.trim(),
                    seeEverything,
                    codeService.trim());

            if (listArticleBudgetaire != null && !listArticleBudgetaire.isEmpty()) {

                for (ArticleBudgetaire ab : listArticleBudgetaire) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            ab.getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                            ab.getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getCode().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getIntitule().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_UNITE,
                            ab.getUnite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_UNITE,
                            ab.getUnite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PALIER,
                            ab.getPalier() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_PERIODICITE,
                            ab.getPeriodicite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE,
                            ab.getPeriodicite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.ASSUJETISSABLE,
                            ab.getAssujetissable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_TARIF,
                            ab.getTarif().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.TARIF_VARIABLE,
                            ab.getTarifVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.PROPRIETAIRE,
                            ab.getProprietaire());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL,
                            ab.getTransactionnel());

                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MINIMUM,
                            ab.getDebut());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MAXIMUM,
                            ab.getFin());

                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE_VARIABLE,
                            ab.getPeriodiciteVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.NBRE_JOUR_LIMITE,
                            ab.getNbrJourLimite());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_NATURE,
                            ab.getNature().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.QUANTITE_VARIABLE,
                            ab.getQuantiteVariable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARRETE,
                            ab.getArrete().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_DOCUMENT_OFFICIEL,
                            ab.getCodeOfficiel() != null ? ab.getCodeOfficiel().trim() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(TaxationConst.ParamName.ADD_N_ARTICLE_PANIER,
                            propertiesConfig.getProperty("ACCEPT_N_ARTICLE_PANIER"));
                    listJsonObjects.add(jsonObject);
                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String ordonnancerNoteTaxation(HttpServletRequest request) {

        String noteCalcul, netApayer, exerciceFiscal, codeSite, codeService, userId, dateCreate = GeneralConst.EMPTY_STRING;

        String idAvis, observation, codeResponsible, dateValidation,
                useNpGenerate, dataReturn = GeneralConst.EMPTY_STRING;

        try {

            noteCalcul = request.getParameter(TaxationConst.ParamName.NUMERO_NC);
            netApayer = request.getParameter(TaxationConst.ParamName.NET_A_PAYE);
            //remise = request.getParameter(TaxationConst.ParamName.REMISE);
            exerciceFiscal = request.getParameter(TaxationConst.ParamName.EXERCICE_FISCAL);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            //notePerceptionManuel = request.getParameter(TaxationConst.ParamName.NP_MANUEL);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            dateCreate = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
            idAvis = request.getParameter(TaxationConst.ParamName.ID_AVIS);
            observation = request.getParameter(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT);
            codeResponsible = request.getParameter(TaxationConst.ParamName.CODE_RESPONSIBLE);
            useNpGenerate = request.getParameter(TaxationConst.ParamName.USE_NP_GENERATE);
            dateValidation = request.getParameter(TaxationConst.ParamName.DATE_VALIDATION);

            NotePerception np = new NotePerception();
            NoteCalcul nc = new NoteCalcul();
            Serie newSerie = new Serie();
            String dateOrdonnancement;

            if (!dateValidation.isEmpty()) {
                dateOrdonnancement = ConvertDate.getValidFormatDatePrint(dateValidation);
            } else {
                dateOrdonnancement = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
            }

            float netAPayeToFloat = Float.valueOf(netApayer);

            nc.setNumero(noteCalcul.trim());
            nc.setAgentValidation(userId);
            nc.setAvis(idAvis.trim());
            nc.setObservationOrdonnancement(observation);
            nc.setExercice(exerciceFiscal.trim());
            nc.setPersonne(codeResponsible.trim());

            int etatOrdonnancement = 0;

            np.setNoteCalcul(nc);
            np.setSite(codeSite.trim());
            np.setDateEcheancePaiement(GeneralConst.EMPTY_STRING);
            np.setAgentCreat(userId);
            np.setDateCreat(dateCreate);
            np.setAmr(GeneralConst.EMPTY_STRING);
            np.setFractionnee(Short.valueOf(GeneralConst.Number.ZERO));

            //here
            String npReturn = saveNotePerceptionV3(np,
                    useNpGenerate.equals(GeneralConst.Boolean.TRUE),
                    newSerie, codeSite.trim(), codeService.trim(),
                    netAPayeToFloat, dateOrdonnancement, etatOrdonnancement, idAvis);

            if (!npReturn.isEmpty()) {
                dataReturn = npReturn;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String closedNoteTaxation(HttpServletRequest request) {

        String codeAvis, noteTaxation, idUser, observation, dataReturn = GeneralConst.EMPTY_STRING;

        try {
            codeAvis = request.getParameter(TaxationConst.ParamName.ID_AVIS);
            noteTaxation = request.getParameter(TaxationConst.ParamName.NUMERO_NC);
            idUser = request.getParameter(TaxationConst.ParamName.USER_ID);
            observation = request.getParameter(TaxationConst.ParamName.OBSERVATION_TAXATION);

            Avis avis = getAvisById(codeAvis.trim());
            int etatTaxation = 0;

            if (avis != null) {

                switch (avis.getCode().trim()) {
                    case TaxationConst.OK:
                        etatTaxation = 1;
                        break;
                    case TaxationConst.KO:
                        etatTaxation = 0;
                        break;
                }
            }

            boolean result = closeNoteCalcul(noteTaxation.trim(), etatTaxation, idUser, observation);

            if (result) {
                switch (etatTaxation) {
                    case 0:
                        dataReturn = GeneralConst.ResultCode.NC_NON_CONFORME_SUCCESS_OPERATION;
                        break;
                    case 1:
                        dataReturn = GeneralConst.ResultCode.NC_CONFORME_SUCCESS_OPERATION;
                        break;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static Serie getNexSerie(Serie serie) {

        String debutNumero = GeneralConst.Number.ONE;
        Integer debutInteger = 0;
        String generateNumber;
        String prefix;

        Serie newSerie = new Serie(serie.getId());

        try {

            prefix = serie.getAnnee() + serie.getPrefix();

            if (serie.getDernierNumeroImprime() != null) {

                if (!serie.getDernierNumeroImprime().equals(GeneralConst.EMPTY_STRING)) {

                    String lastGenerateNumber = serie.getDernierNumeroImprime();

                    debutNumero = StringUtils.remove(lastGenerateNumber, prefix).substring(0, 5);

                    debutInteger = Integer.valueOf(debutNumero);

                    debutInteger++;

                    debutNumero = debutInteger.toString();

                }
            }

            int mod = Integer.valueOf(debutNumero) % 66;

            if (debutNumero.length() < 5) {

                String val1 = StringUtils.leftPad(debutNumero, 5, GeneralConst.Number.ZERO);

                generateNumber = prefix + val1.concat(String.valueOf(mod));

            } else {
                generateNumber = prefix + debutNumero.concat(String.valueOf(mod));
            }

            if (!generateNumber.equals(GeneralConst.EMPTY_STRING)) {

                Integer nbrePrint = 0;
                if (serie.getNombreNoteImprimee() != null) {
                    nbrePrint = serie.getNombreNoteImprimee();
                }
                nbrePrint++;
                newSerie.setNombreNoteImprimee(nbrePrint++);
                newSerie.setDernierNumeroImprime(generateNumber);
            }

        } catch (Exception e) {
            throw e;
        }

        return newSerie;
    }

    public String loadImpots(HttpServletRequest request) {

        String valueSearch, codeBancaire = GeneralConst.EMPTY_STRING;

        listArticleBudgetaire = new ArrayList<>();

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            codeBancaire = request.getParameter(TaxationConst.ParamName.COMPTE_BANCAIRE);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listArticleBudgetaire = getListImpot(
                    valueSearch.trim(),
                    codeBancaire.trim());

            if (listArticleBudgetaire != null && !listArticleBudgetaire.isEmpty()) {

                for (ArticleBudgetaire ab : listArticleBudgetaire) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            ab.getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                            ab.getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ACTE_GENERATEUR,
                            ab.getArticleGenerique().getIntitule().toUpperCase().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getCode().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                            ab.getArticleGenerique().getServiceAssiette().getIntitule().toUpperCase().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_UNITE,
                            ab.getUnite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_UNITE,
                            ab.getUnite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PALIER,
                            ab.getPalier() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_PERIODICITE,
                            ab.getPeriodicite().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE,
                            ab.getPeriodicite().getIntitule().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.ASSUJETISSABLE,
                            ab.getAssujetissable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_TARIF,
                            ab.getTarif().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.TARIF_VARIABLE,
                            ab.getTarifVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.PROPRIETAIRE,
                            ab.getProprietaire());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL,
                            ab.getTransactionnel());

                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MINIMUM,
                            ab.getDebut());
                    jsonObject.addProperty(TaxationConst.ParamName.TRANSACTIONNEL_MAXIMUM,
                            ab.getFin());

                    jsonObject.addProperty(TaxationConst.ParamName.PERIODICITE_VARIABLE,
                            ab.getPeriodiciteVariable());
                    jsonObject.addProperty(TaxationConst.ParamName.NBRE_JOUR_LIMITE,
                            ab.getNbrJourLimite());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_NATURE,
                            ab.getNature().getCode().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.QUANTITE_VARIABLE,
                            ab.getQuantiteVariable() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_ARRETE,
                            ab.getArrete().trim());
                    jsonObject.addProperty(TaxationConst.ParamName.CODE_DOCUMENT_OFFICIEL,
                            ab.getCodeOfficiel() != null ? ab.getCodeOfficiel().trim() : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(TaxationConst.ParamName.ADD_N_ARTICLE_PANIER,
                            propertiesConfig.getProperty("ACCEPT_N_ARTICLE_PANIER"));
                    listJsonObjects.add(jsonObject);
                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String saveNoteCalculConducteurMoto(HttpServletRequest request) {

        String codeResponsible, codeBien, codeAdresse, exerciceFiscal, codeSite, codeService,
                userId, dateCreate = GeneralConst.EMPTY_STRING;

        String cpiCodeReturn = GeneralConst.EMPTY_STRING;
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            codeResponsible = request.getParameter(TaxationConst.ParamName.CODE_RESPONSIBLE);
            codeAdresse = request.getParameter(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            codeBien = request.getParameter(TaxationConst.ParamName.CODE_BIEN);

            String nbreTaxe = request.getParameter("nbreTaxe");
            BigDecimal amountTaxation = new BigDecimal("0");

            switch (nbreTaxe) {
                case "1":
                    amountTaxation = new BigDecimal(propertiesConfig.getProperty("AMOUNT_WITH_1_TAX"));
                    break;
                case "2":
                    amountTaxation = new BigDecimal(propertiesConfig.getProperty("AMOUNT_WITH_2_TAX"));
                    break;
                case "3":
                    amountTaxation = new BigDecimal(propertiesConfig.getProperty("AMOUNT_WITH_3_TAX"));
                    break;
            }

            exerciceFiscal = getCurrentExerciceFiscal();

            NewNoteCalcul newNoteCalcul = new NewNoteCalcul();

            newNoteCalcul.setCodePersonne(codeResponsible.trim());
            newNoteCalcul.setCodeAdresse(codeAdresse.trim());
            newNoteCalcul.setCodeService(codeService.trim());
            newNoteCalcul.setCodeSite(codeSite.trim());
            newNoteCalcul.setAgentCreate(userId);
            dateCreate = ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/yyyy");
            newNoteCalcul.setDateCreate(dateCreate);
            newNoteCalcul.setCodeDepot(null);
            newNoteCalcul.setCodeBien(codeBien);
            newNoteCalcul.setExerciceFiscal(exerciceFiscal);

            cpiCodeReturn = TaxationBusiness.saveNoteCalculWithShortProcesure(newNoteCalcul, Integer.valueOf(nbreTaxe), amountTaxation);

            if (!cpiCodeReturn.isEmpty()) {

                cd.hologram.erecettesvg.models.AcquitLiberatoire cpi = new cd.hologram.erecettesvg.models.AcquitLiberatoire();

                cpi = AcquitLiberatoireBusiness.getAcquitLiberatoireByNumero(cpiCodeReturn);

                if (cpi != null) {

                    PrintDocument printDocument = new PrintDocument();

                    List<DetailPaiement> listDetailPaiements = new ArrayList<>();

                    List<SuiviComptableDeclaration> listSuiviComptableDeclarations
                            = AcquitLiberatoireBusiness.getListSuiviComptableDeclarationByNc(
                                    cpi.getNotePerception().getNoteCalcul().getNumero().trim());

                    int partInteger, partVirgule;

                    String numberConvert = GeneralConst.EMPTY_STRING,
                            detailPaiementCpi = GeneralConst.EMPTY_STRING = GeneralConst.EMPTY_STRING;

                    boolean virguleExist = false;
                    BigDecimal amountPaid = new BigDecimal("0");

                    if (!listSuiviComptableDeclarations.isEmpty()) {

                        for (SuiviComptableDeclaration scd : listSuiviComptableDeclarations) {

                            if (scd.getCredit().floatValue() > 0) {

                                Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(scd.getDocumentReference().trim());

                                DetailPaiement detailPaiement = new DetailPaiement();

                                if (journal != null) {

                                    amountPaid = amountPaid.add(journal.getMontantPercu());

                                    if (journal.getTypeDocument().trim().equals(propertiesConfig.getProperty("BY_NP"))
                                            || journal.getTypeDocument().trim().equals(propertiesConfig.getProperty("BY_DEC"))) {
                                        detailPaiement.setRubrique(propertiesConfig.getProperty("TXT_PRINCIPAL"));
                                    } else {
                                        detailPaiement.setRubrique(propertiesConfig.getProperty("TXT_PENALITE"));
                                    }

                                    detailPaiement.setNumeroPiece(journal.getNumeroAttestationPaiement() != null ? journal.getNumeroAttestationPaiement().trim() : GeneralConst.EMPTY_STRING);
                                    detailPaiement.setDatePaiement(ConvertDate.formatDateToStringOfFormat(journal.getDateCreat(), GeneralConst.Format.FORMAT_DATE));
                                }

                                detailPaiement.setReference(scd.getDocumentReference().trim());
                                detailPaiement.setCompteBancaire(journal == null ? GeneralConst.EMPTY_STRING : journal.getCompteBancaire().getIntitule().toUpperCase());
                                partInteger = 0;
                                partVirgule = 0;

                                numberConvert = String.valueOf(scd.getCredit());
                                numberConvert = numberConvert.replace(".", ",");

                                if (numberConvert != null && !numberConvert.isEmpty()) {
                                    if (numberConvert.contains(",")) {
                                        String[] credi = numberConvert.split(",");
                                        partInteger = Integer.valueOf(String.valueOf(credi[0]));
                                        partVirgule = Integer.valueOf(String.valueOf(credi[1]));
                                    }
                                }

                                if (partInteger > 0) {
                                    virguleExist = partVirgule > 0;
                                } else {
                                    virguleExist = false;
                                }

                                if (virguleExist) {
                                    detailPaiement.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                            BigDecimal.valueOf(Double.valueOf(String.valueOf(scd.getCredit()))))
                                            + GeneralConst.SPACE + scd.getDevise().trim());
                                } else {
                                    detailPaiement.setMontantPayer(Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                            BigDecimal.valueOf(Double.valueOf(String.valueOf(scd.getCredit()))))
                                            + GeneralConst.Number.ZERO_FORMAT
                                            + GeneralConst.SPACE + scd.getDevise().trim());
                                }

                                detailPaiement.setMontantTotaux(scd.getCredit().floatValue());
                                listDetailPaiements.add(detailPaiement);
                            }

                        }

                        detailPaiementCpi = Tools.getTableHtmlV(listDetailPaiements);
                    }

                    Agent agent = GeneralBusiness.getAgentByCode(userId);

                    dataReturn = printDocument.createCPI(cpi, agent.toString().toUpperCase(), "", "",
                            Double.valueOf(String.valueOf(amountPaid.floatValue())), detailPaiementCpi);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return dataReturn;

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public void createPeriodesDeclarationsV2(int idPeriode) {

        String result;

        try {

            String date = GeneralConst.EMPTY_STRING;
            String codePeriodicite = GeneralConst.EMPTY_STRING;
            String nombreJour = GeneralConst.EMPTY_STRING;
            String nombreJourLimite = GeneralConst.EMPTY_STRING;
            String echeanceLegale = GeneralConst.EMPTY_STRING;
            String periodeEcheance = GeneralConst.EMPTY_STRING;
            String nombreJourLimitePaiement = GeneralConst.EMPTY_STRING;
            String isGenerate = GeneralConst.EMPTY_STRING;

            PeriodeDeclaration pd = RecouvrementBusiness.getPeriodeDeclarationById(idPeriode);

            String idAssujettissement = pd.getAssujetissement().getId();

            cd.hologram.erecettesvg.models.Assujeti assujeti = AssujettissementBusiness.getAssujettiNormalById(idAssujettissement);

            ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

            if (assujeti != null) {

                articleBudgetaire = assujeti.getArticleBudgetaire();
                date = ConvertDate.getValidFormatDateString(assujeti.getDateFin());
            }

            codePeriodicite = articleBudgetaire.getPeriodicite().getCode();
            nombreJour = articleBudgetaire.getPeriodicite().getNbrJour() + "";
            nombreJourLimite = articleBudgetaire.getNbrJourLimite() + "";
            echeanceLegale = ConvertDate.formatDateToStringOfFormat(pd.getDateLimite(), "dd/MM/yyyy");

            if (articleBudgetaire.getPeriodeEcheance() != null) {
                periodeEcheance = articleBudgetaire.getPeriodeEcheance() == true ? "1" : "0";
            } else {
                periodeEcheance = "0";
            }

            nombreJourLimitePaiement = articleBudgetaire.getDateLimitePaiement();
            isGenerate = "1";

            List<PeriodeDeclaration> periodeDeclarations = AssujettissementBusiness.getPeriodeDeclarationDispo(idAssujettissement);

            boolean isNullAndEmpty = false;
            boolean generate = false;

            if (periodeDeclarations == null || periodeDeclarations.isEmpty()) {
                isNullAndEmpty = true;
            } else {
                Assujeti assujettissement = periodeDeclarations.get(0).getAssujetissement();
                String dateFin = assujettissement.getDateFin();

                if (isGenerate.equals(GeneralConst.Number.ONE)) {
                    generate = true;
                    isNullAndEmpty = true;
                } else {
                    generate = ifNotExistDernierePeriode(periodeDeclarations, dateFin);
                }
            }

            if (isNullAndEmpty || generate) {

                int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                        ? GeneralConst.Number.ZERO
                        : nombreJour);

                String[] dateArray = date.split(GeneralConst.SEPARATOR_SLASH_NO_SPACE);
                String jour = dateArray[0];
                String mois = dateArray[1];
                String annee = dateArray[2];

                List<JSONObject> jsonPeriodes = generatePeriodesDeclarations(
                        codePeriodicite,
                        jour,
                        mois,
                        annee,
                        nombreDeJours,
                        nombreJourLimite,
                        echeanceLegale,
                        nombreJourLimitePaiement,
                        true,
                        periodeEcheance);

                if (jsonPeriodes != null) {

                    int size = jsonPeriodes.size();
                    String dateFin = GeneralConst.EMPTY_STRING;

                    periodeDeclarations = new ArrayList<>();

                    for (JSONObject jsonPeriode : jsonPeriodes) {

                        int ordrePeriode = Integer.parseInt(jsonPeriode.getString(AssujettissementConst.ParamName.ORDRE_PERIODE));

                        if (ordrePeriode == size) {
                            dateFin = jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN);
                        }

                        PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();

                        periodeDeclaration.setDebut(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_DEBUT)));

                        periodeDeclaration.setFin(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_FIN)));

                        periodeDeclaration.setDateLimite(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE)));

                        periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(
                                jsonPeriode.getString(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT)));

                        periodeDeclarations.add(periodeDeclaration);

                    }

                    boolean succes = AssujettissementBusiness.savePeriodesDeclarations(
                            idAssujettissement,
                            periodeDeclarations,
                            dateFin);

                    if (succes) {
                        result = GeneralConst.ResultCode.SUCCES_OPERATION;
                    } else {
                        result = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            } else {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (JSONException | NumberFormatException e) {
            CustumException.LogException(e);

        }

    }

    public boolean ifNotExistDernierePeriode(
            List<PeriodeDeclaration> periodeDeclarations,
            String dateFinAssujettissement) {

        boolean result = true;

        for (PeriodeDeclaration declaration : periodeDeclarations) {

            String dateFin = ConvertDate.formatDateToStringOfFormat(declaration.getFin(), "yyyy-MM-dd");

            if (dateFin.equals(dateFinAssujettissement)) {

                result = false;
                break;
            }
        }
        return result;
    }

    public List<JSONObject> generatePeriodesDeclarations(
            String codePeriodicite, String jour, String mois, String annee, int nombreJour,
            String nombreJourLimite, String echeanceLegale, String nombreJourLimitePaiement, boolean forTaxation, String periodeEcheance) {

        final String PONC = "PR0012015",
                JOUR = "PR0022015",
                MENS = "PR0032015",
                TRIME = "PR0052015",
                SEMS = "PR0072015",
                ANNEE = "PR0042015";

        List<JSONObject> jsonPeriodes = new ArrayList();

        Date dateDepart = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + annee);
        int anneeDepart = Integer.parseInt(annee);
        int moisDepart = Integer.parseInt(mois);

        String DATE_DAY_ECHANCE = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE = GeneralConst.EMPTY_STRING;
        String DATE_DAY_ECHANCE_PAIEMENT = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE_PAIEMENT = GeneralConst.EMPTY_STRING;

        if (!echeanceLegale.equals(GeneralConst.Number.ZERO) && !echeanceLegale.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = echeanceLegale.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE = dateArray[2];
            DATE_MONTH_ECHEANCE = dateArray[1];
        }

        if (!nombreJourLimitePaiement.equals(GeneralConst.Number.ZERO) && !nombreJourLimitePaiement.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = nombreJourLimitePaiement.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE_PAIEMENT = dateArray[2];
            DATE_MONTH_ECHEANCE_PAIEMENT = dateArray[1];
        }

        switch (codePeriodicite) {
            case PONC:
            case JOUR:

                int length = 1;
                Date dateJour = new Date();
                if (forTaxation) {
                    length = 10;
                    dateJour = ConvertDate.formatDate(jour + "/" + mois + "/" + annee);
                    dateJour = ConvertDate.addDayOfDate(dateJour, 1);
                }

                jsonPeriodes = PeriodiciteData.getPeriodeJournaliere(dateJour, length, codePeriodicite, nombreJourLimitePaiement);
                break;

            case MENS:
            case "BIMEN":
            case TRIME:
            case SEMS:

                if (forTaxation) {
                    moisDepart += 1;
                    if (moisDepart > 12) {
                        moisDepart = 1;
                        anneeDepart += 1;
                    }
                }

                jsonPeriodes = PeriodiciteData.getPeriodesMensuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        moisDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
                break;

            case ANNEE:
            case "2ANS":
            case "5ANS":

                anneeDepart = (forTaxation) ? (anneeDepart + 1) : anneeDepart;

                jsonPeriodes = PeriodiciteData.getPeriodesAnnuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_MONTH_ECHEANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        DATE_MONTH_ECHEANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
        }

        return jsonPeriodes;
    }

    public String loadBiensTaxation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numeroTaxation = request.getParameter("numeroTaxation");

            List<Bien> bienTaxationList = TaxationBusiness.getListBienTaxation(numeroTaxation);

            String infosComplementaire = GeneralConst.EMPTY_STRING;

            if (!bienTaxationList.isEmpty()) {

                List<JSONObject> jsonBienList = new ArrayList<>();

                for (Bien bien : bienTaxationList) {

                    JSONObject jsonBien = new JSONObject();

                    jsonBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                            bien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getTypeBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : bien.getTypeBien().getCode());

                    jsonBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                            bien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getTypeBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : bien.getTypeBien().getIntitule().toUpperCase());

                    jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                            bien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getId());

                    jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                            bien.getDescription() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getDescription());

                    if (bien.getFkTarif() != null) {

                        Tarif tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());

                        jsonBien.put("tarifCode", tarif.getCode());
                        jsonBien.put("tarifName", tarif.getIntitule().toUpperCase());

                    } else {
                        jsonBien.put("tarifCode", GeneralConst.EMPTY_STRING);
                        jsonBien.put("tarifName", GeneralConst.EMPTY_STRING);
                    }

                    infosComplementaire = GeneralConst.EMPTY_STRING;

                    String valUnity = GeneralConst.EMPTY_STRING;

                    for (ComplementBien cb : bien.getComplementBienList()) {

                        if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {

                            switch (cb.getDevise()) {
                                case GeneralConst.Devise.DEVISE_CDF:
                                case GeneralConst.Devise.DEVISE_USD:

                                    valUnity = cb.getDevise();

                                    break;
                                default:
                                    Unite unite = TaxationBusiness.getUnitebyCode(cb.getDevise());
                                    valUnity = unite != null ? unite.getIntitule() : "";

                                    break;
                            }

                        } else {
                            valUnity = GeneralConst.EMPTY_STRING;
                        }

                        ValeurPredefinie valeurPredefinie = new ValeurPredefinie();
                        String txtValeurPredefinie = GeneralConst.EMPTY_STRING;

                        if (infosComplementaire.isEmpty()) {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {

                                infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        } else {

                            if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                                valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                                txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                            } else {
                                infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                            }

                        }

                    }

                    jsonBien.put("complement", infosComplementaire);

                    jsonBien.put(AssujettissementConst.ParamName.CHAINE_ADRESSE,
                            bien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getFkAdressePersonne() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : bien.getFkAdressePersonne().getAdresse().toString());

                    jsonBien.put(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE,
                            bien == null
                                    ? GeneralConst.EMPTY_STRING
                                    : bien.getFkAdressePersonne() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : bien.getFkAdressePersonne().getCode());

                    Acquisition acquisition = TaxationBusiness.getAcquisitionByBienAndPersonne(bien.getId(), numeroTaxation);

                    jsonBien.put(AssujettissementConst.ParamName.PROPRIETAIRE, acquisition.getProprietaire() == null
                            ? false : acquisition.getProprietaire());

                    if (bien.getIntitule() != null) {

                        jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                        if (acquisition.getProprietaire()) {
                            jsonBien.put("estLocation", "");
                            jsonBien.put("isMaster", true);
                        } else {
                            jsonBien.put("estLocation", GeneralConst.TEXT_LOCATION);
                            jsonBien.put("isMaster", false);
                        }

                    } else {
                        jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, "");
                    }

                    String responsable = GeneralConst.MY_SELF;
                    /*String intituleBien = String.format(
                     GeneralConst.INTITULE_DATA,
                     acquisition.getBien() == null
                     ? GeneralConst.EMPTY_STRING
                     : acquisition.getBien().getIntitule().toUpperCase());*/

                    BienNoteCalcul bienNoteCalcul = TaxationBusiness.getBienByNoteCalculAndBien(numeroTaxation, bien.getId());

                    if (bienNoteCalcul != null) {

                        if (bienNoteCalcul.getMessage() != null) {
                            jsonBien.put("messageAnnonceur", bienNoteCalcul.getMessage());
                        } else {
                            jsonBien.put("messageAnnonceur", GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonBien.put("messageAnnonceur", GeneralConst.EMPTY_STRING);
                    }

                    Personne personne = AssujettissementBusiness.getResponsableBien(bien.getId());
                    if (personne != null) {
                        responsable = personne.toString();
                    }

                    //intituleBien += "<br/>" + GeneralConst.TEXT_LOCATION;
                    jsonBien.put(AssujettissementConst.ParamName.RESPONSABLE, responsable);
                    //jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, intituleBien);

                    jsonBienList.add(jsonBien);

                }

                dataReturn = jsonBienList.toString();

            } else {

                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public static String getArchivDoc(ArchiveTaxation at) {
        String directory = at.getDirectory();
        return (directory != null && !directory.isEmpty())
                ? Tools.encoder(directory)
                : at.getArchive();
    }

    /*public List<ArchiveData> getArchiveDataList(String userId, String archives, JSONArray jsonDocumentArray) throws IOException {

     String directory = DirectoryData.createDirectoryIfDoesntExist(userId);

     if (directory.isEmpty()) {
     return null;
     }

     List<ArchiveData> documentList = new ArrayList<>();

     try {

     for (int i = 0; i < jsonDocumentArray.length(); i++) {
     JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
     String pvDocument = jsonObject.getString(ControleConst.ParamName.PV_DOCUMENT);
     String base64 = pvDocument;
     base64 = base64.split(";")[1].split(",")[1];
     String fileName = Tools.createFileFromBase64(base64, directory);
     ArchiveData archiveData = new ArchiveData();
     archiveData.setData(pvDocument);
     archiveData.setFile(fileName);
     documentList.add(archiveData);
     }
     } catch (JSONException e) {
     return null;
     }
     return documentList;
     }*/
    List<NotePerceptionMairie> notePerceptionMairieList;
    HashMap<String, Object[]> bulkUpdateNP;

    public String printerDocumentGenerique(HttpServletRequest request) {

        bulkUpdateNP = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;

        String dataReturn = GeneralConst.EMPTY_STRING;

        String documentReference;
        String typeDocument;

        Archive archive_1;
        Archive archive_2;
        PrintDocument printDocument = new PrintDocument();
        NotePerceptionMairie npMairieVol_1 = null;
        NotePerceptionMairie npMairieVol_2 = null;

        try {

            documentReference = request.getParameter("documentReference");
            typeDocument = request.getParameter("typeDocument");

            switch (typeDocument.toUpperCase().trim()) {

                case DocumentConst.DocumentCode.NP_MAIRIE:

                    notePerceptionMairieList = TaxationBusiness.getNotePerceptionMairieByNumero(
                            documentReference.trim());

                    if (notePerceptionMairieList.size() > 0) {
                        for (NotePerceptionMairie npM : notePerceptionMairieList) {
                            if (npM.getVole().equals(GeneralConst.Number.ONE)) {
                                npMairieVol_1 = new NotePerceptionMairie();
                                npMairieVol_1.setNumeroNp(npM.getNumeroNp().trim());
                                npMairieVol_1.setAgentCreate(npM.getAgentCreate());
                                npMairieVol_1.setId(npM.getId());
                                npMairieVol_1.setFkPersonne(npM.getFkPersonne());
                                npMairieVol_1.setVole(npM.getVole());
                                npMairieVol_1.setMontant(npM.getMontant());
                                npMairieVol_1.setFkAb(npM.getFkAb());
                                npMairieVol_1.setDateCreate(npM.getDateCreate());
                                npMairieVol_1.setDateEcheance(npM.getDateEcheance());
                                npMairieVol_1.setEtat(npM.getEtat());
                                npMairieVol_1.setFkBien(npM.getFkBien());
                                npMairieVol_1.setFkDevise(npM.getFkDevise());
                                npMairieVol_1.setNumeroNp(npM.getNumeroNp());
                                npMairieVol_1.setNpMere(null);
                                npMairieVol_1.setFkBanque(npM.getFkBanque());
                                npMairieVol_1.setFkCompteBancaire(npM.getFkCompteBancaire());
                                npMairieVol_1.setFkSite(npM.getFkSite());
                                npMairieVol_1.setFkBien(npM.getFkBien());
                            } else {
                                npMairieVol_2 = new NotePerceptionMairie();
                                npMairieVol_2.setNumeroNp(npM.getNumeroNp().trim());
                                npMairieVol_2.setAgentCreate(npM.getAgentCreate());
                                npMairieVol_2.setId(npM.getId());
                                npMairieVol_2.setFkPersonne(npM.getFkPersonne());
                                npMairieVol_2.setVole(npM.getVole());
                                npMairieVol_2.setMontant(npM.getMontant());
                                npMairieVol_2.setFkAb(npM.getFkAb());
                                npMairieVol_2.setDateCreate(npM.getDateCreate());
                                npMairieVol_2.setDateEcheance(npM.getDateEcheance());
                                npMairieVol_2.setEtat(npM.getEtat());
                                npMairieVol_2.setFkBien(npM.getFkBien());
                                npMairieVol_2.setFkDevise(npM.getFkDevise());
                                npMairieVol_2.setNumeroNp(npM.getNumeroNp());
                                npMairieVol_2.setNpMere(npM.getNpMere());
                                npMairieVol_2.setFkBanque(npM.getFkBanque());
                                npMairieVol_2.setFkCompteBancaire(npM.getFkCompteBancaire());
                                npMairieVol_2.setFkSite(npM.getFkSite());
                                npMairieVol_2.setFkBien(npM.getFkBien());
                            }
                        }
                    }

                    if (npMairieVol_1 != null && npMairieVol_2 != null) {

                        JSONObject jsonDataReturnVolet = new JSONObject();
                        String dataReturnVol1 = GeneralConst.EMPTY_STRING;
                        String dataReturnVol2 = GeneralConst.EMPTY_STRING;

                        archive_1 = NotePerceptionBusiness.getArchiveByRefDocument(String.valueOf(npMairieVol_1.getId()));
                        archive_2 = NotePerceptionBusiness.getArchiveByRefDocument(String.valueOf(npMairieVol_2.getId()));

                        if (archive_1 != null && archive_2 != null) {
                            dataReturnVol1 = archive_1.getDocumentString();
                            dataReturnVol2 = archive_2.getDocumentString();
                        } else {
                            dataReturnVol1 = printDocument.createVolet_A(npMairieVol_1);
                            dataReturnVol2 = printDocument.createVolet_B(npMairieVol_2);
                        }
                        jsonDataReturnVolet.put("dataReturnVol1", dataReturnVol1);
                        jsonDataReturnVolet.put("dataReturnVol2", dataReturnVol2);

                        dataReturn = jsonDataReturnVolet.toString();

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                    break;
                case DocumentConst.DocumentCode.BP:

                    break;

            }

        } catch (NumberFormatException | InvocationTargetException e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        } catch (JSONException ex) {
            Logger.getLogger(Taxation.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dataReturn;
    }

}
