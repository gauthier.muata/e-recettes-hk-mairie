/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AssignationBusiness;
import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.DetailsAssignationBudgetaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Periodicite;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.util.*;
import java.io.*;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "AssignationBudgetaire", urlPatterns = {"/assignationBudgetaire_servlet"})
public class AssignationBudgetaire extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);
        
        switch (operation) {
            case GestionArticleBudgetaireConst.Operation.LOAD_MINISTERE:
                result = loadMinistere(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_SERVICES:
                result = loadServices(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_FAITS_GENERATEURS_SERVICE:
                result = loadFaitsGenerateurs(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_BUDGETAIRE:
                result = loadArticleBudgetaires(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_DEVISE:
                result = loadDevises();
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ARTICLE_BUDGETAIRE_BY_EXERCICE:
                result = loadArticleBudgetairesByExercice(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ASSIGNATION_BY_EXERCICE:
                result = RegistreAssignation(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ALL_SERVICE:
                result = loadAllServices(request);
                break;
            case GestionArticleBudgetaireConst.Operation.SAVE_ASSIGNATION_BUDGETAIRE:
                result = saveAssignationBudgetaire(request);
                break;
            case GestionArticleBudgetaireConst.Operation.UPDATE_ASSIGNATION_MONTANT:
                result = updateAssignationMontant(request);
                break;
            case GestionArticleBudgetaireConst.Operation.LOAD_ASSIGN_BY_EXERCICE:
                result = loadAssignationByExercice(request);
                break;
            case GestionArticleBudgetaireConst.Operation.SAVE_DETAILS_ASSIGNATION_BUDGETAIRE:
                result = saveDetailsAssignationBudgetaire(request);
                break;
        }
        out.print(result);
    }
    
    private String loadMinistere(HttpServletRequest request) {
        
        String result;
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            List<Service> services = GestionArticleBudgetaireBusiness.loadMinistere("1");
            if (!services.isEmpty() || services.size() > 0) {
                for (Service service : services) {
                    
                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule() == null ? GeneralConst.EMPTY_STRING : service.getIntitule());
                    object.put("etat", service.getEtat());
                    object.put("fkService", service.getService() == null ? GeneralConst.Number.ZERO : service.getService().getCode());
                    jSONObject.add(object);
                    
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadServices(HttpServletRequest request) {
        String result;
        List<JSONObject> jSONObject = new ArrayList<>();
        String code_ministere = request.getParameter("codeMinistere");
        
        try {
            
            List<Service> services = GestionArticleBudgetaireBusiness.loadServices(code_ministere, "1");
            if (!services.isEmpty()) {
                for (Service service : services) {
                    
                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule());
                    object.put("fkService", service.getService() == null ? GeneralConst.Number.ZERO : service.getService().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadFaitsGenerateurs(HttpServletRequest request) {
        
        String result;
        String code_service = request.getParameter("codeService");
        
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            
            List<ArticleGenerique> faitGenerateurs = GestionArticleBudgetaireBusiness.loadFaitGenerateurByService(code_service);
            if (!faitGenerateurs.isEmpty()) {
                for (ArticleGenerique articleGenerique : faitGenerateurs) {
                    
                    FormeJuridique formeJuridique = articleGenerique.getFormeJuridique();
                    Service service = articleGenerique.getServiceAssiette();
                    
                    JSONObject object = new JSONObject();
                    object.put("code", articleGenerique.getCode());
                    object.put("intitule", articleGenerique.getIntitule());
                    object.put("codeForme", formeJuridique == null
                            ? "" : formeJuridique.getCode());
                    object.put("intituleForme", formeJuridique == null
                            ? "" : formeJuridique.getIntitule().toUpperCase());
                    object.put("service", service == null ? "" : service.getIntitule());
                    object.put("ministere", service == null ? "" : service.getService() == null
                            ? ""
                            : service.getService().getIntitule());
                    
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadArticleBudgetaires(HttpServletRequest request) {
        
        String result;
        String code_ag = request.getParameter("codeAG");
        
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            
            List<ArticleBudgetaire> articlebudgetaire = GestionArticleBudgetaireBusiness.loadArticleBudgetaireByGenerique(code_ag);
            if (!articlebudgetaire.isEmpty()) {
                for (ArticleBudgetaire article : articlebudgetaire) {
                    
                    ArticleGenerique articlegenerique = article.getArticleGenerique();
                    Periodicite periode = article.getPeriodicite();
                    
                    JSONObject object = new JSONObject();
                    object.put("code", article.getCode());
                    object.put("intitule", article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel());
                    object.put("faitgenerateur", articlegenerique.getIntitule());
                    object.put("intituleperiode", periode.getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadArticleBudgetairesByExercice(HttpServletRequest request) {
        
        String result;
        String code_ag = request.getParameter("codeAG");
        String exerciceFiscal = request.getParameter("exerciceFiscal");
        String secteur = request.getParameter("secteur");
        
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            
            List<DetailsAssignationBudgetaire> detailAssignation = GestionArticleBudgetaireBusiness.loadArticleBudgetaireByExercice(code_ag, exerciceFiscal, secteur);
            if (!detailAssignation.isEmpty()) {
                for (DetailsAssignationBudgetaire dab : detailAssignation) {
                    
                    ArticleBudgetaire article = GestionArticleBudgetaireBusiness.getArticleBudgetaireByAG(dab.getFkArticleBudgetaire());
                    //AssignationBudgetaire assignationBudgetaire = GestionArticleBudgetaireBusiness.getAssignationByCode(dab.getFkAssignation());

                    ArticleGenerique articlegenerique = article.getArticleGenerique();
                    Float total_Realisation = GestionArticleBudgetaireBusiness.getRealisation(
                            secteur, dab.getFkArticleBudgetaire(), exerciceFiscal, "R");
                    Float total_Assignation = GestionArticleBudgetaireBusiness.getAssignation(
                            secteur, dab.getFkArticleBudgetaire(), exerciceFiscal, "A");
                    
                    Periodicite periode = article.getPeriodicite();
                    JSONObject object = new JSONObject();
                    object.put("intitule", article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel());
                    object.put("faitgenerateur", articlegenerique.getIntitule());
                    object.put("intituleperiode", periode.getIntitule());
                    object.put("assignation", total_Assignation);
                    object.put("devise", dab.getFkDevise().getCode());
                    object.put("totalRealisation", total_Realisation);
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String RegistreAssignation(HttpServletRequest request) {
        
        String result;
        //String code_ag = request.getParameter("codeAG");
        String exerciceFiscal = request.getParameter("exerciceFiscal");
        String secteur = request.getParameter("secteur");
        
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            
            List<DetailsAssignationBudgetaire> detailAssignation = GestionArticleBudgetaireBusiness.loadAssignationByExercice(secteur, exerciceFiscal);
            if (!detailAssignation.isEmpty()) {
                for (DetailsAssignationBudgetaire dab : detailAssignation) {
                    
                    ArticleBudgetaire article = GestionArticleBudgetaireBusiness.getArticleBudgetaireByAG(dab.getFkArticleBudgetaire());
                    //cd.hologram.erecettesvg.models.AssignationBudgetaire assignationBudgetaire = GestionArticleBudgetaireBusiness.getAssignationByCode(dab.getFkAssignation());

                    ArticleGenerique articlegenerique = article.getArticleGenerique();
//                    Float total_Realisation = GestionArticleBudgetaireBusiness.getRealisation(
//                            secteur, dab.getFkArticleBudgetaire(), exerciceFiscal, "R");
//                     Float total_Assignation = GestionArticleBudgetaireBusiness.getAssignation(
//                            secteur, dab.getFkArticleBudgetaire(), exerciceFiscal, "A");
                    String dateAssignation = ConvertDate.formatDateToStringOfFormat(dab.getDateMaj(), GeneralConst.Format.FORMAT_DATE);
                    
                    Periodicite periode = article.getPeriodicite();
                    JSONObject object = new JSONObject();
                    object.put("id", dab.getId());
                    object.put("intitule", article.getIntitule());
                    object.put("codeofficiel", article.getCodeOfficiel());
                    object.put("faitgenerateur", articlegenerique.getIntitule());
                    object.put("intituleperiode", periode.getIntitule());
                    object.put("assignation", dab.getMontantAssigne());
                    object.put("dateassignation", dateAssignation);
                    object.put("devise", dab.getFkDevise().getCode());
                    object.put("totalRealisation", 0);
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadAllServices(HttpServletRequest request) {
        String result;
        List<JSONObject> jSONObject = new ArrayList<>();
        String code_ministere = "";
        
        try {
            
            List<Service> services = GestionArticleBudgetaireBusiness.loadServices(code_ministere, "2");
            if (!services.isEmpty()) {
                for (Service service : services) {
                    
                    JSONObject object = new JSONObject();
                    object.put("code", service.getCode());
                    object.put("intitule", service.getIntitule());
                    object.put("fkService", service.getService() == null ? GeneralConst.Number.ZERO : service.getService().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    private String loadDevises() {
        
        String result;
        
        List<JSONObject> jSONObject = new ArrayList<>();
        
        try {
            
            List<Devise> devises = TaxationBusiness.getListAllDevises();
            if (!devises.isEmpty()) {
                for (Devise devise : devises) {
                    JSONObject object = new JSONObject();
                    object.put("code", devise.getCode());
                    object.put("intitule", devise.getIntitule());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }
    
    public String saveAssignationBudgetaire(HttpServletRequest request) {
        
        String result;
        
        try {
            
            String id = request.getParameter("id");
            String exercice = request.getParameter("exercice");
            String montantGlobal = request.getParameter("montantGlobal");
            String devise = request.getParameter("devise");
            String idUser = request.getParameter("idUser");
            
            cd.hologram.erecettesvg.models.AssignationBudgetaire assignationBudgetaire = new cd.hologram.erecettesvg.models.AssignationBudgetaire();
            assignationBudgetaire.setId(Integer.valueOf(id));
            assignationBudgetaire.setFkExercice(exercice);
            assignationBudgetaire.setMontantGlobal(Double.valueOf(montantGlobal));
            assignationBudgetaire.setFkDevise(devise);
            assignationBudgetaire.setAgentCreate(Integer.valueOf(idUser));
            assignationBudgetaire.setDateCreate(new Date());
            assignationBudgetaire.setEtat(1);
            
            result = AssignationBusiness.saveAssignationBudgetaire(assignationBudgetaire);
            
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        
        return result;
    }
    
    public String saveDetailsAssignationBudgetaire(HttpServletRequest request) {
        
        String result;
        
        try {
            
            String id = request.getParameter("assignationId");
            String idUser = request.getParameter("idUser");
            String devise = request.getParameter("devise");
            
            JSONArray details = new JSONArray(request.getParameter("details"));
            
            List<DetailsAssignationBudgetaire> detailsAssignationBudgetaireList = new ArrayList<>();
            
            for (int i = 0; i < details.length(); i++) {
                JSONObject data = details.getJSONObject(i);
                DetailsAssignationBudgetaire dab = new DetailsAssignationBudgetaire();
                dab.setFkAssignation(Integer.valueOf(id));
                dab.setFkArticleBudgetaire(data.getString("code"));
                dab.setMontantAssigne(data.getDouble("montant"));
                dab.setAgentMaj(Integer.valueOf(idUser));
                dab.setFkDevise(new Devise(devise));
                dab.setEtat(1);
                detailsAssignationBudgetaireList.add(dab);
            }
            
            if (AssignationBusiness.saveDetailsAssignationBudgetaire(id, idUser, detailsAssignationBudgetaireList)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
            
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        
        return result;
    }
    
    public String updateAssignationMontant(HttpServletRequest request) {
        
        String result;
        
        try {
            String id = request.getParameter("id");
            String montant = request.getParameter("montant");
            
            if (AssignationBusiness.updateAmountAssignation(Long.valueOf(montant), Integer.valueOf(id))) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
            
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        
        return result;
    }
    
    private String loadAssignationByExercice(HttpServletRequest request) {
        
        String result;
        String exerciceFiscal = request.getParameter("exerciceFiscal");
        
        JSONObject object = new JSONObject();
        
        try {
            
            cd.hologram.erecettesvg.models.AssignationBudgetaire assignation = GestionArticleBudgetaireBusiness.loadAssignationByExercice(exerciceFiscal);
            
            if (assignation != null) {
                object.put("id", assignation.getId());
                object.put("exercice", assignation.getFkExercice());
                object.put("montantGlobal", assignation.getMontantGlobal());
                object.put("archive", assignation.getArchive() == null ? "" : assignation.getArchive());
                result = object.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
