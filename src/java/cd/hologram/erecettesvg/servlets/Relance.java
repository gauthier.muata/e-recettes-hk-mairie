/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.*;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getArchiveByRefDocument;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.RecepissePrint;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "Relance", urlPatterns = {"/relance_servlet"})
public class Relance extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Med med;
    Adresse adresse;
    Archive archive;
    List<NotePerception> notePerceptionList;
    List<PeriodeDeclaration> periodeDeclarationList;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(TaxationConst.ParamName.OPERATION);

        switch (operation) {
            case RecouvrementConst.Operation.LOAD_DEFAILLANT_DECLARATION:
                result = loadDefaillantDeclaration(request);
                break;
            case RecouvrementConst.Operation.LOAD_INVITE_SERVICE:
                result = loadInvitationService(request);
                break;
            case RecouvrementConst.Operation.LOAD_RELANCE:
                result = loadRelance(request);
                break;
            case RecouvrementConst.Operation.PRINT_INVITE_SERVICE:
                result = saveAndPrintInviteService(request);
                break;
            case RecouvrementConst.Operation.PRINT_RELANCE:
                result = saveAndPrintMed(request);
                break;
            case RecouvrementConst.Operation.TAXATION_OFFICE:
                result = taxationOffice(request);
                break;
            case DeclarationConst.Operation.SEARCH_RETRAIT_DECLARATION:
                result = loadRetraitDeclaration(request);
                break;
            case DeclarationConst.Operation.SEARCH_AVANCED_RETRAIT_DECLARATION:
                result = loadRetraitDeclarationBySearchAvanced(request);
                break;
            case DeclarationConst.Operation.REPRINT_RECEPISSE:
                result = reprintRecepisse(request);
                break;
            case PoursuiteConst.Operation.PRINT_DOCUMENT:
                result = printDocument(request);
                break;
            case PoursuiteConst.Operation.LOAD_AMR:
                result = getAvisMiseEnRecouvrement(request);
                break;
            case PoursuiteConst.Operation.PRINT_AMR:
                result = printAmr(request);
                break;
            case PoursuiteConst.Operation.GENERATE_CONTRAINTE:
                result = generateContrainte(request);
        }

        out.print(result);
    }

    public String saveAndPrintInviteService(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String libelleArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            String periodeDeclaration = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION);
            String echeanceDeclaration = request.getParameter(RecouvrementConst.ParamName.ECHEANCE_DECLARATION);
            String adresseCode = request.getParameter(RecouvrementConst.ParamName.ADRESSE_CODE);
            String adresseName = request.getParameter(RecouvrementConst.ParamName.ADRESSE_NAME);
            String printExist = request.getParameter(RecouvrementConst.ParamName.PRINT_EXISTS);
            String agentCreat = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            String periodeDeclarationId = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID);
            String numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);
            String dateHeureInvitation = request.getParameter(RecouvrementConst.ParamName.DATE_HEURE_INVITATION);

            String amountPeriodeDeclaration = request.getParameter("amountPeriodeDeclaration");
            String penaliteDu = request.getParameter("penaliteDu");

            float amountPeriodeDeclarationF = Float.valueOf(amountPeriodeDeclaration);
            float penaliteDuF = Float.valueOf(penaliteDu);

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    PeriodeDeclaration pd = DeclarationBusiness.getPeriodeDeclarationById(periodeDeclarationId);

                    if (RecouvrementBusiness.saveMed(
                            codeArticleBudgetaire,
                            assujettiCode,
                            periodeDeclaration,
                            echeanceDeclaration,
                            adresseCode, agentCreat,
                            amountPeriodeDeclarationF,
                            Integer.valueOf(periodeDeclarationId), "SERVICE",
                            GeneralConst.EMPTY_STRING, GeneralConst.EMPTY_STRING, penaliteDuF)) {

                        Med med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(periodeDeclarationId));

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();

                            String periodeDec = GeneralConst.EMPTY_STRING, agentSignateur = GeneralConst.EMPTY_STRING;
                            if (pd != null) {
                                periodeDec = Tools.getPeriodeIntitule(pd.getDebut(), pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode());
                                Agent agent = ConnexionBusiness.getAgentByCode(agentCreat);
                                agentSignateur = agent == null ? GeneralConst.EMPTY_STRING : agent.toString();
                            }

                            dataReturn = printDocument.createInviteSevice(med, libelleArticleBudgetaire, assujettiName,
                                    adresseName, periodeDeclaration, echeanceDeclaration, dateHeureInvitation,
                                    periodeDec, agentSignateur, Integer.valueOf(agentCreat));

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAndPrintMed(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String libelleArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            String periodeDeclaration = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION);
            String echeanceDeclaration = request.getParameter(RecouvrementConst.ParamName.ECHEANCE_DECLARATION);
            String adresseCode = request.getParameter(RecouvrementConst.ParamName.ADRESSE_CODE);
            String adresseName = request.getParameter(RecouvrementConst.ParamName.ADRESSE_NAME);
            String printExist = request.getParameter(RecouvrementConst.ParamName.PRINT_EXISTS);
            String agentCreat = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            String periodeDeclarationId = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID);
            String numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    //PeriodeDeclaration pd = DeclarationBusiness.getPeriodeDeclarationById(periodeDeclarationId);
                    Med med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(periodeDeclarationId));

                    if (RecouvrementBusiness.saveMed(
                            codeArticleBudgetaire,
                            assujettiCode,
                            periodeDeclaration,
                            echeanceDeclaration,
                            adresseCode, agentCreat,
                            med.getMontant().floatValue(),
                            Integer.valueOf(periodeDeclarationId), "RELANCE",
                            GeneralConst.EMPTY_STRING, med.getId(),
                            med.getMontantPenalite().floatValue())) {

                        med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(periodeDeclarationId), "RELANCE");

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();

                            dataReturn = printDocument.createMed(
                                    med, libelleArticleBudgetaire, assujettiName,
                                    adresseName, periodeDeclaration,
                                    echeanceDeclaration);

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadDefaillantDeclaration(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING,
                sqlQueryParam = GeneralConst.EMPTY_STRING;

        try {

            String advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String codePeriodicite = request.getParameter(RecouvrementConst.ParamName.CODE_PERIODICITE);
            String periodValueMonth = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_MONTH);
            String periodValueYear = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_YEAR);

            periodeDeclarationList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    periodeDeclarationList = RecouvrementBusiness.getListDefaillantDeclarationBySimpleSearch(
                            assujettiCode);
                    break;

                case GeneralConst.Number.ONE:

                    if (codePeriodicite != null) {

                        switch (codePeriodicite) {

                            case "PR0032015": // MENSUEL
                                sqlQueryParam = " AND MONTH(PD.DEBUT) = %s AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueMonth, periodValueYear);
                                break;
                            case "PR0042015": // ANNUEL
                                sqlQueryParam = " AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueYear);
                                break;
                        }
                    }

                    periodeDeclarationList = RecouvrementBusiness.getListDefaillantDeclarationByAdvancedSearch(
                            codeArticleBudgetaire == null ? GeneralConst.EMPTY_STRING : codeArticleBudgetaire, sqlQueryParam);

                    break;
            }

            if (!periodeDeclarationList.isEmpty()) {

                List<JsonObject> pdJsonList = new ArrayList<>();

                for (PeriodeDeclaration pd : periodeDeclarationList) {

                    JsonObject pdJsonObject = new JsonObject();

                    Palier palier = new Palier();

                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;
                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;

                    adresse = new Adresse();

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID, pd.getId());

                    Med med = RecouvrementBusiness.getMedByPerideDeclaration(pd.getId(), "SERVICE");

                    if (med != null) {

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());
                        pdJsonObject.addProperty("numeroDocument", med.getNumeroDocument());
                        pdJsonObject.addProperty("typeMed", "SERVICE");

                        /*archive = NotePerceptionBusiness.getArchiveByRefDocument(med.getId());

                         if (archive != null) {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());
                         } else {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                         }*/
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);

                    } else {
                        pdJsonObject.addProperty("typeMed", "");
                        pdJsonObject.addProperty("numeroDocument", GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                    }

                    long moisRetard = 0;
                    String echeanceDate = GeneralConst.EMPTY_STRING;

                    if (pd.getAssujetissement() != null) {

                        if (pd.getDateLimite() != null) {

                            if (Compare.before(pd.getDateLimite(), new Date())) {

                                //moisRetard = ConvertDate.getMonthsBetweenV2(pd.getDateLimite(), new Date(), pd.getId());
                                echeanceDate = ConvertDate.formatDateToString(pd.getDateLimite());
                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanceDate);

                            } else {
                                moisRetard = 0;
                            }

                        } else {

                            if (Compare.before(pd.getFin(), new Date())) {

                                //moisRetard = ConvertDate.getMonthsBetweenV2(pd.getFin(), new Date(), pd.getId());
                                echeanceDate = ConvertDate.formatDateToString(pd.getFin());
                                moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanceDate);

                            } else {
                                moisRetard = 0;
                            }
                        }

                        pdJsonObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                        Bien bien = pd.getAssujetissement().getBien();

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN, bien.getTypeBien().getIntitule().toUpperCase());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien.getTypeBien().getCode());

                        int _type = 0;

                        TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(bien.getTypeBien().getCode());

                        _type = typeBienService.getType();

                        pdJsonObject.addProperty("type", _type);

                        if (bien.getFkAdressePersonne() != null) {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                        } else {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getDescription() != null && !bien.getDescription().isEmpty()) {

                            pdJsonObject.addProperty("descriptionBien", bien.getDescription());

                        } else {
                            pdJsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkCommune() != null) {

                            EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkCommune());

                            String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                            pdJsonObject.addProperty("communeCode", ea.getCode());
                            pdJsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                        } else {
                            pdJsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkQuartier() != null) {

                            EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkQuartier());

                            pdJsonObject.addProperty("quartierCode", eaQuartier.getCode());
                            pdJsonObject.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                        } else {
                            pdJsonObject.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkTarif() != null) {

                            Tarif tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());

                            pdJsonObject.addProperty("tarifCode", tarif.getCode());
                            pdJsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                        } else {
                            pdJsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkUsageBien() != null) {

                            UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(bien.getFkUsageBien());

                            pdJsonObject.addProperty("usageCode", usageBien.getId());
                            pdJsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                        } else {
                            pdJsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                        }

                        if (advancedSearch.equals(GeneralConst.Number.ZERO)) {
                            codePeriodicite = pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode();
                        }

                        float amountPeriode = 0, taux = 0, valeurBase = 0;

                        String devisePeriode = GeneralConst.EMPTY_STRING;

                        palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonneV2(
                                pd.getAssujetissement().getArticleBudgetaire().getCode(),
                                bien.getFkTarif(),
                                pd.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                                bien.getFkQuartier(),
                                bien.getTypeBien().getCode());

                        if (palier != null) {

                            if (palier.getMultiplierValeurBase() == GeneralConst.Numeric.ONE) {

                                taux = palier.getTaux().floatValue();
                                valeurBase = pd.getAssujetissement().getValeur().floatValue();

                                amountPeriode = (taux * valeurBase);

                            } else {
                                amountPeriode = pd.getAssujetissement().getValeur().floatValue();
                            }

                            devisePeriode = palier.getDevise().getCode();

                        } else {
                            amountPeriode = 0;
                        }

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION,
                                amountPeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION,
                                devisePeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                pd.getAssujetissement().getArticleBudgetaire().getCode());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                pd.getAssujetissement().getArticleBudgetaire().getIntitule());

                        if (pd.getAssujetissement().getPersonne() != null) {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    pd.getAssujetissement().getPersonne().getCode());

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    pd.getAssujetissement().getPersonne().toString().toUpperCase());

                            typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    pd.getAssujetissement().getPersonne().getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                    pd.getAssujetissement().getPersonne().getCode());

                            if (adresse != null) {
                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat("</span>"));

                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        adresse.getId());
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        adresse.toString().toUpperCase());

                            } else {
                                adresseAssujetti = GeneralConst.EMPTY_STRING;
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(pd.getAssujetissement().getPersonne().toString().toUpperCase()).concat("</span>"));

                            assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    assujettiNameComposite);

                        } else {
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    GeneralConst.EMPTY_STRING);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                    GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                    GeneralConst.EMPTY_STRING);

                        }
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.ECHEANCE_DECLARATION,
                            ConvertDate.formatDateToString(pd.getDateLimite()));

                    String echeance = Tools.getPeriodeIntitule(pd.getDebut(), codePeriodicite);

                    if (!echeance.isEmpty()) {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, echeance.toUpperCase());
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION,
                                GeneralConst.EMPTY_STRING);
                    }

                    pdJsonList.add(pdJsonObject);

                }

                dataReturn = pdJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadInvitationService(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING, sqlQueryParam = GeneralConst.EMPTY_STRING;

        List<Med> medList = new ArrayList<>();

        try {

            String advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String codePeriodicite = request.getParameter(RecouvrementConst.ParamName.CODE_PERIODICITE);
            String periodValueMonth = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_MONTH);
            String periodValueYear = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_YEAR);

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = RecouvrementBusiness.getListMedByPersonne(assujettiCode, "SERVICE");

                    break;

                case GeneralConst.Number.ONE:

                    if (codePeriodicite != null) {

                        switch (codePeriodicite) {
                            case "PR0032015": // MENSUEL
                                sqlQueryParam = " AND MONTH(PD.DEBUT) = %s AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueMonth, periodValueYear);
                                break;
                            case "PR0042015": // ANNUEL
                                sqlQueryParam = " AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueYear);
                                break;
                        }
                    }

                    medList = RecouvrementBusiness.getListMedAvance(codeArticleBudgetaire == null
                            ? GeneralConst.EMPTY_STRING : codeArticleBudgetaire, sqlQueryParam, "SERVICE");

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> pdJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject pdJsonObject = new JsonObject();

                    PeriodeDeclaration pd = RecouvrementBusiness.getPeriodeDeclarationById(med.getPeriodeDeclaration() == null ? 0 : med.getPeriodeDeclaration());

                    Palier palier = new Palier();

                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;
                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;

                    adresse = new Adresse();

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID, pd.getId());
                    pdJsonObject.addProperty("numeroDocument", med.getNumeroDocument() == null ? "" : med.getNumeroDocument().toUpperCase());

                    if (med.getDateEcheance() != null) {

                        pdJsonObject.addProperty("accuserExist", GeneralConst.Number.ONE);

                        if (new Date().after(med.getDateEcheance())) {
                            pdJsonObject.addProperty("nextStap", GeneralConst.Number.ONE);
                        } else {
                            pdJsonObject.addProperty("nextStap", GeneralConst.Number.ZERO);
                        }

                        pdJsonObject.addProperty("echeanceIS", ConvertDate.formatDateToString(med.getDateEcheance()));
                        pdJsonObject.addProperty("receptionIS", ConvertDate.formatDateToString(med.getDateReception()));

                    } else {
                        pdJsonObject.addProperty("accuserExist", GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty("nextStap", GeneralConst.Number.ZERO);

                        pdJsonObject.addProperty("echeanceIS", "NON DEFINIE");
                        pdJsonObject.addProperty("receptionIS", "NON DEFINIE");
                    }

                    /*Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(med.getNotePerception());

                     if (journal != null) {

                     String codeStatePaid = GeneralConst.Number.ONE;
                     pdJsonObject.addProperty("estPayer", codeStatePaid);

                     } else {
                     pdJsonObject.addProperty("estPayer", GeneralConst.Number.ZERO);
                     }*/
                    RetraitDeclaration declarationPrincipal = AssujettissementBusiness.getRetraitDeclarationPrincipalByPeriode(
                            med.getPeriodeDeclaration() + "");

                    BigDecimal amountPaidPenalite = new BigDecimal(0);
                    BigDecimal amountPaidTotal = new BigDecimal(0);

//                    System.out.println(" MED : " + med.getNumeroDocument());
                    if (declarationPrincipal != null) {

                        List<RetraitDeclaration> declarationPenaliteList = new ArrayList<>();

                        declarationPenaliteList = AssujettissementBusiness.getListNoteTaxationPenaliteByPrincipal(declarationPrincipal.getId());

                        if (!declarationPenaliteList.isEmpty()) {

                            pdJsonObject.addProperty("ntPenaliteExiste", GeneralConst.Number.ONE);

                            for (RetraitDeclaration declarationPenalite : declarationPenaliteList) {

                                Journal journalNTPenalite = AcquitLiberatoireBusiness.getJournalByDocumentApure(declarationPenalite.getCodeDeclaration());

                                if (journalNTPenalite != null) {
                                    amountPaidPenalite = amountPaidPenalite.add(journalNTPenalite.getMontant());
                                }
                            }
                        } else {
                            pdJsonObject.addProperty("ntPenaliteExiste", GeneralConst.Number.ZERO);
                        }
                    } else {
                        pdJsonObject.addProperty("ntPenaliteExiste", GeneralConst.Number.ZERO);
                    }

                    BigDecimal amountDu = new BigDecimal(0);
                    float resteARecouvre = 0;

                    amountDu = amountDu.add(med.getMontant());
                    amountDu = amountDu.add(med.getMontantPenalite());

                    if (declarationPrincipal != null) {

                        Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(declarationPrincipal.getCodeDeclaration());

                        if (journal != null) {

                            amountPaidTotal = amountPaidTotal.add(journal.getMontant());
                            amountPaidTotal = amountPaidTotal.add(amountPaidPenalite);

                            if (amountDu.floatValue() > amountPaidTotal.floatValue()) {

                                resteARecouvre = amountDu.floatValue() - amountPaidTotal.floatValue();

                                pdJsonObject.addProperty("estPayer", "-1");

                            } else {
                                pdJsonObject.addProperty("estPayer", "1");
                            }
                        } else {
                            pdJsonObject.addProperty("estPayer", GeneralConst.Number.ZERO);
                            resteARecouvre = amountDu.floatValue();
                        }

                        pdJsonObject.addProperty("resteARecouvre", resteARecouvre);

                    } else {
                        pdJsonObject.addProperty("estPayer", GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty("resteARecouvre", 0);
                    }

                    pdJsonObject.addProperty("amountPenaliteMed", med.getMontantPenalite());
                    pdJsonObject.addProperty("penaliteDu", med.getMontantPenalite());

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());

                    Med medRelance = RecouvrementBusiness.getMedByPerideDeclaration(pd.getId(), "RELANCE");

                    if (medRelance != null) {

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);
                        pdJsonObject.addProperty("relanceExist", GeneralConst.Number.ONE);
                        pdJsonObject.addProperty("numeroMedRelance", medRelance.getId());
                        pdJsonObject.addProperty("numeroReferenceRelance", medRelance.getNumeroDocument());

                        /*archive = NotePerceptionBusiness.getArchiveByRefDocument(medRelance.getId());

                         if (archive != null) {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());
                         } else {
                         pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                         }*/
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);

                    } else {

                        pdJsonObject.addProperty("numeroMedRelance", GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty("relanceExist", GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty("numeroReferenceRelance", GeneralConst.EMPTY_STRING);
                    }

                    if (pd.getAssujetissement() != null) {

                        Bien bien = pd.getAssujetissement().getBien();

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN, bien.getTypeBien().getIntitule().toUpperCase());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien.getTypeBien().getCode());

                        if (bien.getFkAdressePersonne() != null) {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                        } else {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    GeneralConst.EMPTY_STRING);
                        }

                        int _type = 0;

                        TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(bien.getTypeBien().getCode());

                        _type = typeBienService.getType();

                        pdJsonObject.addProperty("type", _type);

                        if (bien.getDescription() != null && !bien.getDescription().isEmpty()) {

                            pdJsonObject.addProperty("descriptionBien", bien.getDescription());

                        } else {
                            pdJsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkCommune() != null) {

                            EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkCommune());

                            String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                            pdJsonObject.addProperty("communeCode", ea.getCode());
                            pdJsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                        } else {
                            pdJsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkQuartier() != null) {

                            EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkQuartier());

                            pdJsonObject.addProperty("quartierCode", eaQuartier.getCode());
                            pdJsonObject.addProperty("quartiereName", eaQuartier.getIntitule().toUpperCase());

                        } else {
                            pdJsonObject.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("quartiereName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkTarif() != null) {

                            Tarif tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());

                            pdJsonObject.addProperty("tarifCode", tarif.getCode());
                            pdJsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                        } else {
                            pdJsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkUsageBien() != null) {

                            UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(bien.getFkUsageBien());

                            pdJsonObject.addProperty("usageCode", usageBien.getId());
                            pdJsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                        } else {
                            pdJsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                        }

                        if (advancedSearch.equals(GeneralConst.Number.ZERO)) {
                            codePeriodicite = pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode();
                        }

                        float amountPeriode = 0, taux = 0, valeurBase = 0;

                        String devisePeriode = GeneralConst.EMPTY_STRING;

                        /*palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonne(
                         pd.getAssujetissement().getArticleBudgetaire().getCode(),
                         pd.getAssujetissement().getTarif().getCode(),
                         pd.getAssujetissement().getPersonne().getFormeJuridique().getCode());*/
                        palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonneV2(
                                pd.getAssujetissement().getArticleBudgetaire().getCode(),
                                bien.getFkTarif(),
                                pd.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                                bien.getFkQuartier(),
                                bien.getTypeBien().getCode());

                        if (palier != null) {

                            devisePeriode = palier.getDevise().getCode();

                        } else {
                            devisePeriode = GeneralConst.Devise.DEVISE_USD;
                        }

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION, med.getMontant());
                        pdJsonObject.addProperty("penaliteDu", med.getMontantPenalite());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION, devisePeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE, pd.getAssujetissement().getArticleBudgetaire().getCode());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME, pd.getAssujetissement().getArticleBudgetaire().getIntitule());

                        if (pd.getAssujetissement().getPersonne() != null) {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    pd.getAssujetissement().getPersonne().getCode());

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    pd.getAssujetissement().getPersonne().toString().toUpperCase());

                            typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    pd.getAssujetissement().getPersonne().getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                    pd.getAssujetissement().getPersonne().getCode());

                            if (adresse != null) {
                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat("</span>"));

                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        adresse.getId());
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        adresse.toString().toUpperCase());

                            } else {
                                adresseAssujetti = GeneralConst.EMPTY_STRING;
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(pd.getAssujetissement().getPersonne().toString().toUpperCase()).concat("</span>"));

                            assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, assujettiNameComposite);

                        } else {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);

                        }
                    } else {

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                    }

                    Date date = ConvertDate.addDayOfDate(med.getDateCreat(), 2);
                    if (date.after(new Date())) {
                        pdJsonObject.addProperty("depassement", 0);
                    } else {
                        pdJsonObject.addProperty("depassement", 1);
                    }

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.ECHEANCE_DECLARATION, ConvertDate.formatDateToString(pd.getDateLimite()));

                    String echeance = Tools.getPeriodeIntitule(pd.getDebut(), codePeriodicite);

                    if (!echeance.isEmpty()) {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, echeance.toUpperCase());
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                    }

                    pdJsonList.add(pdJsonObject);
                }

                dataReturn = pdJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadRelance(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING, sqlQueryParam = GeneralConst.EMPTY_STRING;

        List<Med> medList = new ArrayList<>();

        try {

            String advancedSearch = request.getParameter(RecouvrementConst.ParamName.ADVANCED_SEARCH);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String codePeriodicite = request.getParameter(RecouvrementConst.ParamName.CODE_PERIODICITE);
            String periodValueMonth = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_MONTH);
            String periodValueYear = request.getParameter(RecouvrementConst.ParamName.PERIOD_VALUE_YEAR);

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    medList = RecouvrementBusiness.getListMedByPersonne(assujettiCode, "RELANCE");

                    break;

                case GeneralConst.Number.ONE:

                    if (codePeriodicite != null) {

                        switch (codePeriodicite) {
                            case "PR0032015": // MENSUEL
                                sqlQueryParam = " AND MONTH(PD.DEBUT) = %s AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueMonth, periodValueYear);
                                break;
                            case "PR0042015": // ANNUEL
                                sqlQueryParam = " AND YEAR(PD.DEBUT) = %s";
                                sqlQueryParam = String.format(sqlQueryParam, periodValueYear);
                                break;
                        }
                    }

                    medList = RecouvrementBusiness.getListMedAvance(codeArticleBudgetaire == null
                            ? GeneralConst.EMPTY_STRING : codeArticleBudgetaire, sqlQueryParam, "RELANCE");

                    break;
            }

            if (!medList.isEmpty()) {

                List<JsonObject> pdJsonList = new ArrayList<>();

                for (Med med : medList) {

                    JsonObject pdJsonObject = new JsonObject();

                    PeriodeDeclaration pd = RecouvrementBusiness.getPeriodeDeclarationById(med.getPeriodeDeclaration() == null ? 0 : med.getPeriodeDeclaration());

                    Palier palier = new Palier();

                    String assujettiNameComposite = GeneralConst.EMPTY_STRING;
                    String typeAssujetti = GeneralConst.EMPTY_STRING;
                    String adresseAssujetti = GeneralConst.EMPTY_STRING;
                    String assujettiName = GeneralConst.EMPTY_STRING;

                    adresse = new Adresse();

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID, pd.getId());

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());
                    pdJsonObject.addProperty("numeroDocument", med.getNumeroDocument());

                    RetraitDeclaration retaRetraitDeclaration = AssujettissementBusiness.getRetraitDeclarationOfTaxationOfficeByPeriode(med.getPeriodeDeclaration());

                    if (retaRetraitDeclaration != null) {

                        pdJsonObject.addProperty("taxationOfficeExiste", GeneralConst.Number.ONE);

                        Amr amrMed = PoursuiteBusiness.getAmrByMed(med.getId());

                        if (amrMed != null) {
                            pdJsonObject.addProperty("amrCode", amrMed.getNumero());
                        } else {
                            pdJsonObject.addProperty("amrCode", GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        pdJsonObject.addProperty("taxationOfficeExiste", GeneralConst.Number.ZERO);
                        pdJsonObject.addProperty("amrCode", GeneralConst.EMPTY_STRING);
                    }

                    if (pd.getAssujetissement() != null) {

                        Bien bien = pd.getAssujetissement().getBien();

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().toUpperCase());

                        pdJsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN, bien.getTypeBien().getIntitule().toUpperCase());
                        pdJsonObject.addProperty(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien.getTypeBien().getCode());

                        if (bien.getFkAdressePersonne() != null) {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    bien.getFkAdressePersonne().getAdresse().toString().toUpperCase());

                        } else {

                            pdJsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getDescription() != null && !bien.getDescription().isEmpty()) {

                            pdJsonObject.addProperty("descriptionBien", bien.getDescription());

                        } else {
                            pdJsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkCommune() != null) {

                            EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(bien.getFkCommune());

                            String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                            pdJsonObject.addProperty("communeCode", ea.getCode());
                            pdJsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                        } else {
                            pdJsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkTarif() != null) {

                            Tarif tarif = TaxationBusiness.getTarifByCode(bien.getFkTarif());

                            pdJsonObject.addProperty("tarifCode", tarif.getCode());
                            pdJsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                        } else {
                            pdJsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                        }

                        if (bien.getFkUsageBien() != null) {

                            UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(bien.getFkUsageBien());

                            pdJsonObject.addProperty("usageCode", usageBien.getId());
                            pdJsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                        } else {
                            pdJsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                        }

                        if (advancedSearch.equals(GeneralConst.Number.ZERO)) {
                            codePeriodicite = pd.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode();
                        }

                        //float amountPeriode = 0, taux = 0, valeurBase = 0;
                        String devisePeriode = GeneralConst.EMPTY_STRING;

                        palier = AssujettissementBusiness.getPalierByAbTarifAndTypePersonneV2(
                                pd.getAssujetissement().getArticleBudgetaire().getCode(),
                                bien.getFkTarif(),
                                pd.getAssujetissement().getPersonne().getFormeJuridique().getCode(),
                                bien.getFkQuartier(),
                                bien.getTypeBien().getCode());

                        if (palier != null) {

                            devisePeriode = palier.getDevise().getCode();

                        } else {
                            devisePeriode = GeneralConst.Devise.DEVISE_USD;
                        }

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION, med.getMontant());
                        pdJsonObject.addProperty("penaliteDu", med.getMontantPenalite());

                        if (med.getNotePerception() != null) {
                            Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(med.getNotePerception());

                            if (journal != null) {

                                String codeStatePaid = GeneralConst.Number.ONE;
                                pdJsonObject.addProperty("estPayer", codeStatePaid);

                            } else {
                                pdJsonObject.addProperty("estPayer", GeneralConst.Number.ZERO);
                            }
                        }

                        if (med.getDateEcheance() != null) {

                            pdJsonObject.addProperty("accuserExist", GeneralConst.Number.ONE);

                            if (new Date().after(med.getDateEcheance())) {
                                pdJsonObject.addProperty("nextStap", GeneralConst.Number.ONE);
                            } else {
                                pdJsonObject.addProperty("nextStap", GeneralConst.Number.ZERO);
                            }

                            pdJsonObject.addProperty("echeanceRelance", ConvertDate.formatDateToString(med.getDateEcheance()));
                            pdJsonObject.addProperty("receptionRelance", ConvertDate.formatDateToString(med.getDateReception()));

                        } else {
                            pdJsonObject.addProperty("accuserExist", GeneralConst.Number.ZERO);
                            pdJsonObject.addProperty("nextStap", GeneralConst.Number.ZERO);

                            pdJsonObject.addProperty("echeanceRelance", "NON DEFINIE");
                            pdJsonObject.addProperty("receptionRelance", "NON DEFINIE");
                        }

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.NUMERO_MED, med.getId());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION, devisePeriode);

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE, pd.getAssujetissement().getArticleBudgetaire().getCode());

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME, pd.getAssujetissement().getArticleBudgetaire().getIntitule());

                        if (pd.getAssujetissement().getPersonne() != null) {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE,
                                    pd.getAssujetissement().getPersonne().getCode());

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME,
                                    pd.getAssujetissement().getPersonne().toString().toUpperCase());

                            typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                    pd.getAssujetissement().getPersonne().getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                    pd.getAssujetissement().getPersonne().getCode());

                            if (adresse != null) {
                                adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat("</span>"));

                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        adresse.getId());
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        adresse.toString().toUpperCase());

                            } else {
                                adresseAssujetti = GeneralConst.EMPTY_STRING;
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE,
                                        GeneralConst.EMPTY_STRING);
                                pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME,
                                        GeneralConst.EMPTY_STRING);
                            }

                            assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(pd.getAssujetissement().getPersonne().toString().toUpperCase()).concat("</span>"));

                            assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, assujettiNameComposite);

                        } else {

                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE, GeneralConst.EMPTY_STRING);
                            pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);

                        }
                    } else {

                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ASSUJETTI_NAME_COMPOSITE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_CODE, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.ADRESSE_NAME, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.AMOUNT_PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.DEVISE_PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                    }

                    Date date = ConvertDate.addDayOfDate(med.getDateCreat(), 5);
                    if (date.after(new Date())) {
                        pdJsonObject.addProperty("depassement", 0);
                    } else {
                        pdJsonObject.addProperty("depassement", 1);
                    }

                    pdJsonObject.addProperty(RecouvrementConst.ParamName.ECHEANCE_DECLARATION, ConvertDate.formatDateToString(pd.getDateLimite()));

                    String echeance = Tools.getPeriodeIntitule(pd.getDebut(), codePeriodicite);

                    if (!echeance.isEmpty()) {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, echeance.toUpperCase());
                    } else {
                        pdJsonObject.addProperty(RecouvrementConst.ParamName.PERIODE_DECLARATION, GeneralConst.EMPTY_STRING);
                    }

                    pdJsonList.add(pdJsonObject);
                }

                dataReturn = pdJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String taxationOffice(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_CODE);
            String libelleArticleBudgetaire = request.getParameter(RecouvrementConst.ParamName.ARTICLE_BUDGETAIRE_NAME);
            String assujettiCode = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_CODE);
            String assujettiName = request.getParameter(RecouvrementConst.ParamName.ASSUJETTI_NAME);
            String agentCreat = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            String periodeDeclarationId = request.getParameter(RecouvrementConst.ParamName.PERIODE_DECLARATION_ID);
            String numeroMed = request.getParameter(RecouvrementConst.ParamName.NUMERO_MED);
            String sTaux = request.getParameter("percent");
            String penaliteDu = request.getParameter("penaliteDu");
            String devisePeriodeDeclaration = request.getParameter("devisePeriodeDeclaration");
            String amountPeriodeDeclaration = request.getParameter("amountPeriodeDeclaration");
            String observation = request.getParameter("inputObservation");
            String periodeDeclarationName = request.getParameter("periodeDeclarationName");

            String banqueCode = request.getParameter("codeBanque");
            String compteBancaireCode = request.getParameter("codeCompteBancaire");

            RetraitDeclaration retraitDeclaration = new RetraitDeclaration();

            BigDecimal amountPrincipal = new BigDecimal(0);
            amountPrincipal = amountPrincipal.add(BigDecimal.valueOf(Double.valueOf(amountPeriodeDeclaration)));

            BigDecimal amountPenalite = new BigDecimal(0);
            amountPenalite = amountPenalite.add(BigDecimal.valueOf(Double.valueOf(penaliteDu)));

            float penaliteCalculate = 0;

            switch (sTaux) {
                case "100":
                    penaliteCalculate = amountPenalite.floatValue();
                    break;
                default:
                    penaliteCalculate = (amountPenalite.floatValue() * Float.valueOf(sTaux) / 100);
                    break;
            }

            retraitDeclaration.setFkAssujetti(assujettiCode);
            retraitDeclaration.setRequerant(assujettiName);
            retraitDeclaration.setFkAb(codeArticleBudgetaire);
            retraitDeclaration.setFkPeriode(periodeDeclarationId);
            retraitDeclaration.setFkAgentCreate(agentCreat);
            retraitDeclaration.setMontant(amountPrincipal);
            retraitDeclaration.setDevise(devisePeriodeDeclaration);
            retraitDeclaration.setCodeDeclaration("DEC-" + periodeDeclarationId);
            retraitDeclaration.setEstPenalise(1);
            retraitDeclaration.setEstTaxationOffice(1);
            retraitDeclaration.setObservationRemise(null);
            retraitDeclaration.setTauxRemise(Integer.valueOf(sTaux));

            retraitDeclaration.setFkBanque(banqueCode);
            retraitDeclaration.setFkCompteBancaire(compteBancaireCode);

            if (RecouvrementBusiness.saveTaxationOffice(retraitDeclaration, amountPenalite.floatValue(),
                    penaliteCalculate, numeroMed, observation, periodeDeclarationName, libelleArticleBudgetaire.toUpperCase())) {

                PrintDocument printDocument = new PrintDocument();

                Med med = RecouvrementBusiness.getMedById(numeroMed);
                Amr amrMed = PoursuiteBusiness.getAmrByMedV2(numeroMed);

                RetraitDeclaration rd = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");

                amrMed.setFkRetraitDeclaration(rd == null ? null : rd.getId());

                dataReturn = printDocument.createAmrOfInvitationApayer(amrMed, agentCreat, med, 2,
                        BigDecimal.valueOf(Double.valueOf(Float.valueOf(penaliteCalculate))));

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadRetraitDeclaration(HttpServletRequest request) {

        String valueSearch, typeSearch, typeRegister, allSite, allService,
                codeSite, codeService, userId = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            allSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            allService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            List<RetraitDeclaration> listRetraitDeclaration = new ArrayList<>();

            listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclaration(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    false,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            if (listRetraitDeclaration != null && !listRetraitDeclaration.isEmpty()) {

                List<ArchiveAccuseReception> archiveAccuseReceptions = new ArrayList<>();

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    Bordereau bordereau;
                    PeriodeDeclaration periodeDeclarat;
                    String intitulePeriode;
                    //ArchiveAccuseReception archiveAccuseReception = new ArchiveAccuseReception();

                    jsonObject.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    bordereau = DeclarationBusiness.getBordereau(retraitDecl.getCodeDeclaration().trim());

                    if (bordereau != null) {

                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                bordereau.getTotalMontantPercu());

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ONE);

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                GeneralConst.Numeric.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ZERO);

                    }

                    /*archiveAccuseReception = DeclarationBusiness.getArchivePreuvePaiementByDeclaration(
                     retraitDecl.getCodeDeclaration().trim());

                     if (archiveAccuseReception != null) {

                     jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                     GeneralConst.Number.ONE);

                     jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                     archiveAccuseReception.getArchive());

                     } else {

                     jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                     GeneralConst.Number.ZERO);

                     jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                     GeneralConst.EMPTY_STRING);

                     }*/
                    archiveAccuseReceptions = DeclarationBusiness.getListArchivePreuvePaiementByDeclaration(
                            retraitDecl.getCodeDeclaration().trim());

                    if (!archiveAccuseReceptions.isEmpty()) {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ONE);

                        List<JsonObject> jsonArchiveList = new ArrayList<>();

                        for (ArchiveAccuseReception aar : archiveAccuseReceptions) {

                            JsonObject jsonArchive = new JsonObject();

                            jsonArchive.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT,
                                    aar.getArchive());

                            jsonArchive.addProperty(DeclarationConst.ParamName.NUMERO_NC,
                                    aar.getDocumentReference());

                            jsonArchive.addProperty(DeclarationConst.ParamName.OBSERVATION_DOC,
                                    aar.getObservation() == null ? GeneralConst.EMPTY_STRING : aar.getObservation().trim());

                            TypeDocument typeDocument = DeclarationBusiness.getTypeDocumentByCode(aar.getTypeDocument());

                            if (typeDocument != null) {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        typeDocument.getIntitule());
                            } else {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            jsonArchiveList.add(jsonArchive);

                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                jsonArchiveList.toString());

                    } else {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);

                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    jsonObject.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    article = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb().trim());

                    if (article != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? retraitDecl.getMontant()
                                    : GeneralConst.EMPTY_ZERO);

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                            periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().concat(GeneralConst.BRAKET_CLOSE)));
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    jsonObject.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadRetraitDeclarationBySearchAvanced(HttpServletRequest request) {

        String codeSite, codeService, dateDebut, dateFin, typeRegister, declarationOnlineOnly = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            codeService = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SERVICE_CODE);
            dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            declarationOnlineOnly = request.getParameter(TaxationConst.ParamName.DECLARATION_ONLINE);
            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            List<RetraitDeclaration> listRetraitDeclaration = new ArrayList<>();

            listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclarationBySearchAvanced(
                    codeSite.trim(),
                    codeService,
                    dateDebut, dateFin, declarationOnlineOnly);

            if (!listRetraitDeclaration.isEmpty()) {

                List<ArchiveAccuseReception> archiveAccuseReceptions = new ArrayList<>();

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;
                    String intitulePeriode;
                    //ArchiveAccuseReception archiveAccuseReception = new ArchiveAccuseReception();

                    Bordereau bordereau = DeclarationBusiness.getBordereau(retraitDecl.getCodeDeclaration().trim());

                    if (bordereau != null) {

                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                bordereau.getTotalMontantPercu());

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ONE);

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                GeneralConst.Numeric.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ZERO);

                    }

                    archiveAccuseReceptions = DeclarationBusiness.getListArchivePreuvePaiementByDeclaration(
                            retraitDecl.getCodeDeclaration().trim());

                    if (!archiveAccuseReceptions.isEmpty()) {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ONE);

                        List<JsonObject> jsonArchiveList = new ArrayList<>();

                        for (ArchiveAccuseReception aar : archiveAccuseReceptions) {

                            JsonObject jsonArchive = new JsonObject();

                            jsonArchive.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT,
                                    aar.getArchive());

                            jsonArchive.addProperty(DeclarationConst.ParamName.NUMERO_NC,
                                    aar.getDocumentReference());

                            jsonArchive.addProperty(DeclarationConst.ParamName.OBSERVATION_DOC,
                                    aar.getObservation() == null ? GeneralConst.EMPTY_STRING : aar.getObservation().trim());

                            TypeDocument typeDocument = DeclarationBusiness.getTypeDocumentByCode(aar.getTypeDocument());

                            if (typeDocument != null) {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        typeDocument.getIntitule());
                            } else {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            jsonArchiveList.add(jsonArchive);

                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                jsonArchiveList.toString());

                    } else {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);

                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    jsonObject.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    article = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb().trim());

                    if (article != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? retraitDecl.getMontant()
                                    : GeneralConst.EMPTY_ZERO);

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                            periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().concat(GeneralConst.BRAKET_CLOSE)));
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    jsonObject.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String reprintRecepisse(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        RecepissePrint recepisse = new RecepissePrint();
        PrintDocument printDocument = new PrintDocument();

        try {

            String declaration = request.getParameter(DeclarationConst.ParamName.CODE_DEPOT_DECLARATION);

            if (declaration != null) {

                Archive archive = getArchiveByRefDocument(declaration);

                if (archive != null) {

                    recepisse.setNumDepotDeclaration(archive.getDossier().trim());

                    dataReturn = printDocument.createRecepisse(recepisse, true);

                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String printDocument(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String referenceDocument = GeneralConst.EMPTY_STRING;
        String typeDocument = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        Archive archive = new Archive();

        try {

            referenceDocument = request.getParameter(PoursuiteConst.ParamName.REFERENCE_DOCUMENT);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);
            typeDocument = request.getParameter(PoursuiteConst.ParamName.TYPE_DOCUMENT);

            archive = NotePerceptionBusiness.getArchiveByRefDocument(referenceDocument);

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                PrintDocument printDocument = new PrintDocument();

                switch (typeDocument) {
                    case DocumentConst.DocumentCode.CONTRAINTE:

                        Contrainte contrainte = PoursuiteBusiness.getContrainteByCode(referenceDocument);

                        if (contrainte != null) {

                            dataReturn = printDocument.createContrainte(contrainte, userId);
                        }

                        break;
                    case DocumentConst.DocumentCode.COMMANDEMENT:

                        Commandement commandement = PoursuiteBusiness.getCommandementByCode(referenceDocument);

                        if (commandement != null) {
                            dataReturn = printDocument.createCommandement(commandement, userId);
                        }

                        break;
                    default:

                        BonAPayer bonAPayer = PoursuiteBusiness.getBonAPayerByCode(referenceDocument);

                        if (bonAPayer != null) {
                            dataReturn = printDocument.createBonAPayer(bonAPayer);
                        }
                        break;
                }
            }

        } catch (Exception e) {

            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getAvisMiseEnRecouvrement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String codeService = GeneralConst.EMPTY_STRING;
        String codeTypeAmr = GeneralConst.EMPTY_STRING;
        String codeStateAmr = GeneralConst.EMPTY_STRING;
        String advancedSearch = GeneralConst.EMPTY_STRING;
        String valueSearch = GeneralConst.EMPTY_STRING;
        String typeSearch = GeneralConst.EMPTY_STRING;

        try {

            dateDebut = request.getParameter(PoursuiteConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(PoursuiteConst.ParamName.DATE_FIN);
            codeService = request.getParameter(PoursuiteConst.ParamName.CODE_SERVICE);
            codeTypeAmr = request.getParameter(PoursuiteConst.ParamName.CODE_TYPE_AMR);
            codeStateAmr = request.getParameter(PoursuiteConst.ParamName.CODE_STATE_AMR);
            advancedSearch = request.getParameter(PoursuiteConst.ParamName.ADVANCED_SEARCH);
            valueSearch = request.getParameter(PoursuiteConst.ParamName.VALUE_SEARCH);
            typeSearch = request.getParameter(PoursuiteConst.ParamName.TYPE_SEARCH);

            List<Amr> amrList = new ArrayList<>();

            switch (advancedSearch) {

                case GeneralConst.Number.ZERO:

                    amrList = PoursuiteBusiness.getListAmrBySimpleSearch(typeSearch, valueSearch);

                    break;

                case GeneralConst.Number.ONE:

                    amrList = PoursuiteBusiness.getListAmrByAdvancedSearch(codeService,
                            codeTypeAmr, codeStateAmr, dateDebut, dateFin);

                    break;
            }

            if (!amrList.isEmpty()) {

                List<JsonObject> amrJsonObjList = new ArrayList<>();

                for (Amr amr : amrList) {

                    JsonObject amrJsonObj = new JsonObject();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    Archive archive = new Archive();

                    String deviseAmr = GeneralConst.EMPTY_STRING;

                    if (!amr.getContrainteList().isEmpty()) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ONE);
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.POURSUITE_EXIST, GeneralConst.Number.ZERO);
                    }

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.NUMERO_AMR, amr.getNumero());

                    Date dateCreate = Tools.formatStringFullToDate(amr.getDateCreat());

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_CREATE_AMR,
                            Tools.formatDateToStringV2(dateCreate));

                    if (amr.getDateEcheance() != null) {

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                ConvertDate.formatDateToString(amr.getDateEcheance()));

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                Math.abs(ConvertDate.getMonthsBetween(amr.getDateEcheance(), new Date())));

                        if (amr.getDateEcheance().before(new Date())) {

                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ONE);

                        } else {
                            amrJsonObj.addProperty(PoursuiteConst.ParamName.ACTIVATE_NEXT_STAP,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_LIMITE_AMR,
                                GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (amr.getDateReceptionAmr() != null) {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                ConvertDate.formatDateToString(amr.getDateReceptionAmr()));
                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DATE_RECEPTION_AMR,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (amr.getFichePriseCharge() != null) {

                        deviseAmr = amr.getFichePriseCharge().getDevise();
                        personne = amr.getFichePriseCharge().getPersonne() != null ? amr.getFichePriseCharge().getPersonne() : null;
                    }

                    if (personne != null) {

                        String typeAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiName = GeneralConst.EMPTY_STRING;
                        String adresseAssujetti = GeneralConst.EMPTY_STRING;
                        String assujettiNameComposite = GeneralConst.EMPTY_STRING;

                        typeAssujetti = "Type : ".concat(GeneralConst.SPACE).concat("<span style='font-weight:bold'>".concat(
                                personne.getFormeJuridique().getIntitule().toUpperCase().concat("</span>")));

                        assujettiName = "Assujetti : ".concat("<span style='font-weight:bold'>".concat(
                                personne.toString().toUpperCase()).concat("</span>"));

                        adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                        if (adresse != null) {

                            adresseAssujetti = "Adresse : ".concat(GeneralConst.SPACE).concat(
                                    "<span style='font-weight:bold'>".concat(adresse.toString().toUpperCase()).concat(
                                            "</span>"));

                        }

                        assujettiNameComposite = typeAssujetti.concat("<br/>").concat(adresseAssujetti).concat("<br/>").concat(assujettiName);

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                assujettiNameComposite);

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.ASSUJETTI_NAME_COMPOSITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    amrJsonObj.addProperty(PoursuiteConst.ParamName.TYPE_AMR, amr.getTypeAmr());
                    amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PRINCIPAL_AMR, amr.getNetAPayer());
                    amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PRINCIPAL_AMR_WITH_PENALITE, amr.getNetAPayer());
                    amrJsonObj.addProperty(PoursuiteConst.ParamName.AMOUNT_PAID_AMR, amr.getPayer());
                    amrJsonObj.addProperty(PoursuiteConst.ParamName.DEVISE_AMR, deviseAmr);

                    archive = NotePerceptionBusiness.getArchiveByRefDocument(amr.getNumero());

                    if (archive != null) {

                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, archive.getDocumentString());
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ONE);

                    } else {
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.DOCUMENT_PRINT, GeneralConst.EMPTY_STRING);
                        amrJsonObj.addProperty(PoursuiteConst.ParamName.PRINT_EXISTS, GeneralConst.Number.ZERO);
                    }

                    amrJsonObjList.add(amrJsonObj);

                }

                dataReturn = amrJsonObjList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printAmr(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String numeroAmr = GeneralConst.EMPTY_STRING;
        String userId = GeneralConst.EMPTY_STRING;

        try {
            numeroAmr = request.getParameter(PoursuiteConst.ParamName.NUMERO_AMR);
            userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);

            Amr amr = PoursuiteBusiness.getAmrByNumero(numeroAmr);

            if (amr != null) {

                PrintDocument printDocument = new PrintDocument();

                dataReturn = printDocument.createAmr(amr, userId);

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String generateContrainte(HttpServletRequest request) {

        String dataReturn;

        try {

            String numeroAmr = request.getParameter(PoursuiteConst.ParamName.NUMERO_AMR);
            String userId = request.getParameter(PoursuiteConst.ParamName.USER_ID);
            String numberMonth = request.getParameter(PoursuiteConst.ParamName.NOMBRE_MOIS_REATARD);

            dataReturn = PoursuiteBusiness.saveContrainte(numeroAmr, userId, numberMonth);

            if (!dataReturn.isEmpty()) {

                Contrainte contrainte = PoursuiteBusiness.getContrainteByCode(dataReturn);

                if (contrainte != null) {

                    PrintDocument printDocument = new PrintDocument();

                    dataReturn = printDocument.createContrainte(contrainte, userId);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
