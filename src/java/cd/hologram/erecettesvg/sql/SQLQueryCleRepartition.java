/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author enochbukasa
 */
public class SQLQueryCleRepartition {

    public static String SELECT_CLE_REPARTITION_BY_CODE = "SELECT CL.* FROM T_CLE_REPARTITION CL WITH (READPAST) WHERE CL.FK_ARTICLE_BUDGETAIRE = ?1";
    public static String SELECT_CLE_REPARTITION_BY_CODE_TRESOR = "SELECT CL.* FROM T_CLE_REPARTITION CL WITH (READPAST) WHERE CL.FK_ARTICLE_BUDGETAIRE = ?1 AND IS_TRESOR_PART = 1";
    public static String SELECT_ARTICLE_BUDGETAIRE_REPARTITION_BY_CODE = "SELECT AB.* FROM T_ARTICLE_BUDGETAIRE AB\n"
            + "INNER JOIN T_ARTICLE_GENERIQUE AG ON AB.ARTICLE_GENERIQUE = AG.CODE\n"
            + "INNER JOIN T_SERVICE SR ON AG.SERVICE_ASSIETTE = SR.CODE\n"
            + "INNER JOIN T_SERVICE MI ON SR.SERVICE = MI.CODE\n"
            + "WHERE MI.CODE =?1 AND AB.CODE NOT IN(SELECT CR.FK_ARTICLE_BUDGETAIRE FROM T_CLE_REPARTITION CR WHERE CR.ETAT = 1)";
     public static String DELETE_CLE_REPARTITION = "DELETE FROM T_CLE_REPARTITION WHERE CODE = ?1";

}
