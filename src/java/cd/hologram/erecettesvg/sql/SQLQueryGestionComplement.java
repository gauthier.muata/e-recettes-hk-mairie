/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author WILLY
 */
public class SQLQueryGestionComplement {

    public static String GET_COMPLEMENT_FORME_BY_TYPE = "SELECT * FROM T_COMPLEMENT_FORME CF WITH (READPAST) "
            + "WHERE CF.FORME_JURIDIQUE IN (SELECT FJ.CODE FROM T_FORME_JURIDIQUE FJ WITH (READPAST)"
            + " WHERE FJ.ETAT = 1 AND FJ.CODE = ?1) AND CF.ETAT = 1";

    public static String SELECT_TYPE_COMPLEMENT_BY_INTITULE = "SELECT TC.* FROM T_TYPE_COMPLEMENT TC WITH (READPAST) "
            + "WHERE TC.CODE NOT IN (SELECT CF.COMPLEMENT FROM T_COMPLEMENT_FORME CF WITH (READPAST)"
            + " %s %s";

    public static String GET_CATEGORIE_TYPE_COMPLEMENT_BY_CODE = "SELECT * FROM T_CATEGORIE_TYPE_COMPLEMENT WITH (READPAST)"
            + "WHERE CODE = ?1 ";
    
     public static String AFFECTER_COMPLEMENT_FORME = ":INSERT INTO T_COMPLEMENT_FORME(FORME_JURIDIQUE,COMPLEMENT "
            + ",ETAT) VALUES(?1,?2,?3)";
     
     public static String DESACTIVER_COMPLEMENT_FORME = "UPDATE T_COMPLEMENT_FORME SET ETAT = ?1 WHERE CODE = ?2";
     
     public static String SELECT_ARTICLE_COMPLEMENT_BY_CODE = "SELECT * FROM T_AB_COMPLEMENT_BIEN WITH (READPAST)"
            + "WHERE CODE = ?1 ";
     
     public static String SELECT_ARTICLE_NON_COMPLEMENT_BY_INTITULE = "SELECT * FROM T_ARTICLE_BUDGETAIRE WITH (READPAST)"
            + " WHERE CODE NOT IN (SELECT ARTICLE_BUDGETAIRE "
             + " FROM T_AB_COMPLEMENT_BIEN WITH (READPAST) WHERE ETAT = 1) AND ETAT = 1"
             + " AND INTITULE LIKE ?1 ORDER BY INTITULE";
     
     public static String SELECT_COMPLEMENT_BIEN_BY_TYPE = "SELECT * FROM T_COMPLEMENT_FORME CF WITH (READPAST) "
            + "WHERE CF.FORME_JURIDIQUE IN (SELECT FJ.CODE FROM T_FORME_JURIDIQUE FJ WITH (READPAST)"
            + " WHERE FJ.ETAT = 1 AND FJ.CODE = ?1) AND CF.ETAT = 1";
     
     public static String SELECT_TYPE_COMPLEMENT_BIEN = "SELECT TC.* FROM T_TYPE_COMPLEMENT TC WITH (READPAST) "
            + "WHERE TC.CODE NOT IN (SELECT TCB.COMPLEMENT FROM T_TYPE_COMPLEMENT_BIEN TCB WITH (READPAST)"
            + " %s %s";
     
      public static String F_NEW_COMPLEMENT_TYPE_BIEN = ":EXEC F_NEW_COMPLEMENT_TYPE_BIEN ?1,?2,?3";      
      
      public static String DESACTIVER_TYPE_COMPLEMENT_BIEN = "UPDATE T_TYPE_COMPLEMENT_BIEN SET ETAT = 0 WHERE CODE = ?1";
      
       public static String SELECT_ARTICLE_COMPLEMENT_BY_TYPE_COMPLEMENT = "SELECT * FROM T_AB_COMPLEMENT_BIEN WITH (READPAST)"
            + "WHERE TYPE_COMPLEMENT_BIEN = ?1 ";
       
       public static String SELECT_ARTICLE_NON_AFFECTER_COMPLEMENT_BIEN = "SELECT AB.* FROM T_ARTICLE_BUDGETAIRE AB WITH (READPAST)"
            + " WHERE AB.ETAT = 1 %s %s ORDER BY AB.INTITULE";
       
       public static String F_NEW_AB_COMPLEMENT_BIEN = ":EXEC F_NEW_AB_COMPLEMENT_BIEN ?1,?2";
       
       public static String UPDATE_AB_COMPLEMENT_BIEN = ":UPDATE T_AB_COMPLEMENT_BIEN SET TYPE_COMPLEMENT_BIEN = ?1"
               + " , ARTICLE_BUDGETAIRE = ?2 WHERE CODE = ?3";
    
}
