/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author gauthier.muata
 */
public class SQLQueryOrdonnancement {

    public static String UPDATE_NOTE_CALCUL = ":UPDATE T_NOTE_CALCUL SET AGENT_VALIDATION = ?1,"
            + " DATE_VALIDATION = CONVERT(DATE,?2,103), AVIS = ?3, ETAT = ?4, OBSERVATION_ORDONNANCEMENT = ?5 "
            + " WHERE NUMERO = ?6";

    public static String UPDATE_SERIE = ":UPDATE T_SERIE SET SERIE_FINIE= ?1,DERNIER_NUMERO_IMPRIME = ?2"
            + ",NOMBRE_NOTE_IMPRIMEE= ?3 WHERE ID = ?4";

}
