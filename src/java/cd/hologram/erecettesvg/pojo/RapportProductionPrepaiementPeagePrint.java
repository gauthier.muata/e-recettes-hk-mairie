/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class RapportProductionPrepaiementPeagePrint implements Serializable{
    
    String datePrint;
    String axepeage;
    String poste;
    String controleurName;
    String controleurCode;
    String controleurPhone;
    String percepteurName;
    String percepteurCode;
    String percepteurPhone;
    
    String ppfV;
    String ppfVL;
    String ppfCIP;
    String ppfBM;
    String ppfTOTAL;
    
    String ppfC2P;
    String ppfCR;
    String ppfCRTR;
    String ppfVIT;
    String ppfVLIT;
    String ppfCIPBM;
    String ppfTOTAL2;
       
    String totlGenTour;
    String observation;
    String lieuPrint;

    public String getDatePrint() {
        return datePrint;
    }

    public void setDatePrint(String datePrint) {
        this.datePrint = datePrint;
    }

    public String getAxepeage() {
        return axepeage;
    }

    public void setAxepeage(String axepeage) {
        this.axepeage = axepeage;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getControleurName() {
        return controleurName;
    }

    public void setControleurName(String controleurName) {
        this.controleurName = controleurName;
    }

    public String getControleurCode() {
        return controleurCode;
    }

    public void setControleurCode(String controleurCode) {
        this.controleurCode = controleurCode;
    }

    public String getControleurPhone() {
        return controleurPhone;
    }

    public void setControleurPhone(String controleurPhone) {
        this.controleurPhone = controleurPhone;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getPercepteurCode() {
        return percepteurCode;
    }

    public void setPercepteurCode(String percepteurCode) {
        this.percepteurCode = percepteurCode;
    }

    public String getPercepteurPhone() {
        return percepteurPhone;
    }

    public void setPercepteurPhone(String percepteurPhone) {
        this.percepteurPhone = percepteurPhone;
    }

    public String getPpfV() {
        return ppfV;
    }

    public void setPpfV(String ppfV) {
        this.ppfV = ppfV;
    }

    public String getPpfVL() {
        return ppfVL;
    }

    public void setPpfVL(String ppfVL) {
        this.ppfVL = ppfVL;
    }

    public String getPpfCIP() {
        return ppfCIP;
    }

    public void setPpfCIP(String ppfCIP) {
        this.ppfCIP = ppfCIP;
    }

    public String getPpfBM() {
        return ppfBM;
    }

    public void setPpfBM(String ppfBM) {
        this.ppfBM = ppfBM;
    }

    public String getPpfTOTAL() {
        return ppfTOTAL;
    }

    public void setPpfTOTAL(String ppfTOTAL) {
        this.ppfTOTAL = ppfTOTAL;
    }

    public String getPpfC2P() {
        return ppfC2P;
    }

    public void setPpfC2P(String ppfC2P) {
        this.ppfC2P = ppfC2P;
    }

    public String getPpfCR() {
        return ppfCR;
    }

    public void setPpfCR(String ppfCR) {
        this.ppfCR = ppfCR;
    }

    public String getPpfCRTR() {
        return ppfCRTR;
    }

    public void setPpfCRTR(String ppfCRTR) {
        this.ppfCRTR = ppfCRTR;
    }

    public String getPpfVIT() {
        return ppfVIT;
    }

    public void setPpfVIT(String ppfVIT) {
        this.ppfVIT = ppfVIT;
    }

    public String getPpfVLIT() {
        return ppfVLIT;
    }

    public void setPpfVLIT(String ppfVLIT) {
        this.ppfVLIT = ppfVLIT;
    }

    public String getPpfCIPBM() {
        return ppfCIPBM;
    }

    public void setPpfCIPBM(String ppfCIPBM) {
        this.ppfCIPBM = ppfCIPBM;
    }

    public String getPpfTOTAL2() {
        return ppfTOTAL2;
    }

    public void setPpfTOTAL2(String ppfTOTAL2) {
        this.ppfTOTAL2 = ppfTOTAL2;
    }

    public String getTotlGenTour() {
        return totlGenTour;
    }

    public void setTotlGenTour(String totlGenTour) {
        this.totlGenTour = totlGenTour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

   
    
}
