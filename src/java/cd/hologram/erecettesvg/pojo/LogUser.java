/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import cd.hologram.erecettesvg.util.ClientInfo;
import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author gauthier.muata
 */
public class LogUser implements Serializable {

    String macAddress;
    String ipAddress;
    String hostName;
    String referenceOccurence;
    String event;
    String browserContext;

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getReferenceOccurence() {
        return referenceOccurence;
    }

    public void setReferenceOccurence(String referenceOccurence) {
        this.referenceOccurence = referenceOccurence;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getBrowserContext() {
        return browserContext;
    }

    public void setBrowserContext(String browserContext) {
        this.browserContext = browserContext;
    }

    public LogUser(HttpServletRequest request, String eventValue) {
        
        String ip = ClientInfo.getClientIpAddr(request);
        String mac = ClientInfo.getMACAddress(ip.trim());
        String browser = ClientInfo.getClientInfo(request);
        String host = ClientInfo.getHostName(request);
        
        setMacAddress(mac.trim());
        setIpAddress(ip.trim());
        setBrowserContext(browser.trim());
        setEvent(eventValue.trim());
        setHostName(host.trim());

    }

    @Override
    public String toString() {
        return "LogUser{" + "macAddress=" + macAddress + ", ipAddress=" + ipAddress + ", hostName=" + hostName + ", referenceOccurence=" + referenceOccurence + ", event=" + event + ", browserContext=" + browserContext + '}';
    }
    
    

}
