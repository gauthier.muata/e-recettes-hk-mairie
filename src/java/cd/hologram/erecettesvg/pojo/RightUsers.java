/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class RightUsers implements Serializable {

    String droit;

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }
    
    @Override
    public String toString() {
        return droit;
    }

}
