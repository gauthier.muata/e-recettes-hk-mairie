/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class AmrPrint implements Serializable {
    
    String id;
    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;
    
    String numeroAmr;
    String dateEcheance;
    String amountToChiffre;
    String motif;
    String detailMotif;
    String documentReference;
    String provinceAmr;
    String accountBankList;
    
    String idTax;
    String annee;
    String articleName;
    String base;
    String taux;
    String droit;
    String major;
    String penal;
    String amende;
    String degrev;
    String amountToLetter;
    
    String userPrintName;
    String userPrintFunction;
    
    String amountTotal;
    
    String typeAmr;
    
    String accountBankInfo;
    String amountAmr2;
    String numeroDocument;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroAmr() {
        return numeroAmr;
    }

    public void setNumeroAmr(String numeroAmr) {
        this.numeroAmr = numeroAmr;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getAmountToChiffre() {
        return amountToChiffre;
    }

    public void setAmountToChiffre(String amountToChiffre) {
        this.amountToChiffre = amountToChiffre;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getDetailMotif() {
        return detailMotif;
    }

    public void setDetailMotif(String detailMotif) {
        this.detailMotif = detailMotif;
    }

    public String getDocumentReference() {
        return documentReference;
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public String getProvinceAmr() {
        return provinceAmr;
    }

    public void setProvinceAmr(String provinceAmr) {
        this.provinceAmr = provinceAmr;
    }

    public String getAccountBankList() {
        return accountBankList;
    }

    public void setAccountBankList(String accountBankList) {
        this.accountBankList = accountBankList;
    }

    public String getIdTax() {
        return idTax;
    }

    public void setIdTax(String idTax) {
        this.idTax = idTax;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getTaux() {
        return taux;
    }

    public void setTaux(String taux) {
        this.taux = taux;
    }

    public String getDroit() {
        return droit;
    }

    public void setDroit(String droit) {
        this.droit = droit;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getPenal() {
        return penal;
    }

    public void setPenal(String penal) {
        this.penal = penal;
    }

    public String getAmende() {
        return amende;
    }

    public void setAmende(String amende) {
        this.amende = amende;
    }

    public String getDegrev() {
        return degrev;
    }

    public void setDegrev(String degrev) {
        this.degrev = degrev;
    }

    public String getAmountToLetter() {
        return amountToLetter;
    }

    public void setAmountToLetter(String amountToLetter) {
        this.amountToLetter = amountToLetter;
    }

    public String getUserPrintName() {
        return userPrintName;
    }

    public void setUserPrintName(String userPrintName) {
        this.userPrintName = userPrintName;
    }

    public String getUserPrintFunction() {
        return userPrintFunction;
    }

    public void setUserPrintFunction(String userPrintFunction) {
        this.userPrintFunction = userPrintFunction;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getTypeAmr() {
        return typeAmr;
    }

    public void setTypeAmr(String typeAmr) {
        this.typeAmr = typeAmr;
    }

    public String getAccountBankInfo() {
        return accountBankInfo;
    }

    public void setAccountBankInfo(String accountBankInfo) {
        this.accountBankInfo = accountBankInfo;
    }

    public String getAmountAmr2() {
        return amountAmr2;
    }

    public void setAmountAmr2(String amountAmr2) {
        this.amountAmr2 = amountAmr2;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
}
