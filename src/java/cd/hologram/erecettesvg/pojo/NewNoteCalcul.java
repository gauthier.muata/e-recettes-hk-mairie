/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

/**
 *
 * @author gauthier.muata
 */
public class NewNoteCalcul {
    
    String codePersonne;
    String codeAdresse;
    String exerciceFiscal;
    String codeSite;
    String codeService;
    String agentCreate;
    String dateCreate;
    String codeDepot;
    String codeBien;
    String codeArticleBudgetaire;

    public String getCodePersonne() {
        return codePersonne;
    }

    public void setCodePersonne(String codePersonne) {
        this.codePersonne = codePersonne;
    }

    public String getCodeAdresse() {
        return codeAdresse;
    }

    public void setCodeAdresse(String codeAdresse) {
        this.codeAdresse = codeAdresse;
    }

    public String getExerciceFiscal() {
        return exerciceFiscal;
    }

    public void setExerciceFiscal(String exerciceFiscal) {
        this.exerciceFiscal = exerciceFiscal;
    }

    public String getCodeSite() {
        return codeSite;
    }

    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    public String getCodeService() {
        return codeService;
    }

    public void setCodeService(String codeService) {
        this.codeService = codeService;
    }

    public String getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(String agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getCodeDepot() {
        return codeDepot;
    }

    public void setCodeDepot(String codeDepot) {
        this.codeDepot = codeDepot;
    }

    public String getCodeBien() {
        return codeBien;
    }

    public void setCodeBien(String codeBien) {
        this.codeBien = codeBien;
    }
    
}
