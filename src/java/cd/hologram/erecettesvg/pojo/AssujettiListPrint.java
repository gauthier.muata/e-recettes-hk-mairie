/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import cd.hologram.erecettesvg.models.DetailsRole;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gauthier.muata
 */
public class AssujettiListPrint implements Serializable {

    public String nif;
    public String raison_Sociale;
    public String type;
    public String telephone;
    public String siege_Social;
    private String codeAssujetti;
    private String articleExtraitRole;
    private String periodeExists;

    private List<DetailRolePrint> detailsRoleListOfAssujetti = new ArrayList<>();

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getRaison_Sociale() {
        return raison_Sociale;
    }

    public void setRaison_Sociale(String raison_Sociale) {
        this.raison_Sociale = raison_Sociale;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSiege_Social() {
        return siege_Social;
    }

    public void setSiege_Social(String siege_Social) {
        this.siege_Social = siege_Social;
    }

    public String getCodeAssujetti() {
        return codeAssujetti;
    }

    public void setCodeAssujetti(String codeAssujetti) {
        this.codeAssujetti = codeAssujetti;
    }

    public String getArticleExtraitRole() {
        return articleExtraitRole;
    }

    public void setArticleExtraitRole(String articleExtraitRole) {
        this.articleExtraitRole = articleExtraitRole;
    }

    public List<DetailRolePrint> getDetailsRoleListOfAssujetti() {
        return detailsRoleListOfAssujetti;
    }

    public void setDetailsRoleListOfAssujetti(List<DetailRolePrint> detailsRoleListOfAssujetti) {
        this.detailsRoleListOfAssujetti = detailsRoleListOfAssujetti;
    }

    public String getPeriodeExists() {
        return periodeExists;
    }

    public void setPeriodeExists(String periodeExists) {
        this.periodeExists = periodeExists;
    }

    
    
    

}
