/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import cd.hologram.erecettesvg.models.EntiteAdministrative;

/**
 *
 * @author emmanuel.tsasa
 */
public class EntiteAdresse {

    EntiteAdministrative province;
    EntiteAdministrative ville;
    EntiteAdministrative district;
    EntiteAdministrative commune;
    EntiteAdministrative quartier;
    EntiteAdministrative avenue;

    public EntiteAdministrative getProvince() {
        return province;
    }

    public void setProvince(EntiteAdministrative province) {
        this.province = province;
    }

    public EntiteAdministrative getVille() {
        return ville;
    }

    public void setVille(EntiteAdministrative ville) {
        this.ville = ville;
    }

    public EntiteAdministrative getDistrict() {
        return district;
    }

    public void setDistrict(EntiteAdministrative district) {
        this.district = district;
    }

    public EntiteAdministrative getCommune() {
        return commune;
    }

    public void setCommune(EntiteAdministrative commune) {
        this.commune = commune;
    }

    public EntiteAdministrative getQuartier() {
        return quartier;
    }

    public void setQuartier(EntiteAdministrative quartier) {
        this.quartier = quartier;
    }

    public EntiteAdministrative getAvenue() {
        return avenue;
    }

    public void setAvenue(EntiteAdministrative avenue) {
        this.avenue = avenue;
    }

    @Override
    public String toString() {
        String sVille = (ville == null) ? "" : ville.getIntitule();
        String sCommune = (commune == null) ? "" : commune.getIntitule();
        String sQuartier = (quartier == null) ? "" : quartier.getIntitule();
        String sAvenue = (avenue == null) ? "" : avenue.getIntitule();

        String result = sVille + "--> C/" + sCommune + " - Q/" + sQuartier + " - Av. " + sAvenue + " n°";
        return result;
    }
}
