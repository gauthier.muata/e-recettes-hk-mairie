    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author gauthier.muata
 */
@Entity
public class GetTauxNonFiscal implements Serializable {

    @Column(name = "ARTICLE_BUDGETAIRE")
    @Id
    String articleBudgetaire;
    @Column(name = "TAUX")
    float taux;
    @Column(name = "TYPE_TAUX")
    String typeTaux;
    @Column(name = "MULTIPLIER_VALEUR_BASE")
    boolean multiplier;
    @Column(name = "DEVISE")
    String devise;
    @Column(name = "BORNE_SUPERIEURE")
    float borneSuperieure;
    @Column(name = "BORNE_INFERIEURE")
    float borneInferieure;

    public GetTauxNonFiscal() {
        articleBudgetaire = "";
        taux = (float) 0.0;
        typeTaux = "";
        multiplier = false;
        devise = "";
        borneSuperieure = (float) 0.0;
        borneInferieure = (float) 0.0;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public float getTaux() {
        return taux;
    }

    public void setTaux(float taux) {
        this.taux = taux;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public boolean isMultiplier() {
        return multiplier;
    }

    public void setMultiplier(boolean multiplier) {
        this.multiplier = multiplier;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public float getBorneSuperieure() {
        return borneSuperieure;
    }

    public void setBorneSuperieure(float borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public float getBorneInferieure() {
        return borneInferieure;
    }

    public void setBorneInferieure(float borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

}
