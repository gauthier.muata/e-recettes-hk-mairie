/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class TabDetailRole implements Serializable {

    private String assujettiName;
    public String np;
    public String date;
    public String secteur;
    public String libelle;
    public String principal;
    public String penalite_Assiette;
    public String mois_Retard;
    public String penalite_Recouvrement;
    public String total_Penalite;
    public String total_Tresor;
    public String Bon_A_Payer;
    public String Total_General;

    private String getAssujettiName() {
        return assujettiName;
    }

    private void setAssujettiName(String assujettiName) {
        this.assujettiName = assujettiName;
    }



    public String getSecteur() {
        return secteur;
    }

    public void setSecteur(String secteur) {
        this.secteur = secteur;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getNp() {
        return np;
    }

    public void setNp(String np) {
        this.np = np;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }


    public String getPenalite_Assiette() {
        return penalite_Assiette;
    }

    public void setPenalite_Assiette(String penalite_Assiette) {
        this.penalite_Assiette = penalite_Assiette;
    }

    public String getPenalite_Recouvrement() {
        return penalite_Recouvrement;
    }

    public void setPenalite_Recouvrement(String penalite_Recouvrement) {
        this.penalite_Recouvrement = penalite_Recouvrement;
    }

    public String getTotal_Penalite() {
        return total_Penalite;
    }

    public void setTotal_Penalite(String total_Penalite) {
        this.total_Penalite = total_Penalite;
    }

    public String getTotal_Tresor() {
        return total_Tresor;
    }

    public void setTotal_Tresor(String total_Tresor) {
        this.total_Tresor = total_Tresor;
    }

    public String getBon_A_Payer() {
        return Bon_A_Payer;
    }

    public void setBon_A_Payer(String Bon_A_Payer) {
        this.Bon_A_Payer = Bon_A_Payer;
    }

    public String getTotal_General() {
        return Total_General;
    }

    public void setTotal_General(String Total_General) {
        this.Total_General = Total_General;
    }

    public String getMois_Retard() {
        return mois_Retard;
    }

    public void setMois_Retard(String mois_Retard) {
        this.mois_Retard = mois_Retard;
    }

    
    
    
}
