/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.util.*;
import static cd.hologram.erecettesvg.util.Tools.getDayOfMonthByYear;
import java.util.*;
import org.apache.commons.lang.StringUtils;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
public class PeriodiciteData {

    public static List<JSONObject> getPeriodeJournaliere(Date dateJour, int length, String codePeriodicite, String nbreJourLimitePaiement) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            for (int i = 0; i < length; i++) {

                String date = ConvertDate.formatDateToStringOfFormat(dateJour, GeneralConst.Format.FORMAT_DATE);

                JSONObject jsonPeriode = new JSONObject();
                jsonPeriode.put(AssujettissementConst.ParamName.ORDRE_PERIODE, (i + 1));
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_DEBUT, date);
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_FIN, date);
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, date);
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, nbreJourLimitePaiement.isEmpty() ? nbreJourLimitePaiement : date);
                jsonPeriode.put(AssujettissementConst.ParamName.PERIODE,
                        Tools.getPeriodeIntitule(dateJour, codePeriodicite));

                jsonPeriodes.add(jsonPeriode);

                dateJour = ConvertDate.addDayOfDate(dateJour, 1);
            }

        } catch (JSONException | NumberFormatException e) {

        }
        return jsonPeriodes;
    }

    public static List<JSONObject> getPeriodesMensuelles(
            int nombreJour,
            boolean forTaxation, Date dateDepart, int moisDepart, int anneeDepart,
            final String day_echeance, String day_echeance_paiement, String codePeriodicite, String periodeEcheance) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            int i = 0;

            while (dateDepart.before(new Date()) || forTaxation) {

                i++;

                JSONObject jsonPeriode = new JSONObject();

                jsonPeriode.put(AssujettissementConst.ParamName.ORDRE_PERIODE, i);

                String dateDebut = "01/" + StringUtils.leftPad(String.valueOf(moisDepart), 2, "0") + "/" + anneeDepart;
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_DEBUT, dateDebut);
                jsonPeriode.put(AssujettissementConst.ParamName.PERIODE,
                        Tools.getPeriodeIntitule(ConvertDate.formatDate(dateDebut), codePeriodicite));

                switch (codePeriodicite) {
                    case GeneralConst.Periodicite.BIMENS:
                        moisDepart += 1;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                        break;
                    case GeneralConst.Periodicite.TRIME:
                        moisDepart += 2;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                        break;
                    case GeneralConst.Periodicite.SEMS:
                        moisDepart += 5;
                        if (moisDepart > 12) {
                            moisDepart = (moisDepart - 12);
                            anneeDepart += 1;
                        }
                }

                String dateFin = getDayOfMonthByYear(anneeDepart, moisDepart) + "/" + StringUtils.leftPad(String.valueOf(moisDepart), 2, "0") + "/" + anneeDepart;
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_FIN, dateFin);

                if (!day_echeance.equals(GeneralConst.EMPTY_STRING) && !day_echeance.equals(GeneralConst.Number.ZERO)) {

                    String dateLimite;
                    String dateLimitePaiement;

                    int moisFin, anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.Number.ONE)) {
                            moisFin = (moisDepart == 12) ? 1 : moisDepart + 1;
                            anneeFin = (moisDepart == 12) ? (anneeDepart + 1) : anneeDepart;
                        } else {
                            moisFin = moisDepart;
                            anneeFin = anneeDepart;
                        }
                    } else {
                        moisFin = moisDepart;
                        anneeFin = anneeDepart;
                    }

                    int numberDayNextMonth = getDayOfMonthByYear(anneeFin, moisFin);

                    if (numberDayNextMonth < Integer.parseInt(day_echeance)) {
                        dateLimite = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    } else {
                        dateLimite = day_echeance + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;

                    }

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, takeEcheanceDate(dateLimite, true));
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, Compare.before(Tools.formatStringFullToDateV2(dateLimite), new Date()));

                    if (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.Number.ZERO)) {

                        if (numberDayNextMonth < Integer.parseInt(day_echeance_paiement)) {
                            dateLimitePaiement = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                        } else {
                            dateLimitePaiement = day_echeance_paiement + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                        }

                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, takeEcheanceDate(dateLimitePaiement, true));
                        jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    } else {

                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, GeneralConst.NON_DEFINI);
                        jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, false);

                    }

                } else if ((day_echeance.equals(GeneralConst.EMPTY_STRING) || day_echeance.equals(GeneralConst.Number.ZERO)) && (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.Number.ZERO))) {

                    String dateLimitePaiement;

                    int moisFin, anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.Number.ONE)) {
                            moisFin = (moisDepart == 12) ? 1 : moisDepart + 1;
                            anneeFin = (moisDepart == 12) ? (anneeDepart + 1) : anneeDepart;
                        } else {
                            moisFin = moisDepart;
                            anneeFin = anneeDepart;
                        }
                    } else {
                        moisFin = moisDepart;
                        anneeFin = anneeDepart;
                    }

                    int numberDayNextMonth = getDayOfMonthByYear(anneeFin, moisFin);

                    if (numberDayNextMonth < Integer.parseInt(day_echeance_paiement)) {
                        dateLimitePaiement = numberDayNextMonth + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    } else {
                        dateLimitePaiement = day_echeance_paiement + "/" + StringUtils.leftPad(String.valueOf(moisFin), 2, "0") + "/" + anneeFin;
                    }

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, takeEcheanceDate(dateLimitePaiement, true));
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, GeneralConst.NON_DEFINI);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, false);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, false);

                } else {

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, GeneralConst.NON_DEFINI);
                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, GeneralConst.NON_DEFINI);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, false);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, false);
                }

                jsonPeriodes.add(jsonPeriode);

                //Pour incrementer la date
                dateDepart = ConvertDate.addDayOfDate(dateDepart, nombreJour);

                if (moisDepart == 12) {
                    anneeDepart++;
                    moisDepart = 0;
                }

                moisDepart++;

                //Regeneration des 2 periodes en cas de manque
                if (forTaxation) {
                    if (i == 2) {
                        break;
                    }
                }

            }
        } catch (JSONException | NumberFormatException e) {

        }
        return jsonPeriodes;
    }

    public static List<JSONObject> getPeriodesAnnuelles(
            int nombreJour,
            boolean forTaxation, Date dateDepart, int anneeDepart, final String day_echeance,
            final String month_eheance, final String day_echeance_paiement,
            final String month_eheance_paiement, String codePeriodicite, String periodeEcheance) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            int i = 0;

            while (dateDepart.before(new Date()) || forTaxation) {
                i++;

                JSONObject jsonPeriode = new JSONObject();

                jsonPeriode.put(AssujettissementConst.ParamName.ORDRE_PERIODE, i);

                String dateDebut = "01/01/" + anneeDepart;
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_DEBUT, dateDebut);
                jsonPeriode.put(AssujettissementConst.ParamName.PERIODE,
                        Tools.getPeriodeIntitule(ConvertDate.formatDate(dateDebut), codePeriodicite));

                switch (codePeriodicite) {
                    case "2ANS":
                        anneeDepart += 1;
                        break;
                    case "5ANS":
                        anneeDepart += 4;
                }

                String dateFin = "31/12/" + anneeDepart;
                jsonPeriode.put(AssujettissementConst.ParamName.DATE_FIN, dateFin);

                if (!day_echeance.equals(GeneralConst.EMPTY_STRING) && !day_echeance.equals(GeneralConst.Number.ZERO)) {

                    int anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.Number.ONE)) {
                            anneeFin = (anneeDepart + 1);
                        } else {
                            anneeFin = anneeDepart;
                        }
                    } else {
                        anneeFin = anneeDepart;
                    }

                    String dateLimite = day_echeance + "/" + month_eheance + "/" + anneeFin;

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, takeEcheanceDate(dateLimite, true));
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, Compare.before(Tools.formatStringFullToDateV2(dateLimite), new Date()));

                    if (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.Number.ZERO)) {

                        String dateLimitePaiement = day_echeance_paiement + "/" + month_eheance_paiement + "/" + anneeFin;

                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, takeEcheanceDate(dateLimitePaiement, true));
                        jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                    } else {

                        jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, GeneralConst.NON_DEFINI);
                        jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, false);
                    }
                } else if ((day_echeance.equals(GeneralConst.EMPTY_STRING) || day_echeance.equals(GeneralConst.Number.ZERO)) && (!day_echeance_paiement.equals(GeneralConst.EMPTY_STRING) && !day_echeance_paiement.equals(GeneralConst.Number.ZERO))) {

                    int anneeFin;
                    if (!periodeEcheance.equals(GeneralConst.EMPTY_STRING)) {
                        if (periodeEcheance.equals(GeneralConst.Number.ONE)) {
                            anneeFin = (anneeDepart + 1);
                        } else {
                            anneeFin = anneeDepart;
                        }
                    } else {
                        anneeFin = anneeDepart;
                    }

                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, false);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, false);

                    String dateLimitePaiement = day_echeance_paiement + "/" + month_eheance_paiement + "/" + anneeFin;

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, takeEcheanceDate(dateLimitePaiement, true));
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, Compare.before(Tools.formatStringFullToDateV2(dateLimitePaiement), new Date()));

                } else {

                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE, GeneralConst.NON_DEFINI);
                    jsonPeriode.put(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT, GeneralConst.NON_DEFINI);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS, false);
                    jsonPeriode.put(AssujettissementConst.ParamName.IS_ECHUS_PAIEMENT, false);
                }

                jsonPeriodes.add(jsonPeriode);

                //Pour incrementer la date
                dateDepart = ConvertDate.addDayOfDate(dateDepart, nombreJour);

                anneeDepart++;

                //Regeneration des 2 periodes en cas de manque
                if (forTaxation) {
                    if (i == 2) {
                        break;
                    }
                }

            }
        } catch (JSONException | NumberFormatException e) {

        }
        return jsonPeriodes;
    }

    public static String takeEcheanceDate(String echeanceDate, boolean isLegal) {

        Date dateEcheance = ConvertDate.formatDate(echeanceDate);
        dateEcheance = Tools.getEcheanceDate(dateEcheance, 1, isLegal);
        echeanceDate = ConvertDate.formatDateToStringOfFormat(dateEcheance, GeneralConst.Format.FORMAT_DATE);
        return echeanceDate;
    }

}
