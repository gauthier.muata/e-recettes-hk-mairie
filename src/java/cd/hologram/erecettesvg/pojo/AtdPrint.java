/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class AtdPrint implements Serializable {
    
    String id;
    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;
    
    String numeroAtd;

    String datePrintDocument;
    String userPrintDocument;
    String functionUserDocument;
    
    String nomDetenteur;
    String adresseDetenteur;
    String telephoneDetenteur;
    String emailDetenteur;
    
    String amountGlobal;
    String amountPrincipal;
    String compteTresorUrbain;
    String amountPenalite;
    String comptePenalite;
    String amountFonctionnement;
    String compteFonctionnement;
    
    String nomReceveur;
    String dateReceptionDetenteur;
    
    String nomSaisi;
    String qualiteSaisi;
    String dateReceptionSaisi;
    
    String qualiteDetenteur;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getDatePrintDocument() {
        return datePrintDocument;
    }

    public void setDatePrintDocument(String datePrintDocument) {
        this.datePrintDocument = datePrintDocument;
    }

    public String getUserPrintDocument() {
        return userPrintDocument;
    }

    public void setUserPrintDocument(String userPrintDocument) {
        this.userPrintDocument = userPrintDocument;
    }

    public String getFunctionUserDocument() {
        return functionUserDocument;
    }

    public void setFunctionUserDocument(String functionUserDocument) {
        this.functionUserDocument = functionUserDocument;
    }

    public String getNomDetenteur() {
        return nomDetenteur;
    }

    public void setNomDetenteur(String nomDetenteur) {
        this.nomDetenteur = nomDetenteur;
    }

    public String getAdresseDetenteur() {
        return adresseDetenteur;
    }

    public void setAdresseDetenteur(String adresseDetenteur) {
        this.adresseDetenteur = adresseDetenteur;
    }

    public String getTelephoneDetenteur() {
        return telephoneDetenteur;
    }

    public void setTelephoneDetenteur(String telephoneDetenteur) {
        this.telephoneDetenteur = telephoneDetenteur;
    }

    public String getEmailDetenteur() {
        return emailDetenteur;
    }

    public void setEmailDetenteur(String emailDetenteur) {
        this.emailDetenteur = emailDetenteur;
    }

    public String getAmountGlobal() {
        return amountGlobal;
    }

    public void setAmountGlobal(String amountGlobal) {
        this.amountGlobal = amountGlobal;
    }

    public String getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(String amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getCompteTresorUrbain() {
        return compteTresorUrbain;
    }

    public void setCompteTresorUrbain(String compteTresorUrbain) {
        this.compteTresorUrbain = compteTresorUrbain;
    }

    public String getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(String amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public String getComptePenalite() {
        return comptePenalite;
    }

    public void setComptePenalite(String comptePenalite) {
        this.comptePenalite = comptePenalite;
    }

    public String getAmountFonctionnement() {
        return amountFonctionnement;
    }

    public void setAmountFonctionnement(String amountFonctionnement) {
        this.amountFonctionnement = amountFonctionnement;
    }

    public String getCompteFonctionnement() {
        return compteFonctionnement;
    }

    public void setCompteFonctionnement(String compteFonctionnement) {
        this.compteFonctionnement = compteFonctionnement;
    }

    public String getNomReceveur() {
        return nomReceveur;
    }

    public void setNomReceveur(String nomReceveur) {
        this.nomReceveur = nomReceveur;
    }

    public String getDateReceptionDetenteur() {
        return dateReceptionDetenteur;
    }

    public void setDateReceptionDetenteur(String dateReceptionDetenteur) {
        this.dateReceptionDetenteur = dateReceptionDetenteur;
    }

    public String getNomSaisi() {
        return nomSaisi;
    }

    public void setNomSaisi(String nomSaisi) {
        this.nomSaisi = nomSaisi;
    }

    

    public String getQualiteSaisi() {
        return qualiteSaisi;
    }

    public void setQualiteSaisi(String qualiteSaisi) {
        this.qualiteSaisi = qualiteSaisi;
    }

    public String getDateReceptionSaisi() {
        return dateReceptionSaisi;
    }

    public void setDateReceptionSaisi(String dateReceptionSaisi) {
        this.dateReceptionSaisi = dateReceptionSaisi;
    }

    public String getNumeroAtd() {
        return numeroAtd;
    }

    public void setNumeroAtd(String numeroAtd) {
        this.numeroAtd = numeroAtd;
    }

    public String getQualiteDetenteur() {
        return qualiteDetenteur;
    }

    public void setQualiteDetenteur(String qualiteDetenteur) {
        this.qualiteDetenteur = qualiteDetenteur;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    

}
