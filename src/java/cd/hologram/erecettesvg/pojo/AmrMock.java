/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class AmrMock implements Serializable{
    
    String id;
    String logo;
    String codeQr;
    String lieuPrint;
    String dateImpression;
    String dateEcheance;
    String numeroAmr;
    String contribuable;
    String adresseContribuable;
    String amountTotal;
    String amountPrincipal;
    String accountPrincipal;
    String bankAccountPrincipal;
    String fiftyPenaliteA;
    String accountPenaliteA;
    String bankAccountPenaliteA;
    String fiftyPenaliteB;
    String accountPenaliteB;
    String bankAccountPenaliteB;
    String motif;
    String documentReference;
    String detailMotifs;
    String nameChefBureau;
    
    Integer retraitDeclID;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getNumeroAmr() {
        return numeroAmr;
    }

    public void setNumeroAmr(String numeroAmr) {
        this.numeroAmr = numeroAmr;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(String amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getAccountPrincipal() {
        return accountPrincipal;
    }

    public void setAccountPrincipal(String accountPrincipal) {
        this.accountPrincipal = accountPrincipal;
    }

    public String getBankAccountPrincipal() {
        return bankAccountPrincipal;
    }

    public void setBankAccountPrincipal(String bankAccountPrincipal) {
        this.bankAccountPrincipal = bankAccountPrincipal;
    }

    public String getFiftyPenaliteA() {
        return fiftyPenaliteA;
    }

    public void setFiftyPenaliteA(String fiftyPenaliteA) {
        this.fiftyPenaliteA = fiftyPenaliteA;
    }

    public String getAccountPenaliteA() {
        return accountPenaliteA;
    }

    public void setAccountPenaliteA(String accountPenaliteA) {
        this.accountPenaliteA = accountPenaliteA;
    }

    public String getBankAccountPenaliteA() {
        return bankAccountPenaliteA;
    }

    public void setBankAccountPenaliteA(String bankAccountPenaliteA) {
        this.bankAccountPenaliteA = bankAccountPenaliteA;
    }

    public String getFiftyPenaliteB() {
        return fiftyPenaliteB;
    }

    public void setFiftyPenaliteB(String fiftyPenaliteB) {
        this.fiftyPenaliteB = fiftyPenaliteB;
    }

    public String getAccountPenaliteB() {
        return accountPenaliteB;
    }

    public void setAccountPenaliteB(String accountPenaliteB) {
        this.accountPenaliteB = accountPenaliteB;
    }

    public String getBankAccountPenaliteB() {
        return bankAccountPenaliteB;
    }

    public void setBankAccountPenaliteB(String bankAccountPenaliteB) {
        this.bankAccountPenaliteB = bankAccountPenaliteB;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getDocumentReference() {
        return documentReference;
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public String getDetailMotifs() {
        return detailMotifs;
    }

    public void setDetailMotifs(String detailMotifs) {
        this.detailMotifs = detailMotifs;
    }

    public String getNameChefBureau() {
        return nameChefBureau;
    }

    public void setNameChefBureau(String nameChefBureau) {
        this.nameChefBureau = nameChefBureau;
    }

    public Integer getRetraitDeclID() {
        return retraitDeclID;
    }

    public void setRetraitDeclID(Integer retraitDeclID) {
        this.retraitDeclID = retraitDeclID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
    
}
