/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author PC
 */
public class InvitationAPayerMock implements Serializable {
    
    String id;
    String logo;
    String codeQr;
    String lieuPrint;
    String dateImpression;
    String numeroInvitationPaie;
    String contribuable;
    String referenceDocument;
    String dateTransmissionDocument;
    String amountPrincipal;
    String amountPenalite;
    String amountTotal;
    String nature;
    String nameChefBureau;
    
    String principal;
    String penaliteProvince;
    String penaliteDRHKAT;
    
    BigDecimal amountPenalite2 = new BigDecimal(0);

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroInvitationPaie() {
        return numeroInvitationPaie;
    }

    public void setNumeroInvitationPaie(String numeroInvitationPaie) {
        this.numeroInvitationPaie = numeroInvitationPaie;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public String getDateTransmissionDocument() {
        return dateTransmissionDocument;
    }

    public void setDateTransmissionDocument(String dateTransmissionDocument) {
        this.dateTransmissionDocument = dateTransmissionDocument;
    }

    public String getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(String amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(String amountPenalite) {
        this.amountPenalite = amountPenalite;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getNameChefBureau() {
        return nameChefBureau;
    }

    public void setNameChefBureau(String nameChefBureau) {
        this.nameChefBureau = nameChefBureau;
    }

    public BigDecimal getAmountPenalite2() {
        return amountPenalite2;
    }

    public void setAmountPenalite2(BigDecimal amountPenalite2) {
        this.amountPenalite2 = amountPenalite2;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPenaliteProvince() {
        return penaliteProvince;
    }

    public void setPenaliteProvince(String penaliteProvince) {
        this.penaliteProvince = penaliteProvince;
    }

    public String getPenaliteDRHKAT() {
        return penaliteDRHKAT;
    }

    public void setPenaliteDRHKAT(String penaliteDRHKAT) {
        this.penaliteDRHKAT = penaliteDRHKAT;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   
    
    
    
    
       
}
