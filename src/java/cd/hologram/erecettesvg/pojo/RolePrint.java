/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class RolePrint implements Serializable {

    String extraitRoleId;
    String articleRole;
    String adresse;
    String totalGeneral;
    String codeQr;
    String dateImpression;
    String detailRole;
    String assujettiName;
    String nomReceveur;
    String lieuSite;

    String nameMinistrie;
    String nameSector;
    String nameSite;
    String logo;

    public String getDetailRole() {
        return detailRole;
    }

    public void setDetailRole(String detailRole) {
        this.detailRole = detailRole;
    }

    public String getArticleRole() {
        return articleRole;
    }

    public void setArticleRole(String articleRole) {
        this.articleRole = articleRole;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTotalGeneral() {
        return totalGeneral;
    }

    public void setTotalGeneral(String totalGeneral) {
        this.totalGeneral = totalGeneral;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getAssujettiName() {
        return assujettiName;
    }

    public void setAssujettiName(String assujettiName) {
        this.assujettiName = assujettiName;
    }

    public String getExtraitRoleId() {
        return extraitRoleId;
    }

    public void setExtraitRoleId(String extraitRoleId) {
        this.extraitRoleId = extraitRoleId;
    }

    public String getNomReceveur() {
        return nomReceveur;
    }

    public void setNomReceveur(String nomReceveur) {
        this.nomReceveur = nomReceveur;
    }

    public String getLieuSite() {
        return lieuSite;
    }

    public void setLieuSite(String lieuSite) {
        this.lieuSite = lieuSite;
    }

    public String getNameMinistrie() {
        return nameMinistrie;
    }

    public void setNameMinistrie(String nameMinistrie) {
        this.nameMinistrie = nameMinistrie;
    }

    public String getNameSector() {
        return nameSector;
    }

    public void setNameSector(String nameSector) {
        this.nameSector = nameSector;
    }

    public String getNameSite() {
        return nameSite;
    }

    public void setNameSite(String nameSite) {
        this.nameSite = nameSite;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
    
    

}
