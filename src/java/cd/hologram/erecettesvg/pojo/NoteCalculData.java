/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.math.BigDecimal;

/**
 *
 * @author WILLY KASHALA
 */
public class NoteCalculData {
    
    String codePersonne;
    String codeAdresse;
    String exerciceFiscal;
    String codeSite;
    String codeService;
    String agentCreate;
    String dateCreate;
    String codeDepot;
    String codeBien;
    String codeArticleBudgetaire;
    BigDecimal tauxArticleBudgetaire;
    BigDecimal valeurBase;
    BigDecimal montantDu;
    Short penaliser;
    Integer periodeDeclaration;
    String tarif;
    String devise;
    int quatite;
    String typeTaux;
    String compteBancaire;
    String numeroBordereau;
    String datePaiementBordereau;
    String numeroDepotDeclaration;

    public String getCodePersonne() {
        return codePersonne;
    }

    public void setCodePersonne(String codePersonne) {
        this.codePersonne = codePersonne;
    }

    public String getCodeAdresse() {
        return codeAdresse;
    }

    public void setCodeAdresse(String codeAdresse) {
        this.codeAdresse = codeAdresse;
    }

    public String getExerciceFiscal() {
        return exerciceFiscal;
    }

    public void setExerciceFiscal(String exerciceFiscal) {
        this.exerciceFiscal = exerciceFiscal;
    }

    public String getCodeSite() {
        return codeSite;
    }

    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    public String getCodeService() {
        return codeService;
    }

    public void setCodeService(String codeService) {
        this.codeService = codeService;
    }

    public String getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(String agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getCodeDepot() {
        return codeDepot;
    }

    public void setCodeDepot(String codeDepot) {
        this.codeDepot = codeDepot;
    }

    public String getCodeBien() {
        return codeBien;
    }

    public void setCodeBien(String codeBien) {
        this.codeBien = codeBien;
    }

    public String getCodeArticleBudgetaire() {
        return codeArticleBudgetaire;
    }

    public void setCodeArticleBudgetaire(String codeArticleBudgetaire) {
        this.codeArticleBudgetaire = codeArticleBudgetaire;
    }

    public BigDecimal getTauxArticleBudgetaire() {
        return tauxArticleBudgetaire;
    }

    public void setTauxArticleBudgetaire(BigDecimal tauxArticleBudgetaire) {
        this.tauxArticleBudgetaire = tauxArticleBudgetaire;
    }

    public BigDecimal getValeurBase() {
        return valeurBase;
    }

    public void setValeurBase(BigDecimal valeurBase) {
        this.valeurBase = valeurBase;
    }

    public BigDecimal getMontantDu() {
        return montantDu;
    }

    public void setMontantDu(BigDecimal montantDu) {
        this.montantDu = montantDu;
    }

    public Short getPenaliser() {
        return penaliser;
    }

    public void setPenaliser(Short penaliser) {
        this.penaliser = penaliser;
    }

    public Integer getPeriodeDeclaration() {
        return periodeDeclaration;
    }

    public void setPeriodeDeclaration(Integer periodeDeclaration) {
        this.periodeDeclaration = periodeDeclaration;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public int getQuatite() {
        return quatite;
    }

    public void setQuatite(int quatite) {
        this.quatite = quatite;
    }

    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getNumeroBordereau() {
        return numeroBordereau;
    }

    public void setNumeroBordereau(String numeroBordereau) {
        this.numeroBordereau = numeroBordereau;
    }

    public String getDatePaiementBordereau() {
        return datePaiementBordereau;
    }

    public void setDatePaiementBordereau(String datePaiementBordereau) {
        this.datePaiementBordereau = datePaiementBordereau;
    }

    public String getNumeroDepotDeclaration() {
        return numeroDepotDeclaration;
    }

    public void setNumeroDepotDeclaration(String numeroDepotDeclaration) {
        this.numeroDepotDeclaration = numeroDepotDeclaration;
    }
    
}
