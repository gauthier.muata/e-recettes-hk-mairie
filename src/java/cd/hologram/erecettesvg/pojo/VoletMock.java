/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author WILLY
 */
public class VoletMock implements Serializable{
    
    String id;
    String logo;
    String codeQr;
    String exerciceFiscal;
    String dateImpression;
    String numeroFiscalUrbain;
    String contribuable;
    String referenceDocument;
    String numeroNote;
    String nameActivite;
    String amountTotal;
    String nameUnite;
    String avenue;
    String quartier;
    String commune;
    String nameChefBureau;
    String flag;
    String compteBancaire;
    String banque;
    String nameTaxe;
    String lieuTaxation;
    String nameComptable;
    String lettre;
    String chiffre;
    String dateCreat;
    String dateEcheance;
    String nameProprietaire;
    String codeUnite;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getExerciceFiscal() {
        return exerciceFiscal;
    }

    public void setExerciceFiscal(String exerciceFiscal) {
        this.exerciceFiscal = exerciceFiscal;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroFiscalUrbain() {
        return numeroFiscalUrbain;
    }

    public void setNumeroFiscalUrbain(String numeroFiscalUrbain) {
        this.numeroFiscalUrbain = numeroFiscalUrbain;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public String getNumeroNote() {
        return numeroNote;
    }

    public void setNumeroNote(String numeroNote) {
        this.numeroNote = numeroNote;
    }

    public String getNameActivite() {
        return nameActivite;
    }

    public void setNameActivite(String nameActivite) {
        this.nameActivite = nameActivite;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getNameUnite() {
        return nameUnite;
    }

    public void setNameUnite(String nameUnite) {
        this.nameUnite = nameUnite;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getNameChefBureau() {
        return nameChefBureau;
    }

    public void setNameChefBureau(String nameChefBureau) {
        this.nameChefBureau = nameChefBureau;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getNameTaxe() {
        return nameTaxe;
    }

    public void setNameTaxe(String nameTaxe) {
        this.nameTaxe = nameTaxe;
    }

    public String getLieuTaxation() {
        return lieuTaxation;
    }

    public void setLieuTaxation(String lieuTaxation) {
        this.lieuTaxation = lieuTaxation;
    }

    public String getNameComptable() {
        return nameComptable;
    }

    public void setNameComptable(String nameComptable) {
        this.nameComptable = nameComptable;
    }

    public String getLettre() {
        return lettre;
    }

    public void setLettre(String lettre) {
        this.lettre = lettre;
    }

    public String getChiffre() {
        return chiffre;
    }

    public void setChiffre(String chiffre) {
        this.chiffre = chiffre;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(String dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getNameProprietaire() {
        return nameProprietaire;
    }

    public void setNameProprietaire(String nameProprietaire) {
        this.nameProprietaire = nameProprietaire;
    }

    public String getCodeUnite() {
        return codeUnite;
    }

    public void setCodeUnite(String codeUnite) {
        this.codeUnite = codeUnite;
    }
   
}
