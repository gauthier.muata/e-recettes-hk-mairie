/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class RapportProductionPeagePrint implements Serializable{
    
    String datePrint;
    String axepeage;
    String poste;
    String controleurName;
    String controleurCode;
    String controleurPhone;
    String percepteurName;
    String percepteurCode;
    String percepteurPhone;
    
    String fVCdf;
    String fVLCdf;
    String fCIPCdf;
    String fBMCdf;
    String fTOTALCdf;
    
    String tuVCdf;
    String tuVLCdf;
    String tuCIPCdf;
    String tuBMCdf;
    String tuTOTALCdf;
    
    String mVCdf;
    String mVLCdf;
    String mCIPCdf;
    String mBMCdf;
    String mTOTALCdf;
    
    String fC2PUsd;
    String fCRUsd;
    String fCRTRUsd;
    String fVITUsd;
    String fVLITUsd;
    String fCIPBMUsd;
    String fTOTALUsd;
    
    String tuC2PUsd;
    String tuCRUsd;
    String tuCRTRUsd;
    String tuVITUsd;
    String tuVLITUsd;
    String tuCIPBMUsd;
    String tuTOTALUsd;
    
    String mC2PUsd;
    String mCRUsd;
    String mCRTRUsd;
    String mVTUsd;
    String mVLITUsd;
    String mCIPBMUsd;
    String mTOTALUsd;
    
    String ppfV;
    String ppfVL;
    String ppfCIP;
    String ppfBM;
    String ppfTOTAL;
    
    String ppfC2P;
    String ppfCR;
    String ppfCRTR;
    String ppfVIT;
    String ppfVLIT;
    String ppfCIPBM;
    String ppfTOTAL2;
    
    String cfV;
    String cfVL;
    String cfCIP;
    String cfBM;
    String cfTOTAL;
    
    String cfC2P;
    String cfCR;
    String cfCRTR;
    String cfVIT;
    String cfVLIT;
    String cfCIPBM;
    String cfTOTAL2;
    
    String efV;
    String efVL;
    String efCIP;
    String efBM;
    String efTOTAL;
    
    String efC2P;
    String efCR;
    String efCRTR;
    String efVIT;
    String efVLIT;
    String efCIPBM;
    String efTOTAL2;
    
    String totlGenTour;
    String observation;
    String lieuPrint;

    public String getDatePrint() {
        return datePrint;
    }

    public void setDatePrint(String datePrint) {
        this.datePrint = datePrint;
    }

    public String getAxepeage() {
        return axepeage;
    }

    public void setAxepeage(String axepeage) {
        this.axepeage = axepeage;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getControleurName() {
        return controleurName;
    }

    public void setControleurName(String controleurName) {
        this.controleurName = controleurName;
    }

    public String getControleurCode() {
        return controleurCode;
    }

    public void setControleurCode(String controleurCode) {
        this.controleurCode = controleurCode;
    }

    public String getControleurPhone() {
        return controleurPhone;
    }

    public void setControleurPhone(String controleurPhone) {
        this.controleurPhone = controleurPhone;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getPercepteurCode() {
        return percepteurCode;
    }

    public void setPercepteurCode(String percepteurCode) {
        this.percepteurCode = percepteurCode;
    }

    public String getPercepteurPhone() {
        return percepteurPhone;
    }

    public void setPercepteurPhone(String percepteurPhone) {
        this.percepteurPhone = percepteurPhone;
    }

    public String getfVCdf() {
        return fVCdf;
    }

    public void setfVCdf(String fVCdf) {
        this.fVCdf = fVCdf;
    }

    public String getfVLCdf() {
        return fVLCdf;
    }

    public void setfVLCdf(String fVLCdf) {
        this.fVLCdf = fVLCdf;
    }

    public String getfCIPCdf() {
        return fCIPCdf;
    }

    public void setfCIPCdf(String fCIPCdf) {
        this.fCIPCdf = fCIPCdf;
    }

    public String getfBMCdf() {
        return fBMCdf;
    }

    public void setfBMCdf(String fBMCdf) {
        this.fBMCdf = fBMCdf;
    }

    public String getfTOTALCdf() {
        return fTOTALCdf;
    }

    public void setfTOTALCdf(String fTOTALCdf) {
        this.fTOTALCdf = fTOTALCdf;
    }

    public String getTuVCdf() {
        return tuVCdf;
    }

    public void setTuVCdf(String tuVCdf) {
        this.tuVCdf = tuVCdf;
    }

    public String getTuVLCdf() {
        return tuVLCdf;
    }

    public void setTuVLCdf(String tuVLCdf) {
        this.tuVLCdf = tuVLCdf;
    }

    public String getTuCIPCdf() {
        return tuCIPCdf;
    }

    public void setTuCIPCdf(String tuCIPCdf) {
        this.tuCIPCdf = tuCIPCdf;
    }

    public String getTuBMCdf() {
        return tuBMCdf;
    }

    public void setTuBMCdf(String tuBMCdf) {
        this.tuBMCdf = tuBMCdf;
    }

    public String getTuTOTALCdf() {
        return tuTOTALCdf;
    }

    public void setTuTOTALCdf(String tuTOTALCdf) {
        this.tuTOTALCdf = tuTOTALCdf;
    }

    public String getmVCdf() {
        return mVCdf;
    }

    public void setmVCdf(String mVCdf) {
        this.mVCdf = mVCdf;
    }

    public String getmVLCdf() {
        return mVLCdf;
    }

    public void setmVLCdf(String mVLCdf) {
        this.mVLCdf = mVLCdf;
    }

    public String getmCIPCdf() {
        return mCIPCdf;
    }

    public void setmCIPCdf(String mCIPCdf) {
        this.mCIPCdf = mCIPCdf;
    }

    public String getmBMCdf() {
        return mBMCdf;
    }

    public void setmBMCdf(String mBMCdf) {
        this.mBMCdf = mBMCdf;
    }

    public String getmTOTALCdf() {
        return mTOTALCdf;
    }

    public void setmTOTALCdf(String mTOTALCdf) {
        this.mTOTALCdf = mTOTALCdf;
    }

    public String getfC2PUsd() {
        return fC2PUsd;
    }

    public void setfC2PUsd(String fC2PUsd) {
        this.fC2PUsd = fC2PUsd;
    }

    public String getfCRUsd() {
        return fCRUsd;
    }

    public void setfCRUsd(String fCRUsd) {
        this.fCRUsd = fCRUsd;
    }

    public String getfCRTRUsd() {
        return fCRTRUsd;
    }

    public void setfCRTRUsd(String fCRTRUsd) {
        this.fCRTRUsd = fCRTRUsd;
    }

    public String getfVITUsd() {
        return fVITUsd;
    }

    public void setfVITUsd(String fVITUsd) {
        this.fVITUsd = fVITUsd;
    }

    public String getfVLITUsd() {
        return fVLITUsd;
    }

    public void setfVLITUsd(String fVLITUsd) {
        this.fVLITUsd = fVLITUsd;
    }

    public String getfCIPBMUsd() {
        return fCIPBMUsd;
    }

    public void setfCIPBMUsd(String fCIPBMUsd) {
        this.fCIPBMUsd = fCIPBMUsd;
    }

    public String getfTOTALUsd() {
        return fTOTALUsd;
    }

    public void setfTOTALUsd(String fTOTALUsd) {
        this.fTOTALUsd = fTOTALUsd;
    }

    public String getTuC2PUsd() {
        return tuC2PUsd;
    }

    public void setTuC2PUsd(String tuC2PUsd) {
        this.tuC2PUsd = tuC2PUsd;
    }

    public String getTuCRUsd() {
        return tuCRUsd;
    }

    public void setTuCRUsd(String tuCRUsd) {
        this.tuCRUsd = tuCRUsd;
    }

    public String getTuCRTRUsd() {
        return tuCRTRUsd;
    }

    public void setTuCRTRUsd(String tuCRTRUsd) {
        this.tuCRTRUsd = tuCRTRUsd;
    }

    public String getTuVITUsd() {
        return tuVITUsd;
    }

    public void setTuVITUsd(String tuVITUsd) {
        this.tuVITUsd = tuVITUsd;
    }

    public String getTuVLITUsd() {
        return tuVLITUsd;
    }

    public void setTuVLITUsd(String tuVLITUsd) {
        this.tuVLITUsd = tuVLITUsd;
    }

    public String getTuCIPBMUsd() {
        return tuCIPBMUsd;
    }

    public void setTuCIPBMUsd(String tuCIPBMUsd) {
        this.tuCIPBMUsd = tuCIPBMUsd;
    }

    public String getTuTOTALUsd() {
        return tuTOTALUsd;
    }

    public void setTuTOTALUsd(String tuTOTALUsd) {
        this.tuTOTALUsd = tuTOTALUsd;
    }

    public String getmC2PUsd() {
        return mC2PUsd;
    }

    public void setmC2PUsd(String mC2PUsd) {
        this.mC2PUsd = mC2PUsd;
    }

    public String getmCRUsd() {
        return mCRUsd;
    }

    public void setmCRUsd(String mCRUsd) {
        this.mCRUsd = mCRUsd;
    }

    public String getmCRTRUsd() {
        return mCRTRUsd;
    }

    public void setmCRTRUsd(String mCRTRUsd) {
        this.mCRTRUsd = mCRTRUsd;
    }

    public String getmVTUsd() {
        return mVTUsd;
    }

    public void setmVTUsd(String mVTUsd) {
        this.mVTUsd = mVTUsd;
    }

    public String getmVLITUsd() {
        return mVLITUsd;
    }

    public void setmVLITUsd(String mVLITUsd) {
        this.mVLITUsd = mVLITUsd;
    }

    public String getmCIPBMUsd() {
        return mCIPBMUsd;
    }

    public void setmCIPBMUsd(String mCIPBMUsd) {
        this.mCIPBMUsd = mCIPBMUsd;
    }

    public String getmTOTALUsd() {
        return mTOTALUsd;
    }

    public void setmTOTALUsd(String mTOTALUsd) {
        this.mTOTALUsd = mTOTALUsd;
    }

    public String getPpfV() {
        return ppfV;
    }

    public void setPpfV(String ppfV) {
        this.ppfV = ppfV;
    }

    public String getPpfVL() {
        return ppfVL;
    }

    public void setPpfVL(String ppfVL) {
        this.ppfVL = ppfVL;
    }

    public String getPpfCIP() {
        return ppfCIP;
    }

    public void setPpfCIP(String ppfCIP) {
        this.ppfCIP = ppfCIP;
    }

    public String getPpfBM() {
        return ppfBM;
    }

    public void setPpfBM(String ppfBM) {
        this.ppfBM = ppfBM;
    }

    public String getPpfTOTAL() {
        return ppfTOTAL;
    }

    public void setPpfTOTAL(String ppfTOTAL) {
        this.ppfTOTAL = ppfTOTAL;
    }

    public String getPpfC2P() {
        return ppfC2P;
    }

    public void setPpfC2P(String ppfC2P) {
        this.ppfC2P = ppfC2P;
    }

    public String getPpfCR() {
        return ppfCR;
    }

    public void setPpfCR(String ppfCR) {
        this.ppfCR = ppfCR;
    }

    public String getPpfCRTR() {
        return ppfCRTR;
    }

    public void setPpfCRTR(String ppfCRTR) {
        this.ppfCRTR = ppfCRTR;
    }

    public String getPpfVIT() {
        return ppfVIT;
    }

    public void setPpfVIT(String ppfVIT) {
        this.ppfVIT = ppfVIT;
    }

    public String getPpfVLIT() {
        return ppfVLIT;
    }

    public void setPpfVLIT(String ppfVLIT) {
        this.ppfVLIT = ppfVLIT;
    }

    public String getPpfCIPBM() {
        return ppfCIPBM;
    }

    public void setPpfCIPBM(String ppfCIPBM) {
        this.ppfCIPBM = ppfCIPBM;
    }

    public String getPpfTOTAL2() {
        return ppfTOTAL2;
    }

    public void setPpfTOTAL2(String ppfTOTAL2) {
        this.ppfTOTAL2 = ppfTOTAL2;
    }

    public String getCfV() {
        return cfV;
    }

    public void setCfV(String cfV) {
        this.cfV = cfV;
    }

    public String getCfVL() {
        return cfVL;
    }

    public void setCfVL(String cfVL) {
        this.cfVL = cfVL;
    }

    public String getCfCIP() {
        return cfCIP;
    }

    public void setCfCIP(String cfCIP) {
        this.cfCIP = cfCIP;
    }

    public String getCfBM() {
        return cfBM;
    }

    public void setCfBM(String cfBM) {
        this.cfBM = cfBM;
    }

    public String getCfTOTAL() {
        return cfTOTAL;
    }

    public void setCfTOTAL(String cfTOTAL) {
        this.cfTOTAL = cfTOTAL;
    }

    public String getCfC2P() {
        return cfC2P;
    }

    public void setCfC2P(String cfC2P) {
        this.cfC2P = cfC2P;
    }

    public String getCfCR() {
        return cfCR;
    }

    public void setCfCR(String cfCR) {
        this.cfCR = cfCR;
    }

    public String getCfCRTR() {
        return cfCRTR;
    }

    public void setCfCRTR(String cfCRTR) {
        this.cfCRTR = cfCRTR;
    }

    public String getCfVIT() {
        return cfVIT;
    }

    public void setCfVIT(String cfVIT) {
        this.cfVIT = cfVIT;
    }

    public String getCfVLIT() {
        return cfVLIT;
    }

    public void setCfVLIT(String cfVLIT) {
        this.cfVLIT = cfVLIT;
    }

    public String getCfCIPBM() {
        return cfCIPBM;
    }

    public void setCfCIPBM(String cfCIPBM) {
        this.cfCIPBM = cfCIPBM;
    }

    public String getCfTOTAL2() {
        return cfTOTAL2;
    }

    public void setCfTOTAL2(String cfTOTAL2) {
        this.cfTOTAL2 = cfTOTAL2;
    }

    public String getEfV() {
        return efV;
    }

    public void setEfV(String efV) {
        this.efV = efV;
    }

    public String getEfVL() {
        return efVL;
    }

    public void setEfVL(String efVL) {
        this.efVL = efVL;
    }

    public String getEfCIP() {
        return efCIP;
    }

    public void setEfCIP(String efCIP) {
        this.efCIP = efCIP;
    }

    public String getEfBM() {
        return efBM;
    }

    public void setEfBM(String efBM) {
        this.efBM = efBM;
    }

    public String getEfTOTAL() {
        return efTOTAL;
    }

    public void setEfTOTAL(String efTOTAL) {
        this.efTOTAL = efTOTAL;
    }

    public String getEfC2P() {
        return efC2P;
    }

    public void setEfC2P(String efC2P) {
        this.efC2P = efC2P;
    }

    public String getEfCR() {
        return efCR;
    }

    public void setEfCR(String efCR) {
        this.efCR = efCR;
    }

    public String getEfCRTR() {
        return efCRTR;
    }

    public void setEfCRTR(String efCRTR) {
        this.efCRTR = efCRTR;
    }

    public String getEfVIT() {
        return efVIT;
    }

    public void setEfVIT(String efVIT) {
        this.efVIT = efVIT;
    }

    public String getEfVLIT() {
        return efVLIT;
    }

    public void setEfVLIT(String efVLIT) {
        this.efVLIT = efVLIT;
    }

    public String getEfCIPBM() {
        return efCIPBM;
    }

    public void setEfCIPBM(String efCIPBM) {
        this.efCIPBM = efCIPBM;
    }

    public String getEfTOTAL2() {
        return efTOTAL2;
    }

    public void setEfTOTAL2(String efTOTAL2) {
        this.efTOTAL2 = efTOTAL2;
    }

    public String getTotlGenTour() {
        return totlGenTour;
    }

    public void setTotlGenTour(String totlGenTour) {
        this.totlGenTour = totlGenTour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }
    
}
