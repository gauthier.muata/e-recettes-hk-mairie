/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class DetailRolePrint implements Serializable{
    
    String documentReference;
    String typeDocument;
    String assujetti;
    String amountPenalite;
    String amountPrincipal;
    String assujettiCode;
    String budgetArticleCode;
    String tarifCode;
    String budgetArticleLibelle;
    String devise;
    String service;
    String dateCreate;
    String idRole;
    String npPenaliteManuel;
    

    public String getAmountPenalite() {
        return amountPenalite;
    }

    public void setAmountPenalite(String amountPenalite) {
        this.amountPenalite = amountPenalite;
    }
    
    public String getDocumentReference() {
        return documentReference;
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getAssujetti() {
        return assujetti;
    }

    public void setAssujetti(String assujetti) {
        this.assujetti = assujetti;
    }

    public String getAmountPrincipal() {
        return amountPrincipal;
    }

    public void setAmountPrincipal(String amountPrincipal) {
        this.amountPrincipal = amountPrincipal;
    }

    public String getAssujettiCode() {
        return assujettiCode;
    }

    public void setAssujettiCode(String assujettiCode) {
        this.assujettiCode = assujettiCode;
    }

    public String getBudgetArticleCode() {
        return budgetArticleCode;
    }

    public void setBudgetArticleCode(String budgetArticleCode) {
        this.budgetArticleCode = budgetArticleCode;
    }

    public String getTarifCode() {
        return tarifCode;
    }

    public void setTarifCode(String tarifCode) {
        this.tarifCode = tarifCode;
    }

    public String getBudgetArticleLibelle() {
        return budgetArticleLibelle;
    }

    public void setBudgetArticleLibelle(String budgetArticleLibelle) {
        this.budgetArticleLibelle = budgetArticleLibelle;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getNpPenaliteManuel() {
        return npPenaliteManuel;
    }

    public void setNpPenaliteManuel(String npPenaliteManuel) {
        this.npPenaliteManuel = npPenaliteManuel;
    }
    
    
    

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getIdRole() {
        return idRole;
    }

    public void setIdRole(String idRole) {
        this.idRole = idRole;
    }

    @Override
    public String toString() {
        return "DetailRolePrint{" + "documentReference=" + documentReference + ", typeDocument=" + typeDocument + ", assujetti=" + assujetti + ", amountPenalite=" + amountPenalite + ", amountPrincipal=" + amountPrincipal + ", assujettiCode=" + assujettiCode + ", budgetArticleCode=" + budgetArticleCode + ", tarifCode=" + tarifCode + ", budgetArticleLibelle=" + budgetArticleLibelle + ", devise=" + devise + ", service=" + service + ", dateCreate=" + dateCreate + ", idRole=" + idRole + '}';
    }

    
    
    
}
