/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.ContentieuxBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getAccountBankByAccount;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getArchiveByRefDocument;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getArticleBudgetaireByCode;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getComplementByParams;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getDocumentOfficielByCode;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getListDetailsNcsNoteCalcul;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getTarifByCode;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.PoursuiteBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.ContentieuxConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.etats.DocumentReturnString;
import cd.hologram.erecettesvg.models.Acquisition;
import cd.hologram.erecettesvg.models.AcquitLiberatoire;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Atd;
import cd.hologram.erecettesvg.models.Avis;
import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.BienActivites;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.Complement;
import cd.hologram.erecettesvg.models.ComplementInfoTaxe;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DernierAvertissement;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.DocumentOfficiel;
import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.NotePerceptionMairie;
import cd.hologram.erecettesvg.models.ParamAmr;
import cd.hologram.erecettesvg.models.ParamContrainte;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Role;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Taux;
import cd.hologram.erecettesvg.models.TicketPeage;
import cd.hologram.erecettesvg.pojo.AccountBankAmr;
import cd.hologram.erecettesvg.pojo.ActeGenerateurPrint;
import cd.hologram.erecettesvg.pojo.AmrMock;
import cd.hologram.erecettesvg.pojo.AmrPrint;
import cd.hologram.erecettesvg.pojo.AtdPrint;
import cd.hologram.erecettesvg.pojo.BapPrint;
import cd.hologram.erecettesvg.pojo.BonAPayerPrint;
import cd.hologram.erecettesvg.pojo.CommandementMock;
import cd.hologram.erecettesvg.pojo.CommandementPrint;
import cd.hologram.erecettesvg.pojo.ContrainteMock;
import cd.hologram.erecettesvg.pojo.ContraintePrint;
import cd.hologram.erecettesvg.pojo.CpiPrint;
import cd.hologram.erecettesvg.pojo.DernierAvertissementPrint;
import cd.hologram.erecettesvg.pojo.DetailRolePrint;
import cd.hologram.erecettesvg.pojo.InvitationAPayerMock;
import cd.hologram.erecettesvg.pojo.MedPrint;
import cd.hologram.erecettesvg.pojo.MepPrint;
import cd.hologram.erecettesvg.pojo.NotePerceptionPrint;
import cd.hologram.erecettesvg.pojo.NotePerceptionVoirieConcentres;
import cd.hologram.erecettesvg.pojo.NoteTaxationMock;
import cd.hologram.erecettesvg.pojo.ProjectRolePrint;
import cd.hologram.erecettesvg.pojo.RapportProductionCashPeagePrint;
import cd.hologram.erecettesvg.pojo.RapportProductionCreditPeagePrint;
import cd.hologram.erecettesvg.pojo.RapportProductionExemptePeagePrint;
import cd.hologram.erecettesvg.pojo.RapportProductionPrepaiementPeagePrint;
import cd.hologram.erecettesvg.pojo.RecepissePrint;
import cd.hologram.erecettesvg.pojo.Response;
import cd.hologram.erecettesvg.pojo.RolePrint;
import cd.hologram.erecettesvg.pojo.TabDetailRole;
import cd.hologram.erecettesvg.pojo.TrackingDocModel;
import cd.hologram.erecettesvg.pojo.VoletMock;
import cd.hologram.erecettesvg.sql.SQLQueryDeclaration;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryRecouvrement;
import static cd.hologram.erecettesvg.util.Tools.formatNombreToString;
import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author moussa.toure
 */
public class PrintDocument {

    Properties propertiesMessage, propertiesConfig;
    boolean saveArchive;
    boolean existArchive;
    Date echeanceMep;
    int xfrom;

    public PrintDocument() {

        propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE);
        propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);
    }

    String generateQRCode(String contenu) {
        String qrBase64 = GeneralConst.EMPTY_STRING;
        try {
            ByteArrayOutputStream out = QRCode.from(contenu).to(ImageType.PNG).stream();
            ByteArrayInputStream qrIn = new ByteArrayInputStream(out.toByteArray());
            file = new DefaultStreamedContent(qrIn, GeneralConst.Format.FORMAT_IMAGE);
            qrBase64 = Tools.getByteaToBase64String(out.toByteArray());
        } catch (Exception e) {
            throw e;
        }
        return qrBase64;
    }

    public String createCPI(AcquitLiberatoire acquitLiberatoire, String nameUser, String rubPapier, String rubTimbre, Double montant, String contenuPaiement) throws InvocationTargetException {

        List<ActeGenerateurPrint> listActeGenerateurPrints = new ArrayList<>();

        CpiPrint cpiPrint = new CpiPrint();
        cpiPrint.setNumeroCpi(acquitLiberatoire.getCode());
        cpiPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE));
        cpiPrint.setRubPapier(rubPapier);
        cpiPrint.setRubTimbre(rubTimbre);

        String devise = GeneralConst.EMPTY_STRING;
        if (acquitLiberatoire.getNotePerception().getCompteBancaire() != null
                && !acquitLiberatoire.getNotePerception().getCompteBancaire().isEmpty()) {
            CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    acquitLiberatoire.getNotePerception().getCompteBancaire().trim());
            if (compteBancaire != null) {
                devise = compteBancaire.getDevise().getCode().trim();
            }
        }

        String numberConvert = String.valueOf(montant);
        numberConvert = numberConvert.replace(".", ",");

        BigDecimal partInteger = new BigDecimal("0");
        BigDecimal partVirgule = new BigDecimal("0");

        boolean virguleExist;

        String amount = String.format("%.2f", BigDecimal.valueOf(montant));
        amount = amount.replace(".", GeneralConst.VIRGULE);

        if (amount != null && !amount.isEmpty()) {

            if (amount.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amount.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));

            }

        }

        String letterOne = GeneralConst.EMPTY_STRING;
        String letterTwo = GeneralConst.EMPTY_STRING;
        String chiffreEnLettre = GeneralConst.EMPTY_STRING;
        String deviseLetter = GeneralConst.EMPTY_STRING;

        switch (acquitLiberatoire.getNotePerception().getDevise().trim().toUpperCase()) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                break;
        }

        String credit = Tools.formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                BigDecimal.valueOf(montant))
                + GeneralConst.SPACE + devise;

        cpiPrint.setMontantEnChiffre(credit);

        if (partInteger.floatValue() > 0) {
            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {
                letterTwo = FrenchNumberToWords.convert(partVirgule);

                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                virguleExist = false;
            }

            cpiPrint.setMontantEnLettre(chiffreEnLettre);

        } else {
            cpiPrint.setMontantEnLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        //long creditLong = montant.longValue();
        //cpiPrint.setMontantEnLettre(Nombre.getLettre(creditLong) + GeneralConst.SPACE + devise);
        Journal journal = AcquitLiberatoireBusiness.getJournalByDocumentApure(acquitLiberatoire.getNotePerception().getNumero().trim());

        if (journal != null) {
            cpiPrint.setDatePaiement(ConvertDate.formatDateToStringOfFormat(journal.getDateCreat(), GeneralConst.Format.FORMAT_DATE));
            cpiPrint.setNumeroPiece(journal.getNumeroAttestationPaiement() != null ? journal.getNumeroAttestationPaiement().trim() : GeneralConst.EMPTY_STRING);
        } else {
            cpiPrint.setDatePaiement(GeneralConst.EMPTY_STRING);
        }

        Personne personne = IdentificationBusiness.getPersonneByCode(
                acquitLiberatoire.getNotePerception().getNoteCalcul().getPersonne().trim());

        if (personne != null) {
            cpiPrint.setNomsContribuable(personne.toString().trim().toUpperCase());
        } else {
            cpiPrint.setNomsContribuable(GeneralConst.EMPTY_STRING);
        }

        cpiPrint.setReference(acquitLiberatoire.getNotePerception().getNumero().trim());

        cpiPrint.setNomAgent(nameUser);
        cpiPrint.setCodeComptablePublic(propertiesConfig.getProperty("CODE_COMPTABLE_PUBLIC"));

        Adresse adresse = IdentificationBusiness.getDefaultAdressByPerson(acquitLiberatoire.getNotePerception().getNoteCalcul().getPersonne().trim());

        if (adresse != null) {
            cpiPrint.setAdresseContribuable(adresse.toString());
        } else {
            cpiPrint.setAdresseContribuable(GeneralConst.EMPTY_STRING);
        }

        cpiPrint.setPrincipal(propertiesConfig.getProperty("TXT_PRINCIPAL"));
        cpiPrint.setUrl(propertiesConfig.getProperty("URL_TRACKING_DOC") + acquitLiberatoire.getCode().trim());

        String villePrint = GeneralConst.EMPTY_STRING;

        Site site = TaxationBusiness.getSiteByCode(acquitLiberatoire.getNotePerception().getSite());

        if (site != null) {

            villePrint = site.getAdresse().getVille().getIntitule();
        } else {
            villePrint = propertiesConfig.getProperty("VILLE_LUBUMBASHI");
        }

        cpiPrint.setLieuImpression(villePrint);
        //cpiPrint.setLieuImpression(propertiesConfig.getProperty("VILLE_KINSHASA"));
        //cpiPrint.setLogo(propertiesConfig.getProperty("LOGO_DGRK_2"));
        cpiPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String codeQR = generateQRCode(cpiPrint.getUrl());

        if (codeQR != null && !codeQR.isEmpty()) {
            cpiPrint.setCodeQr(codeQR);
        } else {
            cpiPrint.setCodeQr(GeneralConst.EMPTY_STRING);
        }

        String tarifStr = GeneralConst.EMPTY_STRING;

        if (acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList().size() == 1) {

            Tarif tarif = TaxationBusiness.getTarifByCode(acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList().get(0).getTarif().trim());

            if (tarif != null) {
                tarifStr = tarif.getIntitule().trim();
            } else {
                tarifStr = GeneralConst.EMPTY_STRING;
            }

            cpiPrint.setArticleTaxes("1)" + GeneralConst.SPACE + acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getIntitule().trim()
                    + GeneralConst.TWO_POINTS + tarifStr.trim());

        } else if (acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList().size() > 1) {

            cpiPrint.setArticleTaxes(GeneralConst.EMPTY_STRING);

            if (!listActeGenerateurPrints.isEmpty()) {
                listActeGenerateurPrints.clear();
            }
            int nbre = 0;
            for (DetailsNc dnc : acquitLiberatoire.getNotePerception().getNoteCalcul().getDetailsNcList()) {

                if (dnc.getArticleBudgetaire() != null) {

                    ActeGenerateurPrint acteGenerateurPrint = new ActeGenerateurPrint();
                    Tarif tarif = TaxationBusiness.getTarifByCode(dnc.getTarif().trim());

                    nbre += 1;

                    if (tarif != null) {
                        tarifStr = tarif.getIntitule().trim();
                    } else {
                        tarifStr = GeneralConst.EMPTY_STRING;
                    }

                    acteGenerateurPrint.setNumero(nbre);
                    acteGenerateurPrint.setArticlesBudgetaires(dnc.getArticleBudgetaire().getIntitule().trim()
                            + " : " + tarifStr.trim());

                    listActeGenerateurPrints.add(acteGenerateurPrint);
                }
            }

            String articleTaxe = Tools.getDataTableToHtml(listActeGenerateurPrints);

            if (articleTaxe != null && !articleTaxe.isEmpty()) {
                cpiPrint.setArticleTaxes(articleTaxe);
            } else {
                cpiPrint.setArticleTaxes(GeneralConst.EMPTY_STRING);
            }
        }

        cpiPrint.setDetailPaiementCpi(contenuPaiement);

        String dataXhtm = createDocument(cpiPrint, DocumentConst.DocumentCode.CPI, false);

        return dataXhtm;

    }

    public String createAmr2(RetraitDeclaration rd, String userId) throws InvocationTargetException {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        AmrPrint amrPrint = new AmrPrint();
        Amr amr = RecouvrementBusiness.getAMRByRetrait(rd.getFkRetraitPere());

        amrPrint.setNomAssujetti(rd.getRequerant() == null ? GeneralConst.EMPTY_STRING : rd.getRequerant().toUpperCase());
        amrPrint.setNumImpot(rd.getFkAssujetti());
        //amrPrint.setMotif(motif);
        //amrPrint.setDetailMotif(detailMotif);
        //amrPrint.setDocumentReference(documentReference);

        amrPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(amr.getNumero());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            amrPrint.setCodeQR(codeQR);
        } else {
            amrPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        amrPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        amrPrint.setTypeAmr("AMR");
        amrPrint.setDateEcheance(ConvertDate.formatDateToStringOfFormat(amr.getDateEcheance(), "dd/MM/YYYY"));

        String province = GeneralConst.EMPTY_STRING;

        Agent agent = GeneralBusiness.getAgentByCode(userId);
        if (agent != null) {
            amrPrint.setUserPrintName(agent.toString().toUpperCase());
            amrPrint.setUserPrintFunction(agent.getFonction().getIntitule());
            province = agent.getSite().getAdresse().getProvince().getIntitule().toUpperCase();
        } else {
            amrPrint.setUserPrintName(GeneralConst.EMPTY_STRING);
            amrPrint.setUserPrintFunction(GeneralConst.EMPTY_STRING);
        }

        amrPrint.setProvinceAmr("VILLE PROVINCE DE ".concat(province));

        String letterOne;
        String letterTwo;
        String chiffreEnLettre = "";

        BigDecimal partInteger = new BigDecimal(BigInteger.ZERO);
        BigDecimal partVirgule = new BigDecimal(BigInteger.ZERO);

        String amountDisplay = String.format(GeneralConst.FORMAT_CONVERT, amr.getNetAPayer()).concat(GeneralConst.SPACE).concat(rd.getDevise());

        amrPrint.setAmountToChiffre(amountDisplay);
        amrPrint.setAmountTotal(amountDisplay);

        String amountFormat = String.format(GeneralConst.FORMAT_CONVERT, amr.getNetAPayer());
        amountFormat = amountFormat.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

        if (amountFormat != null && !amountFormat.isEmpty()) {
            if (amountFormat.contains(GeneralConst.VIRGULE)) {
                String[] amountSplit = amountFormat.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
            }
        }

        boolean virguleExist;
        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);

                //chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE") + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLibelle;
                virguleExist = true;
            } else {
                //chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLibelle;
                virguleExist = false;
            }

            amrPrint.setAmountToLetter(chiffreEnLettre);

        } else {
            amrPrint.setAmountToLetter(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        String exercice = GeneralConst.EMPTY_STRING;
        String taux = GeneralConst.EMPTY_STRING;
        String droit = GeneralConst.EMPTY_STRING;
        String major = GeneralConst.EMPTY_STRING;
        String penal = GeneralConst.EMPTY_STRING;
        String typeTaux = GeneralConst.EMPTY_STRING;
        String amende = GeneralConst.EMPTY_STRING;
        String degreve = GeneralConst.EMPTY_STRING;

        amrPrint.setAnnee(exercice);
        //amrPrint.setArticleName(articleName);
        //amrPrint.setBase(base);
        amrPrint.setDroit(droit);
        amrPrint.setTaux(taux.concat(GeneralConst.SPACE).concat(typeTaux));

        amrPrint.setMajor(major);
        amrPrint.setPenal(penal);
        amrPrint.setAmende(amende);
        amrPrint.setDegrev(degreve);
        //amrPrint.setIdTax(ab == null ? GeneralConst.EMPTY_STRING : ab.getCodeOfficiel());

        dataXhtm = createDocument(amrPrint, DocumentConst.DocumentCode.AMR_, false);

        return dataXhtm;
    }

    public String createNP(NotePerception notePerception) throws NumberFormatException, InvocationTargetException {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Complement complement;
        Adresse adresse;
        Service service;
        Agent agent;
        ArticleBudgetaire articleBudgetaire;
        DocumentOfficiel documentOfficiel;
        NotePerceptionPrint notePerceptionPrint = new NotePerceptionPrint();
        Personne personne;
        NotePerceptionVoirieConcentres npvc;
        String provinceName = "";

        try {
            if (notePerception != null) {

                Archive archive = getArchiveByRefDocument(notePerception.getNumero().trim());

                String dataXhtm = GeneralConst.EMPTY_STRING;

                if (archive != null) {

                    notePerceptionPrint.setNumeroNotePerception(notePerception.getNumero().trim());
                    dataReturn = createDocument(notePerceptionPrint, DocumentConst.DocumentCode.NP, true);

                } else {

                    ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNP(notePerception.getNumero());

                    if (complementInfoTaxe != null) {

                        npvc = new NotePerceptionVoirieConcentres();

                        String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC")
                                + notePerception.getNumero().trim());
                        if (qrCode != null && !qrCode.isEmpty()) {
                            npvc.setCodeQR(qrCode);
                        } else {
                            npvc.setCodeQR(GeneralConst.EMPTY_STRING);
                        }

                        npvc.setFlag(propertiesConfig.getProperty("LOGO_DRHKAT"));

                        npvc.setNameTaxe(notePerception.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getIntitule().toUpperCase());

                        npvc.setNumeroPerception(notePerception.getNumero());
                        npvc.setTransporteur(complementInfoTaxe.getTransporteur().toUpperCase());
                        npvc.setPlaque(complementInfoTaxe.getNumeroPlaque().toUpperCase());
                        npvc.setProduit(complementInfoTaxe.getNatureProduit().toUpperCase());
                        npvc.setModePaiement(complementInfoTaxe.getModePaiement());
                        npvc.setPoids(notePerception.getNoteCalcul().getDetailsNcList().get(0).getValeurBase() + "");

                        CompteBancaire cb = GeneralBusiness.getCompteBancaireByCode(notePerception.getCompteBancaire());

                        if (cb != null) {
                            npvc.setCompteBancaire(cb.getCode() + " " + cb.getIntitule().toUpperCase());
                            npvc.setBanque(cb.getBanque().getIntitule().toUpperCase());

                        } else {
                            npvc.setCompteBancaire(GeneralConst.EMPTY_STRING);
                            npvc.setBanque(GeneralConst.EMPTY_STRING);
                        }

                        agent = GeneralBusiness.getAgentByCode(notePerception.getAgentCreat());

                        if (agent != null) {

                            npvc.setLieuTaxation(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
                            npvc.setNomFonctionTaxateur(agent.toString().toUpperCase() + " (Fonction : " + agent.getFonction().getIntitule().toUpperCase() + ")");
                        } else {
                            npvc.setLieuTaxation(GeneralConst.EMPTY_STRING);
                            npvc.setNomFonctionTaxateur(GeneralConst.EMPTY_STRING);
                        }

                        String deviseLetter = GeneralConst.EMPTY_STRING;

                        switch (notePerception.getDevise().trim().toUpperCase()) {
                            case GeneralConst.Devise.DEVISE_CDF:
                                deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                                break;
                            case GeneralConst.Devise.DEVISE_USD:
                                deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                                break;
                        }

                        String letterOne;
                        String letterTwo;
                        String chiffreEnLettre;

                        BigDecimal partInteger = new BigDecimal("0");
                        BigDecimal partVirgule = new BigDecimal("0");

                        boolean virguleExist;

                        String amount = String.format("%.2f", notePerception.getNetAPayer());

                        amount = amount.replace(".", ",");

                        if (amount != null && !amount.isEmpty()) {

                            if (amount.contains(GeneralConst.VIRGULE)) {

                                String[] amountSplit = amount.split(GeneralConst.VIRGULE);

                                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
                            }
                        }

                        if (partInteger.floatValue() > 0) {

                            letterOne = FrenchNumberToWords.convert(partInteger);

                            if (partVirgule.floatValue() > 0) {

                                letterTwo = FrenchNumberToWords.convert(partVirgule);
                                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                                virguleExist = true;
                            } else {
                                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                                virguleExist = false;
                            }

                            npvc.setLettre(chiffreEnLettre);

                        } else {
                            npvc.setLettre(GeneralConst.EMPTY_STRING);
                            virguleExist = false;
                        }

                        if (virguleExist) {
                            npvc.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                    notePerception.getNetAPayer())
                                    + GeneralConst.SPACE + notePerception.getDevise().toUpperCase().trim());
                        } else {
                            npvc.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                    notePerception.getNetAPayer())
                                    + GeneralConst.Format.ZERO_FORMAT
                                    + GeneralConst.SPACE + notePerception.getDevise().toUpperCase().trim());
                        }

                        npvc.setDateTaxation(ConvertDate.formatDateHeureToStringV2(new Date()));

                        personne = IdentificationBusiness.getPersonneByCode(notePerception.getNoteCalcul().getPersonne());

                        if (personne != null) {

                            npvc.setProprietaire(personne.toString().toUpperCase());

                            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                            if (adresse != null) {
                                npvc.setAdresse(adresse.toString().toUpperCase());
                            } else {
                                npvc.setAdresse(GeneralConst.EMPTY_STRING);
                            }

                            if (personne.getLoginWeb() != null) {
                                npvc.setTelephone(personne.getLoginWeb().getTelephone());
                            } else {
                                npvc.setTelephone(GeneralConst.EMPTY_STRING);
                            }

                        } else {
                            npvc.setProprietaire(GeneralConst.EMPTY_STRING);
                            npvc.setAdresse(GeneralConst.EMPTY_STRING);
                            npvc.setTelephone(GeneralConst.EMPTY_STRING);
                        }

                        dataXhtm = createDocument(npvc, DocumentConst.DocumentCode.NP_VOIRIE, false);

                    } else {

                        notePerceptionPrint.setNumeroNotePerception(notePerception.getNumero().trim());
                        notePerceptionPrint.setNumeroNoteCalcul(notePerception.getNoteCalcul().getNumero().trim());

                        CompteBancaire compteBancaire = getAccountBankByAccount(
                                notePerception.getCompteBancaire().trim());

                        if (compteBancaire != null) {
                            notePerceptionPrint.setCompteBancaire(compteBancaire.getCode().trim());
                            notePerceptionPrint.setBanque(compteBancaire.getBanque().getIntitule().toUpperCase().trim());
                        } else {
                            notePerceptionPrint.setCompteBancaire(GeneralConst.EMPTY_STRING);
                            notePerceptionPrint.setBanque(GeneralConst.EMPTY_STRING);
                        }

                        personne = IdentificationBusiness.getPersonneByCode(
                                notePerception.getNoteCalcul().getPersonne().trim());

                        if (personne != null) {

                            notePerceptionPrint.setNomContribuable(personne.toString().toUpperCase().trim());

                            if (personne.getNif() != null && !personne.getNif().isEmpty()) {
                                notePerceptionPrint.setNifContribuable(personne.getNif().trim());
                            } else {
                                notePerceptionPrint.setNifContribuable(GeneralConst.EMPTY_STRING);
                            }

                            //Get complement Telephone
                            complement = getComplementByParams(personne.getFormeJuridique().getCode().trim(),
                                    propertiesConfig.getProperty("CODE_TELEPHONE"), personne.getCode().trim());

                            if (complement != null) {
                                notePerceptionPrint.setPhoneContribuable(complement.getValeur().trim());
                            } else {
                                notePerceptionPrint.setPhoneContribuable(GeneralConst.EMPTY_STRING);
                            }

                            //Get complement Adresse postale
                            complement = getComplementByParams(personne.getFormeJuridique().getCode().trim(),
                                    propertiesConfig.getProperty("CODE_ADRESSE_POSTALE"), personne.getCode().trim());

                            if (complement != null) {
                                notePerceptionPrint.setBoitePostale(complement.getValeur().trim());
                            } else {
                                notePerceptionPrint.setBoitePostale(GeneralConst.EMPTY_STRING);
                            }

                            //Get complement Identification nationale
                            complement = getComplementByParams(personne.getFormeJuridique().getCode().trim(),
                                    propertiesConfig.getProperty("CODE_ID_NAT"), personne.getCode().trim());

                            if (complement != null) {
                                notePerceptionPrint.setIdNationaleContribuable(complement.getValeur().trim());
                            } else {
                                notePerceptionPrint.setIdNationaleContribuable(GeneralConst.EMPTY_STRING);
                            }

                            adresse = TaxationBusiness.getAdressByCode(
                                    notePerception.getNoteCalcul().getFkAdressePersonne().trim());

                            if (adresse != null) {

                                provinceName = adresse.getProvince().getIntitule();

                                notePerceptionPrint.setAvenueRueContribuable(adresse.getAvenue().getIntitule()
                                        + GeneralConst.SPACE + " nÂ° " + adresse.getNumero().trim()
                                        + GeneralConst.SPACE + " Q/" + adresse.getQuartier().getIntitule().trim());

                                notePerceptionPrint.setCommuneContribuable(adresse.getCommune().getIntitule().trim());
                                notePerceptionPrint.setAdresseContribuable(adresse.toString().trim());
                            } else {
                                notePerceptionPrint.setAvenueRueContribuable(GeneralConst.EMPTY_STRING);
                                notePerceptionPrint.setCommuneContribuable(GeneralConst.EMPTY_STRING);
                            }
                        }

                        service = TaxationBusiness.getServiceByCode(
                                notePerception.getNoteCalcul().getService().trim());

                        if (service != null) {
                            notePerceptionPrint.setServiceAssiete(service.getIntitule().toUpperCase().trim());
                        } else {
                            notePerceptionPrint.setServiceAssiete(GeneralConst.EMPTY_STRING);
                        }

                        String deviseLetter = GeneralConst.EMPTY_STRING;

                        if (notePerception.getDevise() == null) {
                            notePerception.setDevise(GeneralConst.Devise.DEVISE_CDF);
                        }

                        switch (notePerception.getDevise().trim().toUpperCase()) {
                            case GeneralConst.Devise.DEVISE_CDF:
                                deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                                break;
                            case GeneralConst.Devise.DEVISE_USD:
                                deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                                break;
                        }

                        String letterOne;
                        String letterTwo;
                        String chiffreEnLettre;

                        //int partInteger = 0;
                        //int partVirgule = 0;
                        BigDecimal partInteger = new BigDecimal("0");
                        BigDecimal partVirgule = new BigDecimal("0");

                        boolean virguleExist;

                        String amount = String.format("%.2f", notePerception.getNetAPayer());

                        amount = amount.replace(".", ",");

                        if (amount != null && !amount.isEmpty()) {

                            if (amount.contains(GeneralConst.VIRGULE)) {

                                String[] amountSplit = amount.split(GeneralConst.VIRGULE);

                                //partInteger = Integer.valueOf(String.valueOf(amountSplit[0]));
                                //partVirgule = Integer.valueOf(String.valueOf(amountSplit[1]));
                                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
                            }
                        }

                        //if (partInteger > 0) {
                        if (partInteger.floatValue() > 0) {
                            //letterOne = getLetterOfNumber(partInteger);
                            letterOne = FrenchNumberToWords.convert(partInteger);

                            //if (partVirgule > 0) {
                            if (partVirgule.floatValue() > 0) {
                                //letterTwo = getLetterOfNumber(partVirgule);
                                letterTwo = FrenchNumberToWords.convert(partVirgule);
                                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                                virguleExist = true;
                            } else {
                                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                                virguleExist = false;
                            }

                            notePerceptionPrint.setMontantEnLettre(chiffreEnLettre);

                        } else {
                            notePerceptionPrint.setMontantEnLettre(GeneralConst.EMPTY_STRING);
                            virguleExist = false;
                        }

                        if (virguleExist) {
                            notePerceptionPrint.setMontantEnChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                    notePerception.getNetAPayer())
                                    + GeneralConst.SPACE + notePerception.getDevise().toUpperCase().trim());
                        } else {
                            notePerceptionPrint.setMontantEnChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                                    notePerception.getNetAPayer())
                                    + GeneralConst.Format.ZERO_FORMAT
                                    + GeneralConst.SPACE + notePerception.getDevise().toUpperCase().trim());
                        }

                        agent = PaiementBusiness.getAgentByCode(
                                Integer.valueOf(notePerception.getNoteCalcul().getAgentCreat()));

                        if (agent != null) {
                            notePerceptionPrint.setNomTaxateur(agent.toString().trim().toUpperCase());
                        } else {
                            notePerceptionPrint.setNomTaxateur(GeneralConst.EMPTY_STRING);
                        }

                        agent = PaiementBusiness.getAgentByCode(
                                Integer.valueOf(notePerception.getNoteCalcul().getAgentValidation()));

                        if (agent != null) {
                            notePerceptionPrint.setNomOrdonnanceur(agent.toString().trim().toUpperCase());
                        } else {
                            notePerceptionPrint.setNomOrdonnanceur(GeneralConst.EMPTY_STRING);
                        }

                        notePerceptionPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

                        notePerceptionPrint.setVillePrint(provinceName.toUpperCase());

                        notePerceptionPrint.setDateTaxation(
                                ConvertDate.formatDateToStringOfFormat(notePerception.getNoteCalcul().getDateCreat(),
                                        GeneralConst.Format.FORMAT_DATE));
                        notePerceptionPrint.setDateOrdonnancement(
                                ConvertDate.formatDateToStringOfFormat(notePerception.getNoteCalcul().getDateValidation(),
                                        GeneralConst.Format.FORMAT_DATE));

                        Avis avis = TaxationBusiness.getAvisById(
                                notePerception.getNoteCalcul().getAvis().trim());

                        if (avis != null) {
                            notePerceptionPrint.setAvisOrdonnateur(avis.getIntitule().toUpperCase().trim());
                        } else {
                            notePerceptionPrint.setAvisOrdonnateur(GeneralConst.EMPTY_STRING);
                        }

                        if (notePerception.getDateEcheancePaiement() != null && !notePerception.getDateEcheancePaiement().isEmpty()) {

                            notePerceptionPrint.setDateEcheance(notePerception.getDateEcheancePaiement());

                        } else {
                            notePerceptionPrint.setDateEcheance(GeneralConst.MULTIPLE_POINT);
                        }

                        if (notePerception.getDateReception() != null && !notePerception.getDateReception().isEmpty()) {

                            notePerceptionPrint.setDateReception(notePerception.getDateReception());

                        } else {

                            notePerceptionPrint.setDateReception(GeneralConst.MULTIPLE_POINT);
                        }

                        String arreteOfficiel;

                        articleBudgetaire = getArticleBudgetaireByCode(
                                notePerception.getNoteCalcul().getNumero().trim());

                        if (articleBudgetaire != null) {

                            notePerceptionPrint.setActeGenerateur(
                                    articleBudgetaire.getArticleGenerique().getIntitule().trim());

                            documentOfficiel = getDocumentOfficielByCode(articleBudgetaire.getArrete().trim());
                            if (documentOfficiel != null) {

                                arreteOfficiel = documentOfficiel.getNumero().trim()
                                        + GeneralConst.SPACE + documentOfficiel.getDateArrete();

                                notePerceptionPrint.setReferenceOfficiel(propertiesConfig.getProperty("CODE_ARRETE_NP_HK"));
                            } else {
                                arreteOfficiel = GeneralConst.EMPTY_STRING;
                                notePerceptionPrint.setReferenceOfficiel(propertiesConfig.getProperty("CODE_ARRETE_NP_HK"));
                            }

                        }

                        notePerceptionPrint.setQte(notePerception.getNoteCalcul().getDetailsNcList().get(0).getQte().toString());

                        notePerceptionPrint.setCodeCentreOrdonnancement(GeneralConst.EMPTY_STRING);
                        notePerceptionPrint.setNumeroImpotContribuable(GeneralConst.EMPTY_STRING);

                        notePerceptionPrint.setUrlTrackingDoc(propertiesConfig.getProperty("URL_TRACKING_DOC")
                                + notePerception.getNumero().trim());

                        if (notePerception.getReceptionniste() != null) {
                            notePerceptionPrint.setNomReceptionniste(notePerception.getReceptionniste().toString());
                        } else {
                            notePerceptionPrint.setNomReceptionniste(GeneralConst.MULTIPLE_POINT);
                        }

                        String qrCode = generateQRCode(notePerceptionPrint.getUrlTrackingDoc().trim());
                        if (qrCode != null && !qrCode.isEmpty()) {
                            notePerceptionPrint.setCodeQR(qrCode);
                        } else {
                            notePerceptionPrint.setCodeQR(GeneralConst.EMPTY_STRING);
                        }

                        List<DetailsNc> listDetailsNc = new ArrayList<>();
                        List<ActeGenerateurPrint> listActeGenerateurPrint = new ArrayList<>();
                        ActeGenerateurPrint arGenerateurPrint;
                        Tarif tarif;
                        String tarifStr = GeneralConst.EMPTY_STRING;

                        listDetailsNc = getListDetailsNcsNoteCalcul(notePerception.getNoteCalcul().getNumero().trim());

                        if (listDetailsNc != null && !listDetailsNc.isEmpty()) {
                            listActeGenerateurPrint.clear();
                            int nbre = 0;
                            for (DetailsNc dn : listDetailsNc) {

                                arGenerateurPrint = new ActeGenerateurPrint();
                                tarif = new Tarif();
                                nbre += 1;

                                tarif = getTarifByCode(dn.getTarif());

                                if (tarif != null) {
                                    //tarifStr = tarif.getIntitule().trim();
                                    tarifStr = tarif.getIntitule().substring(0, 1).toUpperCase() + tarif.getIntitule().substring(1).toLowerCase();
                                } else {
                                    tarifStr = GeneralConst.EMPTY_STRING;
                                }

                                arGenerateurPrint.setNumero(nbre);

                                String article = GeneralConst.EMPTY_STRING;

                                article = dn.getArticleBudgetaire().getIntitule().substring(0, 1).toUpperCase() + dn.getArticleBudgetaire().getIntitule().substring(1).toLowerCase();

                                /*arGenerateurPrint.setArticlesBudgetaires(dn.getArticleBudgetaire().getIntitule().toUpperCase().trim()
                                 + " : " + tarifStr.toUpperCase().trim());*/
                                arGenerateurPrint.setArticlesBudgetaires(article + " : " + tarifStr);

                                listActeGenerateurPrint.add(arGenerateurPrint);

                            }
                        }

                        String articleTaxe = GeneralConst.EMPTY_STRING;

                        if (listActeGenerateurPrint.size() > 0) {

                            articleTaxe = Tools.getDataTableToHtml(listActeGenerateurPrint);

                            if (articleTaxe != null && !articleTaxe.isEmpty()) {
                                notePerceptionPrint.setArticlesBudgetaires(articleTaxe);
                            } else {
                                notePerceptionPrint.setArticlesBudgetaires(GeneralConst.EMPTY_STRING);
                            }
                        } else {
                            articleTaxe = listActeGenerateurPrint.get(0).getArticlesBudgetaires();
                            notePerceptionPrint.setArticlesBudgetaires(articleTaxe);
                        }

                        dataXhtm = createDocument(notePerceptionPrint, DocumentConst.DocumentCode.NP, false);
                    }

                    if (saveArchive) {
                        dataReturn = dataXhtm;
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                }

            }
        } catch (NumberFormatException e) {
            throw e;
        }

        return dataReturn;

    }

    public String createBP(BonAPayer bonAPayer) throws NumberFormatException, InvocationTargetException {

        BonAPayerPrint bonAPayerPrint = new BonAPayerPrint();
        String dataXhtm = GeneralConst.EMPTY_STRING;

        try {

            bonAPayerPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

            bonAPayerPrint.setNameSite(GeneralConst.EMPTY_STRING);

            bonAPayerPrint.setNameMinistrie(GeneralConst.EMPTY_STRING);

            bonAPayerPrint.setNameSector(GeneralConst.EMPTY_STRING);

            String devise = GeneralConst.EMPTY_STRING;
            String article = GeneralConst.EMPTY_STRING;
            String codeOfficielArticle = GeneralConst.EMPTY_STRING;
            DetailFichePriseCharge detailFPC = new DetailFichePriseCharge();
            SuiviComptableDeclaration sc;
            Amr amr = ContentieuxBusiness.loaderAmrByNumero(bonAPayer.getFkAmr().getNumero().trim());

            if (amr != null) {
                detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());
                if (detailFPC != null) {

                    sc = ContentieuxBusiness.getSuiviComptableDeclarationByNc(detailFPC.getNc().trim());
                    if (sc != null) {
                        if (sc.getDevise() != null) {
                            devise = sc.getDevise();
                        } else {
                            devise = GeneralConst.Devise.DEVISE_CDF;
                        }

                    } else {
                        devise = GeneralConst.Devise.DEVISE_CDF;
                    }
                    if (detailFPC.getArticleBudgetaire() != null) {

                        if (detailFPC.getArticleBudgetaire().getCodeOfficiel() != null
                                && !detailFPC.getArticleBudgetaire().getCodeOfficiel().isEmpty()) {
                            bonAPayerPrint.setCodeBudgetaire(detailFPC.getArticleBudgetaire().getCodeOfficiel());
                        } else {
                            bonAPayerPrint.setCodeBudgetaire(GeneralConst.EMPTY_STRING);
                        }

                        if (detailFPC.getArticleBudgetaire().getArrete() != null
                                && !detailFPC.getArticleBudgetaire().getArrete().isEmpty()) {
                            DocumentOfficiel documentOfficiel = getDocumentOfficielByCode(detailFPC.getArticleBudgetaire().getArrete().trim());
                            if (documentOfficiel != null) {
                                bonAPayerPrint.setArrete(documentOfficiel.getIntitule().toUpperCase().concat(
                                        GeneralConst.SPACE).concat(documentOfficiel.getNumero().trim()));
                            } else {
                                bonAPayerPrint.setArrete(GeneralConst.EMPTY_STRING);
                            }
                        } else {
                            bonAPayerPrint.setArrete(GeneralConst.EMPTY_STRING);
                        }
                        bonAPayerPrint.setArticleBudgetaire(detailFPC.getArticleBudgetaire().getIntitule().toUpperCase());
                    } else {
                        bonAPayerPrint.setCodeBudgetaire(GeneralConst.EMPTY_STRING);
                        bonAPayerPrint.setArticleBudgetaire(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    bonAPayerPrint.setArrete(GeneralConst.EMPTY_STRING);
                    bonAPayerPrint.setCodeBudgetaire(GeneralConst.EMPTY_STRING);
                    bonAPayerPrint.setArticleBudgetaire(GeneralConst.EMPTY_STRING);
                }
            } else {
                bonAPayerPrint.setArrete(GeneralConst.EMPTY_STRING);
                bonAPayerPrint.setCodeBudgetaire(GeneralConst.EMPTY_STRING);
                bonAPayerPrint.setArticleBudgetaire(GeneralConst.EMPTY_STRING);
            }
            bonAPayerPrint.setMontantChiffre(
                    formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            bonAPayer.getMontant()).concat(GeneralConst.SPACE).concat(devise));
            bonAPayerPrint.setNumeroBp(bonAPayer.getCode().trim());

            if (bonAPayer.getMotifPenalite() != null && !bonAPayer.getMotifPenalite().isEmpty()) {
                bonAPayerPrint.setMotifPenalite(bonAPayer.getMotifPenalite().trim());
            } else {
                bonAPayerPrint.setMotifPenalite(propertiesConfig.getProperty("TXT_PAIEMENT_UN_CINQUIEME_MONTANT_NON_CONTESTE"));
            }

            bonAPayerPrint.setAdresse("");

            if (bonAPayer.getFkCompte() != null) {

                bonAPayerPrint.setAccountBank(bonAPayer.getFkCompte().getCode().concat(
                        GeneralConst.SEPARATOR_SLASH).concat(
                                bonAPayer.getFkCompte().getIntitule().toUpperCase()));

                bonAPayerPrint.setBanque(bonAPayer.getFkCompte().getBanque().getIntitule().toUpperCase());

            } else {
                bonAPayerPrint.setAccountBank(propertiesConfig.getProperty("TXT_ACCOUNT_BANK_BAP"));
                bonAPayerPrint.setBanque(propertiesConfig.getProperty("TXT_BANK_NAME_BAP"));
            }
            bonAPayerPrint.setOrdonnancementDate(
                    ConvertDate.formatDateToString(bonAPayer.getDateCreat()));

            if (bonAPayer.getDateEcheance() != null) {
                bonAPayerPrint.setDateEcheance(
                        ConvertDate.formatDateToString(bonAPayer.getDateEcheance()));
            } else {
                bonAPayerPrint.setDateEcheance(GeneralConst.EMPTY_STRING);
            }

            Agent agent = ContentieuxBusiness.getAgentByCode(bonAPayer.getAgentCreat());
            if (agent != null) {
                bonAPayerPrint.setOrdonnanteurName(
                        agent.toString().toUpperCase().trim());
            } else {
                bonAPayerPrint.setOrdonnanteurName(GeneralConst.EMPTY_STRING);
            }

            if (propertiesConfig.getProperty("DISPLAY_CODE_QR_TO_DOCUMENT").equals(
                    GeneralConst.Number.ONE)) {

                String qrCode = generateQRCode(
                        propertiesConfig.getProperty("URL_TRACKING_DOC").concat(
                                bonAPayer.getCode().trim()));

                if (qrCode != null && !qrCode.isEmpty()) {
                    bonAPayerPrint.setCodeQR(qrCode);
                } else {
                    bonAPayerPrint.setCodeQR(GeneralConst.EMPTY_STRING);
                }
            } else {
                bonAPayerPrint.setCodeQR(GeneralConst.EMPTY_STRING);
            }

            String deviseLetter = GeneralConst.EMPTY_STRING;

            switch (devise.trim().toUpperCase()) {
                case GeneralConst.Devise.DEVISE_CDF:
                    deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                    break;
                case GeneralConst.Devise.DEVISE_USD:
                    deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                    break;
            }

            String letterOne;
            String letterTwo;
            String chiffreEnLettre;

            BigDecimal partInteger = new BigDecimal("0");
            BigDecimal partVirgule = new BigDecimal("0");

            String amount = String.format(GeneralConst.FORMAT_CONVERT, bonAPayer.getMontant());

            amount = amount.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

            if (amount != null && !amount.isEmpty()) {

                if (amount.contains(GeneralConst.VIRGULE)) {

                    String[] amountSplit = amount.split(GeneralConst.VIRGULE);

                    partInteger = BigDecimal.valueOf(Double.valueOf(String.valueOf(amountSplit[0])));
                    partVirgule = BigDecimal.valueOf(Double.valueOf(String.valueOf(amountSplit[1])));
                }
            }

            if (partInteger.floatValue() > 0) {
                letterOne = FrenchNumberToWords.convert(partInteger);

                if (partVirgule.floatValue() > 0) {
                    letterTwo = FrenchNumberToWords.convert(partVirgule);
                    chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                            + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;

                } else {
                    chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                }

                bonAPayerPrint.setMontantLettre(chiffreEnLettre);

            } else {
                bonAPayerPrint.setMontantLettre(GeneralConst.EMPTY_STRING);
            }

            if (bonAPayer.getFkPersonne() != null) {
                bonAPayerPrint.setAssujettiName(bonAPayer.getFkPersonne().toString().toUpperCase().trim());

                if (bonAPayer.getFkPersonne().getNif() != null
                        && !bonAPayer.getFkPersonne().getNif().isEmpty()) {

                    bonAPayerPrint.setNif(bonAPayer.getFkPersonne().getNif());

                } else {
                    bonAPayerPrint.setNif(GeneralConst.EMPTY_STRING);
                }

                if (bonAPayer.getFkPersonne().getLoginWeb() != null) {

                    if (bonAPayer.getFkPersonne().getLoginWeb().getTelephone() != null
                            && !bonAPayer.getFkPersonne().getLoginWeb().getTelephone().isEmpty()) {

                        bonAPayerPrint.setPhone(bonAPayer.getFkPersonne().getLoginWeb().getTelephone());

                    } else {
                        bonAPayerPrint.setPhone(GeneralConst.EMPTY_STRING);
                    }

                    if (bonAPayer.getFkPersonne().getLoginWeb().getMail() != null
                            && !bonAPayer.getFkPersonne().getLoginWeb().getMail().isEmpty()) {

                        bonAPayerPrint.setEmail(bonAPayer.getFkPersonne().getLoginWeb().getMail());
                    } else {
                        bonAPayerPrint.setEmail(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    bonAPayerPrint.setPhone(GeneralConst.EMPTY_STRING);
                    bonAPayerPrint.setEmail(GeneralConst.EMPTY_STRING);
                }
            } else {
                bonAPayerPrint.setPhone(GeneralConst.EMPTY_STRING);
                bonAPayerPrint.setEmail(GeneralConst.EMPTY_STRING);
                bonAPayerPrint.setAssujettiName(GeneralConst.EMPTY_STRING);
            }

            dataXhtm = createDocument(bonAPayerPrint, DocumentConst.DocumentCode.BON_A_PAYER, false);

        } catch (Exception e) {
            dataXhtm = ContentieuxConst.ResultCode.FAILED_EXCEPTION_SAVE_PRINT_BAP;
        }
        return dataXhtm;
    }

    String createDocument(Object printObject, String typeDocument, boolean exist) {

        String contenuXhtml;
        String reImpression;
        String documentContent = GeneralConst.SPACE;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Response response;
        String txtObservation = GeneralConst.EMPTY_STRING;
        String textObservation = GeneralConst.EMPTY_STRING;

        try {
            switch (typeDocument.trim()) {
                case DocumentConst.DocumentCode.PROJECT_ROLE:

                    ProjectRolePrint projectRolePrint = (ProjectRolePrint) printObject;

                    if (!exist) {

                        contenuXhtml = propertiesConfig.getProperty("MODEL_PROJET_ROLE");
                        reImpression = GeneralConst.Number.ONE;

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.PROJECT_ROLE,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        if (propertiesConfig.getProperty("SAVE_DOCUMENT_TO_TRACKINGDOC_ONLINE").equals("1")) {

                            response = saveDocumentToTrackingDoc(printObject, typeDocument);
                            if (response != null) {
                                switch (response.getCode()) {
                                    case "10":
                                        saveArchive = false;
                                        break;
                                    case "100":
                                        counter++;
                                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                                            projectRolePrint.getRoleId().trim(),
                                            DocumentConst.DocumentCode.PROJECT_ROLE,
                                            documentContent,
                                            textObservation,
                                            projectRolePrint.getRoleId().trim(),
                                            DocumentConst.DocumentCode.PROJECT_ROLE
                                        });
                                        saveArchive = executeQueryBulkInsert(bulkQuery);
                                        break;
                                }
                            } else {
                                saveArchive = false;
                            }

                        } else if (propertiesConfig.getProperty("SAVE_DOCUMENT_TO_TRACKINGDOC_ONLINE").equals("0")) {

                            counter++;
                            bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                                projectRolePrint.getRoleId().trim(),
                                DocumentConst.DocumentCode.PROJECT_ROLE,
                                documentContent,
                                textObservation,
                                projectRolePrint.getRoleId().trim(),
                                DocumentConst.DocumentCode.PROJECT_ROLE
                            });
                            saveArchive = executeQueryBulkInsert(bulkQuery);
                        }

                    }

                    break;

                case DocumentConst.DocumentCode.NP_VOIRIE:

                    NotePerceptionVoirieConcentres npvc = (NotePerceptionVoirieConcentres) printObject;

                    if (npvc != null) {
                        if (!exist) {

                            contenuXhtml = propertiesConfig.getProperty("MODEL_NP_VOIRIE_CONCENTRE");
                            reImpression = GeneralConst.Number.ONE;

                            documentContent = DocumentReturnString.getDocument(
                                    DocumentConst.DocumentSession.NOTE_PERCETION_VOIRIE,
                                    printObject,
                                    contenuXhtml, reImpression,
                                    GeneralConst.Number.ONE);

                            counter++;
                            bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                                npvc.getNumeroPerception(),
                                DocumentConst.DocumentCode.NP2,
                                documentContent,
                                "Impression de la note de perception : " + npvc.getNumeroPerception(),
                                npvc.getNumeroPerception(),
                                DocumentConst.DocumentCode.NP2
                            });

                            saveArchive = executeQueryBulkInsert(bulkQuery);

                        } else {
                            Archive archive = getArchiveByRefDocument(npvc.getNumeroPerception());
                            if (archive != null) {
                                documentContent = archive.getDocumentString();
                            }

                        }
                    }

                    break;

                case DocumentConst.DocumentCode.NP:

                    NotePerceptionPrint np = (NotePerceptionPrint) printObject;
                    if (np != null) {
                        if (!exist) {

                            contenuXhtml = propertiesConfig.getProperty("MODEL_NP_DRHKAT");
                            reImpression = GeneralConst.Number.ONE;

                            documentContent = DocumentReturnString.getDocument(
                                    DocumentConst.DocumentSession.NOTE_PERCETION,
                                    printObject,
                                    contenuXhtml, reImpression,
                                    GeneralConst.Number.ONE);

                            counter++;
                            bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                                np.getNumeroNotePerception().trim(),
                                DocumentConst.DocumentCode.NP2,
                                documentContent,
                                "Impression de la note de perception : " + np.getNumeroNotePerception().trim(),
                                np.getNumeroNotePerception().trim(),
                                DocumentConst.DocumentCode.NP2
                            });

                            /*counter++;
                             bulkQuery.put(counter + SQLQueryNotePerception.UPDATE_NBRE_IMPRESSION,
                             new Object[]{
                             np.getNumeroNotePerception().trim()
                             });*/
                            saveArchive = executeQueryBulkInsert(bulkQuery);

                        } else {
                            Archive archive = getArchiveByRefDocument(np.getNumeroNotePerception().trim());
                            if (archive != null) {
                                documentContent = archive.getDocumentString();
                            }

                        }
                    }

                    break;

                /*case "PRE-PAIEMENT":

                 RapportProductionPrepaiementPeagePrint rapportProductionPrepaiementPeagePrint = (RapportProductionPrepaiementPeagePrint) printObject;

                 contenuXhtml = propertiesConfig.getProperty("MODEL_RAPPORT_PREPAIEMENT_PEAGE");
                 reImpression = GeneralConst.Number.ONE;

                 documentContent = DocumentReturnString.getDocument(
                 "rapProDoc",
                 printObject,
                 contenuXhtml, reImpression,
                 GeneralConst.Number.ONE);

                 break;*/
                case DocumentConst.DocumentCode.INVITATION_A_PAYER:

                    InvitationAPayerMock invitationAPayerMock = (InvitationAPayerMock) printObject;

                    contenuXhtml = propertiesConfig.getProperty("MODEL_INVITATION_A_PAYER");
                    reImpression = GeneralConst.Number.ONE;

                    txtObservation = "Impression Invvitation à payer : " + invitationAPayerMock.getNumeroInvitationPaie();

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.INVITATION_A_PAYER,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        invitationAPayerMock.getId(),
                        DocumentConst.DocumentCode.INVITATION_A_PAYER_TXT,
                        documentContent,
                        txtObservation,
                        invitationAPayerMock.getId(),
                        DocumentConst.DocumentCode.INVITATION_A_PAYER_TXT
                    });

                    counter++;
                    bulkQuery.put(counter + ":UPDATE T_MED SET MONTANT_PENALITE = ?1 WHERE ID = ?2", new Object[]{
                        invitationAPayerMock.getAmountPenalite2(),
                        invitationAPayerMock.getId()
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.ATD:

                    AtdPrint atdPrint = (AtdPrint) printObject;

                    contenuXhtml = propertiesConfig.getProperty("MODEL_ATD_DRHKAT");
                    reImpression = GeneralConst.Number.ONE;

                    txtObservation = "Impression ATD : " + atdPrint.getNumeroAtd();

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.ATD,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        atdPrint.getNumeroAtd(),
                        DocumentConst.DocumentCode.ATD,
                        documentContent,
                        txtObservation,
                        atdPrint.getNumeroAtd(),
                        DocumentConst.DocumentCode.ATD
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.COMMANDEMENT:

                    CommandementMock commandementMock = (CommandementMock) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_COMMANDEMENT_HAUT_KATANGA");
                    reImpression = GeneralConst.Number.ONE;

                    txtObservation = "Impression du commandement : " + commandementMock.getNumeroCommandement();

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.COMMANDEMENT,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        commandementMock.getId(),
                        DocumentConst.DocumentCode.COMMANDEMENT,
                        documentContent,
                        txtObservation,
                        commandementMock.getId(),
                        DocumentConst.DocumentCode.COMMANDEMENT
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.AMR_INVITATION_A_PAYER:

                    AmrMock amrMock = (AmrMock) printObject;

                    if (amrMock != null) {

                        contenuXhtml = propertiesConfig.getProperty("MODEL_AMR_HAUT_KATANGA");
                        reImpression = GeneralConst.Number.ONE;

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.AMR_INVITATION_A_PAYER,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        counter++;
                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                            amrMock.getId(),
                            DocumentConst.DocumentCode.AMR_TAXATION_OFFICE_TXT,
                            documentContent,
                            "Impression AMR: " + amrMock.getId(),
                            amrMock.getId(),
                            DocumentConst.DocumentCode.AMR_TAXATION_OFFICE_TXT
                        });

                        if (xfrom == 2) {

                            counter++;
                            bulkQuery.put(counter + ":UPDATE T_AMR SET FK_RETRAIT_DECLARATION = ?1 WHERE NUMERO = ?2", new Object[]{
                                amrMock.getRetraitDeclID(),
                                amrMock.getId()

                            });
                        }

                        saveArchive = executeQueryBulkInsert(bulkQuery);

                    }

                    break;

                case DocumentConst.DocumentCode.NOTE_TAXATION_DECLARATION:

                    NoteTaxationMock noteTaxationMock = (NoteTaxationMock) printObject;

                    if (noteTaxationMock != null) {

                        contenuXhtml = propertiesConfig.getProperty("MODEL_NOTE_TAXATION_BIEN_IMMOBILIER");
                        reImpression = GeneralConst.Number.ONE;

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.NOTE_TAXATION,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        counter++;
                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                            noteTaxationMock.getNumeroDeclaration(),
                            DocumentConst.DocumentCode.NOTE_TAXATION_DECLARATION_TXT,
                            documentContent,
                            "Impression de la note de taxation : " + noteTaxationMock.getNumeroNoteTaxation(),
                            noteTaxationMock.getNumeroNoteTaxation(),
                            DocumentConst.DocumentCode.NOTE_TAXATION_DECLARATION_TXT
                        });

                        saveArchive = executeQueryBulkInsert(bulkQuery);

                    }

                    break;

                case DocumentConst.DocumentCode.CONTRAINTE:

                    ContrainteMock contrainteMock = (ContrainteMock) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_CONTRAINTE_HAUT_KATANGA");

                    reImpression = GeneralConst.Number.ONE;

                    txtObservation = "Impression de la contrainte : " + contrainteMock.getNumeroContrainte();

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.CONTRAINTE,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        contrainteMock.getId(),
                        DocumentConst.DocumentCode.CONTRAINTE,
                        documentContent,
                        txtObservation,
                        contrainteMock.getId(),
                        DocumentConst.DocumentCode.CONTRAINTE
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.AMR:

                    AmrPrint amrPrint = (AmrPrint) printObject;
                    contenuXhtml = GeneralConst.EMPTY_STRING;
                    reImpression = GeneralConst.Number.ONE;

                    switch (amrPrint.getTypeAmr()) {
                        case DocumentConst.DocumentCode.AMR1:
                            contenuXhtml = propertiesConfig.getProperty("MODEL_AMR_1_DRHKAT");
                            txtObservation = "Impression de l'avis de mise en recouvrement (1) : ";
                            break;
                        case DocumentConst.DocumentCode.AMR2:
                            contenuXhtml = propertiesConfig.getProperty("MODEL_AMR_2_DRHKAT");
//                            contenuXhtml = propertiesConfig.getProperty("MODEL_AMR_2_DGRK");
                            txtObservation = "Impression de l'avis de mise en recouvrement (2) : ";
                            break;
                    }

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.AMR,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        amrPrint.getId(),
                        DocumentConst.DocumentCode.AMR,
                        documentContent,
                        txtObservation.concat(GeneralConst.SPACE).concat(amrPrint.getId()),
                        amrPrint.getId(),
                        DocumentConst.DocumentCode.AMR
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.MEP:

                    MepPrint mepPrint = (MepPrint) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_MEP_DRHKAT");
//                    contenuXhtml = propertiesConfig.getProperty("MODEL_MEP_DGRK");
                    reImpression = GeneralConst.Number.ONE;

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.MEP,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        mepPrint.getNumeroMed().trim(),
                        DocumentConst.DocumentCode.MEP,
                        documentContent,
                        "Impression de la mise en demuere de paiement : " + mepPrint.getNumeroMed().trim(),
                        mepPrint.getNumeroMed().trim(),
                        "HTML"
                    });

                    counter++;
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_UPDATE_PRINT_MED, new Object[]{
                        mepPrint.getNumeroMed().trim()
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);
                    break;

                case DocumentConst.DocumentCode.INVITE_SERVICE:

                    MedPrint medPrinter = (MedPrint) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_INVIT_SERVICE_DRHKAT");
                    reImpression = GeneralConst.Number.ONE;

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.MED,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        medPrinter.getId(),
                        DocumentConst.DocumentCode.INVITATION_SERVICE_TXT,
                        documentContent,
                        "Impression de l'invitation de service : " + medPrinter.getNumeroMed().trim(),
                        medPrinter.getId(),
                        DocumentConst.DocumentCode.INVITATION_SERVICE_TXT
                    });

                    counter++;
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_UPDATE_PRINT_MED, new Object[]{
                        medPrinter.getId()
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.RELANCE:

                    MedPrint medPrint = (MedPrint) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_MED_DRHKAT");
//                    contenuXhtml = propertiesConfig.getProperty("MODEL_MED_DGRK");
                    reImpression = GeneralConst.Number.ONE;

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.MED,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        medPrint.getId(),
                        DocumentConst.DocumentCode.RELANCE,
                        documentContent,
                        "Impression de la relance : " + medPrint.getId(),
                        medPrint.getId(),
                        DocumentConst.DocumentCode.RELANCE
                    });

                    counter++;
                    bulkQuery.put(counter + SQLQueryRecouvrement.F_UPDATE_PRINT_MED, new Object[]{
                        medPrint.getId()
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.CPI:
                    CpiPrint cpi = (CpiPrint) printObject;
                    contenuXhtml = propertiesConfig.getProperty("MODEL_CPI_HAUT_KATANGA");
                    //contenuXhtml = propertiesConfig.getProperty("MODEL_CPI_KINSHASA");
                    //contenuXhtml = propertiesConfig.getProperty("MODEL_CPI_KINSHASA_PROJET_CONDUCTEUR");
                    reImpression = GeneralConst.Number.ONE;

                    documentContent = DocumentReturnString.getDocument(
                            DocumentConst.DocumentSession.CPI,
                            printObject,
                            contenuXhtml, reImpression,
                            GeneralConst.Number.ONE);

                    counter++;
                    bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                        cpi.getNumeroCpi().trim(),
                        DocumentConst.DocumentCode.CPI_TEXT,
                        documentContent,
                        "Impression de l'acquit libÃ©ratoire : " + cpi.getNumeroCpi().trim(),
                        cpi.getNumeroCpi().trim(),
                        DocumentConst.DocumentCode.CPI_TEXT
                    });

                    saveArchive = executeQueryBulkInsert(bulkQuery);

                    break;

                case DocumentConst.DocumentCode.BON_A_PAYER:

                    BapPrint bapPrint = (BapPrint) printObject;

                    if (bapPrint != null) {

                        contenuXhtml = propertiesConfig.getProperty("MODEL_BAP_DRHKAT");
//                        contenuXhtml = propertiesConfig.getProperty("MODEL_BAP_DGRK");
                        reImpression = GeneralConst.Number.ONE;

                        txtObservation = "Impression du bon Ã  payer : " + bapPrint.getNumeroBap();

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.BON_A_PAYER,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        counter++;
                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                            bapPrint.getNumeroBap(),
                            DocumentConst.DocumentCode.BON_A_PAYER,
                            documentContent,
                            txtObservation,
                            bapPrint.getNumeroBap(),
                            "HTML"
                        });

                        saveArchive = executeQueryBulkInsert(bulkQuery);

                    }

                    break;
                case DocumentConst.DocumentCode.RECEPISSE:

                    RecepissePrint recepissePrint = (RecepissePrint) printObject;

                    if (recepissePrint != null) {
                        exist = false;

                        if (!exist) {

                            textObservation = propertiesMessage.getProperty("TXT_IMPRESSION_DOCUMENT_RECEPISSE").concat(
                                    GeneralConst.SPACE).concat(recepissePrint.getNumDepotDeclaration().trim());

                            contenuXhtml = propertiesConfig.getProperty("MODEL_RECEPISSE_DRHKAT");

                            reImpression = GeneralConst.Number.ONE;

                            documentContent = DocumentReturnString.getDocument(
                                    DocumentConst.DocumentSession.RECEPISSE,
                                    printObject,
                                    contenuXhtml, reImpression,
                                    GeneralConst.Number.ONE);

                            counter++;
                            bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                                recepissePrint.getCodeDepotDeclaration(),
                                DocumentConst.DocumentCode.RECEPISSE_TXT,
                                documentContent,
                                textObservation,
                                recepissePrint.getCodeDepotDeclaration(),
                                DocumentConst.DocumentCode.RECEPISSE_TXT
                            });

                            counter++;
                            bulkQuery.put(counter + SQLQueryDeclaration.UPDATE_DECLARATION_PRINT,
                                    new Object[]{
                                        recepissePrint.getCodeDeclaration().trim()
                                    });

                            saveArchive = executeQueryBulkInsert(bulkQuery);
                            //break;

                            /*if (propertiesConfig.getProperty("SAVE_DOCUMENT_TO_TRACKINGDOC_ONLINE").equals("1")) {

                             response = saveDocumentToTrackingDoc(printObject, typeDocument);
                             if (response != null) {

                             switch (response.getCode()) {
                             case "10":
                             saveArchive = false;
                             break;
                             case "100":

                             counter++;
                             bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                             recepissePrint.getNumDepotDeclaration().trim(),
                             DocumentConst.DocumentCode.RECEPISSE,
                             documentContent,
                             textObservation,
                             recepissePrint.getNumDepotDeclaration().trim(),
                             DocumentConst.DocumentCode.RECEPISSE
                             });

                             counter++;
                             bulkQuery.put(counter + SQLQueryDeclaration.UPDATE_DECLARATION_PRINT,
                             new Object[]{
                             recepissePrint.getCodeDeclaration().trim()
                             });

                             saveArchive = executeQueryBulkInsert(bulkQuery);
                             break;
                             }
                             } else {
                             saveArchive = false;
                             }
                             } else if (propertiesConfig.getProperty("SAVE_DOCUMENT_TO_TRACKINGDOC_ONLINE").equals("0")) {

                             counter++;
                             bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                             recepissePrint.getNumDepotDeclaration().trim(),
                             DocumentConst.DocumentCode.RECEPISSE,
                             documentContent,
                             textObservation,
                             recepissePrint.getNumDepotDeclaration().trim(),
                             DocumentConst.DocumentCode.RECEPISSE
                             });

                             counter++;
                             bulkQuery.put(counter + SQLQueryDeclaration.UPDATE_DECLARATION_PRINT,
                             new Object[]{
                             recepissePrint.getCodeDeclaration().trim()
                             });

                             saveArchive = executeQueryBulkInsert(bulkQuery);
                             break;
                             }*/
                        } else {
                            Archive archive = getArchiveByRefDocument(recepissePrint.getNumDepotDeclaration().trim());
                            if (archive != null) {
                                documentContent = archive.getDocumentString();
                            }
                        }

                    }

                    break;
                case DocumentConst.DocumentCode.VOLET_A:

                    VoletMock voletMock = (VoletMock) printObject;

                    if (!exist) {
                        contenuXhtml = propertiesConfig.getProperty("MODELE_VOLET_A");
                        reImpression = GeneralConst.Number.ONE;

                        txtObservation = "Impression volet A : " + voletMock.getNumeroNote();

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.VOLET_A,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        counter++;
                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                            voletMock.getId(),
                            DocumentConst.DocumentCode.VOLET_A_TXT,
                            documentContent,
                            txtObservation,
                            voletMock.getId(),
                            DocumentConst.DocumentCode.VOLET_A_TXT
                        });

                        saveArchive = executeQueryBulkInsert(bulkQuery);
                    } else {
                        Archive archive = getArchiveByRefDocument(voletMock.getId());
                        if (archive != null) {
                            documentContent = archive.getDocumentString();
                        }
                    }
                    break;
                case DocumentConst.DocumentCode.VOLET_B:

                    VoletMock voletMock2 = (VoletMock) printObject;

                    if (!exist) {
                        contenuXhtml = propertiesConfig.getProperty("MODELE_VOLET_B");
                        reImpression = GeneralConst.Number.ONE;

                        txtObservation = "Impression volet B : " + voletMock2.getNumeroNote();

                        documentContent = DocumentReturnString.getDocument(
                                DocumentConst.DocumentSession.VOLET_A,
                                printObject,
                                contenuXhtml, reImpression,
                                GeneralConst.Number.ONE);

                        counter++;
                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_ARCHIVE_WEB, new Object[]{
                            voletMock2.getId(),
                            DocumentConst.DocumentCode.VOLET_B_TXT,
                            documentContent,
                            txtObservation,
                            voletMock2.getId(),
                            DocumentConst.DocumentCode.VOLET_B_TXT
                        });

                        saveArchive = executeQueryBulkInsert(bulkQuery);
                    } else {
                        Archive archive = getArchiveByRefDocument(voletMock2.getId());
                        if (archive != null) {
                            documentContent = archive.getDocumentString();
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            throw e;
        }

        return documentContent;
    }

    private Response saveDocumentToTrackingDoc(Object printObject, String typeDoument) {
        Response response = null;

        if (printObject != null) {

            TrackingDocModel trackingDocModel = new TrackingDocModel();

            String dateProduction = GeneralConst.EMPTY_STRING;

            String codeDocument = GeneralConst.EMPTY_STRING;
            String typeDoc = GeneralConst.EMPTY_STRING;

            switch (typeDoument.trim()) {
                case DocumentConst.DocumentCode.RECEPISSE:
                    RecepissePrint recepissePrint = (RecepissePrint) printObject;
                    codeDocument = recepissePrint.getNumDepotDeclaration().trim();
                    typeDoc = DocumentConst.DocumentCode.RECEPISSE;
                    break;
            }

            trackingDocModel.setCode(codeDocument);
            trackingDocModel.setType(typeDoc);
            trackingDocModel.setFormat(propertiesConfig.getProperty("TYPE_FORMAT_APPLICATION"));
            trackingDocModel.setDate_production(dateProduction);
            trackingDocModel.setApplication(propertiesConfig.getProperty("ID_APPLICATION"));

            List<TrackingDocModel> trackingDocModels = new ArrayList<>();

            trackingDocModels.add(trackingDocModel);

            Gson gson = new Gson();
            String objectJson = gson.toJson(trackingDocModels);

            Form f = new Form();
            f.param("data", objectJson);

            Client client = ClientBuilder.newClient();
            Invocation.Builder builder = (Invocation.Builder) client.target(
                    propertiesConfig.getProperty("URL_POST_TRACKING_DOC")).path(GeneralConst.EMPTY_STRING).request();
            Invocation inv = builder
                    .header("Content-type", MediaType.APPLICATION_FORM_URLENCODED_TYPE)
                    .buildPost(Entity.form(f));
            String result = inv.invoke(String.class);
            response = gson.fromJson(result, Response.class);

        }

        return response;
    }

    public String createInviteSevice(Med med, String articleName, String assujettiName,
            String adresseName, String exercice, String echeance, String dateHeureInvitation,
            String periodeDeclaration, String agentSignateur, int userID) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        MedPrint medPrint = new MedPrint();

        medPrint.setId(med.getId());
        medPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
        medPrint.setEcheance(echeance);
        medPrint.setArticleBudgetaire(articleName);
        medPrint.setImpot(articleName.toUpperCase());
        medPrint.setNomAssujetti(assujettiName.toUpperCase());
        medPrint.setExercice(exercice);
        medPrint.setNumImpot(med.getPersonne().trim());
        medPrint.setAdresseAssujetti(adresseName);

        medPrint.setNumeroMed(Tools.generateDocumentNumber(userID + "", "SERVICE", med.getId()));
        medPrint.setDateHeureInvitation(dateHeureInvitation);
        medPrint.setPeriodeDeclaration(periodeDeclaration);
        medPrint.setAgentSignateur(agentSignateur);

        Agent agent = GeneralBusiness.getAgentByCode(userID + "");

        medPrint.setLieuImpression(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());

//        if (codeAgent != null && !codeAgent.equals(GeneralConst.EMPTY_STRING)) {
//            medPrint.setNumeroMed(Tools.generateDocumentNumber(codeAgent, "INVITATION_SERVICE", med.getId()));
//        } else {
//            medPrint.setNumeroMed(med.getId());
//        }
        if (med.getPersonne() != null && !med.getPersonne().isEmpty()) {
            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(med.getPersonne().trim());

            if (loginWeb != null) {
                medPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                medPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                medPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                medPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }
        } else {
            medPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            medPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
        }

        medPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        echeanceMep = Tools.getEcheanceDate(new Date(), GeneralConst.Numeric.HEIGHT);

        if (echeanceMep != null) {
            medPrint.setDateLimiteAccordee(ConvertDate.formatDateToStringOfFormat(
                    echeanceMep, "dd/MM/YYYY"));
        } else {
            medPrint.setDateLimiteAccordee(GeneralConst.EMPTY_STRING);
        }

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(med.getId());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            medPrint.setCodeQR(codeQR);
        } else {
            medPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        dataXhtm = createDocument(medPrint, DocumentConst.DocumentCode.INVITE_SERVICE, false);

        return dataXhtm;
    }

    public String createRecepisse(RecepissePrint recepissePrint, Boolean exist) throws InvocationTargetException {

        if (!exist) {

            recepissePrint.setHeadTitle(String.format(propertiesConfig.getProperty("CONTENT_HEADER_RCPS"),
                    null, null, null));
            recepissePrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
//            recepissePrint.setLogo(propertiesConfig.getProperty("LOGO_DGRK_2"));

            if (propertiesConfig.getProperty("DISPLAY_CODE_QR_TO_DOCUMENT").equals(
                    GeneralConst.Number.ONE)) {

                String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(recepissePrint.getNumDepotDeclaration());
                String codeQR = generateQRCode(url);

                if (codeQR != null && !codeQR.isEmpty()) {
                    recepissePrint.setCodeQR(codeQR);
                } else {
                    recepissePrint.setCodeQR(GeneralConst.EMPTY_STRING);
                }

            } else {
                recepissePrint.setCodeQR(GeneralConst.EMPTY_STRING);
            }

        }

        String dataXhtm = createDocument(recepissePrint, DocumentConst.DocumentCode.RECEPISSE, exist);

        return dataXhtm;
    }

    public String createContrainte(Contrainte contrainte, String userId) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        ContraintePrint comContraintePrint = new ContraintePrint();

        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();

        ParamContrainte paramContrainte = new ParamContrainte();
        String devise = GeneralConst.EMPTY_STRING;

        Amr amr = contrainte.getIdAmr() != null ? contrainte.getIdAmr() : null;

        if (amr != null) {

            comContraintePrint.setNumeroAmr(amr.getNumero());

            String dateReception = ConvertDate.formatDateToStringOfFormat(amr.getDateReceptionAmr(), "dd/MM/YYYY");
            comContraintePrint.setDateReceptionAmr(dateReception);

            personne = amr.getFichePriseCharge().getPersonne();

            devise = amr.getFichePriseCharge().getDevise() != null ? amr.getFichePriseCharge().getDevise() : GeneralConst.EMPTY_STRING;

            comContraintePrint.setNomAssujetti(personne.toString().toUpperCase());
            comContraintePrint.setNumImpot(personne.getCode());

            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                comContraintePrint.setTelephoneAssujetti(loginWeb.getTelephone());
                comContraintePrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                comContraintePrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                comContraintePrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                comContraintePrint.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                comContraintePrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }
        }

        comContraintePrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(contrainte.getId());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            comContraintePrint.setCodeQR(codeQR);
        } else {
            comContraintePrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        comContraintePrint.setDateImpression(
                ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            comContraintePrint.setUserPrintDocument(agent.toString().toUpperCase());

        } else {
            comContraintePrint.setUserPrintDocument(GeneralConst.EMPTY_STRING);

        }

        //comContraintePrint.setNumeroContrainte(contrainte.getId());
        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            comContraintePrint.setNumeroContrainte(Tools.generateDocumentNumber(userId, "CONTRAINTE", contrainte.getId()));
        } else {
            comContraintePrint.setNumeroContrainte(contrainte.getId());
        }

        paramContrainte = PoursuiteBusiness.getParamContrainte();

        if (paramContrainte != null) {

            String amendeContrainte = "Amendes nÂ° ".concat(paramContrainte.getRefFaitGenerateur());

            comContraintePrint.setAmendeContrainte(amendeContrainte);
            comContraintePrint.setAgentHuissier("Mr. ".concat(paramContrainte.getAgentCmdt()));

            CompteBancaire compteBancairePenalite = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getComptePenalite());

            if (compteBancairePenalite != null) {
                comContraintePrint.setCompteBancaireDgrk(
                        compteBancairePenalite.getCode().concat(" Ã  ").concat(
                                compteBancairePenalite.getBanque().getIntitule().toUpperCase()));
            } else {
                comContraintePrint.setCompteBancaireDgrk(GeneralConst.EMPTY_STRING);
            }

            CompteBancaire compteBancaireTresorUrbain = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getCompteTresorUrbain());

            if (compteBancaireTresorUrbain != null) {
                comContraintePrint.setCompteBancaireTresorUrbain(
                        compteBancaireTresorUrbain.getCode().concat(" Ã  ").concat(
                                compteBancaireTresorUrbain.getBanque().getIntitule().toUpperCase()));
            } else {
                comContraintePrint.setCompteBancaireTresorUrbain(GeneralConst.EMPTY_STRING);
            }

            CompteBancaire compteBancaireDgrk = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getCompteFonctionnement());

            if (compteBancaireDgrk != null) {
                comContraintePrint.setCompteBancaireDgrk_Bap(
                        compteBancaireDgrk.getCode().concat(" Ã  ").concat(
                                compteBancaireDgrk.getBanque().getIntitule().toUpperCase()));
            } else {
                comContraintePrint.setCompteBancaireDgrk_Bap(GeneralConst.EMPTY_STRING);
            }

        } else {
            comContraintePrint.setAmendeContrainte(GeneralConst.EMPTY_STRING);
            comContraintePrint.setAgentHuissier(GeneralConst.EMPTY_STRING);
            comContraintePrint.setCompteBancaireDgrk_Bap(GeneralConst.EMPTY_STRING);
            comContraintePrint.setCompteBancaireTresorUrbain(GeneralConst.EMPTY_STRING);
            comContraintePrint.setCompteBancaireDgrk(GeneralConst.EMPTY_STRING);
        }

        comContraintePrint.setFaitGenerateur("Amendes");

        if (contrainte != null) {

            comContraintePrint.setMontantAmr(String.format(GeneralConst.FORMAT_CONVERT,
                    contrainte.getMontantRecouvrement()).concat(GeneralConst.SPACE).concat(devise));

            comContraintePrint.setPenalite(String.format(GeneralConst.FORMAT_CONVERT,
                    contrainte.getMontantPenalite()).concat(GeneralConst.SPACE).concat(devise));

            comContraintePrint.setCodePartDgrk(String.format(GeneralConst.FORMAT_CONVERT,
                    contrainte.getMontantPenalite()).concat(GeneralConst.SPACE).concat(devise));

            comContraintePrint.setCotePartTresorUrbain(String.format(GeneralConst.FORMAT_CONVERT,
                    contrainte.getMontantRecouvrement()).concat(GeneralConst.SPACE).concat(devise));

            comContraintePrint.setFraisPursuite(String.format(GeneralConst.FORMAT_CONVERT,
                    contrainte.getMontantFraisPoursuite()).concat(GeneralConst.SPACE).concat(devise));

        } else {

            comContraintePrint.setMontantAmr(GeneralConst.EMPTY_STRING);
            comContraintePrint.setPenalite(GeneralConst.EMPTY_STRING);
            comContraintePrint.setCodePartDgrk(GeneralConst.EMPTY_STRING);
            comContraintePrint.setCotePartTresorUrbain(GeneralConst.EMPTY_STRING);
            comContraintePrint.setFraisPursuite(GeneralConst.EMPTY_STRING);
        }

        comContraintePrint.setTaux(GeneralConst.Number.THREE.concat("%"));

        return dataXhtm = createDocument(comContraintePrint, DocumentConst.DocumentCode.CONTRAINTE, false);
    }

    public String createContrainteV2(Contrainte contrainte, String userId, String inputParquet, Med med) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        ContrainteMock contrainteMock = new ContrainteMock();

        Personne personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());
        Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(med.getPersonne());

        Agent agent = GeneralBusiness.getAgentByCode(userId);

        contrainteMock.setNameReceveur(agent == null ? GeneralConst.EMPTY_STRING : agent.toString().toUpperCase());

        contrainteMock.setLieuPrint(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());

        contrainteMock.setContribuable(personne.toString().toUpperCase());
        contrainteMock.setAdresseContribuable(adresse.toString().toUpperCase());
        contrainteMock.setNumeroAmr(contrainte.getIdAmr().getNumeroDocument());
        //contrainteMock.setNumeroContrainte(contrainte.getId());
        contrainteMock.setParquet(inputParquet);

        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            contrainteMock.setNumeroContrainte(Tools.generateDocumentNumber(userId, "CONTRAINTE", contrainte.getId()));
        } else {
            contrainteMock.setNumeroContrainte(contrainte.getId());
        }

        contrainteMock.setId(contrainte.getId());

        contrainteMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(contrainte.getId() + "");

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            contrainteMock.setCodeQr(codeQR);
        } else {
            contrainteMock.setCodeQr(GeneralConst.EMPTY_STRING);
        }

        contrainteMock.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        contrainteMock.setNomHuissier(contrainte.getAgentCommandement().toUpperCase());

        ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(contrainte.getFaitGenerateur());

        contrainteMock.setActeGenerateur(articleBudgetaire.getIntitule().toUpperCase());

        String devise = GeneralConst.EMPTY_STRING;
        String libellePeriode = GeneralConst.EMPTY_STRING;

        BigDecimal acompte = new BigDecimal(0);

        NotePerception notePerception = new NotePerception();
        List<Journal> listJournal = new ArrayList<>();

        switch (med.getTypeMed()) {

            case "INVITATION_PAIEMENT":
            case "RELANCE":

                PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(med.getPeriodeDeclaration() + "");
                libellePeriode = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());

                RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");
                devise = retraitDeclaration.getDevise();

                Bordereau bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDeclaration.getCodeDeclaration());

                if (bordereau != null) {
                    acompte = acompte.add(bordereau.getTotalMontantPercu());
                }

                break;

            case "INVITATION_SERVICE":

                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                libellePeriode = notePerception.getNoteCalcul().getExercice();
                devise = notePerception.getDevise();

                listJournal = AcquitLiberatoireBusiness.getListJournalByDocumentApure(notePerception.getNumero());

                if (!listJournal.isEmpty()) {

                    for (Journal journal : listJournal) {
                        acompte = acompte.add(journal.getMontant());
                    }
                }
                break;
            default:
                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                libellePeriode = notePerception.getNoteCalcul().getExercice();
                devise = notePerception.getDevise();

                listJournal = AcquitLiberatoireBusiness.getListJournalByDocumentApure(notePerception.getNumero());

                if (!listJournal.isEmpty()) {

                    for (Journal journal : listJournal) {
                        acompte = acompte.add(journal.getMontant());
                    }
                }
                break;
        }

        contrainteMock.setPeriodeDeclaration(libellePeriode.toUpperCase());

        contrainteMock.setPrincipal(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                med.getMontant()) + GeneralConst.SPACE + devise);

        BigDecimal solde = new BigDecimal(0);

        contrainteMock.setAccompte(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                acompte) + GeneralConst.SPACE + devise);

        if (acompte.floatValue() > 0) {
            solde = med.getMontant().subtract(acompte);
        } else {
            solde = solde.add(med.getMontant());
        }

        contrainteMock.setSolde(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                solde) + GeneralConst.SPACE + devise);

        contrainteMock.setDateEcheance(ConvertDate.formatDateToStringOfFormat(contrainte.getDateEcheance(), "dd/MM/YYYY"));

        contrainteMock.setTauxPenalite(propertiesConfig.getProperty("TAUX_PENALITE_CONTRAINTE"));

        contrainteMock.setCumulPenalite(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                med.getMontantPenalite()) + GeneralConst.SPACE + devise);

        BigDecimal amountTotal = new BigDecimal(0);
        BigDecimal fraisPoursuites = new BigDecimal(0);

        amountTotal = amountTotal.add(med.getMontant());
        amountTotal = amountTotal.add(med.getMontantPenalite());

        BigDecimal _3pourcent = new BigDecimal("0.03");

        fraisPoursuites = fraisPoursuites.add(amountTotal.multiply(_3pourcent));

        contrainteMock.setFraisPoursuite(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                fraisPoursuites) + GeneralConst.SPACE + devise);

        amountTotal = amountTotal.add(fraisPoursuites);

        contrainteMock.setTotalCreanceFiscal(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                amountTotal) + GeneralConst.SPACE + devise);

        return dataXhtm = createDocument(contrainteMock, DocumentConst.DocumentCode.CONTRAINTE, false);
    }

    public String createNoteTaxationDeclaration(RetraitDeclaration retraitDeclaration, String userId) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        NoteTaxationMock noteTaxationMock = new NoteTaxationMock();

        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        Banque banque = new Banque();
        CompteBancaire compteBancaire = new CompteBancaire();

        noteTaxationMock.setFlag(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(retraitDeclaration.getId() + "");

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            noteTaxationMock.setCodeQR(codeQR);
        } else {
            noteTaxationMock.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        BigDecimal amountTaxation = new BigDecimal(0);

        noteTaxationMock.setDateTaxation(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        noteTaxationMock.setDateEcheance(ConvertDate.formatDateToStringOfFormat(retraitDeclaration.getDateEcheancePaiement(), "dd/MM/YYYY"));
        noteTaxationMock.setNumeroNoteTaxation(retraitDeclaration.getId() + "");

        personne = IdentificationBusiness.getPersonneByCode(retraitDeclaration.getFkAssujetti());

        String nomProprietaire = GeneralConst.EMPTY_STRING;

        if (retraitDeclaration.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL"))) {

            PeriodeDeclaration pd = AssujettissementBusiness.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());

            if (pd != null) {

                Bien bien = pd.getAssujetissement().getBien();

                if (bien.getAcquisitionList().size() > 0) {

                    for (Acquisition acquisition : bien.getAcquisitionList()) {

                        if (acquisition.getProprietaire()) {
                            nomProprietaire = acquisition.getPersonne().toString().toUpperCase();
                        }
                    }
                }
            }
        }

        if (personne != null) {

            if (retraitDeclaration.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL"))) {

                noteTaxationMock.setContribuable(nomProprietaire);
                noteTaxationMock.setNif(personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase());
                noteTaxationMock.setRedevable(personne.toString().toUpperCase());

            } else {

                noteTaxationMock.setContribuable(personne.toString().toUpperCase());
                noteTaxationMock.setNif(personne.getNif() == null ? GeneralConst.EMPTY_STRING : personne.getNif().toUpperCase());
                noteTaxationMock.setRedevable(GeneralConst.EMPTY_STRING);

            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                noteTaxationMock.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                noteTaxationMock.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {
            noteTaxationMock.setContribuable(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setNif(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setRedevable(GeneralConst.EMPTY_STRING);

        }

        String villeDefault = propertiesConfig.getProperty("VILLE_DEFAULT");

        agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            noteTaxationMock.setNomFonctionTaxateur(agent.toString().toUpperCase() + "<br/>" + agent.getFonction().getIntitule().toUpperCase());

            if (agent.getSite() != null) {
                noteTaxationMock.setLieuTaxation(agent.getSite().getAdresse() == null ? villeDefault : agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());

                String divisionName = GeneralConst.EMPTY_STRING;

                if (agent.getSite().getDivision() != null) {

                    divisionName = " / " + agent.getSite().getDivision().getIntitule().toUpperCase();

                }

                noteTaxationMock.setEntiteEmetrice(agent.getSite().getIntitule().toUpperCase().concat(divisionName));

            } else {
                noteTaxationMock.setLieuTaxation(villeDefault);
                noteTaxationMock.setEntiteEmetrice(GeneralConst.EMPTY_STRING);
            }

        } else {
            noteTaxationMock.setNomFonctionTaxateur(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setEntiteEmetrice(GeneralConst.EMPTY_STRING);
            noteTaxationMock.setLieuTaxation(villeDefault);
        }

        if (retraitDeclaration.getRetraitDeclarationMere() == null) {
            noteTaxationMock.setTypeNoteTaxation("PRINCIPALE");
        } else {
            noteTaxationMock.setTypeNoteTaxation("PENALITE");
        }

        ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

        noteTaxationMock.setImpot(articleBudgetaire == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getIntitule().toUpperCase());

        PeriodeDeclaration periodeDeclaration = AssujettissementBusiness.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());

        String periodeName = Tools.getPeriodeIntitule(periodeDeclaration.getDebut(), articleBudgetaire.getPeriodicite().getCode());

        noteTaxationMock.setPeriodeDeclaration(periodeName.toUpperCase());

        noteTaxationMock.setAdresseBien(periodeDeclaration == null ? GeneralConst.EMPTY_STRING : periodeDeclaration.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString().toUpperCase());

        noteTaxationMock.setNumeroDeclaration(retraitDeclaration.getCodeDeclaration().toUpperCase());

        String deviseLetter = GeneralConst.EMPTY_STRING;

        switch (retraitDeclaration.getDevise().toUpperCase()) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal("0");
        BigDecimal partVirgule = new BigDecimal("0");

        boolean virguleExist;

        if (retraitDeclaration.getEstPenalise() == GeneralConst.Numeric.ONE) {

            amountTaxation = amountTaxation.add(retraitDeclaration.getMontant());

        } else {

            if (retraitDeclaration.getPenaliteRemise().floatValue() > 0) {
                amountTaxation = amountTaxation.add(retraitDeclaration.getPenaliteRemise());
            } else {
                amountTaxation = amountTaxation.add(retraitDeclaration.getMontant());
            }
        }

        String amount = String.format("%.2f", amountTaxation);

        amount = amount.replace(".", ",");

        if (amount != null && !amount.isEmpty()) {

            if (amount.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amount.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);
                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                virguleExist = false;
            }

            noteTaxationMock.setMontantLettre(chiffreEnLettre);

        } else {
            noteTaxationMock.setMontantLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        if (virguleExist) {

            noteTaxationMock.setMontantChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                    amountTaxation) + GeneralConst.SPACE + retraitDeclaration.getDevise());
        } else {
            noteTaxationMock.setMontantChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                    amountTaxation)
                    + GeneralConst.Format.ZERO_FORMAT
                    + GeneralConst.SPACE + retraitDeclaration.getDevise());
        }

        banque = GeneralBusiness.getBanqueByCode(retraitDeclaration.getFkBanque());

        noteTaxationMock.setBanque(banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

        compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDeclaration.getFkCompteBancaire());

        noteTaxationMock.setCompteBancaire(compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

        return dataXhtm = createDocument(noteTaxationMock, DocumentConst.DocumentCode.NOTE_TAXATION_DECLARATION, false);
    }

    public String createAtd(Atd atd, String userId) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        PrintDocument printDocument = new PrintDocument();
        AtdPrint atdPrint = new AtdPrint();

        ParamContrainte paramContrainte = PoursuiteBusiness.getParamContrainte();
        CompteBancaire compteBancaire = new CompteBancaire();
        Agent agent = new Agent();
        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        LoginWeb loginWeb = new LoginWeb();

        //atdPrint.setNumeroAtd(atd.getId());
        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            atdPrint.setNumeroAtd(Tools.generateDocumentNumber(userId, "ATD", atd.getId()));
        } else {
            atdPrint.setNumeroAtd(atd.getId());
        }

        atdPrint.setId(atd.getId());

        personne = IdentificationBusiness.getPersonneByCode(atd.getIdRedevable());

        if (personne != null) {

            atdPrint.setNomAssujetti(personne.toString().toUpperCase());
            atdPrint.setNumImpot(personne.getCode());

            loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                atdPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                atdPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                atdPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                atdPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                atdPrint.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                atdPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {
            atdPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            atdPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            atdPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
        }

        personne = IdentificationBusiness.getPersonneByCode(atd.getIdDetenteur());

        if (personne != null) {

            atdPrint.setNomDetenteur(personne.toString().toUpperCase());

            loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                atdPrint.setTelephoneDetenteur(loginWeb.getTelephone());
                atdPrint.setEmailDetenteur(loginWeb.getMail());
            } else {
                atdPrint.setTelephoneDetenteur(GeneralConst.EMPTY_STRING);
                atdPrint.setEmailDetenteur(GeneralConst.EMPTY_STRING);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                atdPrint.setAdresseDetenteur(adresse.toString().toUpperCase());
            } else {
                atdPrint.setAdresseDetenteur(GeneralConst.EMPTY_STRING);
            }

        } else {
            atdPrint.setNomDetenteur(GeneralConst.EMPTY_STRING);
            atdPrint.setTelephoneDetenteur(GeneralConst.EMPTY_STRING);
            atdPrint.setEmailDetenteur(GeneralConst.EMPTY_STRING);
            atdPrint.setAdresseDetenteur(GeneralConst.EMPTY_STRING);
        }

        atdPrint.setNomSaisi(GeneralConst.EMPTY_STRING);
        atdPrint.setQualiteSaisi(GeneralConst.EMPTY_STRING);
        atdPrint.setDateReceptionSaisi(GeneralConst.EMPTY_STRING);
        atdPrint.setQualiteDetenteur(GeneralConst.EMPTY_STRING);

        String compteName = GeneralConst.EMPTY_STRING;
        String banqueName = GeneralConst.EMPTY_STRING;
        String devise = GeneralConst.EMPTY_STRING;

        Amr amr = PoursuiteBusiness.getAmrByNumero(atd.getNumeroDocument());

        if (amr != null) {
            devise = amr.getFichePriseCharge().getDevise();
        }

        if (paramContrainte != null) {

            compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getCompteTresorUrbain());

            if (compteBancaire != null) {
                banqueName = compteBancaire.getBanque().getIntitule().toUpperCase();
                compteName = compteBancaire.getCode().concat(" Ã  la ").concat(banqueName).concat(" (Compte trÃ©sor urbain)");
                atdPrint.setCompteTresorUrbain(compteName);

            } else {
                atdPrint.setCompteTresorUrbain(compteName);
            }

            compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getComptePenalite());

            if (compteBancaire != null) {
                banqueName = compteBancaire.getBanque().getIntitule().toUpperCase();
                compteName = compteBancaire.getCode().concat(" Ã  la ").concat(banqueName);

                atdPrint.setComptePenalite(compteName);
            } else {
                atdPrint.setComptePenalite(compteName);
            }

            compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(
                    paramContrainte.getCompteFonctionnement());

            if (compteBancaire != null) {
                banqueName = compteBancaire.getBanque().getIntitule().toUpperCase();
                compteName = compteBancaire.getCode().concat(" Ã  la ").concat(banqueName);

                atdPrint.setCompteFonctionnement(compteName);
            } else {
                atdPrint.setCompteFonctionnement(compteName);
            }

        } else {
            atdPrint.setCompteTresorUrbain(compteName);
            atdPrint.setComptePenalite(compteName);
            atdPrint.setCompteFonctionnement(compteName);
        }

        atdPrint.setAmountGlobal(String.format(GeneralConst.FORMAT_CONVERT,
                atd.getMontantGlobal()).concat(GeneralConst.SPACE).concat(devise));

        atdPrint.setAmountPenalite(String.format(GeneralConst.FORMAT_CONVERT,
                atd.getMontantPenalite()).concat(GeneralConst.SPACE).concat(devise));

        atdPrint.setAmountPrincipal(String.format(GeneralConst.FORMAT_CONVERT,
                atd.getMontantPrincipal()).concat(GeneralConst.SPACE).concat(devise));

        atdPrint.setAmountFonctionnement(String.format(GeneralConst.FORMAT_CONVERT,
                atd.getMontantFonctionnement()).concat(GeneralConst.SPACE).concat(devise));

        atdPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(atd.getId());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            atdPrint.setCodeQR(codeQR);
        } else {
            atdPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        atdPrint.setDateImpression(
                ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        atdPrint.setDateReceptionDetenteur(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            atdPrint.setNomReceveur(agent.toString().toUpperCase());

        } else {
            atdPrint.setNomReceveur(GeneralConst.EMPTY_STRING);

        }

        return dataXhtm = createDocument(atdPrint, DocumentConst.DocumentCode.ATD, false);
    }

    public String createCommandement(Commandement commandement, String userId) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        CommandementPrint commandementPrint = new CommandementPrint();

        String devise = GeneralConst.EMPTY_STRING;

        String toDays = Tools.getDateString(new Date(), true);

        if (!toDays.isEmpty()) {
            toDays.concat(" vers ").concat(ConvertDate.timeDate(new Date()).concat(" heures"));
        }

        commandementPrint.setDateAndHeurePrintDocumentToText(toDays);

        if (commandement.getIdContrainte() != null) {

            Contrainte contrainte = commandement.getIdContrainte();

            Amr amr = contrainte.getIdAmr() != null ? contrainte.getIdAmr() : null;

            if (amr != null) {

                personne = amr.getFichePriseCharge().getPersonne();

                devise = amr.getFichePriseCharge().getDevise() != null ? amr.getFichePriseCharge().getDevise() : GeneralConst.EMPTY_STRING;

                commandementPrint.setNomAssujetti(personne.toString().toUpperCase());
                commandementPrint.setNumImpot(personne.getCode());

                LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

                if (loginWeb != null) {
                    commandementPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                    commandementPrint.setEmailAssujetti(loginWeb.getMail());
                } else {
                    commandementPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                    commandementPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
                }

                adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                if (adresse != null) {
                    commandementPrint.setAdresseAssujetti(adresse.toString().toUpperCase());
                } else {
                    commandementPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
                }
            }

            commandementPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

            String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(contrainte.getId());

            String codeQR = generateQRCode(url);

            if (codeQR != null && !codeQR.isEmpty()) {
                commandementPrint.setCodeQR(codeQR);
            } else {
                commandementPrint.setCodeQR(GeneralConst.EMPTY_STRING);
            }

            commandementPrint.setDateImpression(
                    ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

            commandementPrint.setDateEmission(
                    ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

            agent = GeneralBusiness.getAgentByCode(userId);

            if (agent != null) {
                commandementPrint.setUserPrintDocument(agent.toString().toUpperCase());
            } else {
                commandementPrint.setUserPrintDocument(GeneralConst.EMPTY_STRING);
            }

            commandementPrint.setAgentHuissier(commandement.getIdContrainte().getAgentCommandement());

        } else {
            commandementPrint.setAgentHuissier(GeneralConst.EMPTY_STRING);
            commandementPrint.setUserPrintDocument(GeneralConst.EMPTY_STRING);
            commandementPrint.setDateEmission(GeneralConst.EMPTY_STRING);
            commandementPrint.setDateImpression(GeneralConst.EMPTY_STRING);
        }

        commandementPrint.setCompteBancaireDgrk(commandement.getIdContrainte().getId());
        commandementPrint.setDateEmissionContrainte(
                ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        commandementPrint.setMontantPoursuivi(String.format(GeneralConst.FORMAT_CONVERT,
                commandement.getMontantPoursuivi()).concat(GeneralConst.SPACE).concat(devise));

        commandementPrint.setFraisPoursuite(String.format(GeneralConst.FORMAT_CONVERT,
                commandement.getMontantFraisPoursuite()).concat(GeneralConst.SPACE).concat(devise));

        commandementPrint.setDelaiCommandement("huit jours");
        commandementPrint.setFaitGenerateur("Amendes");
        commandementPrint.setDateExigibilite(GeneralConst.EMPTY_STRING);
        //commandementPrint.setNumeroCommandement(commandement.getId());

        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            commandementPrint.setNumeroCommandement(Tools.generateDocumentNumber(userId, "COMMANDEMENT", commandement.getId()));
        } else {
            commandementPrint.setNumeroCommandement(commandement.getId());
        }

        commandementPrint.setNomReceptionniste(GeneralConst.EMPTY_STRING);
        commandementPrint.setQualiteReceptionniste(GeneralConst.EMPTY_STRING);
        commandementPrint.setDateReception(GeneralConst.EMPTY_STRING);

        return dataXhtm = createDocument(commandementPrint, DocumentConst.DocumentCode.COMMANDEMENT, false);
    }

    public String createCommandementV2(Commandement commandement, String userId, String parquetName, long monthLater) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Med med = new Med();
        Agent agent = GeneralBusiness.getAgentByCode(userId);

        CommandementMock commandementMock = new CommandementMock();

        String devise = GeneralConst.EMPTY_STRING;

        String toDays = Tools.getDateString(new Date(), true);

        if (!toDays.isEmpty()) {
            toDays.concat(" à ").concat(ConvertDate.timeDate(new Date()).concat(" heures"));
        }

        commandementMock.setDateJour(toDays);
        commandementMock.setLieuPrint(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
        //commandementMock.setNumeroCommandement(commandement.getId());

        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            commandementMock.setNumeroCommandement(Tools.generateDocumentNumber(userId, "COMMANDEMENT", commandement.getId()));
        } else {
            commandementMock.setNumeroCommandement(commandement.getId());
        }

        commandementMock.setId(commandement.getId());

        commandementMock.setParquet(parquetName.toUpperCase());

        if (commandement.getIdContrainte() != null) {

            Contrainte contrainte = commandement.getIdContrainte();

            commandementMock.setDateReceptionContrainte(ConvertDate.formatDateToStringOfFormat(contrainte.getDateReception(), "dd/MM/YYYY"));

            commandementMock.setNameHuissier(contrainte.getAgentCommandement().toUpperCase());
            commandementMock.setNomHuissier(contrainte.getAgentCommandement().toUpperCase());
            commandementMock.setNumeroContrainte(contrainte.getNumeroDocument().toUpperCase());

            Amr amr = contrainte.getIdAmr() != null ? contrainte.getIdAmr() : null;

            med = RecouvrementBusiness.getMedById(amr.getFkMed());

            if (amr != null) {

                personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());
                commandementMock.setContribuable(personne.toString().toUpperCase());
                commandementMock.setQualiteContribuable(propertiesConfig.getProperty("VALUE_QUALITE_CONTRIBUABLE"));

                adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                commandementMock.setAdresseContribuable(adresse.toString().toUpperCase());

            }

        }

        commandementMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(commandement.getId());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            commandementMock.setCodeQr(codeQR);
        } else {
            commandementMock.setCodeQr(GeneralConst.EMPTY_STRING);
        }

        commandementMock.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        BigDecimal amountTotal = new BigDecimal(0);
        BigDecimal amountTotalWithFraisPoursuites = new BigDecimal(0);

        amountTotal = amountTotal.add(med.getMontant());
        amountTotal = amountTotal.add(med.getMontantPenalite());

        NotePerception notePerception;

        switch (med.getTypeMed()) {

            case "INVITATION_PAIEMENT":
            case "RELANCE":

                RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");
                devise = retraitDeclaration.getDevise();

                break;

            case "INVITATION_SERVICE":

                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                devise = notePerception.getDevise();

                break;
            default:
                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(med.getNotePerception());
                devise = notePerception.getDevise();

                break;
        }

        BigDecimal fraisPoursuites = new BigDecimal(0);
        BigDecimal _3percent = new BigDecimal("0.03");
        BigDecimal _25percent = new BigDecimal("0.25");
        BigDecimal _2percent = new BigDecimal("0.02");

        fraisPoursuites = fraisPoursuites.add(amountTotal.multiply(_3percent));

        commandementMock.setPrincipal(String.format(GeneralConst.FORMAT_CONVERT,
                amountTotal).concat(GeneralConst.SPACE).concat(devise));

        commandementMock.setFraisPoursuite(String.format(GeneralConst.FORMAT_CONVERT,
                fraisPoursuites).concat(GeneralConst.SPACE).concat(devise));

        BigDecimal penaliteRecouvrement = new BigDecimal(0);

        penaliteRecouvrement = penaliteRecouvrement.add(amountTotal.multiply(_2percent));

        penaliteRecouvrement = penaliteRecouvrement.add(penaliteRecouvrement.multiply(BigDecimal.valueOf(monthLater)));

        commandementMock.setPenaliteRecouvrement(String.format(GeneralConst.FORMAT_CONVERT,
                penaliteRecouvrement).concat(GeneralConst.SPACE).concat(devise));

        BigDecimal penaliteAssiette = new BigDecimal(0);

        penaliteAssiette = penaliteAssiette.add(amountTotal.multiply(_25percent));
        penaliteAssiette = penaliteAssiette.add(penaliteAssiette.multiply(BigDecimal.valueOf(monthLater)));

        commandementMock.setPenaliteAssiette(String.format(GeneralConst.FORMAT_CONVERT,
                penaliteAssiette).concat(GeneralConst.SPACE).concat(devise));

        commandementMock.setFraisPoursuite(String.format(GeneralConst.FORMAT_CONVERT,
                fraisPoursuites).concat(GeneralConst.SPACE).concat(devise));

        BigDecimal amountTotalGen = new BigDecimal(0);

        amountTotalGen = amountTotalGen.add(amountTotal);
        amountTotalGen = amountTotalGen.add(penaliteRecouvrement);
        amountTotalGen = amountTotalGen.add(penaliteAssiette);
        amountTotalGen = amountTotalGen.add(fraisPoursuites);

        commandementMock.setCumulAmount(String.format(GeneralConst.FORMAT_CONVERT,
                amountTotalGen).concat(GeneralConst.SPACE).concat(devise));

        commandementMock.setAmountTotal(String.format(GeneralConst.FORMAT_CONVERT,
                amountTotalGen).concat(GeneralConst.SPACE).concat(devise));

        commandementMock.setMontantChiffre(String.format(GeneralConst.FORMAT_CONVERT,
                amountTotalGen).concat(GeneralConst.SPACE).concat(devise));

        String deviseLibelle = GeneralConst.EMPTY_STRING;

        switch (devise) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLibelle = " franc congolais";
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLibelle = " dollars américain";

                break;
        }

        String letterOne = GeneralConst.EMPTY_STRING;
        String letterTwo = GeneralConst.EMPTY_STRING;
        String chiffreEnLettre = GeneralConst.EMPTY_STRING;

        BigDecimal partInteger = new BigDecimal(BigInteger.ZERO);
        BigDecimal partVirgule = new BigDecimal(BigInteger.ZERO);

        boolean virguleExist;

        String amountFormat = String.format(GeneralConst.FORMAT_CONVERT, amountTotalGen);

        amountFormat = amountFormat.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

        if (amountFormat != null && !amountFormat.isEmpty()) {

            if (amountFormat.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amountFormat.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));

            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);

                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLibelle;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLibelle;
                virguleExist = false;
            }

            commandementMock.setMontantLettre(chiffreEnLettre);

        } else {
            commandementMock.setMontantLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        return dataXhtm = createDocument(commandementMock, DocumentConst.DocumentCode.COMMANDEMENT, false);
    }

    public String createBonAPayer(BonAPayer bonAPayer) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        BapPrint bapPrint = new BapPrint();
        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();

        String deviseLibelle = GeneralConst.EMPTY_STRING;

        String deviseCode = bonAPayer.getFkCompte().getDevise().getCode();

        personne = bonAPayer.getFkPersonne() != null ? bonAPayer.getFkPersonne() : null;

        if (personne != null) {

            bapPrint.setNomAssujetti(personne.toString().toUpperCase());
            bapPrint.setNumImpot(personne.getCode());

            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                bapPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                bapPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                bapPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                bapPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                bapPrint.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                bapPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {

            bapPrint.setNomAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setNumImpot(GeneralConst.EMPTY_STRING);

            bapPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);

        }

        bapPrint.setNumeroBap(bonAPayer.getCode());
        bapPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(bonAPayer.getDateCreat(), "dd/MM/YYYY"));
        bapPrint.setCompteBancaireBap(bonAPayer.getFkCompte().getCode());
        bapPrint.setFaitGenerateur(bonAPayer.getActeGenerateur());

        agent = GeneralBusiness.getAgentByCode(bonAPayer.getAgentCreat());

        if (agent != null) {
            bapPrint.setNomReceveur(agent.toString().toUpperCase());
        } else {
            bapPrint.setNomReceveur(GeneralConst.EMPTY_STRING);
        }

        bapPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(bonAPayer.getCode());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            bapPrint.setCodeQR(codeQR);
        } else {
            bapPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        bapPrint.setMotif(bonAPayer.getMotifPenalite());

        bapPrint.setMontantEnChiffre(String.format(GeneralConst.FORMAT_CONVERT,
                bonAPayer.getMontant()).concat(GeneralConst.SPACE).concat(deviseCode));

        switch (deviseCode) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLibelle = " franc congolais";
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLibelle = " dollars amÃ©ricain";

                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal(BigInteger.ZERO);
        BigDecimal partVirgule = new BigDecimal(BigInteger.ZERO);

        boolean virguleExist;

        String amountFormat = String.format(GeneralConst.FORMAT_CONVERT, bonAPayer.getMontant());

        amountFormat = amountFormat.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

        if (amountFormat != null && !amountFormat.isEmpty()) {

            if (amountFormat.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amountFormat.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));

            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);

                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLibelle;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLibelle;
                virguleExist = false;
            }

            bapPrint.setMontantEnLettre(chiffreEnLettre);

        } else {
            bapPrint.setMontantEnLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        return dataXhtm = createDocument(bapPrint, DocumentConst.DocumentCode.BON_A_PAYER, false);
    }

    public String createAmr(Amr amr, String userId) throws InvocationTargetException {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        AmrPrint amrPrint = new AmrPrint();
        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        DetailFichePriseCharge dfpc = new DetailFichePriseCharge();
        ArticleBudgetaire ab = new ArticleBudgetaire();

        PeriodeDeclaration pd = new PeriodeDeclaration();
        NoteCalcul noteCalcul = new NoteCalcul();

        String deviseCode = GeneralConst.EMPTY_STRING;
        String deviseLibelle = GeneralConst.EMPTY_STRING;
        String motif = GeneralConst.EMPTY_STRING;
        String detailMotif = GeneralConst.EMPTY_STRING;
        String documentReference = GeneralConst.EMPTY_STRING;

        String articleName = GeneralConst.EMPTY_STRING;
        String base = GeneralConst.EMPTY_STRING;

        //amrPrint.setNumeroAmr(amr.getNumero());
        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            amrPrint.setNumeroAmr(Tools.generateDocumentNumber(userId, "AMR", amr.getNumero()));
        } else {
            amrPrint.setNumeroAmr(amr.getNumero());
        }

        String referenceTxt = GeneralConst.EMPTY_STRING;

        amrPrint.setId(amr.getNumero());

        if (amr.getFichePriseCharge() != null) {

            deviseCode = amr.getFichePriseCharge().getDevise();

            if (amr.getFichePriseCharge().getMotif() != null) {

                motif = amr.getFichePriseCharge().getMotif().getIntitule();
                documentReference = amr.getFichePriseCharge().getCode();

            } else {

                switch (amr.getFichePriseCharge().getCasFpc()) {

                    case GeneralConst.Number.ONE:
                        motif = "Retard dÃ©claration";
                        documentReference = amr.getFichePriseCharge().getCode();
                        break;
                    case GeneralConst.Number.TWO:
                        motif = "IntÃ©rÃªt moratoire";
                        documentReference = amr.getFichePriseCharge().getCode();
                        break;
                    case GeneralConst.Number.THREE:
                        motif = "Absence dÃ©claration";
                        documentReference = amr.getFichePriseCharge().getCode();
                        break;
                }
            }

            detailMotif = amr.getFichePriseCharge().getDetailMotif() != null ? amr.getFichePriseCharge().getDetailMotif() : GeneralConst.EMPTY_STRING;
            detailMotif = detailMotif.concat(" (".concat(motif).concat(")"));

            personne = amr.getFichePriseCharge().getPersonne();

            amrPrint.setNomAssujetti(personne.toString().toUpperCase());
            amrPrint.setNumImpot(personne.getCode());

            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                amrPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                amrPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                amrPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                amrPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }

            if (!amr.getFichePriseCharge().getDetailFichePriseChargeList().isEmpty()) {
                dfpc = amr.getFichePriseCharge().getDetailFichePriseChargeList().get(0);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                amrPrint.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                amrPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {
            amrPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            amrPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            amrPrint.setNomAssujetti(GeneralConst.EMPTY_STRING);
            amrPrint.setNumImpot(GeneralConst.EMPTY_STRING);
            amrPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
        }

        //amrPrint.setMotif(motif);
        amrPrint.setMotif(motif);
        amrPrint.setDetailMotif(detailMotif);
        amrPrint.setDocumentReference(documentReference);

        amrPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(amr.getNumero());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            amrPrint.setCodeQR(codeQR);
        } else {
            amrPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        amrPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        amrPrint.setTypeAmr(amr.getTypeAmr());
        amrPrint.setDateEcheance(ConvertDate.formatDateToStringOfFormat(amr.getDateEcheance(), "dd/MM/YYYY"));

        String province = GeneralConst.EMPTY_STRING;

        agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            amrPrint.setUserPrintName(agent.toString().toUpperCase());
            amrPrint.setUserPrintFunction(agent.getFonction().getIntitule());

            province = agent.getSite().getAdresse().getProvince().getIntitule().toUpperCase();

        } else {
            amrPrint.setUserPrintName(GeneralConst.EMPTY_STRING);
            amrPrint.setUserPrintFunction(GeneralConst.EMPTY_STRING);
        }

        amrPrint.setProvinceAmr("VILLE PROVINCE DE ".concat(province));

        switch (deviseCode) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLibelle = " franc congolais";
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLibelle = " dollars amÃ©ricain";
                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal(BigInteger.ZERO);
        BigDecimal partVirgule = new BigDecimal(BigInteger.ZERO);

        boolean virguleExist;

        String amountDisplay = String.format(GeneralConst.FORMAT_CONVERT, amr.getNetAPayer()).concat(GeneralConst.SPACE).concat(deviseCode);

        amrPrint.setAmountToChiffre(amountDisplay);
        amrPrint.setAmountTotal(amountDisplay);

        String amountFormat = String.format(GeneralConst.FORMAT_CONVERT, amr.getNetAPayer());

        amountFormat = amountFormat.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

        if (amountFormat != null && !amountFormat.isEmpty()) {

            if (amountFormat.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amountFormat.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));

            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);

                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLibelle;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLibelle;
                virguleExist = false;
            }

            amrPrint.setAmountToLetter(chiffreEnLettre);

        } else {
            amrPrint.setAmountToLetter(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        String exercice = GeneralConst.EMPTY_STRING;
        String taux = GeneralConst.EMPTY_STRING;
        String droit = GeneralConst.EMPTY_STRING;
        String major = GeneralConst.EMPTY_STRING;
        String penal = GeneralConst.EMPTY_STRING;
        String typeTaux = GeneralConst.EMPTY_STRING;
        String amende = GeneralConst.EMPTY_STRING;
        String degreve = GeneralConst.EMPTY_STRING;

        if (dfpc != null) {

            base = String.format(GeneralConst.FORMAT_CONVERT, dfpc.getBase());
            taux = String.format(GeneralConst.FORMAT_CONVERT, dfpc.getTauxPenalite());
            droit = String.format(GeneralConst.FORMAT_CONVERT, dfpc.getPrincipaldu());
            major = GeneralConst.Number.ZERO;//String.format(GeneralConst.FORMAT_CONVERT, dfpc.getMajoration());
            penal = String.format(GeneralConst.FORMAT_CONVERT, dfpc.getPenalitedu());
            amende = GeneralConst.Number.ZERO;//String.format(GeneralConst.FORMAT_CONVERT, dfpc.getAmende());

            if (dfpc.getDegrevement() != null) {
                degreve = String.format(GeneralConst.FORMAT_CONVERT, dfpc.getDegrevement());
            }

            typeTaux = dfpc.getTypeTaux();

            if (dfpc.getNc() != null && !dfpc.getNc().isEmpty()) {

                NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(dfpc.getNc());

                if (nc != null) {

                    ab = nc.getDetailsNcList().get(0).getArticleBudgetaire();

                    articleName = Tools.getArticleByNC_V2(nc);

                    exercice = Tools.getExerciceFiscal(nc);
                }

            } else if (dfpc.getPeriodeDeclaration() > 0) {

                pd = DeclarationBusiness.getPeriodeDeclarationById(dfpc.getPeriodeDeclaration() + "");

                if (pd != null) {

                    ab = pd.getAssujetissement().getArticleBudgetaire();
                    articleName = ab.getIntitule();
                    exercice = Tools.getPeriodeIntitule(pd.getDebut(), ab.getPeriodicite().getCode());
                }

            }

        }

        amrPrint.setAnnee(exercice);
        amrPrint.setArticleName(articleName);
        amrPrint.setBase(base);
        amrPrint.setDroit(droit);
        amrPrint.setTaux(taux.concat(GeneralConst.SPACE).concat(typeTaux));

        amrPrint.setMajor(major);
        amrPrint.setPenal(penal);
        amrPrint.setAmende(amende);
        amrPrint.setDegrev(degreve);
        amrPrint.setIdTax(ab == null ? GeneralConst.EMPTY_STRING : ab.getCodeOfficiel());

        List<ParamAmr> paramAmrs_cdf = PoursuiteBusiness.getistAccountBankToCdfFromParamAmr();
        List<ParamAmr> paramAmrs_usd = PoursuiteBusiness.getistAccountBankToUsdFromParamAmr();

        String accountCdf_text = GeneralConst.EMPTY_STRING;
        String accountUsd_text = GeneralConst.EMPTY_STRING;
        String accountComposite_text = GeneralConst.EMPTY_STRING;

        String bankCdf_text = GeneralConst.EMPTY_STRING;
        String bankUsd_text = GeneralConst.EMPTY_STRING;
        String bankComposite_text = GeneralConst.EMPTY_STRING;

        List<AccountBankAmr> accountBankAmrList = new ArrayList<>();

        if (!paramAmrs_cdf.isEmpty()) {

            for (ParamAmr paramAmrCdf : paramAmrs_cdf) {

                AccountBankAmr accountBankAmrCdf = new AccountBankAmr();
                CompteBancaire compteBancaireCdf = new CompteBancaire();

                compteBancaireCdf = AcquitLiberatoireBusiness.getCompteBancaireByCode(paramAmrCdf.getCompteDgrkCdf());

                if (compteBancaireCdf != null) {

                    accountBankAmrCdf.setBanqueName(compteBancaireCdf.getBanque().getIntitule());
                    accountBankAmrCdf.setCompteCode(compteBancaireCdf.getCode());
                    accountBankAmrCdf.setDeviseName(compteBancaireCdf.getDevise().getCode());

                    if (amr.getTypeAmr().equals(DocumentConst.DocumentCode.AMR2)) {

                        if (accountCdf_text.isEmpty()) {
                            accountCdf_text = compteBancaireCdf.getCode().concat(" en ").concat(compteBancaireCdf.getDevise().getCode());

                            bankCdf_text = compteBancaireCdf.getBanque().getIntitule();

                        } else {
                            accountCdf_text += accountCdf_text = compteBancaireCdf.getCode().concat(" en ").concat(
                                    compteBancaireCdf.getDevise().getCode()).concat(GeneralConst.SPACE);

                            if (!bankCdf_text.equals(bankCdf_text)) {
                                bankCdf_text += ", ".concat(bankCdf_text = compteBancaireCdf.getBanque().getIntitule());
                            }

                        }
                    }
                }

                accountBankAmrList.add(accountBankAmrCdf);
            }

        }

        if (!paramAmrs_usd.isEmpty()) {

            for (ParamAmr paramAmrUsd : paramAmrs_usd) {

                AccountBankAmr accountBankAmrUsd = new AccountBankAmr();

                CompteBancaire compteBancaireUsd = new CompteBancaire();

                compteBancaireUsd = AcquitLiberatoireBusiness.getCompteBancaireByCode(paramAmrUsd.getCompteDgrkUsd());

                if (compteBancaireUsd != null) {
                    accountBankAmrUsd.setBanqueName(compteBancaireUsd.getBanque().getIntitule());
                    accountBankAmrUsd.setCompteCode(compteBancaireUsd.getCode());
                    accountBankAmrUsd.setDeviseName(compteBancaireUsd.getDevise().getCode());

                    if (amr.getTypeAmr().equals(DocumentConst.DocumentCode.AMR2)) {

                        if (accountUsd_text.isEmpty()) {
                            accountUsd_text = compteBancaireUsd.getCode().concat(" en ").concat(compteBancaireUsd.getDevise().getCode());

                            if (!bankCdf_text.equals(bankUsd_text)) {
                                bankUsd_text = compteBancaireUsd.getBanque().getIntitule();
                            }

                        } else {
                            accountUsd_text += accountUsd_text = compteBancaireUsd.getCode().concat(" en ").concat(
                                    compteBancaireUsd.getDevise().getCode()).concat(GeneralConst.SPACE);

                            bankUsd_text += ", ".concat(bankUsd_text = compteBancaireUsd.getBanque().getIntitule());

                        }
                    }

                }

                accountBankAmrList.add(accountBankAmrUsd);
            }

        }

        switch (amr.getTypeAmr()) {
            case DocumentConst.DocumentCode.AMR1:
                String tableAccount = GeneralConst.EMPTY_STRING;

                if (!accountBankAmrList.isEmpty()) {
                    tableAccount = Tools.getFormatTabAccountBankAmr(accountBankAmrList);
                }

                amrPrint.setAccountBankList(tableAccount);
                break;
            case DocumentConst.DocumentCode.AMR2:
                bankComposite_text = bankCdf_text.concat(GeneralConst.SPACE).concat(bankUsd_text);
                accountComposite_text = accountCdf_text.concat(GeneralConst.SPACE).concat(accountUsd_text).concat(
                        " auprÃ¨s de la ").concat(bankComposite_text);

                amrPrint.setAccountBankInfo(accountComposite_text);

                String amountAmr2 = String.format(GeneralConst.FORMAT_CONVERT, amr.getNetAPayer()).concat(
                        GeneralConst.SPACE).concat(deviseCode);
                amrPrint.setAmountAmr2(amountAmr2);
                break;
        }

        dataXhtm = createDocument(amrPrint, DocumentConst.DocumentCode.AMR, false);

        return dataXhtm;

    }

    public String createMep(String numeroMed, String articleName, String assujettiName,
            String adresseName, String dateEcheance,
            String dateOrdo, String numeroNp, String amountNp, String assujettiCode,
            String codeAgent) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        MepPrint mepPrint = new MepPrint();

        NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(numeroNp);

        if (np != null) {
            articleName = Tools.getArticleByNC_V2(np.getNoteCalcul());
        }

        Adresse adrese = TaxationBusiness.getAdresseDefaultByAssujetti(assujettiCode);

        if (adrese != null) {
            adresseName = adrese.toString().toUpperCase();
        }

        Agent agent = ContentieuxBusiness.getAgentByCode(codeAgent);

        if (agent != null) {

            mepPrint.setUserPrint(agent.toString().toUpperCase());

            if (agent.getFonction() != null) {
                mepPrint.setFonctionUserPrint(agent.getFonction().getIntitule());
            } else {
                mepPrint.setFonctionUserPrint(GeneralConst.EMPTY_STRING);
            }
        } else {
            mepPrint.setUserPrint(GeneralConst.EMPTY_STRING);
            mepPrint.setFonctionUserPrint(GeneralConst.EMPTY_STRING);
        }

        mepPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
        mepPrint.setArticleBudgetaire(articleName);
        mepPrint.setAdresseAssujetti(adresseName);
        mepPrint.setNomAssujetti(assujettiName);

        if (dateEcheance.contains(GeneralConst.DASH_SEPARATOR)) {

            dateEcheance = dateEcheance.replaceAll(GeneralConst.DASH_SEPARATOR,
                    GeneralConst.SEPARATOR_SLASH_NO_SPACE);

        }

        mepPrint.setDateEcheanceNp(dateEcheance);
        mepPrint.setDateOrdonnancement(dateOrdo);
        mepPrint.setNumeroNp(numeroNp);
        mepPrint.setNumeroMed(numeroMed);
        mepPrint.setAmountNp(amountNp);
        mepPrint.setNumImpot(assujettiCode);

        if (codeAgent != null && !codeAgent.equals(GeneralConst.EMPTY_STRING)) {
            mepPrint.setNumeroMed(Tools.generateDocumentNumber(codeAgent, "INVITATION_SERVICE", numeroMed));
        } else {
            mepPrint.setNumeroMed(numeroMed);
        }

        if (assujettiCode != null && !assujettiCode.isEmpty()) {
            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(assujettiCode);

            if (loginWeb != null) {
                mepPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                mepPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                mepPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                mepPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }
        } else {
            mepPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            mepPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
        }

        mepPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        echeanceMep = Tools.getEcheanceDate(new Date(), GeneralConst.Numeric.HEIGHT);

        if (echeanceMep != null) {
            mepPrint.setDateEcheanceMep(ConvertDate.formatDateToStringOfFormat(
                    echeanceMep, "dd/MM/YYYY"));
        } else {
            mepPrint.setDateEcheanceMep(GeneralConst.EMPTY_STRING);
        }

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(numeroMed);
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            mepPrint.setCodeQR(codeQR);
        } else {
            mepPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        dataXhtm = createDocument(mepPrint, DocumentConst.DocumentCode.MEP, false);

        return dataXhtm;
    }

    public String createInvitationService(String numeroMed, String articleName, String assujettiName,
            String adresseName, String dateEcheance,
            String dateOrdo, String numeroNp, String amountNp, String assujettiCode,
            String codeAgent) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        MepPrint mepPrint = new MepPrint();

        NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(numeroNp);

        if (np != null) {
            articleName = Tools.getArticleByNC_V2(np.getNoteCalcul());
        }

        Adresse adrese = TaxationBusiness.getAdresseDefaultByAssujetti(assujettiCode);

        if (adrese != null) {
            adresseName = adrese.toString().toUpperCase();
        }

        Agent agent = ContentieuxBusiness.getAgentByCode(codeAgent);

        if (agent != null) {

            mepPrint.setUserPrint(agent.toString().toUpperCase());

            if (agent.getFonction() != null) {
                mepPrint.setFonctionUserPrint(agent.getFonction().getIntitule());
            } else {
                mepPrint.setFonctionUserPrint(GeneralConst.EMPTY_STRING);
            }
        } else {
            mepPrint.setUserPrint(GeneralConst.EMPTY_STRING);
            mepPrint.setFonctionUserPrint(GeneralConst.EMPTY_STRING);
        }

        mepPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
        mepPrint.setArticleBudgetaire(articleName);
        mepPrint.setAdresseAssujetti(adresseName);
        mepPrint.setNomAssujetti(assujettiName);

        if (dateEcheance.contains(GeneralConst.DASH_SEPARATOR)) {

            dateEcheance = dateEcheance.replaceAll(GeneralConst.DASH_SEPARATOR,
                    GeneralConst.SEPARATOR_SLASH_NO_SPACE);

        }

        mepPrint.setDateEcheanceNp(dateEcheance);
        mepPrint.setDateOrdonnancement(dateOrdo);
        mepPrint.setNumeroNp(numeroNp);
        //mepPrint.setNumeroMed(numeroMed);
        mepPrint.setAmountNp(amountNp);
        mepPrint.setNumImpot(assujettiCode);

        if (codeAgent != null && !codeAgent.equals(GeneralConst.EMPTY_STRING)) {
            mepPrint.setNumeroMed(Tools.generateDocumentNumber(codeAgent, "PAIEMENT", numeroMed));
        } else {
            mepPrint.setNumeroMed(numeroMed);
        }

        if (assujettiCode != null && !assujettiCode.isEmpty()) {
            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(assujettiCode);

            if (loginWeb != null) {
                mepPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                mepPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                mepPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                mepPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }
        } else {
            mepPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            mepPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
        }

        mepPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        echeanceMep = Tools.getEcheanceDate(new Date(), GeneralConst.Numeric.HEIGHT);

        if (echeanceMep != null) {
            mepPrint.setDateEcheanceMep(ConvertDate.formatDateToStringOfFormat(
                    echeanceMep, "dd/MM/YYYY"));
        } else {
            mepPrint.setDateEcheanceMep(GeneralConst.EMPTY_STRING);
        }

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(numeroMed);
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            mepPrint.setCodeQR(codeQR);
        } else {
            mepPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        dataXhtm = createDocument(mepPrint, DocumentConst.DocumentCode.INVITATION_SERVICE, false);

        return dataXhtm;
    }

    public String createMed(Med med, String articleName, String assujettiName,
            String adresseName, String exercice, String echeance) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        MedPrint medPrint = new MedPrint();

        medPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
        medPrint.setEcheance(echeance);
        medPrint.setArticleBudgetaire(articleName);
        medPrint.setNomAssujetti(assujettiName);
        medPrint.setExercice(exercice);
        medPrint.setNumImpot(med.getPersonne().trim());
        medPrint.setAdresseAssujetti(adresseName);

        medPrint.setNumeroMed(Tools.generateDocumentNumber(med.getAgentCreat(), "RELANCE", med.getId()));

        medPrint.setId(med.getId());

        Med medInvitationService = RecouvrementBusiness.getMedById(med.getMedMere());

        medPrint.setNumeroInvitationService(medInvitationService == null ? "" : medInvitationService.getNumeroDocument());

        if (med.getPersonne() != null && !med.getPersonne().isEmpty()) {
            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(med.getPersonne().trim());

            if (loginWeb != null) {
                medPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                medPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                medPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                medPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }
        } else {
            medPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            medPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
        }

        medPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));

        echeanceMep = Tools.getEcheanceDate(new Date(), GeneralConst.Numeric.HEIGHT);

        if (echeanceMep != null) {
            medPrint.setDateLimiteAccordee(ConvertDate.formatDateToStringOfFormat(
                    echeanceMep, "dd/MM/YYYY"));
        } else {
            medPrint.setDateLimiteAccordee(GeneralConst.EMPTY_STRING);
        }

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(med.getId());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            medPrint.setCodeQR(codeQR);
        } else {
            medPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        dataXhtm = createDocument(medPrint, DocumentConst.DocumentCode.RELANCE, false);

        return dataXhtm;
    }

    StreamedContent file = null;

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public String createBonAPayerContentieux(BonAPayer bonAPayer) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        BapPrint bapPrint = new BapPrint();
        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();

        String deviseLibelle = GeneralConst.EMPTY_STRING;

        String deviseCode = bonAPayer.getFkAmr().getFichePriseCharge().getDevise();

        personne = bonAPayer.getFkPersonne() != null ? bonAPayer.getFkPersonne() : null;

        if (personne != null) {

            bapPrint.setNomAssujetti(personne.toString().toUpperCase());
            bapPrint.setNumImpot(personne.getCode());

            LoginWeb loginWeb = RecouvrementBusiness.getLoginWebByAssujetti(personne.getCode());

            if (loginWeb != null) {
                bapPrint.setTelephoneAssujetti(loginWeb.getTelephone());
                bapPrint.setEmailAssujetti(loginWeb.getMail());
            } else {
                bapPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
                bapPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            }

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {
                bapPrint.setAdresseAssujetti(adresse.toString().toUpperCase());
            } else {
                bapPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);
            }

        } else {

            bapPrint.setNomAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setNumImpot(GeneralConst.EMPTY_STRING);

            bapPrint.setTelephoneAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setEmailAssujetti(GeneralConst.EMPTY_STRING);
            bapPrint.setAdresseAssujetti(GeneralConst.EMPTY_STRING);

        }

        bapPrint.setNumeroBap(bonAPayer.getCode());
        bapPrint.setDateImpression(ConvertDate.formatDateToStringOfFormat(bonAPayer.getDateCreat(), "dd/MM/YYYY"));
        bapPrint.setCompteBancaireBap(bonAPayer.getFkCompte().getCode());
        bapPrint.setFaitGenerateur(bonAPayer.getActeGenerateur());

        agent = GeneralBusiness.getAgentByCode(bonAPayer.getAgentCreat());

        if (agent != null) {
            bapPrint.setNomReceveur(agent.toString().toUpperCase());
        } else {
            bapPrint.setNomReceveur(GeneralConst.EMPTY_STRING);
        }

        bapPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
//        bapPrint.setLogo(propertiesConfig.getProperty("LOGO_DGRK_2"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(bonAPayer.getCode());

        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            bapPrint.setCodeQR(codeQR);
        } else {
            bapPrint.setCodeQR(GeneralConst.EMPTY_STRING);
        }

        bapPrint.setMotif(bonAPayer.getMotifPenalite());

        bapPrint.setMontantEnChiffre(String.format(GeneralConst.FORMAT_CONVERT,
                bonAPayer.getMontant()).concat(GeneralConst.SPACE).concat(deviseCode));

        switch (deviseCode) {
            case GeneralConst.Devise.DEVISE_CDF:
                deviseLibelle = propertiesConfig.getProperty("TXT_USD_LETTRE");
                break;
            case GeneralConst.Devise.DEVISE_USD:
                deviseLibelle = propertiesConfig.getProperty("TXT_USD_LETTRE");

                break;
        }

        String letterOne;
        String letterTwo;
        String chiffreEnLettre;

        BigDecimal partInteger = new BigDecimal(BigInteger.ZERO);
        BigDecimal partVirgule = new BigDecimal(BigInteger.ZERO);

        boolean virguleExist;

        String amountFormat = String.format(GeneralConst.FORMAT_CONVERT, bonAPayer.getMontant());

        amountFormat = amountFormat.replace(GeneralConst.DOT, GeneralConst.VIRGULE);

        if (amountFormat != null && !amountFormat.isEmpty()) {

            if (amountFormat.contains(GeneralConst.VIRGULE)) {

                String[] amountSplit = amountFormat.split(GeneralConst.VIRGULE);
                partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));

            }
        }

        if (partInteger.floatValue() > 0) {

            letterOne = FrenchNumberToWords.convert(partInteger);

            if (partVirgule.floatValue() > 0) {

                letterTwo = FrenchNumberToWords.convert(partVirgule);

                chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                        + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLibelle;
                virguleExist = true;
            } else {
                chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLibelle;
                virguleExist = false;
            }

            bapPrint.setMontantEnLettre(chiffreEnLettre);

        } else {
            bapPrint.setMontantEnLettre(GeneralConst.EMPTY_STRING);
            virguleExist = false;
        }

        return dataXhtm = createDocument(bapPrint, DocumentConst.DocumentCode.BON_A_PAYER, false);
    }

    public String createRapportProductionPeage(List<TicketPeage> ticketPeages, String percepteurCode, String percepteurName,
            String controleurCode, String controleurName, String poste, String type, String axe, String observation, String userId) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        RapportProductionCashPeagePrint rapportProductionCashPeagePrint;
        RapportProductionPrepaiementPeagePrint rapportProductionPrepaiementPeagePrint;
        RapportProductionCreditPeagePrint rapportProductionCreditPeagePrint;
        RapportProductionExemptePeagePrint rapportProductionExemptePeagePrint;

        String villePrint = GeneralConst.EMPTY_STRING;

        Agent agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            villePrint = agent.getSite().getAdresse().getVille().getIntitule().toUpperCase();
        }

        switch (type) {
            case "CASH":
                rapportProductionCashPeagePrint = new RapportProductionCashPeagePrint();
                break;
            case "SPONTANE":
                break;
            case "CREDIT":
                rapportProductionCreditPeagePrint = new RapportProductionCreditPeagePrint();
                break;
            case "EXEMPTE":

                rapportProductionExemptePeagePrint = new RapportProductionExemptePeagePrint();

                break;
            case "PRE-PAIEMENT":

                rapportProductionPrepaiementPeagePrint = new RapportProductionPrepaiementPeagePrint();

                rapportProductionPrepaiementPeagePrint.setDatePrint(ConvertDate.formatDateToStringV2(new Date()));
                rapportProductionPrepaiementPeagePrint.setAxepeage(axe.toUpperCase());
                rapportProductionPrepaiementPeagePrint.setPoste(poste.toUpperCase());
                rapportProductionPrepaiementPeagePrint.setControleurCode("");
                rapportProductionPrepaiementPeagePrint.setControleurName(controleurCode + " - " + controleurName.toUpperCase());

                rapportProductionPrepaiementPeagePrint.setPercepteurCode("");
                rapportProductionPrepaiementPeagePrint.setPercepteurName(percepteurName.toUpperCase());

                rapportProductionPrepaiementPeagePrint.setPercepteurPhone("");
                rapportProductionPrepaiementPeagePrint.setControleurPhone("");

                rapportProductionPrepaiementPeagePrint.setObservation(observation);
                rapportProductionPrepaiementPeagePrint.setTotlGenTour(ticketPeages.size() + "");
                rapportProductionPrepaiementPeagePrint.setLieuPrint(villePrint);

                dataXhtm = createDocument(rapportProductionPrepaiementPeagePrint, type, false);

                break;
        }

        return dataXhtm;
    }

    public String createAmrOfInvitationApayer(Amr amr, String userId, Med med, int from, BigDecimal penaliteDu) throws InvocationTargetException {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        AmrMock amrMock = new AmrMock();
        Personne personne = new Personne();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        xfrom = from;

        amrMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(amr.getNumero());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            amrMock.setCodeQr(codeQR);
        } else {
            amrMock.setCodeQr(GeneralConst.EMPTY_STRING);
        }

        //amrMock.setNumeroAmr(amr.getNumero());
        if (userId != null && !userId.equals(GeneralConst.EMPTY_STRING)) {
            amrMock.setNumeroAmr(Tools.generateDocumentNumber(userId, "AMR_OF_INVIT_PAYER", amr.getNumero()));
        } else {
            amrMock.setNumeroAmr(amr.getNumero());
        }

        amrMock.setId(amr.getNumero());
        amrMock.setDateImpression(ConvertDate.formatDateToStringOfFormat(new Date(), "dd/MM/YYYY"));
        amrMock.setDateEcheance(ConvertDate.formatDateToStringOfFormat(amr.getDateEcheance(), "dd/MM/YYYY"));

        personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

        if (personne != null) {

            amrMock.setContribuable(personne.toString().toUpperCase());

            adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

            if (adresse != null) {

                amrMock.setAdresseContribuable(adresse.toString().toUpperCase());
            } else {
                amrMock.setAdresseContribuable(GeneralConst.EMPTY_STRING);
            }
        } else {
            amrMock.setContribuable(GeneralConst.EMPTY_STRING);
        }

        agent = GeneralBusiness.getAgentByCode(userId);

        if (agent != null) {
            amrMock.setNameChefBureau(agent.toString().toUpperCase());

            if (agent.getSite() != null) {
                amrMock.setLieuPrint(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
            } else {
                amrMock.setLieuPrint(GeneralConst.EMPTY_STRING);
            }
        } else {
            amrMock.setNameChefBureau(GeneralConst.EMPTY_STRING);
        }

        BigDecimal amountTotal = new BigDecimal(0);
        BigDecimal amountPrincipal = new BigDecimal(0);
        BigDecimal amountPenalite = new BigDecimal(0);

        amountTotal = amountTotal.add(med.getMontant());

        if (from == 1) {
            amountTotal = amountTotal.add(med.getMontantPenalite());
            amountPenalite = amountPenalite.add(med.getMontantPenalite().divide(BigDecimal.valueOf(2)));
        } else {
            amountTotal = amountTotal.add(penaliteDu);
            amountPenalite = amountPenalite.add(penaliteDu.divide(BigDecimal.valueOf(2)));
        }

        amountPrincipal = amountPrincipal.add(med.getMontant());

        RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(med.getPeriodeDeclaration() + "");

        amrMock.setAmountTotal(String.format(GeneralConst.FORMAT_CONVERT,
                amountTotal).concat(GeneralConst.SPACE).concat(retraitDeclaration.getDevise()));

        amrMock.setAmountPrincipal(String.format(GeneralConst.FORMAT_CONVERT,
                amountPrincipal).concat(GeneralConst.SPACE).concat(retraitDeclaration.getDevise()));

        amrMock.setFiftyPenaliteA(String.format(GeneralConst.FORMAT_CONVERT,
                amountPenalite).concat(GeneralConst.SPACE).concat(retraitDeclaration.getDevise()));

        amrMock.setFiftyPenaliteB(String.format(GeneralConst.FORMAT_CONVERT,
                amountPenalite).concat(GeneralConst.SPACE).concat(retraitDeclaration.getDevise()));

        amrMock.setAccountPrincipal(propertiesConfig.getProperty("AMR_COMPTE_BANCAIRE_PRINCIPAL"));
        amrMock.setAccountPenaliteA(propertiesConfig.getProperty("AMR_COMPTE_BANCAIRE_PENALITE_1"));
        amrMock.setAccountPenaliteB(propertiesConfig.getProperty("AMR_COMPTE_BANCAIRE_PENALITE_2"));

        amrMock.setBankAccountPrincipal(propertiesConfig.getProperty("NAME_BANK_ACCOUNT_MASTER"));
        amrMock.setBankAccountPenaliteA(propertiesConfig.getProperty("NAME_BANK_ACCOUNT_OTHER"));
        amrMock.setBankAccountPenaliteB(propertiesConfig.getProperty("NAME_BANK_ACCOUNT_OTHER"));

        if (from == 1) {
            amrMock.setRetraitDeclID(null);
            amrMock.setDocumentReference("Invitation à payer n° " + med.getNumeroDocument());

            amrMock.setMotif(amr.getObservation());
            amrMock.setDetailMotifs(amr.getMotif());

        } else if (from == 2) {
            amrMock.setRetraitDeclID(amr.getFkRetraitDeclaration());
            amrMock.setDocumentReference("Relance n° " + med.getNumeroDocument());

            amrMock.setMotif(amr.getMotif());
            amrMock.setDetailMotifs(amr.getMotif());
        }

        return dataXhtm = createDocument(amrMock, DocumentConst.DocumentCode.AMR_INVITATION_A_PAYER, false);
    }

    public String createInvitationApayer(Med med, int codeAgent, String numeroTaxation, String dateTaxation,
            Date echeancePaiement, String devise, String nature) {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        InvitationAPayerMock invitationAPayerMock = new InvitationAPayerMock();

        invitationAPayerMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(med.getId());
        String codeQR = generateQRCode(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            invitationAPayerMock.setCodeQr(codeQR);
        } else {
            invitationAPayerMock.setCodeQr(GeneralConst.EMPTY_STRING);
        }

        invitationAPayerMock.setId(med.getId());
        invitationAPayerMock.setDateImpression(ConvertDate.formatDateToStringV2(new Date()));
        //invitationAPayerMock.setNumeroInvitationPaie(med.getId());

        String agentCode = String.valueOf(codeAgent);

        if (agentCode != null && !agentCode.equals(GeneralConst.EMPTY_STRING)) {
            invitationAPayerMock.setNumeroInvitationPaie(Tools.generateDocumentNumber(agentCode, "INVITATION_PAIEMENT", med.getId()));
        } else {
            invitationAPayerMock.setNumeroInvitationPaie(med.getId());
        }

        Personne personne = IdentificationBusiness.getPersonneByCode(med.getPersonne());

        invitationAPayerMock.setContribuable(personne.toString().toUpperCase());
        invitationAPayerMock.setReferenceDocument(numeroTaxation);
        invitationAPayerMock.setDateTransmissionDocument(dateTaxation);

        String echeanceDate = ConvertDate.formatDateToString(echeancePaiement);
        long moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanceDate);

        BigDecimal amountPenalite1 = new BigDecimal(0);
        BigDecimal amountPenalite2 = new BigDecimal(0);
        BigDecimal amountTotal = new BigDecimal(0);

        BigDecimal amountPenaliteTotal = new BigDecimal(0);
        BigDecimal tauxPenalite_2 = new BigDecimal(2);
        //BigDecimal tauxPenalite_2 = new BigDecimal(25);
        BigDecimal diviseur = new BigDecimal(100);

        if (moisRetard == 1 || moisRetard == 0) {

            amountPenalite1 = amountPenalite1.add((med.getMontant().multiply(tauxPenalite_2)).divide(diviseur));
            amountPenalite1 = amountPenalite1.multiply(BigDecimal.valueOf(Double.valueOf("1")));

        } else if (moisRetard > 1) {

            amountPenalite1 = amountPenalite1.add((med.getMontant().multiply(tauxPenalite_2)).divide(diviseur));
            amountPenalite1 = amountPenalite1.multiply(BigDecimal.valueOf(moisRetard));
        }

        amountPenaliteTotal = amountPenaliteTotal.add(amountPenalite1);

        invitationAPayerMock.setAmountPenalite2(amountPenaliteTotal);

        String amount = GeneralConst.EMPTY_STRING;

        amount = String.format(GeneralConst.FORMAT_CONVERT, amountPenaliteTotal).concat(GeneralConst.SPACE).concat(devise);

        invitationAPayerMock.setAmountPenalite(amount);

        amount = String.format(GeneralConst.FORMAT_CONVERT, med.getMontant()).concat(GeneralConst.SPACE).concat(devise);

        invitationAPayerMock.setAmountPrincipal(amount);

        amountTotal = amountTotal.add(med.getMontant());
        amountTotal = amountTotal.add(amountPenaliteTotal);

        amount = String.format(GeneralConst.FORMAT_CONVERT, amountTotal).concat(GeneralConst.SPACE).concat(devise);

        invitationAPayerMock.setAmountTotal(amount);
        invitationAPayerMock.setPrincipal(String.format(GeneralConst.FORMAT_CONVERT, med.getMontant()).concat(GeneralConst.SPACE).concat(devise));

        float _50_PercentPenalite = (amountPenalite1.floatValue() / 2);

        invitationAPayerMock.setPenaliteProvince(String.format(GeneralConst.FORMAT_CONVERT, _50_PercentPenalite).concat(GeneralConst.SPACE).concat(devise));
        invitationAPayerMock.setPenaliteDRHKAT(String.format(GeneralConst.FORMAT_CONVERT, _50_PercentPenalite).concat(GeneralConst.SPACE).concat(devise));

        invitationAPayerMock.setNature(nature);

        Agent agent = GeneralBusiness.getAgentByCode(codeAgent + "");

        invitationAPayerMock.setNameChefBureau(agent.toString().toUpperCase());

        if (agent != null) {

            if (agent.getSite() != null) {
                invitationAPayerMock.setLieuPrint(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
            } else {
                invitationAPayerMock.setLieuPrint(GeneralConst.EMPTY_STRING);
            }

        } else {
            invitationAPayerMock.setLieuPrint(GeneralConst.EMPTY_STRING);
        }

        dataXhtm = createDocument(invitationAPayerMock, DocumentConst.DocumentCode.INVITATION_A_PAYER, false);

        return dataXhtm;
    }

    public String createDernierAvertissement(DernierAvertissement da)
            throws NumberFormatException, InvocationTargetException {

        String dataReturn = GeneralConst.EMPTY_STRING;
        DernierAvertissementPrint dernierAvertissementPrint = new DernierAvertissementPrint();
        try {

            dernierAvertissementPrint.setTransmisCopie(
                    propertiesConfig.getProperty("TXT_TRANSMIS_COPIE_DERNIER_AVERTISSEMENT"));

            dernierAvertissementPrint.setLibelleArticle1(
                    propertiesConfig.getProperty("TXT_LIBELLE_ARTICLE_1_DERNIER_AVERTISSEMENT"));

            dernierAvertissementPrint.setLibelleArticle2(
                    propertiesConfig.getProperty("TXT_LIBELLE_ARTICLE_2_DERNIER_AVERTISSEMENT"));

            dernierAvertissementPrint.setCodeArticle1(
                    propertiesConfig.getProperty("TXT_NUMERO_ARTICLE_1_DERNIER_AVERTISSEMENT"));

            dernierAvertissementPrint.setAssujettiName(da.getFkPersonne().toString().toUpperCase());
            dernierAvertissementPrint.setNumeroDernierAvertissement(da.getId().trim());
            dernierAvertissementPrint.setArticleExtraitRole(da.getFkExtraitRole().getArticleRole().toUpperCase().trim());

            if (da.getFkExtraitRole().getDateReception() != null) {
                String dateConvert = Tools.formatDateToString(da.getFkExtraitRole().getDateReception());
                if (dateConvert != null && !dateConvert.isEmpty()) {
                    dernierAvertissementPrint.setDateExtraitRole(dateConvert);
                } else {
                    dernierAvertissementPrint.setDateExtraitRole(GeneralConst.EMPTY_STRING);
                }
            } else {
                dernierAvertissementPrint.setDateExtraitRole(GeneralConst.EMPTY_STRING);
            }

            if (da.getAgentCreat().getSite() != null) {
                Site site = da.getAgentCreat().getSite();

                dernierAvertissementPrint.setNameSite(site.getIntitule().toUpperCase().trim());
                dernierAvertissementPrint.setLieuSite("");
            } else {
                dernierAvertissementPrint.setLieuSite(GeneralConst.EMPTY_STRING);
                dernierAvertissementPrint.setNameSite(GeneralConst.EMPTY_STRING);
            }

            if (da.getAgentCreat().getService() != null) {

                dernierAvertissementPrint.setNameSector(da.getAgentCreat().getService().getIntitule().toUpperCase());

                if (da.getAgentCreat().getService().getService() != null) {
                    dernierAvertissementPrint.setNameMinistrie(
                            da.getAgentCreat().getService().getService().getIntitule().toUpperCase());
                } else {
                    dernierAvertissementPrint.setNameMinistrie(GeneralConst.EMPTY_STRING);
                }

            } else {
                dernierAvertissementPrint.setNameSector(GeneralConst.EMPTY_STRING);
            }

            List<NotePerception> listNotePerceptions = NotePerceptionBusiness.getListNotePerceptionsByExtraitRole(
                    da.getFkExtraitRole().getId().trim());

            BigDecimal sumAmountExtraitRole = new BigDecimal("0");
            BigDecimal sumAmountPrincipal = new BigDecimal("0");
            BigDecimal sumAmountTotalPenalite = new BigDecimal("0");
            BigDecimal sumAmountPenaliteNP = new BigDecimal("0");
            BigDecimal sumAmountPenaliteBP = new BigDecimal("0");
            BigDecimal sumAmountpTresor = new BigDecimal("0");

            Taux tauxchange = RecouvrementBusiness.getTauxByDevise(GeneralConst.Devise.DEVISE_USD);
            BigDecimal taux = new BigDecimal(GeneralConst.Number.ZERO);

            if (tauxchange != null) {
                taux = tauxchange.getTaux();
            }

            //debut
            NotePerception np;
            String deviseTitre;
            BonAPayer bap;
            List<NotePerception> listNpDependancy = new ArrayList<>();

            ExtraitDeRole extraitDeRole = new ExtraitDeRole();
            extraitDeRole = da.getFkExtraitRole();

            for (DetailsRole detailsRole : extraitDeRole.getDetailsRoleList()) {

                switch (detailsRole.getFkTypeDocument().trim()) {
                    case DocumentConst.DocumentCode.NP:

                        np = NotePerceptionBusiness.getNotePerceptionByCode(
                                detailsRole.getDocumentRef().trim());

                        deviseTitre = np.getDevise() != null ? np.getDevise() : GeneralConst.Devise.DEVISE_CDF;

                        Journal journal = PaiementBusiness.getJournalByDocument(np.getNumero().trim(), false);

                        if (journal == null) {
                            if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {

                                BigDecimal amountConvert = Tools.convertAmountToCDF(np.getNetAPayer(), taux);
                                sumAmountPrincipal = sumAmountPrincipal.add(amountConvert);

                            } else {

                                sumAmountPrincipal = sumAmountPrincipal.add(np.getNetAPayer());
                            }
                        }

                        listNpDependancy = RecouvrementBusiness.getListNotePerceptionDependancy(
                                np.getNumero().trim());

                        for (NotePerception npDependacy : listNpDependancy) {

                            journal = PaiementBusiness.getJournalByDocument(npDependacy.getNumero().trim(), false);

                            if (journal == null) {

                                if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {
                                    BigDecimal amountConvert = Tools.convertAmountToCDF(npDependacy.getNetAPayer(), taux);
                                    sumAmountTotalPenalite = sumAmountTotalPenalite.add(amountConvert);
                                    sumAmountPenaliteNP = sumAmountPenaliteNP.add(amountConvert);

                                } else {
                                    sumAmountTotalPenalite = sumAmountTotalPenalite.add(npDependacy.getNetAPayer());
                                    sumAmountPenaliteNP = sumAmountPenaliteNP.add(npDependacy.getNetAPayer());
                                }
                            }
                        }

//                        bap = RecouvrementBusiness.getBonAPayerByNpMere(np.getNumero().trim());
//
//                        if (bap != null) {
//
//                            journal = PaiementBusiness.getJournalByDocument(bap.getBpManuel().trim(), false);
//
//                            if (journal == null) {
//
//                                if (deviseTitre.equals(GeneralConst.Devise.DEVISE_USD)) {
//                                    BigDecimal amountConvert = Tools.convertAmountToCDF(bap.getMontant(), taux);
//                                    sumAmountTotalPenalite = sumAmountTotalPenalite.add(amountConvert);
//                                    sumAmountPenaliteBP = sumAmountPenaliteBP.add(amountConvert);
//
//                                } else {
//                                    sumAmountTotalPenalite = sumAmountTotalPenalite.add(bap.getMontant());
//                                    sumAmountPenaliteBP = sumAmountPenaliteBP.add(bap.getMontant());
//                                }
//                            }
//                        }
                        break;
                }
                sumAmountExtraitRole = sumAmountPrincipal.add(sumAmountTotalPenalite);
                sumAmountpTresor = sumAmountPrincipal.add(sumAmountPenaliteNP);
            }

            if (sumAmountExtraitRole.floatValue() > 0) {
                dernierAvertissementPrint.setAmountExtraitRole(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        sumAmountExtraitRole).concat(GeneralConst.SPACE).concat(
                                GeneralConst.Devise.DEVISE_CDF));
            } else {
                dernierAvertissementPrint.setAmountExtraitRole(GeneralConst.EMPTY_STRING);
            }

            if (sumAmountPrincipal.floatValue() > 0) {
                dernierAvertissementPrint.setAmountPrincipal(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        sumAmountpTresor).concat(GeneralConst.SPACE).concat(
                                GeneralConst.Devise.DEVISE_CDF));
            } else {
                dernierAvertissementPrint.setAmountPrincipal(GeneralConst.EMPTY_STRING);
            }

            if (sumAmountPenaliteBP.floatValue() > 0) {
                dernierAvertissementPrint.setAmountPenalite(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        sumAmountPenaliteBP));
            } else {
                dernierAvertissementPrint.setAmountPenalite(GeneralConst.EMPTY_STRING);
            }

            dernierAvertissementPrint.setAccountBankPrincipal(
                    propertiesConfig.getProperty("TXT_ACCOUNT_BANK_PRINCIPAL"));

            dernierAvertissementPrint.setAccountBankPenalite(
                    propertiesConfig.getProperty("TXT_ACCOUNT_BANK_PENALITE"));

            dernierAvertissementPrint.setDateImpression(Tools.formatDateToString(new Date()));
            dernierAvertissementPrint.setUserPrint(da.getAgentCreat().toString().toUpperCase().trim());

            if (da.getAgentCreat().getFonction() != null) {
                dernierAvertissementPrint.setFonctionUserPrint(
                        da.getAgentCreat().getFonction().getIntitule().toUpperCase().trim());
            } else {
                dernierAvertissementPrint.setFonctionUserPrint(GeneralConst.EMPTY_STRING);
            }

            dernierAvertissementPrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

            if (propertiesConfig.getProperty("DISPLAY_CODE_QR_TO_DOCUMENT").equals(
                    GeneralConst.Number.ONE)) {

                String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC").concat(
                        da.getId().trim()));

                if (qrCode != null && !qrCode.isEmpty()) {
                    dernierAvertissementPrint.setCodeQr(qrCode);
                } else {
                    dernierAvertissementPrint.setCodeQr(GeneralConst.EMPTY_STRING);
                }
            } else {
                dernierAvertissementPrint.setCodeQr(GeneralConst.EMPTY_STRING);
            }

            dataReturn = createDocument(dernierAvertissementPrint,
                    DocumentConst.DocumentCode.DERNIER_AVERTISSEMENT, false);

        } catch (NumberFormatException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String createExtraitRolePrint(ExtraitDeRole extraitDeRole,
            List<DetailRolePrint> listDetailRolePrint,
            String receveurName) throws NumberFormatException, InvocationTargetException {

        String dataXhtm = GeneralConst.EMPTY_STRING;
        RolePrint rolePrint;
        rolePrint = new RolePrint();
        Role role = new Role();
        Adresse adresse = new Adresse();
        Agent agent = new Agent();
        List<TabDetailRole> tabDetailRoleList = new ArrayList<>();

        try {

            role = RecouvrementBusiness.getRoleById(extraitDeRole.getFkRole().trim());
            if (role != null) {

                if (extraitDeRole != null) {
                    rolePrint.setArticleRole(extraitDeRole.getArticleRole().toUpperCase().trim());
                } else {
                    rolePrint.setArticleRole(GeneralConst.EMPTY_STRING);
                }

                agent = role.getAgentCreat();

            } else {
                rolePrint.setArticleRole(GeneralConst.EMPTY_STRING);
            }

            if (agent != null) {

                //rolePrint.setNameMinistrie(agent.getService().getService().getIntitule().toUpperCase().trim());
                //rolePrint.setNameSector(agent.getService().getIntitule().toUpperCase().trim());
                //rolePrint.setNameSite(agent.getSite().getIntitule().toUpperCase().trim());
                if (agent.getService() != null) {

                    rolePrint.setNameSector(agent.getService().getIntitule().toUpperCase());

                    if (agent.getService().getService() != null) {
                        rolePrint.setNameMinistrie(agent.getService().getService().getIntitule().toUpperCase());
                    } else {
                        rolePrint.setNameMinistrie(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    rolePrint.setNameSector(GeneralConst.EMPTY_STRING);
                }

                if (agent.getSite() != null) {
                    rolePrint.setNameSite(agent.getSite().getIntitule().toUpperCase());
                } else {
                    rolePrint.setNameSite(GeneralConst.EMPTY_STRING);
                }

            } else {
                rolePrint.setNameMinistrie(GeneralConst.EMPTY_STRING);
                rolePrint.setNameSector(GeneralConst.EMPTY_STRING);
                rolePrint.setNameSite(GeneralConst.EMPTY_STRING);
            }

            if (role.getAgentCreat().getSite() != null) {
                Site site = role.getAgentCreat().getSite();

                if (site.getAdresse() != null) {
                    rolePrint.setLieuSite(site.getAdresse().getProvince().getIntitule().trim());
                } else {
                    rolePrint.setLieuSite(GeneralConst.EMPTY_STRING);
                }
            } else {
                rolePrint.setLieuSite(GeneralConst.EMPTY_STRING);
            }

            rolePrint.setNomReceveur(receveurName.trim());
            rolePrint.setAssujettiName(extraitDeRole.getFkPersonne().toString().toUpperCase().trim());
            adresse = IdentificationBusiness.getDefaultAdressByPerson(
                    extraitDeRole.getFkPersonne().getCode().trim());

            if (adresse != null) {
                rolePrint.setAdresse(adresse.toString().toUpperCase().trim());
            } else {
                rolePrint.setAdresse(GeneralConst.EMPTY_STRING);
            }

            rolePrint.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));

            rolePrint.setExtraitRoleId(extraitDeRole.getId().trim());
            rolePrint.setDateImpression(Tools.formatDateToString(new Date()));

            if (propertiesConfig.getProperty("DISPLAY_CODE_QR_TO_DOCUMENT").equals(
                    GeneralConst.Number.ONE)) {

                String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC").concat(
                        extraitDeRole.getId().trim()));

                if (qrCode != null && !qrCode.isEmpty()) {
                    rolePrint.setCodeQr(qrCode);
                } else {
                    rolePrint.setCodeQr(GeneralConst.EMPTY_STRING);
                }

            } else {
                rolePrint.setCodeQr(GeneralConst.EMPTY_STRING);
            }

            BigDecimal sumAmountPrincipal = new BigDecimal("0");
            BigDecimal sumAmountPenalite = new BigDecimal("0");
            BigDecimal sumAmountTotal = new BigDecimal("0");
            BigDecimal sumAmountTotalTresor = new BigDecimal("0");
            BigDecimal sumAmountTotalBap = new BigDecimal("0");
            BigDecimal sumAmountTotalGeneral = new BigDecimal("0");

            for (DetailRolePrint detailRolePrint : listDetailRolePrint) {

                TabDetailRole tabDetailRole = new TabDetailRole();

                BigDecimal amountPrincipal = new BigDecimal("0");
                BigDecimal amountPenalite = new BigDecimal("0");
                BigDecimal amountTotal = new BigDecimal("0");
                BigDecimal amountTotalTresor = new BigDecimal("0");
                BigDecimal amountTotalBap = new BigDecimal("0");

                double diviseur = 2;

                //tabDetailRole.setNp(detailRolePrint.getDocumentReference().trim());
                tabDetailRole.setDate(detailRolePrint.getDateCreate());
                tabDetailRole.setSecteur(detailRolePrint.getService().trim());

                Date echeanceTitre = null;

                switch (detailRolePrint.getTypeDocument().trim()) {
                    case DocumentConst.DocumentCode.NP:

                        NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(
                                detailRolePrint.getDocumentReference().trim());

                        if (np != null && (np.getDateEcheancePaiement() != null
                                && !np.getDateEcheancePaiement().isEmpty())) {

                            echeanceTitre = Tools.formatStringFullToDate(np.getDateEcheancePaiement());

                            tabDetailRole.setNp(np.getNumero().trim());
                        } else {
                            tabDetailRole.setNp(GeneralConst.EMPTY_STRING);
                        }

                        break;
//                    case DocumentConst.DocumentCode.BP:
//                        BonAPayer bp = PaiementBusiness.getBonAPayerByCode(
//                                detailRolePrint.getDocumentReference().trim());
//
//                        if (bp != null && bp.getDateEcheance() != null) {
//                            echeanceTitre = bp.getDateEcheance();
//                            tabDetailRole.setNp(bp.getBpManuel().trim());
//                        } else {
//                            tabDetailRole.setNp(GeneralConst.EMPTY_STRING);
//                        }
//                        break;
                }

                String nbreMois = String.valueOf(ConvertDate.getMonthsBetween(echeanceTitre,
                        role.getDateCreat()));

                if (nbreMois != null && !nbreMois.isEmpty()) {
                    tabDetailRole.setMois_Retard(nbreMois);
                } else {
                    tabDetailRole.setMois_Retard(GeneralConst.EMPTY_STRING);
                }

                ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

                articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(
                        detailRolePrint.getBudgetArticleCode().trim());

                String article = GeneralConst.EMPTY_STRING;
//
//                if (articleBudgetaire != null) {
//
//                    if (articleBudgetaire.getArticleReference() != null
//                            && !articleBudgetaire.getArticleReference().isEmpty()) {
//                        ArticleMere abReference = getArticleMereByMere(articleBudgetaire.getArticleReference());
//                        if (abReference != null) {
//                            article = abReference.getCodeOfficiel().concat(
//                                    GeneralConst.SEPARATOR_SLASH).concat(abReference.getIntitule());
//                        }
//                    } else {
//
//                        article = "<span style=\"font-weight:bold\">".concat(articleBudgetaire.getCodeOfficiel()).concat("</span>").concat("<br/><br/>").concat(
//                                articleBudgetaire.getIntitule());
//                        /*article = "<span style=\"font-weight:bold\">".concat(articleBudgetaire.getCodeOfficiel()).concat("</span>").concat("<br/><br/>").concat(
//                         detailRolePrint.getBudgetArticleLibelle().trim());*/
//                    }
//
//                }

                tabDetailRole.setLibelle(article);

                amountPrincipal = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPrincipal()));
                amountPenalite = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPenalite()));
                amountTotalTresor = (amountPenalite.divide(BigDecimal.valueOf(diviseur))).add(amountPrincipal);
                amountTotalBap = amountPenalite.divide(BigDecimal.valueOf(diviseur));
                amountTotal = amountPrincipal;
                amountTotal = amountTotal.add(amountPenalite);

                sumAmountPrincipal = sumAmountPrincipal.add(amountPrincipal);
                sumAmountPenalite = sumAmountPenalite.add(amountPenalite);
                sumAmountTotal = sumAmountTotal.add(amountTotal);
                sumAmountTotalTresor = sumAmountTotalTresor.add(amountTotalTresor);
                sumAmountTotalBap = sumAmountTotalBap.add(amountTotalBap);
                sumAmountTotalGeneral = sumAmountTotalGeneral.add(amountTotal);

                tabDetailRole.setPrincipal(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPrincipal).concat(GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setPenalite_Assiette("0,00 ".concat(GeneralConst.Devise.DEVISE_CDF));

                tabDetailRole.setPenalite_Recouvrement(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPenalite).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_Penalite(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPenalite).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_Tresor(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotalTresor).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setBon_A_Payer(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotalBap).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_General(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotal).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRoleList.add(tabDetailRole);

            }

            TabDetailRole tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux.setPenalite_Assiette(
                    propertiesConfig.getProperty("TXT_FORMULE_PENALITE_ASSIETTE"));

            tabDetailRoleTotaux.setMois_Retard(
                    propertiesConfig.getProperty("TXT_FORMULE_MOIS_RETARD"));

            tabDetailRoleTotaux.setPrincipal(
                    propertiesConfig.getProperty("TXT_FORMULE_AMOUNT_PRINCIPAL"));

            tabDetailRoleTotaux.setPenalite_Recouvrement(
                    propertiesConfig.getProperty("TXT_FORMULE_PENALITE_RECOUVREMENT"));

            tabDetailRoleTotaux.setTotal_Penalite(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_PENALITE"));

            tabDetailRoleTotaux.setTotal_Tresor(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_TRESOR"));

            tabDetailRoleTotaux.setBon_A_Payer(
                    propertiesConfig.getProperty("TXT_FORMULE_BON_A_PAYER"));

            tabDetailRoleTotaux.setTotal_General(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_GENERAL"));

            tabDetailRoleTotaux.setLibelle(propertiesConfig.getProperty("TXT_FORMULE"));

            tabDetailRoleList.add(tabDetailRoleTotaux);

            tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux.setNp(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setDate(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setSecteur(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setLibelle(propertiesConfig.getProperty("TXT_TOTAUX"));
            tabDetailRoleTotaux.setPenalite_Assiette("0,00 ".concat(GeneralConst.Devise.DEVISE_CDF));
            tabDetailRoleTotaux.setMois_Retard(GeneralConst.DASH_SEPARATOR);

            tabDetailRoleTotaux.setPenalite_Recouvrement(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPenalite).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setPrincipal(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPrincipal).concat(GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_Penalite(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPenalite).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_Tresor(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalTresor).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setBon_A_Payer(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalBap).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_General(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalGeneral).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleList.add(tabDetailRoleTotaux);

            //int[] tab = {4, 6, 7, 8, 9, 10, 11};
            int[] tab = {4, 5, 6, 7, 8, 9, 10, 11};

            String dataDetailRole = Tools.getTableHtmlV(tabDetailRoleList, tab, true, true);

            if (!dataDetailRole.isEmpty()) {
                rolePrint.setDetailRole(dataDetailRole);
            } else {
                rolePrint.setDetailRole(GeneralConst.EMPTY_STRING);
            }

            dataXhtm = createDocument(rolePrint, DocumentConst.DocumentCode.EXTRAIT_ROLE, false);

        } catch (Exception e) {
            dataXhtm = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataXhtm;
    }

    public String createProjectRole(ProjectRolePrint projectRolePrint) {

        String dataXhtm = GeneralConst.EMPTY_STRING;

        try {
            dataXhtm = createDocument(projectRolePrint, DocumentConst.DocumentCode.PROJECT_ROLE, false);
        } catch (Exception e) {
            dataXhtm = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataXhtm;
    }

    public String preparedDetailProjectRole(List<DetailRolePrint> detailRolePrints) {

        String dataDetailRoleXhtm = GeneralConst.EMPTY_STRING;

        try {

            List<TabDetailRole> tabDetailRoleList = new ArrayList<>();

            BigDecimal sumAmountPrincipal = new BigDecimal("0");
            BigDecimal sumAmountPenalite = new BigDecimal("0");
            BigDecimal sumAmountTotal = new BigDecimal("0");
            BigDecimal sumAmountTotalTresor = new BigDecimal("0");
            BigDecimal sumAmountTotalBap = new BigDecimal("0");
            BigDecimal sumAmountTotalGeneral = new BigDecimal("0");

            for (DetailRolePrint detailRolePrint : detailRolePrints) {

                TabDetailRole tabDetailRole = new TabDetailRole();
                NoteCalcul noteCalcul = new NoteCalcul();

                BigDecimal amountPrincipal = new BigDecimal("0");
                BigDecimal amountPenalite = new BigDecimal("0");
                BigDecimal amountTotal = new BigDecimal("0");
                BigDecimal amountTotalTresor = new BigDecimal("0");
                BigDecimal amountTotalBap = new BigDecimal("0");

                double diviseur = 2;

                tabDetailRole.setNp(detailRolePrint.getDocumentReference().trim());
                tabDetailRole.setDate(detailRolePrint.getDateCreate());
                tabDetailRole.setSecteur(detailRolePrint.getService().toUpperCase().trim());

                Date echeanceTitre = null;

                switch (detailRolePrint.getTypeDocument().trim()) {
                    case DocumentConst.DocumentCode.NP:

                        NotePerception np = NotePerceptionBusiness.getNotePerceptionByCode(
                                detailRolePrint.getDocumentReference().trim());

                        if (np != null && (np.getDateEcheancePaiement() != null
                                && !np.getDateEcheancePaiement().isEmpty())) {

                            echeanceTitre = Tools.formatStringFullToDate(np.getDateEcheancePaiement());
                            noteCalcul = np.getNoteCalcul();
                        }

                        break;
                    case DocumentConst.DocumentCode.BP:
                        BonAPayer bp = PaiementBusiness.getBonAPayerByCode(
                                detailRolePrint.getDocumentReference().trim());

                        if (bp != null && bp.getDateEcheance() != null) {
                            echeanceTitre = bp.getDateEcheance();
                            noteCalcul = bp.getFkNoteCalcul();
                        }
                        break;
                }

                Role role = RecouvrementBusiness.getRoleById(detailRolePrint.getIdRole().trim());

                String nbreMois = String.valueOf(ConvertDate.getMonthsBetween(echeanceTitre,
                        role.getDateCreat()));

                if (nbreMois != null && !nbreMois.isEmpty()) {
                    tabDetailRole.setMois_Retard(nbreMois);
                } else {
                    tabDetailRole.setMois_Retard(GeneralConst.EMPTY_STRING);
                }

                //String codeBudgetaire = GeneralConst.EMPTY_STRING;
                String libelleArticle = GeneralConst.EMPTY_STRING;
                //String article = GeneralConst.EMPTY_STRING;

                //codeBudgetaire = Tools.getCodeBudgetaireByNC(noteCalcul);
                //libelleArticle = Tools.getArticleByNC(noteCalcul);
//                for (DetailsNc dnc : noteCalcul.getDetailsNcList()) {
//
//                    if (dnc.getArticleBudgetaire() != null
//                            && !dnc.getArticleBudgetaire().isEmpty()) {
//
//                        ArticleMere abReference = getArticleMereByMere(
//                                dnc.getArticleBudgetaire());
//
//                        if (abReference != null) {
//
//                            libelleArticle = abReference.getCodeOfficiel().concat(
//                                    GeneralConst.SEPARATOR_SLASH).concat(abReference.getIntitule());
//                        }
//
//                    } else {
//                        if (libelleArticle.isEmpty()) {
//
//                            libelleArticle = dnc.getArticleBudgetaire().getCodeOfficiel().concat(
//                                    GeneralConst.SEPARATOR_SLASH).concat(dnc.getArticleBudgetaire().getIntitule());
//                        } else {
//                            libelleArticle += libelleArticle = "<br/>".concat(dnc.getArticleBudgetaire().getCodeOfficiel().concat(
//                                    GeneralConst.SEPARATOR_SLASH).concat(dnc.getArticleBudgetaire().getIntitule()));
//
//                        }
//                    }
//                }

                /*if (!codeBudgetaire.isEmpty() && !libelleArticle.isEmpty()) {

                 article = "<span style=\"font-weight:bold\">".concat(codeBudgetaire.toUpperCase()).concat(
                 "</span>").concat("<br/><br/>").concat(
                 libelleArticle.toUpperCase().trim());
                 }*/
                //tabDetailRole.setLibelle(article);
                tabDetailRole.setLibelle(libelleArticle);

                amountPrincipal = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPrincipal()));
                amountPenalite = BigDecimal.valueOf(Double.valueOf(detailRolePrint.getAmountPenalite()));
                amountTotalTresor = (amountPenalite.divide(BigDecimal.valueOf(diviseur))).add(amountPrincipal);
                amountTotalBap = amountPenalite.divide(BigDecimal.valueOf(diviseur));
                amountTotal = amountPrincipal;
                amountTotal = amountTotal.add(amountPenalite);

                sumAmountPrincipal = sumAmountPrincipal.add(amountPrincipal);
                sumAmountPenalite = sumAmountPenalite.add(amountPenalite);
                sumAmountTotal = sumAmountTotal.add(amountTotal);
                sumAmountTotalTresor = sumAmountTotalTresor.add(amountTotalTresor);
                sumAmountTotalBap = sumAmountTotalBap.add(amountTotalBap);
                sumAmountTotalGeneral = sumAmountTotalGeneral.add(amountTotal);

                tabDetailRole.setPrincipal(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPrincipal).concat(GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setPenalite_Assiette("0,00 ".concat(GeneralConst.Devise.DEVISE_CDF));

                tabDetailRole.setPenalite_Recouvrement(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPenalite).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_Penalite(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"),
                        amountPenalite).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_Tresor(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotalTresor).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setBon_A_Payer(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotalBap).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRole.setTotal_General(formatNombreToString(
                        propertiesConfig.getProperty("NUMBER_FORMAT"), amountTotal).concat(
                                GeneralConst.SPACE).concat(
                                detailRolePrint.getDevise().trim()));

                tabDetailRoleList.add(tabDetailRole);

            }

            TabDetailRole tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux.setPenalite_Assiette(
                    propertiesConfig.getProperty("TXT_FORMULE_PENALITE_ASSIETTE"));

            tabDetailRoleTotaux.setMois_Retard(
                    propertiesConfig.getProperty("TXT_FORMULE_MOIS_RETARD"));

            tabDetailRoleTotaux.setPrincipal(
                    propertiesConfig.getProperty("TXT_FORMULE_AMOUNT_PRINCIPAL"));

            tabDetailRoleTotaux.setPenalite_Recouvrement(
                    propertiesConfig.getProperty("TXT_FORMULE_PENALITE_RECOUVREMENT"));

            tabDetailRoleTotaux.setTotal_Penalite(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_PENALITE"));

            tabDetailRoleTotaux.setTotal_Tresor(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_TRESOR"));

            tabDetailRoleTotaux.setBon_A_Payer(
                    propertiesConfig.getProperty("TXT_FORMULE_BON_A_PAYER"));

            tabDetailRoleTotaux.setTotal_General(
                    propertiesConfig.getProperty("TXT_FORMULE_TOTAL_GENERAL"));

            tabDetailRoleTotaux.setLibelle(propertiesConfig.getProperty("TXT_FORMULE"));

            tabDetailRoleList.add(tabDetailRoleTotaux);

            tabDetailRoleTotaux = new TabDetailRole();

            tabDetailRoleTotaux.setNp(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setDate(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setSecteur(GeneralConst.EMPTY_STRING);
            tabDetailRoleTotaux.setLibelle(propertiesConfig.getProperty("TXT_TOTAUX"));
            tabDetailRoleTotaux.setPenalite_Assiette("0,00 ".concat(GeneralConst.Devise.DEVISE_CDF));
            tabDetailRoleTotaux.setMois_Retard(GeneralConst.DASH_SEPARATOR);

            tabDetailRoleTotaux.setPenalite_Recouvrement(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPenalite).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setPrincipal(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPrincipal).concat(GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_Penalite(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"),
                    sumAmountPenalite).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_Tresor(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalTresor).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setBon_A_Payer(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalBap).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleTotaux.setTotal_General(formatNombreToString(
                    propertiesConfig.getProperty("NUMBER_FORMAT"), sumAmountTotalGeneral).concat(
                            GeneralConst.SPACE).concat(
                            propertiesConfig.getProperty("TXT_DEFAULT_DEVISE")));

            tabDetailRoleList.add(tabDetailRoleTotaux);

            int[] tab = {4, 5, 6, 7, 8, 9, 10, 11};

            String dataDetailRole = Tools.getTableHtmlV(tabDetailRoleList, tab, true, true);

            if (!dataDetailRole.isEmpty()) {
                dataDetailRoleXhtm = dataDetailRole;
            } else {
                dataDetailRoleXhtm = GeneralConst.EMPTY_STRING;
            }

        } catch (Exception e) {
            dataDetailRoleXhtm = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataDetailRoleXhtm;
    }

    public String createVolet_A(NotePerceptionMairie noteMairie) throws NumberFormatException, InvocationTargetException {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Adresse adresse;
        Agent agent;
        DocumentOfficiel documentOfficiel;
        VoletMock voletMock = new VoletMock();
        Personne personne;

        try {
            if (noteMairie != null) {

                String dataXhtm = GeneralConst.EMPTY_STRING;

                String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC")
                        + noteMairie.getId());

                if (qrCode != null && !qrCode.isEmpty()) {
                    voletMock.setCodeQr(qrCode);
                } else {
                    voletMock.setCodeQr(GeneralConst.EMPTY_STRING);
                }

                voletMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
                voletMock.setExerciceFiscal(propertiesConfig.getProperty("EXERCICE_FISCAL"));
                voletMock.setNumeroFiscalUrbain(propertiesConfig.getProperty("NUMERO_FISCAL_URBAIN"));
                voletMock.setId(String.valueOf(noteMairie.getId()));
                voletMock.setReferenceDocument(noteMairie.getNumeroNp());
                voletMock.setNumeroNote(noteMairie.getNumeroNp());

                CompteBancaire cb = GeneralBusiness.getCompteBancaireByCode(noteMairie.getFkCompteBancaire());

                if (cb != null) {
                    voletMock.setCompteBancaire(noteMairie.getFkCompteBancaire() + " " + cb.getIntitule().toUpperCase());
                    voletMock.setBanque(cb.getBanque().getIntitule().toUpperCase());

                } else {
                    voletMock.setCompteBancaire(GeneralConst.EMPTY_STRING);
                    voletMock.setBanque(GeneralConst.EMPTY_STRING);
                }

                ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(noteMairie.getFkAb());

                if (ab != null) {
                    voletMock.setNameTaxe(ab.getIntitule().toUpperCase());
                } else {
                    voletMock.setNameTaxe(GeneralConst.EMPTY_STRING);
                }

                agent = GeneralBusiness.getAgentByCode(String.valueOf(noteMairie.getAgentCreate()));

                if (agent != null) {

                    voletMock.setLieuTaxation(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
//                    voletMock.setNameComptable(agent.toString().toUpperCase() + " (Fonction : " + agent.getFonction().getIntitule().toUpperCase() + ")");
                    voletMock.setNameComptable(agent.toString().toUpperCase());
                    voletMock.setNameChefBureau(agent.toString().toUpperCase());
                } else {
                    voletMock.setLieuTaxation(GeneralConst.EMPTY_STRING);
                    voletMock.setNameComptable(GeneralConst.EMPTY_STRING);
                    voletMock.setNameChefBureau(GeneralConst.EMPTY_STRING);
                }

                String deviseLetter = GeneralConst.EMPTY_STRING;

                switch (noteMairie.getFkDevise().trim().toUpperCase()) {
                    case GeneralConst.Devise.DEVISE_CDF:
                        deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                        break;
                    case GeneralConst.Devise.DEVISE_USD:
                        deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                        break;
                }

                String letterOne;
                String letterTwo;
                String chiffreEnLettre;

                BigDecimal partInteger = new BigDecimal("0");
                BigDecimal partVirgule = new BigDecimal("0");

                boolean virguleExist;

                String amount = String.format("%.2f", noteMairie.getMontant());

                amount = amount.replace(".", ",");

                if (amount != null && !amount.isEmpty()) {

                    if (amount.contains(GeneralConst.VIRGULE)) {

                        String[] amountSplit = amount.split(GeneralConst.VIRGULE);

                        partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                        partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
                    }
                }

                if (partInteger.floatValue() > 0) {

                    letterOne = FrenchNumberToWords.convert(partInteger);

                    if (partVirgule.floatValue() > 0) {

                        letterTwo = FrenchNumberToWords.convert(partVirgule);
                        chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                                + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                        virguleExist = true;
                    } else {
                        chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                        virguleExist = false;
                    }

                    voletMock.setLettre(chiffreEnLettre);

                } else {
                    voletMock.setLettre(GeneralConst.EMPTY_STRING);
                    virguleExist = false;
                }

                if (virguleExist) {
                    voletMock.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            noteMairie.getMontant())
                            + GeneralConst.SPACE + noteMairie.getFkDevise().toUpperCase().trim());
                } else {
                    voletMock.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            noteMairie.getMontant())
                            + GeneralConst.Format.ZERO_FORMAT
                            + GeneralConst.SPACE + noteMairie.getFkDevise().toUpperCase().trim());
                }

                voletMock.setDateImpression(ConvertDate.formatDateHeureToStringV2(new Date()));
                voletMock.setDateCreat(ConvertDate.formatDateHeureToStringV2(noteMairie.getDateCreate()));
                voletMock.setDateEcheance(ConvertDate.formatDateHeureToStringV2(noteMairie.getDateEcheance()));

                personne = IdentificationBusiness.getPersonneByCode(noteMairie.getFkPersonne().trim());

                if (personne != null) {

                    voletMock.setContribuable(personne.toString().toUpperCase());

                    adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                    if (adresse != null) {
                        voletMock.setCommune(adresse.getCommune().getIntitule().toUpperCase());
                        voletMock.setQuartier(adresse.getQuartier().getIntitule().toUpperCase());
                        voletMock.setAvenue(adresse.getAvenue().getIntitule().toUpperCase());
                    } else {
                        voletMock.setCommune(GeneralConst.EMPTY_STRING);
                        voletMock.setQuartier(GeneralConst.EMPTY_STRING);
                        voletMock.setAvenue(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    voletMock.setNameProprietaire(GeneralConst.EMPTY_STRING);
                    voletMock.setCommune(GeneralConst.EMPTY_STRING);
                    voletMock.setQuartier(GeneralConst.EMPTY_STRING);
                    voletMock.setAvenue(GeneralConst.EMPTY_STRING);
                }

                Bien bien = AssujettissementBusiness.getBienByCode(noteMairie.getFkBien());

                if (bien != null) {

                    voletMock.setNameUnite(bien.getIntitule());

                    List<BienActivites> nameActiviteList = new ArrayList<>();
                    String nameActivite = GeneralConst.EMPTY_STRING;
                    nameActiviteList = AssujettissementBusiness.getBienActiviteByBien(bien.getId());
                    if (nameActiviteList.size() > 0) {
                        int i = 1;
                        for (BienActivites bienActivite : nameActiviteList) {
                            if (nameActiviteList.size() == 1) {
                                nameActivite += bienActivite.getFkActivite().getIntitule();
                            } else {
                                if (i == 1) {
                                    nameActivite += bienActivite.getFkActivite().getIntitule();
                                    i++;
                                } else {
                                    nameActivite += nameActivite = ", ".concat(bienActivite.getFkActivite().getIntitule());
                                }
                            }
                        }
                        voletMock.setNameActivite(nameActivite);
                    } else {
                        voletMock.setNameActivite(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    voletMock.setNameUnite(GeneralConst.EMPTY_STRING);
                    voletMock.setNameActivite(GeneralConst.EMPTY_STRING);
                }

                dataXhtm = createDocument(voletMock, DocumentConst.DocumentCode.VOLET_A, false);

                return dataXhtm;
//                }

            }
        } catch (NumberFormatException e) {
            throw e;
        }

        return dataReturn;

    }

    public String createVolet_B(NotePerceptionMairie noteMairie) throws NumberFormatException, InvocationTargetException {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Adresse adresse;
        Agent agent;
        DocumentOfficiel documentOfficiel;
        VoletMock voletMock = new VoletMock();
        Personne personne;

        try {
            if (noteMairie != null) {

                String dataXhtm = GeneralConst.EMPTY_STRING;

                String qrCode = generateQRCode(propertiesConfig.getProperty("URL_TRACKING_DOC")
                        + noteMairie.getId());

                if (qrCode != null && !qrCode.isEmpty()) {
                    voletMock.setCodeQr(qrCode);
                } else {
                    voletMock.setCodeQr(GeneralConst.EMPTY_STRING);
                }

                voletMock.setLogo(propertiesConfig.getProperty("LOGO_DRHKAT"));
                voletMock.setExerciceFiscal(propertiesConfig.getProperty("EXERCICE_FISCAL"));
                voletMock.setNumeroFiscalUrbain(propertiesConfig.getProperty("NUMERO_FISCAL_URBAIN"));
                voletMock.setId(String.valueOf(noteMairie.getId()));
                voletMock.setReferenceDocument(noteMairie.getNumeroNp());
                voletMock.setNumeroNote(noteMairie.getNumeroNp());

                CompteBancaire cb = GeneralBusiness.getCompteBancaireByCode(noteMairie.getFkCompteBancaire());

                if (cb != null) {
                    voletMock.setCompteBancaire(noteMairie.getFkCompteBancaire() + " " + cb.getIntitule().toUpperCase());
                    voletMock.setBanque(cb.getBanque().getIntitule().toUpperCase());

                } else {
                    voletMock.setCompteBancaire(GeneralConst.EMPTY_STRING);
                    voletMock.setBanque(GeneralConst.EMPTY_STRING);
                }

                ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(noteMairie.getFkAb());

                if (ab != null) {
                    voletMock.setNameTaxe(ab.getIntitule().toUpperCase());
                } else {
                    voletMock.setNameTaxe(GeneralConst.EMPTY_STRING);
                }

                agent = GeneralBusiness.getAgentByCode(String.valueOf(noteMairie.getAgentCreate()));

                if (agent != null) {

                    voletMock.setLieuTaxation(agent.getSite().getAdresse().getVille().getIntitule().toUpperCase());
//                    voletMock.setNameComptable(agent.toString().toUpperCase() + " (Fonction : " + agent.getFonction().getIntitule().toUpperCase() + ")");
                    voletMock.setNameComptable(agent.toString().toUpperCase());
                    voletMock.setNameChefBureau(agent.toString().toUpperCase());
                } else {
                    voletMock.setLieuTaxation(GeneralConst.EMPTY_STRING);
                    voletMock.setNameComptable(GeneralConst.EMPTY_STRING);
                    voletMock.setNameChefBureau(GeneralConst.EMPTY_STRING);
                }

                String deviseLetter = GeneralConst.EMPTY_STRING;

                switch (noteMairie.getFkDevise().trim().toUpperCase()) {
                    case GeneralConst.Devise.DEVISE_CDF:
                        deviseLetter = propertiesConfig.getProperty("TXT_CDF_LETTRE");
                        break;
                    case GeneralConst.Devise.DEVISE_USD:
                        deviseLetter = propertiesConfig.getProperty("TXT_USD_LETTRE");
                        break;
                }

                String letterOne;
                String letterTwo;
                String chiffreEnLettre;

                BigDecimal partInteger = new BigDecimal("0");
                BigDecimal partVirgule = new BigDecimal("0");

                boolean virguleExist;

                String amount = String.format("%.2f", noteMairie.getMontant());

                amount = amount.replace(".", ",");

                if (amount != null && !amount.isEmpty()) {

                    if (amount.contains(GeneralConst.VIRGULE)) {

                        String[] amountSplit = amount.split(GeneralConst.VIRGULE);

                        partInteger = BigDecimal.valueOf(Double.valueOf(amountSplit[0]));
                        partVirgule = BigDecimal.valueOf(Double.valueOf(amountSplit[1]));
                    }
                }

                if (partInteger.floatValue() > 0) {

                    letterOne = FrenchNumberToWords.convert(partInteger);

                    if (partVirgule.floatValue() > 0) {

                        letterTwo = FrenchNumberToWords.convert(partVirgule);
                        chiffreEnLettre = letterOne + GeneralConst.SPACE + propertiesConfig.getProperty("TXT_VIRGULE")
                                + GeneralConst.SPACE + letterTwo + GeneralConst.SPACE + deviseLetter;
                        virguleExist = true;
                    } else {
                        chiffreEnLettre = letterOne + GeneralConst.SPACE + deviseLetter;
                        virguleExist = false;
                    }

                    voletMock.setLettre(chiffreEnLettre);

                } else {
                    voletMock.setLettre(GeneralConst.EMPTY_STRING);
                    virguleExist = false;
                }

                if (virguleExist) {
                    voletMock.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            noteMairie.getMontant())
                            + GeneralConst.SPACE + noteMairie.getFkDevise().toUpperCase().trim());
                } else {
                    voletMock.setChiffre(formatNombreToString(propertiesConfig.getProperty("NUMBER_FORMAT"),
                            noteMairie.getMontant())
                            + GeneralConst.Format.ZERO_FORMAT
                            + GeneralConst.SPACE + noteMairie.getFkDevise().toUpperCase().trim());
                }

                voletMock.setDateImpression(ConvertDate.formatDateHeureToStringV2(new Date()));
                voletMock.setDateCreat(ConvertDate.formatDateHeureToStringV2(noteMairie.getDateCreate()));
                voletMock.setDateEcheance(ConvertDate.formatDateHeureToStringV2(noteMairie.getDateEcheance()));

                personne = IdentificationBusiness.getPersonneByCode(noteMairie.getFkPersonne().trim());

                if (personne != null) {

                    voletMock.setContribuable(personne.toString().toUpperCase());

                    adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                    if (adresse != null) {
                        voletMock.setCommune(adresse.getCommune().getIntitule().toUpperCase());
                        voletMock.setQuartier(adresse.getQuartier().getIntitule().toUpperCase());
                        voletMock.setAvenue(adresse.getAvenue().getIntitule().toUpperCase());
                    } else {
                        voletMock.setCommune(GeneralConst.EMPTY_STRING);
                        voletMock.setQuartier(GeneralConst.EMPTY_STRING);
                        voletMock.setAvenue(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    voletMock.setNameProprietaire(GeneralConst.EMPTY_STRING);
                    voletMock.setCommune(GeneralConst.EMPTY_STRING);
                    voletMock.setQuartier(GeneralConst.EMPTY_STRING);
                    voletMock.setAvenue(GeneralConst.EMPTY_STRING);
                }

                Bien bien = AssujettissementBusiness.getBienByCode(noteMairie.getFkBien());

                if (bien != null) {

                    voletMock.setNameUnite(bien.getIntitule());

                    List<BienActivites> nameActiviteList = new ArrayList<>();
                    String nameActivite = GeneralConst.EMPTY_STRING;
                    nameActiviteList = AssujettissementBusiness.getBienActiviteByBien(bien.getId());
                    if (nameActiviteList.size() > 0) {
                        int h = 1;
                        for (BienActivites bienActivite : nameActiviteList) {
                            if (nameActiviteList.size() == 1) {
                                nameActivite += bienActivite.getFkActivite().getIntitule();
                            } else {
                                if (h == 1){
                                    nameActivite += bienActivite.getFkActivite().getIntitule();
                                    h++;
                                }else{
                                     nameActivite += nameActivite = ", ".concat(bienActivite.getFkActivite().getIntitule());
                                }                              
                            }
                        }
                        voletMock.setNameActivite(nameActivite);
                    } else {
                        voletMock.setNameActivite(GeneralConst.EMPTY_STRING);
                    }

                } else {
                    voletMock.setNameUnite(GeneralConst.EMPTY_STRING);
                    voletMock.setNameActivite(GeneralConst.EMPTY_STRING);
                }

                dataXhtm = createDocument(voletMock, DocumentConst.DocumentCode.VOLET_B, false);

                return dataXhtm;
//                }

            }
        } catch (NumberFormatException e) {
            throw e;
        }

        return dataReturn;

    }
}
