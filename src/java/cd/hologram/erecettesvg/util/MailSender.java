/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author moussa.toure
 */
public class MailSender {

    private String sender;
    private String reciever;
    private String host;
    private String subject;
    private String message;
    private String password;
    private String cc;
    private Date date;
    private String name;

    public MailSender() {

    }

    public MailSender(String host, String sender, String password, String reciever, String subject, String message) {

        this.sender = sender;
        this.reciever = reciever;
        this.host = host;
        this.subject = subject;
        this.message = message;
        this.password = password;
    }

    public MailSender(String host, String sender, String reciever, String subject, String message) {

        this.sender = sender;
        this.reciever = reciever;
        this.host = host;
        this.subject = subject;
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sendMail() throws AddressException, MessagingException {

        try {

            Properties properties = System.getProperties();

            properties.setProperty("mail.smtp.host", this.host);

            Session session = Session.getDefaultInstance(properties);

            MimeMessage mimeMessage = new MimeMessage(session);

            mimeMessage.setFrom(new InternetAddress(this.sender));

            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.reciever));

            mimeMessage.setSentDate(date);

            mimeMessage.setSubject(this.subject, "UTF-8");

            mimeMessage.setText(this.message, "UTF-8");

            Transport transport = session.getTransport("smtp");

            transport.connect(this.host, this.sender, this.password);

            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

            transport.close();

        } catch (Exception e) {

            throw e;

        }

    }

    public String sendMailWithAPI(String username, String passwword) {

        try {

            SSLContext sc = SSLContext.getInstance(GeneralConst.Mail.CRYPTO_TLSv1);

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            String usernameMailjet = username;
            String passwordMailjet = passwword;

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(usernameMailjet, passwordMailjet);

            client.register(feature);

            WebTarget webTarget = client.target(this.host);

            String body = String.format(GeneralConst.Mail.MAIL_PARAM_JSON,
                    this.sender,
                    this.reciever,
                    this.reciever,
                    this.subject,
                    this.subject,
                    this.message);

            String response = webTarget.request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(body), String.class);

            JSONObject jSONObject = new JSONObject(response);

            JSONArray array = jSONObject.getJSONArray(GeneralConst.Mail.MESSAGE);

            String status = array.getJSONObject(GeneralConst.Numeric.ZERO)
                    .getString(GeneralConst.Mail.STATUS);

            if (status.equals(GeneralConst.Mail.SUCCESS)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NoSuchAlgorithmException | KeyManagementException | JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

}
