/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class IdentificationConst {

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String TYPE_SEARCH = "typeSearch";
        public final static String LIBELLE = "libelle";

        public final static String CODE_FORME_JURIDIQUE = "codeFormeJuridique";
        public final static String LIBELLE_FORME_JURIDIQUE = "libelleFormeJuridique";

        public final static String NIF = "nif";
        public final static String NOM = "nom";
        public final static String NOM_COMPLET = "nomComplet";
        public final static String POST_NOM = "postNom";
        public final static String PRENOM = "prenom";
        public final static String TELEPHONE = "telephone";
        public final static String TELE_DECLARATION_IS_EXISTE = "teleDelarationIsExiste";
        public final static String EMAIL = "email";
        public final static String CODE_PERSONNE = "codePersonne";
        public final static String ID_COMPLEMENT = "id";
        public final static String CODE_ADRESSE_PERSONNE = "codeAP";
        public final static String CHAINE_ADRESSE = "chaine";
        public final static String ADRESSES = "adresses";
        public final static String COMPLEMENTS = "complements";
        public final static String MATRICULE = "matricule";
        public final static String GRADE = "grade";
        public final static String SELECT_STATE_DECLARATION = "selectStateDeclaration";
        public final static String CODE_USER = "codeuser";

        public final static String CODE_COMPLEMENT_FORME = "code";
        public final static String VALEUR_COMPLEMENT_FORME = "valeur";
        public final static String CODE_ENTITE_ADMINISTRATIVE = "code";

        public final static String CODE_TYPE_COMPLEMENT = "codeTypeComplement";
        public final static String LIBELLE_TYPE_COMPLEMENT = "libelleTypeComplement";
        public final static String OBJET_INTERACTION = "objetInteraction";
        public final static String VALEUR_PREDEFINIE = "valeurPredefinie";
        public final static String INPUT_VALUE = "inputValue";

        public final static String ID_ADRESSE = "idAdresse";
        public final static String NUMERO = "numero";
        public final static String DEFAUT = "defaut";
        public final static String CODE_AVENUE = "codeAvenue";
        public final static String AVENUE = "avenue";
        public final static String CODE_QUARTIER = "codeQuartier";
        public final static String QUARTIER = "quartier";
        public final static String CODE_COMMUNE = "codeCommune";
        public final static String COMMUNE = "commune";
        public final static String CODE_DISTRICT = "codeDistrict";
        public final static String DISTRICT = "district";
        public final static String CODE_VILLE = "codeVille";
        public final static String VILLE = "ville";
        public final static String CODE_PROVINCE = "codeProvince";
        public final static String PROVINCE = "province";
        public final static String LIBELLE_PROVINCE = "libelleProvince";

        public final static String LIBELLE_DISTRINCT = "libelleDistrinct";

        public final static String ETAT = "etat";

        public final static String CODE_FONCTION = "codeFonction";
        public final static String LIBELLE_FONCTION = "libelleFonction";
        public final static String ETAT_FONCTION = "etat";
        public final static String FONCTION = "fonction";
        public final static String SELECT_FONCTION = "selectFonctionVal";
        public final static String TYPE_RECHERCHE_USER = "typeRechercheUser";

        public final static String CODE_SITE = "codeSite";
        public final static String LIBELLE_SITE = "libelleSite";
        public final static String SITE = "site";

        public final static String CODE_SERVICE = "codeService";
        public final static String LIBELLE_SERVICE = "libelleService";
        public final static String SERVICE = "service";
        public final static String DIVISION = "division";
        
        public final static String CODE_DIVISION = "codeDivision";
        public final static String LIBELLE_DIVISION= "libelleDivision";

        // Groupe Param
        public final static String ID_GROUPE = "code_groupe";
        public final static String INTITULE_GROUPE = "intitule";
        public final static String ETAT_GROUPE = "etat";
        public final static String DESCRIPTION_GROUPE = "description";

        // Droit Param
        public final static String ID_DROIT = "code_droit";
        public final static String INTITULE_DROIT = "intitule_droit";
        public final static String TABLEAU_DROIT = "droits";
        public final static String ETAT_DROIT = "etat";
        public final static String FK_MODULE = "fkModule";       

        // Agent Param
        public final static String ID_AGENT = "code_agent";
        public final static String NOM_AGENT = "nom_agent";
        public final static String PRENOM_AGENT = "prenom_agent";
        public final static String AGENT_CREAT = "agent_creat";
        public final static String CODE_UA = "codeUa";
        public final static String LIBELLE_UA = "libelleUa";
        public final static String UA = "ua";

        public final static String LOGIN = "login";
        public final static String COMPTE_EXPIRE = "compteExiper";
        public final static String DATE_EXPIRATION = "dateExipiration";

        public final static String ASSUJETTI_LIST = "assujettiList";
        public final static String DROIT_LIST = "droitList";

        public final static String CODE_AGENT_EDITION = "codeAgentEdition";
        
         // Module Param
        public final static String CODE_MODULE = "codeModule";
        public final static String INTITULE_MODULE = "intituleModule";
        
        // Banque Param
        public final static String CODE_BANQUE = "codeBanque";
        public final static String CODE_SUIFT_BANQUE = "codeSuiftBanque";
        public final static String INTITULE_BANQUE = "intituleBanque";
        public final static String SIGLE_BANQUE = "sigleBanque";
        
        // Compte Bancaire Param
        public final static String CODE_COMPTE_BANQUE = "codeCompteBancaire";
        public final static String INTITULE_COMPTE_BANQUE = "intituleCompteBancaire";
        public final static String DEVISE_COMPTE_BANQUE = "deviseCompteBancaire";
        
        //Personne
        public final static String USER_NAME = "userName";

    }

    public class Operation {

        public final static String LOAD_FORME_JURIDIQUE = "loadFormeJuridique";
        public final static String LOAD_ADRESSES = "loadAdresses";
        public final static String LOAD_COMPLEMENT_FORME = "loadComplementForme";
        public final static String SAVE_ASSUJETTI = "saveAssujetti";
        public final static String LOAD_ASSUJETTIS = "loadAssujettis";
        public final static String LOAD_ASSUJETTIS_VALIDER = "loadAssujettisValider";
        public final static String LOAD_INFO_ASSUJETTI = "loadInfoAssujetti";
        public final static String DISABLED_ASSUJETTI = "disabledAssujetti";
        public final static String LOAD_ASSUJETTI_AVANCEE = "loadAssujettiAvancee";
        public final static String LOAD_ADRESSES_PERSONNE = "loadAdressesPersonne";
        public final static String SAVE_CARTE = "saveCarte";
        public final static String VALIDER_ASSUJETTIS = "validateAssujetti";
        public final static String ASSUJETTIS_FROM = "assujtettisFrom";
    }

    public class ObjetInteraction {

        public final static String EMPTY = "";
        public final static String DATE = "DATE";
        public final static String MONTANT = "MONTANT";
        public final static String NOMBRE = "NOMBRE";
        public final static String SEXE = "SEXE";
    }

    public class ParamControl {

        public final static String INPUT_CONTROL = "<div class=\"form-group\">\n"
                + " <p class=\"control-label col-sm-4\" for=\"%s\">%s %s</p>\n"
                + " <div class=\"col-sm-8\">\n"
                + " <input type=\"%s\" class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\" value=\"%s\"/>\n"
                + " </div>\n"
                + " </div>";

        public final static String COMPLEMENT_LIST = "<div class=\"form-group\">\n"
                + " <p class=\"control-label col-sm-4\" for=\"%s\">%s %s</p>\n"
                + " <div class=\"col-sm-8\">\n"
                + " <select class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\">%s</select>\n"
                + " </div>\n"
                + " </div>";

        public final static String ITEM_OPTION = "<option value =\"%s\" %s>%s</option>";

        public final static String SELECTED_OPTION_ATTR = "selected";

        public final static String DATA_TABLE_ROW = "<tr>\n"
                + " <td>&nbsp;%s</td>\n"
                + " <td>&nbsp;%s</td>\n"
                + " </tr>";

        public final static String SPAN_REQUIRED_VALUE = "<span style=\"color:red\"> *</span>";
    }

    public static class TypeData {

        public final static String TYPE_TEXT = "text";
        public final static String TYPE_NUMBER = "number";
        public final static String TYPE_DATE = "date";
    }

    public static class ObjetSexeData {

        public final static String CODE_SEXE_MASCULIN = "option@M";
        public final static String LIBELLE_SEXE_MASCULIN = "Masculin";
        public final static String CODE_SEXE_FEMININ = "option@F";
        public final static String LIBELLE_SEXE_FEMININ = "Féminin";
    }

    public class ParamQUERY {

        public final static String CODE = "CODE";
        public final static String PERSONNE = "PERSONNE";
        public final static String NOM = "NOM";
        public final static String POSTNOM = "POSTNOM";
        public final static String PRENOM = "PRENOM";
        public final static String PRENOMS = "PRENOMS";
        public final static String FORME = "FORME";
        public final static String AGENT_CREATE = "AGENT_CREATE";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String AGENT_MAJ = "AGENT_MAJ";
        public final static String NIF = "NIF";

        public final static String FONCTION = "FONCTION";
        public final static String SERVICE = "SERVICE";
        public final static String UA = "UA";
        public final static String LOGIN = "LOGIN";
        public final static String MPD = "MDP";
        public final static String MATRICULE = "MATRICULE";
        public final static String ETAT = "ETAT";
        public final static String GRADE = "GRADE";
        public final static String COMPTE_EXPIRE = "COMPTE_EXPIRE";
        public final static String DATE_EXPIRATION = "DATE_EXPIRATION";
        public final static String DISTRICT = "DISTRICT";
        public final static String SITE = "SITE";
        public final static String CHANGE_PWD = "CHANGE_PWD";
        public final static String UTILISATEUR = "RET";
    }

}
