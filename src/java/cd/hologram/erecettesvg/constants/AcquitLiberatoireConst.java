/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class AcquitLiberatoireConst {

    public class Operation {

        public final static String LOAD_NOTES_CALCUL = "loadNotesCalcul";
        public final static String PRINT_ACQUIT_LIBERATOIRE = "printAcquitLiberatoire";
        public final static String LOAD_ACQUITS_LIBERATOIRES = "loadAcquitsLiberatoires";
        public final static String RE_PRINT_ACQUIT_LIBERATOIRE = "rePrintAcquitLiberatoire";
        
        public final static String CHECK_FICHE_CPI = "checkFicheCPI";
        public final static String SEARCH_CPI = "searchCPI";
    }

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String ADVANCED = "advanced";
        public final static String TYPE_SEARCH = "typeSearch";
        public final static String LIBELLE = "libelle";
        public final static String ALL_SITE = "allSite";
        public final static String ALL_SERVICE = "allService";
        public final static String ID_USER = "idUser";
        public final static String CODE_SERVICE = "codeService";
        public final static String CODE_SITE = "codeSite";
        public final static String NAME_USER = "nameUser";
        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";
        
        public final static String CODE_CPI = "codeCPI";
        
        public final static String PERSONNE = "personne";
        public final static String CPI = "cpi";

        public final static String SERVICE_ASSIETTE = "serviceAssiette";
        public final static String DATE = "date";
        public final static String ARTICLE_BUDGETAIRE = "articleBudgetaire";
        public final static String PERIODE = "periode";
        public final static String NOTE_PERCEPTION = "notePerception";
        public final static String REQUERANT = "requerant";
        public final static String FORME_JURIDIQUE = "formeJuridique";
        public final static String MONTANT_DU = "montantDu";
        public final static String PAYE = "paye";
        public final static String MONTANT_DU_PENALITE = "PenaliteMontantDu";
        public final static String MONTANT_PAYE_PENALITE = "Penalitepaye";
        public final static String RESTE_A_PAYER = "resteAPayer";
        public final static String DEFAULT_ADRESS = "defaultAdress";
        public final static String RECEPTIONNISTE = "receptionniste";
        public final static String NOTE_CALCUL = "noteCalcul";
        public final static String NUMERO_PAPIER = "numeroPapier";
        public final static String NUMERO_TIMBRE = "numeroTimbre";
        public final static String REFERENCE = "reference";
        public final static String IS_PRINT = "isPrint";
        public final static String DEVISE = "devise";
        public final static String PAYLATE = "payLate";
    }

    public class ResultCode {

        public final static String SUCCES_OPERATION = "1";
        public final static String FAILED_OPERATION = "0";
        public final static String EXCEPTION_OPERATION = "-1";
        public final static String PAIEMENT_PARTIEL = "2";
        public final static String PAIEMENT_NON_APURE = "3";
        public final static String NON_APURE_COMPTABLE = "4";
        public final static String NOT_VALIDATE_FPC = "5";
    }

}
