/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class LoginConst {

    public class ParamName {

        public final static String LOGIN_USER = "login";
        public final static String PASSWORD = "password";
        public final static String SESSION = "session";
        public final static String EXPIRED = "expire";
        public final static String LIBELLE_SITE = "site";
        public final static String CHANGE_PWD = "changePassword";
        public final static String LIBELLE_SERVICE = "serviceAssiette";
        public final static String MATRICULE = "matricule";
        public final static String NOM_COMPLET = "nomComplet";
        public final static String LIBELLE_GRADE = "grade";
        public final static String LIBELLE_FONCTION = "fonction";
        public final static String CODE_FONCTION = "codeFonction";
        public final static String ADRESSE = "adresse";
        public final static String ID_USER = "idUser";
        public final static String ADRESSE_SITE = "adresseSite";
        public final static String RIGHTS_USER_LIST = "rightUserList";
        public final static String SIGNATURE = "signature";

        public final static String NEW_PASSWORD = "newPassword";
        public final static String CURRENT_PASSWORD = "currentPassword";

        public final static String SUFFIXE_LOGIN = "@erecettes.cd";

    }

    public class Operation {

        public final static String LOGIN = "login";
        public final static String CHECK_SESSION = "checkSession";
        public final static String UPDATE_PASSWORD = "updatePassword";
        public final static String GET_SIGNATURE_AGENT = "getSignatureAgent";
        public final static String SAVE_SIGNATURE_AGENT = "saveSignatureAgent";
        public final static String LAST_DECONEXION = "lastDeconnexion";
    }
}
