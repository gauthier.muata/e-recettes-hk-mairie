/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author WILLY KASHALA
 */
public class DeclarationConst {

    public class ParamName {

        public final static String TYPE_DOCUMENT = "typeDocument";
        public final static String NUMERO_DOCUMENT = "numeroDocument";
        public final static String TYPE_RESEACH = "typeReseach";

        public final static String CODE_BORDEREAU = "codeBordereau";
        public final static String CODE_PERSONNE = "codePersonne";
        public final static String USER_ID = "userId";
        public final static String NOM_COMPLET = "nomComplet";
        public final static String DATE_PAIEMENT = "datePaiement";
        public final static String DATE_MISE_A_JOUR = "dateMiseJoujr";
        public final static String TOTAL_MONTANT_PERCU = "totalMontantPercu";
        public final static String TOTAL_MONTANT_PERCU_TO_STRING = "totalMontantPercuToString";
        public final static String COMPTE_BANCAIRE = "compteBancaire";
        public final static String LIBELLE_BANQUE = "libelleBanque";
        public final static String AGENT_CREATE = "agentCreate";
        public final static String ETAT = "etat";
        public final static String SERVICE = "service";
        public final static String SITE = "site";
        public final static String NUMERO_DECLARATION = "numeroDeclaration";
        public final static String NUMERO_BORDEREAU = "numeroBordereau";
        public final static String NUMERO_ATTESTATION_PAIEMENT = "numeroAttestationPaiement";
        public final static String IMPRESSION_RECU = "impressionPercu";
        public final static String CENTRE_BORDEREAU = "centreBordereau";
        public final static String TYPE_DECLARATION = "typeDeclaration";
        public final static String FORME_JURIDIQUE = "formeJuridique";
        public final static String CODE_FORME_JURIDIQUE = "codeFormeJuridique";
        public final static String ADRESSE_PERSONNE = "adressePersonne";
        public final static String CODE_ADRESSE_PERSONNE = "codeAdressePersonne";
        public final static String OBSERVATION = "observation";
        public final static String ARCHIVES = "archives";
        public final static String ARCHIVES_DOCUMENT = "pvDocument";

        public final static String LIST_DETAIL_BORDEREAU = "listDetailBordereau";
        public final static String CODE_DETAIL_BORDEREAU = "codeDetailBordereau";
        public final static String CODE_ARTICLE_BUDGETAIRE = "codeArticleBudgetaire";
        public final static String LIBELLE_ARTICLE_BUDGETAIRE = "libelleArticleBudgetaire";
        public final static String CODE_OFFICIEL = "codeOfficiel";
        public final static String MONTANT_PERCU = "montantPercu";
        public final static String MONTANT_PAYER = "montantPayer";
        public final static String DEVISE = "devise";
        public final static String PERIODICITE = "periodicite";
        public final static String INTITULE_PERIODICITE = "IntitulePeriodicite";
        public final static String CODE_PERIODICITE = "codePeriodiciteAB";
        public final static String INTITULE_BIEN = "intituleBien";
        public final static String ADRESSE_BIEN = "adresseBien";
        public final static String STATUT = "statut";
        public final static String ANNEE = "annee";
        public final static String MOIS = "mois";

        public final static String CODE_DEPOT_DECLARATION = "codeDepotDeclaration";
        public final static String NUMERO_DEPOT_DECLARATION = "numeroDepotDeclaration";
        public final static String DATE_CREATE = "dateCreate";
        public final static String NOMBRE_IMPRESSION = "nombreImpression";

        public final static String LIST_DETAIL_DEPOT_DECLARATION = "listDetailDepotDeclaration";
        public final static String TAUX = "taux";
        public final static String QUANTITE = "quantite";
        public final static String VALEUR_BASE = "valeurBase";
        public final static String CODE_TARIF = "codeTarif";
        public final static String INTITULE_TARIF = "intituleTarif";

        public final static String ID_RETRAIT_DECLARATION = "idRetraitDeclaration";
        public final static String REQUERANT = "requerant";
        public final static String BORDEREAU = "bordereau";

        public final static String VALUE_SEACH = "valueSearch";

        public final static String BORDEREAU_EXIST = "bordereauExist";
        public final static String PREUVE_PAIEMENT_EXIST = "preuvePaiementExist";
        public final static String PREUVE_PAIEMENT_DOCUMENT = "preuvePaiementDocument";

        public final static String DECLARATION_DOCUMENT_LIST = "declarationDocumentList";
        public final static String DECLARATION_DOCUMENT = "declarationDocument";
        public final static String NUMERO_NC = "noteCalcul";
        public final static String OBSERVATION_DOC = "observation";
        public final static String LIBELLE_DOCUMENT = "libelleDocument";
    }

    public class Operation {

        public final static String SEARCH_DOCUMENT_DEPOT_DECLARATION = "searchDocumentDepotDeclaration";
        public final static String SAVE_DEPOT_DECLARATION = "saveDepotDeclaration";
        public final static String SEARCH_DEPOT_DECLARATION = "searchDepotDeclaration";
        public final static String SEARCH_DEPOT_OF_ARTICLE_BUDGETAIRE = "searchDepotOfArticleBudgetaire";
        public final static String SEARCH_AVANCED_DEPOT_DECLARATION = "SearchAvancedDepotDeclaration";
        public final static String SEARCH_AVANCED_RETRAIT_DECLARATION = "SearchAvancedRetraitDeclaration";
        public final static String SEARCH_RETRAIT_DECLARATION = "searchRetraitDeclaration";
        public final static String PRINT_RECEPISSE = "printRecepisse";
        public final static String SEARCH_ARTICLE_BUDGETAIRE = "researchArticleBudgetaire";
        public final static String SEARCH_ARTICLE_BUDGETAIRE_ASSUJETTISSABLE = "researchArticleBudgetaireAssujettissable";
        public final static String REPRINT_RECEPISSE = "reprintRecepisse";
    }

    public class ParamNameDocument {

        public static final String BORDEREAU = "BR";
        public static final String DEPOT_DECLARATION = "DCL";

    }

    public class ParamQuery {

        public final static String DEPOT_DECLARATION_CODE = "RETURN_CODE_DEPOT";

        public final static String NUMERO_DEPOT = "NUMERO_DEPOT";
        public final static String PERSONNE = "PERSONNE";
        public final static String BORDEREAU = "BORDEREAU";
        public final static String DATE_CREATE = "DATE_CREATE";
        public final static String AGENT = "AGENT";
        public final static String NBR_IMPRESSION = "NBR_IMPRESSION";
        public final static String SERVICE = "SERVICE";
        public final static String ETAT = "ETAT";
        public final static String NUMERO_ATTESTATION_PAIEMENT = "NUMERO_ATTESTATION_PAIEMENT";
        public final static String HOST_NAME = "HOST_NAME";
        public final static String ADRESSE_MAC = "MAC_ADRESSE";
        public final static String ADRESSE_IP = "IP_ADRESSE";
    }

    public class ConfigCodeRegister {

        public static final String DEPOT_DECLATION = "DCL";
        public static final String REGISTRE_DEPOT_DECLATION = "RDCL";
        public static final String REGISTRE_RETRAIT_DECLATION = "RRCL";
    }

}
