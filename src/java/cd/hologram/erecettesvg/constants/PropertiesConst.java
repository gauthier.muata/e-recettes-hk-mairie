/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author WILLY KASHALA
 */
public class PropertiesConst {

    public class Config {

        public static final String PROPERTIES_FILE_PATH = "/cd/hologram/erecettesvg/properties/APP_CONFIG.properties";
        public static final String TXT_ACCOUNT_BANK_BAP_CODE = "TXT_ACCOUNT_BANK_BAP_CODE";

        public static final String MAILJET_USERNAME = "MAILJET_USERNAME";
        public static final String MAILJET_PASSWORD = "MAILJET_PASSWORD";
        public static final String MAILJET_URL = "MAILJET_URL";
        public static final String SMS_URL = "SMS_URL";
        public static final String SMS_URL2 = "SMS_URL2";
        public static final String MAIL_SMTP = "MAIL_SMTP";
        public static final String SMS_SERVICE = "SMS_SERVICE";

        public static final String ID_NATURE_AB_AMENDE = "ID_NATURE_AB_AMENDE";
        public static final String CODE_AGENT_AUTO = "CODE_AGENT_AUTO";
    }

    public class Message {

        public static final String PROPERTIES_FILE_PATH = "/cd/hologram/erecettesvg/properties/FR_Message.properties";
        public static final String TXT_CONTENTIEUX_RECLAMATION = "TXT_CONTENTIEUX_RECLAMATION";
        public static final String TXT_MOTIF_CONTENTIEUX = "TXT_MOTIF_CONTENTIEUX";
        public static final String TXT_GENERE_BON_A_PAYER_CONTENTIEUX = "TXT_GENERE_BON_A_PAYER_CONTENTIEUX";
        public static final String SMS_SUBSCRIBE = "SMS_SUBSCRIBE";
        public static final String SMS_SUBSCRIBE_NON = "SMS_SUBSCRIBE_NON";
        public static final String MAIL_SUBSCRIBE_CLIENT_SUBJECT = "MAIL_SUBSCRIBE_CLIENT_SUBJECT";
        public static final String MAIL_SUBSCRIBE_CLIENT_SUBJECT_NON = "MAIL_SUBSCRIBE_CLIENT_SUBJECT_NON";
        public static final String MAIL_SUBSCRIBE_CLIENT_CONTENT = "MAIL_SUBSCRIBE_CLIENT_CONTENT";
        public static final String MAIL_SUBSCRIBE_CLIENT_CONTENT_NON = "MAIL_SUBSCRIBE_CLIENT_CONTENT_NON";
        public static final String TXT_OBSERVATION_TAXATION_REPETITIVE = "MAIL_SUBSCRIBE_CLIENT_CONTENT_NON";

        public static final String TXT_PREVISION = "TXT_PREVISION";
        public static final String TXT_CREDIT = "TXT_CREDIT";

        public static final String DEMANDE_ECHELONNEMENT = "DEMANDE_ECHELONNEMENT";

        public static final String ECHELONNEMENT_VALIDATION = "ECHELONNEMENT_VALIDATION";
        public static final String ECHELONNEMENT_REJET = "ECHELONNEMENT_REJET";
        public static final String TRAITEMENT_DEMANDE_ECHELONNEMENT = "TRAITEMENT_DEMANDE_ECHELONNEMENT";
        public static final String TXT_OBSERVATION_BAP_INTERCALAIRE = "TXT_OBSERVATION_BAP_INTERCALAIRE";
        public static final String TXT_OBSERVATION_BAP_INTERCALAIRE_MOTIF = "TXT_OBSERVATION_BAP_INTERCALAIRE_MOTIF";
        
        public static final String TXT_OPERATION_NP_PENALITE = "TXT_OPERATION_NP_PENALITE"; 
        public static final String TXT_OBSERVATION_NP_PENALITE = "TXT_OBSERVATION_NP_PENALITE";
        public static final String ECHELONNEMENT_ORDONANCEMENT = "ECHELONNEMENT_ORDONANCEMENT";
        public static final String FRACTIONNEMENT_REVOCATION = "FRACTIONNEMENT_REVOCATION";
    }
}
