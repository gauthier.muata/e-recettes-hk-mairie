/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class PoursuiteConst {

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";
        public final static String CODE_SERVICE = "codeService";

        public final static String CODE_TYPE_MISE_EN_DEMEURE = "codeTypeMiseEnDemeure";
        public final static String CODE_TYPE_AMR = "codeTypeAmr";
        public final static String CODE_STATE_MISE_EN_DEMEURE = "codeStateMiseEnDemeure";
        public final static String CODE_STATE_AMR = "codeStateAmr";
        public final static String ADVANCED_SEARCH = "advancedSearch";
        public final static String VALUE_SEARCH = "valueSearch";
        public final static String TYPE_SEARCH = "typeSearch";

        public final static String NUMERO_MED = "numeroMed";

        public final static String DATE_ECHEANCE_MED = "dateEcheanceMed";
        public final static String DATE_RECEPTION_MED = "dateReceptionMed";
        public final static String DATE_CREATION_MED = "dateCreationMed";
        public final static String AGENT_CREATION_MED = "agentCreationMed";

        public final static String ASSUJETTI_NAME_COMPOSITE = "assujettiNameComposite";
        public final static String ARTICLE_BUDGETAIRE_NAME = "articleBudgetaireName";
        public final static String PERIODE_DECLARATION = "periodeDeclaration";
        public final static String ASSUJETTI_CODE = "assujettiCode";
        public final static String ASSUJETTI_NAME = "assujettiName";
        public final static String ADRESSE_NAME = "adresseName";

        public final static String PRINT_EXISTS = "printExist";
        public final static String DOCUMENT_PRINT = "documentPrint";

        public final static String AMOUNT_MED = "amountMed";
        public final static String DEVISE_MED = "deviseMed";

        public final static String EXERCICE_MED = "exerciceMed";
        public final static String TYPE_MED = "typeMed";
        public final static String ACTIVATE_NEXT_STAP = "activateNextStap";
        public final static String NUMERO_DOCUMENT = "numeroDocument";

        public final static String NUMERO_AMR = "numeroAmr";
        public final static String DATE_CREATE_AMR = "dateCreateAmr";
        public final static String DATE_RECEPTION_AMR = "dateReceptionAmr";
        public final static String DATE_LIMITE_AMR = "dateLimiteAmr";
        public final static String AMOUNT_PRINCIPAL_AMR = "amountPrincipalAmr";
        public final static String AMOUNT_PRINCIPAL_AMR_WITH_PENALITE = "amountPrincipalAmrWithPenalite";
        public final static String AMOUNT_PAID_AMR = "amountPaidAmr";
        public final static String DEVISE_AMR = "deviseAmr";
        public final static String TYPE_AMR = "typeAmr";
        public final static String USER_ID = "userId";
        public final static String NOMBRE_MOIS_REATARD = "nombreMoisRetard";

        public final static String HTML_CONTRAINTE = "htmlContrainte";
        public final static String HTML_COMMANDEMENT = "htmlCommandement";
        public final static String HTML_BON_A_PAYER = "htmlBonApayer";

        public final static String POURSUITE_EXIST = "poursuiteExist";
        public final static String ATD_EXIST = "atdExist";

        public final static String REFERENCE_DOCUMENT = "referenceDocument";
        public final static String TYPE_DOCUMENT = "typeDocument";

        public final static String CODE_TYPE_ATD = "codeTypeAtd";
        public final static String CODE_TYPE_CONTRAINTE = "codeTypeContrainte";
        public final static String CODE_STATE_CONTRAINTE = "codeStateContrainte";

        public final static String CODE_TYPE_FPC = "codeTypeFpc";
        public final static String CODE_TYPE = "codeType";

        public final static String NUMERO_CONTRAINTE = "numeroContrainte";
        public final static String NUMERO_COMMANDEMENT = "numeroCommandement";
        public final static String NUMERO_BON_A_PAYER = "numeroBap";

        public final static String AMOUNT_RECOUVREMENT = "amountRecouvrement";
        public final static String AMOUNT_PENALITE = "amountPenalite";
        public final static String AMOUNT_POURSUIVI = "amountPoursuivi";
        public final static String FRAIS_POURSUITE = "fraisPoursuite";
        public final static String DEVISE = "devise";

        public final static String DATE_CREAT_CONTRAINTE = "dateCreatContrainte";
        public final static String DATE_RECEPTION_CONTRAINTE = "dateReceptionContrainte";
        public final static String DATE_ECHEANCE_CONTRAINTE = "dateEcheanceContrainte";

        public final static String DATE_CREAT_COMMANDEMENT = "dateCreatCommandement";
        public final static String DATE_RECEPTION_COMMANDEMENT = "dateReceptionCommandement";
        public final static String DATE_ECHEANCE_COMMANDEMENT = "dateEcheanceCommandement";

        public final static String FAIT_GENERATEUR = "faitGenerateur";
        public final static String CONTRAINTE_HTML_DOCUMENT = "contrainteHtmlDocument";
        public final static String COMMANDEMENT_HTML_DOCUMENT = "commandementHtmlDocument";
        public final static String BON_A_PAYER_HTML_DOCUMENT = "bonApayerHtmlDocument";
        public final static String COMMANDEMENT_ACTIVATE = "commandementActivate";
        public final static String TIER_DETENTEUR = "tierDetenteur";

        public final static String ID_FPC = "idFpc";
        public final static String MOTIF_PENALITE = "motifPenalite";
        public final static String MONTANT_DU = "montantDu";
        public final static String MONTANT_TOTAL_DU = "montantTotalDu";
        public final static String STATE = "etat";
        public final static String CAS_FPC = "typeFpc";
        public final static String STATE_NAME = "etatLibelle";
        public final static String DETAILS_FPC_LIST = "detailFpcList";
        public final static String TITRE_PERCEPTION = "titrePerception";

        public final static String ID_DETAIL_FPC = "idDetailFpc";
        public final static String PENALITE_NAME = "penaliteName";
        public final static String PERIODE_DECLARATION_NAME = "periodeDeclarationName";
        public final static String TAUX_PENALITE = "tauxPenalite";
        public final static String PRINCIPAL_DU = "principalDu";
        public final static String PENALITE_DU = "penaliteDu";
        public final static String NBRE_MOIS_RETARD = "nombreMoisRetard";
        public final static String DATE_CREATE_FPC = "dateCreateFpc";
        public final static String COMPTE_BANCAIRE = "compteBancaire";
        public final static String ACTIVE_ROLE = "activeRole";

    }

    public class Operation {

        public final static String LOAD_MISE_EN_DEMEURE = "loadMiseEnDemeure";
        public final static String LOAD_AMR = "loadAmr";
        public final static String PRINT_AMR = "printAmr";
        public final static String GENERATE_CONTRAINTE = "generateContrainte";
        public final static String ACTIVATE_COMMANDEMENT = "activateCommandement";
        public final static String PRINT_DOCUMENT = "printDocument";
        public final static String LOAD_CONTRAINTE = "loadContrainte";
        public final static String LOAD_COMMANDEMENT = "loadCommandement";
        public final static String LOAD_ATD = "loadAtd";
        public final static String PRINT_ATD = "printAtd";
        public final static String LOAD_FICHE_PRISE_CHARGE = "loadFichePriseCharge";
        public final static String VALIDATE_FICHE_PRISE_CHARGE = "validateFichePriseCharge";
        public final static String GENERATE_ATD = "generateAtd";
        public final static String TRAITER_FICHE_PRISE_CHARGE = "traiterFichePriseCharge";
        public final static String LOAD_BON_A_PAYER = "loadBonAPayer";
        public final static String LOAD_PENALITE_LIST = "getPenalite";
    }
}
