/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class AssujettissementConst {

    public class Operation {

        public final static String LOAD_TYPE_BIEN = "loadTypeBien";
        public final static String LOAD_TYPE_COMPLEMENT_BIEN = "loadTypeComplementBien";
        public final static String SAVE_BIEN = "saveBien";
        public final static String SAVE_BIEN_IMMOBILIER = "saveBienImmobilier";
        public final static String LOAD_BIENS_PERSONNE = "loadBiensPersonne";
        public final static String LOAD_BIENS = "loadBiens";
        public final static String GET_BIENS_PERSONNE = "getBiensPersonne";
        public final static String SAVE_BIEN_LOCATION = "saveBienLocation";
        public final static String LOAD_ARTICLES_BUDGETAIRES = "loadArticlesBudgetaires";
        public final static String LOAD_DATA_ASSUJETTISSEMENT = "loadDataAssujettissement";
        public final static String GENERATE_PERIODES_DECLARATIONS = "generatePeriodesDeclarations";
        public final static String SAVE_ASSUJETTISSEMENT = "204";
        public final static String LOAD_ARTICLE_BUDGETAIRE_ASSUJETTISSABLE = "loadArticleBudgetaireAssujettissable";
        public final static String CREATE_PERIODE_DECLARATION = "createPeriodeDeclaration";
        public final static String DESACTIVATE_ASSUJETTISSEMENT = "desactivateAssujettissement";
        public final static String GET_BIEN_FOR_EDITION = "getBienForEdition";
        public final static String DESACTIVATE_BIEN_ACQUISITION = "desactivateBienAcquisition";
        public final static String GET_AB_ASSUJETTI = "getABAssujetti";
        public final static String SAVE_RETRAIT = "saveRetraitDeclaration";
        public final static String LOAD_TAXES_PEAGES = "loadTaxesPeages";
        public final static String SAVE_ASSUJETTISSEMENT_PEAGE = "saveAssujettissementPeage";
        public final static String LOAD_BIENS_PERSONNE2 = "loadBiensPersonne2";
        public final static String LOAD_INFORMATIONS_CARTE = "loadInformationsCarte";

        public final static String LOAD_TYPE_BIEN_BY_SERVICE = "loadTypeBienByService";

    }

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String LIBELLE = "libelle";
        public final static String CODE_TYPE_BIEN = "codeTypeBien";
        public final static String LIBELLE_TYPE_BIEN = "libelleTypeBien";
        public final static String EST_CONTRACTUEL = "estContractuel";

        public final static String ID_BIEN = "idBien";
        public final static String ID_AB = "ab";
        public final static String INTITULE_BIEN = "intituleBien";
        public final static String INTITULE_BIEN_COMPOSITE = "intituleBienComposite";
        public final static String CONTRAT_EXIST = "contratExist";
        public final static String CONTRAT_ID = "contratId";
        public final static String ID_ACQUISITION = "idAcquisition";
        public final static String ID_ASSUJETTISSEMENT = "idAssujettissement";
        public final static String DESCRIPTION_BIEN = "descriptionBien";
        public final static String DATE_ACQUISITION = "dateAcquisition";
        public final static String CODE_ADRESSE_PERSONNE = "codeAP";
        public final static String CODE_PERSONNE = "codePersonne";
        public final static String LISTE_RETRAIT = "listRetrait";
        public final static String PERSONNE_ACQUIT = "personneAcquit";
        public final static String ADRESSE_BIEN = "adresseBien";
        public final static String CHAINE_ADRESSE = "chaineAdresse";
        public final static String RESPONSABLE = "responsable";
        public final static String CODE_RESPONSABLE = "codeResponsable";
        public final static String REFERENCE_CONTRAT = "referenceContrat";
        public final static String NUM_ACTE_NOTARIE = "numActeNotarie";
        public final static String DATE_ACTE_NOTARIE = "dateActeNotarie";
        public final static String ID_USER = "idUser";
        public final static String FOR_LOCATION = "forLocation";
        public final static String PROPRIETAIRE = "proprietaire";
        public final static String IMMATRICULATION = "immatriculation";
        public final static String HASTARIF = "hasTarif";
        public final static String DATA_TARIF = "dataTarif";

        public final static String CODE_ARTICLE_BUDGETAIRE = "codeAB";
        public final static String FORME_JURIDIQUE = "formeJuridique";
        public final static String INTITULE_ARTICLE_BUDGETAIRE = "intituleAB";
        public final static String INTITULE_ARTICLE_GENERIQUE = "intituleArticleGenerique";
        public final static String TARIF = "tarif";
        public final static String BASE_VARIABLE = "baseVariable"; //TYPE COMPLEMENT -- Champ actif
        public final static String PERIODICITE_VARIABLE = "periodiciteVariable"; // AB -- periodicite true
        public final static String TARIF_VARIABLE = "tarifVariable"; // AB -- tarif true
        public final static String NOMBRE_JOUR_LIMITE = "nombreJourLimite";
        public final static String ECHEANCE_LEGALE = "echeanceLegale";
        public final static String PERIODE = "periode";
        public final static String PERIODE_ID = "periodeID";
        public final static String DUREE = "duree";
        public final static String TACITE_RECONDUCTION = "reconduction";
        public final static String MOIS = "mois";
        public final static String ANNEE = "annee";
        public final static String NUMERO_NOTE_CALCUL = "numeroNoteCalcul";
        public final static String PERIODE_ECHEANCE = "periodeEcheance";
        public final static String MONTANT = "montant";
        public final static String DEVISE = "devise";
        public final static String NUMERO_DECLARATION = "numDeclaration";
        public final static String NOM_REQUERANT = "requerant";
        public final static String HAS_PALIER = "hasPalier";
        public final static String PALIER_LIST = "palierList";
        public final static String ETAT = "etat";

        //Nombre jour limite
        public final static String COMPLEMENTS_BIEN = "complementBiens";

        public final static String ID_COMPLEMENT_BIEN = "id";
        public final static String CODE_TYPE_COMPLEMENT_BIEN = "code";
        public final static String VALEUR_COMPLEMENT_BIEN = "valeur";
        public final static String UNITE = "unite";
        public final static String DATE_DEPART = "dateDepart";
        public final static String VALEUR_BASE = "valeurBase";
        public final static String UNITE_DEVISE = "uniteDevise";

        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";
        public final static String DATE_FIN2 = "dateFin2";
        public final static String DATE_LIMITE = "dateLimite";
        public final static String ORDRE_PERIODE = "ordrePeriode";

        public final static String CODE_PERIODICITE = "codePeriodicite";
        public final static String INTITULE_PERIODICITE = "intitulePeriodicite";
        public final static String NOMBRE_JOUR = "nombreJour";
        public final static String CODE_TARIF = "codeTarif";
        public final static String MULTIPLIER_VALEUR_BASE = "multiplierValeurBase";
        public final static String TAUX_AB = "tauxAB";
        public final static String PALIERS = "paliers";
        public final static String CODE_GESTIONNAIRE = "codeGestionnaire";
        public final static String INTITULE_TARIF = "intituleTarif";
        public final static String PERIODES_DECLARATIONS = "periodesDeclarations";

        public final static String CODE_PERIODICITE_AB = "codePeriodiciteAB";
        public final static String LIBELLE_PERIODICITE_AB = "libellePeriodiciteAB";
        public final static String CODE_TARIF_AB = "codeTarifAB";
        public final static String CODE_TARIF_BIEN = "codeTarifBien";

        public final static String LIST_PERIODICITES = "listPeriodicites";
        public final static String LIST_TARIFS = "listTarifs";
        public final static String LIST_PERIODES_DECLARATIONS = "listPeriodesDeclarations";

        public final static String LIST_BIENS = "listBiens";
        public final static String LIST_ARTICLE_BUDGETAIRES = "listArticleBudgetaires";

        public final static String CODE_TYPE_COMPLEMENT = "codeTypeComplement";
        public final static String LIBELLE_TYPE_COMPLEMENT = "libelleTypeComplement";
        public final static String OBJET_INTERACTION = "objetInteraction";
        public final static String VALEUR_PREDEFINIE = "valeurPredefinie";
        public final static String INPUT_VALUE = "inputValue";

        public final static String CODE_OFFICIEL = "codeOfficiel";
        public final static String PALIER_TYPE_TAUX = "palierTypeTaux";
        public final static String CODE_TYPE_PERSONNE = "codeTypePersonne";
        public final static String BORNE_INFERIEUR = "borneInferieur";
        public final static String BORNE_SUPERIEUR = "borneSuperieur";
        public final static String NBRE_JOUR_LEGALE_PAIEMENT = "nbreJourLegalePaiement";
        public final static String DATE_LIMITE_PAIEMENT = "dateLimitePaiement";
        public final static String IS_ECHUS = "isEchus";
        public final static String IS_ECHUS_PAIEMENT = "isEchusPaiement";
        public final static String SELECTED_AB = "selectAB";
        public final static String NVL_ECHEANCE = "nvlEcheance";
        public final static String INFO_COMPLEMENT_BIEN = "infoComplementBien";
        public final static String UNITE_VALEUR = "UNITE_VALEUR";

        public final static String IS_MANY_ARTICLE_BUDGETAIRE = "isManyAB";
        public final static String SECTEUR_ACTIVITE = "secteurActivite";
        public final static String ETAT_ASSUJETTISSEMENT = "etat";
        public final static String OBSERVATION = "observation";
        public final static String AGENT_MAJ = "agentMaj";
        public final static String DATE_MAJ = "dateMaj";

        public final static String SECTEUR_ACTIVITE_CODE = "secteurActiviteCode";
        public final static String MINISTERE_LIBELLE = "ministereLibelle";
        public final static String IS_PALIER = "isPalier";
        public final static String TYPE_AB = "typeAB";
        public final static String ARTICLE_REFERENCE = "articleReference";

        public final static String TARIFS = "tarifs";

        public final static String IS_GENERATE = "isGenerate";

        public final static String CODE_SERVICE = "codeService";

        public final static String TYPE_VALEUR = "typeValeur";
        public final static String AGENT_CREAT = "agentCreat";
        public final static String VALEUR_TYPE = "valeurType";

        public final static String PERIODE_RECIDIVE = "periodeRecidive";

        public final static String CODE_UNITE = "codeUnite";
        public final static String INTITULE_UNITE = "intituleUnite";
        public final static String SUPERFICIE_BATIE = "superficieBatie";
        public final static String INTITULE_COMPLEMENT_IF = "intituleCOmplementIF";
        public final static String VALEUR_COMPLEMENT_IF = "ValeurCOmplementIF";
        public final static String CODE_UNITE_COMPLEMENT = "codeUniteComplement";
        public final static String CODE_PROPRIETAIRE_BIEN = "codeProprietaireBien";
        public final static String UNITE_ECONOMIQUE_LIST = "uniteEconomiqueList";
        public final static String BIEN_ACTIVITE_LIST = "bienActiviteList";
        
         public final static String ID_ACTIVITE = "idActivite";
         public final static String INTITULE_ACTIVITE = "intituleActivite";
    }

    public class ResultCode {

        public final static String SUCCES_OPERATION = "1";
        public final static String FAILED_OPERATION = "0";
        public final static String EXCEPTION_OPERATION = "-1";
        public final static String EXIST_ASSUJETTISSEMENT = "2";
    }

    public class ObjetInteraction {

        public final static String EMPTY = "";
        public final static String DATE = "DATE";
        public final static String MONTANT = "MONTANT";
        public final static String NOMBRE = "NOMBRE";
    }

    public class ParamControl {

        public final static String INPUT_CONTROL = "<div class=\"form-group\">\n"
                + " <p class=\"control-label col-sm-4\" for=\"%s\">%s %s</p>\n"
                + " <div class=\"col-sm-8\">\n"
                + " <input type=\"%s\" class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\" value=\"%s\"/>\n"
                + " </div>\n"
                + " </div>";

        public final static String BOX_CONTROL = "<div class=\"form-group\">\n"
                + " <p class=\"control-label col-sm-4\" for=\"%s\">%s %s</p>\n"
                + " <div class=\"col-sm-4\">\n"
                + " <input type=\"%s\" class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\" value=\"%s\"/>\n"
                + " </div>\n"
                + " <div class=\"col-sm-4\">\n"
                + " <select class=\"form-control\" id=\"%s\" placeholder=\"\" name=\"\">%s</select>\n"
                + " </div>\n"
                + " </div>";

        public final static String COMPLEMENT_LIST = "<div class=\"form-group\">\n"
                + " <p class=\"control-label col-sm-4\" for=\"%s\">%s %s</p>\n"
                + " <div class=\"col-sm-8\">\n"
                + " <select class=\"form-control\" id=\"%s\" placeholder=\"%s\" name=\"%s\">%s</select>\n"
                + " </div>\n"
                + " </div>";

        public final static String ITEM_OPTION = "<option value =\"%s\" %s>%s</option>";

        public final static String SELECTED_OPTION_ATTR = "selected";

        public final static String DATA_TABLE_ROW = "<tr>\n"
                + " <td>&nbsp;%s</td>\n"
                + " <td>&nbsp;%s</td>\n"
                + " </tr>";

        public final static String SPAN_REQUIRED_VALUE = "<span style=\"color:red\"> *</span>";
    }

    public static class TypeData {

        public final static String TYPE_TEXT = "text";
        public final static String TYPE_NUMBER = "number";
        public final static String TYPE_DATE = "date";
    }

    public static class ParamQUERY {

        public final static String BIENID = "BIEN_ID";
        public final static String INTITULE = "INTITULE";
        public final static String DESCRIPTION = "DESCRIPTION";
        public final static String PERSONNE = "PERSONNE";
        public final static String ADRESSE_PERSONNE = "ADRESSE";
        public final static String TYPE_BIEN = "TYPE_BIEN";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String DATE_ACQUISITION = "DATE_ACQUISITION";

        public final static String ASSUJETTISSEMENT_ID = "ASSUJETTISSEMENT_ID";
        public final static String BIEN = "BIEN";
        public final static String ARTICLE_BUDGETAIRE = "ARTICLE_BUDGETAIRE";
        public final static String DUREE = "DUREE";
        public final static String RENOUVELLEMENT = "RENOUVELLEMENT";
        public final static String DATE_DEBUT = "DATE_DEBUT";
        public final static String DATE_FIN = "DATE_FIN";
        public final static String VALEUR = "VALEUR";
        public final static String ADRESSE = "ADRESSE";
        public final static String NBR_JOUR_LIMITE = "NBR_JOUR_LIMITE";
        public final static String PERIODICITE = "PERIODICITE";
        public final static String TARIF = "TARIF";
        public final static String GESTIONNAIRE = "GESTIONNAIRE";
        public final static String COMPLEMENT_BIEN = "COMPLEMENT_BIEN";
        public final static String NOUVELLES_ECHEANCES = "NOUVELLES_ECHEANCES";
        public final static String ASSIGNATION_RETURN_ID = "ID";
        public final static String UNITE_VALEUR = "UNITE_VALEUR";
    }
}
