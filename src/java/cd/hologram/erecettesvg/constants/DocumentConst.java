/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author moussa.toure
 */
public class DocumentConst {

    public class DocumentCode {

        public static final String NP = "NP";
        public static final String NP_VOIRIE = "NP_VOIRIE";
        public static final String NP2 = "NOTE DE PERCEPTION";
        public static final String CPI = "CPI";
        public static final String CPI_TEXT = "CERTIFICAT DE PAIEMENT INFORMATISE";
        public static final String AMR_ = "AMR_";
        public static final String QUITTANCE = "QUITTANCE";
        public static final String AMR = "AMR";
        public static final String LETTRE_RECLAMATION = "LR";
        public static final String RECLAMATION = "RCLM";
        public static final String BON_A_PAYER = "BP";
        public static final String BON_A_PAYER_TEXT = "BON A PAYER";
        public static final String DECISION_ADMINISTRATIVE = "DECISIONADMIN";
        public static final String DECISION_JURIDICTIONNELLE = "DECISIONJURIDIC";
        public static final String DEPOT_DECLARATION = "DEC";
        public static final String BORDEREAU = "BR";
        public static final String RECEPISSE = "RCPS";
        public static final String RECEPISSE_TXT = "RECEPISSE";
        public static final String MED = "MED";
        public static final String RELANCE = "RELANCE";
        public static final String MED_2 = "DECLARATION";
        public static final String MEP = "MEP";
        public static final String MEP_2 = "PAIEMENT";
        public static final String AMR1 = "AMR1";
        public static final String AMR2 = "AMR2";
        public static final String CONTRAINTE = "CONTRAINTE";
        public static final String CTR = "CTR";
        public static final String COMMANDEMENT = "COMMANDEMENT";
        public static final String CMDT = "CMDT";
        public static final String ATD = "ATD";
        public static final String AMR_1 = "AMR1";
        public static final String AMR_2 = "AMR2";
        public static final String BON_A_PAYER_V2 = "BP_V2";
        public static final String INVITATION_SERVICE = "INVITATION_SERVICE";
        public static final String INVITATION_SERVICE_TXT = "INVITATION DE SERVICE";

        public static final String NP_P = "NP_P";
        public static final String BP = "BP";
        public static final String INVITATION_A_PAYER = "INVITATION_A_PAYER";
        public static final String INVITATION_A_PAYER_TXT = "INVITATION A PAYER";
        public static final String NOTE_TAXATION_DECLARATION = "NOTE_TAXATION_DECLARATION";
        public static final String NOTE_TAXATION_DECLARATION_TXT = "NOTE DE TAXATION";
        public static final String AMR_INVITATION_A_PAYER = "AMR_INVITATION_A_PAYER";
        public static final String AMR_INVITATION_A_PAYER_TXT = "AMR INVITATION A PAYER";
        public static final String AMR_TAXATION_OFFICE_TXT = "AMR TAXATION OFFICE";
        public static final String DERNIER_AVERTISSEMENT = "DA";
        public static final String EXTRAIT_ROLE = "EXTRAIT DE ROLE";
        public static final String PROJECT_ROLE = "ROLE";
        public static final String INVITE_SERVICE = "SERVICE";
        public static final String NP_MAIRIE = "NP_M";
        public static final String VOLET_A = "VL_A";
        public static final String VOLET_B = "VL_B";
        public static final String VOLET_A_TXT = "VOLET_A_TXT";
        public static final String VOLET_B_TXT = "VOLET_B_TXT";
        //public static final String RELANCE = "RELANCE";
    }

    public class DocumentName {

        public static final String NOTE_PERCEPTION = "NOTE DE PERCEPTION";
        public static final String CERTIFICAT_PAIEMENT_INFORMATISE = "CERTIFICAT DE PAIEMENT INFORMATISE";
        public static final String QUITTANCE = "QUITTANCE";
        public static final String AVIS_MISE_RECOUVREMENT = "AVIS DE MISE EN RECOUVREMENT";
        public static final String NOTE_TAXATION = "NOTE DE TAXATION";
    }

    public class DocumentSession {

        public static final String NOTE_PERCETION = "npDoc";
        public static final String NOTE_PERCETION_VOIRIE = "np2Doc";
        public static final String CPI = "cpiDoc";
        public static final String QUITTANCE = "SESSION_QUITTANCE";
        public static final String BON_A_PAYER = "bapDoc";
        public static final String RECEPISSE = "rcpDoc";
        public static final String MED = "medDoc";
        public static final String MEP = "mepDoc";
        public static final String AMR = "amrDoc";
        public static final String CONTRAINTE = "contDoc";
        public static final String COMMANDEMENT = "cmdDoc";
        public static final String ATD = "atdDoc";
        public static final String NOTE_TAXATION = "ntDoc";
        public static final String INVITATION_A_PAYER = "ia";
        public static final String AMR_INVITATION_A_PAYER = "amr";
        public static final String PROJECT_ROLE = "role";
        public static final String VOLET_A = "vla";
    }

    public class DocumentParamName {

        public static final String TEXT_A_AFFICHE = "textAafficher";
        public static final String TEXT_TRAITER_A_VISUALISER = "textTraiterAvisualiser";
        public static final String LIST_ANCHOR = "listAnchor";
        public static final String INITIER_CODE_DOCUMENT = "initieCodDocument";
        public static final String CODE_MODEL_DOCUMENT_CONFIG = "codeModelDocConfig";
        public static final String INPUT = "input";
        public static final String AREA = "area";
        public static final String TEXT_TRAITER_VISUALISER = "textTtraitervisualise";
        public static final String GET = "get";
        public static final String DEFAULT_TEXT = "defaultText";

    }

    public class ButtonName {

        public static final String BTN = "btn";
        public static final String ENREGISTRER = "Enregistrer";
        public static final String VISUALISER = "Visualiser";
    }

}
