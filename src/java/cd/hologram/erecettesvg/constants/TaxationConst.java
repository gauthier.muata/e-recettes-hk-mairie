/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

public class TaxationConst {

    public final static String OK = "OK";
    public final static String KO = "KO";

    public final static String TAXATION_CONFORME = "AV0012015";
    public final static String TAXATION_NON_CONFORME = "AV0022015";

    public class ParamName {

        public final static String SESSION = "session";
        public final static String OPERATION = "operation";
        public final static String VALUE_SEACH = "valueSearch";
        public final static String TYPE_SEACH = "typeSearch";
        public final static String TYPE_PAGE = "TypePage";
        public final static String CODE = "code";
        public final static String NIF = "nif";
        public final static String NOM_ASSUJETTI = "nom";
        public final static String AMOUNT_V1 = "amount1";
        public final static String AMOUNT_V2 = "amount2";
        public final static String BIEN = "bien";
        public final static String NUMERO = "numero";
        public final static String USER_NAME = "userName";
        public final static String NOM_COMPLET_ASSUJETTI = "nomCompletAssujetti";
        public final static String POSTNOM_ASSUJETTI = "postnom";
        public final static String PRENOM_ASSUJETTI = "prenom";
        public final static String LIBELLE_FORME_JURIDIQUE = "libelleFormeJuridique";
        public final static String CODE_FORME_JURIDIQUE = "codeFormeJuridique";
        public final static String ADRESSE_ASSUJETTI = "adresseAssujetti";
        public final static String ID_ADRESSE_ASSUJETTI = "adresseId";
        public final static String SEE_EVERYTHING = "seeEverything";
        public final static String CODE_SERVICE = "codeService";
        public final static String CODE_BIEN = "codeBien";
        public final static String LIBELLE_SERVICE = "libelleService";
        public final static String CODE_ARTICLE_BUDGETAIRE = "codeArticleBudgetaire";
        public final static String LIBELLE_ARTICLE_BUDGETAIRE = "libelleArticleBudgetaire";
        public final static String CODE_ACTE_GENERATEUR = "codeActeGenerateur";
        public final static String LIBELLE_ACTE_GENERATEUR = "libelleActeGenerateur";
        public final static String CODE_UNITE = "codeUnite";
        public final static String LIBELLE_UNITE = "libelleUnite";
        public final static String PALIER = "palier";
        public final static String CODE_PERIODICITE = "codePeriodicite";
        public final static String ASSUJETISSABLE = "assujetissable";
        public final static String CODE_TARIF = "codeTarif";
        public final static String PROPRIETAIRE = "proprietaire";
        public final static String TRANSACTIONNEL = "transactionnel";
        public final static String TRANSACTIONNEL_MINIMUM = "transactionnelMinimum";
        public final static String TRANSACTIONNEL_MAXIMUM = "transactionnelMaximum";
        public final static String NBRE_JOUR_LIMITE = "nbreJourLimite";
        public final static String CODE_NATURE = "codeNature";
        public final static String QUANTITE_VARIABLE = "quantiteVariable";
        public final static String CODE_ARRETE = "codeArrete";
        public final static String PALIER_LIST = "palierList";
        public final static String PALIER_EXIST = "palierExist";
        public final static String CODE_DOCUMENT_OFFICIEL = "codeDocumentOfficiel";
        public final static String PERIODICITE_VARIABLE = "periodiciteVariable";
        public final static String TARIF_VARIABLE = "tarifVariable";
        public final static String LIBELLE_TARIF = "libelleTarif";
        public final static String VALEUR_TARIF = "valeurTarif";
        public final static String TYPE_VALEUR_TARIF = "typeValeurTarif";
        public final static String CODE_ADRESSE_ASSUJETTI = "codeAdresse";
        public final static String EXERCICE_FISCAL = "exerciceFiscal";
        public final static String CODE_SITE = "codeSite";
        public final static String USER_ID = "userId";
        public final static String DATE_CREATE = "dateCreate";
        public final static String DATE_ECHEANCE = "dateEcheance";
        public final static String DEPOT_DECLARATION = "depotDeclaration";
        public final static String TYPE_TAUX_PALIER = "typeTaux";
        public final static String CODE_PALIER = "codePalier";
        public final static String TAUX_PALIER = "tauxPalier";
        public final static String TAUX = "taux";
        public final static String TAUX_PALIER_TO_FLOAT = "tauxPalierToFloat";
        public final static String BORNE_INFERIEUR_PALIER = "borneInferieurPalier";
        public final static String BORNE_SUPERIEUR_PALIER = "borneSuperieurPalier";
        public final static String DEVIE_PALIER = "devisePalier";
        public final static String UNITE_PALIER = "unitePalier";
        public final static String UNITE_LIBELLE_PALIER = "uniteLibellePalier";
        public final static String MULTIPLIER_PAR_VALEUR_BASE = "multiplierParValeurBase";
        public final static String BASE_CALCUL = "baseCalcul";
        public final static String QUANTITY = "quantity";
        public final static String DETAIL_TAXATION = "detailTaxation";
        public final static String CODE_RESPONSIBLE = "codeResponsible";
        public final static String IDuniteEco = "IDuniteEco";
        public final static String TYPE_REGISTER = "typeRegister";
        public final static String DECLARATION_ONLINE = "declarationOnlineOnly";
        public final static String IS_AVANCED_SEARCH = "isAdvancedSearch";
        public final static String VIEW_ALL_SITE = "allSite";
        public final static String VIEW_ALL_SERVICE = "allService";
        public final static String NUMERO_NC = "noteCalcul";
        public final static String LIBELLE_SITE = "libelleSite";
        public final static String AMOUNT = "amount";
        public final static String AMOUNT_PERCU = "amountPercu";
        public final static String ID_AVIS = "idAvis";
        public final static String CODE_AVIS = "codeAvis";
        public final static String LIBELLE_AVIS = "libelleAvis";
        public final static String LIST_DETAIL_NC = "listDetailNcs";
        public final static String CODE_OFFICIEL = "codeOfficiel";
        public final static String DEVISE = "devise";

        public final static String OBSERVATION_TAXATION = "observationTaxation";

        public final static String TOTAL_AMOUNT1 = "totalAmount1";
        public final static String TOTAL_AMOUNT2 = "totalAmount2";

        public final static String OBSERVATION_ORDONNANCEMENT = "observationOrdonnancement";
        public final static String DATE_VALIDATION = "dateValidation";

        public final static String SOLDE = "solde";
        public final static String NET_A_PAYE = "netAPaye";
        public final static String AMOUNT_PAY = "amountPay";
        public final static String REMISE = "remise";
        public final static String NP_MANUEL = "npManuel";
        public final static String NP_MERE = "npMere";
        public final static String FRACTION = "fraction";
        public final static String AMR = "amr";
        public final static String USE_NP_GENERATE = "useNpGenerate";

        public final static String ADD_N_ARTICLE_PANIER = "addNArticleToPanier";
        public final static String ARTICLE_BUDGETAIRE = "articleBudgetaire";
        public final static String PERIODICITE = "periodicite";
        public final static String COMPTE_BANCAIRE = "compteBancaire";

        public final static String CODE_ENTITE = "codeEntite";
        public final static String GET_MERE = "getMere";

        public final static String CODE_BUDGETAIRE = "codeBudgetaire";
        public final static String ARCHIVES = "archives";
        public final static String ARCHIVE_LIST = "archives";

        public final static String ID_ASSUJETTISSEMENT = "idAssujettissement";
        public final static String IS_CORRECTION = "isCorrection";
        public final static String PERIODICITE_NAME = "periodiciteName";
        public final static String VALEUR_BASE = "valeurBase";
        public final static String NC_SOURCE = "ncSource";
        public final static String EST_RECIDIVISTE = "estRecidiviste";
        public final static String ECHEANCE = "echeance";
        public final static String ECHEANCE_PAIEMENT_EXIST = "echeancePaiementExist";
        public final static String ECHEANCE_PAIEMENT = "echeancePaiement";
        public final static String NBRE_MOIS = "moisRetard";
        public final static String EST_PENALISE_PAIEMENT = "estPenalisePaiement";
        public final static String PERIODE_DECLARATION = "periodeDeclaration";
        public final static String ID_PERIODE_DECLARATION = "idPeriodeDeclaration";
        public final static String EST_PENALISE = "estPenalise";
        public final static String TEXT_OBSERVATION = "textObservation";
        public final static String PERIODE_DECLARATION_LIST = "periodeDeclarationList";

        public final static String DECLARATION_DOCUMENT = "declarationDocument";
        public final static String DECLARATION_DOCUMENT_LIST = "declarationDocumentList";

        public final static String ID_DISPATCH_SERIE = "idDispatch";

        public final static String DECISION_JURIDICTIONNEL = "decisionJuridictionnel";
        public final static String OBSERVATION_TRAITEMENT = "observationTraitement";

        public final static String CODE_AMR = "codeAmr";
        
        public final static String NUMERO_DEPOT = "numeroDepot";
        
         public final static String OBSERVATION_CONTROLE = "observationControle";

        public final static String MONTANT_TOTAL_PAYER = "montanTotalPayer";
        public final static String MONTANT_TOTAL_RESTE = "montanTotalReste";

        public final static String MONTANT_PAYE = "montantPaye";
         public final static String BUDGET_ARTICLE_CODE = "budgetArticleCode";
         public final static String IS_ADVANCE = "isAdvance";
    }

    public class Operation {

        public final static String OPERATION = "operation";
        public final static String RESEARCH_RESPONSIBLE = "researchResponsible";
        public final static String RESEARCH_BUDGET_ARTICLE = "researchBudgetArticle";
        public final static String RESEARCH_RATE_BY_ARTICLE_BUDGET = "researchRateArticle";
        public final static String RESEARCH_TAXATION = "researchTaxation";
        public final static String RESEARCH_ADVANCED_TAXATION = "researchAdvancedTaxation";
        public final static String SAVE_NOTE_CALCUL = "saveNoteCalcul";
        public final static String GET_TARIF = "getTarif";
        public final static String GET_PALIER = "getPalier";
        public final static String CHECK_IS_NUMERIC = "checkIsNumeric";
        public final static String GET_LIST_AVIS = "getListAvis";
        public final static String CLOSED_TAXATION = "closedTaxation";
        public final static String GET_TOTAL_AMOUNT = "getTotalAmount";
        public final static String ORDONNANCER_TAXATION = "ordonnancerTaxation";
        public final static String RESEARCH_IMPOT = "researchImpot";
        public final static String RESEARCH_AB_CONDUCTEUR_MOTO = "researchAbConducteurMoto";
        public final static String RESEARCH_TAXATION_REPETITIVE = "researchTaxationRepetitive";
    }

    public class ConfigCodeRegister {

        public static final String RNC = "RNC";
        public static final String RNCC = "RNCC";
        public static final String RNCR = "RNCR";
        public static final String RNCRO = "RNCRO";
        public static final String RNCO = "RNCO";
    }

}
