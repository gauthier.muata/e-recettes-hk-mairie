/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ATD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Atd.findAll", query = "SELECT a FROM Atd a"),
    @NamedQuery(name = "Atd.findById", query = "SELECT a FROM Atd a WHERE a.id = :id"),
    @NamedQuery(name = "Atd.findByIdCommandement", query = "SELECT a FROM Atd a WHERE a.idCommandement = :idCommandement"),
    @NamedQuery(name = "Atd.findByIdDetenteur", query = "SELECT a FROM Atd a WHERE a.idDetenteur = :idDetenteur"),
    @NamedQuery(name = "Atd.findByDateEmission", query = "SELECT a FROM Atd a WHERE a.dateEmission = :dateEmission"),
    @NamedQuery(name = "Atd.findByNumeroDocument", query = "SELECT a FROM Atd a WHERE a.numeroDocument = :numeroDocument"),
    @NamedQuery(name = "Atd.findByMontantGlobal", query = "SELECT a FROM Atd a WHERE a.montantGlobal = :montantGlobal"),
    @NamedQuery(name = "Atd.findByMontantPenalite", query = "SELECT a FROM Atd a WHERE a.montantPenalite = :montantPenalite"),
    @NamedQuery(name = "Atd.findByMontantFonctionnement", query = "SELECT a FROM Atd a WHERE a.montantFonctionnement = :montantFonctionnement"),
    @NamedQuery(name = "Atd.findByAgentCreat", query = "SELECT a FROM Atd a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "Atd.findByDateCreat", query = "SELECT a FROM Atd a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "Atd.findByAgentMaj", query = "SELECT a FROM Atd a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "Atd.findByDateMaj", query = "SELECT a FROM Atd a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "Atd.findByEtat", query = "SELECT a FROM Atd a WHERE a.etat = :etat"),
    @NamedQuery(name = "Atd.findByIdRedevable", query = "SELECT a FROM Atd a WHERE a.idRedevable = :idRedevable"),
    @NamedQuery(name = "Atd.findByMontantPrincipal", query = "SELECT a FROM Atd a WHERE a.montantPrincipal = :montantPrincipal")})
public class Atd implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ID")
    private String id;
    @Size(max = 25)
    @Column(name = "ID_COMMANDEMENT")
    private String idCommandement;
    @Size(max = 20)
    @Column(name = "ID_DETENTEUR")
    private String idDetenteur;
    @Size(max = 10)
    @Column(name = "DATE_EMISSION")
    private String dateEmission;
    @Size(max = 100)
    @Column(name = "NUMERO_DOCUMENT")
    private String numeroDocument;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_GLOBAL")
    private BigDecimal montantGlobal;
    @Column(name = "MONTANT_PENALITE")
    private BigDecimal montantPenalite;
    @Column(name = "MONTANT_FONCTIONNEMENT")
    private BigDecimal montantFonctionnement;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "ID_REDEVABLE")
    private String idRedevable;
    @Column(name = "MONTANT_PRINCIPAL")
    private BigDecimal montantPrincipal;

    public Atd() {
    }

    public Atd(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCommandement() {
        return idCommandement;
    }

    public void setIdCommandement(String idCommandement) {
        this.idCommandement = idCommandement;
    }

    public String getIdDetenteur() {
        return idDetenteur;
    }

    public void setIdDetenteur(String idDetenteur) {
        this.idDetenteur = idDetenteur;
    }

    public String getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(String dateEmission) {
        this.dateEmission = dateEmission;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public BigDecimal getMontantGlobal() {
        return montantGlobal;
    }

    public void setMontantGlobal(BigDecimal montantGlobal) {
        this.montantGlobal = montantGlobal;
    }

    public BigDecimal getMontantPenalite() {
        return montantPenalite;
    }

    public void setMontantPenalite(BigDecimal montantPenalite) {
        this.montantPenalite = montantPenalite;
    }

    public BigDecimal getMontantFonctionnement() {
        return montantFonctionnement;
    }

    public void setMontantFonctionnement(BigDecimal montantFonctionnement) {
        this.montantFonctionnement = montantFonctionnement;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getIdRedevable() {
        return idRedevable;
    }

    public void setIdRedevable(String idRedevable) {
        this.idRedevable = idRedevable;
    }

    public BigDecimal getMontantPrincipal() {
        return montantPrincipal;
    }

    public void setMontantPrincipal(BigDecimal montantPrincipal) {
        this.montantPrincipal = montantPrincipal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atd)) {
            return false;
        }
        Atd other = (Atd) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Atd[ id=" + id + " ]";
    }
    
}
