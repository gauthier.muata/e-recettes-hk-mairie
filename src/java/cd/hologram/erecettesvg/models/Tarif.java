/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_TARIF")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarif.findAll", query = "SELECT t FROM Tarif t"),
    @NamedQuery(name = "Tarif.findByCode", query = "SELECT t FROM Tarif t WHERE t.code = :code"),
    @NamedQuery(name = "Tarif.findByIntitule", query = "SELECT t FROM Tarif t WHERE t.intitule = :intitule"),
    @NamedQuery(name = "Tarif.findByValeur", query = "SELECT t FROM Tarif t WHERE t.valeur = :valeur"),
    @NamedQuery(name = "Tarif.findByTypeValeur", query = "SELECT t FROM Tarif t WHERE t.typeValeur = :typeValeur"),
    @NamedQuery(name = "Tarif.findByEtat", query = "SELECT t FROM Tarif t WHERE t.etat = :etat"),
    @NamedQuery(name = "Tarif.findByAgentCreat", query = "SELECT t FROM Tarif t WHERE t.agentCreat = :agentCreat"),
    @NamedQuery(name = "Tarif.findByEstTarifPeage", query = "SELECT t FROM Tarif t WHERE t.estTarifPeage = :estTarifPeage"),
    @NamedQuery(name = "Tarif.findByDateCreat", query = "SELECT t FROM Tarif t WHERE t.dateCreat = :dateCreat"),
    @NamedQuery(name = "Tarif.findByAgentMaj", query = "SELECT t FROM Tarif t WHERE t.agentMaj = :agentMaj"),
    @NamedQuery(name = "Tarif.findByDateMaj", query = "SELECT t FROM Tarif t WHERE t.dateMaj = :dateMaj")})
public class Tarif implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "VALEUR")
    private BigDecimal valeur;
    @Size(max = 1)
    @Column(name = "TYPE_VALEUR")
    private String typeValeur;
    
    @Column(name = "EST_TARIF_PEAGE")
    private int estTarifPeage;
     
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @OneToMany(mappedBy = "tarif")
    private List<Palier> palierList;
    @OneToMany(mappedBy = "tarif")
    private List<ArticleBudgetaire> articleBudgetaireList;
    @OneToMany(mappedBy = "tarif")
    private List<Assujeti> assujetiList;
    @OneToMany(mappedBy = "tarif")
    private List<ServiceTarif> serviceTarifList;
    @OneToMany(mappedBy = "tarif")
    private List<DetailDepotDeclaration> detailDepotDeclarationList;
    @OneToMany(mappedBy = "tarif")
    private List<ParamTauxVignette> paramTauxVignettesList;

    public Tarif() {
    }

    public Tarif(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getEstTarifPeage() {
        return estTarifPeage;
    }

    public void setEstTarifPeage(int estTarifPeage) {
        this.estTarifPeage = estTarifPeage;
    }
    
    

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getTypeValeur() {
        return typeValeur;
    }

    public void setTypeValeur(String typeValeur) {
        this.typeValeur = typeValeur;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @XmlTransient
    public List<Palier> getPalierList() {
        return palierList;
    }

    public void setPalierList(List<Palier> palierList) {
        this.palierList = palierList;
    }

    @XmlTransient
    public List<ArticleBudgetaire> getArticleBudgetaireList() {
        return articleBudgetaireList;
    }

    public void setArticleBudgetaireList(List<ArticleBudgetaire> articleBudgetaireList) {
        this.articleBudgetaireList = articleBudgetaireList;
    }

    @XmlTransient
    public List<Assujeti> getAssujetiList() {
        return assujetiList;
    }

    public void setAssujetiList(List<Assujeti> assujetiList) {
        this.assujetiList = assujetiList;
    }

    @XmlTransient
    public List<ServiceTarif> getServiceTarifList() {
        return serviceTarifList;
    }

    public void setServiceTarifList(List<ServiceTarif> serviceTarifList) {
        this.serviceTarifList = serviceTarifList;
    }

    @XmlTransient
    public List<DetailDepotDeclaration> getDetailDepotDeclarationList() {
        return detailDepotDeclarationList;
    }

    public void setDetailDepotDeclarationList(List<DetailDepotDeclaration> detailDepotDeclarationList) {
        this.detailDepotDeclarationList = detailDepotDeclarationList;
    }

    public List<ParamTauxVignette> getParamTauxVignettesList() {
        return paramTauxVignettesList;
    }

    public void setParamTauxVignettesList(List<ParamTauxVignette> paramTauxVignettesList) {
        this.paramTauxVignettesList = paramTauxVignettesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tarif)) {
            return false;
        }
        Tarif other = (Tarif) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Tarif[ code=" + code + " ]";
    }
    
}
