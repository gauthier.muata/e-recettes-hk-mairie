/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_NOTE_PERCEPTION_MAIRIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotePerceptionMairie.findAll", query = "SELECT n FROM NotePerceptionMairie n"),
    @NamedQuery(name = "NotePerceptionMairie.findById", query = "SELECT n FROM NotePerceptionMairie n WHERE n.id = :id"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkPersonne", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkPersonne = :fkPersonne"),
    @NamedQuery(name = "NotePerceptionMairie.findByVole", query = "SELECT n FROM NotePerceptionMairie n WHERE n.vole = :vole"),
    @NamedQuery(name = "NotePerceptionMairie.findByMontant", query = "SELECT n FROM NotePerceptionMairie n WHERE n.montant = :montant"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkAb", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkAb = :fkAb"),
    @NamedQuery(name = "NotePerceptionMairie.findByDateCreate", query = "SELECT n FROM NotePerceptionMairie n WHERE n.dateCreate = :dateCreate"),
    @NamedQuery(name = "NotePerceptionMairie.findByDateEcheance", query = "SELECT n FROM NotePerceptionMairie n WHERE n.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "NotePerceptionMairie.findByAgentCreate", query = "SELECT n FROM NotePerceptionMairie n WHERE n.agentCreate = :agentCreate"),
    @NamedQuery(name = "NotePerceptionMairie.findByEtat", query = "SELECT n FROM NotePerceptionMairie n WHERE n.etat = :etat"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkBien", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkBien = :fkBien"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkDevise", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkDevise = :fkDevise"),
    @NamedQuery(name = "NotePerceptionMairie.findByNumeroNp", query = "SELECT n FROM NotePerceptionMairie n WHERE n.numeroNp = :numeroNp"),
    @NamedQuery(name = "NotePerceptionMairie.findByNpMere", query = "SELECT n FROM NotePerceptionMairie n WHERE n.npMere = :npMere"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkBanque", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkBanque = :fkBanque"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkCompteBancaire", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkCompteBancaire = :fkCompteBancaire"),
    @NamedQuery(name = "NotePerceptionMairie.findByFkSite", query = "SELECT n FROM NotePerceptionMairie n WHERE n.fkSite = :fkSite")})
public class NotePerceptionMairie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "FK_PERSONNE")
    private String fkPersonne;
    @Size(max = 5)
    @Column(name = "VOLE")
    private String vole;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 50)
    @Column(name = "FK_AB")
    private String fkAb;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "FK_BIEN")
    private String fkBien;
    @Size(max = 5)
    @Column(name = "FK_DEVISE")
    private String fkDevise;
    @Size(max = 20)
    @Column(name = "NUMERO_NP")
    private String numeroNp;
    @Column(name = "NP_MERE")
    private Integer npMere;
    @Size(max = 50)
    @Column(name = "FK_BANQUE")
    private String fkBanque;
    @Size(max = 50)
    @Column(name = "FK_COMPTE_BANCAIRE")
    private String fkCompteBancaire;
    @Size(max = 50)
    @Column(name = "FK_SITE")
    private String fkSite;

    public NotePerceptionMairie() {
    }

    public NotePerceptionMairie(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public String getVole() {
        return vole;
    }

    public void setVole(String vole) {
        this.vole = vole;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getFkAb() {
        return fkAb;
    }

    public void setFkAb(String fkAb) {
        this.fkAb = fkAb;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getFkBien() {
        return fkBien;
    }

    public void setFkBien(String fkBien) {
        this.fkBien = fkBien;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public String getNumeroNp() {
        return numeroNp;
    }

    public void setNumeroNp(String numeroNp) {
        this.numeroNp = numeroNp;
    }

    public Integer getNpMere() {
        return npMere;
    }

    public void setNpMere(Integer npMere) {
        this.npMere = npMere;
    }

    public String getFkBanque() {
        return fkBanque;
    }

    public void setFkBanque(String fkBanque) {
        this.fkBanque = fkBanque;
    }

    public String getFkCompteBancaire() {
        return fkCompteBancaire;
    }

    public void setFkCompteBancaire(String fkCompteBancaire) {
        this.fkCompteBancaire = fkCompteBancaire;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotePerceptionMairie)) {
            return false;
        }
        NotePerceptionMairie other = (NotePerceptionMairie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.NotePerceptionMairie[ id=" + id + " ]";
    }
    
}
