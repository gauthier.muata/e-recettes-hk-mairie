/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_UA_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UaSite.findAll", query = "SELECT u FROM UaSite u"),
    @NamedQuery(name = "UaSite.findById", query = "SELECT u FROM UaSite u WHERE u.id = :id"),
    @NamedQuery(name = "UaSite.findByEtat", query = "SELECT u FROM UaSite u WHERE u.etat = :etat"),
    @NamedQuery(name = "UaSite.findByAgentCreat", query = "SELECT u FROM UaSite u WHERE u.agentCreat = :agentCreat"),
    @NamedQuery(name = "UaSite.findByDateCreat", query = "SELECT u FROM UaSite u WHERE u.dateCreat = :dateCreat"),
    @NamedQuery(name = "UaSite.findByAgentMaj", query = "SELECT u FROM UaSite u WHERE u.agentMaj = :agentMaj"),
    @NamedQuery(name = "UaSite.findByDateMaj", query = "SELECT u FROM UaSite u WHERE u.dateMaj = :dateMaj")})
public class UaSite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @JoinColumn(name = "SITE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Site site;
    @JoinColumn(name = "UA", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Ua ua;

    public UaSite() {
    }

    public UaSite(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Ua getUa() {
        return ua;
    }

    public void setUa(Ua ua) {
        this.ua = ua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UaSite)) {
            return false;
        }
        UaSite other = (UaSite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.UaSite[ id=" + id + " ]";
    }
    
}
