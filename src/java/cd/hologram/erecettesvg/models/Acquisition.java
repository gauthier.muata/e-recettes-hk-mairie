/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ACQUISITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acquisition.findAll", query = "SELECT a FROM Acquisition a"),
    @NamedQuery(name = "Acquisition.findById", query = "SELECT a FROM Acquisition a WHERE a.id = :id"),
    @NamedQuery(name = "Acquisition.findByObservation", query = "SELECT a FROM Acquisition a WHERE a.observation = :observation"),
    @NamedQuery(name = "Acquisition.findByDateAcquisition", query = "SELECT a FROM Acquisition a WHERE a.dateAcquisition = :dateAcquisition"),
    @NamedQuery(name = "Acquisition.findByEtat", query = "SELECT a FROM Acquisition a WHERE a.etat = :etat"),
    @NamedQuery(name = "Acquisition.findByProprietaire", query = "SELECT a FROM Acquisition a WHERE a.proprietaire = :proprietaire"),
    @NamedQuery(name = "Acquisition.findByReferenceContrat", query = "SELECT a FROM Acquisition a WHERE a.referenceContrat = :referenceContrat"),
    @NamedQuery(name = "Acquisition.findByNumActeNotarie", query = "SELECT a FROM Acquisition a WHERE a.numActeNotarie = :numActeNotarie"),
    @NamedQuery(name = "Acquisition.findByDateActeNotarie", query = "SELECT a FROM Acquisition a WHERE a.dateActeNotarie = :dateActeNotarie")})
public class Acquisition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 250)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 10)
    @Column(name = "DATE_ACQUISITION")
    private String dateAcquisition;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "PROPRIETAIRE")
    private Boolean proprietaire;
    @Size(max = 50)
    @Column(name = "REFERENCE_CONTRAT")
    private String referenceContrat;
    @Size(max = 50)
    @Column(name = "NUM_ACTE_NOTARIE")
    private String numActeNotarie;
    @Size(max = 10)
    @Column(name = "DATE_ACTE_NOTARIE")
    private String dateActeNotarie;
    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;

    public Acquisition() {
    }

    public Acquisition(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDateAcquisition() {
        return dateAcquisition;
    }

    public void setDateAcquisition(String dateAcquisition) {
        this.dateAcquisition = dateAcquisition;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Boolean getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Boolean proprietaire) {
        this.proprietaire = proprietaire;
    }

    public String getReferenceContrat() {
        return referenceContrat;
    }

    public void setReferenceContrat(String referenceContrat) {
        this.referenceContrat = referenceContrat;
    }

    public String getNumActeNotarie() {
        return numActeNotarie;
    }

    public void setNumActeNotarie(String numActeNotarie) {
        this.numActeNotarie = numActeNotarie;
    }

    public String getDateActeNotarie() {
        return dateActeNotarie;
    }

    public void setDateActeNotarie(String dateActeNotarie) {
        this.dateActeNotarie = dateActeNotarie;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acquisition)) {
            return false;
        }
        Acquisition other = (Acquisition) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.Acquisition[ id=" + id + " ]";
    }
    
}
