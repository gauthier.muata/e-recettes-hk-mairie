/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_DETAIL_ASSUJETTISSEMENT")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailAssujettissement.findAll", query = "SELECT d FROM DetailAssujettissement d"),
    @NamedQuery(name = "DetailAssujettissement.findById", query = "SELECT d FROM DetailAssujettissement d WHERE d.id = :id"),
    @NamedQuery(name = "DetailAssujettissement.findByTaux", query = "SELECT d FROM DetailAssujettissement d WHERE d.taux = :taux"),

    @NamedQuery(name = "DetailAssujettissement.findByDateCreateExemption", query = "SELECT d FROM DetailAssujettissement d WHERE d.dateCreateExemption = :dateCreateExemption"),
    @NamedQuery(name = "DetailAssujettissement.findByAgentCreateExemption", query = "SELECT d FROM DetailAssujettissement d WHERE d.agentCreateExemption = :agentCreateExemption"),
    @NamedQuery(name = "DetailAssujettissement.findByDateMajExemption", query = "SELECT d FROM DetailAssujettissement d WHERE d.dateMajExemption = :dateMajExemption"),
    @NamedQuery(name = "DetailAssujettissement.findByAgentMajExemption", query = "SELECT d FROM DetailAssujettissement d WHERE d.agentMajExemption = :agentMajExemption"),

    @NamedQuery(name = "DetailAssujettissement.findByFkPersonne", query = "SELECT d FROM DetailAssujettissement d WHERE d.fkPersonne = :fkPersonne"),
    @NamedQuery(name = "DetailAssujettissement.findByEtat", query = "SELECT d FROM DetailAssujettissement d WHERE d.etat = :etat")})
public class DetailAssujettissement implements Serializable {

    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TAUX")
    private BigDecimal taux;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "FK_PERSONNE")
    private String fkPersonne;

    @Column(name = "DATE_CREATE_EXEMPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreateExemption;

    @Column(name = "AGENT_CREATE_EXEMPTION")
    private Integer agentCreateExemption;

    @Column(name = "DATE_MAJ_EXEMPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMajExemption;

    @Column(name = "AGENT_MAJ_EXEMPTION")
    private Integer agentMajExemption;

    @OneToMany(mappedBy = "detailAssujettissement")
    private List<DetailPrevisionCredit> detailPrevisionCreditList;
    @JoinColumn(name = "ASSUJETTISSEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Assujettissement assujettissement;
    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "COMPLEMENT_BIEN", referencedColumnName = "ID")
    @ManyToOne
    private ComplementBien complementBien;
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;

    public DetailAssujettissement() {
    }

    public DetailAssujettissement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public Date getDateCreateExemption() {
        return dateCreateExemption;
    }

    public void setDateCreateExemption(Date dateCreateExemption) {
        this.dateCreateExemption = dateCreateExemption;
    }

    public Integer getAgentCreateExemption() {
        return agentCreateExemption;
    }

    public void setAgentCreateExemption(Integer agentCreateExemption) {
        this.agentCreateExemption = agentCreateExemption;
    }

    public Date getDateMajExemption() {
        return dateMajExemption;
    }

    public void setDateMajExemption(Date dateMajExemption) {
        this.dateMajExemption = dateMajExemption;
    }

    public Integer getAgentMajExemption() {
        return agentMajExemption;
    }

    public void setAgentMajExemption(Integer agentMajExemption) {
        this.agentMajExemption = agentMajExemption;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<DetailPrevisionCredit> getDetailPrevisionCreditList() {
        return detailPrevisionCreditList;
    }

    public void setDetailPrevisionCreditList(List<DetailPrevisionCredit> detailPrevisionCreditList) {
        this.detailPrevisionCreditList = detailPrevisionCreditList;
    }

    public Assujettissement getAssujettissement() {
        return assujettissement;
    }

    public void setAssujettissement(Assujettissement assujettissement) {
        this.assujettissement = assujettissement;
    }

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public ComplementBien getComplementBien() {
        return complementBien;
    }

    public void setComplementBien(ComplementBien complementBien) {
        this.complementBien = complementBien;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailAssujettissement)) {
            return false;
        }
        DetailAssujettissement other = (DetailAssujettissement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.DetailAssujettissement[ id=" + id + " ]";
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

}
