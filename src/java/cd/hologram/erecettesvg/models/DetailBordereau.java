/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DETAIL_BORDEREAU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailBordereau.findAll", query = "SELECT d FROM DetailBordereau d"),
    @NamedQuery(name = "DetailBordereau.findByCode", query = "SELECT d FROM DetailBordereau d WHERE d.code = :code"),
    @NamedQuery(name = "DetailBordereau.findByMontantPercu", query = "SELECT d FROM DetailBordereau d WHERE d.montantPercu = :montantPercu"),
    @NamedQuery(name = "DetailBordereau.findByDevise", query = "SELECT d FROM DetailBordereau d WHERE d.devise = :devise"),
    @NamedQuery(name = "DetailBordereau.findByPeriodicite", query = "SELECT d FROM DetailBordereau d WHERE d.periodicite = :periodicite"),
    @NamedQuery(name = "DetailBordereau.findByStatut", query = "SELECT d FROM DetailBordereau d WHERE d.statut = :statut")})
public class DetailBordereau implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Long code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_PERCU")
    private BigDecimal montantPercu;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;
    @Size(max = 100)
    @Column(name = "PERIODICITE")
    private String periodicite;
    @Column(name = "STATUT")
    private Character statut;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "BORDEREAU", referencedColumnName = "CODE")
    @ManyToOne
    private Bordereau bordereau;

    public DetailBordereau() {
    }

    public DetailBordereau(Long code) {
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public BigDecimal getMontantPercu() {
        return montantPercu;
    }

    public void setMontantPercu(BigDecimal montantPercu) {
        this.montantPercu = montantPercu;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getPeriodicite() {
        return periodicite;
    }

    public void setPeriodicite(String periodicite) {
        this.periodicite = periodicite;
    }

    public Character getStatut() {
        return statut;
    }

    public void setStatut(Character statut) {
        this.statut = statut;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public Bordereau getBordereau() {
        return bordereau;
    }

    public void setBordereau(Bordereau bordereau) {
        this.bordereau = bordereau;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailBordereau)) {
            return false;
        }
        DetailBordereau other = (DetailBordereau) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DetailBordereau[ code=" + code + " ]";
    }
    
}
