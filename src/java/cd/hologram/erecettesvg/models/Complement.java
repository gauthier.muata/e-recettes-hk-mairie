/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_COMPLEMENT")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Complement.findAll", query = "SELECT c FROM Complement c"),
    @NamedQuery(name = "Complement.findById", query = "SELECT c FROM Complement c WHERE c.id = :id"),
    @NamedQuery(name = "Complement.findByValeur", query = "SELECT c FROM Complement c WHERE c.valeur = :valeur"),
    @NamedQuery(name = "Complement.findByEtat", query = "SELECT c FROM Complement c WHERE c.etat = :etat")})
public class Complement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 150)
    @Column(name = "VALEUR")
    private String valeur;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FK_COMPLEMENT_FORME", referencedColumnName = "CODE")
    @ManyToOne
    private ComplementForme fkComplementForme;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;

    public Complement() {
    }

    public Complement(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public ComplementForme getFkComplementForme() {
        return fkComplementForme;
    }

    public void setFkComplementForme(ComplementForme fkComplementForme) {
        this.fkComplementForme = fkComplementForme;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Complement)) {
            return false;
        }
        Complement other = (Complement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Complement[ id=" + id + " ]";
    }
    
}
