/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_DROIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Droit.findAll", query = "SELECT d FROM Droit d"),
    @NamedQuery(name = "Droit.findByCode", query = "SELECT d FROM Droit d WHERE d.code = :code"),
    @NamedQuery(name = "Droit.findByIntitule", query = "SELECT d FROM Droit d WHERE d.intitule = :intitule"),
    @NamedQuery(name = "Droit.findByAgentCreat", query = "SELECT d FROM Droit d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "Droit.findByDateCreat", query = "SELECT d FROM Droit d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "Droit.findByAgentMaj", query = "SELECT d FROM Droit d WHERE d.agentMaj = :agentMaj"),
    @NamedQuery(name = "Droit.findByDateMaj", query = "SELECT d FROM Droit d WHERE d.dateMaj = :dateMaj"),
    @NamedQuery(name = "Droit.findByEtat", query = "SELECT d FROM Droit d WHERE d.etat = :etat"),
    @NamedQuery(name = "Droit.findByFkModule", query = "SELECT d FROM Droit d WHERE d.fkModule = :fkModule")})
public class Droit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @JoinColumn(name = "FK_MODULE", referencedColumnName = "CODE")
    @ManyToOne
    private Module fkModule;   

    public Droit() {
    }

    public Droit(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Module getFkModule() {
        return fkModule;
    }

    public void setFkModule(Module fkModule) {
        this.fkModule = fkModule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Droit)) {
            return false;
        }
        Droit other = (Droit) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Droit[ code=" + code + " ]";
    }
    
}
