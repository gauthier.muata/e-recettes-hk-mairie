///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import javax.persistence.Basic;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//
///**
// *
// * @author moussa.toure
// */
//@Entity
//@Table(name = "T_ASSUJ_AB")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "AssujAb.findAll", query = "SELECT a FROM AssujAb a"),
//    @NamedQuery(name = "AssujAb.findById", query = "SELECT a FROM AssujAb a WHERE a.id = :id"),
//    @NamedQuery(name = "AssujAb.findByTarif", query = "SELECT a FROM AssujAb a WHERE a.tarif = :tarif"),
//    @NamedQuery(name = "AssujAb.findByEtat", query = "SELECT a FROM AssujAb a WHERE a.etat = :etat")})
//public class AssujAb implements Serializable {
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Integer id;
//    @Size(max = 50)
//    @Column(name = "TARIF")
//    private String tarif;
//    @Column(name = "ETAT")
//    private Boolean etat;
//    @JoinColumn(name = "ID_AB", referencedColumnName = "CODE")
//    @ManyToOne
//    private ArticleBudgetaire idAb;
//    @JoinColumn(name = "ID_ASS", referencedColumnName = "ID")
//    @ManyToOne(optional = false)
//    private Assujeti idAss;
//
//    public AssujAb() {
//    }
//
//    public AssujAb(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public String getTarif() {
//        return tarif;
//    }
//
//    public void setTarif(String tarif) {
//        this.tarif = tarif;
//    }
//
//    public Boolean getEtat() {
//        return etat;
//    }
//
//    public void setEtat(Boolean etat) {
//        this.etat = etat;
//    }
//
//    public ArticleBudgetaire getIdAb() {
//        return idAb;
//    }
//
//    public void setIdAb(ArticleBudgetaire idAb) {
//        this.idAb = idAb;
//    }
//
//    public Assujeti getIdAss() {
//        return idAss;
//    }
//
//    public void setIdAss(Assujeti idAss) {
//        this.idAss = idAss;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof AssujAb)) {
//            return false;
//        }
//        AssujAb other = (AssujAb) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.AssujAb[ id=" + id + " ]";
//    }
//    
//}
