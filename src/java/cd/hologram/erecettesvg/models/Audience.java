/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_AUDIENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Audience.findAll", query = "SELECT a FROM Audience a"),
    @NamedQuery(name = "Audience.findByCode", query = "SELECT a FROM Audience a WHERE a.code = :code"),
    @NamedQuery(name = "Audience.findByDossier", query = "SELECT a FROM Audience a WHERE a.dossier = :dossier"),
    @NamedQuery(name = "Audience.findByDateAudience", query = "SELECT a FROM Audience a WHERE a.dateAudience = :dateAudience"),
    @NamedQuery(name = "Audience.findByComposition", query = "SELECT a FROM Audience a WHERE a.composition = :composition"),
    @NamedQuery(name = "Audience.findByObservation", query = "SELECT a FROM Audience a WHERE a.observation = :observation"),
    @NamedQuery(name = "Audience.findByEtat", query = "SELECT a FROM Audience a WHERE a.etat = :etat")})
public class Audience implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 20)
    @Column(name = "DOSSIER")
    private String dossier;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "DATE_AUDIENCE")
    private String dateAudience;
    @Size(max = 20)
    @Column(name = "COMPOSITION")
    private String composition;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "ETAT")
    private Short etat;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "audience1")
    private Audience audience;
    @JoinColumn(name = "CODE", referencedColumnName = "CODE", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Audience audience1;

    public Audience() {
    }

    public Audience(String code) {
        this.code = code;
    }

    public Audience(String code, String dateAudience) {
        this.code = code;
        this.dateAudience = dateAudience;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getDateAudience() {
        return dateAudience;
    }

    public void setDateAudience(String dateAudience) {
        this.dateAudience = dateAudience;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Audience getAudience() {
        return audience;
    }

    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public Audience getAudience1() {
        return audience1;
    }

    public void setAudience1(Audience audience1) {
        this.audience1 = audience1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Audience)) {
            return false;
        }
        Audience other = (Audience) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Audience[ code=" + code + " ]";
    }
    
}
