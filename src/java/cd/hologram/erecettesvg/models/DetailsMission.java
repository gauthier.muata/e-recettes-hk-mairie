///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import javax.persistence.Basic;
//import javax.persistence.Cacheable;
//import javax.persistence.CascadeType;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToOne;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//import javax.xml.bind.annotation.XmlRootElement;
//
///**
// *
// * @author WILLY
// */
//@Entity
//@Cacheable(false)
//@Table(name = "T_DETAILS_MISSION")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "DetailsMission.findAll", query = "SELECT d FROM DetailsMission d"),
//    @NamedQuery(name = "DetailsMission.findById", query = "SELECT d FROM DetailsMission d WHERE d.id = :id")})
//public class DetailsMission implements Serializable {
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Integer id;
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "detailsMission1")
//    private DetailsMission detailsMission;
//    @JoinColumn(name = "ID", referencedColumnName = "ID", insertable = false, updatable = false)
//    @OneToOne(optional = false)
//    private DetailsMission detailsMission1;
//    @JoinColumn(name = "FK_EQUIPE_MISSION", referencedColumnName = "ID")
//    @ManyToOne
//    private EquipeMission fkEquipeMission;
//    @JoinColumn(name = "FK_MISSION", referencedColumnName = "ID")
//    @ManyToOne
//    private Mission fkMission;
//    @JoinColumn(name = "FK_PERIODE_DECLARATION", referencedColumnName = "ID")
//    @ManyToOne
//    private PeriodeDeclaration fkPeriodeDeclaration;
//    @JoinColumn(name = "FK_PV", referencedColumnName = "ID")
//    @ManyToOne
//    private Pv fkPv;
//    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
//    @ManyToOne
//    private Personne fkPersonne;
//
//    public DetailsMission() {
//    }
//
//    public DetailsMission(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public DetailsMission getDetailsMission() {
//        return detailsMission;
//    }
//
//    public void setDetailsMission(DetailsMission detailsMission) {
//        this.detailsMission = detailsMission;
//    }
//
//    public DetailsMission getDetailsMission1() {
//        return detailsMission1;
//    }
//
//    public void setDetailsMission1(DetailsMission detailsMission1) {
//        this.detailsMission1 = detailsMission1;
//    }
//
//    public EquipeMission getFkEquipeMission() {
//        return fkEquipeMission;
//    }
//
//    public void setFkEquipeMission(EquipeMission fkEquipeMission) {
//        this.fkEquipeMission = fkEquipeMission;
//    }
//
//    public Mission getFkMission() {
//        return fkMission;
//    }
//
//    public void setFkMission(Mission fkMission) {
//        this.fkMission = fkMission;
//    }
//
//    public PeriodeDeclaration getFkPeriodeDeclaration() {
//        return fkPeriodeDeclaration;
//    }
//
//    public void setFkPeriodeDeclaration(PeriodeDeclaration fkPeriodeDeclaration) {
//        this.fkPeriodeDeclaration = fkPeriodeDeclaration;
//    }
//
//    public Pv getFkPv() {
//        return fkPv;
//    }
//
//    public void setFkPv(Pv fkPv) {
//        this.fkPv = fkPv;
//    }
//
//    public Personne getFkPersonne() {
//        return fkPersonne;
//    }
//
//    public void setFkPersonne(Personne fkPersonne) {
//        this.fkPersonne = fkPersonne;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof DetailsMission)) {
//            return false;
//        }
//        DetailsMission other = (DetailsMission) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.DetailsMission[ id=" + id + " ]";
//    }
//    
//}
