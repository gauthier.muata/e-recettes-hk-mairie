/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY KASHALA
 */
@Cacheable(false)
@Entity
@Table(name = "T_DETAILS_RECLAMATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsReclamation.findAll", query = "SELECT d FROM DetailsReclamation d"),
    @NamedQuery(name = "DetailsReclamation.findById", query = "SELECT d FROM DetailsReclamation d WHERE d.id = :id"),
    @NamedQuery(name = "DetailsReclamation.findByReferenceDocument", query = "SELECT d FROM DetailsReclamation d WHERE d.referenceDocument = :referenceDocument"),
    @NamedQuery(name = "DetailsReclamation.findByTypeDocument", query = "SELECT d FROM DetailsReclamation d WHERE d.typeDocument = :typeDocument"),
    @NamedQuery(name = "DetailsReclamation.findByDateDecisionSurcis", query = "SELECT d FROM DetailsReclamation d WHERE d.dateDecisionSurcis = :dateDecisionSurcis"),
    @NamedQuery(name = "DetailsReclamation.findByDateTraitement", query = "SELECT d FROM DetailsReclamation d WHERE d.dateTraitement = :dateTraitement"),
    @NamedQuery(name = "DetailsReclamation.findByDecisionSurcis", query = "SELECT d FROM DetailsReclamation d WHERE d.decisionSurcis = :decisionSurcis"),
    @NamedQuery(name = "DetailsReclamation.findByAvecSurcis", query = "SELECT d FROM DetailsReclamation d WHERE d.avecSurcis = :avecSurcis"),
    @NamedQuery(name = "DetailsReclamation.findByFkDecisionJuridique", query = "SELECT d FROM DetailsReclamation d WHERE d.fkDecisionJuridique = :fkDecisionJuridique"),
    @NamedQuery(name = "DetailsReclamation.findByDateTraitementJuridique", query = "SELECT d FROM DetailsReclamation d WHERE d.dateTraitementJuridique = :dateTraitementJuridique"),
    @NamedQuery(name = "DetailsReclamation.findByMontantContester", query = "SELECT d FROM DetailsReclamation d WHERE d.montantContester = :montantContester"),
    @NamedQuery(name = "DetailsReclamation.findByNcDecisionAdmin", query = "SELECT d FROM DetailsReclamation d WHERE d.ncDecisionAdmin = :ncDecisionAdmin"),
    @NamedQuery(name = "DetailsReclamation.findByagentTraitementJuridique", query = "SELECT d FROM DetailsReclamation d WHERE d.agentTraitementJuridique = :agentTraitementJuridique"),
    @NamedQuery(name = "DetailsReclamation.findByNcPartieNonConteste", query = "SELECT d FROM DetailsReclamation d WHERE d.ncPartieNonConteste = :ncPartieNonConteste"),
    @NamedQuery(name = "DetailsReclamation.findByNcDecisionJuridique", query = "SELECT d FROM DetailsReclamation d WHERE d.ncDecisionJuridique = :ncDecisionJuridique"),
    @NamedQuery(name = "DetailsReclamation.findByPourcentageMontantNonContestre", query = "SELECT d FROM DetailsReclamation d WHERE d.pourcentageMontantNonContestre = :pourcentageMontantNonContestre")})
public class DetailsReclamation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
     Integer id;
    @Size(max = 50)
    @Column(name = "REFERENCE_DOCUMENT")
    private String referenceDocument;
    @Size(max = 50)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MOTIF")
    private String motif;
    @Column(name = "DATE_DECISION_SURCIS")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDecisionSurcis;
    @Column(name = "DATE_TRAITEMENT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTraitement;
    @Column(name = "DECISION_SURCIS")
    private Integer decisionSurcis;
    @Column(name = "AVEC_SURCIS")
    private Integer avecSurcis;
    @Column(name = "FK_DECISION_JURIDIQUE")
    private Integer fkDecisionJuridique;
    @Column(name = "DATE_TRAITEMENT_JURIDIQUE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTraitementJuridique;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION_TRAITEMENT")
    private String observationTraitement;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_CONTESTER")
    private BigDecimal montantContester;
    @Size(max = 50)
    @Column(name = "NC_DECISION_ADMIN")
    private String ncDecisionAdmin;
    @Size(max = 50)
    @Column(name = "NC_PARTIE_NON_CONTESTE")
    private String ncPartieNonConteste;
    @Size(max = 50)
    @Column(name = "NC_DECISION_JURIDIQUE")
    private String ncDecisionJuridique;
    @Column(name = "POURCENTAGE_MONTANT_NON_CONTESTRE")
    private BigDecimal pourcentageMontantNonContestre;
    @JoinColumn(name = "AGENT_TRAITEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentTraitement;
    @JoinColumn(name = "FK_DECISION", referencedColumnName = "ID")
    @ManyToOne
    private Decision fkDecision;
    @JoinColumn(name = "FK_RECLAMATION", referencedColumnName = "ID")
    @ManyToOne
    private Reclamation fkReclamation;
    @Column(name = "AGENT_TRAITEMENT_JURIDIQUE")
    private Integer agentTraitementJuridique;

    public DetailsReclamation() {
    }

    public DetailsReclamation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReferenceDocument() {
        return referenceDocument;
    }

    public void setReferenceDocument(String referenceDocument) {
        this.referenceDocument = referenceDocument;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public Date getDateDecisionSurcis() {
        return dateDecisionSurcis;
    }

    public void setDateDecisionSurcis(Date dateDecisionSurcis) {
        this.dateDecisionSurcis = dateDecisionSurcis;
    }

    public Date getDateTraitement() {
        return dateTraitement;
    }

    public void setDateTraitement(Date dateTraitement) {
        this.dateTraitement = dateTraitement;
    }

    public Integer getDecisionSurcis() {
        return decisionSurcis;
    }

    public void setDecisionSurcis(Integer decisionSurcis) {
        this.decisionSurcis = decisionSurcis;
    }

    public Integer getAvecSurcis() {
        return avecSurcis;
    }

    public void setAvecSurcis(Integer avecSurcis) {
        this.avecSurcis = avecSurcis;
    }

    public Integer getFkDecisionJuridique() {
        return fkDecisionJuridique;
    }

    public void setFkDecisionJuridique(Integer fkDecisionJuridique) {
        this.fkDecisionJuridique = fkDecisionJuridique;
    }

    public Date getDateTraitementJuridique() {
        return dateTraitementJuridique;
    }

    public void setDateTraitementJuridique(Date dateTraitementJuridique) {
        this.dateTraitementJuridique = dateTraitementJuridique;
    }

    public String getObservationTraitement() {
        return observationTraitement;
    }

    public void setObservationTraitement(String observationTraitement) {
        this.observationTraitement = observationTraitement;
    }

    public BigDecimal getMontantContester() {
        return montantContester;
    }

    public void setMontantContester(BigDecimal montantContester) {
        this.montantContester = montantContester;
    }

    public String getNcDecisionAdmin() {
        return ncDecisionAdmin;
    }

    public void setNcDecisionAdmin(String ncDecisionAdmin) {
        this.ncDecisionAdmin = ncDecisionAdmin;
    }

    public String getNcPartieNonConteste() {
        return ncPartieNonConteste;
    }

    public void setNcPartieNonConteste(String ncPartieNonConteste) {
        this.ncPartieNonConteste = ncPartieNonConteste;
    }

    public String getNcDecisionJuridique() {
        return ncDecisionJuridique;
    }

    public void setNcDecisionJuridique(String ncDecisionJuridique) {
        this.ncDecisionJuridique = ncDecisionJuridique;
    }

    public Integer getAgentTraitementJuridique() {
        return agentTraitementJuridique;
    }

    public void setAgentTraitementJuridique(Integer agentTraitementJuridique) {
        this.agentTraitementJuridique = agentTraitementJuridique;
    }

    public BigDecimal getPourcentageMontantNonContestre() {
        return pourcentageMontantNonContestre;
    }

    public void setPourcentageMontantNonContestre(BigDecimal pourcentageMontantNonContestre) {
        this.pourcentageMontantNonContestre = pourcentageMontantNonContestre;
    }

    public Agent getAgentTraitement() {
        return agentTraitement;
    }

    public void setAgentTraitement(Agent agentTraitement) {
        this.agentTraitement = agentTraitement;
    }

    public Decision getFkDecision() {
        return fkDecision;
    }

    public void setFkDecision(Decision fkDecision) {
        this.fkDecision = fkDecision;
    }

    public Reclamation getFkReclamation() {
        return fkReclamation;
    }

    public void setFkReclamation(Reclamation fkReclamation) {
        this.fkReclamation = fkReclamation;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsReclamation)) {
            return false;
        }
        DetailsReclamation other = (DetailsReclamation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.DetailsReclamation[ id=" + id + " ]";
    }

}
