/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_BIEN_ACTIVITES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BienActivites.findAll", query = "SELECT a FROM BienActivites a"),
    @NamedQuery(name = "BienActivites.findById", query = "SELECT a FROM BienActivites a WHERE a.id = :id"),
    @NamedQuery(name = "BienActivites.findByEtat", query = "SELECT a FROM BienActivites a WHERE a.etat = :etat")})
public class BienActivites implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "FK_ACTIVITE", referencedColumnName = "ID")
    @ManyToOne
    private Activites fkActivite;
    @JoinColumn(name = "FK_BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien fkBien;

    public BienActivites() {
    }
    public BienActivites(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Activites getFkActivite() {
        return fkActivite;
    }

    public void setFkActivite(Activites fkActivite) {
        this.fkActivite = fkActivite;
    }

    public Bien getFkBien() {
        return fkBien;
    }

    public void setFkBien(Bien fkBien) {
        this.fkBien = fkBien;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsReclamation)) {
            return false;
        }
        DetailsReclamation other = (DetailsReclamation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.BienActivites[ id=" + id + " ]";
    }
}
