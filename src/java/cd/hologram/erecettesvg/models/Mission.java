///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import java.util.Date;
//import java.util.List;
//import javax.persistence.Basic;
//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.Lob;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.OneToMany;
//import javax.persistence.Table;
//import javax.persistence.Temporal;
//import javax.persistence.TemporalType;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//import javax.xml.bind.annotation.XmlTransient;
//
///**
// *
// * @author WILLY
// */
//@Entity
//@Table(name = "T_MISSION")
//@Cacheable(false)
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "Mission.findAll", query = "SELECT m FROM Mission m"),
//    @NamedQuery(name = "Mission.findById", query = "SELECT m FROM Mission m WHERE m.id = :id"),
//    @NamedQuery(name = "Mission.findByDateCreat", query = "SELECT m FROM Mission m WHERE m.dateCreat = :dateCreat"),
//    @NamedQuery(name = "Mission.findByDateDebut", query = "SELECT m FROM Mission m WHERE m.dateDebut = :dateDebut"),
//    @NamedQuery(name = "Mission.findByDateFin", query = "SELECT m FROM Mission m WHERE m.dateFin = :dateFin"),
//    @NamedQuery(name = "Mission.findByTypeMission", query = "SELECT m FROM Mission m WHERE m.typeMission = :typeMission"),
//    @NamedQuery(name = "Mission.findByEtat", query = "SELECT m FROM Mission m WHERE m.etat = :etat"),
//    @NamedQuery(name = "Mission.findByObservationValidation", query = "SELECT m FROM Mission m WHERE m.observationValidation = :observationValidation"),
//    @NamedQuery(name = "Mission.findByAgentValidation", query = "SELECT m FROM Mission m WHERE m.agentValidation = :agentValidation"),
//    @NamedQuery(name = "Mission.findByNumeroMission", query = "SELECT m FROM Mission m WHERE m.numeroMission = :numeroMission")})
//public class Mission implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Size(min = 1, max = 50)
//    @Column(name = "ID")
//    private String id;
//    @Column(name = "DATE_CREAT")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dateCreat;
//    @Column(name = "DATE_DEBUT")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dateDebut;
//    @Column(name = "DATE_FIN")
//    @Temporal(TemporalType.TIMESTAMP)
//    private Date dateFin;
//    @Column(name = "TYPE_MISSION")
//    private Integer typeMission;
//    @Column(name = "ETAT")
//    private Integer etat;
//
//    @Column(name = "AGENT_VALIDATION")
//    private Integer agentValidation;
//    @Lob
//    @Size(max = 2147483647)
//    @Column(name = "OBSERVATION")
//    private String observation;
//    @Size(max = 50)
//    
//    @Column(name = "OBSERVATION_VALIDATION")
//    private String observationValidation;
//
//    @Column(name = "NUMERO_MISSION")
//    private String numeroMission;
//    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
//    @ManyToOne
//    private Agent agentCreat;
//    @OneToMany(mappedBy = "fkMission")
//    private List<EquipeMission> equipeMissionList;
//    @OneToMany(mappedBy = "fkMission")
//    private List<DetailsMission> detailsMissionList;
//
//    public Mission() {
//    }
//
//    public Mission(String id) {
//        this.id = id;
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public Date getDateCreat() {
//        return dateCreat;
//    }
//
//    public void setDateCreat(Date dateCreat) {
//        this.dateCreat = dateCreat;
//    }
//
//    public Integer getAgentValidation() {
//        return agentValidation;
//    }
//
//    public void setAgentValidation(Integer agentValidation) {
//        this.agentValidation = agentValidation;
//    }
//    
//    
//    
//
//    public Date getDateDebut() {
//        return dateDebut;
//    }
//
//    public void setDateDebut(Date dateDebut) {
//        this.dateDebut = dateDebut;
//    }
//
//    public Date getDateFin() {
//        return dateFin;
//    }
//
//    public void setDateFin(Date dateFin) {
//        this.dateFin = dateFin;
//    }
//
//    public Integer getTypeMission() {
//        return typeMission;
//    }
//
//    public void setTypeMission(Integer typeMission) {
//        this.typeMission = typeMission;
//    }
//
//    public Integer getEtat() {
//        return etat;
//    }
//
//    public void setEtat(Integer etat) {
//        this.etat = etat;
//    }
//
//    public String getObservation() {
//        return observation;
//    }
//
//    public void setObservation(String observation) {
//        this.observation = observation;
//    }
//
//    public String getObservationValidation() {
//        return observationValidation;
//    }
//
//    public void setObservationValidation(String observationValidation) {
//        this.observationValidation = observationValidation;
//    }
//
//    public String getNumeroMission() {
//        return numeroMission;
//    }
//
//    public void setNumeroMission(String numeroMission) {
//        this.numeroMission = numeroMission;
//    }
//
//    public Agent getAgentCreat() {
//        return agentCreat;
//    }
//
//    public void setAgentCreat(Agent agentCreat) {
//        this.agentCreat = agentCreat;
//    }
//
//    @XmlTransient
//    public List<EquipeMission> getEquipeMissionList() {
//        return equipeMissionList;
//    }
//
//    public void setEquipeMissionList(List<EquipeMission> equipeMissionList) {
//        this.equipeMissionList = equipeMissionList;
//    }
//
//    @XmlTransient
//    public List<DetailsMission> getDetailsMissionList() {
//        return detailsMissionList;
//    }
//
//    public void setDetailsMissionList(List<DetailsMission> detailsMissionList) {
//        this.detailsMissionList = detailsMissionList;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Mission)) {
//            return false;
//        }
//        Mission other = (Mission) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.Mission[ id=" + id + " ]";
//    }
//
//}
