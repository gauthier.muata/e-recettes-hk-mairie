/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PALIER_TAUX_PENALITE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PalierTauxPenalite.findAll", query = "SELECT p FROM PalierTauxPenalite p"),
    @NamedQuery(name = "PalierTauxPenalite.findById", query = "SELECT p FROM PalierTauxPenalite p WHERE p.id = :id"),
    @NamedQuery(name = "PalierTauxPenalite.findByBorneInferieure", query = "SELECT p FROM PalierTauxPenalite p WHERE p.borneInferieure = :borneInferieure"),
    @NamedQuery(name = "PalierTauxPenalite.findByBorneSuperieure", query = "SELECT p FROM PalierTauxPenalite p WHERE p.borneSuperieure = :borneSuperieure"),
    @NamedQuery(name = "PalierTauxPenalite.findByNatureArticleBudgetaire", query = "SELECT p FROM PalierTauxPenalite p WHERE p.natureArticleBudgetaire = :natureArticleBudgetaire"),
    @NamedQuery(name = "PalierTauxPenalite.findByRecidive", query = "SELECT p FROM PalierTauxPenalite p WHERE p.recidive = :recidive"),
    @NamedQuery(name = "PalierTauxPenalite.findByValeur", query = "SELECT p FROM PalierTauxPenalite p WHERE p.valeur = :valeur"),
    @NamedQuery(name = "PalierTauxPenalite.findByTypeValeur", query = "SELECT p FROM PalierTauxPenalite p WHERE p.typeValeur = :typeValeur"),
    @NamedQuery(name = "PalierTauxPenalite.findByEstPourcentage", query = "SELECT p FROM PalierTauxPenalite p WHERE p.estPourcentage = :estPourcentage"),
    @NamedQuery(name = "PalierTauxPenalite.findByNature", query = "SELECT p FROM PalierTauxPenalite p WHERE p.nature = :nature"),
    @NamedQuery(name = "PalierTauxPenalite.findByEtat", query = "SELECT p FROM PalierTauxPenalite p WHERE p.etat = :etat")})
public class PalierTauxPenalite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BORNE_INFERIEURE")
    private BigDecimal borneInferieure;
    @Column(name = "BORNE_SUPERIEURE")
    private BigDecimal borneSuperieure;
    @Size(max = 20)
    @Column(name = "NATURE_ARTICLE_BUDGETAIRE")
    private String natureArticleBudgetaire;
    @Column(name = "RECIDIVE")
    private Boolean recidive;
    @Column(name = "VALEUR")
    private BigDecimal valeur;
    @Size(max = 1)
    @Column(name = "TYPE_VALEUR")
    private String typeValeur;
    @Column(name = "EST_POURCENTAGE")
    private Boolean estPourcentage;
    @Size(max = 20)
    @Column(name = "NATURE")
    private String nature;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FORME_JURIDIQUE", referencedColumnName = "CODE")
    @ManyToOne
    private FormeJuridique formeJuridique;
    @JoinColumn(name = "PENALITE", referencedColumnName = "CODE")
    @ManyToOne
    private Penalite penalite;

    public PalierTauxPenalite() {
    }

    public PalierTauxPenalite(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBorneInferieure() {
        return borneInferieure;
    }

    public void setBorneInferieure(BigDecimal borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

    public BigDecimal getBorneSuperieure() {
        return borneSuperieure;
    }

    public void setBorneSuperieure(BigDecimal borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public String getNatureArticleBudgetaire() {
        return natureArticleBudgetaire;
    }

    public void setNatureArticleBudgetaire(String natureArticleBudgetaire) {
        this.natureArticleBudgetaire = natureArticleBudgetaire;
    }

    public Boolean getRecidive() {
        return recidive;
    }

    public void setRecidive(Boolean recidive) {
        this.recidive = recidive;
    }

    public BigDecimal getValeur() {
        return valeur;
    }

    public void setValeur(BigDecimal valeur) {
        this.valeur = valeur;
    }

    public String getTypeValeur() {
        return typeValeur;
    }

    public void setTypeValeur(String typeValeur) {
        this.typeValeur = typeValeur;
    }

    public Boolean getEstPourcentage() {
        return estPourcentage;
    }

    public void setEstPourcentage(Boolean estPourcentage) {
        this.estPourcentage = estPourcentage;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public FormeJuridique getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(FormeJuridique formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public Penalite getPenalite() {
        return penalite;
    }

    public void setPenalite(Penalite penalite) {
        this.penalite = penalite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PalierTauxPenalite)) {
            return false;
        }
        PalierTauxPenalite other = (PalierTauxPenalite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.PalierTauxPenalite[ id=" + id + " ]";
    }
    
}
