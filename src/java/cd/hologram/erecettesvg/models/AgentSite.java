/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_AGENT_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentSite.findAll", query = "SELECT a FROM AgentSite a"),
    @NamedQuery(name = "AgentSite.findByCode", query = "SELECT a FROM AgentSite a WHERE a.code = :code"),
    @NamedQuery(name = "AgentSite.findByEtat", query = "SELECT a FROM AgentSite a WHERE a.etat = :etat")})
public class AgentSite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "CODEAGENT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent codeagent;
    @JoinColumn(name = "CODESITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site codesite;

    public AgentSite() {
    }

    public AgentSite(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Agent getCodeagent() {
        return codeagent;
    }

    public void setCodeagent(Agent codeagent) {
        this.codeagent = codeagent;
    }

    public Site getCodesite() {
        return codesite;
    }

    public void setCodesite(Site codesite) {
        this.codesite = codesite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentSite)) {
            return false;
        }
        AgentSite other = (AgentSite) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AgentSite[ code=" + code + " ]";
    }
    
}
