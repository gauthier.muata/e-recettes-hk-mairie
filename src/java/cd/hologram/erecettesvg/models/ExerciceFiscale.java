/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_EXERCICE_FISCALE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ExerciceFiscale.findAll", query = "SELECT e FROM ExerciceFiscale e"),
    @NamedQuery(name = "ExerciceFiscale.findByCode", query = "SELECT e FROM ExerciceFiscale e WHERE e.code = :code"),
    @NamedQuery(name = "ExerciceFiscale.findByIntitule", query = "SELECT e FROM ExerciceFiscale e WHERE e.intitule = :intitule"),
    @NamedQuery(name = "ExerciceFiscale.findByDebut", query = "SELECT e FROM ExerciceFiscale e WHERE e.debut = :debut"),
    @NamedQuery(name = "ExerciceFiscale.findByFin", query = "SELECT e FROM ExerciceFiscale e WHERE e.fin = :fin"),
    @NamedQuery(name = "ExerciceFiscale.findByEtat", query = "SELECT e FROM ExerciceFiscale e WHERE e.etat = :etat"),
    @NamedQuery(name = "ExerciceFiscale.findByAgentCreat", query = "SELECT e FROM ExerciceFiscale e WHERE e.agentCreat = :agentCreat"),
    @NamedQuery(name = "ExerciceFiscale.findByDateCreat", query = "SELECT e FROM ExerciceFiscale e WHERE e.dateCreat = :dateCreat"),
    @NamedQuery(name = "ExerciceFiscale.findByAgentMaj", query = "SELECT e FROM ExerciceFiscale e WHERE e.agentMaj = :agentMaj"),
    @NamedQuery(name = "ExerciceFiscale.findByDateMaj", query = "SELECT e FROM ExerciceFiscale e WHERE e.dateMaj = :dateMaj")})
public class ExerciceFiscale implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date debut;
    @Column(name = "FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fin;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public ExerciceFiscale() {
    }

    public ExerciceFiscale(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExerciceFiscale)) {
            return false;
        }
        ExerciceFiscale other = (ExerciceFiscale) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ExerciceFiscale[ code=" + code + " ]";
    }
    
}
