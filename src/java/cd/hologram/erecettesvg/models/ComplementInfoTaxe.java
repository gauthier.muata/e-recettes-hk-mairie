/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_COMPLEMENT_INFO_TAXE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComplementInfoTaxe.findAll", query = "SELECT l FROM ComplementInfoTaxe l"),
    @NamedQuery(name = "ComplementInfoTaxe.findById", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.id = :id"),
    @NamedQuery(name = "ComplementInfoTaxe.findByTransporteur", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.transporteur = :transporteur"),
    @NamedQuery(name = "ComplementInfoTaxe.findByNatureProduit", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.natureProduit = :natureProduit"),
    @NamedQuery(name = "ComplementInfoTaxe.findByNumeroPlaque", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.numeroPlaque = :numeroPlaque"),
    @NamedQuery(name = "ComplementInfoTaxe.findByModePaiement", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.modePaiement = :modePaiement"),
    @NamedQuery(name = "ComplementInfoTaxe.findByFkNoteCalcul", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.fkNoteCalcul = :fkNoteCalcul"),
    @NamedQuery(name = "ComplementInfoTaxe.findByFkNotePerception", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.fkNotePerception = :fkNotePerception"),
    @NamedQuery(name = "ComplementInfoTaxe.findByEtat", query = "SELECT l FROM ComplementInfoTaxe l WHERE l.etat = :etat")})

public class ComplementInfoTaxe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "TRANSPORTEUR")
    private String transporteur;

    @Column(name = "NATURE_PRODUIT")
    private String natureProduit;

    @Column(name = "NUMERO_PLAQUE")
    private String numeroPlaque;

    @Column(name = "MODE_PAIEMENT")
    private String modePaiement;

    @Column(name = "FK_NOTE_CALCUL")
    private String fkNoteCalcul;

    @Column(name = "FK_NOTE_PERCEPTION")
    private String fkNotePerception;

    @Column(name = "ETAT")
    private Integer etat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransporteur() {
        return transporteur;
    }

    public void setTransporteur(String transporteur) {
        this.transporteur = transporteur;
    }

    public String getNatureProduit() {
        return natureProduit;
    }

    public void setNatureProduit(String natureProduit) {
        this.natureProduit = natureProduit;
    }

    public String getNumeroPlaque() {
        return numeroPlaque;
    }

    public void setNumeroPlaque(String numeroPlaque) {
        this.numeroPlaque = numeroPlaque;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    public String getFkNoteCalcul() {
        return fkNoteCalcul;
    }

    public void setFkNoteCalcul(String fkNoteCalcul) {
        this.fkNoteCalcul = fkNoteCalcul;
    }

    public String getFkNotePerception() {
        return fkNotePerception;
    }

    public void setFkNotePerception(String fkNotePerception) {
        this.fkNotePerception = fkNotePerception;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }
    
    
    
    
    
    

    public ComplementInfoTaxe() {
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComplementInfoTaxe)) {
            return false;
        }
        ComplementInfoTaxe other = (ComplementInfoTaxe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ComplementInfoTaxe[ id=" + id + " ]";
    }
}
