/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_GROUP_USER")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GroupUser.findAll", query = "SELECT g FROM GroupUser g"),
    @NamedQuery(name = "GroupUser.findById", query = "SELECT g FROM GroupUser g WHERE g.id = :id"),
    @NamedQuery(name = "GroupUser.findByIntitule", query = "SELECT g FROM GroupUser g WHERE g.intitule = :intitule"),
    @NamedQuery(name = "GroupUser.findByAgentCreat", query = "SELECT g FROM GroupUser g WHERE g.agentCreat = :agentCreat"),
    @NamedQuery(name = "GroupUser.findByDateCreat", query = "SELECT g FROM GroupUser g WHERE g.dateCreat = :dateCreat"),
    @NamedQuery(name = "GroupUser.findByAgentMaj", query = "SELECT g FROM GroupUser g WHERE g.agentMaj = :agentMaj"),
    @NamedQuery(name = "GroupUser.findByDateMaj", query = "SELECT g FROM GroupUser g WHERE g.dateMaj = :dateMaj"),
    @NamedQuery(name = "GroupUser.findByEtat", query = "SELECT g FROM GroupUser g WHERE g.etat = :etat"),
    @NamedQuery(name = "GroupUser.findByDescription", query = "SELECT g FROM GroupUser g WHERE g.description = :description")})
public class GroupUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 250)
    @Column(name = "DESCRIPTION")
    private String description;
    @OneToMany(mappedBy = "fkGu")
    private List<AgentGu> agentGuList;

    public GroupUser() {
    }

    public GroupUser(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public List<AgentGu> getAgentGuList() {
        return agentGuList;
    }

    public void setAgentGuList(List<AgentGu> agentGuList) {
        this.agentGuList = agentGuList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupUser)) {
            return false;
        }
        GroupUser other = (GroupUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.GroupUser[ id=" + id + " ]";
    }
    
}
