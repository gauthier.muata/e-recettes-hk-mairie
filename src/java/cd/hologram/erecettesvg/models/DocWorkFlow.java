/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_DOC_WORK_FLOW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DocWorkFlow.findAll", query = "SELECT d FROM DocWorkFlow d"),
    @NamedQuery(name = "DocWorkFlow.findById", query = "SELECT d FROM DocWorkFlow d WHERE d.id = :id"),
    @NamedQuery(name = "DocWorkFlow.findByOrdre", query = "SELECT d FROM DocWorkFlow d WHERE d.ordre = :ordre"),
    @NamedQuery(name = "DocWorkFlow.findByDocument", query = "SELECT d FROM DocWorkFlow d WHERE d.document = :document"),
    @NamedQuery(name = "DocWorkFlow.findByWorkFlow", query = "SELECT d FROM DocWorkFlow d WHERE d.workFlow = :workFlow"),
    @NamedQuery(name = "DocWorkFlow.findByObligatoire", query = "SELECT d FROM DocWorkFlow d WHERE d.obligatoire = :obligatoire"),
    @NamedQuery(name = "DocWorkFlow.findByArchivageObligatoire", query = "SELECT d FROM DocWorkFlow d WHERE d.archivageObligatoire = :archivageObligatoire"),
    @NamedQuery(name = "DocWorkFlow.findByProcess", query = "SELECT d FROM DocWorkFlow d WHERE d.process = :process"),
    @NamedQuery(name = "DocWorkFlow.findByUa", query = "SELECT d FROM DocWorkFlow d WHERE d.ua = :ua"),
    @NamedQuery(name = "DocWorkFlow.findByAgentCreat", query = "SELECT d FROM DocWorkFlow d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "DocWorkFlow.findByDateCreat", query = "SELECT d FROM DocWorkFlow d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "DocWorkFlow.findByAgentMaj", query = "SELECT d FROM DocWorkFlow d WHERE d.agentMaj = :agentMaj"),
    @NamedQuery(name = "DocWorkFlow.findByDateMaj", query = "SELECT d FROM DocWorkFlow d WHERE d.dateMaj = :dateMaj"),
    @NamedQuery(name = "DocWorkFlow.findByEtat", query = "SELECT d FROM DocWorkFlow d WHERE d.etat = :etat")})
public class DocWorkFlow implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 3)
    @Column(name = "ORDRE")
    private String ordre;
    @Size(max = 25)
    @Column(name = "DOCUMENT")
    private String document;
    @Column(name = "WORK_FLOW")
    private BigInteger workFlow;
    @Column(name = "OBLIGATOIRE")
    private Short obligatoire;
    @Column(name = "ARCHIVAGE_OBLIGATOIRE")
    private Short archivageObligatoire;
    @Size(max = 25)
    @Column(name = "PROCESS")
    private String process;
    @Size(max = 25)
    @Column(name = "UA")
    private String ua;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public DocWorkFlow() {
    }

    public DocWorkFlow(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public BigInteger getWorkFlow() {
        return workFlow;
    }

    public void setWorkFlow(BigInteger workFlow) {
        this.workFlow = workFlow;
    }

    public Short getObligatoire() {
        return obligatoire;
    }

    public void setObligatoire(Short obligatoire) {
        this.obligatoire = obligatoire;
    }

    public Short getArchivageObligatoire() {
        return archivageObligatoire;
    }

    public void setArchivageObligatoire(Short archivageObligatoire) {
        this.archivageObligatoire = archivageObligatoire;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocWorkFlow)) {
            return false;
        }
        DocWorkFlow other = (DocWorkFlow) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.DocWorkFlow[ id=" + id + " ]";
    }
    
}
