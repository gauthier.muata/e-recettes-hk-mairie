/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PERSONNE_COMPOSITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonneComposition.findAll", query = "SELECT p FROM PersonneComposition p"),
    @NamedQuery(name = "PersonneComposition.findByCode", query = "SELECT p FROM PersonneComposition p WHERE p.code = :code"),
    @NamedQuery(name = "PersonneComposition.findByPersonne", query = "SELECT p FROM PersonneComposition p WHERE p.personne = :personne"),
    @NamedQuery(name = "PersonneComposition.findByComposition", query = "SELECT p FROM PersonneComposition p WHERE p.composition = :composition"),
    @NamedQuery(name = "PersonneComposition.findByFonction", query = "SELECT p FROM PersonneComposition p WHERE p.fonction = :fonction"),
    @NamedQuery(name = "PersonneComposition.findByEtat", query = "SELECT p FROM PersonneComposition p WHERE p.etat = :etat"),
    @NamedQuery(name = "PersonneComposition.findByNumOrdre", query = "SELECT p FROM PersonneComposition p WHERE p.numOrdre = :numOrdre")})
public class PersonneComposition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Size(max = 20)
    @Column(name = "COMPOSITION")
    private String composition;
    @Size(max = 20)
    @Column(name = "FONCTION")
    private String fonction;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "NUM_ORDRE")
    private Integer numOrdre;

    public PersonneComposition() {
    }

    public PersonneComposition(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Integer getNumOrdre() {
        return numOrdre;
    }

    public void setNumOrdre(Integer numOrdre) {
        this.numOrdre = numOrdre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonneComposition)) {
            return false;
        }
        PersonneComposition other = (PersonneComposition) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.PersonneComposition[ code=" + code + " ]";
    }
    
}
