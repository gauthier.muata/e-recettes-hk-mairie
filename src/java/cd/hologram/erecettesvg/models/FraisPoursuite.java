/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_FRAIS_POURSUITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FraisPoursuite.findAll", query = "SELECT f FROM FraisPoursuite f"),
    @NamedQuery(name = "FraisPoursuite.findByCode", query = "SELECT f FROM FraisPoursuite f WHERE f.code = :code"),
    @NamedQuery(name = "FraisPoursuite.findByContrainte", query = "SELECT f FROM FraisPoursuite f WHERE f.contrainte = :contrainte"),
    @NamedQuery(name = "FraisPoursuite.findByCommandement", query = "SELECT f FROM FraisPoursuite f WHERE f.commandement = :commandement"),
    @NamedQuery(name = "FraisPoursuite.findByMontant", query = "SELECT f FROM FraisPoursuite f WHERE f.montant = :montant"),
    @NamedQuery(name = "FraisPoursuite.findByDateCreat", query = "SELECT f FROM FraisPoursuite f WHERE f.dateCreat = :dateCreat")})
public class FraisPoursuite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "CONTRAINTE")
    private String contrainte;
    @Size(max = 25)
    @Column(name = "COMMANDEMENT")
    private String commandement;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @JoinColumn(name = "AMR1", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr amr1;
    @JoinColumn(name = "AMR2", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr amr2;

    public FraisPoursuite() {
    }

    public FraisPoursuite(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContrainte() {
        return contrainte;
    }

    public void setContrainte(String contrainte) {
        this.contrainte = contrainte;
    }

    public String getCommandement() {
        return commandement;
    }

    public void setCommandement(String commandement) {
        this.commandement = commandement;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Amr getAmr1() {
        return amr1;
    }

    public void setAmr1(Amr amr1) {
        this.amr1 = amr1;
    }

    public Amr getAmr2() {
        return amr2;
    }

    public void setAmr2(Amr amr2) {
        this.amr2 = amr2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FraisPoursuite)) {
            return false;
        }
        FraisPoursuite other = (FraisPoursuite) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.FraisPoursuite[ code=" + code + " ]";
    }
    
}
