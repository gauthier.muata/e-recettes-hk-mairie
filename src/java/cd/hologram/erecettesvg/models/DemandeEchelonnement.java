/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_DEMANDE_ECHELONNEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DemandeEchelonnement.findAll", query = "SELECT d FROM DemandeEchelonnement d"),
    @NamedQuery(name = "DemandeEchelonnement.findByCode", query = "SELECT d FROM DemandeEchelonnement d WHERE d.code = :code"),
    @NamedQuery(name = "DemandeEchelonnement.findByDateCreat", query = "SELECT d FROM DemandeEchelonnement d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "DemandeEchelonnement.findByDateValidation", query = "SELECT d FROM DemandeEchelonnement d WHERE d.dateValidation = :dateValidation"),
    @NamedQuery(name = "DemandeEchelonnement.findByAgentValidation", query = "SELECT d FROM DemandeEchelonnement d WHERE d.agentValidation = :agentValidation"),
    @NamedQuery(name = "DemandeEchelonnement.findByAgentCreat", query = "SELECT d FROM DemandeEchelonnement d WHERE d.agentCreat = :agentCreat"),
    @NamedQuery(name = "DemandeEchelonnement.findByReferenceLettre", query = "SELECT d FROM DemandeEchelonnement d WHERE d.referenceLettre = :referenceLettre"),
    @NamedQuery(name = "DemandeEchelonnement.findByEtat", query = "SELECT d FROM DemandeEchelonnement d WHERE d.etat = :etat")})
public class DemandeEchelonnement implements Serializable {
    @OneToMany(mappedBy = "fkDemandeEchelonnement")
    private List<DetailEchelonnement> detailEchelonnementList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;

    @Column(name = "DATE_VALIDATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateValidation;

    @Column(name = "AGENT_CREAT")
    private Integer agentCreat;
    @Size(max = 50)
    @Column(name = "REFERENCE_LETTRE")
    private String referenceLettre;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;

    @Size(max = 2147483647)
    @Column(name = "OBSERVATION_VALIDATION")
    private String observationValidation;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "AGENT_VALIDATION")
    private Integer agentValidation;

    @JoinColumn(name = "FK_NP", referencedColumnName = "NUMERO")
    @ManyToOne
    private NotePerception fkNp;

    public DemandeEchelonnement() {
    }

    public DemandeEchelonnement(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getObservationValidation() {
        return observationValidation;
    }

    public void setObservationValidation(String observationValidation) {
        this.observationValidation = observationValidation;
    }

    public Integer getAgentValidation() {
        return agentValidation;
    }

    public void setAgentValidation(Integer agentValidation) {
        this.agentValidation = agentValidation;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getReferenceLettre() {
        return referenceLettre;
    }

    public void setReferenceLettre(String referenceLettre) {
        this.referenceLettre = referenceLettre;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public NotePerception getFkNp() {
        return fkNp;
    }

    public void setFkNp(NotePerception fkNp) {
        this.fkNp = fkNp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DemandeEchelonnement)) {
            return false;
        }
        DemandeEchelonnement other = (DemandeEchelonnement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.DemandeEchelonnement[ code=" + code + " ]";
    }

    @XmlTransient
    public List<DetailEchelonnement> getDetailEchelonnementList() {
        return detailEchelonnementList;
    }

    public void setDetailEchelonnementList(List<DetailEchelonnement> detailEchelonnementList) {
        this.detailEchelonnementList = detailEchelonnementList;
    }

}
