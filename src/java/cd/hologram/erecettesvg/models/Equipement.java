/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_EQUIPEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Equipement.findAll", query = "SELECT e FROM Equipement e"),
    @NamedQuery(name = "Equipement.findById", query = "SELECT e FROM Equipement e WHERE e.id = :id"),
    @NamedQuery(name = "Equipement.findBySn", query = "SELECT e FROM Equipement e WHERE e.sn = :sn"),
    @NamedQuery(name = "Equipement.findByMarque", query = "SELECT e FROM Equipement e WHERE e.marque = :marque"),
    @NamedQuery(name = "Equipement.findByModel", query = "SELECT e FROM Equipement e WHERE e.model = :model"),
    @NamedQuery(name = "Equipement.findByMacAdresse", query = "SELECT e FROM Equipement e WHERE e.macAdresse = :macAdresse"),
    @NamedQuery(name = "Equipement.findByIpAdresse", query = "SELECT e FROM Equipement e WHERE e.ipAdresse = :ipAdresse"),
    @NamedQuery(name = "Equipement.findByDescription", query = "SELECT e FROM Equipement e WHERE e.description = :description"),
    @NamedQuery(name = "Equipement.findByAgentCreat", query = "SELECT e FROM Equipement e WHERE e.agentCreat = :agentCreat"),
    @NamedQuery(name = "Equipement.findByDateCreat", query = "SELECT e FROM Equipement e WHERE e.dateCreat = :dateCreat"),
    @NamedQuery(name = "Equipement.findByEtat", query = "SELECT e FROM Equipement e WHERE e.etat = :etat")})
public class Equipement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "SN")
    private String sn;
    @Size(max = 50)
    @Column(name = "MARQUE")
    private String marque;
    @Size(max = 50)
    @Column(name = "MODEL")
    private String model;
    @Size(max = 50)
    @Column(name = "MAC_ADRESSE")
    private String macAdresse;
    @Size(max = 50)
    @Column(name = "IP_ADRESSE")
    private String ipAdresse;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 50)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;
    @OneToMany(mappedBy = "fkEquipement")
    private List<ProfilEquipement> profilEquipementList;

    public Equipement() {
    }

    public Equipement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMacAdresse() {
        return macAdresse;
    }

    public void setMacAdresse(String macAdresse) {
        this.macAdresse = macAdresse;
    }

    public String getIpAdresse() {
        return ipAdresse;
    }

    public void setIpAdresse(String ipAdresse) {
        this.ipAdresse = ipAdresse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    @XmlTransient
    public List<ProfilEquipement> getProfilEquipementList() {
        return profilEquipementList;
    }

    public void setProfilEquipementList(List<ProfilEquipement> profilEquipementList) {
        this.profilEquipementList = profilEquipementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipement)) {
            return false;
        }
        Equipement other = (Equipement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Equipement[ id=" + id + " ]";
    }
    
}
