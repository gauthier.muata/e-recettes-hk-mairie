/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_SERVICE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Service.findAll", query = "SELECT s FROM Service s"),
    @NamedQuery(name = "Service.findByCode", query = "SELECT s FROM Service s WHERE s.code = :code"),
    @NamedQuery(name = "Service.findByIntitule", query = "SELECT s FROM Service s WHERE s.intitule = :intitule"),
    @NamedQuery(name = "Service.findByEtat", query = "SELECT s FROM Service s WHERE s.etat = :etat")})
public class Service implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @OneToMany(mappedBy = "service")
    private List<Service> serviceList;
    @JoinColumn(name = "SERVICE", referencedColumnName = "CODE")
    @ManyToOne
    private Service service;
    @OneToMany(mappedBy = "service")
    private List<TypeBienService> typeBienServiceList;

    public Service() {
    }

    public Service(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @XmlTransient
    public List<TypeBienService> getTypeBienServiceList() {
        return typeBienServiceList;
    }

    public void setTypeBienServiceList(List<TypeBienService> typeBienServiceList) {
        this.typeBienServiceList = typeBienServiceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Service)) {
            return false;
        }
        Service other = (Service) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Service[ code=" + code + " ]";
    }
    
}
