/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_REPARTITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Repartition.findAll", query = "SELECT r FROM Repartition r"),
    @NamedQuery(name = "Repartition.findByCode", query = "SELECT r FROM Repartition r WHERE r.code = :code"),
    @NamedQuery(name = "Repartition.findByMontant", query = "SELECT r FROM Repartition r WHERE r.montant = :montant")})
public class Repartition implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @JoinColumn(name = "DETAILS_NC", referencedColumnName = "ID")
    @ManyToOne
    private DetailsNc detailsNc;
    @JoinColumn(name = "RETRIBUTION", referencedColumnName = "CODE")
    @ManyToOne
    private Retribution retribution;

    public Repartition() {
    }

    public Repartition(String code) {
        this.code = code;
    }

    public Repartition(String code, BigDecimal montant) {
        this.code = code;
        this.montant = montant;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public DetailsNc getDetailsNc() {
        return detailsNc;
    }

    public void setDetailsNc(DetailsNc detailsNc) {
        this.detailsNc = detailsNc;
    }

    public Retribution getRetribution() {
        return retribution;
    }

    public void setRetribution(Retribution retribution) {
        this.retribution = retribution;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Repartition)) {
            return false;
        }
        Repartition other = (Repartition) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Repartition[ code=" + code + " ]";
    }
    
}
