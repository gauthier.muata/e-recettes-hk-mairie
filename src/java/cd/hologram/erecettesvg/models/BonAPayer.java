/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_BON_A_PAYER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BonAPayer.findAll", query = "SELECT b FROM BonAPayer b"),
    @NamedQuery(name = "BonAPayer.findByCode", query = "SELECT b FROM BonAPayer b WHERE b.code = :code"),
    @NamedQuery(name = "BonAPayer.findByMontant", query = "SELECT b FROM BonAPayer b WHERE b.montant = :montant"),
    @NamedQuery(name = "BonAPayer.findByDateCreat", query = "SELECT b FROM BonAPayer b WHERE b.dateCreat = :dateCreat"),
    @NamedQuery(name = "BonAPayer.findByDateEcheance", query = "SELECT b FROM BonAPayer b WHERE b.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "BonAPayer.findByAgentCreat", query = "SELECT b FROM BonAPayer b WHERE b.agentCreat = :agentCreat"),
    @NamedQuery(name = "BonAPayer.findByMotifPenalite", query = "SELECT b FROM BonAPayer b WHERE b.motifPenalite = :motifPenalite"),
    @NamedQuery(name = "BonAPayer.findByEtatImpression", query = "SELECT b FROM BonAPayer b WHERE b.etatImpression = :etatImpression"),
    @NamedQuery(name = "BonAPayer.findByEnContentieux", query = "SELECT b FROM BonAPayer b WHERE b.enContentieux = :enContentieux"),
    @NamedQuery(name = "BonAPayer.findByActeGenerateur", query = "SELECT b FROM BonAPayer b WHERE b.acteGenerateur = :acteGenerateur"),
    @NamedQuery(name = "BonAPayer.findByEstFraction", query = "SELECT b FROM BonAPayer b WHERE b.estFraction = :estFraction"),
    @NamedQuery(name = "BonAPayer.findByTypeBp", query = "SELECT b FROM BonAPayer b WHERE b.typeBp = :typeBp"),
    @NamedQuery(name = "BonAPayer.findByEtat", query = "SELECT b FROM BonAPayer b WHERE b.etat = :etat")})
public class BonAPayer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "CODE")
    private String code;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 50)
    @Column(name = "MOTIF_PENALITE")
    private String motifPenalite;
    @Column(name = "ETAT_IMPRESSION")
    private Boolean etatImpression;
    @Column(name = "EN_CONTENTIEUX")
    private Boolean enContentieux;
    @Size(max = 50)
    @Column(name = "ACTE_GENERATEUR")
    private String acteGenerateur;
    @Column(name = "EST_FRACTION")
    private Boolean estFraction;
    @Column(name = "TYPE_BP")
    private Integer typeBp;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "FK_AMR", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr fkAmr;
    @JoinColumn(name = "FK_COMPTE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire fkCompte;
    @JoinColumn(name = "FK_NOTE_CALCUL", referencedColumnName = "NUMERO")
    @ManyToOne
    private NoteCalcul fkNoteCalcul;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @OneToMany(mappedBy = "bonAPayer")
    private List<Degrevement> degrevementList;

    public BonAPayer() {
    }

    public BonAPayer(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getMotifPenalite() {
        return motifPenalite;
    }

    public void setMotifPenalite(String motifPenalite) {
        this.motifPenalite = motifPenalite;
    }

    public Boolean getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Boolean etatImpression) {
        this.etatImpression = etatImpression;
    }

    public Boolean getEnContentieux() {
        return enContentieux;
    }

    public void setEnContentieux(Boolean enContentieux) {
        this.enContentieux = enContentieux;
    }

    public String getActeGenerateur() {
        return acteGenerateur;
    }

    public void setActeGenerateur(String acteGenerateur) {
        this.acteGenerateur = acteGenerateur;
    }

    public Boolean getEstFraction() {
        return estFraction;
    }

    public void setEstFraction(Boolean estFraction) {
        this.estFraction = estFraction;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Integer getTypeBp() {
        return typeBp;
    }

    public void setTypeBp(Integer typeBp) {
        this.typeBp = typeBp;
    }

    public Amr getFkAmr() {
        return fkAmr;
    }

    public void setFkAmr(Amr fkAmr) {
        this.fkAmr = fkAmr;
    }

    public CompteBancaire getFkCompte() {
        return fkCompte;
    }

    public void setFkCompte(CompteBancaire fkCompte) {
        this.fkCompte = fkCompte;
    }

    public NoteCalcul getFkNoteCalcul() {
        return fkNoteCalcul;
    }

    public void setFkNoteCalcul(NoteCalcul fkNoteCalcul) {
        this.fkNoteCalcul = fkNoteCalcul;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @XmlTransient
    public List<Degrevement> getDegrevementList() {
        return degrevementList;
    }

    public void setDegrevementList(List<Degrevement> degrevementList) {
        this.degrevementList = degrevementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BonAPayer)) {
            return false;
        }
        BonAPayer other = (BonAPayer) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.BonAPayer[ code=" + code + " ]";
    }

}
