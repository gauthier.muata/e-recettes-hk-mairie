/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JUSLINTSHIAMUA
 */
@Cacheable(false)
@Entity
@Table(name = "T_PARAM_TAUX_VIGNETTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ParamTauxVignette.findAll", query = "SELECT p FROM ParamTauxVignette p"),
    @NamedQuery(name = "ParamTauxVignette.findByCode", query = "SELECT p FROM ParamTauxVignette p WHERE p.code = :code"),
    @NamedQuery(name = "ParamTauxVignette.findByMontantImpot", query = "SELECT p FROM ParamTauxVignette p WHERE p.montantImpot = :montantImpot"),
    @NamedQuery(name = "ParamTauxVignette.findByMontantTscr", query = "SELECT p FROM ParamTauxVignette p WHERE p.montantTscr = :montantTscr"),
    @NamedQuery(name = "ParamTauxVignette.findByMontantTotal", query = "SELECT p FROM ParamTauxVignette p WHERE p.montantTotal = :montantTotal")})
public class ParamTauxVignette implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Column(name = "MONTANT_IMPOT")
    private BigDecimal montantImpot;
    @Column(name = "MONTANT_TSCR")
    private BigDecimal montantTscr;
    @Column(name = "MONTANT_TOTAL")
    private BigDecimal montantTotal;
    
    @JoinColumn(name = "TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif tarif;
    @JoinColumn(name = "FORME_JURIDIQUE", referencedColumnName = "CODE")
    @ManyToOne
    private FormeJuridique formeJuridique;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getMontantImpot() {
        return montantImpot;
    }

    public void setMontantImpot(BigDecimal montantImpot) {
        this.montantImpot = montantImpot;
    }

    public BigDecimal getMontantTscr() {
        return montantTscr;
    }

    public void setMontantTscr(BigDecimal montantTscr) {
        this.montantTscr = montantTscr;
    }

    public BigDecimal getMontantTotal() {
        return montantTotal;
    }

    public void setMontantTotal(BigDecimal montantTotal) {
        this.montantTotal = montantTotal;
    }

    public Tarif getTarif() {
        return tarif;
    }

    public void setTarif(Tarif tarif) {
        this.tarif = tarif;
    }

    public FormeJuridique getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(FormeJuridique formeJuridique) {
        this.formeJuridique = formeJuridique;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParamTauxVignette)) {
            return false;
        }
        ParamTauxVignette other = (ParamTauxVignette) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.ParamTauxVignette[ code=" + code + " ]";
    }
    
}
