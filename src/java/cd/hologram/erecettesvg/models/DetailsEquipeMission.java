///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package cd.hologram.erecettesvg.models;
//
//import java.io.Serializable;
//import javax.persistence.Basic;
//import javax.persistence.Cacheable;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//
///**
// *
// * @author WILLY
// */
//@Entity
//@Cacheable(false)
//@Table(name = "T_DETAILS_EQUIPE_MISSION")
//@XmlRootElement
//@NamedQueries({
//    @NamedQuery(name = "DetailsEquipeMission.findAll", query = "SELECT d FROM DetailsEquipeMission d"),
//    @NamedQuery(name = "DetailsEquipeMission.findById", query = "SELECT d FROM DetailsEquipeMission d WHERE d.id = :id"),
//    @NamedQuery(name = "DetailsEquipeMission.findByIsChefEquipe", query = "SELECT d FROM DetailsEquipeMission d WHERE d.isChefEquipe = :isChefEquipe"),
//    @NamedQuery(name = "DetailsEquipeMission.findByEtat", query = "SELECT d FROM DetailsEquipeMission d WHERE d.etat = :etat"),
//    @NamedQuery(name = "DetailsEquipeMission.findByDateAffectation", query = "SELECT d FROM DetailsEquipeMission d WHERE d.dateAffectation = :dateAffectation")})
//public class DetailsEquipeMission implements Serializable {
//    private static final long serialVersionUID = 1L;
//    @Id
//    @Basic(optional = false)
//    @NotNull
//    @Column(name = "ID")
//    private Integer id;
//    @Column(name = "IS_CHEF_EQUIPE")
//    private Boolean isChefEquipe;
//    @Column(name = "ETAT")
//    private Integer etat;
//    @Size(max = 10)
//    @Column(name = "DATE_AFFECTATION")
//    private String dateAffectation;
//    @JoinColumn(name = "FK_AGENT", referencedColumnName = "CODE")
//    @ManyToOne
//    private Agent fkAgent;
//    @JoinColumn(name = "FK_EQUIPE_MISSION", referencedColumnName = "ID")
//    @ManyToOne
//    private EquipeMission fkEquipeMission;
//
//    public DetailsEquipeMission() {
//    }
//
//    public DetailsEquipeMission(Integer id) {
//        this.id = id;
//    }
//
//    public Integer getId() {
//        return id;
//    }
//
//    public void setId(Integer id) {
//        this.id = id;
//    }
//
//    public Boolean getIsChefEquipe() {
//        return isChefEquipe;
//    }
//
//    public void setIsChefEquipe(Boolean isChefEquipe) {
//        this.isChefEquipe = isChefEquipe;
//    }
//
//    public Integer getEtat() {
//        return etat;
//    }
//
//    public void setEtat(Integer etat) {
//        this.etat = etat;
//    }
//
//    public String getDateAffectation() {
//        return dateAffectation;
//    }
//
//    public void setDateAffectation(String dateAffectation) {
//        this.dateAffectation = dateAffectation;
//    }
//
//    public Agent getFkAgent() {
//        return fkAgent;
//    }
//
//    public void setFkAgent(Agent fkAgent) {
//        this.fkAgent = fkAgent;
//    }
//
//    public EquipeMission getFkEquipeMission() {
//        return fkEquipeMission;
//    }
//
//    public void setFkEquipeMission(EquipeMission fkEquipeMission) {
//        this.fkEquipeMission = fkEquipeMission;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof DetailsEquipeMission)) {
//            return false;
//        }
//        DetailsEquipeMission other = (DetailsEquipeMission) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "cd.hologram.erecettesvg.models.DetailsEquipeMission[ id=" + id + " ]";
//    }
//    
//}
