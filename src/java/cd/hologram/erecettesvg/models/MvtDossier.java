/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_MVT_DOSSIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MvtDossier.findAll", query = "SELECT m FROM MvtDossier m"),
    @NamedQuery(name = "MvtDossier.findById", query = "SELECT m FROM MvtDossier m WHERE m.id = :id"),
    @NamedQuery(name = "MvtDossier.findByDossier", query = "SELECT m FROM MvtDossier m WHERE m.dossier = :dossier"),
    @NamedQuery(name = "MvtDossier.findByReferenceInterne", query = "SELECT m FROM MvtDossier m WHERE m.referenceInterne = :referenceInterne"),
    @NamedQuery(name = "MvtDossier.findByUa", query = "SELECT m FROM MvtDossier m WHERE m.ua = :ua"),
    @NamedQuery(name = "MvtDossier.findByService", query = "SELECT m FROM MvtDossier m WHERE m.service = :service"),
    @NamedQuery(name = "MvtDossier.findByObservation", query = "SELECT m FROM MvtDossier m WHERE m.observation = :observation"),
    @NamedQuery(name = "MvtDossier.findByTypeMvt", query = "SELECT m FROM MvtDossier m WHERE m.typeMvt = :typeMvt"),
    @NamedQuery(name = "MvtDossier.findBySensMvt", query = "SELECT m FROM MvtDossier m WHERE m.sensMvt = :sensMvt"),
    @NamedQuery(name = "MvtDossier.findByValider", query = "SELECT m FROM MvtDossier m WHERE m.valider = :valider"),
    @NamedQuery(name = "MvtDossier.findByAgentCreat", query = "SELECT m FROM MvtDossier m WHERE m.agentCreat = :agentCreat"),
    @NamedQuery(name = "MvtDossier.findByDateCreat", query = "SELECT m FROM MvtDossier m WHERE m.dateCreat = :dateCreat"),
    @NamedQuery(name = "MvtDossier.findByAgentMaj", query = "SELECT m FROM MvtDossier m WHERE m.agentMaj = :agentMaj"),
    @NamedQuery(name = "MvtDossier.findByDateMaj", query = "SELECT m FROM MvtDossier m WHERE m.dateMaj = :dateMaj"),
    @NamedQuery(name = "MvtDossier.findByEtat", query = "SELECT m FROM MvtDossier m WHERE m.etat = :etat")})
public class MvtDossier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "DOSSIER")
    private String dossier;
    @Size(max = 20)
    @Column(name = "REFERENCE_INTERNE")
    private String referenceInterne;
    @Size(max = 20)
    @Column(name = "UA")
    private String ua;
    @Size(max = 20)
    @Column(name = "SERVICE")
    private String service;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "TYPE_MVT")
    private Short typeMvt;
    @Column(name = "SENS_MVT")
    private Short sensMvt;
    @Column(name = "VALIDER")
    private Short valider;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;

    public MvtDossier() {
    }

    public MvtDossier(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getReferenceInterne() {
        return referenceInterne;
    }

    public void setReferenceInterne(String referenceInterne) {
        this.referenceInterne = referenceInterne;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Short getTypeMvt() {
        return typeMvt;
    }

    public void setTypeMvt(Short typeMvt) {
        this.typeMvt = typeMvt;
    }

    public Short getSensMvt() {
        return sensMvt;
    }

    public void setSensMvt(Short sensMvt) {
        this.sensMvt = sensMvt;
    }

    public Short getValider() {
        return valider;
    }

    public void setValider(Short valider) {
        this.valider = valider;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MvtDossier)) {
            return false;
        }
        MvtDossier other = (MvtDossier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.MvtDossier[ id=" + id + " ]";
    }
    
}
