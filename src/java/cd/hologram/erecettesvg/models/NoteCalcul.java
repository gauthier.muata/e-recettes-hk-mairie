/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_NOTE_CALCUL")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NoteCalcul.findAll", query = "SELECT n FROM NoteCalcul n"),
    @NamedQuery(name = "NoteCalcul.findByNumero", query = "SELECT n FROM NoteCalcul n WHERE n.numero = :numero"),
    @NamedQuery(name = "NoteCalcul.findByExercice", query = "SELECT n FROM NoteCalcul n WHERE n.exercice = :exercice"),
    @NamedQuery(name = "NoteCalcul.findBySite", query = "SELECT n FROM NoteCalcul n WHERE n.site = :site"),
    @NamedQuery(name = "NoteCalcul.findByService", query = "SELECT n FROM NoteCalcul n WHERE n.service = :service"),
    @NamedQuery(name = "NoteCalcul.findByEtat", query = "SELECT n FROM NoteCalcul n WHERE n.etat = :etat"),
    @NamedQuery(name = "NoteCalcul.findByAgentCreat", query = "SELECT n FROM NoteCalcul n WHERE n.agentCreat = :agentCreat"),
    @NamedQuery(name = "NoteCalcul.findByDateCreat", query = "SELECT n FROM NoteCalcul n WHERE n.dateCreat = :dateCreat"),
    @NamedQuery(name = "NoteCalcul.findByAgentValidation", query = "SELECT n FROM NoteCalcul n WHERE n.agentValidation = :agentValidation"),
    @NamedQuery(name = "NoteCalcul.findByDateValidation", query = "SELECT n FROM NoteCalcul n WHERE n.dateValidation = :dateValidation"),
    @NamedQuery(name = "NoteCalcul.findByObservation", query = "SELECT n FROM NoteCalcul n WHERE n.observation = :observation"),
    @NamedQuery(name = "NoteCalcul.findByObservationOrdonnancement", query = "SELECT n FROM NoteCalcul n WHERE n.observationOrdonnancement = :observationOrdonnancement"),
    @NamedQuery(name = "NoteCalcul.findByAvis", query = "SELECT n FROM NoteCalcul n WHERE n.avis = :avis"),
    @NamedQuery(name = "NoteCalcul.findByAgentCloture", query = "SELECT n FROM NoteCalcul n WHERE n.agentCloture = :agentCloture"),
    @NamedQuery(name = "NoteCalcul.findByDateCloture", query = "SELECT n FROM NoteCalcul n WHERE n.dateCloture = :dateCloture"),
    @NamedQuery(name = "NoteCalcul.findByFkAdressePersonne", query = "SELECT n FROM NoteCalcul n WHERE n.fkAdressePersonne = :fkAdressePersonne"),
    @NamedQuery(name = "NoteCalcul.findByPersonne", query = "SELECT n FROM NoteCalcul n WHERE n.personne = :personne"),
    @NamedQuery(name = "NoteCalcul.findByEstTaxationOffice", query = "SELECT n FROM NoteCalcul n WHERE n.estTaxationOffice = :estTaxationOffice"),
    @NamedQuery(name = "NoteCalcul.findByBien", query = "SELECT n FROM NoteCalcul n WHERE n.bien = :bien")})
public class NoteCalcul implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "NUMERO")
    private String numero;
    @Size(max = 10)
    @Column(name = "EXERCICE")
    private String exercice;
    @Size(max = 10)
    @Column(name = "SITE")
    private String site;
    @Size(max = 25)
    @Column(name = "SERVICE")
    private String service;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_VALIDATION")
    private String agentValidation;
    @Column(name = "DATE_VALIDATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateValidation;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    
    @Column(name = "OBSERVATION_ORDONNANCEMENT")
    private String observationOrdonnancement;
     
    @Size(max = 10)
    @Column(name = "AVIS")
    private String avis;
    @Size(max = 20)
    @Column(name = "AGENT_CLOTURE")
    private String agentCloture;
    @Column(name = "DATE_CLOTURE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCloture;
    @Size(max = 25)
    @Column(name = "FK_ADRESSE_PERSONNE")
    private String fkAdressePersonne;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Column(name = "EST_TAXATION_OFFICE")
    private Boolean estTaxationOffice;
    @Size(max = 50)
    @Column(name = "BIEN")
    private String bien;
    @OneToMany(mappedBy = "fkNoteCalcul")
    private List<BonAPayer> bonAPayerList;
    @JoinColumn(name = "DEPOT_DECLARATION", referencedColumnName = "CODE")
    @ManyToOne
    private DepotDeclaration depotDeclaration;
    @OneToMany(mappedBy = "noteCalcul")
    private List<NotePerception> notePerceptionList;
    @OneToMany(mappedBy = "noteCalcul")
    private List<DetailsNc> detailsNcList;

    public NoteCalcul() {
    }

    public NoteCalcul(String numero) {
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentValidation() {
        return agentValidation;
    }

    public void setAgentValidation(String agentValidation) {
        this.agentValidation = agentValidation;
    }

    public Date getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(Date dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

    public String getAgentCloture() {
        return agentCloture;
    }

    public void setAgentCloture(String agentCloture) {
        this.agentCloture = agentCloture;
    }

    public Date getDateCloture() {
        return dateCloture;
    }

    public void setDateCloture(Date dateCloture) {
        this.dateCloture = dateCloture;
    }

    public String getFkAdressePersonne() {
        return fkAdressePersonne;
    }

    public void setFkAdressePersonne(String fkAdressePersonne) {
        this.fkAdressePersonne = fkAdressePersonne;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public Boolean getEstTaxationOffice() {
        return estTaxationOffice;
    }

    public void setEstTaxationOffice(Boolean estTaxationOffice) {
        this.estTaxationOffice = estTaxationOffice;
    }

    public String getObservationOrdonnancement() {
        return observationOrdonnancement;
    }

    public void setObservationOrdonnancement(String observationOrdonnancement) {
        this.observationOrdonnancement = observationOrdonnancement;
    }
    
    
    public String getBien() {
        return bien;
    }

    public void setBien(String bien) {
        this.bien = bien;
    }

    @XmlTransient
    public List<BonAPayer> getBonAPayerList() {
        return bonAPayerList;
    }

    public void setBonAPayerList(List<BonAPayer> bonAPayerList) {
        this.bonAPayerList = bonAPayerList;
    }

    public DepotDeclaration getDepotDeclaration() {
        return depotDeclaration;
    }

    public void setDepotDeclaration(DepotDeclaration depotDeclaration) {
        this.depotDeclaration = depotDeclaration;
    }

    @XmlTransient
    public List<NotePerception> getNotePerceptionList() {
        return notePerceptionList;
    }

    public void setNotePerceptionList(List<NotePerception> notePerceptionList) {
        this.notePerceptionList = notePerceptionList;
    }

    @XmlTransient
    public List<DetailsNc> getDetailsNcList() {
        return detailsNcList;
    }

    public void setDetailsNcList(List<DetailsNc> detailsNcList) {
        this.detailsNcList = detailsNcList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero != null ? numero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NoteCalcul)) {
            return false;
        }
        NoteCalcul other = (NoteCalcul) object;
        if ((this.numero == null && other.numero != null) || (this.numero != null && !this.numero.equals(other.numero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.NoteCalcul[ numero=" + numero + " ]";
    }
    
}
