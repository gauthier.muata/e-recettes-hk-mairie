/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Cacheable(false)
@Table(name = "T_ENTITE_ADMINISTRATIVE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EntiteAdministrative.findAll", query = "SELECT e FROM EntiteAdministrative e"),
    @NamedQuery(name = "EntiteAdministrative.findByCode", query = "SELECT e FROM EntiteAdministrative e WHERE e.code = :code"),
    @NamedQuery(name = "EntiteAdministrative.findByIntitule", query = "SELECT e FROM EntiteAdministrative e WHERE e.intitule = :intitule"),
    @NamedQuery(name = "EntiteAdministrative.findByEtat", query = "SELECT e FROM EntiteAdministrative e WHERE e.etat = :etat")})
public class EntiteAdministrative implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Short etat;
    @OneToMany(mappedBy = "district")
    private List<Adresse> adresseList;
    @OneToMany(mappedBy = "ville")
    private List<Adresse> adresseList1;
    @OneToMany(mappedBy = "commune")
    private List<Adresse> adresseList2;
    @OneToMany(mappedBy = "quartier")
    private List<Adresse> adresseList3;
    @OneToMany(mappedBy = "avenue")
    private List<Adresse> adresseList4;
    @OneToMany(mappedBy = "province")
    private List<Adresse> adresseList5;
    @OneToMany(mappedBy = "entiteMere")
    private List<EntiteAdministrative> entiteAdministrativeList;
    @JoinColumn(name = "ENTITE_MERE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative entiteMere;
    @JoinColumn(name = "TYPE_ENTITE", referencedColumnName = "CODE")
    @ManyToOne
    private TypeEntite typeEntite;

    public EntiteAdministrative() {
    }

    public EntiteAdministrative(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Adresse> getAdresseList() {
        return adresseList;
    }

    public void setAdresseList(List<Adresse> adresseList) {
        this.adresseList = adresseList;
    }

    @XmlTransient
    public List<Adresse> getAdresseList1() {
        return adresseList1;
    }

    public void setAdresseList1(List<Adresse> adresseList1) {
        this.adresseList1 = adresseList1;
    }

    @XmlTransient
    public List<Adresse> getAdresseList2() {
        return adresseList2;
    }

    public void setAdresseList2(List<Adresse> adresseList2) {
        this.adresseList2 = adresseList2;
    }

    @XmlTransient
    public List<Adresse> getAdresseList3() {
        return adresseList3;
    }

    public void setAdresseList3(List<Adresse> adresseList3) {
        this.adresseList3 = adresseList3;
    }

    @XmlTransient
    public List<Adresse> getAdresseList4() {
        return adresseList4;
    }

    public void setAdresseList4(List<Adresse> adresseList4) {
        this.adresseList4 = adresseList4;
    }

    @XmlTransient
    public List<Adresse> getAdresseList5() {
        return adresseList5;
    }

    public void setAdresseList5(List<Adresse> adresseList5) {
        this.adresseList5 = adresseList5;
    }

    @XmlTransient
    public List<EntiteAdministrative> getEntiteAdministrativeList() {
        return entiteAdministrativeList;
    }

    public void setEntiteAdministrativeList(List<EntiteAdministrative> entiteAdministrativeList) {
        this.entiteAdministrativeList = entiteAdministrativeList;
    }

    public EntiteAdministrative getEntiteMere() {
        return entiteMere;
    }

    public void setEntiteMere(EntiteAdministrative entiteMere) {
        this.entiteMere = entiteMere;
    }

    public TypeEntite getTypeEntite() {
        return typeEntite;
    }

    public void setTypeEntite(TypeEntite typeEntite) {
        this.typeEntite = typeEntite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntiteAdministrative)) {
            return false;
        }
        EntiteAdministrative other = (EntiteAdministrative) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.EntiteAdministrative[ code=" + code + " ]";
    }
    
}
