/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_DETAIL_PREVISION_CREDIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailPrevisionCredit.findAll", query = "SELECT d FROM DetailPrevisionCredit d"),
    @NamedQuery(name = "DetailPrevisionCredit.findById", query = "SELECT d FROM DetailPrevisionCredit d WHERE d.id = :id"),
    @NamedQuery(name = "DetailPrevisionCredit.findByMontant", query = "SELECT d FROM DetailPrevisionCredit d WHERE d.montant = :montant"),
    @NamedQuery(name = "DetailPrevisionCredit.findByEtat", query = "SELECT d FROM DetailPrevisionCredit d WHERE d.etat = :etat")})
public class DetailPrevisionCredit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "DETAIL_ASSUJETTISSEMENT", referencedColumnName = "ID")
    @ManyToOne
    private DetailAssujettissement detailAssujettissement;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "PREVISION_CREDIT", referencedColumnName = "CODE")
    @ManyToOne
    private PrevisionCredit previsionCredit;

    public DetailPrevisionCredit() {
    }

    public DetailPrevisionCredit(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public DetailAssujettissement getDetailAssujettissement() {
        return detailAssujettissement;
    }

    public void setDetailAssujettissement(DetailAssujettissement detailAssujettissement) {
        this.detailAssujettissement = detailAssujettissement;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public PrevisionCredit getPrevisionCredit() {
        return previsionCredit;
    }

    public void setPrevisionCredit(PrevisionCredit previsionCredit) {
        this.previsionCredit = previsionCredit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailPrevisionCredit)) {
            return false;
        }
        DetailPrevisionCredit other = (DetailPrevisionCredit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.DetailPrevisionCredit[ id=" + id + " ]";
    }
    
}
