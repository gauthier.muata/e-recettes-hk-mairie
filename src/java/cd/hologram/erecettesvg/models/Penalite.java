/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PENALITE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Penalite.findAll", query = "SELECT p FROM Penalite p"),
    @NamedQuery(name = "Penalite.findByCode", query = "SELECT p FROM Penalite p WHERE p.code = :code"),
    @NamedQuery(name = "Penalite.findByIntitule", query = "SELECT p FROM Penalite p WHERE p.intitule = :intitule"),
    @NamedQuery(name = "Penalite.findByPalier", query = "SELECT p FROM Penalite p WHERE p.palier = :palier"),
    @NamedQuery(name = "Penalite.findByVisibilteUtilisateur", query = "SELECT p FROM Penalite p WHERE p.visibilteUtilisateur = :visibilteUtilisateur"),
    @NamedQuery(name = "Penalite.findByAgentCreat", query = "SELECT p FROM Penalite p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "Penalite.findByDateCreat", query = "SELECT p FROM Penalite p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "Penalite.findByAgentMaj", query = "SELECT p FROM Penalite p WHERE p.agentMaj = :agentMaj"),
    @NamedQuery(name = "Penalite.findByDateMaj", query = "SELECT p FROM Penalite p WHERE p.dateMaj = :dateMaj"),
    @NamedQuery(name = "Penalite.findByEtat", query = "SELECT p FROM Penalite p WHERE p.etat = :etat"),
    @NamedQuery(name = "Penalite.findByAppliquerMoisRetard", query = "SELECT p FROM Penalite p WHERE p.appliquerMoisRetard = :appliquerMoisRetard")})
public class Penalite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "PALIER")
    private Boolean palier;
    @Column(name = "VISIBILTE_UTILISATEUR")
    private Boolean visibilteUtilisateur;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "APPLIQUER_MOIS_RETARD")
    private Boolean appliquerMoisRetard;
    @JoinColumn(name = "TYPE_PENALITE", referencedColumnName = "CODE")
    @ManyToOne
    private TypePenalite typePenalite;
    @OneToMany(mappedBy = "penalite")
    private List<DetailsAmr> detailsAmrList;
    @OneToMany(mappedBy = "penalite")
    private List<PalierTauxPenalite> palierTauxPenaliteList;
    @OneToMany(mappedBy = "motif")
    private List<FichePriseCharge> fichePriseChargeList;

    public Penalite() {
    }

    public Penalite(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getPalier() {
        return palier;
    }

    public void setPalier(Boolean palier) {
        this.palier = palier;
    }

    public Boolean getVisibilteUtilisateur() {
        return visibilteUtilisateur;
    }

    public void setVisibilteUtilisateur(Boolean visibilteUtilisateur) {
        this.visibilteUtilisateur = visibilteUtilisateur;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Boolean getAppliquerMoisRetard() {
        return appliquerMoisRetard;
    }

    public void setAppliquerMoisRetard(Boolean appliquerMoisRetard) {
        this.appliquerMoisRetard = appliquerMoisRetard;
    }

    public TypePenalite getTypePenalite() {
        return typePenalite;
    }

    public void setTypePenalite(TypePenalite typePenalite) {
        this.typePenalite = typePenalite;
    }

    @XmlTransient
    public List<DetailsAmr> getDetailsAmrList() {
        return detailsAmrList;
    }

    public void setDetailsAmrList(List<DetailsAmr> detailsAmrList) {
        this.detailsAmrList = detailsAmrList;
    }

    @XmlTransient
    public List<PalierTauxPenalite> getPalierTauxPenaliteList() {
        return palierTauxPenaliteList;
    }

    public void setPalierTauxPenaliteList(List<PalierTauxPenalite> palierTauxPenaliteList) {
        this.palierTauxPenaliteList = palierTauxPenaliteList;
    }

    @XmlTransient
    public List<FichePriseCharge> getFichePriseChargeList() {
        return fichePriseChargeList;
    }

    public void setFichePriseChargeList(List<FichePriseCharge> fichePriseChargeList) {
        this.fichePriseChargeList = fichePriseChargeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Penalite)) {
            return false;
        }
        Penalite other = (Penalite) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Penalite[ code=" + code + " ]";
    }
    
}
