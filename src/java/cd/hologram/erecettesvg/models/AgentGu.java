/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_AGENT_GU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgentGu.findAll", query = "SELECT a FROM AgentGu a"),
    @NamedQuery(name = "AgentGu.findById", query = "SELECT a FROM AgentGu a WHERE a.id = :id"),
    @NamedQuery(name = "AgentGu.findByFkAgent", query = "SELECT a FROM AgentGu a WHERE a.fkAgent = :fkAgent"),
    @NamedQuery(name = "AgentGu.findByEtat", query = "SELECT a FROM AgentGu a WHERE a.etat = :etat"),
    @NamedQuery(name = "AgentGu.findByFkAgentCreat", query = "SELECT a FROM AgentGu a WHERE a.fkAgentCreat = :fkAgentCreat"),
    @NamedQuery(name = "AgentGu.findByDateCreat", query = "SELECT a FROM AgentGu a WHERE a.dateCreat = :dateCreat")})
public class AgentGu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "FK_AGENT")
    private Integer fkAgent;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "FK_AGENT_CREAT")
    private Integer fkAgentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @JoinColumn(name = "FK_GU", referencedColumnName = "ID")
    @ManyToOne
    private GroupUser fkGu;

    public AgentGu() {
    }

    public AgentGu(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Integer fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Integer getFkAgentCreat() {
        return fkAgentCreat;
    }

    public void setFkAgentCreat(Integer fkAgentCreat) {
        this.fkAgentCreat = fkAgentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public GroupUser getFkGu() {
        return fkGu;
    }

    public void setFkGu(GroupUser fkGu) {
        this.fkGu = fkGu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgentGu)) {
            return false;
        }
        AgentGu other = (AgentGu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AgentGu[ id=" + id + " ]";
    }
    
}
