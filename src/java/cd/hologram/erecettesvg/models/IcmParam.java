/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */

@Entity
@Cacheable(false)
@Table(name = "T_ICM_PARAM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "IcmParam.findAll", query = "SELECT l FROM IcmParam l"),
    @NamedQuery(name = "IcmParam.findById", query = "SELECT l FROM IcmParam l WHERE l.id = :id"),
    @NamedQuery(name = "IcmParam.findByAnnee", query = "SELECT l FROM IcmParam l WHERE l.annee = :annee"),
    @NamedQuery(name = "IcmParam.findByTarif", query = "SELECT l FROM IcmParam l WHERE l.tarif = :tarif"),
    @NamedQuery(name = "IcmParam.findByTaux", query = "SELECT l FROM IcmParam l WHERE l.taux = :taux"),
    @NamedQuery(name = "IcmParam.findByDevise", query = "SELECT l FROM IcmParam l WHERE l.devise = :devise"),
    @NamedQuery(name = "IcmParam.findByUnite", query = "SELECT l FROM IcmParam l WHERE l.unite = :unite"),
    @NamedQuery(name = "IcmParam.findByEtat", query = "SELECT l FROM IcmParam l WHERE l.etat = :etat")})

public class IcmParam implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "ANNEE")
    private Integer annee;

    @Column(name = "TARIF")
    private String tarif;

    @Column(name = "TAUX")
    private BigDecimal taux;

    @Column(name = "ETAT")
    private Integer etat;
    
    @Column(name = "DEVISE")
    private String devise;
    
    @Column(name = "UNITE")
    private String unite;
    
    public IcmParam() {
    }

    public IcmParam(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public String getTarif() {
        return tarif;
    }

    public void setTarif(String tarif) {
        this.tarif = tarif;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof IcmParam)) {
            return false;
        }
        IcmParam other = (IcmParam) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.IcmParam[ id=" + id + " ]";
    }

}
