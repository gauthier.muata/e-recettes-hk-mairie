/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PERIODICITE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periodicite.findAll", query = "SELECT p FROM Periodicite p"),
    @NamedQuery(name = "Periodicite.findByCode", query = "SELECT p FROM Periodicite p WHERE p.code = :code"),
    @NamedQuery(name = "Periodicite.findByIntitule", query = "SELECT p FROM Periodicite p WHERE p.intitule = :intitule"),
    @NamedQuery(name = "Periodicite.findByNbrJour", query = "SELECT p FROM Periodicite p WHERE p.nbrJour = :nbrJour"),
    @NamedQuery(name = "Periodicite.findByEtat", query = "SELECT p FROM Periodicite p WHERE p.etat = :etat"),
    @NamedQuery(name = "Periodicite.findByPeriodeRecidive", query = "SELECT p FROM Periodicite p WHERE p.periodeRecidive = :periodeRecidive")})
public class Periodicite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "NBR_JOUR")
    private Integer nbrJour;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "PERIODE_RECIDIVE")
    private Integer periodeRecidive;
    @OneToMany(mappedBy = "periodicite")
    private List<ArticleBudgetaire> articleBudgetaireList;

    public Periodicite() {
    }

    public Periodicite(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getNbrJour() {
        return nbrJour;
    }

    public void setNbrJour(Integer nbrJour) {
        this.nbrJour = nbrJour;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Integer getPeriodeRecidive() {
        return periodeRecidive;
    }

    public void setPeriodeRecidive(Integer periodeRecidive) {
        this.periodeRecidive = periodeRecidive;
    }

    @XmlTransient
    public List<ArticleBudgetaire> getArticleBudgetaireList() {
        return articleBudgetaireList;
    }

    public void setArticleBudgetaireList(List<ArticleBudgetaire> articleBudgetaireList) {
        this.articleBudgetaireList = articleBudgetaireList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periodicite)) {
            return false;
        }
        Periodicite other = (Periodicite) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Periodicite[ code=" + code + " ]";
    }
    
}
