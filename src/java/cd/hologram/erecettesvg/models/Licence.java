/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_LICENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Licence.findAll", query = "SELECT l FROM Licence l"),
    @NamedQuery(name = "Licence.findById", query = "SELECT l FROM Licence l WHERE l.id = :id"),
    @NamedQuery(name = "Licence.findByTitre1", query = "SELECT l FROM Licence l WHERE l.titre1 = :titre1"),
    @NamedQuery(name = "Licence.findByTitre2", query = "SELECT l FROM Licence l WHERE l.titre2 = :titre2"),
    @NamedQuery(name = "Licence.findByTitre3", query = "SELECT l FROM Licence l WHERE l.titre3 = :titre3"),
    @NamedQuery(name = "Licence.findByTitre4", query = "SELECT l FROM Licence l WHERE l.titre4 = :titre4")})
public class Licence implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 150)
    @Column(name = "TITRE1")
    private String titre1;
    @Size(max = 150)
    @Column(name = "TITRE2")
    private String titre2;
    @Size(max = 150)
    @Column(name = "TITRE3")
    private String titre3;
    @Size(max = 150)
    @Column(name = "TITRE4")
    private String titre4;

    public Licence() {
    }

    public Licence(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre1() {
        return titre1;
    }

    public void setTitre1(String titre1) {
        this.titre1 = titre1;
    }

    public String getTitre2() {
        return titre2;
    }

    public void setTitre2(String titre2) {
        this.titre2 = titre2;
    }

    public String getTitre3() {
        return titre3;
    }

    public void setTitre3(String titre3) {
        this.titre3 = titre3;
    }

    public String getTitre4() {
        return titre4;
    }

    public void setTitre4(String titre4) {
        this.titre4 = titre4;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Licence)) {
            return false;
        }
        Licence other = (Licence) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Licence[ id=" + id + " ]";
    }
    
}
