/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_AB_COMPLEMENT_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AbComplementBien.findAll", query = "SELECT a FROM AbComplementBien a"),
    @NamedQuery(name = "AbComplementBien.findByCode", query = "SELECT a FROM AbComplementBien a WHERE a.code = :code"),
    @NamedQuery(name = "AbComplementBien.findByEtat", query = "SELECT a FROM AbComplementBien a WHERE a.etat = :etat"),
    @NamedQuery(name = "AbComplementBien.findByDateCreat", query = "SELECT a FROM AbComplementBien a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "AbComplementBien.findByAgentCreat", query = "SELECT a FROM AbComplementBien a WHERE a.agentCreat = :agentCreat")})
public class AbComplementBien implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Column(name = "ETAT")
    private BigInteger etat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "AGENT_CREAT")
    private Integer agentCreat;
    @JoinColumn(name = "ARTICLE_BUDGETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire articleBudgetaire;
    @JoinColumn(name = "TYPE_COMPLEMENT_BIEN", referencedColumnName = "CODE")
    @ManyToOne
    private TypeComplementBien typeComplementBien;

    public AbComplementBien() {
    }

    public AbComplementBien(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigInteger getEtat() {
        return etat;
    }

    public void setEtat(BigInteger etat) {
        this.etat = etat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public ArticleBudgetaire getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(ArticleBudgetaire articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public TypeComplementBien getTypeComplementBien() {
        return typeComplementBien;
    }

    public void setTypeComplementBien(TypeComplementBien typeComplementBien) {
        this.typeComplementBien = typeComplementBien;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AbComplementBien)) {
            return false;
        }
        AbComplementBien other = (AbComplementBien) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AbComplementBien[ code=" + code + " ]";
    }
    
}
