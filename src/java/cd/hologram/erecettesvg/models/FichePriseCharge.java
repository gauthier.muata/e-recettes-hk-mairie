/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_FICHE_PRISE_CHARGE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FichePriseCharge.findAll", query = "SELECT f FROM FichePriseCharge f"),
    @NamedQuery(name = "FichePriseCharge.findByCode", query = "SELECT f FROM FichePriseCharge f WHERE f.code = :code"),
    @NamedQuery(name = "FichePriseCharge.findByDetailMotif", query = "SELECT f FROM FichePriseCharge f WHERE f.detailMotif = :detailMotif"),
    @NamedQuery(name = "FichePriseCharge.findByTotalPrincipaldu", query = "SELECT f FROM FichePriseCharge f WHERE f.totalPrincipaldu = :totalPrincipaldu"),
    @NamedQuery(name = "FichePriseCharge.findByTotalPenalitedu", query = "SELECT f FROM FichePriseCharge f WHERE f.totalPenalitedu = :totalPenalitedu"),
    @NamedQuery(name = "FichePriseCharge.findByDateEcheance", query = "SELECT f FROM FichePriseCharge f WHERE f.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "FichePriseCharge.findByDateCreat", query = "SELECT f FROM FichePriseCharge f WHERE f.dateCreat = :dateCreat"),
    @NamedQuery(name = "FichePriseCharge.findByCasFpc", query = "SELECT f FROM FichePriseCharge f WHERE f.casFpc = :casFpc"),
    @NamedQuery(name = "FichePriseCharge.findByFkMed", query = "SELECT f FROM FichePriseCharge f WHERE f.fkMed = :fkMed"),
    @NamedQuery(name = "FichePriseCharge.findByFkArticleBudgetaire", query = "SELECT f FROM FichePriseCharge f WHERE f.fkArticleBudgetaire = :fkArticleBudgetaire"),
    @NamedQuery(name = "FichePriseCharge.findByAvisRegularisation", query = "SELECT f FROM FichePriseCharge f WHERE f.avisRegularisation = :avisRegularisation"),
    @NamedQuery(name = "FichePriseCharge.findByNumeroReference", query = "SELECT f FROM FichePriseCharge f WHERE f.numeroReference = :numeroReference"),
    @NamedQuery(name = "FichePriseCharge.findByDevise", query = "SELECT f FROM FichePriseCharge f WHERE f.devise = :devise")})
public class FichePriseCharge implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 200)
    @Column(name = "DETAIL_MOTIF")
    private String detailMotif;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTAL_PRINCIPALDU")
    private BigDecimal totalPrincipaldu;
    @Column(name = "TOTAL_PENALITEDU")
    private BigDecimal totalPenalitedu;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 2)
    @Column(name = "CAS_FPC")
    private String casFpc;
    
    @Column(name = "FK_MED")
    private String fkMed;
    
    
    @Column(name = "AVIS_REGULARISATION")
    private Integer avisRegularisation;
    @Size(max = 500)
    @Column(name = "NUMERO_REFERENCE")
    private String numeroReference;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;
    
    @Column(name = "FK_ARTICLE_BUDGETAIRE")
    private String fkArticleBudgetaire;
    
    @OneToMany(mappedBy = "fichePriseCharge")
    private List<DetailFichePriseCharge> detailFichePriseChargeList;
    @JoinColumn(name = "ETAT", referencedColumnName = "CODE")
    @ManyToOne
    private Etat etat;
    @JoinColumn(name = "MOTIF", referencedColumnName = "CODE")
    @ManyToOne
    private Penalite motif;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;
    @OneToMany(mappedBy = "fichePriseCharge")
    private List<Amr> amrList;

    public FichePriseCharge() {
    }

    public FichePriseCharge(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetailMotif() {
        return detailMotif;
    }

    public void setDetailMotif(String detailMotif) {
        this.detailMotif = detailMotif;
    }

    public String getFkMed() {
        return fkMed;
    }

    public void setFkMed(String fkMed) {
        this.fkMed = fkMed;
    }
    
    
    public BigDecimal getTotalPrincipaldu() {
        return totalPrincipaldu;
    }

    public void setTotalPrincipaldu(BigDecimal totalPrincipaldu) {
        this.totalPrincipaldu = totalPrincipaldu;
    }

    public BigDecimal getTotalPenalitedu() {
        return totalPenalitedu;
    }

    public void setTotalPenalitedu(BigDecimal totalPenalitedu) {
        this.totalPenalitedu = totalPenalitedu;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getCasFpc() {
        return casFpc;
    }

    public void setCasFpc(String casFpc) {
        this.casFpc = casFpc;
    }

    public Integer getAvisRegularisation() {
        return avisRegularisation;
    }

    public void setAvisRegularisation(Integer avisRegularisation) {
        this.avisRegularisation = avisRegularisation;
    }

    public String getNumeroReference() {
        return numeroReference;
    }

    public void setNumeroReference(String numeroReference) {
        this.numeroReference = numeroReference;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    @XmlTransient
    public List<DetailFichePriseCharge> getDetailFichePriseChargeList() {
        return detailFichePriseChargeList;
    }

    public void setDetailFichePriseChargeList(List<DetailFichePriseCharge> detailFichePriseChargeList) {
        this.detailFichePriseChargeList = detailFichePriseChargeList;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(String fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }
    
    
    
    

    public Penalite getMotif() {
        return motif;
    }

    public void setMotif(Penalite motif) {
        this.motif = motif;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @XmlTransient
    public List<Amr> getAmrList() {
        return amrList;
    }

    public void setAmrList(List<Amr> amrList) {
        this.amrList = amrList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FichePriseCharge)) {
            return false;
        }
        FichePriseCharge other = (FichePriseCharge) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.FichePriseCharge[ code=" + code + " ]";
    }
    
}
