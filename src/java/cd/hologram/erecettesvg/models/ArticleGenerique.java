/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ARTICLE_GENERIQUE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArticleGenerique.findAll", query = "SELECT a FROM ArticleGenerique a"),
    @NamedQuery(name = "ArticleGenerique.findByCode", query = "SELECT a FROM ArticleGenerique a WHERE a.code = :code"),
    @NamedQuery(name = "ArticleGenerique.findByIntitule", query = "SELECT a FROM ArticleGenerique a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "ArticleGenerique.findByEtat", query = "SELECT a FROM ArticleGenerique a WHERE a.etat = :etat")})
public class ArticleGenerique implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Boolean etat;
    @OneToMany(mappedBy = "articleGenerique")
    private List<ArticleBudgetaire> articleBudgetaireList;
    @JoinColumn(name = "FORME_JURIDIQUE", referencedColumnName = "CODE")
    @ManyToOne
    private FormeJuridique formeJuridique;
    @JoinColumn(name = "SERVICE_ASSIETTE", referencedColumnName = "CODE")
    @ManyToOne
    private Service serviceAssiette;

    public ArticleGenerique() {
    }

    public ArticleGenerique(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<ArticleBudgetaire> getArticleBudgetaireList() {
        return articleBudgetaireList;
    }

    public void setArticleBudgetaireList(List<ArticleBudgetaire> articleBudgetaireList) {
        this.articleBudgetaireList = articleBudgetaireList;
    }

    public FormeJuridique getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(FormeJuridique formeJuridique) {
        this.formeJuridique = formeJuridique;
    }

    public Service getServiceAssiette() {
        return serviceAssiette;
    }

    public void setServiceAssiette(Service serviceAssiette) {
        this.serviceAssiette = serviceAssiette;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArticleGenerique)) {
            return false;
        }
        ArticleGenerique other = (ArticleGenerique) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ArticleGenerique[ code=" + code + " ]";
    }
    
}
