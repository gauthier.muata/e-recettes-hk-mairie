/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_BIEN")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bien.findAll", query = "SELECT b FROM Bien b"),
    @NamedQuery(name = "Bien.findById", query = "SELECT b FROM Bien b WHERE b.id = :id"),
    @NamedQuery(name = "Bien.findByIntitule", query = "SELECT b FROM Bien b WHERE b.intitule = :intitule"),
    @NamedQuery(name = "Bien.findByDescription", query = "SELECT b FROM Bien b WHERE b.description = :description"),
    @NamedQuery(name = "Bien.findByFkUsageBien", query = "SELECT b FROM Bien b WHERE b.fkUsageBien = :fkUsageBien"),
    @NamedQuery(name = "Bien.findByFkTarif", query = "SELECT b FROM Bien b WHERE b.fkTarif = :fkTarif"),
    @NamedQuery(name = "Bien.findByFkCommune", query = "SELECT b FROM Bien b WHERE b.fkCommune = :fkCommune"),
    @NamedQuery(name = "Bien.findByFkQuartier", query = "SELECT b FROM Bien b WHERE b.fkQuartier = :fkQuartier"),
    @NamedQuery(name = "Bien.findByEtat", query = "SELECT b FROM Bien b WHERE b.etat = :etat")})
public class Bien implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 250)
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ETAT")
    private Boolean etat;

    @Column(name = "FK_USAGE_BIEN")
    private Integer fkUsageBien;

    @Column(name = "FK_TARIF")
    private String fkTarif;

    @Column(name = "FK_COMMUNE")
    private String fkCommune;

    @Column(name = "FK_QUARTIER")
    private String fkQuartier;

    @OneToMany(mappedBy = "bien")
    private List<Acquisition> acquisitionList;
    @OneToMany(mappedBy = "bien")
    private List<Assujeti> assujetiList;
    @JoinColumn(name = "FK_ADRESSE_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private AdressePersonne fkAdressePersonne;
    @JoinColumn(name = "TYPE_BIEN", referencedColumnName = "CODE")
    @ManyToOne
    private TypeBien typeBien;
    @OneToMany(mappedBy = "bien")
    private List<ComplementBien> complementBienList;
    @OneToMany(mappedBy = "fkBien")
    private List<BienActivites> bienActivitesList;

    public Bien() {
    }

    public Bien(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFkUsageBien() {
        return fkUsageBien;
    }

    public void setFkUsageBien(Integer fkUsageBien) {
        this.fkUsageBien = fkUsageBien;
    }

    public String getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(String fkTarif) {
        this.fkTarif = fkTarif;
    }

    public String getFkCommune() {
        return fkCommune;
    }

    public void setFkCommune(String fkCommune) {
        this.fkCommune = fkCommune;
    }

    public String getFkQuartier() {
        return fkQuartier;
    }

    public void setFkQuartier(String fkQuartier) {
        this.fkQuartier = fkQuartier;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<Acquisition> getAcquisitionList() {
        return acquisitionList;
    }

    public void setAcquisitionList(List<Acquisition> acquisitionList) {
        this.acquisitionList = acquisitionList;
    }

    @XmlTransient
    public List<Assujeti> getAssujetiList() {
        return assujetiList;
    }

    public void setAssujetiList(List<Assujeti> assujetiList) {
        this.assujetiList = assujetiList;
    }

    public AdressePersonne getFkAdressePersonne() {
        return fkAdressePersonne;
    }

    public void setFkAdressePersonne(AdressePersonne fkAdressePersonne) {
        this.fkAdressePersonne = fkAdressePersonne;
    }

    public TypeBien getTypeBien() {
        return typeBien;
    }

    public void setTypeBien(TypeBien typeBien) {
        this.typeBien = typeBien;
    }

    @XmlTransient
    public List<ComplementBien> getComplementBienList() {
        return complementBienList;
    }

    public void setComplementBienList(List<ComplementBien> complementBienList) {
        this.complementBienList = complementBienList;
    }

    public List<BienActivites> getBienActivitesList() {
        return bienActivitesList;
    }

    public void setBienActivitesList(List<BienActivites> bienActivitesList) {
        this.bienActivitesList = bienActivitesList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bien)) {
            return false;
        }
        Bien other = (Bien) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Bien[ id=" + id + " ]";
    }

}
