/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_WORK_FLOW")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WorkFlow.findAll", query = "SELECT w FROM WorkFlow w"),
    @NamedQuery(name = "WorkFlow.findById", query = "SELECT w FROM WorkFlow w WHERE w.id = :id"),
    @NamedQuery(name = "WorkFlow.findByProcess", query = "SELECT w FROM WorkFlow w WHERE w.process = :process"),
    @NamedQuery(name = "WorkFlow.findByOrdre", query = "SELECT w FROM WorkFlow w WHERE w.ordre = :ordre"),
    @NamedQuery(name = "WorkFlow.findByUa", query = "SELECT w FROM WorkFlow w WHERE w.ua = :ua"),
    @NamedQuery(name = "WorkFlow.findByUaOk", query = "SELECT w FROM WorkFlow w WHERE w.uaOk = :uaOk"),
    @NamedQuery(name = "WorkFlow.findByUaKo", query = "SELECT w FROM WorkFlow w WHERE w.uaKo = :uaKo"),
    @NamedQuery(name = "WorkFlow.findByDuree", query = "SELECT w FROM WorkFlow w WHERE w.duree = :duree"),
    @NamedQuery(name = "WorkFlow.findByDescription", query = "SELECT w FROM WorkFlow w WHERE w.description = :description"),
    @NamedQuery(name = "WorkFlow.findByControlSolde", query = "SELECT w FROM WorkFlow w WHERE w.controlSolde = :controlSolde"),
    @NamedQuery(name = "WorkFlow.findByAgentCreat", query = "SELECT w FROM WorkFlow w WHERE w.agentCreat = :agentCreat"),
    @NamedQuery(name = "WorkFlow.findByDateCreat", query = "SELECT w FROM WorkFlow w WHERE w.dateCreat = :dateCreat"),
    @NamedQuery(name = "WorkFlow.findByAgentMaj", query = "SELECT w FROM WorkFlow w WHERE w.agentMaj = :agentMaj"),
    @NamedQuery(name = "WorkFlow.findByDateMaj", query = "SELECT w FROM WorkFlow w WHERE w.dateMaj = :dateMaj"),
    @NamedQuery(name = "WorkFlow.findByEtat", query = "SELECT w FROM WorkFlow w WHERE w.etat = :etat"),
    @NamedQuery(name = "WorkFlow.findByMontant", query = "SELECT w FROM WorkFlow w WHERE w.montant = :montant"),
    @NamedQuery(name = "WorkFlow.findByMetier", query = "SELECT w FROM WorkFlow w WHERE w.metier = :metier")})
public class WorkFlow implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 20)
    @Column(name = "PROCESS")
    private String process;
    @Size(max = 3)
    @Column(name = "ORDRE")
    private String ordre;
    @Size(max = 20)
    @Column(name = "UA")
    private String ua;
    @Size(max = 20)
    @Column(name = "UA_OK")
    private String uaOk;
    @Size(max = 20)
    @Column(name = "UA_KO")
    private String uaKo;
    @Column(name = "DUREE")
    private Integer duree;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CONTROL_SOLDE")
    private Short controlSolde;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Size(max = 50)
    @Column(name = "METIER")
    private String metier;

    public WorkFlow() {
    }

    public WorkFlow(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcess() {
        return process;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public String getOrdre() {
        return ordre;
    }

    public void setOrdre(String ordre) {
        this.ordre = ordre;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getUaOk() {
        return uaOk;
    }

    public void setUaOk(String uaOk) {
        this.uaOk = uaOk;
    }

    public String getUaKo() {
        return uaKo;
    }

    public void setUaKo(String uaKo) {
        this.uaKo = uaKo;
    }

    public Integer getDuree() {
        return duree;
    }

    public void setDuree(Integer duree) {
        this.duree = duree;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getControlSolde() {
        return controlSolde;
    }

    public void setControlSolde(Short controlSolde) {
        this.controlSolde = controlSolde;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getMetier() {
        return metier;
    }

    public void setMetier(String metier) {
        this.metier = metier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkFlow)) {
            return false;
        }
        WorkFlow other = (WorkFlow) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.WorkFlow[ id=" + id + " ]";
    }
    
}
