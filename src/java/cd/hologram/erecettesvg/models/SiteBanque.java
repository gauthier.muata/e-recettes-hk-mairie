/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_SITE_BANQUE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SiteBanque.findAll", query = "SELECT s FROM SiteBanque s"),
    @NamedQuery(name = "SiteBanque.findById", query = "SELECT s FROM SiteBanque s WHERE s.id = :id"),
    @NamedQuery(name = "SiteBanque.findByEtat", query = "SELECT s FROM SiteBanque s WHERE s.etat = :etat")})
public class SiteBanque implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ETAT")
    private boolean etat;
    @JoinColumn(name = "BANQUE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Banque banque;
    @JoinColumn(name = "SITE", referencedColumnName = "CODE")
    @ManyToOne(optional = false)
    private Site site;

    public SiteBanque() {
    }

    public SiteBanque(Integer id) {
        this.id = id;
    }

    public SiteBanque(Integer id, boolean etat) {
        this.id = id;
        this.etat = etat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getEtat() {
        return etat;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public Banque getBanque() {
        return banque;
    }

    public void setBanque(Banque banque) {
        this.banque = banque;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SiteBanque)) {
            return false;
        }
        SiteBanque other = (SiteBanque) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.SiteBanque[ id=" + id + " ]";
    }
    
}
