/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAIL_ECHELONNEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailEchelonnement.findAll", query = "SELECT d FROM DetailEchelonnement d"),
    @NamedQuery(name = "DetailEchelonnement.findById", query = "SELECT d FROM DetailEchelonnement d WHERE d.id = :id"),
    @NamedQuery(name = "DetailEchelonnement.findByNumeroTitre", query = "SELECT d FROM DetailEchelonnement d WHERE d.numeroTitre = :numeroTitre"),
    @NamedQuery(name = "DetailEchelonnement.findByTypeTitre", query = "SELECT d FROM DetailEchelonnement d WHERE d.typeTitre = :typeTitre"),
    @NamedQuery(name = "DetailEchelonnement.findByDateCreat", query = "SELECT d FROM DetailEchelonnement d WHERE d.dateCreat = :dateCreat"),
    @NamedQuery(name = "DetailEchelonnement.findByDateValidation", query = "SELECT d FROM DetailEchelonnement d WHERE d.dateValidation = :dateValidation"),
    @NamedQuery(name = "DetailEchelonnement.findByEtat", query = "SELECT d FROM DetailEchelonnement d WHERE d.etat = :etat")})
public class DetailEchelonnement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 50)
    @Column(name = "NUMERO_TITRE")
    private String numeroTitre;
    @Size(max = 50)
    @Column(name = "TYPE_TITRE")
    private String typeTitre;
    @Size(max = 10)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Size(max = 10)
    @Column(name = "DATE_VALIDATION")
    private String dateValidation;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "AGENT_VALIDATION", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentValidation;
    @JoinColumn(name = "FK_DEMANDE_ECHELONNEMENT", referencedColumnName = "CODE")
    @ManyToOne
    private DemandeEchelonnement fkDemandeEchelonnement;

    public DetailEchelonnement() {
    }

    public DetailEchelonnement(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumeroTitre() {
        return numeroTitre;
    }

    public void setNumeroTitre(String numeroTitre) {
        this.numeroTitre = numeroTitre;
    }

    public String getTypeTitre() {
        return typeTitre;
    }

    public void setTypeTitre(String typeTitre) {
        this.typeTitre = typeTitre;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getDateValidation() {
        return dateValidation;
    }

    public void setDateValidation(String dateValidation) {
        this.dateValidation = dateValidation;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Agent getAgentValidation() {
        return agentValidation;
    }

    public void setAgentValidation(Agent agentValidation) {
        this.agentValidation = agentValidation;
    }

    public DemandeEchelonnement getFkDemandeEchelonnement() {
        return fkDemandeEchelonnement;
    }

    public void setFkDemandeEchelonnement(DemandeEchelonnement fkDemandeEchelonnement) {
        this.fkDemandeEchelonnement = fkDemandeEchelonnement;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailEchelonnement)) {
            return false;
        }
        DetailEchelonnement other = (DetailEchelonnement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.DetailEchelonnement[ id=" + id + " ]";
    }
    
}
