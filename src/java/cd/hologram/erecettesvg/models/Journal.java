/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Cacheable(false)
@Entity
@Table(name = "T_JOURNAL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Journal.findAll", query = "SELECT j FROM Journal j"),
    @NamedQuery(name = "Journal.findByCode", query = "SELECT j FROM Journal j WHERE j.code = :code"),
    @NamedQuery(name = "Journal.findByDocumentApure", query = "SELECT j FROM Journal j WHERE j.documentApure = :documentApure"),
    @NamedQuery(name = "Journal.findByTypeDocument", query = "SELECT j FROM Journal j WHERE j.typeDocument = :typeDocument"),
    @NamedQuery(name = "Journal.findByMontant", query = "SELECT j FROM Journal j WHERE j.montant = :montant"),
    @NamedQuery(name = "Journal.findByBordereau", query = "SELECT j FROM Journal j WHERE j.bordereau = :bordereau"),
    @NamedQuery(name = "Journal.findByDateBordereau", query = "SELECT j FROM Journal j WHERE j.dateBordereau = :dateBordereau"),
    @NamedQuery(name = "Journal.findByMontantApurre", query = "SELECT j FROM Journal j WHERE j.montantApurre = :montantApurre"),
    @NamedQuery(name = "Journal.findByNumeroReleveBancaire", query = "SELECT j FROM Journal j WHERE j.numeroReleveBancaire = :numeroReleveBancaire"),
    @NamedQuery(name = "Journal.findByDateReleveBancaire", query = "SELECT j FROM Journal j WHERE j.dateReleveBancaire = :dateReleveBancaire"),
    @NamedQuery(name = "Journal.findByEtatImpression", query = "SELECT j FROM Journal j WHERE j.etatImpression = :etatImpression"),
    @NamedQuery(name = "Journal.findByAgentCreat", query = "SELECT j FROM Journal j WHERE j.agentCreat = :agentCreat"),
    @NamedQuery(name = "Journal.findByDateCreat", query = "SELECT j FROM Journal j WHERE j.dateCreat = :dateCreat"),
    @NamedQuery(name = "Journal.findByAgentApurement", query = "SELECT j FROM Journal j WHERE j.agentApurement = :agentApurement"),
    @NamedQuery(name = "Journal.findByDateApurement", query = "SELECT j FROM Journal j WHERE j.dateApurement = :dateApurement"),
    @NamedQuery(name = "Journal.findByEtat", query = "SELECT j FROM Journal j WHERE j.etat = :etat"),
    @NamedQuery(name = "Journal.findByAgentMaj", query = "SELECT j FROM Journal j WHERE j.agentMaj = :agentMaj"),
    @NamedQuery(name = "Journal.findByDateMaj", query = "SELECT j FROM Journal j WHERE j.dateMaj = :dateMaj"),
    @NamedQuery(name = "Journal.findByTaux", query = "SELECT j FROM Journal j WHERE j.taux = :taux"),
    @NamedQuery(name = "Journal.findByModePaiement", query = "SELECT j FROM Journal j WHERE j.modePaiement = :modePaiement"),
    @NamedQuery(name = "Journal.findByMontantConvert", query = "SELECT j FROM Journal j WHERE j.montantConvert = :montantConvert"),
    @NamedQuery(name = "Journal.findByMontantPercu", query = "SELECT j FROM Journal j WHERE j.montantPercu = :montantPercu"),
    @NamedQuery(name = "Journal.findByNumeroAttestationPaiement", query = "SELECT j FROM Journal j WHERE j.numeroAttestationPaiement = :numeroAttestationPaiement"),
    @NamedQuery(name = "Journal.findByObservation", query = "SELECT j FROM Journal j WHERE j.observation = :observation"),
    @NamedQuery(name = "Journal.findByVolet", query = "SELECT j FROM Journal j WHERE j.volet = :volet"),
    @NamedQuery(name = "Journal.findByObservationBanque", query = "SELECT j FROM Journal j WHERE j.observationBanque = :observationBanque")})
public class Journal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "DOCUMENT_APURE")
    private String documentApure;
    @Size(max = 10)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT")
    private BigDecimal montant;    
    @Size(max = 50)
    @Column(name = "BORDEREAU")
    private String bordereau;
    @Column(name = "DATE_BORDEREAU")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBordereau;
    @Column(name = "MONTANT_APURRE")
    private BigDecimal montantApurre;
    @Size(max = 25)
    @Column(name = "NUMERO_RELEVE_BANCAIRE")
    private String numeroReleveBancaire;
    @Column(name = "DATE_RELEVE_BANCAIRE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReleveBancaire;
    @Column(name = "ETAT_IMPRESSION")
    private Short etatImpression;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_APUREMENT")
    private String agentApurement;
    @Size(max = 10)
    @Column(name = "DATE_APUREMENT")
    private String dateApurement;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Size(max = 25)
    @Column(name = "MODE_PAIEMENT")
    private String modePaiement;
    @Column(name = "MONTANT_CONVERT")
    private BigDecimal montantConvert;
    @Column(name = "MONTANT_PERCU")
    private BigDecimal montantPercu;
    @Size(max = 50)
    @Column(name = "NUMERO_ATTESTATION_PAIEMENT")
    private String numeroAttestationPaiement;
    @Size(max = 100)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 500)
    @Column(name = "OBSERVATION_BANQUE")
    private String observationBanque;
    @Size(max = 5)
    @Column(name = "VOLET")
    private String volet;
    @JoinColumn(name = "AMR", referencedColumnName = "NUMERO")
    @ManyToOne
    private Amr amr;
    @JoinColumn(name = "COMPTE_BANCAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire compteBancaire;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;
    @JoinColumn(name = "SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site site;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise Devise;

    public Journal() {
    }

    public Journal(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDocumentApure() {
        return documentApure;
    }

    public void setDocumentApure(String documentApure) {
        this.documentApure = documentApure;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateBordereau() {
        return dateBordereau;
    }

    public void setDateBordereau(Date dateBordereau) {
        this.dateBordereau = dateBordereau;
    }

    public String getBordereau() {
        return bordereau;
    }

    public void setBordereau(String bordereau) {
        this.bordereau = bordereau;
    }


    public BigDecimal getMontantApurre() {
        return montantApurre;
    }

    public void setMontantApurre(BigDecimal montantApurre) {
        this.montantApurre = montantApurre;
    }

    public String getNumeroReleveBancaire() {
        return numeroReleveBancaire;
    }

    public void setNumeroReleveBancaire(String numeroReleveBancaire) {
        this.numeroReleveBancaire = numeroReleveBancaire;
    }

    public Date getDateReleveBancaire() {
        return dateReleveBancaire;
    }

    public void setDateReleveBancaire(Date dateReleveBancaire) {
        this.dateReleveBancaire = dateReleveBancaire;
    }

    public Short getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Short etatImpression) {
        this.etatImpression = etatImpression;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentApurement() {
        return agentApurement;
    }

    public void setAgentApurement(String agentApurement) {
        this.agentApurement = agentApurement;
    }

    public String getDateApurement() {
        return dateApurement;
    }

    public void setDateApurement(String dateApurement) {
        this.dateApurement = dateApurement;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    public BigDecimal getMontantConvert() {
        return montantConvert;
    }

    public void setMontantConvert(BigDecimal montantConvert) {
        this.montantConvert = montantConvert;
    }

    public BigDecimal getMontantPercu() {
        return montantPercu;
    }

    public void setMontantPercu(BigDecimal montantPercu) {
        this.montantPercu = montantPercu;
    }

    public String getNumeroAttestationPaiement() {
        return numeroAttestationPaiement;
    }

    public void setNumeroAttestationPaiement(String numeroAttestationPaiement) {
        this.numeroAttestationPaiement = numeroAttestationPaiement;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getObservationBanque() {
        return observationBanque;
    }

    public void setObservationBanque(String observationBanque) {
        this.observationBanque = observationBanque;
    }

    public String getVolet() {
        return volet;
    }

    public void setVolet(String volet) {
        this.volet = volet;
    }

    public Amr getAmr() {
        return amr;
    }

    public void setAmr(Amr amr) {
        this.amr = amr;
    }

    public CompteBancaire getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(CompteBancaire compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Devise getDevise() {
        return Devise;
    }

    public void setDevise(Devise Devise) {
        this.Devise = Devise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Journal)) {
            return false;
        }
        Journal other = (Journal) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Journal[ code=" + code + " ]";
    }
    
}
