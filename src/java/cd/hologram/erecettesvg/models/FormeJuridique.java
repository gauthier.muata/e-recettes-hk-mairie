/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_FORME_JURIDIQUE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormeJuridique.findAll", query = "SELECT f FROM FormeJuridique f"),
    @NamedQuery(name = "FormeJuridique.findByCode", query = "SELECT f FROM FormeJuridique f WHERE f.code = :code"),
    @NamedQuery(name = "FormeJuridique.findByIntitule", query = "SELECT f FROM FormeJuridique f WHERE f.intitule = :intitule"),
    @NamedQuery(name = "FormeJuridique.findByEtat", query = "SELECT f FROM FormeJuridique f WHERE f.etat = :etat"),
    @NamedQuery(name = "FormeJuridique.findByVisibleUtilisateur", query = "SELECT f FROM FormeJuridique f WHERE f.visibleUtilisateur = :visibleUtilisateur")})
public class FormeJuridique implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CODE")
    private String code;
    @Size(max = 150)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "VISIBLE_UTILISATEUR")
    private Boolean visibleUtilisateur;
    @OneToMany(mappedBy = "formeJuridique")
    private List<Personne> personneList;
    @OneToMany(mappedBy = "formeJuridique")
    private List<ParamTauxVignette> paramTauxVignettesList;

    public FormeJuridique() {
    }

    public FormeJuridique(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Boolean getVisibleUtilisateur() {
        return visibleUtilisateur;
    }

    public void setVisibleUtilisateur(Boolean visibleUtilisateur) {
        this.visibleUtilisateur = visibleUtilisateur;
    }

    @XmlTransient
    public List<Personne> getPersonneList() {
        return personneList;
    }

    public void setPersonneList(List<Personne> personneList) {
        this.personneList = personneList;
    }

    public List<ParamTauxVignette> getParamTauxVignettesList() {
        return paramTauxVignettesList;
    }

    public void setParamTauxVignettesList(List<ParamTauxVignette> paramTauxVignettesList) {
        this.paramTauxVignettesList = paramTauxVignettesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormeJuridique)) {
            return false;
        }
        FormeJuridique other = (FormeJuridique) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.FormeJuridique[ code=" + code + " ]";
    }
    
}
