/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_OPERATION_SITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OperationSite.findAll", query = "SELECT l FROM OperationSite l"),
    @NamedQuery(name = "OperationSite.findById", query = "SELECT l FROM OperationSite l WHERE l.id = :id"),
    @NamedQuery(name = "OperationSite.findByFkSite", query = "SELECT l FROM OperationSite l WHERE l.fkSite = :fkSite"),
    @NamedQuery(name = "OperationSite.findByFkAgent", query = "SELECT l FROM OperationSite l WHERE l.fkAgent = :fkAgent"),
    @NamedQuery(name = "OperationSite.findByDateOuvertureOperation", query = "SELECT l FROM OperationSite l WHERE l.dateOuvertureOperation = :dateOuvertureOperation"),
    @NamedQuery(name = "OperationSite.findByDateFermetureOperation", query = "SELECT l FROM OperationSite l WHERE l.dateFermetureOperation = :dateFermetureOperation"),
    @NamedQuery(name = "OperationSite.findByEtat", query = "SELECT l FROM OperationSite l WHERE l.etat = :etat")})

public class OperationSite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_SITE")
    private String fkSite;

    @Column(name = "FK_AGENT")
    private int fkAgent;
    
    @Column(name = "DATE_OUVERTURE_OPERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOuvertureOperation;
    
    @Column(name = "DATE_FERMETURE_OPERATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFermetureOperation;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    public OperationSite() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }

    public int getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(int fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Date getDateOuvertureOperation() {
        return dateOuvertureOperation;
    }

    public void setDateOuvertureOperation(Date dateOuvertureOperation) {
        this.dateOuvertureOperation = dateOuvertureOperation;
    }

    public Date getDateFermetureOperation() {
        return dateFermetureOperation;
    }

    public void setDateFermetureOperation(Date dateFermetureOperation) {
        this.dateFermetureOperation = dateFermetureOperation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OperationSite)) {
            return false;
        }
        OperationSite other = (OperationSite) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.OperationSite[ id=" + id + " ]";
    }
    
}
