/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_NATIONALITE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nationalite.findAll", query = "SELECT n FROM Nationalite n"),
    @NamedQuery(name = "Nationalite.findByCodeNationalitePopulation", query = "SELECT n FROM Nationalite n WHERE n.codeNationalitePopulation = :codeNationalitePopulation"),
    @NamedQuery(name = "Nationalite.findByLibelleNationalite", query = "SELECT n FROM Nationalite n WHERE n.libelleNationalite = :libelleNationalite")})
public class Nationalite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE_NATIONALITE_POPULATION")
    private Integer codeNationalitePopulation;
    @Size(max = 35)
    @Column(name = "LIBELLE_NATIONALITE")
    private String libelleNationalite;

    public Nationalite() {
    }

    public Nationalite(Integer codeNationalitePopulation) {
        this.codeNationalitePopulation = codeNationalitePopulation;
    }

    public Integer getCodeNationalitePopulation() {
        return codeNationalitePopulation;
    }

    public void setCodeNationalitePopulation(Integer codeNationalitePopulation) {
        this.codeNationalitePopulation = codeNationalitePopulation;
    }

    public String getLibelleNationalite() {
        return libelleNationalite;
    }

    public void setLibelleNationalite(String libelleNationalite) {
        this.libelleNationalite = libelleNationalite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codeNationalitePopulation != null ? codeNationalitePopulation.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nationalite)) {
            return false;
        }
        Nationalite other = (Nationalite) object;
        if ((this.codeNationalitePopulation == null && other.codeNationalitePopulation != null) || (this.codeNationalitePopulation != null && !this.codeNationalitePopulation.equals(other.codeNationalitePopulation))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Nationalite[ codeNationalitePopulation=" + codeNationalitePopulation + " ]";
    }
    
}
