/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_PERSONNE_EXEMPTEE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonneExemptee.findAll", query = "SELECT p FROM PersonneExemptee p"),
    @NamedQuery(name = "PersonneExemptee.findById", query = "SELECT p FROM PersonneExemptee p WHERE p.id = :id"),
    @NamedQuery(name = "PersonneExemptee.findByAgentCreat", query = "SELECT p FROM PersonneExemptee p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "PersonneExemptee.findByDateCreate", query = "SELECT p FROM PersonneExemptee p WHERE p.dateCreate = :dateCreate"),
    @NamedQuery(name = "PersonneExemptee.findByEtat", query = "SELECT p FROM PersonneExemptee p WHERE p.etat = :etat")})
public class PersonneExemptee implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "AGENT_CREAT")
    private Integer agentCreat;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne personne;

    public PersonneExemptee() {
    }

    public PersonneExemptee(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonneExemptee)) {
            return false;
        }
        PersonneExemptee other = (PersonneExemptee) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PersonneExemptee[ id=" + id + " ]";
    }
    
}
