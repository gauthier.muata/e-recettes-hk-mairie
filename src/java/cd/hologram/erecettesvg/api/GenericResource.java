/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.api;

import cd.hologram.erecettesvg.business.*;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.util.*;
import com.google.gson.JsonObject;

import java.util.*;
import java.util.logging.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.json.*;

/**
 * REST Web Service
 *
 * @author emmanuel.tsasa
 */
@Path("")
public class GenericResource {

    @Context
    private UriInfo context;

    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    /**
     * Retrieves representation of an instance of
     * cd.hologram.erecettesvg.api.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("login/{login}/{password}")
    public Object login(@PathParam("login") String login, @PathParam("password") String password) throws JSONException {

        System.out.println("Tentative de connexion de l'utilisateur " + login + "...");
        JSONObject data = new JSONObject();
        try {

            Agent agent = ConnexionBusiness.authentification(login, password);

            if (agent == null) {

                data.put("status", "210");
                data.put("message", "Utilisateur ou mot de passe incorrect");
                data.put("data", "");

            } else {

                List<Devise> deviseList = TaxationBusiness.getListAllDevises();

                if (!deviseList.isEmpty()) {
                    List<JSONObject> dataDeviseList = new ArrayList<>();
                    for (Devise devise : deviseList) {
                        JSONObject dataDevise = new JSONObject();
                        dataDevise.put("code", devise.getCode());
                        dataDevise.put("intitule", devise.getIntitule());
                        dataDevise.put("parDefaut", devise.getParDefaut().intValue());

                        List<JSONObject> dataTauxList = new ArrayList<>();
                        for (Devise deviseDest : deviseList) {
                            Taux taux = TaxationBusiness.getTauxByDevise(devise.getCode(), deviseDest.getCode());
                            if (taux != null) {
                                JSONObject dataTaux = new JSONObject();
                                dataTaux.put("devDest", taux.getDeviseDest());
                                dataTaux.put("taux", taux.getTaux());
                                dataTaux.put("date", ConvertDate.formatDateToString(taux.getDateCreat()));
                                dataTauxList.add(dataTaux);
                            }
                        }
                        dataDevise.put("tauxList", dataTauxList);
                        dataDeviseList.add(dataDevise);
                    }

                    String siteCode = agent.getSite() == null ? "" : agent.getSite().getCode();

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    List<JSONObject> dataAccesUserList = new ArrayList<>();
                    List<AccesUser> accesUsers = GestionUtilisateurBusiness.getAccesUserPeageByUser(agent.getCode());
                    if (!accesUsers.isEmpty()) {
                        for (AccesUser accesUser : accesUsers) {
                            JSONObject dataAU = new JSONObject();
                            dataAU.put("code", accesUser.getDroit());
                            dataAccesUserList.add(dataAU);
                        }
                    }

                    String codeSitePeage = GeneralConst.EMPTY_STRING;

                    if (sitePeageUser != null) {
                        codeSitePeage = sitePeageUser.getFkSitePeage().trim();
                    }

                    List<JSONObject> dataTarifList = getSpecifiquesTarifs(siteCode, codeSitePeage);

                    JSONObject dataAgent = new JSONObject();
                    dataAgent.put("code", agent.getCode());
                    dataAgent.put("nom", agent.toString());
                    dataAgent.put("fonction", agent.getFonction() == null ? "" : agent.getFonction().getIntitule());
                    dataAgent.put("grade", agent.getGrade());
                    dataAgent.put("service", agent.getService() == null ? "" : agent.getService().getIntitule());
                    dataAgent.put("siteCode", siteCode);
                    dataAgent.put("site", agent.getSite() == null ? "" : agent.getSite().getIntitule());
                    dataAgent.put("tarifs", dataTarifList);
                    dataAgent.put("devises", dataDeviseList);
                    dataAgent.put("droits", dataAccesUserList.isEmpty() ? "" : dataAccesUserList);

                    data.put("status", "100");
                    data.put("message", "Authentification réussie.");
                    data.put("dataAgent", dataAgent);

                    String sqlQuery = "UPDATE T_AGENT SET CONNECTE = 1, DATE_DERNIERE_CONNEXION = GETDATE() WHERE CODE = %s";
                    sqlQuery = String.format(sqlQuery, agent.getCode());

                    TaxationBusiness.updateEcheanePayment(sqlQuery);

                } else {

                    data.put("status", "220");
                    data.put("message", "Aucune configuration de taux");
                    data.put("data", "");

                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data.toString();

    }

    @POST
    @Path("changePassword")
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Produces({MediaType.APPLICATION_JSON + ";charset=UTF-8"})
    public Object updatePassword(@FormParam("userId") String userId, @FormParam("newPassword") String password) {
        JSONObject response = new JSONObject();
        try {
            boolean passwordChanged = ConnexionBusiness.updatePassword(userId, password);
            if (passwordChanged) {
                response.put("status", "100");
                response.put("message", "Le mot de passe a été modifié avec succès!");
            } else {
                response.put("status", "210");
                response.put("message", "L'opération a échoué!");
            }
        } catch (JSONException e) {
        }
        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("bienbyimm/{imm}")
    public Object getBienByImm(@PathParam("imm") String imm) throws JSONException {

        JSONObject data = new JSONObject();

        try {

            Bien bien = AssujettissementBusiness.getBienByImmatriculation(imm);

            if (bien == null) {

                data.put("status", "210");
                data.put("message", "Il n'y a aucun bien avec cette plaque ou numéro chassis.");
                data.put("data", "");

            } else {

                DetailAssujettissement da = AssujettissementBusiness.getDetailAssujettissementByIdBien(bien.getId());

                if (da == null) {

                    data.put("status", "220");
                    data.put("message", "Aucun tarif n'est configuré pour ce bien.");
                    data.put("data", "");

                } else {

                    Personne personne = IdentificationBusiness.getPersonneByCode(da.getFkPersonne().trim());

                    Personne otherPerson;

                    DetailsPrevisionCredit detailsPrevisionCredit = AssujettissementBusiness.getDetailsPrevisionCreditByAssujettissementID(
                            da.getId());

                    JSONObject detailPersonne = new JSONObject();
                    JSONObject detailPersonneWithCashMode = null;
                    JSONObject detailTaxe = new JSONObject();
                    JSONObject detailData = new JSONObject();
                    JSONObject detailBien = new JSONObject();
                    JSONObject detailTarif = new JSONObject();

                    if (detailsPrevisionCredit == null) {

                        detailsPrevisionCredit = AssujettissementBusiness.getDetailsPrevisionCreditByBien(
                                da.getBien().getId());

                        if (detailsPrevisionCredit != null) {

                            PrevisionCredit pc = AssujettissementBusiness.getAPrevisionCreditByID(detailsPrevisionCredit.getFkPrevisionCredit());

                            if (pc != null) {

                                if (!personne.getCode().equals(pc.getFkPersonne())) {

                                    detailPersonneWithCashMode = new JSONObject();

                                    detailPersonneWithCashMode.put("codePersonne", personne == null ? "" : personne.getCode());
                                    detailPersonneWithCashMode.put("nomsPersonne", personne == null ? "" : personne.toString().toUpperCase().trim().concat(GeneralConst.TWO_POINTS).concat("(CASH)"));
                                    detailPersonneWithCashMode.put("type", 3);
                                    detailPersonneWithCashMode.put("bienId", bien.getId());

                                }
                            }
                        }
                    }

                    if (detailsPrevisionCredit != null) {

                        List<DetailsPrevisionCredit> listDetailsPrevisionCredits = AssujettissementBusiness.getDetailsPrevisionCreditActifByBien(
                                bien.getId().trim());

                        if (!listDetailsPrevisionCredits.isEmpty()) {

                            List<JSONObject> detailPersonneList = new ArrayList<>();

                            for (DetailsPrevisionCredit dpc : listDetailsPrevisionCredits) {

                                detailPersonne = new JSONObject();

                                personne = AssujettissementBusiness.getPersonneByProvisionCredit(dpc.getFkPrevisionCredit().trim());

                                String type = GeneralConst.EMPTY_STRING;

                                if (dpc.getType() == GeneralConst.Numeric.ONE) {
                                    type = "(".concat("PRE-PAIEMENT").concat(")");
                                } else if (dpc.getType() == GeneralConst.Numeric.TWO) {
                                    type = "(".concat("CREDIT").concat(")");
                                }

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomsPersonne", personne == null ? "" : personne.toString().toUpperCase().trim().concat(GeneralConst.TWO_POINTS).concat(type));
                                detailPersonne.put("type", dpc.getType());
                                detailPersonne.put("bienId", dpc.getFkBien());

                                detailPersonneList.add(detailPersonne);
                            }

                            if (detailPersonneWithCashMode != null) {
                                detailPersonneList.add(detailPersonneWithCashMode);
                            }

                            detailData.put("personne", detailPersonneList);
                            detailData.put("taxe", "");
                            detailData.put("case", 2);
                            detailData.put("dataPC", "");
                            detailData.put("bien", "");
                            detailData.put("tarif", "");

                            data.put("status", "100");
                            data.put("message", "success");
                            data.put("data", detailData);
                        }

                    } else {

                        switch (da.getEtat()) {

                            case GeneralConst.Numeric.ONE:

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                                detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                                detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                                detailPersonne.put("exempte", 0);

                                break;

                            case GeneralConst.Numeric.TWO:

                                detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                                detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                                detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                                detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                                detailPersonne.put("exempte", 1);
                                break;
                        }

                        Assujettissement assuj = da.getAssujettissement();
                        ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                        detailTaxe.put("codeTaxe", ab.getCode());
                        detailTaxe.put("intituleTaxe", ab.getIntitule());

                        detailData.put("personne", detailPersonne);
                        detailData.put("taxe", detailTaxe);
                        detailData.put("case", 1);

                        detailData.put("dataPC", "");

                        bien = da.getBien();
                        String intitule = bien == null ? "" : bien.getIntitule();
                        String description = bien == null ? "" : bien.getDescription();

                        detailBien.put("idBien", bien == null ? "" : bien.getId());
                        detailBien.put("intituleBien", intitule);
                        detailBien.put("descriptionBien", description);
                        detailBien.put("inPC", 0);
                        detailBien.put("hasPC", 0);

                        detailTarif.put("taux", da.getTaux());
                        detailTarif.put("devise", da.getDevise() == null ? "" : da.getDevise().getCode());
                        detailTarif.put("codeTarif", da.getTarif().getCode());
                        detailTarif.put("intituleTarif", da.getTarif().getIntitule());

                        detailData.put("bien", detailBien);
                        detailData.put("tarif", detailTarif);

                        data.put("status", "100");
                        data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                        data.put("data", detailData);
                    }
                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue.");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return data.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("bienbyimmV2/{personneCode}/{bienCode}/{type}")
    public Object getBienByImmV2(
            @PathParam("personneCode") String personneCode,
            @PathParam("bienCode") String bienCode,
            @PathParam("type") String type) throws JSONException {

        JSONObject data = new JSONObject();

        String intitule = GeneralConst.EMPTY_STRING;
        String description = GeneralConst.EMPTY_STRING;

        try {

            Bien bien = AssujettissementBusiness.getBienByCode(bienCode);

            if (bien == null) {

                data.put("status", "210");
                data.put("message", "Il n'y a aucun bien avec code.");
                data.put("data", "");

            } else {

                Personne personne = IdentificationBusiness.getPersonneByCode(personneCode.trim());

                Assujettissement assuj;
                ArticleBudgetaire ab;

                JSONObject detailPersonne = new JSONObject();
                JSONObject detailTaxe = new JSONObject();
                JSONObject detailData = new JSONObject();
                JSONObject detailBien = new JSONObject();
                JSONObject detailTarif = new JSONObject();

                switch (type) {

                    case "3":

                        detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                        detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                        detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                        detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                        detailPersonne.put("exempte", 0);

                        DetailAssujettissement detAssuj = AssujettissementBusiness.getDetailAssujettissementByPersonneAndBien(
                                personneCode, bienCode);

                        assuj = detAssuj.getAssujettissement();
                        ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                        detailTaxe.put("codeTaxe", ab.getCode());
                        detailTaxe.put("intituleTaxe", ab.getIntitule());

                        detailData.put("personne", detailPersonne);
                        detailData.put("taxe", detailTaxe);
                        detailData.put("case", 1);

                        detailData.put("dataPC", "");

                        intitule = bien == null ? "" : bien.getIntitule();
                        description = bien == null ? "" : bien.getDescription();

                        detailBien.put("idBien", bien == null ? "" : bien.getId());
                        detailBien.put("intituleBien", intitule);
                        detailBien.put("descriptionBien", description);
                        detailBien.put("inPC", 0);
                        detailBien.put("hasPC", 0);

                        detailTarif.put("taux", detAssuj.getTaux());
                        detailTarif.put("devise", detAssuj.getDevise() == null ? "" : detAssuj.getDevise().getCode());
                        detailTarif.put("codeTarif", detAssuj.getTarif().getCode());
                        detailTarif.put("intituleTarif", detAssuj.getTarif().getIntitule());

                        detailData.put("bien", detailBien);
                        detailData.put("tarif", detailTarif);

                        data.put("status", "100");
                        data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                        data.put("data", detailData);

                        break;
                    default:
                        DetailsPrevisionCredit dpc = AssujettissementBusiness.getDetailsPrevisionCreditByPersonneAndBienAndType(
                                personneCode, bienCode, Integer.valueOf(type));

                        if (dpc != null) {

                            detailPersonne.put("codePersonne", personne == null ? "" : personne.getCode());
                            detailPersonne.put("nomPersonne", personne == null ? "" : personne.getNom() == null ? "" : personne.getNom());
                            detailPersonne.put("postnomPersonne", personne == null ? "" : personne.getPostnom() == null ? "" : personne.getPostnom());
                            detailPersonne.put("prenomPersonne", personne == null ? "" : personne.getPrenoms() == null ? "" : personne.getPrenoms());
                            detailPersonne.put("exempte", 2);

                            assuj = AssujettissementBusiness.getAssujettissementByID(dpc.getFkDetailAssujettissement());
                            ab = TaxationBusiness.getArticleBudgetaireByCode(assuj.getArticleBudgetaire());

                            DetailAssujettissement da = AssujettissementBusiness.getDetailAssujettissementByID(dpc.getFkDetailAssujettissement());

                            detailTaxe.put("codeTaxe", ab.getCode());
                            detailTaxe.put("intituleTaxe", ab.getIntitule());

                            detailData.put("personne", detailPersonne);
                            detailData.put("taxe", detailTaxe);

                            JSONObject dataPV = new JSONObject();

                            dataPV.put("code", dpc.getId());
                            dataPV.put("type", dpc.getType() == 1 ? "PRE-PAIEMENT" : "CREDIT");
                            dataPV.put("montantTotal", dpc.getTotalTaux() == null ? 0 : dpc.getTotalTaux());
                            dataPV.put("devise", dpc.getFkDevise() == null ? "" : dpc.getFkDevise());
                            dataPV.put("nombreTourInitial", dpc.getNbreTourSouscrit() == null ? 0 : dpc.getNbreTourSouscrit());
                            dataPV.put("nombreTourReel", dpc.getNbreTourUtilise() == null ? 0 : dpc.getNbreTourUtilise());
                            detailData.put("dataPC", dataPV);

                            intitule = bien == null ? "" : bien.getIntitule();
                            description = bien == null ? "" : bien.getDescription();

                            detailBien.put("idBien", bien == null ? "" : bien.getId());
                            detailBien.put("intituleBien", intitule);
                            detailBien.put("descriptionBien", description);
                            detailBien.put("inPC", 0);
                            detailBien.put("hasPC", 0);

                            detailTarif.put("taux", dpc.getTaux());
                            detailTarif.put("devise", dpc.getFkDevise() == null ? "" : dpc.getFkDevise());
                            detailTarif.put("codeTarif", da.getTarif().getCode());
                            detailTarif.put("intituleTarif", da.getTarif().getIntitule());

                            detailData.put("bien", detailBien);
                            detailData.put("tarif", detailTarif);

                            data.put("status", "100");
                            data.put("message", (description == null) ? intitule : intitule + " (" + description + ")");
                            data.put("data", detailData);

                        }
                        break;
                }

            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue.");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }

        return data.toString();
    }

    @POST
    @Path("synctickets")
    @Produces({MediaType.APPLICATION_FORM_URLENCODED})
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    //@Consumes({MediaType.APPLICATION_JSON})
    public String syncTickets(@FormParam("ticketsData") String ticketsData) {
        //public String syncTickets(String ticketsData) {

        boolean result = false;

        JsonObject response = new JsonObject();

        try {

            result = AssujettissementBusiness.saveTicketsPeage(ticketsData);

            if (result) {
                response.addProperty("status", "100");
                response.addProperty("message", "Synchronisation effectuée avec succès!");
            } else {
                response.addProperty("status", "200");
                response.addProperty("message", "Synchronisation echouée!");
            }

        } catch (Exception e) {
            response.addProperty("status", "210");
            response.addProperty("message", "Une erreur est survenue!");
        }

        return response.toString();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("refreshparam/{code}")
    public Object refreshParam(@PathParam("code") String code) throws JSONException {

        JSONObject data = new JSONObject();
        try {

            Agent agent = ConnexionBusiness.getAgentByCode(code);

            if (agent == null) {

                data.put("status", "210");
                data.put("message", "Problème d'authentification");
                data.put("data", "");

            } else {

                List<Devise> deviseList = TaxationBusiness.getListAllDevises();

                if (!deviseList.isEmpty()) {

                    List<JSONObject> dataDeviseList = new ArrayList<>();

                    for (Devise devise : deviseList) {
                        JSONObject dataDevise = new JSONObject();
                        dataDevise.put("code", devise.getCode());
                        dataDevise.put("intitule", devise.getIntitule());
                        dataDevise.put("parDefaut", devise.getParDefaut().intValue());

                        List<JSONObject> dataTauxList = new ArrayList<>();
                        for (Devise deviseDest : deviseList) {
                            Taux taux = TaxationBusiness.getTauxByDevise(devise.getCode(), deviseDest.getCode());
                            if (taux != null) {
                                JSONObject dataTaux = new JSONObject();
                                dataTaux.put("devDest", taux.getDeviseDest());
                                dataTaux.put("taux", taux.getTaux());
                                dataTaux.put("date", ConvertDate.formatDateToString(taux.getDateCreat()));
                                dataTauxList.add(dataTaux);
                            }
                        }
                        dataDevise.put("tauxList", dataTauxList);
                        dataDeviseList.add(dataDevise);
                    }

                    String siteCode = agent.getSite() == null ? "" : agent.getSite().getCode();

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    List<JSONObject> dataAccesUserList = new ArrayList<>();
                    List<AccesUser> accesUsers = GestionUtilisateurBusiness.getAccesUserPeageByUser(agent.getCode());
                    if (!accesUsers.isEmpty()) {
                        for (AccesUser accesUser : accesUsers) {
                            JSONObject dataAU = new JSONObject();
                            dataAU.put("code", accesUser.getDroit());
                            dataAccesUserList.add(dataAU);
                        }
                    }

                    String codeSitePeage = GeneralConst.EMPTY_STRING;

                    if (sitePeageUser != null) {
                        codeSitePeage = sitePeageUser.getFkSitePeage().trim();

                    }

                    List<JSONObject> dataTarifList = getSpecifiquesTarifs(siteCode, codeSitePeage);

                    JSONObject dataAgent = new JSONObject();
                    dataAgent.put("code", agent.getCode());
                    dataAgent.put("nom", agent.toString());
                    dataAgent.put("fonction", agent.getFonction() == null ? "" : agent.getFonction().getIntitule());
                    dataAgent.put("grade", agent.getGrade());
                    dataAgent.put("service", agent.getService() == null ? "" : agent.getService().getIntitule());
                    dataAgent.put("siteCode", siteCode);
                    dataAgent.put("site", agent.getSite() == null ? "" : agent.getSite().getIntitule());
                    dataAgent.put("tarifs", dataTarifList);
                    dataAgent.put("devises", dataDeviseList);
                    dataAgent.put("droits", dataAccesUserList);

                    data.put("status", "100");
                    data.put("message", "Authentification réussie.");
                    data.put("dataAgent", dataAgent);

                } else {

                    data.put("status", "220");
                    data.put("message", "Aucune configuration de taux");
                    data.put("data", "");

                }
            }

        } catch (JSONException ex) {
            data.put("status", "200");
            data.put("message", "Une erreur est survenue");
            data.put("data", "");
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data.toString();

    }

    public List<JSONObject> getSpecifiquesTarifs(String site, String siteAccorder) {

        List<JSONObject> tarifJsonList = new ArrayList<>();

        try {
            List<TarifSite> tarifSites = AssujettissementBusiness.getAllTaxesPeagesBySiteUserV2(site.trim(), siteAccorder);
            if (!tarifSites.isEmpty()) {
                for (TarifSite ts : tarifSites) {
                    JSONObject dataTarif = new JSONObject();
                    Tarif tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif());
                    dataTarif.put("code", tarif.getCode());
                    dataTarif.put("intitule", tarif.getIntitule().toUpperCase());
                    //dataTarif.put("valeur", tarif.getValeur());
                    //dataTarif.put("typeValeur", tarif.getTypeValeur());
                    dataTarif.put("montant", ts.getTaux());
                    dataTarif.put("devise", ts.getFkDevise());
                    tarifJsonList.add(dataTarif);
                }
            }
        } catch (Exception e) {
        }

        return tarifJsonList;
    }

}
