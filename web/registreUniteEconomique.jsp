<%-- 
    Document   : registreUniteEconomique
    Created on : 24 févr. 2021, 10:15:08
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Registre des unités économiques</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <!--link rel="stylesheet" href="assets/lib/css/font-awesome.css"--> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }

            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par
                            <select  class="form-control" id="cbxResearchTypeUniteEcono">
                                <option value="1" selected>Assujetti</option>
                                <option value="2">Unité économique</option>
                            </select>

                            <div class="input-group" id="divInputGroupSearch">
                                <input type="text" class="form-control" id="inputSearchUniteEconomique" placeholder="Nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchUniteEconomique">
                                        <i class="fa fa-search"></i> <span id="spanSearch">Rechercher</span>
                                    </button>
                                    <!--<button class="btn btn-success" id="btnCreateUniteEconomique" style="margin-left: 5px; display: inline">
                                        <i class="fa fa-plus-circle"></i> <span class="spanLibelles">Créer unité économique</span>
                                    </button>-->
                                </div>
                            </div>
                        </form>
                    </div>
                    <<button id="btnShowAdvancedSearchModal" style="margin-right: 20px" class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>                      
                </div>

                <hr/>

                <div class="divTableUniteEconomique">
                    <table id="tableUniteEconomique" class="table table-bordered" ></table>
                </div>


            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/modalCreateUniteEconomique.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script type="text/javascript" src="assets/js/rechercheAvancee.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>

        <script type="text/javascript" src="assets/js/gestionUniteEconomique/registreUniteEconomique.js"></script>
    </body>
</html>
