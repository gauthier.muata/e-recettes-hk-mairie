<%-- 
    Document   : affecterArticleBudgetaireBanque
    Created on : 24 août 2020, 11:50:52
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Affectation d'un article budgétaire dans une banque</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> Affectation des articles budgétaire dans des banques</h4>
                                    </div>
                                </div> 

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <p class="control-label col-sm-2" for="cmbBanque">
                                            Banque 
                                        <div class="col-sm-10">
                                            <select class="form-control" id="cmbBanque"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="control-label col-sm-2" for="cmbCompteBancaire">
                                            N°Compte 
                                        <div class="col-sm-10">
                                            <select class="form-control" id="cmbCompteBancaire"></select>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body" style="height: 500px;">

                                                <table width="100%" 
                                                       class="table table-responsive table-bordered table-hover" 
                                                       id="tableArticleAffecter">
                                                </table>                             

                                            </div>      
                                        </div>      
                                    </div>      
                                </div>      

                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <p class="control-label col-sm-2" for="cmbMinistere">
                                            Ministère 
                                        <div class="col-sm-10">
                                            <select class="form-control" id="cmbMinistere"></select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <p class="control-label col-sm-3" for="cmbServiceAssiette">
                                            Service d'assiette 
                                        <div class="col-sm-9">
                                            <select class="form-control" id="cmbServiceAssiette"></select>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body" style="height: 500px;">

                                                <table width="100%" 
                                                       class="table table-responsive table-bordered table-hover" 
                                                       id="tableArticleBudgetaire">
                                                </table> 

                                            </div>      
                                        </div>      
                                    </div> 
                                </div> 

                            </div>                  
                        </div> 

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <button  
                                        id="btnDesactiverArticle"
                                        class="btn btn-danger pull-left">
                                        <i class="fa fa-close"></i>  
                                        Désactiver article
                                    </button>

                                    <button  
                                        id="btnEnregistrerArticle"
                                        class="btn btn-success pull-right">
                                        <i class="fa fa-save"></i>  
                                        Affecter article
                                    </button>
                                </div>                      
                            </div>                      
                        </div> 
                    </div>                  
                </form>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/articleBudgetaire/affecterArticleBudgetaireBanque.js"></script>
    </body>
</html>
