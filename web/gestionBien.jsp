<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des unités économiques</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <!--link rel="stylesheet" href="assets/lib/css/font-awesome.css"--> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU CONTRIBUABLE </h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <span id="lbl1" style="font-size: 16px;display: none">Type contribuable :</span>
                                        <span  >
                                            <p id="lblLegalForm"
                                               style="font-weight: bold;color: black"></p>
                                        </span><br/>
                                        <span id="lbl2" style="font-size: 16px;display: none">Noms :</span>
                                        <span  >
                                            <b id="lblNameResponsible" ></b>
                                        </span><br/><br/>
                                        <span id="lbl3" style="font-size: 16px;display: none">Adresse :</span>
                                        <span  >
                                            <p id="lblAddress"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span>
                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchResponsable"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un contribuable
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DES UNITES ECONOMIQUES 
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <!--               -->

                                        <ul class="nav nav-tabs">
                                            <li id="tabBiens" class="active"><a data-toggle="tab" href="#idBien">LISTE DES UNITES ECONOMIQUES</a></li>
                                            <li id="tabArticles"><a data-toggle="tab" href="#idArticle">LES ACTES GENERATEURS</a></li>
                                        </ul>

                                        <div class="tab-content">

                                            <div id="idBien" class="tab-pane fade in active">
                                                <!--Liste des biens de l'assujetti-->


                                                <div class="panel-body">  </div> 
                                             

                                                <div class="btn btn-default" role="group"  style="width: 100%">
                                                    <button type="button" class="btn btn-secondary" id="btnNewUniteEconomique"><i class="fa fa-plus-square"></i> Créer une unit&eacute; économique</button>
                                                   <!--<button type="button" class="btn btn-secondary" id="btnNewBien"><i class="fa fa-car"></i>Bien automobile </button>
                                                   <button type="button" class="btn btn-secondary" id="btnNewBienImmobilier"><i class="fa fa-home"></i>Bien immobilier</button>
                                                    <!--<button type="button" class="btn btn-secondary" id="btnNewBienConcession"><i class="fa fa-paper-plane"></i> Concession minière</button>
                                                    <button type="button" class="btn btn-secondary" id="btnNewBienPanneauPublicitaire"><i class="fa fa-paw"></i> Panneau publicitaire</button>
                                                    <button type="button" class="btn btn-secondary" id="btnLocateBien"><i class="fa fa-plus-circle"></i> Louer bien</button>
                                                    <button type="button" class="btn btn-secondary" id="btnPermuteBien"><i class="fa fa-exchange"></i> Permutation bien</button>
                                                    <button type="button" class="btn btn-warning" id="btnPasserAB"><i class="fa fa-link"></i> Passer à l'assujettissement</button>-->


                                                </div>


                                                <div class="divTableBien">
                                                    <table id="tableBiens" class="table table-bordered" ></table>
                                                </div>

                                                <label class="lblTxtAssuj" style="font-weight: normal;font-size: 12px">
                                                    Cliquer sur un bien si vous voulez le prendre en compte dans l'assujettissement.
                                                </label>


                                            </div>


                                            <div id="idArticle" class="tab-pane fade">
                                                <!--Liste des articles budgetaires-->
                                                <div class="divTableArticle">
                                                    <table id="tableArticles" class="table table-bordered"></table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="modal fade" id="modalPeriodeDeclaration" data-backdrop="static" data-keyboard="false"
                         tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document" style="width: 85%">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Périodes des déclarations</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="responsive">
                                                <div class="table-responsive">
                                                    <table
                                                        class="table table-bordered table-hover" 
                                                        id="tablePeriodeDeclaration">                                                       
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="modal-footer">
                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/chooseAB.html" %>
        <%@include file="assets/include/modal/locationBien.html" %>
        <%@include file="assets/include/modal/modalPermutationBien.html" %>
        <%@include file="assets/include/modal/modalCreateUniteEconomique.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien3.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/locationBien.js"></script>
        <script type="text/javascript" src="assets/js/gestionBien/permutationBien.js"></script>
        <script type="text/javascript" src="assets/js/gestionUniteEconomique/editerUniteEconomique.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAdressePersonne.js"></script>
    </body>

</html>
