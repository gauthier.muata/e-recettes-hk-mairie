<%-- 
    Document   : taxation
    Created on : 16 juin 2018, 08:03:05
    Author     : gauthier.muata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Taxation unité économique </title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            #inputValueResearchBudgetArticle{width: 400px}
            @media (max-width: 768px) {
                #divTableTaxation{overflow: auto}
                .cArchBudgetArticle{width: 100%}
                #btnAddBudgetArticle{margin-top: 3px}
                #inputValueResearchBudgetArticle{width: 100%}
                #spanSearchAB{display: none}
                #divTableBudgetArticles{overflow: auto}
            }           
        </style>

    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div  class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">INFORMATION SUR LE CONTRIBUABLE
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                  <div class="panel-body" style="margin-top: -4px"> 
                                      <div class="row">
                                          <div style="font-size: 16px;" class="col-md-2">Nom :</div>
                                          <div  style="font-size: 16px;font-weight: bold" class="col-md-10"><span id="lblName"></span></div>
                                      </div>
                                      <br/>
                                      <div class="row">
                                           <div style="font-size: 16px;" class="col-md-2">Unité économique :</div>
                                           <div style="font-size: 16px;font-weight: bold" class="col-md-10"><span id="lblUniteEco"></span></div>
                                      </div>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div  class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">INFORMATION SUR LA TAXE
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body" style="margin-top: -4px"> 
                                    <fieldset  class="fieldset">
                                        <span  id="lblBudgetArticle">
                                            <label style="font-size: 10px"></label>
                                        </span><hr/>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <label  for="inputRate" >Taux</label>
                                                <input type="text" class="form-control" id="inputRate" readonly="true"/>
                                            </div>
                                            <div class="col-lg-3">
                                                <label  for="inputBaseCalcul">Base &nbsp;<span style="font-weight: lighter;color: #08C" id="lblUniteBase"></span> </label>
                                                <input type="number" min="1" value="1"
                                                       class="form-control" 
                                                       id="inputBaseCalcul"
                                                       required="true"/>
                                                <label  for="lblUnityBaseCalcul"><span style="font-weight: lighter;color: #08C" id="lblUnityBaseCalcul"></span> </label>
                                            </div>
                                            <div class="col-lg-3">
                                                <label  for="inputQuantity">Nbre actes</label>
                                                <input type="number" class="form-control" id="inputQuantity"
                                                       value="1" min="1" required="true"/>
                                            </div>
                                            <div class="col-lg-3">
                                                <label  for="inputAmount">Montant Dû</label>
                                                <input type="text" 
                                                       class="form-control" id="inputAmount" 
                                                       readonly="true" style="border: 2px solid green;width: 100%"/>

                                            </div>
                                        </div><br/>
                                    </fieldset>
                                </div>
                                <div class="panel-footer" style="margin-top: 15px">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <button id="btnSearchBudgetArticle" class="btn btn-primary cArchBudgetArticle">
                                                <i class="fa fa-search"></i> Rechercher une taxe
                                            </button>
                                        </div>
                                        <div class="col-lg-6" >
                                            <button id="btnSaveTaxation" class="btn btn-success pull-right cArchBudgetArticle">
                                                <i class="fa fa-save"></i> Enregistrer la taxation
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="budgetArticleModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Registre des articles budgétaires</h5>
                    </div>
                    <div class="modal-body" id="divBudgetArticle">
                        <div class="navbar-form" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputValueResearchBudgetArticle" placeholder="Saisir l'article que vous rechercher ici">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnResearchBudgetArticle">
                                        <i class="fa fa-search"></i><span id="spanSearchAB">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div id="divTableBudgetArticles">
                            <table class="table table-bordered table-hover" id="tableBudgetArticles"></table> 
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" 
                                id="btnSelectedBudgetArticle"
                                style="display: none">
                            <i class="fa fa-check-circle"></i>
                            S&eacute;lectionner
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="tarifBudgetArticleModal" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Sélection du tarif de l'article budgétaire</h5>
                    </div>
                    <div class="modal-body" id="divTarifBudgetArticle">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez sélectionner le tarif : </label>
                            <div class="col-lg-12">
                                <select class="form-control" id="cmbTarif" 
                                        placeholder="Tarif de l'article budgétaire">
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnSelectedTarif">
                            <i class="fa fa-check-circle"></i>Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="infosBaseCalculModal" role="dialog" data-backdrop="static" data-keyboard="false" >

            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" id="exampleModalLongTitle">Informations de la borne de calcul</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez fournir la valeur de la base en &nbsp;
                                <span style="font-weight: lighter;color: #08C" id="lblUniteBase2"></span> : 
                            </label>
                            <div class="col-sm-12">
                                <input class="form-control" id="inputBaseCalculOnCasePourcent" placeholder="Base de calcul" type="number" min="1" value="1">
                            </div>
                            <p id="lblExampleBase" style="color: red;margin-left: 3px;font-size: 13px" class="control-label col-sm-12" > </p>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateInfoBase">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalTransactionnel" 
             role="dialog"
             data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Informations de l'amende transactionnel</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez fournir le montant d&ucirc;  : </label>

                            <div class="col-sm-12" style="margin-top: 10px">
                                <input class="form-control" id="inputMontanTransactionnel" 
                                       placeholder="Montant dû"
                                       type="number" min="1" value="1">
                                </input>
                            </div>
                            <p id="lblTransactionnelValue"
                               style="color: red;margin-left: 3px;margin-top: 15px;font-size: 15px"
                               class="control-label col-sm-12" >
                                Le montant d&ucirc; doit être compris entre 2000 et 3000 (Devise)
                            </p>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateTTransactionnel">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>        

<%@include file="assets/include/modal/header.html" %>
<%@include file="assets/include/modal/rechercheAssujetti.html" %>
<%@include file="assets/include/modal/modalUpload.html" %>
<%@include file="assets/include/modal/rechercheBienPersonne.html" %>
<%@include file="assets/include/modal/modalComplementInfoTaxe.html" %>

<script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
<script src="assets/lib/js/datatables.min.js" type="text/javascript"></script>
<script src="assets/lib/js/dataTables.select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>
<script type="text/javascript" src="assets/js/taxation.js"></script>
<script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
<script type="text/javascript" src="assets/js/rechercheBienPersonne.js"></script>
<script type="text/javascript" src="assets/js/uploading.js"></script>
</body>
</html>
