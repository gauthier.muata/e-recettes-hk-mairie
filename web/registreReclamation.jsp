<%-- 
    Document   : registreReclamation
    Created on : 20 juin 2019, 11:10:33
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Registre des réclamations</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearch">
                                <option value="1">Assujetti</option>
                                <option value="2">Référence du courrier</option>
                                <option value="3">Numéro document</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" disabled="true" class="form-control" style="width: 300px" id="inputResearchReclamation" placeholder="Assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" id="btnSearch">
                                        <i class="fa fa-search"></i> 
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnCallModalSearchAvanded" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée
                    </button>
                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour:</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">
                                        <span id="lbl1" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Type réclamaion:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblTypeReclamation" >&nbsp;</b>
                                        </span> 

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <div class="responsive">
                        <table id="tableReclamation" class="table table-bordered" > </table>
                    </div>

                </div>

            </div>

            <div class="modal fade" id="modalDetailReclaration" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="width: 75%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Registre des réclamations --> Les détails de la réclamation</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations de la réclamation</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label style="font-weight: normal" >
                                                    ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblNameAssujetti">&nbsp;</b>
                                            </div>
                                            <div class="col-lg-12">
                                                <label style="font-weight: normal" >
                                                    TYPE ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblTypeAssujetti">&nbsp;</b>
                                            </div>
                                            <div class="col-lg-12">
                                                <label style="font-weight: normal" >
                                                    ADRESSE PRINCIPALE &nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblAdresseAssujetti">&nbsp;</b>
                                            </div>
                                            <div class="col-lg-12">
                                                <label style="font-weight: normal" >
                                                    DATE VALIDATION &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblDateValidation">&nbsp;</b>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-warning pull-right" id="btnVisualiserLettreReclamation" title="Visualiser la lettre de réclamation" >
                                            <i class="fa fa-archive"></i> Visualiser lettre réclamation
                                        </button>

                                        <button style="display: none" type="button" class="btn btn-success pull-right" id="btnShowAccuserReception">
                                            <i class="fa fa-check-circle"></i> Accuser reception
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <hr style="margin-top: -10px"/>

                            <div style="display: none" id="divRadio" class="form-check">
                                <input class="form-check-input" type="radio" name="radioTraiter" id="radioValider" value="1" >
                                <label for="radioValider" style="margin-right: 40px">APPROUVER</label>
                                <input class="form-check-input" type="radio" name="radioTraiter" id="radioRejeter" value="0" >
                                <label for="radioRejeter">REJETER</label>
                                <hr/>
                            </div>

                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Les titres d'AMR</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="responsive">
                                        <div class="table-responsive">
                                            <table 
                                                class="table table-bordered table-hover" 
                                                id="tableTitreAMR">                                                       
                                            </table> 
                                        </div>                                       
                                    </div>
                                    <div class="panel-footer"  style="text-align: right">
                                        <button style="display: none" type="button" class="btn btn-success pull-left" id="btnValiderReclamation" >
                                            <i class="fa fa-check-circle"></i> Valider la réclamation
                                        </button>

                                        <%--  <button type="button" id="btnDisplayObservation" class="btn btn-warning">
                                            <i class="fa fa-list"></i> Afficher les observations
                                        </button>  %--%>

                                        <button style="display: none"
                                                type="button" 
                                                class="btn btn-primary" 
                                                id="btnPrintBP">
                                            <i class="fa fa-print"></i>  
                                            Imprimer bon à payer 
                                        </button>

                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" >
                                            Fermer
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalVoirPlus" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-dialog-centered" style="width: 60%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" id="locationBienTitle">Détails de la r&eacute;clamation</h5>
                        </div>
                        <div class="modal-body" >

                            <div class="row">
                                <div class="col-lg-12">

                                    <label for="lblReferenceDocument" style="font-weight: normal">
                                        REFERENCE DOCUMENT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                    </label>
                                    <b id="lblReferenceDocument">&nbsp;</b><br/><br/>

                                    <div class="panel panel-primary">

                                        <div class="panel-heading">
                                            <h4 class="panel-title"> Autres informations</h4>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body"> 
                                                <div class="form-group" >

                                                    <label for="lblDemandeSurcis" style="font-weight: normal">
                                                        DEMANDE SURCIS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblDemandeSurcis">&nbsp;</b><br/>

                                                    <label for="lblSurcisAccorde" style="font-weight: normal">
                                                        SURCIS ACCORDE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblSurcisAccorde">&nbsp;</b><br/><br/>

                                                    <label for="textMotif" style="font-weight: normal">
                                                        MOTIF DE LA RECLAMATION
                                                    </label>
                                                    <textarea id="textMotif" readonly="true" style="width: 100%" rows="7"></textarea>
                                                </div>

                                            </div>
                                            <div class="panel-footer" style="text-align: right">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >
                                                    Fermer
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalReclamationAdvancedSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Recherche avanc&eacute;e par type et date réclamation</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label>Type réclamaion</label>
                                    <div class="input-group"  style="width: 100%;">
                                        <select  class="form-control" id="selectTypeReclamation">
                                            <option value="*">*</option>
                                            <option value="1">Récouvrement</option>
                                            <option value="2">Taxation</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Du :</label>
                                    <div class="input-group date " id="datePicker1" style="width: 100%;">
                                        <input type="text" class="form-control" id="inputDateDebut" >
                                        <div class="input-group-addon">
                                            <span class="fa fa-th"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Au :</label>
                                    <div class="input-group date" id="datePicker2" style="width: 100%;">
                                        <input type="text" class="form-control" id="inputdateLast">
                                        <div class="input-group-addon">
                                            <span class="fa fa-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btnAdvancedSearch">
                                <i class="fa fa-search"></i> &nbsp;Rechercher
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalTraitementDecision" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document" style="width: 55%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Gestion des décisions administratives </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="panel panel-primary" id="divInfoTraitement" >
                                <div class="panel-heading">
                                    <h4 class="panel-title">Traitement de la décision</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label>Décision:</label>
                                            <select class="form-control" id="selectDecision">
                                                <option value="*">*</option>
                                            </select>
                                        </div> 

                                        <form id="formRadioButton">
                                            <div id="divRadio" class="form-check">
                                                <input class="form-check-input " type="radio" name="radioValider" checked id="radioDegreveTotal" value="2" >
                                                <label style="margin-right: 40px" class="form-check-label" for="radioValider">DEGREVEMENT TOTAL</label>
                                                <input class="form-check-input " type="radio" name="radioValider" id="radioDegrevePartiel" value="1" >
                                                <label class="form-check-label" for="radioRejeter">DEGREVEMENT PARTIEL</label>

                                            </div>                                        
                                        </form>


                                        <div class="form-group">

                                            <div id="divMontantDu">
                                                <hr/>
                                                <label for="lblMontantDu" style="font-weight: normal" >
                                                    Montant dû (Titre d'avis de mise en recouvrement contesté) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblMontantDu">&nbsp;</b>
                                                <br/>

                                                <label for="lblPartieMontantConteste" style="font-weight: normal" >
                                                    Partie du montant contesté &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblPartieMontantConteste">&nbsp;</b>

                                                <br/>

                                                <label for="lblCinqPourcentPartieConteste" style="font-weight: normal" >
                                                    20% de la partie non contesté &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblCinqPourcentPartieConteste">&nbsp;</b>
                                                <hr/> 
                                            </div>

                                            <div class="">
                                                <br/>
                                                <label style="font-weight: normal">MONTANT DEGREVE : </label>
                                                <input class="form-control" type="number" min="1" id="inputDegreveAmount"/>
                                            </div>
                                            <hr/>

                                            <div id="divRestPayer">
                                                <label for="lblRestePayer" style="font-weight: normal" >
                                                    Reste à payer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                </label>
                                                <b id="lblRestePayer" style="color: red">&nbsp;</b>
                                            </div>
                                        </div> 

                                    </div>
                                    <div class="panel-footer" style="text-align: right">
                                        <button                    
                                            type="button" 
                                            class="btn btn-warning pull-left" style ="display: none"
                                            id="btnJoindreDecisionAdmin">
                                            <i class="fa fa-download"></i>  
                                            Joindre d&eacute;sion administrative
                                        </button>

                                        <a 
                                            id="lblNbArchuiveDecisionAdminstrative"
                                            class="pull-left"
                                            style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                                        </a>

                                        <button type="button" class="btn btn-success" id="btnValiderTraitement">
                                            <i class="fa fa-check-circle"></i>
                                            Valider
                                        </button>
                                        <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal" >Fermer</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalDemandeRecoursJ" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-dialog-centered" style="width: 55%">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" id="locationBienTitle">RECOURS JURIDICTIONNEL</h5>
                        </div>
                        <div class="modal-body" id="divLocationRecours">

                            <div class="row">
                                <div class="col-lg-12">

                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"> Informations du recours</h4>
                                        </div>
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body"> 

                                                <div id="panelEditionRecours" style="">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label style="font-weight: normal" >
                                                                ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                            </label>
                                                            <b id="lblNameAssujettiRecours">&nbsp;</b>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <label style="font-weight: normal" >
                                                                TYPE ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                            </label>
                                                            <b id="lblTypeAssujettiRecours">&nbsp;</b>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <label style="font-weight: normal" >
                                                                ADRESSE PRINCIPALE &nbsp;&nbsp;: 
                                                            </label>
                                                            <b id="lblAdresseAssujettiRecours">&nbsp;</b>
                                                        </div>

                                                    </div>

                                                    <hr/>

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Numéro enregistrement greffé <span style="color:red"> *</span>
                                                                </label>
                                                                <input type="text" class="form-control" id="inputNumeroEnregGreffe" placeholder="N de l'acte notarie"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Date dépôt recours <span style="color:red"> *</span>
                                                                </label>
                                                                <input type="date" class="form-control" id="inputDateDepotCourrier" placeholder="Date d'acquisition"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Date(s) de l'audience <span style="color:red"> *</span>
                                                                </label>
                                                                <div class="input-group date" data-provide="datepicker">
                                                                    <input id="inputDateAudience" type="text" readonly="true" class="form-control"
                                                                           style="background-color: white">
                                                                    <div  class="input-group-addon">
                                                                        <span class="glyphicon glyphicon-th" ></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Motif du recours <span style="color:red"> *</span>
                                                                </label>
                                                                <textarea id="textMotifRecours" class="form-control" rows="7"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div id="panelRegistreRecours" style="display: none">

                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Numéro enregistrement greffé <span style="color:red"> *</span>
                                                                </label>
                                                                <label class="form-control" id="labelNumeroEnregGreffe"></label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Date dépôt recours <span style="color:red"> *</span>
                                                                </label>
                                                                <label class="form-control" id="labelDateDepotCourrier" ></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Date(s) de l'audience <span style="color:red"> *</span>
                                                                </label>
                                                                <label class="form-control" id="labelDateAudience" ></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label style="font-weight: normal">
                                                                    Motif du recours <span style="color:red"> *</span>
                                                                </label>
                                                                <label class="form-control" id="labelMotifRecours" style="border:1px solid #ccc;width: 100%;padding: 2px"></label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="panel-footer" style="text-align: right">
                                                <button  
                                                    id="btnCreerRecours"
                                                    class="btn btn-success">
                                                    <i class="fa fa-check-circle"></i> Créer un recours juridictionnel
                                                </button>

                                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >
                                                    Fermer
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/accuserReception.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/observationModal.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>       


        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/registreReclamation.js"></script>
        <script  type="text/javascript" src="assets/js/accuserReception.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>
    </body>
</html>
