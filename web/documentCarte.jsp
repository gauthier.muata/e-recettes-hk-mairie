<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : moussa.toure
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="stylesheet" href="assets/lib/css/style.css">
        <title>Impression de la carte</title>



    </head>

    <body  id="document_container">

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script> 
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>  
        <script type="text/javascript">

            documentContainer = $('#document_container');
            var content = getDocumentContent();
            $('#document_container').html(content);

        </script>


    </body> 
</html>
