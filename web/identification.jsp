<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Identification d'un contribuable</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/intl-tel-input/build/css/intlTelInput.css" rel="stylesheet" type="text/css"/>

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .spanModalSearchAssuj{display: none;}
                .divInputGroupSearch{width:80%;float:left}
                .divAdressModal{overflow: auto}
            }
        </style>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >
                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-lg-12">                         
                            <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>
                            <button class="btn btn-primary" id="btnEnregistrer" style="float: right">
                                <i class="fa fa-save"></i>  Enregistrer
                            </button>
                            <button class="btn btn-warning" id="btnCallRegister" style="float: right; margin-right: 5px">
                                <i class="fa fa-list"></i>  Répertoire des contribuables
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">INFORMATIONS DE BASE</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="cmbFormeJuridique">
                                                Type <span style="color:red"> *</span></p>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="cmbFormeJuridique" placeholder="Type de l'assujetti"></select>
                                            </div>
                                        </div>                 

                                        <div 
                                            id="divNinfInfo"
                                            class="form-group" style="display: none">
                                            <p class="control-label col-sm-4" 
                                               for="inputNIF"
                                               id="lblNif">
                                                Nif
                                            </p>
                                            <div class="col-sm-8">
                                                <input type="text" 
                                                       class="form-control" 
                                                       id="inputNIF"
                                                       placeholder="Numéro d'identification fiscale"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputNom">
                                                Nom/Raison sociale <span style="color:red"> *</span></p>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputNom" placeholder="Nom / Raison sociale"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputPostNom">
                                                Post-nom/Sigle <!--<span style="color:red"> *</span></p>-->
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputPostNom" placeholder="Post-nom / Sigle"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputPrenom">Prénom</p>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputPrenom" placeholder="Autres"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputTelephone">
                                                Téléphone <span style="color:red"> *</span></p>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputTelephone" placeholder="Téléphone"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputEmail">E-mail</p>
                                            <div class="col-sm-8">
                                                <input type="email" class="form-control" id="inputEmail" placeholder="E-mail"/>
                                            </div>
                                        </div>

                                        <div class="form-group" style="display: none">
                                            <p class="control-label col-sm-4" for="inputTeleDeclaration">Télé déclaration</p>
                                            <div class="col-sm-8">
                                                <input class="form-check-input" type="checkbox" name="checkboxTeleDeclaration" id="checkboxTeleDeclaration" value="1" >                                    
                                            </div>
                                        </div>

                                        <br/>
                                        <fieldset class="fieldset" >

                                            <legend style="font-size: 16px;color: #9e9e9e">
                                                Adresse(s) <span style="color:red"> *</span></legend>

                                            <div >
                                                <table 
                                                    width="100%" 
                                                    class="table table-bordered table-hover"
                                                    id="tableAdressesPersonne">
                                                </table>

                                                <label id="labelInfo"
                                                       style="font-weight: normal;font-size: 13px;color: #449d44;">  
                                                    <br/>
                                                </label>

                                                <button class="btn btn-success" id="btnAjouterAdresse"
                                                        style="float: right">
                                                    <i class="fa fa-plus-circle"></i>  
                                                    Ajouter une adresse
                                                </button>

                                            </div>

                                        </fieldset>

                                    </div>

                                    <div class="modal fade" id="modalAdresses" role="dialog" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-dialog modal-dialog-centered modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Registre des adresses</h5>
                                                </div>
                                                <div class="modal-body" id="divAdresses">
                                                    <div class="navbar-form" role="search">
                                                        <div class="row">
                                                            <div class="col-lg-6 divInputGroupSearch">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" id="inputLibelle" 
                                                                           placeholder="Veuillez saisir l'adresse">
                                                                    <div class="input-group-btn">
                                                                        <button class="btn btn-primary" id="btnRechercherAdresses2">
                                                                            <i class="fa fa-search-plus"></i>
                                                                            <span class="spanModalSearchAssuj">Rechecher</span>                                                                    
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <a id="btnAddEntite"
                                                                   class="btn btn-warning pull-right">
                                                                    <i class="fa fa-plus-circle"></i>  
                                                                    <span class="spanModalSearchAssuj">Nouvelle adresse</span>                                                        
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="divAdressModal">
                                                        <table 
                                                            width="100%" 
                                                            class="table table-bordered table-hover" 
                                                            id="tableAdresses">                                                       
                                                        </table>
                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <div class="navbar-form" >
                                                        <label id="labelSelectedAdresse" style="float:left;font-weight: normal"></label>
                                                        <div class="input-group" >
                                                            <input type="text" 
                                                                   class="form-control" 
                                                                   id="inputNumber" 
                                                                   placeholder="Numéro" 
                                                                   size="5"
                                                                   style="visibility: hidden">

                                                            <div class="input-group-btn">
                                                                <button 
                                                                    class="btn btn-success" 
                                                                    id="btnValiderAdresse"
                                                                    style="visibility: hidden">
                                                                    <i class="fa fa-check-circle"></i>
                                                                    Valider
                                                                </button>
                                                                <button class="btn btn-default" 
                                                                        id="btnValiderAdresse"
                                                                        data-dismiss="modal">
                                                                    Fermer
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-default" id="divPanelComplement">
                                <div class="panel-heading">
                                    <h4 class="panel-title">INFORMATIONS COMPLEMENTAIRES
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body" id="divComplement"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/addEntiteAdministrative.xhtml" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/intl-tel-input/build/js/intlTelInput.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/identification.js"></script>
        <script type="text/javascript" src="assets/js/entityAdministrative.js"></script>
    </body>
</html>
