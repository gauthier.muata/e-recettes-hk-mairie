<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des d&eacute;faillants en paiement</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html"%>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">

                            Filtrer par 
                            <select  class="form-control" id="researchType">
                                <!--option value="0" >--</option-->
                                <option value="1">Assujetti</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 380px" 
                                       id="assujettiCodeValue" 
                                       readonly="true"
                                       placeholder="Veuillez sélectionner l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnSimpleSearch"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">


                                    <span style="font-size: 16px; color: black">
                                        &nbsp;&nbsp;&nbsp;&nbsp;Service : &nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span style="font-size: 16px; color: black">
                                        <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                    </span>

                                    <span style="font-size: 16px; color: black">
                                        &nbsp;&nbsp;&nbsp;&nbsp;Site : &nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span style="font-size: 16px; color: black" >
                                        <b style="font-style: italic" id="lblSite" >&nbsp;&nbsp;&nbsp;</b>
                                    </span>

                                    <span style="font-size: 16px; color: black" >
                                        &nbsp;&nbsp;&nbsp;&nbsp;Date début : &nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span style="font-size: 16px; color: black" >
                                        <b style="font-style: italic" id="lblDateDebut" >&nbsp;&nbsp;&nbsp;</b>
                                    </span>

                                    <span style="font-size: 16px; color: black" >
                                        &nbsp;&nbsp;&nbsp;&nbsp;Date fin : &nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span style="font-size: 16px; color: black" >
                                        <b style="font-style: italic" id="lblDateFin" >&nbsp;&nbsp;&nbsp;</b>
                                    </span>


                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <button class="btn btn-sm btn-primary pull-right rapport-defaillant-paiement hidden">
                        <i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer
                    </button>
                    <table id="tableDefaillantPaiement" class="table table-bordered">

                    </table>
                </div>


            </div>
        </div>

        <div class="modal fade" id="medPrintInviteService" data-backdrop="static" data-keyboard="false"
             tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title">Impression de l'invitation de service</h5>
                    </div>

                    <div class="modal-body">

                        <label style="font-weight: normal">
                            Etes-vous sûre de vouloir imprimer une invitation de service pour cette note de perception ?
                        </label>

                        <br/>

                        <form>
                            <div class="form-group">
                                <input id="inputDateHeureInvitation" class="form-control" placeholder="Date heure invitation (Ex: Jeudi 19/11/2020 à 09h00)" />
                            </div>
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" id="btnPrintInvitation"> <i  class="fa fa-print"></i> Imprimer</button>
                        <a title="Fermer" class="btn btn-secondary" data-dismiss="modal">Fermer</a>
                    </div>
                </div>
            </div>
        </div>


        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/accuserReception.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
        <%@include file="assets/include/modal/modalInfoMed.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        
        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>
        
        <!--script src="assets/js/rechercheAvancee_1.js" type="text/javascript"></script-->
        <!--script src="assets/js/paiement.js" type="text/javascript"></script-->
        <script src="assets/js/resteArecouvrer/recouvrement.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAvancee.js"></script>
        <script src="assets/js/uploading.js" type="text/javascript"></script>
        <script src="assets/js/accuserReception.js" type="text/javascript"></script>

    </body>
</html>
