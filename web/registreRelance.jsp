<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des relances</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html"%>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">

                            Filtrer par 
                            <select  class="form-control" id="researchType">
                                <!--option value="0" >--</option-->
                                <option value="1">Assujetti</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 380px" 
                                       id="assujettiCodeValue" 
                                       readonly="true"
                                       placeholder="Veuillez sélectionner l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnSimpleSearch"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Article budgétaire : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblArticleBudgetaire" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Période : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblPeriodeDeclaration" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal">
                    <table id="tableDefaillants" class="table table-bordered"></table>
                </div>

            </div>


            <div class="modal fade" id="modalRelanceChoosePourcent" data-backdrop="static" data-keyboard="false"
                 tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h5 class="modal-title">Choix du taux (pourcentage)</h5>
                        </div>

                        <div class="modal-body">

                            <label style="font-weight: normal">
                                Veuillez sélectionner le pourcentage à appliquer
                            </label>

                            <br/>

                            <form>
                                <div class="form-group">
                                    <select id="selectPourcentId" class="form-control">
                                        <option value=""></option>
                                        <option value="25">25 %</option>
                                        <option value="50">50 %</option>
                                        <option value="100">100 %</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <span>OBSERVATION : </span><br/><br/>
                                    <textarea rows="5" id="inputObservation" class="form-control" placeholder="Veuillez fournir les observations" >
                                    </textarea>
                                </div><hr/>

                                <div class="form-group">
                                    <span>BANQUE : <span style="color: red">*</span></span>
                                    <select class="form-control" id="cmbBanqueTaxationOffice" >
                                    </select>
                                </div>

                                <div class="form-group">
                                    <span>COMPTE BANCAIRE :<span style="color: red">*</span> </span>
                                    <select class="form-control" id="cmbCompteBancaireTaxationOffice" >
                                    </select>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-warning" id="btnTaxationOffice"> <i  class="fa fa-print"></i> Imprimer</button>
                            <a title="Fermer" class="btn btn-secondary" data-dismiss="modal">Fermer</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheAvanceePaiement_1.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeByArticle.html" %>
        <%@include file="assets/include/modal/accuserReception.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/modalInfoMed.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAvancee_1.js"></script>
        <script type="text/javascript" src="assets/js/resteArecouvrer/relance.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/rechercheAvanceeByArticle.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>
        <script type="text/javascript" src="assets/js/accuserReception.js"></script>

    </body>

</html>