<%-- 
    Document   : document_np2
    Created on : 9 mars 2020, 09:39:37
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <title>Visualisation des documents</title>

        <style>

            @page {
                size: A4;
                margin: 0;
            }

            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                background-color: #FAFAFA;
                font: 12pt "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 290mm;
                min-height: 350mm;
                padding: 20mm;
                margin: 10mm auto;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .subpage {
                padding: 1cm;
                border: 4px #CCC solid;
                height: 310mm;
            }
            .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 16px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }
            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }

            .button2:hover {
                background-color: #008CBA;
                color: white;
            }

        </style>
    </head>

   <body>
        <br/><br/>
        <button id="btnPrint" style="margin-left:300px" class="button button2"><i class="fa fa-print"></i> &nbsp;&nbsp; Imprimer le document</button>
        <br/>
        <input type="button" id="create_pdf" style=" display: none" value="Generate PDF"/>  
        <hr/>
        <div class="page">
            <div class="subpage">
                <div id="document_container">

                </div>
            </div>    
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>    
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/printThis.js"></script>
        <script type="text/javascript" src="assets/lib/js/html2pdf.bundle.min.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/document.js"></script>
        <script type="text/javascript">

            $(function () {

               /* $('#create_pdf').click(function (e) {
                    e.preventDefault();

                   // var element = document.getElementById('document_container');

                    var opt = {
                        margin: 1,
                        filename: 'myfile.pdf',
                        image: {type: 'jpeg', quality: 0.98},
                        html2canvas: {scale: 2},
                        jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
                    };

                    var worker = html2pdf().from(element).set(opt).save(); */

                });

            });

        </script>
    </body> 
</html>
