<%-- 
    Document   : editerDivision
    Created on : 22 déc. 2020, 08:48:01
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Editer une division</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">
                        <div class="col-lg-12">

                            <div class="form-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> Edition d'une division</h4>
                                    </div>
                                </div> 

                                <div class="col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body" style="height: 70px;">

                                                <div class="form-group">
                                                    <p class="control-label col-sm-1" for="cmbEntiteAdminist">
                                                        Ville 
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="cmbEntiteAdminist"></select>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <input class="form-control" placeholder="Intitulé division" style="width: 100%" id="inputIntituleDivision"/>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <input class="form-control" placeholder="Sigle division" style="width: 100%" id="inputSigleDivision"/>
                                                    </div>

                                                    <button  
                                                        id="btnEnregisterDivision"
                                                        class="btn btn-success">
                                                        <i class="fa fa-save"></i>  
                                                        Enregistrer
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>
                <div >
                    <table id="tableEditionDivision" class="table table-bordered">
                    </table>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>


        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/gestionSite/editerDivision.js"></script>
    </body>
</html>
