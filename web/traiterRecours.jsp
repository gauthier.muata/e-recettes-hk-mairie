<%-- 
    Document   : traiterRecours
    Created on : 8 juin 2020, 09:16:45
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Traiter un recours juridictionnel</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-3">
                        <label style="font-weight: normal">Traiter un recours</label>
                    </div>

                    <div  class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <span id="lbl1" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Noms de l'assujetti &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNameResponsible" >&nbsp;</b>
                                </span>

                                <span id="lbl2" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblLegalForm" >&nbsp;</b>
                                </span>

                                <span id="lbl3" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Nif &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNifResponsible" >&nbsp;</b>
                                </span>
                                <br/>
                                <span id="lbl4" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Adresse principale &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblAddress" >&nbsp;</b>
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <span style="float: right;">
                                    <button id="btnCallModalSearchAssujettis" class="btn btn-primary">
                                        <i class="fa fa-search"></i> 
                                        Rechercher un assujetti
                                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                                <br/><br/>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="journal" >
                    <div class="responsive">
                        <table id="tableRecours" class="table table-bordered" > </table>
                    </div>

                </div>

                <div class="modal fade" id="modalTraitementJuridictinnel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 55%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Gestion des décisions juridictionnels</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="panel panel-primary" id="divInfoTraitement" >
                                    <div class="panel-heading">
                                        <h4 class="panel-title">Traiter recours</h4>
                                    </div>

                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label>Décision:</label>
                                                <select class="form-control" id="selectDecision" placeholder="Décision ......">
                                                    <option value="*">*</option>
                                                </select>
                                            </div> 

                                            <%-- --%>  
                                            <form id="formRadioButton">
                                                <div style="display: none" id="divRadio" class="form-check">
                                                    <input class="form-check-input" type="radio" name="radioTraiter" id="radioValider" value="1" >
                                                    <label for="radioValider" style="margin-right: 40px">APPROUVER</label>
                                                    <input class="form-check-input" type="radio" name="radioTraiter" id="radioRejeter" value="0" >
                                                    <label for="radioRejeter">REJETER</label>
                                                    <hr/>
                                                </div>                                       
                                            </form>

                                            <div class="form-group">

                                                <div id="divMontantDu">
                                                    <hr/>
                                                    <label for="lblMontantDu" style="font-weight: normal" >
                                                        Montant dû (Titre d'avis de mise en recouvrement contesté) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblMontantDu">&nbsp;</b>
                                                    <br/>

                                                    <label for="lblPartieMontantConteste" style="font-weight: normal" >
                                                        Partie du montant contesté &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblPartieMontantConteste">&nbsp;</b>

                                                    <br/>

                                                    <label for="lblCinqPourcentPartieConteste" style="font-weight: normal" >
                                                        20% de la partie non contesté  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblCinqPourcentPartieConteste">&nbsp;</b>
                                                    <hr/>
                                                </div>

                                                <div class="">
                                                    <br/>
                                                    <label style="font-weight: normal">MONTANT DEGREVE : </label>
                                                    <input class="form-control" type="number" min="1" id="inputDegreveAmount"/>
                                                </div>
                                                <hr/>

                                                <div id="divRestPayer">
                                                    <label for="lblRestePayer" style="font-weight: normal" >
                                                        Reste à payer &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                                    </label>
                                                    <b id="lblRestePayer" style="color: red">&nbsp;</b>
                                                </div>
                                            </div> 

                                        </div>
                                        <div class="panel-footer" style="text-align: right">
                                            <button                    
                                                type="button" 
                                                class="btn btn-warning pull-left" style ="display: none"
                                                id="btnJoindreDecisionAdmin">
                                                <i class="fa fa-download"></i>  
                                                Joindre d&eacute;cision juridictionnelle
                                            </button>

                                            <a 
                                                id="lblNbArchuiveDecisionAdminstrative"
                                                class="pull-left"
                                                style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                                            </a>

                                            <button type="button" class="btn btn-success" id="btnValiderTraitement">
                                                <i class="fa fa-check-circle"></i>
                                                Valider
                                            </button>
                                            <button type="button" class="btn btn-secondary pull-right" data-dismiss="modal" >Fermer</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>           
        </div>  




        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>

        <script type="text/javascript" src="assets/js/uploading.js"></script>
        <script type="text/javascript" src="assets/js/traiterRecours.js"></script>

    </body>
</html>
