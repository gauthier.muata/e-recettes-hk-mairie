<%-- 
    Document   : registreDecision
    Created on : 4 juil. 2019, 16:11:43
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des décisions juridictionnelles</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearchRecours">
                                <option value="1">Assujetti</option>
                                <option value="2">Référence du courrier</option>
                                <option value="3">Numéro document</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" disabled="true" class="form-control" style="width: 300px" id="inputResearchRecours" placeholder="Assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" id="btnSearchRecours">
                                        <i class="fa fa-search"></i> 
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnCallModalSearchAvandedRecours" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée
                    </button>
                </div>

                <hr/>
                
                <div class="row" id="isAdvanceJuridictionnelle" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour:</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">
                                        <span id="lbl1" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Type recours:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblTypeReclamation" >&nbsp;</b>
                                        </span> 

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au:&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="journal" >
                    <div class="responsive">
                        <table id="tableRegistreDecisionJudictionnelle" class="table table-bordered" > </table>
                    </div>

                </div>


            </div>
                
                <div class="modal fade" id="modalRecoursAdvancedSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Recherche avanc&eacute;e par type et date recours</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label>Type recours</label>
                                    <div class="input-group"  style="width: 100%;">
                                        <select  class="form-control" id="selectTypeRecours">
                                            <option value="*">*</option>
                                            <option value="1">Récouvrement</option>
                                            <option value="2">Taxation</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Du :</label>
                                    <div class="input-group date " id="datePicker1" style="width: 100%;">
                                        <input type="text" class="form-control" id="inputDateDebut" >
                                        <div class="input-group-addon">
                                            <span class="fa fa-th"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Au :</label>
                                    <div class="input-group date" id="datePicker2" style="width: 100%;">
                                        <input type="text" class="form-control" id="inputdateLast">
                                        <div class="input-group-addon">
                                            <span class="fa fa-th"></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btnAdvancedSearch">
                                <i class="fa fa-search"></i> &nbsp;Rechercher
                            </button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/accuserReception.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/observationModal.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>

        <script src="assets/js/uploading.js" type="text/javascript"></script>
        <script src="assets/js/gestionDecisionsJuridictionnelles.js" type="text/javascript"></script>
    </body>
</html>
