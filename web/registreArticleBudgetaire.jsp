<%-- 
    Document   : registreArticleBudgetaire
    Created on : 31 janv. 2020, 15:22:58
    Author     : juslin.tshiamua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestion des articles budg&eacute;taires</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">

                                <div class="col-lg-6">
                                    <label  for="cmbMinistereRegistre" >Minist&egrave;re</label>
                                    <select class="form-control" id="cmbMinistereRegistre"></select>
                                </div>

                                <div class="col-lg-6">
                                    <label  for="cmbServiceRegistre" >Service/Division</label>
                                    <select class="form-control" id="cmbServiceRegistre"></select>
                                </div>
                            </div>
                            <hr/>
                        <form class="form-inline" role="form">
                            
                            <div class="input-group" >
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    style="width: 700px" 
                                    id="inputResearchTaxation">
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary" 
                                        type="submit"id="btnResearchTaxation">
                                        <i class="fa fa-search"></i>
                                        Rechercher
                                    </button>

                                </div>

                            </div>
                            <button class="btn btn-success" 
                                    id="btnModifierArticle"
                                     style="margin-right: 10px">
                                <i class="fa fa-edit"></i> 
                                Modifier article
                            </button>
                            
                            <button 
                                id="btnCallModalSearchAvanded"
                                style="margin-right: 20px" 
                                class="btn btn-warning pull-right">
                                <i class="fa fa-filter"></i> &nbsp;
                                Effectuer une recherche avancée
                            </button>
                        </form>
                    </div>

                </div>
                <hr/>
                <div class="journal" >
                    <table id="tableTaxation" class="table table-bordered">
                    </table>
                </div>
            </div>

        </div>
        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>
        
        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/rechercheAvancee.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/registreArticleBudgetaire.js"></script>
    </body>
</html>