<%-- 
    Document   : statistic
    Created on : 22 juil. 2020, 09:56:36
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Statistique</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <style>

            a { transition: all .3s ease;-webkit-transition: all .3s ease;-moz-transition: all .3s ease;-o-transition: all .3s ease; }
            /* Visitor */
            a.visitor i,.visitor H3.list-group-item-heading { color:#E48A07; }
            a.visitor:hover { background-color:#E48A07; }
            a.visitor:hover * { color:#FFF; }
            /* Facebook */
            a.facebook-like i,.facebook-like H3.list-group-item-heading { color:#3b5998; }
            a.facebook-like:hover { background-color:#3b5998; }
            a.facebook-like:hover * { color:#FFF; }
            /* Google */
            a.google-plus i,.google-plus H3.list-group-item-heading { color:#dd4b39; }
            a.google-plus:hover { background-color:#dd4b39; }
            a.google-plus:hover * { color:#FFF; }
            /* Twitter */
            a.twitter i,.twitter H3.list-group-item-heading { color:#00acee; }
            a.twitter:hover { background-color:#00acee; }
            a.twitter:hover * { color:#FFF; }
            /* Linkedin */
            a.linkedin i,.linkedin H3.list-group-item-heading { color:#0e76a8; }
            a.linkedin:hover { background-color:#0e76a8; }
            a.linkedin:hover * { color:#FFF; }
            /* Tumblr */
            a.tumblr i,.tumblr H3.list-group-item-heading { color:#34526f; }
            a.tumblr:hover { background-color:#34526f; }
            a.tumblr:hover * { color:#FFF; }
            /* Youtube */
            a.youtube i,.youtube H3.list-group-item-heading { color:#c4302b; }
            a.youtube:hover { background-color:#c4302b; }
            a.youtube:hover * { color:#FFF; }
            /* Vimeo */
            a.vimeo i,.vimeo H3.list-group-item-heading { color:#44bbff; }
            a.vimeo:hover { background-color:#44bbff; }
            a.vimeo:hover * { color:#FFF; }

        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>

                <div class="panel-body">

                    <div class="row">
                        <div  class="col-lg-12">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-weight: normal;font-size: 14px">
                                            Exercice fiscale
                                        </label> 
                                        <select class="form-control" id="exerciceFiscal"></select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-weight: normal;font-size: 14px">
                                            S&eacute;lectionner un secteur d'activit&eacute;
                                        </label> 
                                        <select class="form-control" id="cmbService"></select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <br/>
                                    <button id="btnSearchArt" class="btn btn-primary">
                                        <i class="fa fa-search"></i>Recherche</button>
                                </div>
                            </div>                      

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">

                                <div class="panel-heading" style="background-color: #DBDBDB;color: black;">
                                    <h4 class="panel-title"> Registre des assignations
                                    </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body">

                                        <table id="tableArticleBudgetaire" class="table table-bordered" ></table>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="modal fade" id="modalModifyAssignation" role="dialog" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" id="locationBienTitle">ASSIGNATION BUDGETAIRE</h5>
                            </div>
                            <div class="modal-body" id="divLocationBien">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> Modification du montant</h4>
                                            </div>
                                            <div class="panel-wrapper collapse in">
                                                <div class="panel-body"> 
                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <label style="font-weight: normal" id="labelModifyAssign">
                                                                Montant assigné <span style="color:red"> *</span>
                                                            </label>
                                                            <input type="text" class="form-control" id="inputMontantAssign"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-footer" style="text-align: right">
                                                    <button  
                                                        id="btnModifyAssignation"
                                                        class="btn btn-success">
                                                        <i class="fa fa-save"></i> Enregistrer
                                                    </button>
                                                    <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script src="assets/js/registreAssignation.js" type="text/javascript"></script>
    </body>
</html>
