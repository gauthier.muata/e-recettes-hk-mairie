<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des commandements</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>

        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html"%>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="selectTypeSearch">
                                <!--option value="0">Tous</option-->
                                <option value="1">Numéro</option>
                                <option value="2">Assujetti</option>

                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" 
                                       style="width: 380px" 
                                       id="valueTypeSearch" placeholder="Veuillez saisir le numéro de la contrainte">

                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnSimpleSearch"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Type : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="labelType" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <!--span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Etat : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblStateMed" >&nbsp;&nbsp;&nbsp;</b>
                                        </span-->

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black" >
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" >
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <table id="tableCommandement" class="table table-bordered">

                    </table>
                </div>


            </div>

            <div class="modal fade" id="documentListModal" 
                 role="dialog"
                 data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog modal-dialog-centered" style="width: 765px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title" 
                                id="exampleModalLongTitle">Impression du document </h5>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <label class="control-label col-sm-12" >Veuillez sélectionner le document à imprimer : </label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="selectDocument" placeholder="Document &agrave; imprimer">
                                        <option value="0">--</option>
                                        <option value="1">COMMANDEMENT</option>
                                        <option value="2">BON A PAYER</option>
                                    </select>

                                </div>
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-success" id="btnPrintDocument">
                                <i class="fa fa-print"></i>
                                Imprimer ce document
                            </button>
                            <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheAvanceePaiement_1.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeByArticle.html" %>
        <%@include file="assets/include/modal/accuserReception.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeAMR.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/poursuites/commandement.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <script type="text/javascript" src="assets/js/rechercheAvancee.js"></script>
        <script src="assets/js/uploading.js" type="text/javascript"></script>
        <script src="assets/js/accuserReception.js" type="text/javascript"></script>
    </body>
</html>
