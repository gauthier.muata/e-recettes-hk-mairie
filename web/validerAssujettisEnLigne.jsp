<%-- 
    Document   : validerAssujettisEnLigne
    Created on : 17 avr. 2020, 08:47:05
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title>Repertoire des assujettis</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        
        <style>
            @media (max-width: 768px) {
                .journal{overflow: auto}
                .spanSearchLabel{display: none}
                .inputGroupAssuj{margin-top: 3px}
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="cmbSearchTypeAssujettisValider">
                                <option value ="0">Assujetti</option>
                                <option value ="1">Nif</option>
                            </select>
                            <div class="input-group inputGroupAssuj">
                                <input type="text" class="form-control" style="width: 100%" id="inputSearchAssujettisValider" 
                                       placeholder="Nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchAssujettisValider">
                                        <i class="fa fa-search"></i> 
                                        <span class="spanSearchLabel">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <hr/>

                <div class="journal">
                    <table id="tableAssujettiValider" class="table table-bordered">

                    </table>
                </div>

            </div>
        </div>


        <%@include file="assets/include/modal/header.html" %>
        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script  type="text/javascript" src="assets/js/repertoire/validerAssujettis.js"></script>
    </body>
</html>
