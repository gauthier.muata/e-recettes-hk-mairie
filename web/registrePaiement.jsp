
<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Registre des paiements</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            #researchValue{width:320px}
            @media (max-width: 768px) {
                .spanSearch{display: none}
                #researchValue{width:100%}
                .journal{overflow: auto}
                .btnPerso{background-color: transparent;color: blue;cursor: pointer;border-color: transparent;text-decoration: underline}
                .btnPerso:focus{background-color: transparent;border-color: transparent}
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html"%>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            <div class="input-group" >
                                <select  class="form-control" id="typeDocumentResearch">
                                    <option value="0">-- Document --</option>
                                    <!--option value="AMR">AMR</option-->
                                    <!--option value="BP">Bon &agrave; payer</option-->
                                    <!--<option value="DEC">NOTE DE TAXATION</option>-->
                                    <option value="NP">NOTE DE PERCEPTION</option>
                                </select>
                            </div>

                            Filtrer par 
                            <select  class="form-control" id="researchType">
                                <option value="1" >Assujetti</option>
                                <option value="2">R&eacute;f&eacute;rence document</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" id="researchValue" placeholder="Veuillez saisir le nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnSimpleSearch">
                                        <i class="fa fa-search"></i> <span class="spanSearch">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModal" style="margin-right: 20px" class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée
                    </button>
                </div>

                <hr/>

                <div class="row" id="isAdvance">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">
                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Document : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblTypeDocument" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black;display: none" id="idSpan1">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Etat : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" id="idSpan2">
                                            <b style="font-style: italic;display: none" id="lblStatePaiement" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black;display: none" id="idSpan3">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Compte bancaire : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black" id="idSpan4">
                                            <b style="font-style: italic;display: none" id="lblaccountBank" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Agent :
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblAgent" >&nbsp;</b>
                                        </span> 

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <table id="tableJournal" class="table table-bordered"></table>
                </div>

                <div class="modal fade" id="traitementPaiementJournalModal" 
                     role="dialog"
                     data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h5 class="modal-title" 
                                    id="exampleModalLongTitle">Traietement d'un paiement</h5>
                            </div>
                            <div class="modal-body" id="divTableauDB" style="visibility: hidden">
                                <table id="tableDB" class="table table-bordered">

                                </table>
                            </div>

                            <div class="modal-body" id="divInfoTraitementPaiement2" style="visibility: hidden">
                                <div class="row">
                                    <outputlabel class="control-label col-sm-12" >Date du traitement : <span id="idValueDateTraitement"style="font-weight: bold">11/03/2020</span></outputlabel>
                                    <outputlabel class="control-label col-sm-12" >Traiter par &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <span id="idValueAgentTraitement"style="font-weight: bold">Gauthier MUATA</span></outputlabel>
                                </div><hr/>
                            </div>

                            <div class="modal-body" id="divInfoTraitementPaiement">
                                <div class="row">
                                    <label class="control-label col-sm-12" >Veuillez sélectionner l'avis du traitement : </label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="cmbAvisTraitementPaiement">

                                            <option value="0">--</option>
                                            <option value="2">CONFIRMER PAIEMENT</option>
                                            <option value="4">REJETER PAIEMENT</option>
                                        </select><br/>
                                    </div>
                                    <label class="control-label col-sm-12" >Observation : </label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control" 
                                                  rows="5"
                                                  id="txtObservationTraitementPaiement">

                                        </textarea>
                                    </div>

                                </div> 
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success" id="btnTraiterPaiement">
                                    <i class="fa fa-check-circle"></i>
                                    Valider
                                </button>
                                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <%@include file="assets/include/modal/rechercheAvanceePaiement_1.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script src="assets/js/rechercheAvancee_1.js" type="text/javascript"></script>
        <!--script src="assets/js/rechercheAvancee.js" type="text/javascript"></script-->
        <!--script src="assets/js/paiement.js" type="text/javascript"></script-->
        <script src="assets/js/paiement/registrePaiement.js" type="text/javascript"></script>

    </body>
</html>
