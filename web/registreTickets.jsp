<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Registre de production des tickets</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .spanSearch{
                    display: none;                
                }
                #btnCallModalSearchAvanded{background-color: transparent;color: blue;cursor: pointer;border-color: transparent;text-decoration: underline;margin-top: 5px}
                #btnCallModalSearchAvanded:focus{background-color: transparent;border-color: transparent}
                #inputResearchTaxation{margin-top: 3px}
                .journal{overflow: auto}
            }
        </style>

    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-9">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearch">
                                <option value="1">Numéro ticket</option>
                            </select>
                            <div class="input-group" >
                                <input 
                                    type="text" 
                                    class="form-control" 
                                    style="width: 100%" 
                                    placeholder="Saisir le numéro du ticket "
                                    id="inputResearchTaxation">
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary" 
                                        type="submit"id="btnResearchTaxation">
                                        <i class="fa fa-search"></i>
                                        <span class="spanSearch">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-3">
                        <button 
                            id="btnCallModalSearchAvanded"
                            style="margin-right: 20px" 
                            class="btn btn-warning pull-right">
                            <i class="fa fa-filter"></i> &nbsp;
                            <span >Effectuer une recherche avancée</span>                       
                        </button>
                    </div>

                </div>
                <hr/>
                <div class="journal" >
                    <button class="btn btn-sm btn-primary" id="btnGenereDoc" style="display: none"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le Rapport détailler de production</button>
                    <button class="btn btn-sm btn-primary" id="btnGenereDoc2" style="display: none"><i class="fa fa-print"></i>&nbsp;&nbsp;Imprimer le Rapport condenser de production</button>
                    <table id="tableTicketPeage" class="table table-bordered"></table>
                </div>
            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceePeage.html" %>
        <%@include file="assets/include/modal/modalPrintRapportProductionPeage.html" %>
        <%@include file="assets/include/modal/modalUploadTicket.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/export/jspdf.min.js"></script>
        <script type="text/javascript" src="assets/js/export/jspdf.plugin.autotable.js"></script>

        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/previsionCredit/rechercheAvanceeTicketPeage.js"></script>
        <script type="text/javascript" src="assets/js/previsionCredit/registreTickets.js"></script>
        <script type="text/javascript" src="assets/js/uploadingTicket.js" type="text/javascript"></script>
    </body>
</html>