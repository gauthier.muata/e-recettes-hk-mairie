/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    mainNavigationLabel.text('APUREMENT');
    secondNavigationLabel.text('Registre des apurements rejetés');

    removeActiveMenu();
    linkMenuApurement.addClass('active');

    btnSimpleSearch.click(function (e) {
        e.preventDefault();
        if (inputResearchValue.val() === empty || inputResearchValue.val().length < SEARCH_MIN_TEXT) {
             showEmptySearchMessage();
        } else {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            jourrnalSearching(4, 1);
        }
    });
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        jourrnalSearching(4, 2);
    });

    loadJournalTable('', 4);

});