/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalLocationBien;

var
        codePermuteBien,
        codeProprietaireBien;

var
        lblNameProprietairePerm,
        lblAddressProprietairePerm;

var cmbPermutationBien;

var
        inputReferencePerm,
        inputActeNotariePerm,
        inputDateActeNotarie,
        inputDateAcquisitionPerm;

var btnSavePermuteBien;

$(function () {

    modalLocationBien = $('#modalLocationBien');
    lblNameProprietairePerm = $('#lblNameProprietairePerm');
    lblAddressProprietairePerm = $('#lblAddressProprietairePerm');

    cmbPermutationBien = $('#cmbPermutationBien');
    inputReferencePerm = $('#inputReferencePerm');
    inputActeNotarie = $('#inputActeNotarie');
    inputActeNotariePerm = $('#inputActeNotariePerm');
    inputDateAcquisitionPerm = $('#inputDateAcquisitionPerm');

    btnSavePermuteBien = $('#btnSavePermuteBien');

    btnSavePermuteBien.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir permuter ce bien ?', function () {
            savePermuteBien();
        });

    });


});

function savePermuteBien() {

    if (cmbPermutationBien.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le bien à louer.');
        return;
    }

    if (inputDateAcquisitionPerm.val() === '') {
        alertify.alert('Veuillez d\'abord choisir la date de permutation du bien.');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveBienPermute',
            'codePersonne': codePermuteBien,
            'idBien': cmbPermutationBien.val(),
            'dateAcquisition': inputDateAcquisitionPerm.val(),
            'referenceContrat': inputReferencePerm.val(),
            'numActeNotarie': inputActeNotariePerm.val(),
            'dateActeNotarie': inputDateActeNotarie.val(),
            'codeProprietaireBien': codeProprietaireBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'assujetti ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '1') {

                alertify.alert('Le bien a été loué avec succès.');

                setTimeout(function () {
                    window.location = 'gestion-bien?id=' + btoa(codePermuteBien);
                }
                , 1500);

            } else if (response == '0') {
                alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du bien.');
            } else {
                showResponseError();
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function getSelectedProprietaireData() {
    lblNameProprietairePerm.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
    lblAddressProprietairePerm.html(selectAssujettiData.adresse);
}
