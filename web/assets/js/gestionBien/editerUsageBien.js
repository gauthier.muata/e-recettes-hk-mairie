/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnEnregistrerUsageBien;

var inputValueUsageBien;

var tableResultUsageBien;

var usageBienList;

var codeUsageBienSelected;

var btnNettoyerUsageBien;

$(function () {

    codeUsageBienSelected = '0';

    tableResultUsageBien = $('#tableResultUsageBien');

    btnEnregistrerUsageBien = $('#btnEnregistrerUsageBien');

    inputValueUsageBien = $('#inputValueUsageBien');

    btnNettoyerUsageBien = $('#btnNettoyerUsageBien');

    btnEnregistrerUsageBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputValueUsageBien.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir l\'intutilé de l\'usage bien');
            return;
        }

        alertify.confirm('Etes-vous de vouloir enregistrer cet usage bien ?', function () {

            saveUsageBien();

        });
    });

    btnNettoyerUsageBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous de vouloir nettoyer le champ ?', function () {
            inputValueUsageBien.val(empty);
            codeUsageBienSelected = '0';
        });
    });

    loadUsageBiens();

});

function loadUsageBiens() {


    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadUsageBien'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                usageBienList = JSON.parse(JSON.stringify(response));
                printUsageBien(usageBienList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printUsageBien(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">LIBELLE</th>';
    tableContent += '<th style="display:none" scope="col">Code type bien</th>';
    tableContent += '<th scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        var btnDeleteUsageBien = '<button type="button" onclick="removeUsageBien(\'' + data[i].id + '\',\'' + data[i].intitule + '\')" class="btn btn-danger"><i class="fa fa-trash"></i></button>';
        var btnEditUsageBien = '<button type="button" onclick="editUsageBien(\'' + data[i].id + '\',\'' + data[i].intitule + '\')" class="btn btn-success"><i class="fa fa-edit"></i></button>';
        var btnAction = btnDeleteUsageBien + ' ' + btnEditUsageBien;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left">' + data[i].intitule + '</td>';
        tableContent += '<td style="display:none">' + data[i].id + '</td>';
        tableContent += '<td style="width:15%">' + btnAction + '</td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultUsageBien.html(tableContent);

    var myDt = tableResultUsageBien.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé droit _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });


}



function removeUsageBien(id, intitule) {
    alertify.confirm('Voulez-vous désactiver ' + intitule + ' ?', function () {
        validateDisableUsageBien(id);
    });
}

function saveUsageBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveUsageBien',
            'code': codeUsageBienSelected,
            'libelleUsageBien': inputValueUsageBien.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'usage bien en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\enregistrement de l\'usage bien s\'est effectué avec succès.');
                    loadUsageBiens();

                    btnEnregistrerUsageBien.attr('style', 'display:block');

                    codeUsageBienSelected = '0';
                    inputValueUsageBien.val(empty);

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement de l\'usage bien.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function updateUsageBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateUsageBien',
            'libelleUsageBien': inputValueUsageBien.val(),
            'codeUsageBien': codeUsageBienSelected.codeTypeBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour de l\'usage bien en cours...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La modification de l\'usage bien s\'est effectuée avec succès.');
  
                    loadUsageBiens();
                 
                    inputValueUsageBien.val('');
                    btnEnregistrerUsageBien.attr('style', 'display:block');
  

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la modification de l\'usage bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}
function validateDisableUsageBien(id) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disableUsageBien',
            'codeUsageBien': id
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation de l\'usage bien ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La désactivation de l\'usage bien s\'est effectuée avec succès.');
            
                    loadUsageBiens();
                    inputValueUsageBien.val('');
                    btnEnregistrerUsageBien.attr('style', 'display:block');
   
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désactivation de l\'usage bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function displayUsageBienInfo() {

    if (cmbUsageBien.val() !== '0') {
        inputValueUsageBien.val($('#cmbUsageBien option:selected').text());
        codeUsageBienSelected = cmbUsageBien.val();
    } else {
        inputValueUsageBien.val('0');
        codeUsageBienSelected = '0';
    }

}

function editUsageBien(id, libelle) {

    alertify.confirm('Etes-vous de vouloir sélectionner cet usage de bien : ' + libelle + ' ?', function () {

        inputValueUsageBien.val(libelle);
        codeUsageBienSelected = id;

    });
}