/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var number = 0;
var id = 1;
var content2;
var wait = false;

$(function () {

    mainNavigationLabel.text('ERECETTES 1.0');
    secondNavigationLabel.text('Tableau de bord');
    removeActiveMenu();
    linkMenuDahsboard.addClass('active');

    content2 = $('#content2');

    infiniteScroll();

    loadAssujettisFrom();

});

function infiniteScroll() {

    if (number == 0) {
        $('#content2').html('<div id="loader"><img src="assets/images/loading.gif" alt="loader ajax"></div>');
    } else {
        $('#content2').append('<div id="loader"><img src="assets/images/loading.gif" alt="loader ajax"></div>');
    }

    // vérifie si c'est un iPhone, iPod ou iPad
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);

    // on déclence une fonction lorsque l'utilisateur utilise sa molette
    $(window).scroll(function () {
        // cette condition vaut true lorsque le visiteur atteint le bas de page
        // si c'est un iDevice, l'évènement est déclenché 150px avant le bas de page
        var window_size = $(window).scrollTop() + $(window).height();
        var document_size = $(document).height();

        //console.log(Math.trunc(window_size) + ' = ' + document_size);

        if (((Math.trunc(window_size) + 1) >= document_size) || (agentID && ((window_size + 150) > document_size))) {
            // on effectue nos traitements
            console.log('wait = ' + wait);
            if (!wait) {
                loadAssujettisFrom();
            }

        }
    });
}

function loadAssujettisFrom() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'assujtettisFrom',
            'from': number
        },
        beforeSend: function () {
            // on affiche donc loader
            wait = true;
            $('#content2 #loader').fadeIn(400);
        },
        success: function (response)
        {

            if (response == '-1') {
                wait = false;
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                if (result.length > 0) {

                    var contentHtml = '';

                    for (var i = 0; i < result.length; i++) {

                        contentHtml += '<article id="' + id + '">';
                        contentHtml += '<h1>' + id + ') ' + result[i].libelleFormeJuridique + '</h1>';
                        contentHtml += '<p>' + result[i].nom + ' ' + result[i].postNom + ' ' + result[i].prenom + '</p>';
                        contentHtml += result[i].chaine;
                        contentHtml += '</article>';

                        id++;
                    }

                    // on les insère juste avant le loader.gif
                    $('#content2 #loader').before(contentHtml);

                    // on les affiche avec un fadeIn
                    $('#content2 .hidden').fadeIn(400);


//                    if (number == 0) {
//                        content2.html(contentHtml);
//                    } else {
//                        content2.append(contentHtml);
//                    }
//
                    number += 20;

                    wait = false;
                }

                // le chargement est terminé, on fait disparaitre notre loader
                $('#content2 #loader').fadeOut(400);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}