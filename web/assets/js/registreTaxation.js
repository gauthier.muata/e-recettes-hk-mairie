/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var tableTaxation, inputResearchTaxation, typeResearch;
var btnResearchTaxation, btnCallModalSearchAvanded, btnResearchTaxationClosed, btnResearchTaxationRejected;
var dataTaxations, tableDetailTaxation;

var lblNoteCalcul, lblLegalForm, lblNameResponsible, lblAdress, lblExercice, lblAmount;
var btnClosedTaxation;
var inputObservation, cmbAvis;
var modalDetailNoteCalcul;
var codeAvis;
var dataAvis;

var numeroNcSelected;

var modalRechercheAvanceeNC;

var registerType;
var ncParam;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;
var lblInfoValidation, lblInfoOrdonnateur, lblInfoPaiement, lblInfoTaxateur;
var btnVoirDeclaration;
var tableBienTaxation2;

$(function () {

    codeAvis = '';
    registerType = '';

    tableTaxation = $('#tableTaxation');
    tableDetailTaxation = $('#tableDetailTaxation');

    inputResearchTaxation = $('#inputResearchTaxation');
    inputResearchTaxation.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
    inputObservation = $('#inputObservation');
    inputObservation.val('');

    typeResearch = $('#typeResearch');
    btnResearchTaxation = $('#btnResearchTaxation');
    btnClosedTaxation = $('#btnClosedTaxation');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    btnResearchTaxationClosed = $('#btnResearchTaxationClosed');
    btnResearchTaxationRejected = $('#btnResearchTaxationRejected');

    modalDetailNoteCalcul = $('#modalDetailNoteCalcul');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');

    lblExercice = $('#lblExercice');
    lblNoteCalcul = $('#lblNoteCalcul');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAdress = $('#lblAdress');
    lblAmount = $('#lblAmount');

    cmbAvis = $('#cmbAvis');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    lblInfoValidation = $('#lblInfoValidation');
    lblInfoOrdonnateur = $('#lblInfoOrdonnateur');
    lblInfoPaiement = $('#lblInfoPaiement');
    lblInfoTaxateur = $('#lblInfoTaxateur');
    tableBienTaxation2 = $('#tableBienTaxation2');

    btnVoirDeclaration = $('#btnVoirDeclaration');

    cmbAvis.on('change', function (e) {
        codeAvis = cmbAvis.val();
    });

    btnResearchTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchTaxation.val() === empty) {
            alertify.alert("Veuillez renseigner le numéro de la note de perception");
            return;
        }

        taxationNpMairie(false);
    });

    btnResearchTaxationClosed.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputResearchTaxation.val() === empty) {
            alertify.alert("Veuillez renseigner le numéro de la note de perception");
            return;
        }

        taxationNpMairie(false);

    });

    inputResearchTaxation.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchTaxation.trigger('click');
        }
    });

    btnCallModalSearchAvanded.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });


    btnSimpleSearch = $('#btnSimpleSearch');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnShowModalAdvencedRechearch = $('#btnRechercheAvancee');

    btnShowModalAdvencedRechearch.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeNC.modal('hide');
        taxationNpMairie(true);
    });

    var urlNcParam = getUrlParameter('nc');

    if (jQuery.type(urlNcParam) !== 'undefined') {

        ncParam = atob(urlNcParam);
        inputResearchTaxation.val(ncParam);
        btnResearchTaxation.trigger('click');
    }

    btnAdvencedSearch.trigger('click');

    loadTaxationTable('');
//    loadDetailTaxationTable('');
});

function loadTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">DATE</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col">NUMERO</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left;width:11%"scope="col">UNITE ECO.</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">ARTICLE BUBD.</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">ECHEANCE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT VOLET 1</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">MONTANT VOLET 2</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">TOTAL MONTANT</th>';
    tableContent += '<th style="text-align:center;width:6%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumVolet1CDF = 0;
    var sumVolet2CDF = 0;

    var sumVolet1USD = 0;
    var sumVolet2USD = 0;

    var sumGLobalVoletCDF = 0;
    var sumGLobalVoletUSD = 0;

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';

        if (result[i].articleBudgetaire.length > 250) {
            firstLineAB = result[i].articleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].articleBudgetaire.toUpperCase();
        }

        if (result[i].devise === 'CDF') {

            sumVolet1CDF += result[i].amount1;
            sumVolet2CDF += result[i].amount2;
            sumGLobalVoletCDF += (result[i].amount1 + result[i].amount2);

        } else {

            sumVolet1USD += result[i].amount1;
            sumVolet2USD += result[i].amount2;
            sumGLobalVoletUSD += result[i].amount1 + result[i].amount2;
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:6%;text-align:center">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%;text-align:center;font-weight:bold">' + result[i].numero + '</td>';
        tableContent += '<td style="vertical-align:middle;width:11%">' + result[i].nom + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].bien + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle;title="' + result[i].articleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateEcheance + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:10%">' + formatNumber(result[i].amount1, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:10%">' + formatNumber(result[i].amount2, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:10%">' + formatNumber(result[i].amount1 + result[i].amount2, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;vertical-align:middle"><button class="btn btn-primary" onclick="printDocument(\'' + result[i].numero + '\')"><i class="fa fa-print"></i></button></td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    tableContent += '</tfoot>';

    tableTaxation.html(tableContent);

    var myDataTable = tableTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 5}
        ],
        order: [[5, 'asc']],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumVolet1CDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumVolet1USD, 'USD'));

            $(api.column(7).footer()).html(
                    formatNumber(sumVolet2CDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumVolet2USD, 'USD'));

            $(api.column(8).footer()).html(
                    formatNumber(sumGLobalVoletCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumGLobalVoletUSD, 'USD'));
        }
    });

}

function taxationNpMairie(isAdvanced) {

    if (isAdvanced) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());
        isAdvance.attr('style', 'display: block');

    } else {

        isAdvance.attr('style', 'display: none');
    }

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputResearchTaxation.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'operation': 'researchTaxation'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Veuillez patientiez ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            setTimeout(function () {

                if (response == '-1') {

                    showResponseError();

                } else {

                    dataTaxations = JSON.parse(JSON.stringify(response));
                    loadTaxationTable(dataTaxations);

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDocument(numeroDoc) {

    for (var i = 0; i < dataTaxations.length; i++) {

        if (dataTaxations[i].numero == numeroDoc) {
            alertify.confirm('Voulez-vous imprimer le document numéro :  ' + dataTaxations[i].numero + ' ?', function () {
                printDoc(numeroDoc);
            });
            break;
        }
    }
}

function printDoc(referenceDoc) {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': referenceDoc,
            'typeDocument': "NP_M",
            'idUser': userData.idUser,
            'operation': 'printDocument'
        },
        beforeSend: function () {
           $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
             $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0' || response == '-1') {
                showResponseError();
                return;
            }
            setTimeout(function () {
                
                var result = JSON.parse(response);
                
                var documentContentVol1 = result.dataReturnVol1;
                var documentContentVol2  = result.dataReturnVol2;
                
                setDocumentContent(documentContentVol1);
                window.open('visualisation-document', '_blank');
                
                setDocumentContent(documentContentVol2);
                window.open('visualisation-document', '_blank');
                
                setTimeout(function () {
                }, 2000);
               taxationNpMairie(false);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
             $.unblockUI();
            showResponseError();
        }

    });

}