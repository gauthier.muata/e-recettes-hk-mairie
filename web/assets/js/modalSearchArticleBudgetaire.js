/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnSelectedBudgetArticle;
var btnResearchBudgetArticle;

var tbodyTaxation, tableResponsibles,
        tableBudgetArticles,
        inputValueResearchBudgetArticle,
        selectMinistere;
var mySelectService;

var budgetArticleModal;
var infoBudgetArticle = {};

$(function () {

    budgetArticleModal = $('#budgetArticleModal');
    btnSelectedBudgetArticle = $('#btnSelectedBudgetArticle');
    btnResearchBudgetArticle = $('#btnResearchBudgetArticle');
    tableBudgetArticles = $('#tableBudgetArticles');
    inputValueResearchBudgetArticle = $('#inputValueResearchBudgetArticle');

    mySelectService = $('#mySelectService');
    selectMinistere = $('#selectMinistere');

    getAllService();

    printResultBudgetArticle(empty);

    btnResearchBudgetArticle.on('click', function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadBudgetArticles();
    });

    btnSelectedBudgetArticle.on('click', function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (infoBudgetArticle.code === '') {
            alertify.alert('Veuillez d\'abord sélectionner un article budgétaire');
            return;
        }
        getCurrentBudgetArticle();
    });
});

function loadBudgetArticles() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchBudgetArticle.val(),
            'codeService': mySelectService.val(),
            'codePersonne': '',
            'codeEntite' : JSON.stringify(JSON.parse(userData.allEntiteList)),
            'isAssujettissable': '*',
            'operation': 'researchBudgetArticle'
        },
        beforeSend: function () {
            budgetArticleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                budgetArticleModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectedBudgetArticle.hide();
                    printResultBudgetArticle(empty);
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                    return;
                } else {
                    var dataBudgetArticle = JSON.parse(JSON.stringify(response));

                    if (dataBudgetArticle.length > 0) {
                        btnSelectedBudgetArticle.show();
                        printResultBudgetArticle(dataBudgetArticle);
                    } else {
                        btnSelectedBudgetArticle.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            budgetArticleModal.unblock();
            showResponseError();
        }
    });
}

function printResultBudgetArticle(dataBudgetArticle) {

    var tableContent = '';

    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th>ARTICLE BUDGETAIRE</th>';
    tableContent += '<th scope="col" >FAIT GENERATEUR</th>';
    tableContent += '<th>SERVICE D\'ASSIETTE</th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col">Code budgétaire</th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var firstLineAB = '';

    for (var i = 0; i < dataBudgetArticle.length; i++) {

        if (dataBudgetArticle[i].libelleArticleBudgetaire.length > 150) {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire.substring(0, 150) + ' ...';
        } else {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire;
        }
        
        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:40%" title="' + dataBudgetArticle[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;">' + dataBudgetArticle[i].codeDocumentOfficiel + '</span> / ' + firstLineAB + '</td>';
        tableContent += '<td style="vertical-align:middle;width:40%">' + dataBudgetArticle[i].libelleActeGenerateur + '</td>';
        tableContent += '<td>' + dataBudgetArticle[i].libelleMinistere + ' / ' + dataBudgetArticle[i].libelleService + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeArticleBudgetaire + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].palier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].libelleArticleBudgetaire + '(' + dataBudgetArticle[i].codeDocumentOfficiel + ')' + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].addNArticleToPanier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnel + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMinimum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMaximum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeService + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].hasExo + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].exoTaux + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].exoTarif + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeExo + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeDocumentOfficiel + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableBudgetArticles.html(tableContent);

    var myDataTable = tableBudgetArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6;font-weight:bold" class="group"><td colspan="3">' + group + '</td></tr>'
                            );
                    last = group;
                }
            });
        }
    });

    $('#tableBudgetArticles tbody').on('click', 'tr', function () {
        infoBudgetArticle = myDataTable.row(this).data();
    });
}

var tempService = [];

function getAllService() {

    var data;
    tempService = [];
    var AllServiceList = JSON.parse(userData.allServiceList);

    for (var i = 0; i < AllServiceList.length; i++) {

        if (checkServiceABExist(AllServiceList[i].serviceCode)) {
            continue;
        }

        if (AllServiceList[i].serviceCode == userData.serviceCode) {

            data += '<option value="' + AllServiceList[i].serviceCode + '" selected="selected">' + AllServiceList[i].serviceName + '</option>';

        } else {
            data += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName + '</option>';
        }


        tempService.push(AllServiceList[i].serviceCode);
    }

    mySelectService.html(data);
}

function checkServiceABExist(service) {

    var exist = false;
    for (var i = 0; i < tempService.length; i++) {

        if (tempService[i] == service) {
            exist = true;
            break;
        }

    }

    return exist;
}

