/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var medList = [], mepList = [], bankList = [];
var tableDefaillants, tableDefaillantPaiement;
var assujettiModal, modalRechercheAvanceeByArticle;
var assujettiCodeValue, codeAssujetti, typeAssujetti;
var adresseId, medDocumentPrint, isAvancedSearch;
var responsibleObject = {}, medObj = {}, mepObj = {};
var btnSimpleSearch, btnShowAdvancedSerachModal, btnAdvencedSearchByArticle;


var cmbBanqueTaxationOffice, cmbCompteBancaireTaxationOffice;
var selectPourcentId;
var btnTaxationOffice;
var inputObservation;

var codeTypeDoc = 'RELANCE';
var codeDoc = undefined;
var codeMedCurrent, typeMed;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var selectAB, selectAnnee_1, selectMois_1;

var lblService, lblArticleBudgetaire, lblPeriodeDeclaration;

var isAdvance;
var btnAdvencedSearch;
var modalRechercheAvanceeModelTwo, btnShowAdvancedSerachModal;

var lblService, lblSite, lblDateDebut, lblDateFin;

var modalAccuserReception;

var medInfoModal;

var idIconBtnOperation,
        btnPrintMedOrMep,
        valDateEcheance,
        valDateReception,
        valDateCreate,
        valMedOrMep,
        lblMedOrMep,
        divInfoMed,
        valExercice,
        valReference,
        lblReference;

var echeanceMedExist;

var dateDebuts, dateFins, codeService, codeSite;

$(function () {

    medDocumentPrint = '';
    isAvancedSearch = '1';

    mainNavigationLabel.text('POURSUITES');

    secondNavigationLabel.text('Registre des relances des invitations de service');

    //removeActiveMenu();
    //linkMenuContentieux.addClass('active');

    tableDefaillants = $('#tableDefaillants');
    btnSimpleSearch = $('#btnSimpleSearch');
    assujettiModal = $('#assujettiModal');
    assujettiCodeValue = $('#assujettiCodeValue');
    modalRechercheAvanceeByArticle = $('#modalRechercheAvanceeByArticle');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    btnAdvencedSearchByArticle = $('#btnAdvencedSearchByArticle');

    selectAB = $('#selectAB');
    selectAnnee_1 = $('#selectAnnee_1');
    selectMois_1 = $('#selectMois_1');

    lblService = $('#lblService');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');
    lblPeriodeDeclaration = $('#lblPeriodeDeclaration');
    isAdvance = $('#isAdvance');
    tableDefaillantPaiement = $('#tableDefaillantPaiement');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    lblService = $('#lblService');
    lblSite = $('#lblSite');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalAccuserReception = $('#modalAccuserReception');
    medInfoModal = $('#medInfoModal');

    idIconBtnOperation = $('#idIconBtnOperation');
    btnPrintMedOrMep = $('#btnPrintMedOrMep');

    valDateEcheance = $('#valDateEcheance');
    valDateReception = $('#valDateReception');
    valDateCreate = $('#valDateCreate');
    valMedOrMep = $('#valMedOrMep');
    lblMedOrMep = $('#lblMedOrMep');
    divInfoMed = $('#divInfoMed');

    valExercice = $('#valExercice');
    valReference = $('#valReference');
    lblReference = $('#lblReference');

    selectPourcentId = $('#selectPourcentId');
    inputObservation = $('#inputObservation');
    cmbBanqueTaxationOffice = $('#cmbBanqueTaxationOffice');
    cmbCompteBancaireTaxationOffice = $('#cmbCompteBancaireTaxationOffice');

    inputObservation.val(empty);


    cmbBanqueTaxationOffice.on('change', function (e) {

        var selectBanqueCode = ($('#cmbBanqueTaxationOffice').val());

        if (selectBanqueCode == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

        var dataBankAccountList;

        for (var i = 0; i < bankList.length; i++) {

            if (bankList[i].codeBanque == selectBanqueCode) {

                dataBankAccountList += '<option value="0">--</option>';

                var accountList = JSON.parse(bankList[i].compteBancaireList);

                for (var j = 0; j < accountList.length; j++) {

                    dataBankAccountList += '<option value="' + accountList[j].codeCompteBancaire + '">' + accountList[j].libelleCompteBancaire + '</option>';

                }

                cmbCompteBancaireTaxationOffice.html(dataBankAccountList);
            }

        }
    });


    btnTaxationOffice = $('#btnTaxationOffice');
    btnTaxationOffice.on('click', function (e) {
        e.preventDefault();
        var taux = selectPourcentId.val();
        if (taux == '') {
            alertify.alert('Veuillez selectionner un taux correct.');
            return;
        }

        if (inputObservation.val() == '') {
            alertify.alert('Veuillez d\'abord fournir les obsevations');
            return;
        }

        if (cmbBanqueTaxationOffice.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

        if (cmbCompteBancaireTaxationOffice.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
            return;
        }


        medObj.percent = selectPourcentId.val();
        medObj.observation = inputObservation.val();
        medObj.codeBanque = cmbBanqueTaxationOffice.val();
        medObj.codeCompteBancaire = cmbCompteBancaireTaxationOffice.val();

        taxationOffice(medObj);
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAvancedSearch = '0';
        assujettiModal.modal('show');
    });

    btnPrintMedOrMep.on('click', function (e) {
        e.preventDefault();
        printMedOrMep(medDocumentPrint);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeModelTwo.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadMep(isAvancedSearch);
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeByArticle.modal('show');
    });

    btnAdvencedSearchByArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAvancedSearch = '1';
        assujettiCodeValue.val(empty);
        assujettiCodeValue.attr('style', 'font-weight:normal;width: 380px');
        loadRelance(isAvancedSearch);
    });
    setRegisterType('RELANCE');

    printRelance(empty);
    loadRelance(isAdvance);
    initDataBank();

});

function loadRelance(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':
            var valuePeriode = '';

            switch (codePeriodicite) {
                case 'PR0032015' :// Mensuelle
                    valuePeriode = $('#selectMois_1 option:selected').text() + '-' + selectAnnee_1.val();
                    break;
                case 'PR0042015' : // Annuelle
                    valuePeriode = selectAnnee_1.val();
                    break;
            }

            lblPeriodeDeclaration.text(valuePeriode);
            lblPeriodeDeclaration.attr('title', valuePeriode);

            lblService.text($('#selectService_ option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService_ option:selected').text().toUpperCase());

            lblArticleBudgetaire.text($('#selectAB option:selected').text().toUpperCase());
            lblArticleBudgetaire.attr('title', $('#selectAB option:selected').text().toUpperCase());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'articleBudgetaireCode': codeABSelected,
            'codePeriodicite': codePeriodicite,
            'periodValueYear': selectAnnee_1.val(),
            'periodValueMonth': selectMois_1.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': isAvancedSearch, //typeSearch,
            'operation': 'loadRelance'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printRelance('');
                        alertify.alert('Ce contribuable : ' + assujettiCodeValue.val() + ' n\'a pas d\invitation de service.');
                        break;
                    case '1':
                        printRelance('');
                        alertify.alert('Aucune relance ne correspond au critère de recherche fournis.');
                        break;
                }

            } else {
                medList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeByArticle.modal('hide');
                printRelance(medList);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printDefaillantPaiement(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">NOTE DE PERCEPTION</th>';
    tableContent += '<th style="text-align:center">DATE ORDO.</th>';
    tableContent += '<th style="text-align:center">DATE ECHEANCE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amount = formatNumber(result[i].amountNp, result[i].deviseNp);
        var buttonPrintMep = '';

        switch (result[i].printExist) {
            case '0':
                buttonPrintMep = '<button class="btn btn-warning" onclick="callPrintAndSaveMep(\'' + result[i].numeroNp
                        + '\',\'' + result[i].assujettiCode + '\',\'' + result[i].assujettiName + '\',\'' + result[i].adresseCode
                        + '\',\'' + result[i].dateOrdonnancement
                        + '\',\'' + result[i].exerciceNp
                        + '\',\'' + result[i].printExist + '\',\'' + result[i].dateEcheanceNp
                        + '\',\'' + result[i].numeroMed + '\',\'' + amount + '\',\'' + result[i].numeroNc + '\',\'' + result[i].amountNp + '\')"><i class="fa fa-print"></i></button>';
                break;
            case '1':
                buttonPrintMep = '<button class="btn btn-warning" onclick="printDocument(\'' + result[i].numeroMed + '\')"><i class="fa fa-list"></i></button>';
                break;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].exerciceNp + '</td>';
        tableContent += '<td style="text-align:left;width:30%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].numeroNp + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateOrdonnancement + '</td>';
        tableContent += '<td style="text-align:center;width:9%;color:red;vertical-align:middle;font-weight:bold">' + result[i].dateEcheanceNp + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold">' + amount + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintMep + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableDefaillantPaiement.html(tableContent);
    tableDefaillantPaiement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5
    });

}

function printRelance(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center">NUMERO</th>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">CONTRIBUABLE</th>';
    tableContent += '<th style="text-align:left">BIEN</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left">ECHEANCE RELANCE</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {


        var txtBtnModalTaxationOffice = 'Taxer d\'office';
        var iconModalTaxationOffice = 'fa fa-check-circle';
        var btnCallModalTaxationOffice = '';

        if (result[i].nextStap == '1' && result[i].estPayer != '1') {

            btnCallModalTaxationOffice = '<button class="btn btn-success" onclick="callGenerateTaxationOffice(\'' + result[i].periodeDeclarationId
                    + '\',\'' + result[i].assujettiCode
                    + '\',\'' + result[i].assujettiName + '\',\'' + result[i].articleBudgetaireCode + '\',\'' + result[i].articleBudgetaireName
                    + '\',\'' + result[i].amountPeriodeDeclaration + '\',\'' + result[i].numeroMed + '\',\'' + result[i].penaliteDu + '\',\'' + result[i].devisePeriodeDeclaration + '\',\'' + result[i].periodeDeclaration + '\',\'' + result[i].numeroDocument + '\')">' + txtBtnModalTaxationOffice + '<i class="' + iconModalTaxationOffice + '"></i></button>';

        }

        if (result[i].taxationOfficeExiste == '1') {
            txtBtnModalTaxationOffice = 'Ré-imprimer AMR ';
            btnCallModalTaxationOffice = '<button class="btn btn-default" onclick="printDocument(\'' + result[i].numeroDocument + '\',\'' + result[i].amrCode + '\')">' + txtBtnModalTaxationOffice + '<i class="fa fa-print"></i></button>';
        }

        var textBtn = '';

        if (result[i].accuserExist === '1') {
            textBtn = 'Ré-imprimer Relance';
        } else {
            textBtn = 'Accuser réception';
        }

        var color = '';
        if (result[i].nextStap === '1') {
            color = 'red';
        } else {
            color = 'green';
        }

        var statePayment = '';

        if (result[i].estPayer == '1') {
            statePayment = ' ' + '<span style="color:green">' + ' (payé)'.toUpperCase() + '</span>';
        } else {
            statePayment = ' ' + '<span style="color:red">' + ' (non payé)'.toUpperCase() + '</span>';
        }

        var buttonPrintRelance = '<button class="btn btn-default" onclick="accuserReception(\'' + result[i].accuserExist + '\',\'' + result[i].numeroMed + '\',\'' + result[i].numeroDocument + '\')">' + textBtn + ' <i class="fa fa-print"></i></button>';

        var btnOperation = buttonPrintRelance + '<br/><br/>' + btnCallModalTaxationOffice;

        var receptionInfo = 'DATE RECEPTION : ' + '<span style="font-weight:bold">' + result[i].receptionRelance + '</span>';
        var echeanceInfo = 'DATE ECHEANCE : ' + '<span style="font-weight:bold;color:' + color + '">' + result[i].echeanceRelance + '</span>';

        var dateDocumentInfo = receptionInfo + '<hr/>' + echeanceInfo;

        if (result[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';

            var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';

            var adresseInfo = result[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<hr/>' + adresseInfo;

        } else {
            var bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
            var natureInfo2 = 'Genre : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var adresseInfo2 = result[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
        }

        var principalDuInfo = 'PRINCIPAL DÛ : ' + '<span style="font-weight:bold;color:green">' + formatNumber(result[i].amountPeriodeDeclaration, result[i].devisePeriodeDeclaration) + statePayment + '</span>';
        var penaliteDuInfo = 'PENALITE DÛ : ' + '<span style="font-weight:bold;color:red">' + formatNumber(result[i].penaliteDu, result[i].devisePeriodeDeclaration) + '</span>';
        var amountDuInfo = principalDuInfo + '<hr/>' + penaliteDuInfo;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].periodeDeclaration + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + descriptionBien + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].articleBudgetaireName.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;width:15%;vertical-align:middle">' + amountDuInfo + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + dateDocumentInfo + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + btnOperation + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }

    tableContent += '</tbody>';
    tableDefaillants.html(tableContent);
    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5,
        dom: 'Bfrtip', columnDefs: [
            {"visible": false, "targets": 4}
        ], order: [[4, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="7">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

function callPrintAndSaveMep(numeroNp, assujettiCode, assujettiName, adresseCode, dateOrdonnancement,
        exerciceNp, printExist, dateEcheanceNp, numeroMed, amountNp1, numeroNc, amountNp2) {

    alertify.confirm('Etes-vous sûre de vouloir imprimer une mise en demeure pour cette note de perception ? ', function () {

        mepObj = new Object();

        mepObj.numeroNp = numeroNp;
        mepObj.assujettiCode = assujettiCode;
        mepObj.assujettiName = assujettiName;
        mepObj.adresseCode = adresseCode;
        mepObj.dateOrdonnancement = dateOrdonnancement;
        mepObj.exerciceNp = exerciceNp;
        mepObj.printExist = printExist;
        mepObj.dateEcheanceNp = dateEcheanceNp;
        mepObj.numeroMed = numeroMed;
        mepObj.amountNp1 = amountNp1;
        mepObj.amountNp2 = amountNp2;
        mepObj.numeroNc = numeroNc;

        saveAndPrintMep(mepObj);
    });
}

function callGenerateTaxationOffice(periodeID, assujettiCode, assujettiName,
        articleBudgetaireCode, articleBudgetaireName, amountPeriodeDeclaration,
        numeroMed, amountPenalite, devise, periodeDeclaration, numeroDocument) {

    var value = '<span style="font-weight:bold">' + numeroDocument + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir faire une taxation d\'office  pour la relance n° ' + value + ' ?', function () {

        medObj = new Object();

        medObj.periodeDeclarationId = periodeID;
        medObj.exercice = periodeID;
        medObj.assujettiCode = assujettiCode;
        medObj.assujettiName = assujettiName;
        //medObj.adresseCode = adresseCode;
        //medObj.adresseName = adresseName;
        //medObj.echeance = echeanceDeclaration;
        medObj.articleBudgetaireCode = articleBudgetaireCode;
        medObj.articleBudgetaireName = articleBudgetaireName;
        medObj.amountPeriodeDeclaration = amountPeriodeDeclaration;
        //medObj.printExist = printExist;
        medObj.numeroMed = numeroMed;
        medObj.amountPenalite = amountPenalite;
        medObj.devise = devise;
        medObj.periodeDeclarationName = periodeDeclaration;

        selectPourcentId.val(empty);
        inputObservation.val(empty);

        $('#modalRelanceChoosePourcent').modal('show');

    });

}


function getSelectedAssujetiData() {
    assujettiCodeValue.val(selectAssujettiData.nomComplet);
    assujettiCodeValue.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;
    isAvancedSearch = '0';

    switch (getRegisterType()) {
        case 'RELANCE':
            loadRelance(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }

}

function loadDefaillantPaiement(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(dateDebuts);
            lblDateDebut.attr('title', dateDebuts);

            lblDateFin.text(dateFins);
            lblDateFin.attr('title', dateFins);

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': codeService,
            'codeSite': codeSite,
            'dateDebut': dateDebuts,
            'dateFin': dateFins,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                printDefaillantPaiement('');
                alertify.alert('Aucune note de perception en retard de paiement.');
            } else {
                mepList = JSON.parse(JSON.stringify(response));
                printDefaillantPaiement(mepList);
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function taxationOffice(medObj) {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'periodeDeclarationId': medObj.periodeDeclarationId,
            'articleBudgetaireCode': medObj.articleBudgetaireCode,
            'assujettiCode': medObj.assujettiCode,
            'echeanceDeclaration': medObj.echeance,
            'periodeDeclaration': medObj.exercice,
            'adresseCode': medObj.adresseCode,
            'articleBudgetaireName': medObj.articleBudgetaireName,
            'assujettiName': medObj.assujettiName,
            'adresseName': medObj.adresseName,
            'amountPeriodeDeclarationToString': medObj.amountPeriodeDeclarationToString,
            'amountPeriodeDeclaration': medObj.amountPeriodeDeclaration,
            'percent': medObj.percent,
            'idUser': userData.idUser,
            'printExist': medObj.printExist,
            'numeroMed': medObj.numeroMed,
            'penaliteDu': medObj.amountPenalite,
            'devisePeriodeDeclaration': medObj.devise,
            'inputObservation': medObj.observation,
            'periodeDeclarationName': medObj.periodeDeclarationName,
            'codeBanque': medObj.codeBanque,
            'codeCompteBancaire': medObj.codeCompteBancaire,
            'operation': 'taxationOffice'
        },
        beforeSend: function () {
            $('#modalRelanceChoosePourcent').block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {

            $('#modalRelanceChoosePourcent').unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    $('#modalRelanceChoosePourcent').modal('hide');

                    loadRelance(isAvancedSearch);

                    window.open('visualisation-document', '_blank');

                }, 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $('#modalRelanceChoosePourcent').unblock();
            showResponseError();
        }

    });

}

function saveAndPrintMep(mepObj) {

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroNp': mepObj.numeroNp,
            'assujettiCode': mepObj.assujettiCode,
            'assujettiName': mepObj.assujettiName,
            'adresseCode': mepObj.adresseCode,
            'dateOrdonnancement': mepObj.dateOrdonnancement,
            'articleBudgetaireCode': mepObj.articleBudgetaireCode,
            'exerciceNp': mepObj.exerciceNp,
            'printExist': mepObj.printExist,
            'dateEcheanceNp': mepObj.dateEcheanceNp,
            'numeroMed': mepObj.numeroMed,
            'amountNp1': mepObj.amountNp1,
            'amountNp2': mepObj.amountNp2,
            'numeroNc': mepObj.numeroNc,
            'idUser': userData.idUser,
            'operation': 'printMep'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else {

                setTimeout(function () {

                    $.unblockUI();
                    setDocumentContent(response);

                    setTimeout(function () {
                    }, 2000);

                    loadMep(isAvancedSearch);

                    window.open('visualisation-document', '_blank');
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function printDocument(medId, amrCode) {

    var value = '<span style="font-weight:bold">' + medId + '</span>';

    alertify.confirm('Etes-vous sûr de vouloir ré-imprimer l\'AMR pour la relance n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': amrCode,
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune relance trouvée');
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function printMedOrMep(document) {

    var msg = '';

    switch (getRegisterType()) {
        case 'RELANCE':
            msg = 'Etes-vous sûre de vouloir imprimer une relance ';
            break;
        case 'MEP':
            msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure au paiement ? ';
            break;
    }

    alertify.confirm(msg, function () {

        setTimeout(function () {
            setDocumentContent(document);

            setTimeout(function () {
            }, 2000);
            window.open('visualisation-document', '_blank');
        }
        , 1);
    });

}

function getDefaillantPaiement() {

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;
    codeService = userData.serviceCode;
    codeSite = userData.SiteCode;

    loadDefaillantPaiement(1);

}

function loadMep(typeSearch) {

    switch (typeSearch) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblSite.text($('#selectService option:selected').text().toUpperCase());
            lblSite.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblService.text($('#selectService option:selected').text().toUpperCase());
            lblService.attr('title', $('#selectService option:selected').text().toUpperCase());

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'relance_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectService.val(),
            'codeSite': selectSite.val(),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': typeSearch,
            'operation': 'loadDefaillantPaiement'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                switch (typeSearch) {
                    case '0':
                        printDefaillantPaiement('');
                        alertify.alert('Cet assujetti : ' + assujettiCodeValue.val() + ' n\'a pas de notes de perception en retard de paiement.');
                        //return;
                        break;
                    case '1':
                        printDefaillantPaiement('');
                        alertify.alert('Aucune note de perception en retard de paiement ne corresponde au critère de recherche fournis.');
                        //return;
                        break;
                }


            } else {
                mepList = JSON.parse(JSON.stringify(response));
                modalRechercheAvanceeModelTwo.modal('hide');
                printDefaillantPaiement(mepList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function accuserReception(accuserExist, medID, numeroDocument) {


    for (var i = 0; i < medList.length; i++) {

        if (medList[i].numeroMed === medID) {
            codeMedCurrent = medList[i].numeroMed;
            typeMed = 'RELANCE';
            codeTypeDoc = typeMed;
        }
    }

    if (accuserExist == '0') {

        initAccuseReceptionUI(codeMedCurrent, typeMed);
        modalAccuserReception.modal('show');

    } else {

        var value = '<span style= "font-weight: bold">' + numeroDocument + '</span>';

        var msg = 'Etes-vous sûre de vouloir ré-imprimer la relance n° ' + value + ' ?';

        alertify.confirm(msg, function () {

            $.ajax({
                type: 'POST',
                url: 'declaration_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: true,
                data: {
                    'numero': medID,
                    'operation': 'printNoteTaxation'
                },
                beforeSend: function () {

                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

                },
                success: function (response)
                {

                    $.unblockUI();

                    if (response == '-1') {
                        $.unblockUI();
                        showResponseError();
                        return;
                    }

                    if (response == '0') {
                        $.unblockUI();
                        alertify.alert('Aucune relance trouvée correspondant à ce numéro : ' + value);
                        return;
                    }

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });
    }
}

function refrechDataAfterAccuserReception() {

    switch (typeMed) {
        case 'RELANCE':
            loadRelance(isAvancedSearch);
            break;
        case 'MEP':
            loadMep(isAvancedSearch);
            break;
    }
}

function initDataBank() {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initDataBank'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            bankList = JSON.parse(JSON.stringify(response));
            var dataBank = '';
            dataBank += '<option value="0">--</option>';

            for (var i = 0; i < bankList.length; i++) {
                dataBank += '<option value="' + bankList[i].codeBanque + '">' + bankList[i].libelleBanque + '</option>';
            }

            cmbBanqueTaxationOffice.html(dataBank);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

