/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var datePicker1;
var selectCompteBancaire;

var inputAssujetti;
var inputImpot;
var lblPeriodicite;
var selectAnnee, selectMois;
var inputCompteBancaire;
var inputMontantDu;
var inputBordereau;
var inputNumAttestation;
var inputDateEmissionBordereau;
var inputDeclaration;
var inputCentre;
var btnSearchImpot;
var ResearchImpotModal;
var btnSaveBordereau;
var tableBudgetArticles;
var codeImpot;
var periodicite;
var lblFaitGenerateur;
var divJour, inputJour;
var inputMontantDuFormat;

//var selectAnnee_1, selectMois_1;

$(function () {

    mainNavigationLabel.text('PAIEMENT');
    secondNavigationLabel.text('Déclaration');

    inputAssujetti = $('#inputAssujetti');
    inputImpot = $('#inputImpot');
    lblPeriodicite = $('#lblPeriodicite');
    //selectAnnee_1 = $('#selectAnnee_1');
    //selectMois_1 = $('#selectMois_1');
    inputCompteBancaire = $('#inputCompteBancaire');
    inputMontantDu = $('#inputMontantDu');
    inputBordereau = $('#inputBordereau');
    inputNumAttestation = $('#inputNumAttestation');
    inputDateEmissionBordereau = $('#inputDateEmissionBordereau');
    inputDeclaration = $('#inputDeclaration');
    inputCentre = $('#inputCentre');
    btnSearchImpot = $('#btnSearchImpot');
    ResearchImpotModal = $('#ResearchImpotModal');
    btnSaveBordereau = $('#btnSaveBordereau');
    lblFaitGenerateur = $('#lblFaitGenerateur');
    divJour = $('#divJour');
    inputJour = $('#inputJour');
    inputMontantDuFormat = $('#inputMontantDuFormat');

    datePicker1 = $("#datePicker1");
    selectCompteBancaire = $('#selectCompteBancaire');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePicker1.datepicker("setDate", new Date());


    inputMontantDu.on('change', function (e) {
        inputMontantDuFormat.val(formatNumber(inputMontantDu.val(), 'CDF'));
    });

    btnSaveBordereau.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkValueBordereau();
    });

    btnSearchImpot.click(function (e) {
        e.preventDefault();
        ResearchImpotModal.modal('show');
    });

    getAllCompteBancairesUsers();
    loadMois();
    loadAnnee();

});

function saveBordereau() {

//    var periode = selectMois.val() + ' ' + selectAnnee.val()

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'acountBanqueCode': inputCompteBancaire.val(),
            'bordereau': inputBordereau.val(),
            'dateBordereau': inputDateEmissionBordereau.val(),
            'userId': userData.idUser,
            'amountPercu': inputMontantDu.val(),
            'numeroAttestation': inputNumAttestation.val(),
            'numeroDeclaration': inputDeclaration.val(),
            'centre': inputCentre.val(),
            'nomComplet': inputAssujetti.val(),
            'articleBudgetaire': codeImpot,
            'periodicite': periodicite,
            'operation': 'saveBordereau'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >L\'enregistrement du bordereau en cours...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '700') {
                $.unblockUI();
                alertify.alert('Ce bordereau de paiement : ' + inputBordereau.val() + ' est déjà lié au compte bancaire : ' + inputCompteBancaire.val());
                return;
            } else {
                setTimeout(function () {
                    $.unblockUI();
                    alertify.alert('L\'enregistrement du paiement s\'est effectué avec succès.');
//                    setTimeout(function () {
//                        window.location = 'registre-encaissements';
//                    }, 2000);
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function checkValueBordereau() {

    amount = inputMontantDu.val();

    if (inputBordereau.val() === "" || inputDateEmissionBordereau.val() === ""
            || inputDeclaration.val() === "" || inputAssujetti.val().val === "" || inputCompteBancaire.val() === "") {
        alertify.alert('Veuillez d\'abord fournir toutes les valeurs recquises au paiement');
        return;
    }
    else if (!$.isNumeric(inputMontantDu.val())) {
        alertify.alert('Le montant perçu fourni n\'est pas un nombre');
    } else {

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce paiement ? ', function () {
            saveBordereau();
        });

    }
}

function getCurrentBudgetArticle() {


    dataArrayBudgetArticles = infoBudgetArticle;
    inputImpot.val(dataArrayBudgetArticles[0]);
    codeImpot = dataArrayBudgetArticles[1];
    lblFaitGenerateur.html(dataArrayBudgetArticles[2]);
    periodicite = dataArrayBudgetArticles[3];
    lblPeriodicite.html(dataArrayBudgetArticles[3]);

    if (dataArrayBudgetArticles[4] == 'PR0012015' || dataArrayBudgetArticles[4] == 'PR0022015') {
        divJour.removeAttr('hidden');
    } else {
        divJour.attr('hidden', 'true');
    }

    ResearchImpotModal.modal('hide');
}


function getAllCompteBancairesUsers() {

    var AllCompteBancairesUsers = JSON.parse(userData.accountList);
    var data;

    for (var i = 0; i < AllCompteBancairesUsers.length; i++) {
        data += '<option value="' + AllCompteBancairesUsers[i].accountCode + '">' + AllCompteBancairesUsers[i].accountName + ' (' + AllCompteBancairesUsers[i].accountCode + ')' + '</option>';
    }

    inputCompteBancaire.html(data);
//    mySelectService.selectpicker('refresh');
}


function loadMois() {

    var dataMois = '<option value ="0">-- Mois --</option>';
    dataMois += '<option value =01> Janvier </option>';
    dataMois += '<option value =02> Février </option>';
    dataMois += '<option value =03> Mars </option>';
    dataMois += '<option value =04> Avril </option>';
    dataMois += '<option value =05> Mai </option>';
    dataMois += '<option value =06> Juin </option>';
    dataMois += '<option value =07> Juillet </option>';
    dataMois += '<option value =08> Aôut </option>';
    dataMois += '<option value =09> Septembre </option>';
    dataMois += '<option value =10> Octobre </option>';
    dataMois += '<option value =11> Novembre </option>';
    dataMois += '<option value =12> Décembre </option>';
    selectMois.html(dataMois);
}

function loadAnnee() {

    var anneeDebut = 2000;
    var date = new Date();
    var anneeFin = date.getFullYear();

    var dataAnnee = '<option value ="0">-- Année --</option>';

    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }
    selectAnnee.html(dataAnnee);
}
