/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnEnregistrerTaux, selectTauxData;

var cmbDevise, cmbCorrespondance;

var inputTauxChange;

var tableTauxChange;

var deviseList = '';

var tauxList = [];

var codeDevise = '';

var codeCorrespondant = '';

var codeTaux = '';

$(function () {

    mainNavigationLabel.text('GESTION DES BANQUE');
    secondNavigationLabel.text('gestion des taux de change');

    tableTauxChange = $('#tableTauxChange');

    btnEnregistrerTaux = $('#btnEnregistrerTaux');

    cmbCorrespondance = $('#cmbCorrespondance');
    cmbDevise = $('#cmbDevise');

    inputTauxChange = $('#inputTauxChange');

    cmbDevise.on('change', function (e) {
        codeDevise = '';
        codeDevise = cmbDevise.val();
        getTauxByDivise();
    });

    cmbCorrespondance.on('change', function (e) {
        codeCorrespondant = cmbCorrespondance.val();
    });

    btnEnregistrerTaux.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputTauxChange.val() == '' || inputTauxChange.val() == 'null') {
            alertify.alert('Veuillez saisir le taux');
            return;
        }

        if (codeDevise == '' || codeDevise == 'null') {
            alertify.alert('Veuillez selectionner une devise.');
            return;
        }

        if (codeCorrespondant == '' || codeCorrespondant == 'null') {
            alertify.alert('Veuillez selectionner une devise correspondante.');
            return;
        }

        var action;
        if (codeTaux == '') {
            action = 'enregistrer ?';
        } else {
            action = 'modifier ?';
        }

        var message = 'Etes-vous sûrs de vouloir ' + action;

        alertify.confirm(message, function () {
            saveTaux();
        });
    });

    printTauxChange('');

    loadDevise();
});

function loadDevise() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDevise'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {

                return;
            }

            deviseList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner une devise ---</option>';

            for (var i = 0; i < deviseList.length; i++) {
                data += '<option value="' + deviseList[i].code + '">' + deviseList[i].intitule + '</option>';
            }
            cmbDevise.html(data);
            cmbCorrespondance.html(data);

        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function getTauxByDivise() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getTauxByDevise',
            'codeDevise': codeDevise
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                codeDevise = '';
                printTauxChange('');
            }

            tauxList = $.parseJSON(JSON.stringify(response));
            if (tauxList.length > 0) {
                printTauxChange(tauxList);
            } else {
                printTauxChange('');
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}




function printTauxChange(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">Date</th>';
    tableContent += '<th scope="col">Devise</th>';
    tableContent += '<th scope="col">Taux</th>';
    tableContent += '<th scope="col">Correspondance</th>';
    tableContent += '<th hidden="true" scope="col">Code taux</th>';
    tableContent += '<th hidden="true" scope="col">Code devise</th>';
    tableContent += '<th hidden="true" scope="col">Code correspondance</th>';
    tableContent += '<th scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="width:20%;vertical-align:middle">' + data[i].dateCreate + '</td>';
        tableContent += '<td style="width:20%;vertical-align:middle">' + data[i].deviseIntitule + '</td>';
        tableContent += '<td style="width:20%;vertical-align:middle">' + data[i].taux + '</td>';
        tableContent += '<td style="width:40%;vertical-align:middle">' + data[i].deviseDistinIntitule + '</td>';
        tableContent += '<td hidden="true">' + data[i].code + '</td>';
        tableContent += '<td hidden="true">' + data[i].devise + '</td>';
        tableContent += '<td hidden="true">' + data[i].deviseDist + '</td>';
        tableContent += '<td><button type="button" onclick="removeTaux(\'' + data[i].code + '\')" class="btn btn-success" title="Modifier le taux"><i class="fa fa-edit"></i></button></td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableTauxChange.html(tableContent);

    var myDt = tableTauxChange.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé devise _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableTauxChange tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectTauxData = new Object();
        selectTauxData.dateCreate = myDt.row(this).data()[0].toUpperCase();
        selectTauxData.deviseIntitule = myDt.row(this).data()[1].toUpperCase();
        selectTauxData.taux = myDt.row(this).data()[2].toUpperCase();
        selectTauxData.deviseDistinIntitule = myDt.row(this).data()[3].toUpperCase();
        selectTauxData.codeTaux = myDt.row(this).data()[4].toUpperCase();
        selectTauxData.code = myDt.row(this).data()[5].toUpperCase();
        selectTauxData.deviseDist = myDt.row(this).data()[6].toUpperCase();
        preparerModification();
    });
}

function preparerModification() {
    codeTaux = selectTauxData.codeTaux;
    inputTauxChange.val(selectTauxData.taux);
    if (selectTauxData.code != '') {
        cmbDevise.val(selectTauxData.code);
        codeDevise = selectTauxData.code;
    }
    if (selectTauxData.deviseDist != '') {
        cmbCorrespondance.val(selectTauxData.deviseDist);
        codeCorrespondant = selectTauxData.deviseDist;
    }
}

function saveTaux() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveTaux',
            'codeTaux': codeTaux,
            'idUser': userData.idUser,
            'codeDevise': codeDevise,
            'codeCorresp': codeCorrespondant,
            'tauxChange': inputTauxChange.val()

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des taux en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\opération effectuée avec succès.');
                    getTauxByDivise();
                    cleanData();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement des taux.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function cleanData() {

    inputTauxChange.val('');
    codeTaux = '';
    codeCorrespondant = '';
    cmbCorrespondance.val('');

}