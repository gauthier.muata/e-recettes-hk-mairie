/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAddBank;
var modalUpdateBanque;
var tableBanques;

var bankList = [];

var bankNameLong, bankNameShort, bankCodeSwift, btnSaveBank;

var codeBank;

var btnDeleteBank;
var modalAccoutBank, tableAccountBank,idBank;

$(function () {

    mainNavigationLabel.text('Gestion des banques');
    secondNavigationLabel.text('Edition d\'une banque');
    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    codeBank = empty;

    btnAddBank = $('#btnAddBank');
    modalUpdateBanque = $('#modalUpdateBanque');
    tableBanques = $('#tableBanques');

    bankNameLong = $('#bankNameLong');
    bankNameShort = $('#bankNameShort');
    bankCodeSwift = $('#bankCodeSwift');
    btnSaveBank = $('#btnSaveBank');
    btnDeleteBank = $('#btnDeleteBank');
    modalAccoutBank = $('#modalAccoutBank');
    tableAccountBank = $('#tableAccountBank');
    idBank = $('#idBank');

    btnAddBank.on('click', function (e) {
        e.preventDefault();

        codeBank = empty;
        bankNameLong.val(empty);
        bankNameShort.val(empty);
        bankCodeSwift.val(empty);

        btnDeleteBank.attr('style', 'display:none');
        modalUpdateBanque.modal('show');

    });

    btnSaveBank.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnDeleteBank.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteBank();

    });

    loadBanques();
});

function deleteBank() {

    alertify.confirm('Etes-vous sûre de vouloir supprimer la banque : ' + bankNameLong.val() + ' ?', function () {
        $.ajax({
            type: 'POST',
            url: 'banque_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'bankCode': codeBank,
                'userId': userData.idUser,
                'operation': 'deleteBank'
            },
            beforeSend: function () {
                modalUpdateBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression banque en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateBanque.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression de la banque s\'est effectuée avec succès');
                        resetFields();
                        modalUpdateBanque.modal('hide');
                        loadBanques();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalUpdateBanque.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    codeBank = empty;
    bankNameLong.val(empty);
    bankNameShort.val(empty);
    bankCodeSwift.val(empty);
}

function checkFields() {

    if (bankNameLong.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir l\'intitulé de la banque');
        bankNameLong.focus()
        return;
    }

    if (bankCodeSwift.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le code swift de la banque');
        bankCodeSwift.focus()
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cette banque ?', function () {
        saveBank();
    });
}

function saveBank() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'bankCode': codeBank,
            'bankNameLong': bankNameLong.val(),
            'bankNameShort': bankNameShort.val(),
            'userId': userData.idUser,
            'bankCodeSwift': bankCodeSwift.val(),
            'operation': 'saveBank'
        },
        beforeSend: function () {
            modalUpdateBanque.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement banques en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalUpdateBanque.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement de la banque s\'est effectué avec succès');
                    resetFields();
                    modalUpdateBanque.modal('hide');
                    loadBanques();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateBanque.unblock();
            showResponseError();
        }
    });
}

function loadBanques() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des banques en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    bankList = JSON.parse(JSON.stringify(response));
                    printTableBanque(bankList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableBanque(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:40%">INTITULE</th>';
    tableContent += '<th style="text-align:left;width:15%">SIGLE</th>';
    tableContent += '<th style="text-align:left;width:10%">CODE SWIFT</th>';
    tableContent += '<th style="text-align:center;width:10%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditBank = '<button class="btn btn-success" onclick="editBank(\'' + result[i].bankCode + '\')"><i class="fa fa-edit"></i></button>';
        var btnAccountBank = '<button class="btn btn-warning" onclick="accountBank(\'' + result[i].bankCode + '\',\'' + result[i].accountBankExist + '\')"><i class="fa fa-list"></i></button>';

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:35%">' + result[i].bankNameLong + '</td>';
        tableContent += '<td style="text-align:left;width:15%">' + result[i].bankNameShort + '</td>';
        tableContent += '<td style="text-align:left;width:10%">' + result[i].bankSwiftCode + '</td>';
        tableContent += '<td style="text-align:center;width:10%">' + btnEditBank + '&nbsp;&nbsp;' + btnAccountBank + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableBanques.html(tableContent);

    tableBanques.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par intitulé _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function editBank(bankCode) {

    for (var i = 0; i < bankList.length; i++) {

        if (bankList[i].bankCode == bankCode) {

            codeBank = bankCode;

            bankNameLong.val(bankList[i].bankNameLong);
            bankNameShort.val(bankList[i].bankNameShort);
            bankCodeSwift.val(bankList[i].bankSwiftCode);

            btnDeleteBank.attr('style', 'display:inline');

            modalUpdateBanque.modal('show');

            break;
        }
    }

}

function accountBank(bankCode, exist) {

    if (exist == '1') {

        for (var i = 0; i < bankList.length; i++) {

            if (bankList[i].bankCode == bankCode && bankList[i].accountBankExist == '1') {

                codeBank = bankCode;

                var accountList = JSON.parse(bankList[i].accountBankList);
                printTableAccountBank(accountList, bankList[i].bankNameLong);

                break;
            }
        }

    } else {

        alertify.alert('Cette banque n\'a pas des comptes bancaires');
    }


}

function printTableAccountBank(result, bankName) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">LIBELLE COMPTE BANCAIRE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';
    
    idBank.html(bankName.toUpperCase());
    
    for (var i = 0; i < result.length; i++) {

        var libelle = result[i].acountBankCode + '-' + result[i].acountBankName;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:100%;vertical-align:middle">' + libelle + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableAccountBank.html(tableContent);
    tableAccountBank.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

    modalAccoutBank.modal('show');
}