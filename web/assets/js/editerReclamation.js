/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var lblLegalForm, lblNifResponsible, lblNameResponsible, lblAddress, lblNbDocumentReclamation;
var btnCallModalSearchResponsable, btnEnregistrerReclammation, btnAjouterDocumentRole;
var btnSearchAndAddDocument, divEntiteTableRefObs;
var lbl1, lbl2, lbl3, lbl4;
var btnJoindreArchiveReclamation;
var inputNumeroDocument, textReferenceCourrier, inputValueResearchAssujetti;
var tableReclammation, tableTitreAMR;
var textObservation;
var typeDoc = 'AMR', typeReclamation = '1';
var codeDoc = undefined;
var codeTypeDocs;
var archivesReclamation, fkDocument, etatReclamation;
var tempDocumentList = [], tempAMR = [];
var codeResponsible = '';
var assujettiModal;
var codeFormeJuridique = '';
var adresseId = '';


$(function () {

    mainNavigationLabel.text('CONTENTIEUX');
    secondNavigationLabel.text('Editer une réclamation');

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    codeTypeDocs = 'AMR1';

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblNbDocumentReclamation = $('#lblNbDocumentReclamation');
    inputNumeroDocument = $('#inputNumeroDocument');
    textReferenceCourrier = $('#textReferenceCourrier');
    textObservation = $('#textObservation');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');
    btnEnregistrerReclammation = $('#btnEnregistrerReclammation');
    divEntiteTableRefObs = $('#divEntiteTableRefObs');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    tableReclammation = $('#tableReclammation');
    tableTitreAMR = $('#tableTitreAMR');

    cmbTypeDocument = $('#cmbTypeDocument');
    inputNumeroDocument = $('#inputNumeroDocument');
    inputValueResearchAssujetti = $('#inputValueResearchAssujetti');

    textReferenceCourrier = $('#textReferenceCourrier');
    textObservation = $('#textObservation');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');
    modalDetailExtraitRole = $('#modalDetailExtraitRole');
    radioTypeReclamationT = $('#radioTypeReclamationT');
    radioTypeReclamationOR = $('#radioTypeReclamationOR');
    btnJoindreArchiveReclamation = $('#btnJoindreArchiveReclamation');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    assujettiModal = $('#assujettiModal');

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    cmbTypeDocument.on('change', function () {
        typeDoc = cmbTypeDocument.val();
    });

    btnSearchAndAddDocument.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (inputNumeroDocument.val().trim() == '') {
            alertify.alert('Veuillez d\'abord saisir un titre de l\'avis mise en recouvrement.');
            return;
        }

        if (typeDoc == '0') {
            alertify.alert('Veuiller d\'abord sélectionner un type document.');
            return;
        }

        if (checkDocument()) {
            alertify.alert('Veuillez d\'abord enregistrer le titre ajouté.');
            return;
        }

        searchDocument();

    });

    $('input[name=radioTypeReclamation]').on('change', function (e) {

        typeReclamation = $('input[name=radioTypeReclamation]:checked').val();

        var data = '';

        switch (typeReclamation) {
            case '1':
                data += '<option value ="AMR">Avis de mise en recouvrement</option>';
                typeDoc = 'AMR';
                break;
        }

        cmbTypeDocument.html(data);

    });

    btnJoindreArchiveReclamation.click(function (e) {

        e.preventDefault();

        if (archivesReclamation == '') {
            initUpload(codeDoc, codeTypeDocs);
        } else {
            initUpload(archivesReclamation, codeTypeDocs);
        }
    });

    btnEnregistrerReclammation.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (tempAMR.length === 0) {
            alertify.alert('Veuillez choisir au moins un titre d\'AMR.');
            return;
        }

        var listDocument = selectDocument();

        for (var i = 0; i < tempAMR.length; i++) {

            var idMontantNonCont = '#inputMontNonCont' + tempAMR[i].referenceDocument;
            var textMontantNonCont = $('#tableReclammation').find(idMontantNonCont);


            if (textMontantNonCont.val().trim() == '') {
                alertify.alert('Veuillez saisir le montant contesté pour chaque titre d\'AMR.');
                return;
            }

            if (textMontantNonCont.val().trim() > tempAMR[i].amount) {
                alertify.alert('Montant contesté ne peut pes être supérieur au montant dû');
                return;
            }

            if (tempAMR[i].titreExistToReclamation == '1') {
                alertify.alert('Ce titre est déjà en contentieux');
                inputNumeroDocument.val('');
                cmbTypeDocument.val('0');
                textObservation.val('');
                textReferenceCourrier.val('');
                tempAMR.length = 0;
                printAMRDocument('');
                return;

            }

            if (textReferenceCourrier.val().trim() == '') {
                alertify.alert('Veuillez saisir la référence du courrier de réclamation.');
                return;
            }
            if (tempAMR[i].etatReclamation == '9') {
                if (listDocument === undefined) {
                    alertify.alert('Veuillez d\'abord sélectionner au moins un AMR.')
                    return;
                } else {
                    alertify.confirm('Etes-vous sûre de vouloir enregistrer une réclamation dont le délai est au-delà de six mois ?', function () {

                        saveReclamation(listDocument);

                    });
                    return;
                }
            }

            break;
        }


        if (listDocument === undefined) {
            alertify.alert('Veuillez d\'abord sélectionner au moins un AMR.')
            return;
        } else {
            alertify.confirm('Etes-vous sûre de vouloir enregistrer cette réclamation ?', function () {

                saveReclamation(listDocument);

            });
        }

    });

});

function getSelectedAssujetiData() {
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    codeResponsible = selectAssujettiData.code;
    lblNifResponsible.html(selectAssujettiData.nif);
    codeFormeJuridique = selectAssujettiData.codeForme;
    adresseId = selectAssujettiData.codeAdresse;

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();

    responsibleObject = new Object();

    responsibleObject.codeResponsible = codeResponsible;
    responsibleObject.codeFormeJuridique = codeFormeJuridique;
    responsibleObject.adresseId = adresseId;
}

function searchDocument() {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujitteDossier',
            'codeAssujetti': codeResponsible,
            'numeroDocument': inputNumeroDocument.val(),
            'typeSearchDocument': typeDoc,
            'idUser': userData.idUser,
            'codeService': userData.serviceCode,
            'codeSite': userData.SiteCode
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Ce titre d\'avis de mise en recouvrement déjà associé à une réclamation ou ne fait pas l\'objet d\'une réclamation.');
                return;
            }

            if (response == '6') {
                alertify.alert('Le titre d\'avis de mise en recouvrement fourni manque une date de réception.');
                return;
            }

            setTimeout(function () {

                var documentList = JSON.parse(JSON.stringify(response));

                if (documentList.titreExistToReclamation == '1') {
                    alertify.alert('Ce titre d\'avis de mise en recouvrement est déjà associé à une demande réclamation qui est en cours de traitement.');
                    return;
                }

                //alert(JSON.stringify(documentList));

                printAMRDocument(documentList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

    var paramGuideValue = JSON.parse(getTourParam());

    if (paramGuideValue === 1) {
        Tour.run([
            {
                element: $('#divEntiteTableRefObs'),
                content: '<p style="font-size:17px;color:black">Entrer la référence du courrier de réclamation, l\'observation si necessaire et enfin cliquer sur le bouton enregistrer.</p>',
                language: 'fr',
                position: 'top',
                close: 'false'
            }
        ]);
    } else {
        Tour.run();
    }

}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesReclamation = getUploadedData();

    nombre = JSON.parse(archivesReclamation).length;

    switch (nombre) {
        case 0:
            lblNbDocumentReclamation.text('');
            break;
        case 1:
            lblNbDocumentReclamation.html('1 document');
            break;
        default:
            lblNbDocumentReclamation.html(nombre + ' documents');
    }
}

function printAMRDocument(documentList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> Titre </th>';
    header += '<th scope="col"> Fiche de prise en charge </th>';
    header += '<th scope="col"> Date création </th>';
    header += '<th scope="col"> Date exi. </th>';
    header += '<th scope="col"> Nombre de mois </th>';
    header += '<th scope="col"> Article budgétaire </th>';
    header += '<th scope="col"> Montant dû </th>';
    header += '<th scope="col"> Montant contesté </th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody id="idBodyAMR">';

    for (var i = 0; i < documentList.length; i++) {

        var firstLineAB = '';

        if (documentList[i].libelleArticleBudgetaire.length > 100) {
            firstLineAB = documentList[i].libelleArticleBudgetaire.substring(0, 100).toUpperCase() + ' ...';
        } else {
            firstLineAB = documentList[i].libelleArticleBudgetaire.toUpperCase();
        }


        var amr = {};
        amr.referenceDocument = documentList[i].referenceDocument;
        amr.numeroFichePriseEnCharge = documentList[i].numeroFichePriseEnCharge;
        amr.titreExistToReclamation = documentList[i].titreExistToReclamation;
        amr.typeDocument = documentList[i].typeDocument;
        amr.dateCreate = documentList[i].dateCreate;
        amr.dateEcheance = documentList[i].dateEcheance;
        amr.codeOfficiel = documentList[i].codeOfficiel;
        amr.libelleArticleBudgetaire = documentList[i].libelleArticleBudgetaire;
        amr.amount = documentList[i].MONTANTDU;
        amr.devise = documentList[i].DEVISE;
        amr.dateReceptionCourrier = documentList[i].dateReceptionCourrier;
        amr.nombreMois = documentList[i].nombreMois;
        amr.nombreJour = documentList[i].nombreJour;
        amr.etatReclamation = documentList[i].etatReclamation;
        tempAMR.push(amr);

        body += '<tr>';
        body += '<td style="width:10%;vertical-align:middle">' + documentList[i].referenceDocument + '</td>';
        body += '<td style="width:10%;vertical-align:middle">' + documentList[i].numeroFichePriseEnCharge + '</td>';
        body += '<td style="width:10%;vertical-align:middle">' + documentList[i].dateCreate + '</td>';
        body += '<td style="width:10%;vertical-align:middle">' + documentList[i].dateEcheance + '</td>';
        body += '<td style="width:10%;vertical-align:middle"><center>' + documentList[i].nombreMois + '</td>';
        body += '<td style="width:25%;vertical-align:middle" title="' + documentList[i].libelleArticleBudgetaire.toUpperCase() + '"><span style="font-weight:bold;">' + documentList[i].codeOfficiel + '</span><br/><br/>' + firstLineAB + '</td>';
        body += '<td style="width:9%;vertical-align:middle">' + formatNumber(documentList[i].MONTANTDU, documentList[i].DEVISE) + '</td>';
        body += '<td style="width:12%;vertical-align:middle"><input type="number" id="inputMontNonCont' + documentList[i].referenceDocument + '" placeholder="Saisir montant non contesté" style="width:100%"></input></td>';

        if (documentList[i].titreExistToReclamation == '1') {
            body += '<td style="width:5%"></td>';
        } else {
            body += '<td style="width:5%;vertical-align:middle"><center><input type="checkbox" id="checkDocument_' + documentList[i].referenceDocument + '" name="checkDocument_' + documentList[i].referenceDocument + '"></center></td>';
        }

        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableContent += '</tbody>';
    tableReclammation.html(tableContent);
    tableReclammation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 0}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[0, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

//function verifySelection() {
//
//    var isSelected = false;
//    
//    if (tempAMR.length > 0){
//         for (var i = 0; i < tempAMR.length; i++) {
//
//        if (tempAMR[i].titreExistToReclamation == '0') {
//
//            var idDocument = '#checkDocument_' + tempAMR[i].referenceDocument;
//            var document = $('#tableReclammation').find(idDocument);
//            if (document.is(':checked')) {
//                isSelected = true;
//                break;
//            }
//        }
//    }
//    }
//    return isSelected;
//}

function selectDocument() {

    var listDocument = [];

    if (tempAMR.length > 0) {
        for (var i = 0; i < tempAMR.length; i++) {
            if (tempAMR[i].titreExistToReclamation == '0') {

                var idDocument = '#checkDocument_' + tempAMR[i].referenceDocument;
                var document = $('#tableReclammation').find(idDocument);

                var idMontantNonCont = '#inputMontNonCont' + tempAMR[i].referenceDocument;
                var montantCont = $('#tableReclammation').find(idMontantNonCont).val();

                if (document.is(':checked')) {
                    var documentList = {};
                    documentList.referenceDocument = tempAMR[i].referenceDocument;
                    documentList.numeroFichePriseEnCharge = tempAMR[i].numeroFichePriseEnCharge;
                    documentList.titreExistToReclamation = tempAMR[i].titreExistToReclamation;
                    documentList.typeDocument = tempAMR[i].typeDocument;
                    documentList.dateCreate = tempAMR[i].dateCreate;
                    documentList.dateEcheance = tempAMR[i].dateEcheance;
                    documentList.codeOfficiel = tempAMR[i].codeOfficiel;
                    documentList.libelleArticleBudgetaire = tempAMR[i].libelleArticleBudgetaire;
                    documentList.amount = tempAMR[i].amount;
                    documentList.devise = tempAMR[i].DEVISE;
                    documentList.dateReceptionCourrier = tempAMR[i].dateReceptionCourrier;
                    documentList.montantContester = montantCont;
                    documentList.motif = 'Demande d\'une réclamation';
                    fkDocument = tempAMR[i].referenceDocument;
                    etatReclamation = tempAMR[i].etatReclamation;
                    listDocument.push(documentList);
                } else {
                    return;
                }
            }
        }
        listDocument = JSON.stringify(listDocument);

    } else {
        alertify.alert('Veuillez d\'abord sélectionner au moins un AMR.')
        return;
    }
    return listDocument;
}

function checkDocument() {
    var exist = false;

    for (var i = 0; i < tempAMR.length; i++) {
        if (tempAMR.length >= 1) {
            exist = true;
            break;
        }
    }
    return exist;
}

function resetData() {
    codeResponsible = '';
    lblNameResponsible.html('');
    lblLegalForm.html('');
    lblAddress.html('');
    lblNifResponsible.html('');
    lblNbDocumentReclamation.html('');
    archivesReclamation = '';
    fkDocument = '';

    lbl1.hide();
    lbl2.hide();
    lbl3.hide();
    lbl4.hide();

    inputNumeroDocument.val('');
    inputValueResearchAssujetti.val('');
    cmbTypeDocument.val('0');
    textObservation.val('');
    textReferenceCourrier.val('');
    tempAMR.length = 0;

    printAMRDocument('');
}

function saveReclamation(listDocument) {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveReclamation',
            'userId': userData.idUser,
            'observation': textObservation.val(),
            'typeReclamation': typeReclamation,
            'codeAssujetti': codeResponsible,
            'referenceCourrierReclamation': textReferenceCourrier.val(),
            'listDocument': listDocument,
            'fkDocument': fkDocument,
            'observationDocument': textObservDoc.val(),
            'etatReclamation': etatReclamation,
            'archives': archivesReclamation
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de la réclamation en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement de la réclamation s\'est effectué avec succès.');
                    resetData();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de la réclamation.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

