/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalRechercheAvancee;
var modalRechercheAvanceeNC;
var dataService;
var modalRechercheAvanceeMed;

var selectBanque, selectCompteBancaire;
var selectSite, selectService;

var inputDateDebut;
var inputdateLast;

var datePickerDebut;
var datePickerFin;

var divisionList = [];

$(function () {

    modalRechercheAvancee = $('#modalRechercheAvancee');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');
    modalRechercheAvanceeMed = $('#modalRechercheAvanceeMed');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    selectSite = $('#selectSite');
    selectService = $('#selectService');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());

    var UsersSiteList = JSON.parse(userData.siteUserList);
    var AllSiteList = JSON.parse(userData.allSiteList);
    var BanqueUserList = JSON.parse(userData.banqueUserList);
    var AllServiceList = JSON.parse(userData.allServiceList);
    var dataSite = '';
    var dataBanque = '';

    $('.cmbDiviision').click(function (e) {
        e.preventDefault();
        setDataBureau($('.cmbDiviision').val());
    });

    setDataDivision();

    if (!controlAccess('VIEW_ALL_SITE')) {

        for (var i = 0; i < UsersSiteList.length; i++) {
            dataSite += '<option value="' + UsersSiteList[i].SiteCode + '">' + UsersSiteList[i].siteName.toUpperCase() + '</option>';
        }
        var selectSiteContent = dataSite;

        selectSite.html(selectSiteContent);
    }
    else {
        dataSite += '<option value="*">*</option>';
        for (var i = 0; i < AllSiteList.length; i++) {
            dataSite += '<option value="' + AllSiteList[i].SiteCode + '">' + AllSiteList[i].siteName.toUpperCase() + '</option>';

        }
        var selectSiteContent = dataSite;

        selectSite.html(selectSiteContent);
    }

    dataBanque += '<option value="*">*</option>';

    for (var i = 0; i < BanqueUserList.length; i++) {
        dataBanque += '<option value="' + BanqueUserList[i].banqueCode + '">' + BanqueUserList[i].banqueName.toUpperCase() + '</option>';
    }
    var selectSiteContent = dataBanque;

    selectBanque.html(selectSiteContent);

    selectCompteBancaire.html('<option value="*">*</option>');


    if (!controlAccess('SEARCH_FOR_ALL_SERVICES')) {

        dataService += '<option value="' + userData.serviceCode + '">' + userData.serviceAssiette.toUpperCase() + '</option>';
        var selectServiceContent = dataService;

        selectService.html(selectServiceContent);

    } else {

        dataService += '<option value="*">*</option>';

        for (var i = 0; i < AllServiceList.length; i++) {
            dataService += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName.toUpperCase() + '</option>';

        }
        var selectServiceContent = dataService;

        selectService.html(selectServiceContent);
    }

    selectService.val(userData.serviceCode);
    selectSite.val(userData.SiteCode);

    selectBanque.on('change', function (e) {

        var selectBanqueCode = ($('#selectBanque').val());
        var dataBankAccountList;

        if (selectBanqueCode !== "*") {
            var getAccountList = JSON.parse(userData.accountList);

            var accountList = getAccountByBanque(getAccountList, selectBanqueCode);

            dataBankAccountList += '<option value="*">*</option>';
            for (var i = 0; i < accountList.length; i++) {
                dataBankAccountList += '<option value="' + accountList[i].accountCode + '">' + accountList[i].accountCode + ' - ' + accountList[i].accountName + '</option>';
            }

        } else {
            dataBankAccountList = '<option value="*">*</option>';
        }

        selectCompteBancaire.html(dataBankAccountList);

    });

});

function getAccountByBanque(bankAccountList, selectBanque) {

    var accountList = [];

    for (var i = 0; i < bankAccountList.length; i++) {

        if (selectBanque === bankAccountList[i].acountBanqueCode) {

            accountList.push(bankAccountList[i]);
        }
        else if (selectBanque === '') {
            accountList.push(bankAccountList[i].acountBanqueCode);
        }
    }

    return accountList;
}

function setDataDivision() {
    var data = JSON.parse(userData.utilisateurDivisionList);

    var option = '';
    if (data.length > 1) {
        option = '<option value="">S&eacute;lectionner une division</option>';
    }
    $.each(data, function (index, item, array) {
        if (!checkDivisionExists(item.codeDivision)) {
            option += '<option value="' + item.codeDivision + '">' + item.intituleDivision + '</option>';
            var division = {};
            division.code = item.codeDivision;
            divisionList.push(division);
        }
    });
    $('.cmbDiviision').append(option);
    $('.cmbBureau').html('<option value="">S&eacute;lectionner un bureau</option>');
    if (data.length == 1) {
        setDataBureau($('.cmbDiviision').val());
    }
}

function setDataBureau(codeDivision) {
    var data = JSON.parse(userData.utilisateurDivisionList);
    var option = '';
    if (data.length > 1) {
        option = '<option value="">S&eacute;lectionner un bureau</option>';
    }
    $.each(data, function (index, item, array) {
        if (item.codeDivision == codeDivision) {
            option += '<option value="' + item.codeBureau + '">' + item.intituleBureau.toUpperCase() + '</option>';
        }
    });
    $('.cmbBureau').html(option);
}

function checkDivisionExists(code) {
    for (var i = 0; i < divisionList.length; i++) {

        if (divisionList[i].code == code) {
            return true;
        }
    }
    return false;
}
