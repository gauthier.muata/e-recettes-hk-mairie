/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var dataList;

var spnNifID, spnNameID, spnAddressID, lblModule, spnValueNumberID, spnCreditUsdID, spnCreditCdfID, tableData;


$(function () {

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Compte courant fiscal assujetti');

    spnNifID = $("#spnNifID");
    spnNameID = $("#spnNameID");
    spnAddressID = $("#spnAddressID");
    lblModule = $("#lblModule");
    spnValueNumberID = $("#spnValueNumberID");
    spnCreditUsdID = $("#spnCreditUsdID");
    spnCreditCdfID = $("#spnCreditCdfID");
    tableData = $("#tableData");

    displayCompteCourant(JSON.parse(sessionStorage.getItem('compteFiscalAssujetti')));

});

function displayCompteCourant(result) {

    if (result !== null) {

        dataList = empty;

        spnNifID.html(result.nif);
        spnNameID.html(result.nom);
        spnAddressID.html(result.adresse);
        lblModule.html(result.lblModule + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: ' + '<span style="font-weight: bold;font-size: 15px">' + result.nbre + '</span>');
        //spnValueNumberID.html(result.nbre);
        spnCreditUsdID.html(formatNumber(result.creditUsd, 'USD'));
        spnCreditCdfID.html(formatNumber(result.creditCdf, 'CDF'));

        switch (result.module) {
            
            case 'TAX':

                if (result.TAXATION_EXIST == '1') {

                    dataList = JSON.parse(result.TAXATION_LIST);
                }

                printDataCompteCourantFiscalForTaxation(dataList);

                break;

            case 'REC':
                break;
        }
    }
}

function printDataCompteCourantFiscalForTaxation(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left"></th>'
    tableContent += '<th style="text-align:center;width:6%"scope="col">EXERCICE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">SERVICE D\'ASSIETTE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">SITE</th>';
    tableContent += '<th style="text-align:left;width:12%"scope="col">NOTE DE CALCUL</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">TAXATION</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">VALIDATION</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">ORDONNANCEMENT</th>';
    tableContent += '<th style="text-align:right;width:12%"scope="col">MONTANT DÛ</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var agentTaxation = empty;
        var dateTaxation = empty;
        var taxationInfo = empty;

        agentTaxation = 'Taxateur : ' + '<span style="font-weight:bold">' + result[i].TAXATEUR + '</span><br/><br/>';
        dateTaxation = 'Date taxation : ' + '<span style="font-weight:bold">' + result[i].DATE_TAXATION + '</span>';
        taxationInfo = agentTaxation + dateTaxation;

        var agentValidation = empty;
        var dateValidation = empty;
        var avisValidation = empty;
        var validationInfo = empty;

        agentValidation = 'Validateur : ' + '<span style="font-weight:bold">' + result[i].VALIDATEUR + '</span><br/><br/>';
        dateValidation = 'Date validation : ' + '<span style="font-weight:bold">' + result[i].DATE_VALIDATION + '</span><br/><br/>';
        avisValidation = 'Avis validation : ' + '<span style="font-weight:bold">' + result[i].ETAT_VALIDATION + '</span>';

        validationInfo = agentValidation + dateValidation + avisValidation;

        var agentOrdonnancement = empty;
        var dateOrdonnancement = empty;
        var avisOrdonnancement = empty;
        var ordonnancementInfo = empty;

        var color = '';

        if (result[i].ETAT_VALIDATION_ID == 1) {

            if (result[i].AVIS_ORDONNACEMENT_ID == 1) {
                color = 'green';
            } else {
                color = 'red';
            }
        } else {
            color = 'red';
        }

        agentOrdonnancement = 'Ordonnateur : ' + '<span style="font-weight:bold">' + result[i].ORDONNATEUR + '</span><br/><br/>';
        dateOrdonnancement = 'Date ordonnacement : ' + '<span style="font-weight:bold">' + result[i].DATE_ORDONNACEMENT + '</span><br/><br/>';
        avisOrdonnancement = 'Avis ordonnacement : ' + '<span style="font-weight:bold">' + result[i].AVIS_ORDONNACEMENT.toUpperCase() + '</span>';

        ordonnancementInfo = agentOrdonnancement + dateOrdonnancement + avisOrdonnancement;

        tableContent += '<tr>';
        tableContent += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        tableContent += '<td style="vertical-align:middle;width:5%;text-align:center">' + result[i].exerciceFiscal + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].SERVICE + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%">' + result[i].SITE + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%">' + result[i].numero + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20%">' + taxationInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20%">' + validationInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:17%">' + ordonnancementInfo + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;width:15%;font-weight:bold;color:' + color + '">' + formatNumber(result[i].amountNc, result[i].deviseNc) + '</td>';
        tableContent += '</tr>';

    }

    tableContent += '</tbody>';

    tableData.html(tableContent);

    var myDataTable = tableData.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 20,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="10">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableData tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = myDataTable.row(tr);
        var dataDetail = myDataTable.row(tr).data();
        var numeroTitre = dataDetail[4];

        //var tableTitreDependant = '';
        var tableDetailNc = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {

            tableDetailNc = '<center><h4 >LES DETAILS DE LA NOTE DE TAXATION N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableDetailNc += '<thead><tr style="background-color:#e6ceac;color:black"><td style="text-align:left;width:30%;font-weight:bold">ACTE GENERATEUR</td><td style="text-align:left;width:10%;font-weight:bold">PERIODE</td><td style="text-align:center;width:10%;font-weight:bold">BASE</td><td style="text-align:center;width:10%;font-weight:bold">TAUX</td><td style="text-align:center;width:7%;font-weight:bold">NBRE. D\'ACTE</td><td style="text-align:right;width:10%;font-weight:bold">MONTANT DÛ</td></tr></thead>';

            for (var j = 0; j < dataList.length; j++) {

                if (dataList[j].numero == numeroTitre) {

                    var detailNcList = JSON.parse(dataList[j].DETAIL_LIST);

                    for (var k = 0; k < detailNcList.length; k++) {

                        tableDetailNc += '<tr>';
                        tableDetailNc += '<td style="text-align:left;width:20%;vertical-align:middle">' + detailNcList[k].ARTICLE_BUDGETAIRE.toUpperCase() + ' : ' + detailNcList[k].TARIF.toUpperCase() + '</td>';
                        tableDetailNc += '<td style="text-align:left;width:10%;vertical-align:middle">' + detailNcList[k].PERIODE + '</td>';
                        tableDetailNc += '<td style="text-align:center;width:10%;vertical-align:middle">' + detailNcList[k].BASE + '</td>';
                        tableDetailNc += '<td style="text-align:center;width:10%;vertical-align:middle">' + detailNcList[k].TAUX + ' ' + detailNcList[k].TYPE_TAUX + '</td>';
                        tableDetailNc += '<td style="text-align:center;width:5%;vertical-align:middle">' + detailNcList[k].QTE + '</td>';
                        tableDetailNc += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(detailNcList[k].MONTANT_DU, detailNcList[k].DEVISE) + '</td>';
                        tableDetailNc += '</tr>';
                    }
                }
            }

            tableDetailNc += '<tbody>';

            tableDetailNc += '</tbody>';
            row.child(tableDetailNc).show();
            tr.addClass('shown');

        }

    });

}
