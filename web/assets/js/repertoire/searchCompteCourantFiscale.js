/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {


});

function loadCompteCourant(codePersonne, module) {

    $.ajax({
        type: 'POST',
        url: 'compteCourantFiscale',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadCompteFiscalAssujetti',
            'assujetti': codePersonne,
            'module': module
        },
        beforeSend: function () {

            modalChooseModule.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            modalChooseModule.unblock();

            var moduleText = empty;

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                switch (module) {
                    case 'TAX':
                        moduleText = 'Taxation';
                        break;
                    case 'REC':
                        moduleText = 'Recouvrement';
                        break;
                    case 'FRAC':
                        moduleText = 'Fractionnement';
                        break;
                    case 'MED':
                        moduleText = 'Mise en demeure';
                        break;
                    case 'POUR':
                        moduleText = 'Poursuites';
                        break;
                    case 'CONT':
                        moduleText = 'Contentieux';
                        break;
                }

                alertify.alert('Cet assujetti ne possède des données dans le module de : ' + moduleText.toUpperCase());
                return;
                
            } else {
                setTimeout(function () {
                    //dataList = JSON.parse(JSON.stringify(response));
                    sessionStorage.setItem('compteFiscalAssujetti', JSON.stringify(response));
                    window.location = 'compte-courant-fiscal-assujetti';
                    modalChooseModule.modal('hide');
                }
                , 1000);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalChooseModule.unblock();
            showResponseError();
        }
    });
}
