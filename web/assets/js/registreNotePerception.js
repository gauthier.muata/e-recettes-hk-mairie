var inputResearchNP;

var selectContribuable,
        selectNP;

var
        btnRecherche,
        btnRechercherAvancer,
        btnFractionnerNP;

var cbxResearchType;

var
        codeResearch,
        messageEmptyValue;

var NpList, tempNPList;


var btnAdvencedSearch, btnImprimerRegistre;

var isAvancedSearch;

var tableNP;

var sumCDF = 0, sumUSD = 0, sumConvCDF;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;

$(function () {

    mainNavigationLabel.text('ORDONNANCEMENT');
    secondNavigationLabel.text('Registre des notes de perception');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    inputResearchNP = $('#inputSearchNP');
    cbxResearchType = $('#cbxResearchType');

    btnRecherche = $('#btnSearchNP');
    btnRechercherAvancer = $('#btnShowAdvancedSearchModal');
    btnImprimerRegistre = $('#btnImprimerRegistre');

    btnAdvencedSearch = $('#btnAdvencedSearch');

    tableNP = $('#tableNP');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    btnRecherche.click(function (e) {
        e.preventDefault();
        if (inputResearchNP.val() === empty || inputResearchNP.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadNotePerception(0);

    });

    btnImprimerRegistre.click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le rapport des ordonnanc&eacute;s ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempNPList.length != 0) {
                printDataNotePerception();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });


    cbxResearchType.on('change', function (e) {

        e.preventDefault();

        codeResearch = cbxResearchType.val();

        if (codeResearch === "1") {
            inputResearchNP.attr('placeholder', 'Le nom de l\'assujetti');
            inputResearchNP.val('');
        } else {
            inputResearchNP.attr('placeholder', 'Numero de la note de perception');
            inputResearchNP.val('');
        }

    });

    cmbSearchType = $('#ResearchType');
    codeResearch = cmbSearchType.val();
    messageEmptyValue = 'Veuillez saisir le nom de l\'assujetti.';

    btnRechercherAvancer.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadNotePerception(1);
    });

    btnAdvencedSearch.trigger('click');

    loadTableNP('');

});

function loadNotePerception(value) {

    isAvancedSearch = value;

    var viewAllSite = controlAccess('VIEW_ALL_SITES_NC');
    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');

    if (value === 1) {

        lblSite.html($('#selectSite option:selected').text().toUpperCase());
        lblService.html($('#selectService option:selected').text().toUpperCase());
        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());

        isAdvance.attr('style', 'display: block');
    } else {
        isAdvance.attr('style', 'display: none');
    }

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputResearchNP.val(),
            'typeSearch': cbxResearchType.val(),
            'isAdvancedSearch': isAvancedSearch,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'codeService': selectService.val(),
            'codeSite': selectSite.val(),
            'allSite': viewAllSite,
            'allService': viewAllService,
            'userId': userData.userId,
            'typeRegister': 'NP',
            'operation': 'loadNotePerception'
        },
        beforeSend: function () {

            if (isAvancedSearch == 1) {
                modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            } else {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
            }
        },
        success: function (response)
        {

            if (isAvancedSearch == 1) {
                modalRechercheAvanceeNC.unblock();
            } else {
                $.unblockUI();
            }

            if (response == '-1') {
                loadTableNP(empty);
                showResponseError();
                return;

            } else if (response == '0') {

                loadTableNP(empty);
                alertify.alert('Aucune note de perception dispnible');
                return;

            } else {

                setTimeout(function () {

                    NpList = null;
                    NpList = JSON.parse(JSON.stringify(response));
                    loadTableNP(NpList);
                    modalRechercheAvanceeNC.modal('hide');
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (isAvancedSearch == 1) {
                modalRechercheAvanceeNC.unblock();
            } else {
                $.unblockUI();
            }
            loadTableNP(empty);
            showResponseError();
        }
    });
}


function loadTableNP(result) {

    console.log(result);

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left"></th>\n\
                <th style="text-align:left">EXERCICE</th>\n\
                <th style="text-aLign:left">SERVICE ASSIETTE</th>\n\\n\
                <th style="text-aLign:left">NTD</th>\n\\n\n\
                <th style="text-aLign:left">ASSUJETTI</th>\n\\n\
                <th style="text-aLign:left">TYPE ASSUJETTI</th>\n\
                <th style="text-aLign:left">DATE CREATION</th>\n\
                <th style="text-aLign:left">DATE ECHEANCE</th>\n\
                <th style="text-aLign:left">NUMERO</th>\n\\n\
                <th style="text-aLign:left">ARTICLE BUGDETAIRE</th>\n\\n\
                <th style="text-align:right">MONTANT DÛ</th>\n\
                <th hidden="true">Nc</th>\n\
                <th hidden="true">Fraction</th>\n\
                <th></th></tr></thead>';
    var data = '';
    data += '<tbody id="bodyTable">';

    sumCDF = 0, sumUSD = 0, sumConvCDF = 0;
    tempNPList = [];

    for (var i = 0; i < result.length; i++) {

        var devise = result[i].DEVISE;
        var amount = result[i].MONTANTDU;
        var firstLineAB = '';

        if (devise === 'CDF') {
            sumCDF += amount;
        } else if (devise === 'USD') {
            sumUSD += amount;
        }

        var cdfConv = 0;
        var np = new Object();
        np.exerciceFiscal = result[i].exerciceFiscal;
        np.libelleService = result[i].libelleService;
        np.ASSUJETTI = result[i].ASSUJETTI;
        np.libelleFormeJuridique = result[i].libelleFormeJuridique;
        np.dateCreate = result[i].dateCreate;
        np.NUMERO = result[i].NUMERO;
        np.userName = result[i].userName;
        np.libelleArticleBudgetaire = result[i].libelleArticleBudgetaire;
        if (result[i].DEVISE == 'CDF') {
            np.montantPayerCDF = result[i].MONTANTDU;
            cdfConv += result[i].MONTANTDU;
            sumConvCDF += result[i].MONTANTDU;
        } else {
            np.montantPayerUSD = result[i].MONTANTDU;

            cdfConv += result[i].MONTANTDU * TAUX;
            sumConvCDF += result[i].MONTANTDU * TAUX;
        }
        np.montanCDFConv = cdfConv;

        tempNPList.push(np);

        if (result[i].libelleArticleBudgetaire.length > 150) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 150).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        data += '<tr>';
        data += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        data += '<td  style="text-align:center;width:5%;vertical-align:middle">' + result[i].exerciceFiscal + '</td>';
        data += '<td style="text-align:left;width:13%;vertical-align:middle">' + result[i].libelleService + '</td>';
        data += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].userName + '</td>';
        data += '<td style="text-align:left;width:11%;vertical-align:middle">' + result[i].ASSUJETTI + '</td>';
        data += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].libelleFormeJuridique + '</td>';
        data += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].dateCreate + '</td>';
        data += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].echeance + '</td>';
        data += '<td style="text-align:left;width:9%;vertical-align:middle">' + result[i].NUMERO + '</td>';
        data += '<td style="text-align:left;width:18%;vertical-align:middle; title="' + result[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        data += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(result[i].MONTANTDU, result[i].DEVISE) + '</td>';
        data += '<td hidden="true">' + result[i].NOTE_CALCUL + '</td>';
        data += '<td hidden="true">' + result[i].estFractionnee + '</td>';
        data += '<td style="text-align:center;width:5%;vertical-align:middle"><button onclick="initNPDetails(\'' + result[i].NUMERO + '\')" class="btn btn-primary"><i class="fa fa-print"></i></button></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="10" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableNP.html(TableContent);

    var datatable = tableNP.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 6,
        lengthMenu: [[7, 25, 50, -1], [7, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        lengthChange: false,
        select: {
            style: 'os',
            blurable: true
        },
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(10).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableNP tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = datatable.row(tr);
        var dataDetail = datatable.row(tr).data();
        var numeroTitre = dataDetail[8];
        var estFractionnee = dataDetail[12];

        var tableTitreDependant = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {


            if (estFractionnee === '1') {

                tableTitreDependant = '<center><h4>LES TITRES DES PERCEPTIONS DEPENDANTS AU TITRE N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
                tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TITRE DE PERCEPTION</td><td>TYPE TITRE DE PERCEPTION</td><td>DATE CREATION</td><td>ECHEANCE</td><td>MONTANT DÛ</td><td style="text-center:center">ETAT PAIEMENT</td><td></td style="text-center:center"></tr></thead>';

                var titreList = '';

                for (var j = 0; j < NpList.length; j++) {

                    if (NpList[j].NUMERO == numeroTitre) {

                        titreList = JSON.parse(NpList[j].npFilleList);

                        for (var i = 0; i < titreList.length; i++) {

                            var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';

                            if (titreList[i].isPaid === '1') {
                                etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                            }

                            var btn = '<button onclick="printNp(\'' + titreList[i].NUMERO + '\')" class="btn btn-warning"><i class="fa fa-print"></i></button>';

                            tableTitreDependant += '<tr>';
                            tableTitreDependant += '<td style="text-align:left;width:15%;">' + titreList[i].NUMERO + '</td>';
                            tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE_DOCUMENT + '</td>';
                            tableTitreDependant += '<td style="text-align:left;width:12%;">' + titreList[i].dateCreate + '</td>';
                            tableTitreDependant += '<td style="text-align:left;width:12%;">' + titreList[i].echeance + '</td>';
                            tableTitreDependant += '<td style="text-align:right;width:20%;">' + formatNumber(titreList[i].MONTANTDU, titreList[i].DEVISE) + '</td>';
                            tableTitreDependant += '<td style="text-center:center:width:15%;">' + etatPaiement + '</td>';
                            tableTitreDependant += '<td style="text-center:center:width:5%;">' + btn + '</td>';
                            tableTitreDependant += '</tr>';
                        }

                    }
                }

                tableTitreDependant += '<tbody>';

                tableTitreDependant += '</tbody>';
                row.child(tableTitreDependant).show();
                tr.addClass('shown');

            } else {

                tableTitreDependant = '<center><h4 style="color:red">CE TITRE DE PERCEPTION N\'A PAS DES NOTES PERCEPTION FILLES</h4><br/></center><table class="table table-bordered">';
                row.child(tableTitreDependant).show();
                tr.addClass('shown');
            }
        }

    });
}

function printNp(numero) {
    alertify.confirm('Etes-vous sûre de vouloir imprimer cette note de perception fille n° ' + numero + ' ?', function () {
        printNPFile(numero);
    });
}

function printNPFile(numero) {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'NUMERO': numero,
            'operation': 'printNpFille'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression de la note fille en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            }
            setTimeout(function () {

                $.unblockUI();
                setDocumentContent(response);

                setTimeout(function () {
                }, 2000);

                window.open('visualisation-document-np', '_blank');
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function printDataNotePerception() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;
    docTitle = '            ORDONNANCES\n';

    if (tempNPList.length > 0) {

        if (isAvancedSearch == '1') {

            if (selectSite.val() == '*' && selectService.val() == '*') {
                docTitle += '(Du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 350;
            } else if (selectSite.val() !== '*' && selectService.val() == '*') {
                docTitle += '(Du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 350;
                hauteur = 160;
            } else if (selectSite.val() == '*' && selectService.val() !== '*') {
                docTitle += '(Du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 350;
                hauteur = 160;
            } else {
                docTitle += '(Du ' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 350;
                hauteur = 170;
            }
        } else {
            var critere = inputResearchNP.val().toUpperCase();
            position = 400;
        }

    } else {
//        docTitle += 'REGISTRE DES NOTES DES PERCEPTIONS';
        position = 440;
    }

    var columns = [
        {title: "ACTES GENERATEURS", dataKey: "libelleArticleBudgetaire"},
        {title: "CDF", dataKey: "montantPayerCDF"},
        {title: "USD", dataKey: "montantPayerUSD"},
        {title: "CDF CONV.", dataKey: "montanCDFConv"}];

    var rows = tempNPList;

    var pageContent = function (data) {

        doc.setFontType("bold");
        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 73, 40);
        doc.setFontSize(10);
        doc.text("BUREAU DE " + $('#selectSite option:selected').text().toUpperCase().replace('SITE ', ''), 76, 55);
        doc.text(docTitle, position, 140);
        doc.addImage(docImage, 'PNG', 100, 60, 65, 65);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            libelleArticleBudgetaire: {columnWidth: 470, fontSize: 9, overflow: 'linebreak'},
            montantPayerCDF: {columnWidth: 100, overflow: 'linebreak', fontSize: 9},
            montantPayerUSD: {columnWidth: 100, overflow: 'linebreak', fontSize: 9},
            montanCDFConv: {columnWidth: 100, overflow: 'linebreak', fontSize: 9}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumConvCDF), 540, finalY + 40);
    window.open(doc.output('bloburl'), '_blank');
}

function printDataNotePerception2() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (tempNPList.length > 0) {

        if (isAvancedSearch == '1') {
            docTitle = 'ORDONNANCES';
            if (selectSite.val() == '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
            } else if (selectSite.val() !== '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (selectSite.val() == '*' && selectService.val() !== '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 170;
            }
        } else {
            var critere = inputResearchNP.val().toUpperCase();
            docTitle = 'REGISTRE DES NOTES DES PERCEPTIONS : ' + critere;
            position = 300;
        }

    } else {
        docTitle = 'REGISTRE DES NOTES DES PERCEPTIONS';
        position = 440;
    }

    var columns = [
        {title: "EXERCICE", dataKey: "exerciceFiscal"},
        {title: "SERVICE ASSIETTE", dataKey: "libelleService"},
        {title: "ASSUJETTI", dataKey: "ASSUJETTI"},
        {title: "TYPE ASSUJETTI", dataKey: "libelleFormeJuridique"},
        {title: "DATE CREATION", dataKey: "dateCreate"},
        {title: "NUMERO", dataKey: "NUMERO"},
        {title: "ARTICLE BUDGETAIRE", dataKey: "libelleArticleBudgetaire"},
        {title: "MONTANT DU", dataKey: "montantPayer"}];

    var rows = tempNPList;

    var pageContent = function (data) {

        doc.setFontType("bold");
        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text($('#selectSite option:selected').text().toUpperCase(), 40, 55);
        doc.text(docTitle, position, 140);
        doc.addImage(docImage, 'PNG', 50, 45, 75, 75);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            exerciceFiscal: {columnWidth: 70, fontSize: 8, overflow: 'linebreak'},
            libelleService: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            ASSUJETTI: {columnWidth: 150, overflow: 'linebreak', fontSize: 9},
            libelleFormeJuridique: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            dateCreate: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            NUMERO: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            libelleArticleBudgetaire: {columnWidth: 180, overflow: 'linebreak', fontSize: 9},
            montantPayer: {columnWidth: 90, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}


