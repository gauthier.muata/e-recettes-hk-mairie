/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnSimpleSearch, btnAdvencedSearch, btnShowAdvancedSerachModal;
var tableDE, tableDE2;
var ResearchValue, ResearchType;
var inputDateDebut, inputdateLast;
var ordonnancementEchelonnementModal;
var btnOrdonnancerEchelonnement, btnVoirDeclaration;
var textaeraObservation;
var selectSite, selectService;
var tableEchelonnement;
var typeSearch = '';
var dataArchive;

var modalDetailEchelonnement, tableDetailEchelonnement;
var lblNameAssujettiModalDetailEchelonnement;
var divInfoFicheCompte, divRadio;
var lblNameAssujetti, lblNotePerception, lblMontant, labelDepassementEcheance, labelValidationMontant; //labelEtatDemande
var btnValiderEchelonnement;
var tableFicheCompte;
var tempListNoteNP = [];
var tempListEchelonnement = [];
var INTERET_ECHELONNEMENT = 0.10;
var interetBonApayer = 0;
var deviseMontant = '';
var tempListEchelonnement = [];
var listEchelonnement = [];
var echelonnements = [];
var demandeEchelonnementCode = '', dateEcheanceDepassement = '';
var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblProvince, lblEntite, lblAgent;
var isAdvance;
var modalTitreManuelEchelonnement;
var tableEchelonnement;
var lblAssujettiEchelonnement, lblTitreEchelonne, lblAmountTitreEchelonne, txtObservalitionValidation, valueDecision, btnOrdonnancer;
var amount;
var rowNumber;
var titreCheckingList = [];
var referenceDocument, typeReferenceDocument;
var codeService, codeEntite, codeProvince, codeSite;

var dispatchId;

$(function () {

    mainNavigationLabel.text('FRACTIONNEMENT');
    secondNavigationLabel.text('Ordonnancement des fractionnements');
    ResearchValue = $('#ResearchValue');
    btnSimpleSearch = $('#btnSimpleSearch');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    tableDE = $('#tableDE');
    tableDE2 = $('#tableDE2');
    ResearchType = $('#ResearchType');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    ordonnancementEchelonnementModal = $('#ordonnancementEchelonnementModal');
    labelDepassementEcheance = $('#labelDepassementEcheance');
    labelValidationMontant = $('#labelValidationMontant');
    btnOrdonnancerEchelonnement = $('#btnOrdonnancerEchelonnement');
    btnVoirDeclaration = $('#btnVoirDeclaration');
    textaeraObservation = $('#textaeraObservation');
    lblProvince = $('#lblProvince');
    lblEntite = $('#lblEntite');
    tableEchelonnement = $('#tableEchelonnement');
    modalTitreManuelEchelonnement = $('#modalTitreManuelEchelonnement');
    divInfoFicheCompte = $('#divInfoFicheCompte');
    divRadio = $('#divRadio');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblNotePerception = $('#lblNotePerception');
    lblMontant = $('#lblMontant');
    tableFicheCompte = $('#tableFicheCompte');
    btnValiderEchelonnement = $('#btnValiderEchelonnement');
    modalDetailEchelonnement = $('#modalDetailEchelonnement');
    tableDetailEchelonnement = $('#tableDetailEchelonnement');
    lblNameAssujettiModalDetailEchelonnement = $('#lblNameAssujettiModalDetailEchelonnement');
    lblAssujettiEchelonnement = $('#lblAssujettiEchelonnement');
    lblTitreEchelonne = $('#lblTitreEchelonne');
    lblAmountTitreEchelonne = $('#lblAmountTitreEchelonne');
    txtObservalitionValidation = $('#txtObservalitionValidation');
    valueDecision = $('#valueDecision');
    btnOrdonnancer = $('#btnOrdonnancer');
    selectService = $('#selectService');
    selectSite = $('#selectSite');
    tableEchelonnement = $('#tableEchelonnement');
    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblAgent = $('#lblAgent');


    removeActiveMenu();
    linkMenuFractionnement.addClass('active');

//    if (!controlAccess('12003')) {
//        window.location = 'dashboard';
//    }

    ResearchType.on('change', function (e) {
        codeResearch = ResearchType.val();
        if (codeResearch === "1") {
            ResearchValue.attr('placeholder', 'Le nom de l\'assujetti');
            ResearchValue.val('');
        } else if (codeResearch === "2") {
            ResearchValue.attr('placeholder', 'Numero de la note de perception');
            ResearchValue.val('');
        } else {
            ResearchValue.attr('placeholder', 'Référence de la lettre');
            ResearchValue.val('');
        }
    });

    btnSimpleSearch.click(function (e) {
        e.preventDefault();
        typeSearch = 'simple';
        isAdvance.attr('style', 'display: none');
        getCurrentSearchParam(0);
        getRegistreDemandeEchelonnement('1');

    });

    btnShowAdvancedSerachModal.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnOrdonnancer.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (controlFields()) {

            ordonnancer();
        }

    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('hide');
        typeSearch = 'advanced';
        isAdvance.attr('style', 'display: block');
        getCurrentSearchParam(1);
        getRegistreDemandeEchelonnement('2');
    });

    setTimeout(function () {
        btnAdvencedSearch.trigger('click');
    }, 1000);

    loadTableDemandeEchelonnement('');
});

function ordonnancer() {

    alertify.confirm('Etes-vous sûre de vouloir ordonnancer ce fractionnement ?', function () {

        $.ajax({
            type: 'POST',
            url: 'fractionnement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'operation': '1204',
                'numeroTitre': referenceDocument,
                'demandeEchelonnementCode': demandeEchelonnementCode,
                'typeTitre': typeReferenceDocument,
                'prefixProvince': '', //provincePrefix,
                'SiteCode': userData.SiteCode,
                'idDispatch': '', //dispatchId,
                'titreEchelonnement': ''//echelonnements != empty ? JSON.stringify(echelonnements) : empty
            },
            beforeSend: function () {

                modalTitreManuelEchelonnement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ordonnancement en cours ...</h5>'});
            },
            success: function (response)
            {
                if (response == '-1') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    modalTitreManuelEchelonnement.unblock();
                    if (response == '0') {
                        alertify.alert('Echec lors de l\'ordonnancement du fractionnement!');
                    } else {
                        alertify.alert('Ordonnancement effectué avec succès');
                        demandeEchelonnementCode = '';
                        modalDetailEchelonnement.modal('hide');
                        modalTitreManuelEchelonnement.modal('hide');
                        if (typeSearch == 'simple') {
                            getRegistreDemandeEchelonnement('1');
                        } else {
                            getRegistreDemandeEchelonnement('2');
                        }
                    }
                }
                , 1);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalTitreManuelEchelonnement.unblock();
                showResponseError();
            }
        });

    });
}


function getRegistreDemandeEchelonnement(isAdvancedSearch) {

    if (isAdvancedSearch == '2') {

        //        try {

        var service = advancedSearchParam.serviceLibelle;
        var site = advancedSearchParam.siteLibelle;
        var entite = advancedSearchParam.entiteLibelle;
        var province = advancedSearchParam.provinceLibelle;
        var agent = advancedSearchParam.agentName;

        lblAgent.text(getsearchbarText(1, advancedSearchParam.agentName));
        lblAgent.attr('title', agent);

        lblDateDebut.text(advancedSearchParam.dateDebut);
        lblDateFin.text(advancedSearchParam.dateFin);

//        } catch (err) {
//            location.reload(true);
//        }

//        try {
//            var service = advancedSearchParam.serviceLibelle;
//            var site = advancedSearchParam.siteLibelle;
//            var entite = advancedSearchParam.entiteLibelle;
//            var province = advancedSearchParam.provinceLibelle;
//            var agent = advancedSearchParam.agentName;
//
//            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
//            lblProvince.attr('title', province);
//
//            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
//            lblEntite.attr('title', entite);
//
//            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
//            lblService.attr('title', service);
//
//            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
//            lblSite.attr('title', site);
//
//            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
//            lblAgent.attr('title', agent);
//
//            lblDateDebut.text(advancedSearchParam.dateDebut);
//            lblDateFin.text(advancedSearchParam.dateFin);

//        } catch (err) {
//            location.reload(true);
//        }
    }

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    var
            serviceCode,
            siteCode,
            ministereCode;
    if (isAdvancedSearch == '1') {

        serviceCode = userData.serviceCode;
        siteCode = userData.SiteCode;
        ministereCode = userData.ministereCode;
    } else {
        serviceCode = selectService.val();
        siteCode = selectSite.val();
//        ministereCode = selectMinistere.val();
    }

    codeResearch = ResearchType.val();
    operation = "getListDemandeEchelonnement";
    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': codeResearch,
            'valueSearch': (ResearchValue.val()).trim(),
            'operation': operation,
            'modeSearch': isAdvancedSearch,
            'codeSite': siteCode, //JSON.stringify(advancedSearchParam.site),
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'stateCondition': ' = 1 ',
            'codeMinistere': ministereCode,
            'typeRegistre': '2'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                loadTableDemandeEchelonnement('');
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadTableDemandeEchelonnement('');
                return;
            }

            setTimeout(function () {
                result = null;
                result = JSON.parse(JSON.stringify(response));
                tempListEchelonnement = result;
                loadTableDemandeEchelonnement(result);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function loadTableDemandeEchelonnement(result) {

    tempEchelonnementList = result;
    var header = '';
    header += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    header += '<tr>';
    header += '<th>N° REF LETTRE</th>';
    header += '<th>DATE DE LA DEMANDE</th>';
    header += '<th>ASSUJETTI</th>';
    header += '<th>AGENT CREATION</th>';
    header += '<th>OBSERVATION</th>';
    header += '<th></th>';
    header += '</tr>';
    header += '</thead>';
    var data = '';
    data += '<tbody id="bodyTable">';
    for (var i = 0; i < result.length; i++) {

        var firstLineObservation = '';
        if (result[i].observation.length > 120) {
            firstLineObservation = result[i].observation.substring(0, 120) + ' ...';
        } else {
            firstLineObservation = result[i].observation;
        }

        data += '<tr>';
        data += '<td style="text-align:left;width:10%">' + result[i].letterReference + '</td>';
        data += '<td style="text-align:left;width:10%">' + result[i].dateCreateDe + '</td>';
        data += '<td style="text-align:left;width:20%;" title="' + result[i].assujettiName + '"><span style="font-weight:bold;">' + result[i].assujettiName + '</span></td>';
        data += '<td style="text-align:left;width:5%">' + result[i].agentCreate + '</td>';
        data += '<td style="vertical-align:middle;text-align:left;width:20%;" title="' + result[i].observation + '"">' + firstLineObservation + '</td>';
        data += '<td style="text-align:center;width:2%"><button type="button" class="btn btn-warning" onclick="showDetailEchelonnementModal(\'' + result[i].demandeEchelonnementCode + '\')"><i class="fa fa-list"></i></button></td>';
        data += '</tr>';
    }
    data += '</tbody>';
    var TableContent = header + data;
    tableDE.html(TableContent);
    tableDE.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthChange: false,
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
        }
    });
}
function showDetailEchelonnementModal(id) {


    for (i = 0; i < tempEchelonnementList.length; i++) {

        if (id == tempEchelonnementList[i].demandeEchelonnementCode) {

            lblNameAssujettiModalDetailEchelonnement.html(tempEchelonnementList[i].assujettiName);
            lblAssujettiEchelonnement.html(tempEchelonnementList[i].assujettiName);
            txtObservalitionValidation.html(tempEchelonnementList[i].observation);
            valueDecision.html("APPROUVER");
            echelonnementDocumentAttach = tempEchelonnementList[i].declarationDocumentList;
            modalDetailEchelonnement.modal('show');
            demandeEchelonnementCode = id;
            dateLimiteEcheanceRevocationByIntercalaire = '';
            printTitreList(JSON.parse(tempEchelonnementList[i].detailEchelonnement), id);
            break;
        }
    }
}

var tempTitreList = [];
function printTitreList(titreList, codeDemandeEchelonnement) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    tempTitreList = titreList;
    header += '<th scope="col"> </th>';
    header += '<th scope="col"> TYPE TITRE </th>';
    header += '<th scope="col"> NUM. TITRE </th>';
    header += '<th scope="col"> SECTEUR </th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th scope="col"> ETAT </th>';
    header += '<th></th>';
    header += '</tr></thead>';
    var body = '<tbody>';
    for (var i = 0; i < titreList.length; i++) {

        var firstLineAB = '';
        var amountTitre = formatNumber(titreList[i].MONTANTDU, titreList[i].DEVISE);
        if (titreList[i].libelleArticleBudgetaire.length > 75) {
            firstLineAB = titreList[i].libelleArticleBudgetaire.substring(0, 75) + ' ...';
        } else {
            firstLineAB = titreList[i].libelleArticleBudgetaire;
        }

        codeService = titreList[i].codeService;
        codeEntite = titreList[i].codeEntite;
        codeProvince = titreList[i].provinceCode;
        codeSite = titreList[i].codeSite;

        var etat = '';
        if (titreList[i].etat == 4) {
            etat = 'ORDONNANCE';
        } else {
            etat = 'VALIDE';
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="width:7%">' + getTypeDocumentName(titreList[i].TYPE_DOCUMENT) + '</td>';
        body += '<td style="width:7%">' + titreList[i].referenceDocumentManuel + '</td>';
        body += '<td style="width:20%">' + titreList[i].SERVICE + '</td>';
        body += '<td style="text-align:left;width:30%;" title="' + titreList[i].libelleArticleBudgetaire + '"><span style="font-weight:bold;">' + titreList[i].codeBudgetaire + '</span>/ ' + firstLineAB + '</td>';
        body += '<td style="width:10%;text-align:right">' + amountTitre + ' </td>';
        body += '<td style="width:10%;">' + etat.toUpperCase() + '</td>';
        if (titreList[i].etat !== 4) {
            body += '<td style="text-align:center;width:8%"><a title="Traiter" onclick="ordonnancerEchelonnement(\'' + titreList[i].referenceDocument + '\',\'' + titreList[i].TYPE_DOCUMENT + '\',\'' + amountTitre + '\',\'' + titreList[i].referenceDocumentManuel + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>';
        } else {
            body += '<td style="text-align:center;width:8%"></td>';
        }

        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableDetailEchelonnement.html(tableContent);
    var tableDetail = tableDetailEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        scroller: true,
        scrollCollapse: true,
        pageLength: 2,
        datalength: 2
    });
    $('#tableDetailEchelonnement tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = tableDetail.row(tr);
        var dataDetail = tableDetail.row(tr).data();
        var numeroTitre = dataDetail[2];
        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        }
        else {

            var detailTable = '<center><h4>LES NOTES DE PERCEPTION FRACTIONNEES </h4><br/></center><table class="table table-bordered">';
            detailTable += '<thead><tr style="background-color:#e6ceac;color:black"><td style="text-align:center">NUMERO TITRE</td><td style="text-align:center">ECHEANCE</td><td style="text-align:center">MONTANT DU</td></tr></thead>';
            for (var i = 0; i < tempTitreList.length; i++) {

                if (tempTitreList[i].referenceDocumentManuel == numeroTitre) {

                    if (tempTitreList[i].etat == '2' || tempTitreList[i].etat == '0') {

                        row.child('<center><h5>Aucun titre intercalaire.</h5></center>').show();
                        tr.addClass('shown');
                        return;
                    }

                    var listIntercalaireDetail = JSON.parse(tempTitreList[i].listIntercalaireDetail);
                    var etat = tempTitreList[i].etat;
                    detailTable += '<tbody>';
                    for (var j = 0; j < listIntercalaireDetail.length; j++) {

                        if (listIntercalaireDetail[j].IS_INTERET)
                            continue;
                        var colorEcheance = 'black';
                        if (listIntercalaireDetail[j].isEchanceEchus && !listIntercalaireDetail[j].isPaid && etat != '3') {
                            colorEcheance = 'red';
                        }

                        detailTable += '<tr>';
                        detailTable += '<td style="text-align:center;width:30%;">' + listIntercalaireDetail[j].NUMERO_MANUEL + '</td>';
                        detailTable += '<td style="text-align:center;width:20%;color:' + colorEcheance + '""> ' + listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT + ' </td>';
                        detailTable += '<td style="text-align:right;width:20%;">' + formatNumber(listIntercalaireDetail[j].MONTANTDU, listIntercalaireDetail[j].DEVISE) + '</td>';
                        detailTable += '</tr>';
                    }

                    detailTable += '</tbody></table>';

                    var detailTableInteret = '<center><h4>LES INTERETS DU FRACTIONNEMENT </h4><br/></center><table class="table table-bordered">';

                    detailTableInteret += '<thead><tr style="background-color:#e6ceac;color:black"><td style="text-align:center">NUMERO TITRE</td><td style="text-align:center">ECHEANCE</td><td style="text-align:center">MONTANT DU</td></tr></thead>';

                    for (var j = 0; j < listIntercalaireDetail.length; j++) {

                        if (!listIntercalaireDetail[j].IS_INTERET)
                            continue;
                        var colorEcheance = 'black';
                        if (listIntercalaireDetail[j].isEchanceEchus && !listIntercalaireDetail[j].isPaid && etat != '3') {
                            colorEcheance = 'red';
                        }

                        detailTableInteret += '<tr>';
                        detailTableInteret += '<td style="text-align:center;width:30%;">' + listIntercalaireDetail[j].NUMERO_MANUEL + '</td>';
                        detailTableInteret += '<td style="text-align:center;width:20%;color:' + colorEcheance + '""> ' + listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT + ' </td>';
                        detailTableInteret += '<td style="text-align:right;width:20%;">' + formatNumber(listIntercalaireDetail[j].MONTANTDU, listIntercalaireDetail[j].DEVISE) + '</td>';
                        detailTableInteret += '</tr>';
                    }

                    detailTableInteret += '</tbody>';
                    //row.child(detailTable + detailTableInteret).show();
                    row.child(detailTable).show();
                    tr.addClass('shown');
                    break;
                }

            }
        }

    });
}

function rejeterDemandeEchelonnement(codeLettre, etatValidation) {

    ordonnancementEchelonnementModal.modal('hide');
    $.ajax({
        type: 'POST',
        url: 'echelonnement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'rejeterDemande',
            'codeLettre': codeLettre,
            'etat': etatValidation,
            'observation': textaeraObservation.val(),
            'userId': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Validation en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response === '-1') {
                showResponseError();
                return;
            } else if (response === '0') {
                showResponseError();
                return;
            } else if (response === '1') {

                setTimeout(function () {

                    var message;
                    switch (etatValidation) {
                        case 1:
                            message = 'La demande de fractionnement a été approuvée avec sucess.';
                            break;
                        case 0:
                            message = 'La demande de fractionnement a été rejetée avec sucess.';
                    }

                    alertify.alert(message);
                    if (typeSearch = 'simple') {
                        getRegistreDemandeEchelonnement('1');
                    } else {
                        getRegistreDemandeEchelonnement('2');
                    }
                }
                , 1);
            } else {
                showResponseError();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function ordonnancerEchelonnement(reference, typeTitre, amount, referenceDocumentManuel) {

    referenceDocument = reference;

    typeReferenceDocument = typeTitre;

    ordonnancer();

//    referenceDocument = reference;
//    typeReferenceDocument = typeTitre;
//    var valueTitre = '';
//    if (typeTitre == 'NP') {
//        valueTitre = referenceDocumentManuel + ' (NOTE DE PERCEPTION)';
//    } else if (typeTitre == 'BAP') {
//        valueTitre = referenceDocumentManuel + ' (BON A PAYER)';
//    }
//
//    lblTitreEchelonne.html(valueTitre);
//    lblAmountTitreEchelonne.html(amount);
//    for (var i = 0; i < tempListEchelonnement.length; i++) {
//
//        var list = JSON.parse(tempListEchelonnement[i].titreEchelonnementForOrdonnancement);
//        var titreEchelonnementList = [];
//        for (var j = 0; j < list.length; j++) {
//            if (list[j].NP_MERE = referenceDocument) {
//                titreEchelonnementList.push(list[j]);
//            }
//        }
//
//        if (titreEchelonnementList.length > 0) {
//            rowNumber = titreEchelonnementList.length;
//            listEchelonnement = titreEchelonnementList;
//            printTitreEchelonnement(titreEchelonnementList);
//            modalTitreManuelEchelonnement.modal('show');
//        }
//
//    }
}

function printTitreEchelonnement(result) {

    var header = '';
    header += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    header += '<tr>';
    header += '<th style="text-align:left">Tranche</th>';
    header += '<th style="text-align:right">Montant de la tranche</th>';
    header += '<th style="text-align:left">Echéance paiement</th>';
    header += '<th style="text-align:center">Numéro du titre de perception</th>';
    header += '</tr>';
    header += '</thead>';
    var data = '';
    data += '<tbody>';
    for (var i = 0; i < result.length; i++) {

        var inputNumber = "inputNumber_" + i;
        var inputDate = "inputDate_" + i;
        var inputTitreManuel = "inputTitreManuel_" + i;
        var counter = i + 1;
        var valueTrache = '';
        var color = '';
        if (result[i].IS_INTERET) {
            valueTrache = 'INTERET';
            color = 'text-align:left;color:red';
        } else {
            valueTrache = 'TRANCHE (' + counter + ')';
            color = 'text-align:left';
        }

        var provincePrefix = '';

        data += '<tr>';
        data += '<td style="' + color + '">' + valueTrache + '</input></td>';
        data += '<td><input id="' + inputNumber + '" style="width : 100%;text-align:right" readOnly="true" value="' + formatNumber(result[i].MONTANTDU, result[i].DEVISE) + '"></input></td>';
        data += '<td><input id="' + inputDate + '" style="width : 100%" readOnly="true" value="' + result[i].DATE_ECHEANCE_PAIEMENT + '"></input></td>';
        data += '<td><center><span style="font-weight: bold;text-align:center">' + provincePrefix + '&nbsp;&nbsp;</span><input id="' + inputTitreManuel + '" onchange="checkValidateTitre(' + inputTitreManuel + ')" type="number" style="width : 70%""></input></center></td>';
        data += '</tr>';
    }
    data += '</tbody>';
    var TableContent = header + data;
    tableEchelonnement.html(TableContent);
    tableEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune tranche",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 6,
        datalength: 3
    });
}
function controlFields() {

    var result = true;
    echelonnements = [];

    var lastNum = '';

    for (var i = 0; i < rowNumber; i++) {

        var idTitreManuel = "#inputTitreManuel_" + i;

        tableEchelonnement.find(idTitreManuel).attr('style', 'color:black;width:70%');

        var valueTitreManuel = $('#tableEchelonnement').find(idTitreManuel).val();

        if (valueTitreManuel == '') {
            alertify.alert('Veuillez renseigner le numéro manuel du titre de perception');
            //   tableEchelonnement.find(idTitreManuel).attr('style', 'color:red;width:70%');
            result = false;
            break;
        }

        if (lastNum == valueTitreManuel) {
            alertify.alert('Ce numéro : ' + lastNum + ' de note de perception fractionnée est déjà renseignée');
            //    tableEchelonnement.find(idTitreManuel).attr('style', 'color:red;width:70%');
            result = false;
            break;
        }

        lastNum = valueTitreManuel;
    }

    if (listEchelonnement.length > 0 && result) {

        for (var j = 0; j < listEchelonnement.length; j++) {

            var idTitreM = "#inputTitreManuel_" + j;
            var valueTitreManuel = tableEchelonnement.find(idTitreM).val();
            var echelonnement = new Object();
            echelonnement.numeroTitreManuel = valueTitreManuel;
            echelonnement.numeroTitre = listEchelonnement[j].NUMERO;
            echelonnements.push(echelonnement);
        }

        if (echelonnements.length == 0) {
            result = false;
        }
    }
    return result;
}

function checkValidateTitre(titreManuel) {

    titreObj = {};

    var valueTitreChecking = $(titreManuel).val();

    var valueTitreComposite = provincePrefix + '' + $(titreManuel).val();

    titreObj.titreChecking = $(titreManuel).val();

    codeEntite = userData.idUser;

    for (var j = 0; j < titreCheckingList.length; j++) {

        if (titreCheckingList[j].titreChecking === valueTitreChecking) {
            alertify.alert('Ce numéro : ' + valueTitreComposite + ' de note de perception intercalaire est déjà renseigner');
            $(titreManuel).val('');
            return;
        }

    }

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'valueTitrePrincipalChecking': valueTitreChecking,
            'valueTitrePrincipalComposite': valueTitreComposite,
            'withPenality': 0,
            'serviceCode': codeService,
            'codeEntite': codeEntite,
            'SiteCode': codeSite,
            'ordonnancementNormal': 0,
            'provinceCode': codeProvince,
            'operation': 'checkingTitreManuel'
        },
        beforeSend: function () {
            modalTitreManuelEchelonnement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification du numéro en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalTitreManuelEchelonnement.unblock();
                responseChecking = response;

                switch (responseChecking) {
                    case '2':
                        alertify.alert('Ce numéro de note de perception principale est déjà utilisé');
                        $(titreManuel).val('');
                        break;
                    case '3':
                        alertify.alert('Ce numéro de note de perception n\'existe dans votre plage d\'affectation.');
                        $(titreManuel).val('');
                        break;
                    case '4':
                        alertify.alert('Ce numéro de note de perception de pénalité est déjà utilisé');
                        $(titreManuel).val('');
                        break;
                    default:
                        dispatchId = responseChecking;
                        titreCheckingList.push(titreObj);
                        alertify.alert('OK');
                        break;
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalTitreManuelEchelonnement.unblock();
            showResponseError();
        }
    });
}

function getTypeDocumentName(type) {
    var nameDocument = '';
    switch (type) {
        case 'NP':
            nameDocument = 'Note de perception';
            break;
        case 'BP':
            nameDocument = 'Bon à payer';
            break;
        case 'AMR1':
            nameDocument = 'Avis de mise en recouvrement 1';
            break;
        default:
            nameDocument = 'Avis de mise en recouvrement 2';
    }
    return nameDocument;
}