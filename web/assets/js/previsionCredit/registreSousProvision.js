/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lblTypeAssujet, lblTypeAss, lblLegalFormPrevisionCredit;

var lblNomAssujet, lblNameAssujet;

var lblArticleBudg, cmbArticleBudgetaire;

var lblAddresseAssujet, lblAdress;

var codePrePaiementettis, codeTypeAssujetti, codeAdresse;

var codeArticleBudg, codeTypePrevision, codeDevise;

var nbrToure, inputMontantPercu;

var cmbTypePrevisionCredit, cmbDevise;

var btnSearchAssujettis, btnValiderPrevisionCredit;

var tableSousProvision, ModalAssujettiPrevisionCredit;

var dataDetailsPrevisionCredit, tempBienList;

var prepaiementList;

var detailPrepaiementListTampo = [], detailPrepaiementList = [];

var modalDetailProvisionCredit, spnBienSelected, spnTarifBienSelected, selectTypePaiement, inputNbreTour, inputTotalAPayer, selectDevise, btnSaveDetailProvisionCredit;
var codeSelected, tauxSelected, idSelected, detailAssujIdSeleted, prePaiementCodeSeleted, detailPrePaiementCodeSeleted, tarifCodeSelected;

var modaTraitementlDetailProvisionCredit, selectTypeTraitement, divTableTraitementPrevisionCredit, btnTraiterDetailProvisionCredit, selectBiens;

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre des sous-provision (Pré-paiements)');

    removeActiveMenu();
    linkSubMenuRegistreSousProvision.addClass('active');

    btnSearchAssujettis = $('#btnSearchAssujettis');
    btnValiderPrevisionCredit = $('#btnValiderPrevisionCredit');

    tableSousProvision = $('#tableSousProvision');
    ModalAssujettiPrevisionCredit = $('#ModalAssujettiPrevisionCredit');

    cmbTypePrevisionCredit = $('#cmbTypePrevisionCredit');
    cmbDevise = $('#cmbDevise');

    modalDetailProvisionCredit = $('#modalDetailProvisionCredit');
    spnBienSelected = $('#spnBienSelected');
    spnTarifBienSelected = $('#spnTarifBienSelected');
    selectTypePaiement = $('#selectTypePaiement');
    inputNbreTour = $('#inputNbreTour');
    inputTotalAPayer = $('#inputTotalAPayer');
    selectDevise = $('#selectDevise');
    btnSaveDetailProvisionCredit = $('#btnSaveDetailProvisionCredit');

    modaTraitementlDetailProvisionCredit = $('#modaTraitementlDetailProvisionCredit');
    selectTypeTraitement = $('#selectTypeTraitement');
    divTableTraitementPrevisionCredit = $('#divTableTraitementPrevisionCredit');
    btnTraiterDetailProvisionCredit = $('#btnTraiterDetailProvisionCredit');
    selectBiens = $('#selectBiens');


    selectTypeTraitement.on('change', function () {

        if (selectTypeTraitement.val() == '2') {
            loadBiensRemplacement();
        } else if (selectTypeTraitement.val() == '1') {
            divTableTraitementPrevisionCredit.attr('style', 'display:none');
        }

    });

    lblLegalFormPrevisionCredit = $('#lblLegalFormPrevisionCredit');

    lblTypeAssujet = $('#lblTypeAssujet');
    lblTypeAss = $('#lblTypeAss');

    cmbArticleBudgetaire = $('#cmbArticleBudgetaire');

    inputMontantPercu = $('#inputMontantPercu');
    nbrToure = $('#nbrToure');

    lblAddresseAssujet = $('#lblAddresseAssujet');
    lblAdress = $('#lblAdress');

    lblNameAssujet = $('#lblNameAssujet');
    lblNomAssujet = $('#lblNomAssujet');

    btnTraiterDetailProvisionCredit.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectTypeTraitement.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de traitement valide');
            return;

        } else if (selectTypeTraitement.val() == '1') {

            alertify.confirm('Etes-vous de vouloir suspendre ce bien dans cette sous-provision ?', function () {
                suspendreBienPrevisionCredit();
            });

        } else {

            if (selectBiens.val() == '0') {

                alertify.alert('Veuillez d\'abord sélectionner le bien de remplacement');
                return;
            } else {

                alertify.confirm('Etes-vous de vouloir remplacer ce bien dans cette sous-provision ?', function () {
                    remplacerBienPrevisionCredit();
                });

            }

        }
    });

    btnSearchAssujettis.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        ModalAssujettiPrevisionCredit.modal('show');
    });

    btnSaveDetailProvisionCredit.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkFields();
    });

    btnValiderPrevisionCredit.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var typePaidExist = false;

        for (var i = 0; i < detailPrepaiementListTampo.length; i++) {

            if (detailPrepaiementListTampo[i].typePaidExist == "1") {
                typePaidExist = true;
            }
        }

        if (typePaidExist === false) {

            alertify.alert('Veuillez d\'abord définir le type de paiement pour au moins un bien avant de valder cette opération');
            return;
        }

        alertify.confirm('Etes-vous de vouloir valider cette opération ?', function () {
            savePrevisionCredit();
        });

    });

    printPrepaiement(empty);

});


function printDetailsPrevisionCredit(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:20%"> BIEN  (AUTOMOBILE)</th>';
    header += '<th scope="col" style="width:30%"> DESCRIPTION DU BIEN </th>';
    header += '<th scope="col" style="width:30%"> ADRESSE </th>';
    header += '<th scope="col" style="width:10%;text-align:right"> TAUX </th>';
    header += '<th ></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        body += '<tr>';
        body += '<td>' + bienList[i].intituleBien + '</td>';
        body += '<td>' + bienList[i].descriptionBien + '</td>';
        body += '<td>' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="text-align:right;font-weight:bold">' + formatNumber(bienList[i].valeur, bienList[i].devise) + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxSelectBienPrevCred' + bienList[i].codebien + '" name="checkBoxSelectBienPrevCred' + bienList[i].codebien + '"></center></td>';
        body += '<td hidden="true">' + bienList[i].codebien + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableSousProvision.html(tableContent);
    tableSousProvision.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function printPrepaiement(result) {

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left"></th>\n\
                <th style="text-align:left">CODE</th>\n\
                <th style="text-aLign:left">DATE CREATION</th>\n\\n\\n\
                <th style="text-aLign:left">SITE D\'EXPLOITATION</th>\n\\n\
                <th style="text-aLign:center">ETAT</th>\n\
                <th style="text-aLign:center"></th>\n\
                <th hidden="true">ETAT_ID</th>\n\
                </tr></thead>';
    var data = '';
    data += '<tbody id="bodyTable">';


    for (var i = 0; i < result.length; i++) {

        var state = '';
        var color1 = '';

        switch (result[i].prePaiementState) {
            case 1:
                state = 'ACTIVE';
                color1 = 'green';
                break;
            case 2:
                state = 'EN ATTENTE DE VALIDATION';
                color1 = 'orange';
                break;
            case 3:
                state = 'SUSPENDUE';
                color1 = 'red';
                break;

        }

        var btn = empty;

        if (controlAccess('EDIT_SOUS_PROVISION')) {
            btn = '<button type="button" onclick="displayModalTraitementPP(\'' + result[i].prePaiementCode + '\',\'' + result[i].prePaiementState + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></button>';
        }

        var siteName = 'Site : ' + '<span style="font-weight:bold">' + result[i].prePaiementSiteName + '</span>'
        var adresseSite = 'Lieu : ' + '<span style="font-weight:bold">' + result[i].prePaiementAdresseSite + '</span>'

        var siteInfo = siteName + '<hr/>' + adresseSite;

        data += '<tr>';
        data += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        data += '<td  style="text-align:left;width:10%;vertical-align:middle">' + result[i].prePaiementCode + '</td>';
        data += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].prePaiementDate + '</td>';
        data += '<td style="text-align:left;width:25%;vertical-align:middle">' + siteInfo + '</td>';
        data += '<td style="text-align:center;width:15%;vertical-align:middle;font-weight:bold;color:' + color1 + '">' + state + '</td>';
        data += '<td style="text-align:center;width:5%;vertical-align:middle">' + btn + '</td>';
        data += '<td hidden="true"></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    var TableContent = header + data;

    tableSousProvision.html(TableContent);

    var datatable = tableSousProvision.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 6,
        lengthMenu: [[7, 25, 50, -1], [7, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 20,
        lengthChange: false,
        select: {
            style: 'os',
            blurable: true
        }
    });

    $('#tableSousProvision tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = datatable.row(tr);
        var dataDetail = datatable.row(tr).data();
        var codePrePaiement = dataDetail[1];
        var stateID;

        var tableDetailPrePaiement = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {

            tableDetailPrePaiement = '<center><h4>LES DETAILS DE LA SOUS-PROVISION N° : ' + codePrePaiement + '</h4><br/></center><table class="table table-bordered">';
            tableDetailPrePaiement += '<thead><tr style="background-color:#e6ceac;color:black"><td hidden="true">BIENID</td><td>BIEN (AUTOMOBILE)</td><td>DESCRIPTION</td><td>TARIF</td><td>INFORMATIONS SOUS-PROVISIONS</td><td style="text-align:center"></td></tr></thead>';

            for (var j = 0; j < prepaiementList.length; j++) {

                if (prepaiementList[j].prePaiementCode == codePrePaiement) {

                    stateID = prepaiementList[j].prePaiementState;

                    detailPrepaiementList = JSON.parse(prepaiementList[j].detailPrePaiementJsonList);

                    for (var i = 0; i < detailPrepaiementList.length; i++) {

                        var obj = {};

                        obj.bienId = detailPrepaiementList[i].bienCode;
                        obj.taux = detailPrepaiementList[i].taux;
                        obj.devise = detailPrepaiementList[i].devise;
                        obj.numberTour = detailPrepaiementList[i].numberTourSoumission;
                        obj.totalPaid = detailPrepaiementList[i].totalTaux;
                        obj.numberTourUsing = detailPrepaiementList[i].numberTourUsing;
                        obj.type = 1;
                        obj.detailAssujettissementId = detailPrepaiementList[i].detailAssujId;
                        obj.bienPlaque = detailPrepaiementList[i].bienPlaque;
                        obj.id = detailPrepaiementList[i].detailPrePaiementId;
                        obj.prePaiementCode = detailPrepaiementList[i].prePaiementCode;

                        detailPrepaiementListTampo.push(obj);

                        var bienName = detailPrepaiementList[i].bienName + ' (' + detailPrepaiementList[i].typeBien + ')';
                        var plaqueInfo = 'Numéro plaque ou chassis: ' + '<span style="font-weight:bold">' + detailPrepaiementList[i].bienPlaque + '</span>'
                        var bienNameInfo = bienName + '<hr/>' + plaqueInfo;

                        var amount = formatNumber(detailPrepaiementList[i].taux, detailPrepaiementList[i].devise);

                        var btn = '<button type="button" onclick="displayModalTypePayment(\'' + detailPrepaiementList[i].bienCode + '\',\'' + bienName + '\',\'' + amount + '\',\'' + detailPrepaiementList[i].tarifName + '\',\'' + detailPrepaiementList[i].devise + '\',\'' + detailPrepaiementList[i].taux + '\',\'' + detailPrepaiementList[i].detailPrePaiementId + '\',\'' + detailPrepaiementList[i].totalTaux + '\',\'' + detailPrepaiementList[i].numberTourSoumission + '\',\'' + detailPrepaiementList[i].detailAssujId + '\',\'' + detailPrepaiementList[i].prePaiementCode + '\')" class="btn btn-warning"><i class="fa fa-list"></i></button>';
                        var btn2 = '<button type="button" onclick="displayTraiementDP(\'' + detailPrepaiementList[i].detailPrePaiementId + '\',\'' + detailPrepaiementList[i].bienCode + '\',\'' + detailPrepaiementList[i].tarifCode + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></button>';
                        var btn4 = '<button type="button" onclick="activateDetailProvisionCredt(\'' + detailPrepaiementList[i].detailPrePaiementId + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></button>';

                        /*if (controlAccess('EDIT_SOUS_PROVISION')) {
                            btn2 = empty;
                            btn4 = empty;
                        }*/

                        /*if (controlAccess('RENOUVELLER_SOUS_PROVISION')) {
                            btn = empty;
                        }*/

                        var btn3 = btn + ' ' + btn2;

                        if (stateID == '2' || stateID == '3') {
                            btn3 = '';
                        }

                        if (detailPrepaiementList[i].detailPrePaiementState == 2) {
                            btn3 = '';
                        } else if (detailPrepaiementList[i].detailPrePaiementState == 3) {

                            if (stateID == '3') {
                                btn3 = empty;
                            } else {
                                if (detailPrepaiementList[i].numberTourSoumission > detailPrepaiementList[i].numberTourUsing) {
                                    btn3 = btn4;
                                } else {
                                    btn3 = empty;
                                }
                            }
                        } else if (detailPrepaiementList[i].detailPrePaiementState == 4) {
                            btn3 = btn;
                        }

                        var stateDet = '';
                        var color = '';

                        switch (detailPrepaiementList[i].detailPrePaiementState) {
                            case 1:
                                stateDet = 'EN COURS D\'EXPLOITATION';
                                color = 'green';
                                break;
                            case 2:
                                stateDet = 'EN ATTENTE D\'EXPLOITATION';
                                color = 'orange';
                                break;
                            case 3:
                                stateDet = 'SUSPENDUE';
                                color = 'red';
                                break;
                            case 4:
                                stateDet = 'PRE-PAIEMENT EPUISE';
                                color = 'blue';
                                break;
                        }

                        var numberTourInfo1 = 'Nombre de tour souscrit : ' + '<span style="font-weight : bold;size:20px">' + detailPrepaiementList[i].numberTourSoumission + '</span>';
                        var numberTourInfo2 = 'Nombre de tour déjà utiliser : ' + '<span style="font-weight : bold;size:30px">' + detailPrepaiementList[i].numberTourUsing + '</span>';
                        var totalAmountTourInfo = 'Montant total payer : ' + '<span style="font-weight : bold;size:30px">' + formatNumber(detailPrepaiementList[i].totalTaux, detailPrepaiementList[i].devise) + '</span>';
                        var stateInfo = 'Etat : ' + '<span style="font-weight : bold;size:30px;color:' + color + '">' + stateDet + '</span>';

                        var paymentInfo = numberTourInfo1 + '<br/>' + numberTourInfo2 + '<br/>' + totalAmountTourInfo + '<br/>' + stateInfo;

                        tableDetailPrePaiement += '<tr>';

                        tableDetailPrePaiement += '<td hidden="true">' + detailPrepaiementList[i].bienCode + '</td>';
                        tableDetailPrePaiement += '<td style="text-align:left;width:20%;vertical-align:middle">' + bienNameInfo + '</td>';
                        tableDetailPrePaiement += '<td style="text-align:left;width:20%;vertical-align:middle">' + detailPrepaiementList[i].bienDescription + '</td>';
                        tableDetailPrePaiement += '<td style="text-align:left;width:20%;vertical-align:middle">' + detailPrepaiementList[i].tarifName + '<br/>' + '<span style="font-weight:bold">' + amount + '</span>' + '</td>';
                        tableDetailPrePaiement += '<td style="text-align:left;width:30%;vertical-align:middle">' + paymentInfo + '</td>';
                        tableDetailPrePaiement += '<td style="text-align:center;width:10%;vertical-align:middle">' + btn3 + '</td>';
                        tableDetailPrePaiement += '</tr>';
                    }

                }
            }

            tableDetailPrePaiement += '<tbody>';

            tableDetailPrePaiement += '</tbody>';
            row.child(tableDetailPrePaiement).show();
            tr.addClass('shown');


        }

    });

}

function getSelectedAssujetiData() {
    lblNameAssujet.html(selectAssujettiData.nomComplet);
    lblTypeAssujet.html(selectAssujettiData.categorie);
    lblAddresseAssujet.html(selectAssujettiData.adresse);
    codeAssujettis = selectAssujettiData.code;
    codeTypeAssujetti = selectAssujettiData.codeForme;

    lblTypeAss.show();
    lblNomAssujet.show();
    lblAdress.show();

    loadSousProvisions(codeAssujettis);

}

function loadArticleBudgetaire(codePrePaiementetti) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArtcleOfAssujetti',
            'codePersonne': codePrePaiementetti
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                var dataArticleBudgetaire = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < result.length; i++) {

                    dataArticleBudgetaire += '<option value =' + result[i].codeAB + '>' + result[i].intituleAB.toUpperCase() + '</option>';
                }

                cmbArticleBudgetaire.html(dataArticleBudgetaire);

            }
            , 1000);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function loadSousProvisions(codePrePaiementetti) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadSousProvisions',
            'codePersonne': codePrePaiementetti
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            prepaiementList = $.parseJSON(JSON.stringify(response));
            printPrepaiement(prepaiementList);


        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function getDetailsAssujettissement(codeArticle) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeAB': codeArticle,
            'codePersonne': codePrePaiementettis,
            'operation': 'getDetailsAssujettissement'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI()();
                alertify.alert('Aucune données trouvées.');
                return;
            } else {
                setTimeout(function () {

                    $.unblockUI();

                    dataDetailsPrevisionCredit = JSON.parse(JSON.stringify(response));

                    if (dataDetailsPrevisionCredit.length > 0) {
                        printDetailsPrevisionCredit(dataDetailsPrevisionCredit);
                    }

                }
                , 1);
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function preparerDetailsPrevisionCredit() {

    detailPrevisionCreditList = [];

    for (var i = 0; i < dataDetailsPrevisionCredit.length; i++) {

        var selectDetailPrevision = '#checkBoxSelectBienPrevCred' + dataDetailsPrevisionCredit[i].codebien;


        var select = $(selectDetailPrevision);

        if (select.is(':checked')) {

            var detailsPrevision = {};

            detailsPrevision.codeBien = dataDetailsPrevisionCredit[i].codebien;
            detailsPrevision.valeur = dataDetailsPrevisionCredit[i].valeur;
            detailsPrevision.devise = dataDetailsPrevisionCredit[i].devise;
            detailsPrevision.codeDetailAssujettisement = dataDetailsPrevisionCredit[i].codeDetailAssujettisement;


            detailPrevisionCreditList.push(detailsPrevision);

        }

    }

    detailPrevisionCreditList = JSON.stringify(detailPrevisionCreditList);

    return detailPrevisionCreditList;
}

function savePrevisionCredit() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'savePrevisionCredit',
            'userID': userData.idUser,
            'codePersonne': codePrePaiementettis,
            'detailPrevisionCreditList': JSON.stringify(detailPrepaiementListTampo)

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de type paiement en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement de type paiement s\'est effectué avec succès.');
                    loadSousProvisions(codePrePaiementettis);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function suspendreBienPrevisionCredit() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'suspendreBienPrevisionCredit',
            'userID': userData.idUser,
            'id': detailPrePaiementCodeSeleted

        },
        beforeSend: function () {

            modaTraitementlDetailProvisionCredit.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suspension en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modaTraitementlDetailProvisionCredit.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modaTraitementlDetailProvisionCredit.unblock();

                if (response == '1') {
                    alertify.alert('La suspension s\'est effectuée avec succès.');
                    modaTraitementlDetailProvisionCredit.modal('hide');
                    loadSousProvisions(codeAssujettis);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modaTraitementlDetailProvisionCredit.unblock();
            showResponseError();
        }
    });
}

function remplacerBienPrevisionCredit() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'remplacerBienPrevisionCredit',
            'userID': userData.idUser,
            'id': detailPrePaiementCodeSeleted,
            'bien': selectBiens.val()

        },
        beforeSend: function () {

            modaTraitementlDetailProvisionCredit.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Remplacement bien en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modaTraitementlDetailProvisionCredit.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modaTraitementlDetailProvisionCredit.unblock();

                if (response == '1') {
                    divTableTraitementPrevisionCredit.attr('style', 'display:none');
                    selectTypeTraitement.val('0');
                    selectBiens.val('0');
                    alertify.alert('Le remplacement du bien s\'est effectué avec succès.');
                    modaTraitementlDetailProvisionCredit.modal('hide');
                    loadSousProvisions(codeAssujettis);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modaTraitementlDetailProvisionCredit.unblock();
            showResponseError();
        }
    });
}

function cleanData() {

    printDetailsPrevisionCredit('');
    cmbArticleBudgetaire.html('');
    inputMontantPercu.val('');
    nbrToure.val('');

}

function displayModalTypePayment(code, name, amount1, tarif, devise, amount2, id, totalTaux, numnerTour, detailAssujId, prePaiementCode) {

    codeSelected = code;
    tauxSelected = parseFloat(amount2);
    idSelected = id;
    detailAssujIdSeleted = detailAssujId;
    prePaiementCodeSeleted = prePaiementCode;

    spnBienSelected.html(name);
    spnTarifBienSelected.html(tarif + ' : ' + amount1);
    selectTypePaiement.val('1');
    inputNbreTour.val(numnerTour);
    modalDetailProvisionCredit.modal('show');
    inputTotalAPayer.val(totalTaux);
    selectDevise.val(devise);


}

function checkFields() {

    if (selectTypePaiement.val() === '0') {

        alertify.alert('Veuillez d\'abord sélectionner un type paiement');
        return;
    }

    if (inputNbreTour.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir le nombre de tour à souscrire');
        return;
    } else if (inputNbreTour.val() < 0) {
        alertify.alert('Le nombre de tour à souscrire ne doit pas être inférieur à zéro');
        return;
    }

    if (inputTotalAPayer.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir le montant total à payer de nombre de tour souscrit');
        return;
    } else if (inputTotalAPayer.val() < 0) {
        alertify.alert('Le montant total à payer de nombre de tour souscrit ne doit pas être inférieur à zéro');
        return;
    }

    var newTotalPaid = parseFloat(tauxSelected * inputNbreTour.val());

    if (inputTotalAPayer.val() > newTotalPaid) {

        alertify.alert('Le montant total à payer de nombre de tour souscrit ne pas correct');
        return;
    }

    alertify.confirm('Etes-vous de vouloir renouveller cette sous-provision ?', function () {
        saveRenouvellementSouProvision();
    });
}

function saveRenouvellementSouProvision() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveRenouvellementSouProvision',
            'userID': userData.idUser,
            'numberTour': inputNbreTour.val(),
            'inputTotalAPayer': inputTotalAPayer.val(),
            'id': idSelected,
            'type': selectTypePaiement.val(),
            'devise': selectDevise.val(),
            'taux': tauxSelected,
            'bien': codeSelected,
            'prePaiementCode': prePaiementCodeSeleted,
            'detailAssujId': detailAssujIdSeleted
        },
        beforeSend: function () {

            modalDetailProvisionCredit.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Renouvellement en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalDetailProvisionCredit.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalDetailProvisionCredit.unblock();

                if (response == '1') {
                    alertify.alert('Le renouvelleement s\'est effectué avec succès.');
                    modalDetailProvisionCredit.modal('hide');
                    loadSousProvisions(codeAssujettis);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalDetailProvisionCredit.unblock;
            showResponseError();
        }
    });
}

function displayModalTraitementPP(code, state) {

    var operationTxt1 = '';
    var operationTxt2 = '';
    var operationTxt3 = '';

    if (state == '1') {
        operationTxt1 = ' suspendre';
        operationTxt2 = 'La suspension de la sous-provision est en cours...';
        operationTxt3 = 'La suspension de la sous-provision s\'est effectuée avec succès';

    } else if (state == '2') {

        operationTxt1 = ' valider';
        operationTxt2 = 'La validation de la sous-provision est en cours...';
        operationTxt3 = 'La validation de la sous-provision s\'est effectuée avec succès';

    } else if (state == '3') {

        operationTxt1 = ' réactiver';
        operationTxt2 = 'La réactivation de la sous-provision est en cours...';
        operationTxt3 = 'La réactivation de la sous-provision s\'est effectuée avec succès';
    }

    alertify.confirm('Etes-vous sûre de vouloir ' + operationTxt1 + ' cette sous-provision ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'editPrepaiement',
                'userID': userData.idUser,
                'code': code,
                'state': state

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>' + operationTxt2 + '</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert(operationTxt3);
                        loadSousProvisions(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });

    });


}

function displayTraiementDP(id, bienId, tarifCode) {

    divTableTraitementPrevisionCredit.attr('style', 'display:none');
    selectTypeTraitement.val('0');
    selectBiens.val('0');

    codeSelected = bienId;
    detailPrePaiementCodeSeleted = id;
    tarifCodeSelected = tarifCode;

    modaTraitementlDetailProvisionCredit.modal('show');
}


function loadBiensRemplacement() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensRemplacement',
            'codePersonne': codeAssujettis,
            'bien': codeSelected,
            'tarif': tarifCodeSelected
        },
        beforeSend: function () {
            modaTraitementlDetailProvisionCredit.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des biens en cours...</h5>'});
        },
        success: function (response)
        {
            modaTraitementlDetailProvisionCredit.unblock();
            if (response == '-1') {

                showResponseError();
                return;

            } else if (response == '0') {

                alertify.alert('Il n\'y a aucun bien de remplacement lié à cet assujetti ');
                return;

            } else {

                setTimeout(function () {

                    var result = $.parseJSON(JSON.stringify(response));

                    var data = '<option value ="0">--</option>';

                    for (var i = 0; i < result.length; i++) {

                        var nameBien = empty;

                        if (result[i].bienPlaque === empty) {
                            nameBien = result[i].bienName.toUpperCase() + ' (' + result[i].bienType + ')';
                        } else {
                            nameBien = result[i].bienName.toUpperCase() + ' (Plaque : ' + result[i].bienPlaque + ')';
                        }


                        data += '<option value =' + result[i].bienId + '>' + nameBien.toUpperCase() + '</option>';
                    }

                    selectBiens.html(data);
                    divTableTraitementPrevisionCredit.attr('style', 'display:inline');

                }
                , 1000);
            }

        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            modaTraitementlDetailProvisionCredit.unblock();
            showResponseError();
        }
    });
}

function activateDetailProvisionCredt(id) {

    alertify.confirm('Etes-vous sûre de vouloir réactiver le bien de cette sous-provision ?', function () {

        $.ajax({
            type: 'POST',
            url: 'peage_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'activateDetailProvisionCredt',
                'userID': userData.idUser,
                'id': id

            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La réactivation est en cours</h5>'});

            },
            success: function (response)
            {

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    $.unblockUI();

                    if (response == '1') {
                        alertify.alert('La réactivation s\'est effectuée avec succès');
                        loadSousProvisions(codeAssujettis);
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });

    });

}

