/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalRechercheAvanceePeage;

var selectSitePeage, selectTerif, selectTypePaiement, selectPercepteur;

var inputDateDebut;
var inputdateLast;

var datePickerDebut;
var datePickerFin;
var listSitePeage;


$(function () {

    modalRechercheAvanceePeage = $('#modalRechercheAvanceePeage');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    selectSitePeage = $('#selectSitePeage');
    selectTerif = $('#selectTerif');
    selectTypePaiement = $('#selectTypePaiement');
    selectPercepteur = $('#selectPercepteur');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());

    selectSitePeage.on('change', function (e) {
        loadTarifBySite(selectSitePeage.val());
    });

    loadSitePeage();

});

function loadSitePeage() {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadSitePeage'

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Il n\'y a pas des sites péage disponible');
                return;
            } else {

                listSitePeage = $.parseJSON(JSON.stringify(response));
                var dataSitePeage = empty;

                if (controlAccess('VIEW_ALL_SITE_PEAGE')) {

                    dataSitePeage += '<option value="*" style="font-style: italic;font-weight: bold">(Tous)</option>';
                    for (var i = 0; i < listSitePeage.length; i++) {
                        dataSitePeage += '<option value="' + listSitePeage[i].siteCode + '">' + listSitePeage[i].siteName + '</option>';
                    }

                    selectSitePeage.html(dataSitePeage);
                    selectSitePeage.val('*');
                    loadPercepteurBySite(selectSitePeage.val());

                } else {

                    for (var i = 0; i < listSitePeage.length; i++) {
                        if (listSitePeage[i].siteCode == userData.SiteCode) {
                            dataSitePeage += '<option value="' + listSitePeage[i].siteCode + '">' + listSitePeage[i].siteName + '</option>';
                        }

                    }

                    selectSitePeage.html(dataSitePeage);
                    selectSitePeage.val(userData.SiteCode);
                    loadTarifBySite(selectSitePeage.val());
                    loadPercepteurBySite(selectSitePeage.val());
                }

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadTarifBySite(codeSite) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTarifBySite',
            'site': codeSite

        },
        beforeSend: function () {

            modalRechercheAvanceePeage.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            modalRechercheAvanceePeage.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Il n\'y a pas des tarifs disponible dans ce site péage');
                return;
            } else {

                var listTarif = $.parseJSON(JSON.stringify(response));
                var dataTarif = empty;

                dataTarif += '<option value="*" style="font-style: italic;font-weight: bold">(Tous)</option>';
                for (var i = 0; i < listTarif.length; i++) {
                    dataTarif += '<option value="' + listTarif[i].codeTarif + '">' + listTarif[i].intituleTarif + '</option>';
                }

                selectTerif.html(dataTarif);
                selectTerif.val('*');

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalRechercheAvanceePeage.unblock();
            showResponseError();
        }
    });
}

function loadPercepteurBySite(codeSite) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadPercepteurBySite',
            'site': codeSite

        },
        beforeSend: function () {

            modalRechercheAvanceePeage.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});

        },
        success: function (response)
        {

            modalRechercheAvanceePeage.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Il n\'y a pas des percepteurs dans ce site péage');
                return;
            } else {

                var listPercepteur = $.parseJSON(JSON.stringify(response));
                var dataPercepteur = empty;

                var name = '';

                if (controlAccess('REGISTRE_TICKET_USER_ALL')) {

                    dataPercepteur += '<option value="*" style="font-style: italic;font-weight: bold">(Tous)</option>';
                    
                    for (var i = 0; i < listPercepteur.length; i++) {
                        name = listPercepteur[i].matriculePercepteur + ' - ' + listPercepteur[i].nomsPercepteur;
                        dataPercepteur += '<option value="' + listPercepteur[i].codePercepteur + '">' + name + '</option>';
                    }

                    selectPercepteur.html(dataPercepteur);
                    selectPercepteur.val('*');

                } else {

                    for (var i = 0; i < listPercepteur.length; i++) {

                        if (listPercepteur[i].codePercepteur == userData.idUser) {

                            name = listPercepteur[i].matriculePercepteur + ' - ' + listPercepteur[i].nomsPercepteur;
                            dataPercepteur += '<option value="' + listPercepteur[i].codePercepteur + '">' + name + '</option>';

                            selectPercepteur.html(dataPercepteur);
                            selectPercepteur.val(userData.idUser);
                        }

                    }



                }

            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalRechercheAvanceePeage.unblock();
            showResponseError();
        }
    });
}
