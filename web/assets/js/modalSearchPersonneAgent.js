/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var selectPersonneData;
var agentModal, btnCallModalSearchResponsable, inputValueResearchAgent, tableResultSearchAgent,
        btnSearchResponsable, btnSelectAgent;

$(function () {

    agentModal = $("#agentModal");
    btnCallModalSearchResponsable = $("#btnCallModalSearchResponsable");
    inputValueResearchAgent = $("#inputValueResearchAgent");
    tableResultSearchAgent = $("#tableResultSearchAgent");
    btnSearchResponsable = $("#btnSearchResponsable");
    btnSelectAgent = $("#btnSelectAgent");

    btnSearchResponsable.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchPersonnes();

    });

    btnSelectAgent.click(function (e) {

        if (selectPersonneData.code === empty) {
            alertify.alert('Veuillez d\'abord sélectionner une personne');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectPersonneData.nomComplet + '</b> ?', function () {
            getSelectedPersonneData();
            agentModal.modal('hide');
        });
    });

    inputValueResearchAgent.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchPersonnes();
        }

    });

    printPersonneTable('');

});

function printPersonneTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">NOM</th>';
    tableContent += '<th scope="col">POSTNOM</th>';
    tableContent += '<th scope="col">PRENOM</th>';
    tableContent += '<th hidden="true" scope="col">Code</th>';
    tableContent += '<th hidden="true" scope="col">nom Complet</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td>' + data[i].nom + '</td>';
        tableContent += '<td>' + data[i].postnom + '</td>';
        tableContent += '<td>' + data[i].prenom + '</td>';
        tableContent += '<td hidden="true">' + data[i].code + '</td>';
        tableContent += '<td hidden="true">' + data[i].nomComplet + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultSearchAgent.html(tableContent);

    var myDt = tableResultSearchAgent.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultSearchAgent tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectPersonneData = new Object();
        selectPersonneData.code = myDt.row(this).data()[3];
        selectPersonneData.nomComplet = myDt.row(this).data()[4];

    });
}

function searchPersonnes() {

    if (inputValueResearchAgent.val() === empty || inputValueResearchAgent.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'site_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'value': inputValueResearchAgent.val(),
            'operation': 'loadResponsable'
        },
        beforeSend: function () {
            agentModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                agentModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectAgent.hide();
                    printPersonneTable('');
                    return;
                } else {
                    var dataPerosnnes = JSON.parse(JSON.stringify(response));
                    if (dataPerosnnes.length > 0) {
                        btnSelectAgent.show();
                        printPersonneTable(dataPerosnnes);
                    } else {
                        btnSelectAgent.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            agentModal.unblock();
            showResponseError();
        }

    });
}
