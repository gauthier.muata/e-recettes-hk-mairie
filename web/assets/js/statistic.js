/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var data;

var cmbMinistere, cmbService;

var inputExercice;

var tableSecteurDactivite, tableArticleBudgetaire;
var cmbFaitgenerateur;
var modalMinistereService, exerciceFiscal;
var btnSearchArt;

$(function () {

    mainNavigationLabel.text('ASSIGNATION BUDGETAIRE');

    secondNavigationLabel.text('Statistique');

    cmbMinistere = $('#cmbMinistere');
    cmbMinistere.on('change', function () {
        loadFaitGenerateur("");
        var code = cmbMinistere.val();
        if (code == "0") {
            cmbService.html("");
            return;
        }
        loadService(code);
    });

    cmbService = $('#cmbService');
    btnSearchArt = $('#btnSearchArt');
    cmbFaitgenerateur = $('#cmbFaitgenerateur');
    exerciceFiscal = $('#exerciceFiscal');
    cmbService.on('change', function () {
        var code = cmbService.val();
        if (code == "0") {
            printFaitGenerateur("");
            return;
        }
        loadFaitGenerateur(code);
    });

    btnSearchArt.on('click', function (e) {
        e.preventDefault();
        loadArticleBudgetaire(cmbFaitgenerateur.val());
    });

    var myConfig = {
        type: 'bar',
        title: {
            text: 'Aticle budgetaire',
            fontSize: 24,
        },
        legend: {
            draggable: true,
        },
        scaleX: {
            // Set scale label
            label: {text: 'Article budgetaire'},
            // Convert text on scale indices
            labels: ['Article1']
        },
        scaleY: {
            // Scale label with unicode character
            label: {text: 'Montant'}
        },
        plot: {
            // Animation docs here:
            // https://www.zingchart.com/docs/tutorials/styling/animation#effect
            animation: {
                effect: 'ANIMATION_EXPAND_BOTTOM',
                method: 'ANIMATION_STRONG_EASE_OUT',
                sequence: 'ANIMATION_BY_NODE',
                speed: 275,
            }
        },
        series: [
            {
                // Plot 1 values, linear data
                values: [23],
                text: 'Montant realisé',
            },
            {
                // Plot 2 values, linear data
                values: [35],
                text: 'Montant assigné'
            },
            {
                // Plot 2 values, linear data
                values: [15],
                text: 'Ecart'
            }
        ]
    };

    zingchart.render({
        id: 'myChart',
        data: myConfig,
    });

    inputExercice = $('#inputExercice');

    modalMinistereService = $('#modalMinistereService');

    tableArticleBudgetaire = $('#tableArticleBudgetaire');

    loadMinistere();

    printArticleBudgetaire('');
    loadExercice();

});

function send(operation) {

    $.ajax({
        type: 'POST',
        url: 'assignationBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: data,
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            callback(JSON.parse(JSON.stringify(response)), operation);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function callback(response, operation) {

    var content;
    switch (operation) {
        case 'loadMinistere':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbMinistere.html(content);
            break;
        case 'loadServices':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbService.html(content);
            break;
        case 'loadFaitGenerateurs':

            content = '<option value="*">--- Sélectionner ---</option>';
            content = '<option value="0">Tout</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbFaitgenerateur.html(content);
            break;
        case 'loadArticleBudgetaireByExercice':
            printArticleBudgetaire(response);
    }
}

function loadMinistere() {
    data = {};
    data.operation = 'loadMinistere';
    send(data.operation);
}

function loadService(codeMinistere) {
    data = {};
    data.operation = 'loadServices';
    data.codeMinistere = codeMinistere;
    send(data.operation);
}

function loadFaitGenerateur(codeService) {
    data = {};
    data.operation = 'loadFaitGenerateurs';
    data.codeService = codeService;
    send(data.operation);
}

function loadArticleBudgetaire(code) {

    data = {};
    data.operation = 'loadArticleBudgetaireByExercice';
    data.codeAG = code;
    data.exerciceFiscal = exerciceFiscal.val();
    data.secteur = cmbService.val();

    send(data.operation);
}

function printArticleBudgetaire(articleBudgetaireList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    //header += '<th style="width:10%" scope="col"> Code officiel</th>';
    //header += '<th scope="col"> Fait générateur </th>';
    header += '<th scope="col"> Article budégtaire </th>';
    //header += '<th scope="col"> Montant Taxé </th>';
    //header += '<th scope="col"> Montant ordonancé </th>';
    header += '<th scope="col" style="text-align: right;"> Montant recouvré </th>';
    header += '<th scope="col" style="text-align: right;"> Assignation </th>';
    header += '<th scope="col" style="text-align: right;"> Ecart </th>';
    //header += '<th scope="col"> Période </th>';
    header += '<th ></th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    var totRecouvre = 0;
    var totAssignation = 0;
    var totEcart = 0;
    var ecart = 0;

    for (var i = 0; i < articleBudgetaireList.length; i++) {
        ecart = 0;

        ecart = articleBudgetaireList[i].totalRealisation - articleBudgetaireList[i].assignation;
        totRecouvre += articleBudgetaireList[i].totalRealisation;
        totAssignation += articleBudgetaireList[i].assignation;

        totEcart += ecart;

        body += '<tr >';
        //body += '<td>' + articleBudgetaireList[i].codeofficiel + '</td>';
        //body += '<td>' + articleBudgetaireList[i].faitgenerateur + '</td>';
        body += '<td>' + articleBudgetaireList[i].intitule + '</td>';
        //body += '<td></td>';
        //body += '<td></td>';
        body += '<td style="text-align: right;">' + formatNumber(articleBudgetaireList[i].totalRealisation, 'USD') + '</td>';
        body += '<td style="text-align: right;">' + formatNumber(articleBudgetaireList[i].assignation, 'USD') + '</td>';
        body += '<td style="text-align: right;">' + formatNumber(totEcart, articleBudgetaireList[i].devise) + '</td>';
        //body += '<td>' + articleBudgetaireList[i].intituleperiode + '</td>';
        body += '<td style="vertical-align:middle;text-align:center"><a onclick="viewGraphique(\'' + articleBudgetaireList[i].code + '\')" class="btn btn-success"><i class="fa fa-list"></i></a></td>';
        body += '</tr>';
    }

    body += '<tfoot>';

    body += '<tr><th colspan="1" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th></tr>';

    body += '</tfoot>';

    body += '</tbody>';
    var tableContent = header + body;
    tableArticleBudgetaire.html(tableContent);
    var dtArticleBudgetaire = tableArticleBudgetaire.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {

            var api = this.api(), data;

            $(api.column(1).footer()).html(
                    formatNumber(totRecouvre, 'USD'));

            $(api.column(2).footer()).html(
                    formatNumber(totAssignation, 'USD'));

            $(api.column(3).footer()).html(
                    formatNumber(totEcart, 'USD'));
        }
    });
    $('#tableArticleBudgetaire tbody').on('click', 'tr', function () {
        var data = dtArticleBudgetaire.row(this).data();
    });
}

function loadExercice() {

    var anneeDebut = 2000;
    var date = new Date();
    var anneeFin = date.getFullYear();
    var dataAnnee = '<option value ="0">-- Année --</option>';
    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }
    exerciceFiscal.html(dataAnnee);
}

function viewGraphique(code) {
    activaTab('dashTab2');

}

function activaTab(tab) {
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
}