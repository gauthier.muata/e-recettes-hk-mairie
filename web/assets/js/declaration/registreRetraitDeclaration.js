/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeResearchRegistreRetraitDeclaration;

var responsibleObject = new Object();

var inputResearchRetraitDeclar;

var btnResearchRetraitDeclaration, btnAdvencedSearch;

var btnCallModalSearchAvandedRetraitDeclaration;

var btnSelectArticleBudgetaire;

var articleBudgetaireModal;

var tableRegistreRetraitDeclaration;

var listRetraitDeclaration;

var chkbFilterTeleDeclaration, loadOnlyDeclarationOnline;

var assujettiModal;

var avisList;

var isAdvance, lblSite, lblService, labelType, lblDateDebut, lblDateFin;

var modalReviewAmountPenalite, spnAmountPenaliteInit, cmbTauxRemise, spnAmountPenaliteRemise, btnConfirmeRemisePenalite, spnMonthLate, inputObservationRemiseTaux;

var modalOrdonnancementControle;

var lblNoteTaxation, lblNameAssujetti, lblAdressAssujetti,
        lblRequerantControle, codeAvis, lblMereMontantPercu,
        numeroDepot;

var inputAreaObservationControlleur;

var btnValiderControle, tableDetailTaxation, cmbAvisControleur;

var tableNCChildControle, tableDFPControle;

var checkSearch = false;
var tempNCList = [];
var modalRechercheAvancee, isMensuel = false, abCode;
var sumCDF = 0, sumUSD = 0, tscr = 0, penalite = 0, impot = 0;
total = 0;

$(function () {

    loadOnlyDeclarationOnline = '0';

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Registre des notes de taxation');

    removeActiveMenu();
    linkSubMenuRegistreRetraitDeclaration.addClass('active');

    tableRegistreRetraitDeclaration = $('#tableRegistreRetraitDeclaration');

    tableDetailTaxation = $('#tableDetailTaxation');
    tableNCChildControle = $('#tableNCChildControle');
    tableDFPControle = $('#tableDFPControle');

    btnCallModalSearchAvandedRetraitDeclaration = $('#btnCallModalSearchAvandedRetraitDeclaration');

    modalOrdonnancementControle = $('#modalOrdonnancementControle');

    cmbAvisControleur = $('#cmbAvisControleur');

    btnSelectArticleBudgetaire = $('#btnSelectArticleBudgetaire');
    articleBudgetaireModal = $('#articleBudgetaireModal');

    btnResearchRetraitDeclaration = $('#btnResearchRetraitDeclaration');
    btnAdvencedSearch = $('.btnAdvencedSearch');

    btnValiderControle = $('#btnValiderControle');

    inputResearchRetraitDeclar = $('#inputResearchRetraitDeclar');

    inputAreaObservationControlleur = $('#inputAreaObservationControlleur');

    typeResearchRegistreRetraitDeclaration = $('#typeResearchRegistreRetraitDeclaration');
    chkbFilterTeleDeclaration = $('#chkbFilterTeleDeclaration');
    assujettiModal = $('#assujettiModal');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    labelType = $('#labelType');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    lblNoteTaxation = $('#lblNoteTaxation');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblAdressAssujetti = $('#lblAdressAssujetti');
    lblRequerantControle = $('#lblRequerantControle');
    lblMereMontantPercu = $('#lblMereMontantPercu');

    modalReviewAmountPenalite = $('#modalReviewAmountPenalite');
    spnAmountPenaliteInit = $('#spnAmountPenaliteInit');
    cmbTauxRemise = $('#cmbTauxRemise');
    spnAmountPenaliteRemise = $('#spnAmountPenaliteRemise');
    btnConfirmeRemisePenalite = $('#btnConfirmeRemisePenalite');
    spnMonthLate = $('#spnMonthLate');
    inputObservationRemiseTaux = $('#inputObservationRemiseTaux');
    modalRechercheAvancee = $('.modalRechercheAvancee');

    btnCallModalSearchAvandedRetraitDeclaration.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvancee.modal('show');
//        modalRechercheAvanceeNC.modal('show');
    });

    btnResearchRetraitDeclaration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvance.attr('style', 'display: none');
        checkSearch = false;

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (inputResearchRetraitDeclar.val() === empty) {
                    alertify.alert('Veuillez fournir le numéro de la déclaration avant de lancer la recherche');
                    return;
                } else {
                    loadRetraitDeclaration();
                }

                break;
        }


    });

    typeResearchRegistreRetraitDeclaration.on('change', function (e) {

        inputResearchRetraitDeclar.val(empty);

        switch (typeResearchRegistreRetraitDeclaration.val()) {
            case '1':
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                inputResearchRetraitDeclar.attr('readonly', true);
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                inputResearchRetraitDeclar.attr('readonly', false);
                inputResearchRetraitDeclar.attr('placeholder', 'Veuillez saisir le numéro déclaration');
                inputResearchRetraitDeclar.attr('style', 'font-weight:normal;width: 380px');
                break;
        }
    });

    cmbAvisControleur.on('change', function (e) {
        codeAvis = cmbAvisControleur.val();
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isAdvance.attr('style', 'display: block');
        checkSearch = true;
        $('.impression').html('Imprimer le rapport (' + $('.cmbAB option:selected').text() + ')')
        declarationByAdvancedSearch();
    });

    btnValiderControle.click(function (e) {
        e.preventDefault();
        validateControleurAvis();

    });

    loadRetraitDeclarationTable('');

    chkbFilterTeleDeclaration.attr('style', 'display: inline');

//    btnAdvencedSearch.trigger('click');

    getAvis();

    $('.impression').click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer le registre des retraits d&eacute;claration ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            if (tempNCList.length != 0) {
                printDataNoteCalcul();
            } else {
                alertify.alert('Aucune donnée n\'est disponnible pour imprimer ce rapport.');
            }
        });
    });

});

//chkbFilterTeleDeclaration

function handleClick(cb) {

    switch (cb.checked) {
        case true:
            loadOnlyDeclarationOnline = '1';
            break;
        case false:
            loadOnlyDeclarationOnline = '0';
            break;
    }
}

function loadRetraitDeclaration() {

    if (inputDateDebut.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date de début de la recherche.');
        return;
    }

    if (inputdateLast.val() === '') {
        alertify.alert('Veuillez d\'abord indiquer la date fin de la recherche.');
        return;
    }

    var viewAllService = controlAccess('VIEW_ALL_SERVICES_NC');
    registerType = getRegisterType();

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': typeResearchRegistreRetraitDeclaration.val() === '1' ? responsibleObject.codeResponsible : inputResearchRetraitDeclar.val(),
            'typeSearch': typeResearchRegistreRetraitDeclaration.val(),
            'typeRegister': registerType,
            'allService': viewAllService,
            'codeSite': userData.SiteCode,
            'codeService': userData.serviceCode,
            'userId': userData.idUser,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'operation': 'searchRetraitDeclaration'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'})
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    listRetraitDeclaration = null;
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    inputResearchRetraitDeclar.val(selectAssujettiData.nomComplet);
    inputResearchRetraitDeclar.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadRetraitDeclaration();

}

function loadRetraitDeclarationTable(result) {

    console.log(result)

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    //tableContent += '<th style="text-align:left;display:none"scope="col">Déclaration</th>';
    tableContent += '<th style="text-align:left">NOTE TAXATION</th>';
    tableContent += '<th style="text-align:left"scope="col">BIEN</th>';
    tableContent += '<th style="text-align:left"scope="col">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left"scope="col">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left"scope="col">DATE RETRAIT</th>';
    tableContent += '<th style="text-align:left"scope="col">ECHEANCE PAIE</th>';
    tableContent += '<th style="text-align:right"scope="col">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left"scope="col">BANQUE</th>';
    tableContent += '<th style="text-align:left;width:8%"scope="col"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    sumCDF = 0, sumUSD = 0, tscr = 0, penalite = 0, impot = 0, total = 0;
    tempNCList = [];

    for (var i = 0; i < result.length; i++) {

        var firstLineAB = '';
        if (result[i].libelleArticleBudgetaire.length > 250) {
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = result[i].libelleArticleBudgetaire.toUpperCase();
        }

        var buttonDisplayDocument = '';
        var buttonsavePayment = '';
        var buttonOperation = '';
        var callNext = false;
        if (result[i].bordereauExist === '0') {

            //buttonsavePayment = '<center><button class="btn btn-warning" onclick="callModalSavePayment(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-list"></i></button></center>';

            if (result[i].preuvePaiementExist === '1') {
                callNext = true;
                buttonDisplayDocument = '&nbsp;<center><button class="btn btn-primary" onclick="callModalDisplayDocument(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-print"></i></button></center>';
                buttonOperation = buttonDisplayDocument + ' ' + buttonsavePayment;
            }

        } else {
            callNext = true;
            buttonDisplayDocument = '&nbsp;<button class="btn btn-primary" onclick="callModalDisplayDocument(\'' + result[i].idRetraitDeclaration + '\',\'' + callNext + '\')"><i class="fa fa-print"></i></button>';
            buttonOperation = buttonDisplayDocument;
        }

        var descriptionBien = empty, descriptionBien2 = '';
        var btnPrintNoteTaxation = '&nbsp;<button class="btn btn-warning" title="Cliquez ici pour réimprimer la note de taxation" onclick="printNoteTaxation(\'' + result[i].numeroDepotDeclaration + '\',\'' + result[i].idRetraitDeclaration + '\')"><i class="fa fa-print"></i></button>';
        var btnCallModalDsiplayRemise = '';
        if (result[i].resmiseExist == '1') {
            btnCallModalDsiplayRemise = '&nbsp;<button class="btn btn-primary" title="Cliquez ici pour afficher les informations sur la remise de la pénalité" onclick="callModalDsiplayRemise(\'' + result[i].amountRemise + '\',\'' + result[i].tauxRemise + '\',\'' + result[i].observationRemise + '\',\'' + result[i].devise + '\',\'' + result[i].montantPercu + '\',\'' + result[i].moisRetard + '\')"><i class="fa fa-list"></i></button>';
        }

        if (result[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + result[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + result[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + result[i].quartierName + '</span>';
            var bienInfo = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
            var adresseInfo = result[i].adresseBien.toUpperCase();

            descriptionBien2 = result[i].intituleBien + '\n' + 'Nature : ' + result[i].libelleTypeBien + '\n' + 'Usage : ' + result[i].usageName + '\n' + 'Catégorie : ' + result[i].tarifName + '\n' + 'Commune : ' + result[i].communeName.replace('</span', '').replace('<span style=\'font-weight:bold\'>', '').replace('>)', '') + '\n\n' + result[i].adresseBien.toUpperCase();
            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + adresseInfo;
        } else {

            var bienInfo2, natureInfo2, categorieInfo2, adresseInfo2, descriptionBien2;

            if (result[i].type == 1) {

                bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                adresseInfo2 = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;

                descriptionBien2 = bienInfo2 + '\n' + natureInfo2 + '\n' + categorieInfo2 + '\n\n' + adresseInfo2;

            } else if (result[i].type == 3) {
                
                bienInfo2 = '<span style="font-weight:bold">' + result[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + result[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + result[i].tarifName + '</span>';
                adresseInfo2 = result[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;

                descriptionBien2 = bienInfo2 + '\n' + natureInfo2 + '\n' + categorieInfo2 + '\n\n' + adresseInfo2;
            }

        }

        var assujettiInfo = '<span style="font-weight:bold">' + result[i].nom + '</span>';
        var numeroDeclarationInfo = '<span style="font-weight:bold">' + result[i].numeroDepotDeclaration + '</span>';
        var color = 'green';
        var btnCallModalSignalerProbleme = '&nbsp;<button class="btn btn-success" title="Cliquez ici pour faire un contrôle" onclick="callModalSignalerProbleme(\'' + result[i].idRetraitDeclaration + '\',\'' + result[i].numeroDepotDeclaration + '\')"><i class="fa fa-check-circle"></i></button>';
        if (result[i].echeanceDepasse == '1') {
            color = 'red';
        }

        buttonOperation = btnPrintNoteTaxation + btnCallModalDsiplayRemise + btnCallModalSignalerProbleme;
        var statePayment = '';
        if (result[i].bordereauExist == '1') {
            statePayment = '<hr/>' + '<span style="color:green">' + 'payé'.toUpperCase() + '</span>';
        } else {
            statePayment = '<hr/>' + '<span style="color:red">' + 'non payé'.toUpperCase() + '</span>';
        }

        var bank = 'Banque : ' + '<span style="font-weight:bold">' + result[i].banqueName + '</span>';
        var accountBank = 'Compte bancaire : ' + '<br/><br/><span style="font-weight:bold">' + result[i].compteBancaireName + '</span>';
        var bankInfo = bank + '<br/><br/>' + accountBank;
        var textTypeTaxation = '', textTypeTaxation2 = '';
        if (result[i].typeTaxation == '1') {
            textTypeTaxation = '<span style="color:red"> ' + "(TAXATION D\'OFFICE)" + '</span>';
            textTypeTaxation2 = '(TAXATION D\'OFFICE)';
        } else {
            textTypeTaxation = '<span style="color:green">' + "(TAXATION ORDINAIRE)" + '</span>';
            textTypeTaxation2 = '(TAXATION ORDINAIRE)';
        }

        if (checkSearch == false) {
            abCode = result[i].codeArticleBudgetaire;
        }

        var nc = new Object();
        nc.noteTaxation = result[i].numeroDepotDeclaration + '\n' + textTypeTaxation2;
        nc.note = result[i].numeroDepotDeclaration;
        nc.nomComplet = result[i].nomComplet;
        nc.bien = descriptionBien2;
        nc.libelleTypeBien = result[i].libelleTypeBien;
        nc.tarifName = result[i].tarifName;
        nc.nature = result[i].libelleTypeBien;
        nc.marque = result[i].marque;
        nc.modele = '';
        nc.plaque = '';
        nc.intitulePeriodicite = result[i].IntitulePeriodicite;
        nc.anneePeriode = result[i].IntitulePeriodicite.substring(result[i].IntitulePeriodicite.length - 4, 4);
        nc.proprietaire = result[i].proprietaire;
        nc.superficie = '';
        nc.nobreEtage = '';
        nc.penalites = 0;
        nc.idRetraitDeclaration = result[i].idRetraitDeclaration;
        nc.articleBudgetaire = firstLineAB + '\nPériode déclaration: ' + result[i].IntitulePeriodicite;
        nc.dateRetrait = result[i].dateCreate;
        nc.echeancePaie = result[i].dateEcheance;
        nc.adressePersonne = result[i].adressePersonne;
        if (result[i].adresseProprietaire !== null) {
            nc.adresseProprietaire = result[i].adresseProprietaire;
        } else {
            nc.adresseProprietaire = result[i].adressePersonne;
        }
        nc.montantDu = result[i].montantPercu + ' ' + result[i].devise;
        nc.montantImpot = result[i].montantImpot;
        nc.montantTscr = result[i].montantTscr;
        nc.montantTotal = result[i].montantTotal;
        nc.banque = 'Banque: ' + result[i].banqueName + '\nCompte bancaire:' + result[i].compteBancaireName;
        tempNCList.push(nc);

        var siteNameInfo = '';

        if (result[i].siteName !== '') {
            siteNameInfo = '<br/><br/>' + 'BUREAU : ' + '<span style="font-weight:bold">' + result[i].siteName + '</span>';
        }

        var othersInfoTaxation = '';

        if (result[i].estTaxationPenalite == '0') {
            othersInfoTaxation = 'EST TAXATION PRINCIPALE';
        } else {
            othersInfoTaxation = 'EST TAXATION DE PENALITE DE : (' + '<span style="font-weight:bold;font-style: italic;color:red">' + result[i].codeNTPricipal + '</span>' + ')';
        }


        if (result[i].devise === 'CDF') {
            sumCDF += result[i].montantPercu;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].montantPercu;
        }
        impot += result[i].montantImpot;
        tscr += result[i].montantTscr;
        penalite = 0;
        total += result[i].montantTotal;

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:8%;">' + numeroDeclarationInfo + '<hr/>' + textTypeTaxation + '<hr/>' + othersInfoTaxation + '</td>';
        tableContent += '<td style="vertical-align:middle;width:20%">' + descriptionBien + '</td>';
        tableContent += '<td style="vertical-align:middle;width:16%">' + assujettiInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:13%"><span style="font-weight:bold;"></span>' + firstLineAB + ' <hr/>' + 'Période Déclaration : ' + '<span style="font-weight:bold">' + result[i].IntitulePeriodicite + siteNameInfo + '</span>' + '</td>'
        tableContent += '<td style="vertical-align:middle;width:8%">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;font-weight:bold;color:' + color + '">' + result[i].dateEcheance + '</td>';
        tableContent += '<td style="vertical-align:middle;width:8%;text-align:right;font-weight:bold">' + formatNumber(result[i].montantPercu, result[i].devise) + statePayment + '</td>';
        tableContent += '<td style="vertical-align:middle;width:12%;text-align:left">' + bankInfo + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:center">' + buttonOperation + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    tableContent += '</tfoot>';

    tableRegistreRetraitDeclaration.html(tableContent);
    tableRegistreRetraitDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        filter: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        //,lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 25,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6" class="group"><td colspan="10">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function callModalSavePayment(idRetrait, callNextStap) {
    alert('callModalSavePayment : ' + idRetrait);
}

function callModalDisplayDocument(idRetrait, callNextStap) {

    //alert('callModalDisplayDocument : ' + idRetrait);

    for (var i = 0; i < listRetraitDeclaration.length; i++) {

        if (listRetraitDeclaration[i].idRetraitDeclaration == idRetrait) {
            reprintRecepisse(listRetraitDeclaration[i].numeroDepotDeclaration);
//            initPrintData(idRetrait);
        }
    }


}

function declarationByAdvancedSearch() {

    registerType = getRegisterType();

    switch ($('.cmbBureau').val()) {

        case '*':
            lblSite.text('TOUS');
            lblSite.attr('title', 'TOUS');
            break;
        default:
            lblSite.text($('cmbBureau option:selected').text().toUpperCase());
            lblSite.attr('title', $('cmbBureau option:selected').text().toUpperCase());
            break;
    }

    switch ($('.cmbBureau').val()) {

        case '*':
            lblService.text('TOUS');
            lblService.attr('title', 'TOUS');
            break;
        default:
            lblService.text($('.cmbAB option:selected').text().toUpperCase());
            lblService.attr('title', $('.cmbAB option:selected').text().toUpperCase());
            break;
    }

    if (loadOnlyDeclarationOnline === '0') {
        labelType.text('NORMALE');
        labelType.attr('title', 'NORMALE');
    } else {
        labelType.text('EN LIGNE');
        labelType.attr('title', 'EN LIGNE');
    }

    lblDateDebut.text(dateDebut.val());
    lblDateDebut.attr('title', dateDebut.val());

    lblDateFin.text(dateFin.val());
    lblDateFin.attr('title', dateFin.val());


    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'SiteCode': $('.cmbBureau').val(),
            'codeAb': $('.cmbAB').val(),
            'mois': $('.cmbMois').val(),
            'annee': $('.cmbAnnee').val(),
            'declarationOnlineOnly': loadOnlyDeclarationOnline,
            'isMensuel': isMensuel,
            'codeAgent': userData.idUser,
            'dateDebut': dateDebut.val(),
            'dateFin': dateFin.val(),
            'operation': 'SearchAvancedRetraitDeclaration'
        },
        beforeSend: function () {
//            modalRechercheAvanceeNC.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
//                modalRechercheAvancee.unblock();
//                modalRechercheAvanceeNC.unblock();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    modalRechercheAvancee.modal('hide');
                    listRetraitDeclaration = '';
                    tempNCList = [];
                    loadRetraitDeclarationTable('');
                    alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                    return;
                } else {
                    modalRechercheAvancee.modal('hide');
                    listRetraitDeclaration = null;
                    tempNCList = [];
                    listRetraitDeclaration = JSON.parse(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function reprintRecepisse(declaration) {

    if (declaration == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeDepotDeclaration': declaration,
            'operation': 'reprintRecepisse'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            if (response == '0') {
                $.unblockUI();
                alertify.alert('Document non trouvé.');
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printNoteTaxation(numero, codePeriodeDeclaration) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir imprimer la note de taxation n° ' + value + ' ?', function () {

        $.ajax({
            type: 'POST',
            url: 'declaration_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numero': codePeriodeDeclaration,
                'typeDoc': "NT",
                'operation': 'printNoteTaxation'
            },
            beforeSend: function () {

                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    $.unblockUI();
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    $.unblockUI();
                    alertify.alert('Aucune note de taxation trouvée correspondant à ce numéro : ' + numero);
                    return;
                }

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });

    });

}

function callModalSignalerProbleme(id, numero) {

    var value = '<span style="font-weight:bold">' + numero + '</span>';

    alertify.confirm('Etes-vous sûre de vouloir faire un contrôle pour la note de taxation n° ' + value + ' ?', function () {

        modalOrdonnancementControle.modal('show');

        loadDetailTaxationTable('');

        for (var i = 0; i < listRetraitDeclaration.length; i++) {

            if (listRetraitDeclaration[i].numeroDepotDeclaration === numero) {

                lblNoteTaxation.html(listRetraitDeclaration[i].numeroDepotDeclaration);
                lblNameAssujetti.html(listRetraitDeclaration[i].nom);
                lblAdressAssujetti.html(listRetraitDeclaration[i].adresseBien);
                lblRequerantControle.html(listRetraitDeclaration[i].requerant);
                lblMereMontantPercu.html(formatNumber(listRetraitDeclaration[i].montantPercu, listRetraitDeclaration[i].devise));
                numeroDepot = listRetraitDeclaration[i].idRetraitDeclaration;

                if (listRetraitDeclaration[i].idAvis == '') {
                    cmbAvisControleur.attr('disabled', false);
                    cmbAvisControleur.val('');
                    inputAreaObservationControlleur.html('');
                    inputAreaObservationControlleur.attr('readOnly', false);
                    btnValiderControle.attr('style', 'display:inline');
                } else {
                    cmbAvisControleur.val(listRetraitDeclaration[i].idAvis);
                    cmbAvisControleur.attr('disabled', true);
                    inputAreaObservationControlleur.attr('readOnly', true);
                    inputAreaObservationControlleur.html(listRetraitDeclaration[i].observationControle);
                    btnValiderControle.attr('style', 'display:none');
                }

                loadDetailTaxationTable(listRetraitDeclaration);

                break;

            }

        }
    });

}

function callModalDsiplayRemise(amountRemise, tauxRemise, observationRemise, devise, amount, moisRetard) {

    spnAmountPenaliteInit.html(formatNumber(amount, devise));
    cmbTauxRemise.val(tauxRemise);
    cmbTauxRemise.attr('disabled', true);
    spnAmountPenaliteRemise.html(formatNumber(amountRemise, devise));

    btnConfirmeRemisePenalite.attr('style', 'display:none');
    spnMonthLate.html(moisRetard);
    inputObservationRemiseTaux.val(observationRemise);

    modalReviewAmountPenalite.modal('show');
}

function validateControleurAvis() {

    codeAvis = cmbAvisControleur.val();

    if (codeAvis === '' || codeAvis === '0') {
        alertify.alert('Veuillez d\'abord sélectionner un avis.');
        return;
    }

    if (inputAreaObservationControlleur.val() === '' && codeAvis === '0') {
        alertify.alert('Veuillez motiver votre décision dans l\'observation.');
        return;
    }

    alertify.confirm('Etes-vous sûre de vouloir valider cette opération ?', function () {
        validateControle();
    });
}

function validateControle() {


    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'numeroDepot': numeroDepot,
            'idAvis': codeAvis,
            'observationOrdonnancement': inputAreaObservationControlleur.val(),
            'operation': 'controleTaxation'
        },
        beforeSend: function () {
            modalOrdonnancementControle.unblock({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du contrôle du taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            modalOrdonnancementControle.unblock();
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors l\'enregistrement de la décison du contrôle');
                    return;
                } else if (response == '1') {
                    alertify.alert('La taxation est validée');

                    if (checkSearch) {
                        btnAdvencedSearch.trigger('click');
                    } else {
                        loadRetraitDeclaration();
                    }
                    modalOrdonnancementControle.modal('hide');

                    codeAvis = '';
                    inputAreaObservationControlleur.val('');
                    cmbAvisControleur.val('0');
                }
            }, 100);
        },
        complete: function () {
        },
        error: function (xhr) {
            modalOrdonnancementControle.unblock();
            showResponseError();
        }
    });
}

function loadDetailTaxationTable(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">PERIODICITE</th>';
    tableContent += '<th style="text-align:left">MOIS RETARD</th>';
    tableContent += '<th style="text-align:center">PENALITE</th>';
    tableContent += '<th style="text-align:right">TAUX</th>';
    tableContent += '<th style="text-align:right">MONTANT </th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var firstLineAB = '';

    //alert(JSON.stringify(result));

    for (var i = 0; i < result.length; i++) {

        if (result[i].libelleArticleBudgetaire.length > 40) {
            //firstLineAB = result[i].codeBudgetaire + ' / ' + result[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            firstLineAB = result[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
        } else {
            //firstLineAB = result[i].codeBudgetaire + ' / ' + result[i].libelleArticleBudgetaire;
            firstLineAB = result[i].libelleArticleBudgetaire;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:19%;vertical-align:middle" title="' + result[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].IntitulePeriodicite + '</td>';
        tableContent += '<td style="text-align:left;width:6%;vertical-align:middle">' + result[i].moisRetard + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].amountRemise + '</td>';
        tableContent += '<td style="text-align:right;width:6%;vertical-align:middle">' + result[i].tauxRemise + '</td>';
        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + formatNumber(result[i].montantPercu, result[i].devise) + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableDetailTaxation.html(tableContent);
    tableDetailTaxation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le detail des actes générateurs est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        datalength: 7
    });
}

function getAvis() {

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getAvis'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var dataAvais = '<option value ="0">-- Sélectionner un avis --</option>';
                for (var i = 0; i < result.length; i++) {
                    dataAvais += '<option value =' + result[i].idAvis + '>' + result[i].libelleAvis + '</option>';
                }
                cmbAvisControleur.html(dataAvais);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataNoteCalcul() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    var columns = [], columnStyles;
    var carEspace = '';

    if (tempNCList.length > 0) {

        if (checkSearch) {
            position = 270;
        } else {
            var critere = inputResearchRetraitDeclar.val().toUpperCase();
            docTitle = carEspace.substring(0, critere.length * 2) + docTitle + ' : ' + critere;
            position = 250;
        }
        hauteur = 160;

    } else {
        position = 160;
    }

    if (abCode === AB_IF) {
        carEspace = '                                            ';
        docTitle = carEspace + "DIRECTION\nImpot sur la superficie des propriétés foncières baties et non baties";
        columns = [
            {title: "DATE NOTE", dataKey: "dateRetrait"},
            {title: "CONTRIBUABLE", dataKey: "nomComplet"},
            {title: "RANG", dataKey: "tarifName"},
            {title: "NATURE", dataKey: "nature"},
            {title: "SUPERFICIE", dataKey: "superficie"},
            {title: "NBRE. D'ETAGE", dataKey: "nbreEtage"},
            {title: "N° TAXATION", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT", dataKey: "montantDu"},
            {title: "PENALITES", dataKey: "penalites"},
            {title: "ADRESSES", dataKey: "adressePersonne"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            nature: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            superficie: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            nbreEtage: {columnWidth: 85, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 55, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            adressePersonne: {columnWidth: 140, overflow: 'linebreak', fontSize: 8}
        };

    } else if (abCode == AB_IRL) {
        carEspace = '                                       ';
        if (checkSearch) {
            docTitle = carEspace + "                                       DIRECTION\n      Impot sur les revenus locatifs (" + $('.cmbAnnee').val() + ')';
        } else {
            docTitle = carEspace + '                                       DIRECTION\n                   Impot sur les revenus locatifs';
        }
        columns = [
            {title: "DATE NOTE", dataKey: "dateRetrait"},
            {title: "REDEVABLES", dataKey: "nomComplet"},
            {title: "PERIODE", dataKey: "anneePeriode"},
            {title: "N° TAX.", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT", dataKey: "montantDu"},
            {title: "PENALITES", dataKey: "penalites"},
            {title: "RANG", dataKey: "tarifName"},
            {title: "ADRESSE", dataKey: "adressePersonne"},
            {title: "BAILLEURS", dataKey: "proprietaire"},
            {title: "ADRESSE BAILLEURS", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            anneePeriode: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            adressePersonne: {columnWidth: 120, overflow: 'linebreak', fontSize: 8},
            proprietaire: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 8}
        };

    } else if (abCode === AB_RL) {
        carEspace = '                                       ';
        if (checkSearch) {
            docTitle = carEspace + "DIRECTION\n        Impot sur les revenus locatifs (" + $('.cmbMois').val() + '/' + $('.cmbAnnee').val() + ')';
        } else {
            docTitle = carEspace + 'DIRECTION\n                   Impot sur les revenus locatifs';
        }

        columns = [
            {title: "DATE NOTE", dataKey: "dateRetrait"},
            {title: "REDEVABLES", dataKey: "nomComplet"},
            {title: "PERIODE", dataKey: "intitulePeriodicite"},
            {title: "N° TAX.", dataKey: "idRetraitDeclaration"},
            {title: "MONTANT", dataKey: "montantDu"},
            {title: "PENALITES", dataKey: "penalites"},
            {title: "RANG", dataKey: "tarifName"},
            {title: "ADRESSE", dataKey: "adressePersonne"},
            {title: "BAILLEURS", dataKey: "proprietaire"},
            {title: "ADRESSE BAILLEURS", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            intitulePeriodicite: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            idRetraitDeclaration: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            tarifName: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            adressePersonne: {columnWidth: 120, overflow: 'linebreak', fontSize: 8},
            proprietaire: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 8}
        };
    } else if (abCode == AB_VIGNETTE) {
        carEspace = '                                                  ';
//        if (checkSearch) {
        docTitle = carEspace + "DIVISION DES IMPOTS\nRECETTES ORDONNANCEES DE L\'IMPOT SUR LES VEHICULES ET DE LA TAXE\n\
                                   SPECIALE DE CIRCULATION ROUTIERE\n\
                                                                   " + $('.cmbAnnee').val();
//        } else {
//            docTitle = carEspace + 'DIVISION DES IMPOTS\n                   Impot sur les revenus locatifs';
//        }

        hauteur = 185;

        columns = [
            {title: "DATE NOTE", dataKey: "dateRetrait"},
            {title: "CONTRIBUABLES", dataKey: "nomComplet"},
            {title: "NOTE", dataKey: "note"},
            {title: "MARQUE", dataKey: "marque"},
            {title: "MODELE", dataKey: "modele"},
            {title: "GENRE", dataKey: "libelleTypeBien"},
            {title: "PLAQUE", dataKey: "plaque"},
            {title: "IMPOT", dataKey: "montantImpot"},
            {title: "TSCR", dataKey: "montantTscr"},
            {title: "PENALITES", dataKey: "penalites"},
            {title: "MONT. TOT", dataKey: "montantTotal"},
            {title: "ADRESSE", dataKey: "adresseProprietaire"}
        ];

        columnStyles = {
            dateRetrait: {columnWidth: 65, fontSize: 8, overflow: 'linebreak'},
            nomComplet: {columnWidth: 90, overflow: 'linebreak', fontSize: 8},
            note: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            marque: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            modele: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            libelleTypeBien: {columnWidth: 65, overflow: 'linebreak', fontSize: 8},
            plaque: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            montantImpot: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantTscr: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            penalites: {columnWidth: 50, overflow: 'linebreak', fontSize: 8},
            montantTotal: {columnWidth: 60, overflow: 'linebreak', fontSize: 8},
            adresseProprietaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 7}
        };
    } else {
        carEspace = '                                       ';
        docTitle = carEspace + 'DIRECTION\n                   Rapport des notes de taxation';
        columns = [
            {title: "NOTE TAXATIONd", dataKey: "noteTaxation"},
            {title: "BIEN", dataKey: "bien"},
            {title: "ARTICLE BUDGETAIRE", dataKey: "articleBudgetaire"},
            {title: "DATE RETRAIT", dataKey: "dateRetrait"},
            {title: "ECHEANCE PAIE", dataKey: "echeancePaie"},
            {title: "MONTANT DÛ", dataKey: "montantDu"},
            {title: "BANQUE", dataKey: "banque"}];

        columnStyles = {
            noteTaxation: {columnWidth: 85, fontSize: 8, overflow: 'linebreak'},
            bien: {columnWidth: 160, overflow: 'linebreak', fontSize: 8},
            articleBudgetaire: {columnWidth: 120, overflow: 'linebreak', fontSize: 9},
            dateRetrait: {columnWidth: 75, overflow: 'linebreak', fontSize: 8},
            echeancePaie: {columnWidth: 85, overflow: 'linebreak', fontSize: 8},
            montantDu: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            banque: {columnWidth: 180, overflow: 'linebreak', fontSize: 9}
        };
    }

    var rows = tempNCList;
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: columnStyles,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');

    if (abCode == AB_VIGNETTE) {
        doc.text("TSCR :  " + formatNumber(tscr, 'USD'), 540, finalY + 40);
        doc.text("IMPOT :  " + formatNumber(impot, 'USD'), 540, finalY + 55);
        doc.text("MONTANT TOTAL :  " + formatNumber(total, 'USD'), 540, finalY + 70);
        doc.text("PENALITE :  " + formatNumber(penalite, 'USD'), 540, finalY + 85);
    } else {
        doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
        doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 55);
    }
    window.open(doc.output('bloburl'), '_blank');
}