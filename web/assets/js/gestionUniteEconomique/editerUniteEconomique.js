/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalCreateUniteEconomique, divBtnAdresseControlUnite, modalAdressesPersonne;

var cmbTypeBienUnite, cmbCommuneUnite, cmbQuartierUnite, cmbActiviteUnite;

var inputIntituleUnite, inputDescriptionUnite, inputDateAcquissitionUnite,
        inputAdresse;

var btnAjouterAdresse, btnEnregistrerUnite;

var
        codeOfPersonne,
        nameOfPersonne,
        codeActivite;

var tempTypeUniteList = [];

var codeActiviteList = [];

var eaList = [];
var activitesList = [];
var tempActiviteList = [];

var idOfUniteEconomique = '',
        selectedCodeAP = '',
        codeOfTypeBien = '';

$(function () {

    modalEditerTypeBien = $('#modalEditerTypeBien');
    modalAdressesPersonne = $('#modalAdressesPersonne');
    divBtnAdresseControlUnite = $('#divBtnAdresseControlUnite');

    cmbTypeBienUnite = $('#cmbTypeBienUnite');
    cmbCommuneUnite = $('#cmbCommuneUnite');
    cmbQuartierUnite = $('#cmbQuartierUnite');
    cmbActiviteUnite = $('#cmbActiviteUnite');

    inputIntituleUnite = $('#inputIntituleUnite');
    inputDescriptionUnite = $('#inputDescriptionUnite');
    inputDateAcquissitionUnite = $('#inputDateAcquissitionUnite');
    inputAdresse = $('#inputAdresse');

    btnAjouterAdresse = $('#btnAjouterAdresse');
    btnEnregistrerUnite = $('#btnEnregistrerUnite');

    inputDescriptionUnite.val('');

    codeActiviteList = '';


    cmbCommuneUnite.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbCommuneUnite.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une commune valide');
            return;

        } else {

            for (var i = 0; i < eaList.length; i++) {

                if (eaList[i].code == cmbCommuneUnite.val()) {

                    var listQuartier = eaList[i].quartierList;

                    var dataQuartier = '<option value ="0">-- Sélectionner --</option>';

                    for (var j = 0; j < listQuartier.length; j++) {

                        if (listQuartier[j].communeCode == cmbCommuneUnite.val()) {

                            dataQuartier += '<option value =' + listQuartier[j].quartierCode + '>' + listQuartier[j].quartierName + '</option>';
                        }
                    }

                    cmbQuartierUnite.html(empty);
                    cmbQuartierUnite.html(dataQuartier);

                }
            }

        }
    });

    btnEnregistrerUnite.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnAjouterAdresse.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codePersonne = codeResponsible;
        nomPersonne = nameOfPersonne;

        loadAdressesOfPersonne();

        modalAdressesPersonne.modal('show');

    });

//    cmbActiviteUnite.on('change', function () {
//
//        codeActivite = cmbActiviteUnite.val();
//
//        if (!isUndefined(codeActivite)) {
//            if (!checkActiviteExist(codeActivite)) {
//                dataCodeActivite(codeActivite);
//                cmbActiviteUnite.trigger("chosen:updated");
//            }
//        }
//
//    });

    initDataUnite();
});

function initDataUnite() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'initData',
            'codeService': userData.serviceCode,
            'type': 5
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucune type unité trouvée.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                tempTypeUniteList = [];

                var typeBienList = result.typeBienList;

                eaList = result.eaList;
                activitesList = result.activitesList;

                var dataTypeBien = '';

                for (var i = 0; i < typeBienList.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = typeBienList[i].codeTypeBien;
                    typeBien.libelleTypeBien = typeBienList[i].libelleTypeBien;
                    typeBien.estContractuel = typeBienList[i].estContractuel;
                    tempTypeUniteList.push(typeBien);

                    dataTypeBien += '<option value =' + typeBienList[i].codeTypeBien + '>' + typeBienList[i].libelleTypeBien + '</option>';
                }

                cmbTypeBienUnite.html(dataTypeBien);

                var dataCommuneBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < eaList.length; i++) {

                    dataCommuneBien += '<option value =' + eaList[i].code + '>' + eaList[i].communeComposite + '</option>';
                }

                cmbCommuneUnite.html(dataCommuneBien);

                var dataActivites = '';

                for (var i = 0; i < activitesList.length; i++) {
                    dataActivites += '<option value="' + activitesList[i].id + '"  >' + activitesList[i].intitule + '</option>';
                }

                cmbActiviteUnite.html(dataActivites);
                cmbActiviteUnite.trigger("chosen:updated");

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkFields() {

    if (cmbTypeBienUnite.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner la nature de l\'unité économique');
        return;
    }

    if (inputIntituleUnite.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir la dénomination de l\'unité économique');
        return;
    }

    if (cmbActiviteUnite.val() === null) {
        alertify.alert('Veuillez d\'abord sélectionner un ou plusieurs activités de l\'unité économique');
        return;
    }

    if (cmbCommuneUnite.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner la commune de l\'unité économique');
        return;
    }

    if (cmbQuartierUnite.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le quartier de l\'unité économique');
        return;
    }



//    if (inputDateAcquissitionUnite.val().trim() === '') {
//        alertify.alert('Veuillez d\'abord saisir la date d\'acquisition de l\'unité économique');
//        return;
//    }

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cet unité économique ?', function () {
        saveUniteEconomique();
    });
}

function getSelectedDataUnite() {

//    inputAdresse.val(selectedAdressePersonne.chaine);
//    selectedCodeAP = selectedAdressePersonne.codeAP;
    codeOfPersonne = codeResponsible;
    nameOfPersonne = nameResponsible;
}

function getSelectedAdressePersonneData() {

    inputAdresse.val(selectedAdressePersonne.chaine);
    selectedCodeAP = selectedAdressePersonne.codeAP;
}

function saveUniteEconomique() {

    var listActivities = cmbActiviteUnite.val();

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveUniteEconomique',
            'idUniteEconomique': idOfUniteEconomique,
            'codeTypeUnite': cmbTypeBienUnite.val(),
            'codePersonne': codeOfPersonne,
            'intituleUnite': inputIntituleUnite.val(),
            'codeCommune': cmbCommuneUnite.val(),
            'codeQuartier': cmbQuartierUnite.val(),
            'dateAcquisitionUnite': inputDateAcquissitionUnite.val(),
            'descriptionUnite': inputDescriptionUnite.val(),
            'codeAP': selectedCodeAP,
            'idActiviteList': listActivities.toString(),
            'idBienActivite': 0,
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du bien ...</h5>'});
            modalCreateUniteEconomique.modal('hide');

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'enregistrement du bien s\'est effectué avec succès.');
                    cleanData();
                    getBienPersonne();
                    initDataUnite();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de l\'unité économique.');
                    modalCreateUniteEconomique.modal('show');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }
    });
}

function cleanData() {
    idOfUniteEconomique = '';
    inputIntituleUnite.val('');
    inputDateAcquissitionUnite.val('');
    inputDescriptionUnite.val('');
    inputAdresse.val('');
    cmbCommuneUnite.val(empty);
    cmbQuartierUnite.val(empty);
    cmbActiviteUnite.val(empty);
    tempActiviteList = [];
}

function dataCodeActivite(codeActivite) {

    for (var i = 0; i < codeActivite.length; i++) {

        for (var j = 0; j < activitesList.length; j++) {

            var activiteObject = {};
            if (activitesList[j].id == codeActivite[i]) {
                activiteObject.idActivite = activitesList[j].id;
                activiteObject.intitule = activitesList[j].intitule;
                tempActiviteList.push(activiteObject);
            }
        }

    }
}

function checkActiviteExist(activiteId) {

    var isInTempList = false;

    for (var i = 0; i < tempActiviteList.length; i++) {

        if (tempActiviteList[i].id == activiteId) {

            isInTempList = true;
            break;
        }
    }
    return isInTempList;

}