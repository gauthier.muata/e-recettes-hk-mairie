/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnCreateUniteEconomique, btnSearchAssujettiUnite,
        btnSearchUniteEconomique, btnShowAdvancedSearchModal,
        btnAdvencedSearch;

var inputSearchUniteEconomique, inputdateLast, inputDateDebut;

var cbxResearchTypeUniteEcono;

var tableUniteEconomique;

var modalCreateUniteEconomique;

var tempUniteEconomList, tempAssujettiList, tempActiviteList;

var codeResearchUnite, isAdvance, lblDateDebut, lblDateFin;

var checkLoad = true;

$(function () {

    mainNavigationLabel.text('ORDONNANCEMENT');
    secondNavigationLabel.text('Registre des unités économiques');

    tableUniteEconomique = $('#tableUniteEconomique');

    modalCreateUniteEconomique = $('#modalCreateUniteEconomique');

    btnCreateUniteEconomique = $('#btnCreateUniteEconomique');
    btnSearchUniteEconomique = $('#btnSearchUniteEconomique');
    btnShowAdvancedSearchModal = $('#btnShowAdvancedSearchModal');
    btnAdvencedSearch = $('#btnAdvencedSearch');

    inputSearchUniteEconomique = $('#inputSearchUniteEconomique');
    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');
    
    isAdvance = $('#isAdvance');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    cbxResearchTypeUniteEcono = $('#cbxResearchTypeUniteEcono');

    codeResearchUnite = "0";

    btnCreateUniteEconomique.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalCreateUniteEconomique.modal('show');
    });

    btnSearchUniteEconomique.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadUniteEconomique();

    });

    cbxResearchTypeUniteEcono.on('change', function (e) {

        e.preventDefault();

        codeResearchUnite = cbxResearchTypeUniteEcono.val();
        checkLoad = true;

        if (codeResearchUnite === "1") {
            inputSearchUniteEconomique.attr('placeholder', 'Le nom de l\'assujetti');
            inputSearchUniteEconomique.val('');
        } else {
            inputSearchUniteEconomique.attr('placeholder', 'Unité économique');
            inputSearchUniteEconomique.val('');
        }
    });
    
     btnShowAdvancedSearchModal.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    })
    
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeNC.modal('hide');
        loadUniteEconomique(true);
    });

    printUniteEconomique('');
    loadUniteEconomique(false);

    if (checkLoad) {
        codeResearchUnite = cbxResearchTypeUniteEcono.val();
        checkLoad = false;
    }

});

function loadUniteEconomique(checkAdvance) {
    
    if (checkAdvance) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());
        isAdvance.attr('style', 'display: block');

    } else {

        isAdvance.attr('style', 'display: none');
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputSearchUniteEconomique.val(),
            'typeSearch': codeResearchUnite,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'isAdvancedSearch': checkAdvance,
            'operation': 'loadUniteEconomique'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                tempAssujettiList = null;

                tempAssujettiList = JSON.parse(JSON.stringify(response));

                printUniteEconomique(tempAssujettiList);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

var codePersonne = '';
var labelPersonne = '';

function printUniteEconomique(tempAssujettiList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" style="width:30%"> ASSUJETTI </th>';
    header += '<th scope="col" style="width:15%"> TYPE ASSUJETTI </th>';
    header += '<th scope="col" style="width:10%"> NTD </th>';
    header += '<th scope="col" style="width:30%"> ADRESSE </th>';
    header += '<th hidden="true" scope="col"> Code Personne </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyBien">';

    for (var i = 0; i < tempAssujettiList.length; i++) {
        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="vertical-align:middle;">' + tempAssujettiList[i].responsable + '</td>';
        body += '<td style="vertical-align:middle;">' + tempAssujettiList[i].formeJuridique + '</td>';
        body += '<td style="vertical-align:middle;">' + tempAssujettiList[i].userName + '</td>';
        body += '<td style="vertical-align:middle;">' + tempAssujettiList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td hidden="true">' + tempAssujettiList[i].codePersonne + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableUniteEconomique.html(tableContent);

    var tableDetailActivite = tableUniteEconomique.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par le nom _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableUniteEconomique tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = tableDetailActivite.row(tr);
        var dataDetail = tableDetailActivite.row(tr).data();
        codePersonne = dataDetail[5];
        labelPersonne = dataDetail[1];

        console.log(labelPersonne);

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        }

        else {

            var detailTable = '<center><h4>LES UNITES ECONOMIQUES </h4><br/></center>';
            detailTable += '<table class="table table-bordered">';
            detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
            detailTable += '<td style="text-align:center">INTITULE</td>';
            detailTable += '<td style="text-align:center">ACTIVITIES</td>';
            detailTable += '<th scope="col"> </th>';
            detailTable += '</tr></thead>';
            detailTable += '<tbody>';

            for (var i = 0; i < tempAssujettiList.length; i++) {

                if (tempAssujettiList[i].codePersonne === codePersonne) {

                    tempUniteEconomList = JSON.parse(tempAssujettiList[i].uniteEconomiqueList);

                    for (var j = 0; j < tempUniteEconomList.length; j++) {

                        tempActiviteList = JSON.parse(tempUniteEconomList[j].bienActiviteList);

                        detailTable += '<tr>';
                        detailTable += '<td style="text-align:center;width:30%;">' + tempUniteEconomList[j].intituleBien + '</td>';

                        var labelActivities = '';

                        for (var k = 0; k < tempActiviteList.length; k++) {

                            if (k == 0) {
                                labelActivities += tempActiviteList[k].intituleActivite;
                            } else {
                                labelActivities += ', ' + tempActiviteList[k].intituleActivite;
                            }

                        }

                        detailTable += '<td style="text-align:center;width:60%;">' + labelActivities + '</td>';

                        if (tempUniteEconomList[j].etat) {
                            detailTable += '<td style="vertical-align:middle;text-align:center"><a onclick="taxerUniteEconomique(\'' + tempUniteEconomList[j].idBien + '\',\'' + tempUniteEconomList[j].intituleBien + '\')" class="btn btn-success" title="Taxation"><i class="fa fa-calculator"></i>&nbsp;&nbsp;Taxer</a></td>';
                        } else {
                            detailTable += '<td style="vertical-align:middle;text-align:center"><a onclick="validerUniteEconomique(\'' + tempUniteEconomList[j].idBien + '\')" class="btn btn-success" title="Valider unité économique"><i class="fa fa-check"></i>&nbsp;Valider</a></td>';
                        }
                        detailTable += '</tr>';
                    }



                    break;

                }
            }

            detailTable += '</tbody></table>';

            row.child(detailTable).show();
            tr.addClass('shown');
        }

    });
}

function validerUniteEconomique(idUniteEcono) {
    
    
    for (var i = 0; i < tempUniteEconomList.length; i++) {

        if (tempUniteEconomList[i].idBien == idUniteEcono) {
            alertify.confirm('Voulez-vous valider l\'unité économique :  ' + tempUniteEconomList[i].intituleBien + ' ?', function () {
                validerUniteEconomiques(idUniteEcono);
            });
            break;
        }
    }
}

function taxerUniteEconomique(id, label) {

    alertify.confirm('Voulez-vous taxer le bien suivant :  ' + label + ' ?', function () {
        window.open('taxation-ordinaire?a=' + btoa(id) + '&b=' + btoa(codePersonne) + '&c=' + btoa(labelPersonne) + '&d=' + btoa(label), '_blank');
    });


}

function validerUniteEconomiques(idBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'validerUniteEcono',
            'idBien': idBien,
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation unité économique ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('La validation d\'unité économique s\'est effectuée avec succès.');
                    loadUniteEconomique();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la validation de l\'unité économique.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }
    });
}


