/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lblNameGestionnaire, lblFonctionGestionnaire;

var btnCallModalSearchGestionnaire, gestionnaireModal, modalSelectAssujetti;

var btnSelectGestionnaire, btnSelectModalAssujetti, 
        btnDesacterAssujetti;

var codeGestionnaire, codeFonction;

var tableAssujetti;

var lbl1, lbl2;

var tempPersonneList;

$(function () {

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text('Gestionnaire des assujettis');

    removeActiveMenu();
    linkSubMenuGestionnaireAssujettis.addClass('active');

    lblNameGestionnaire = $('#lblNameGestionnaire');
    lblFonctionGestionnaire = $('#lblFonctionGestionnaire');

    btnCallModalSearchGestionnaire = $('#btnCallModalSearchGestionnaire');
    gestionnaireModal = $('#gestionnaireModal');
    modalSelectAssujetti = $('#modalSelectAssujetti');

    tableAssujetti = $('#tableAssujetti');

    btnSelectGestionnaire = $('#btnSelectGestionnaire');
    btnSelectModalAssujetti = $('#btnSelectModalAssujetti');
    btnDesaffecterAssujetti = $('#btnDesaffecterAssujetti');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');


    btnCallModalSearchGestionnaire.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        gestionnaireModal.modal('show');
    });

    btnSelectModalAssujetti.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        codeGestionnaire = selectGestionnaireData.code_agent;
        modalSelectAssujetti.modal('show');
    });

    btnSelectGestionnaire.click(function (e) {

        if (selectGestionnaireData.code_agent === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un gestionnaire');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir sélectionner <b>' + selectGestionnaireData.nomComplet + '</b> ?', function () {
            getSelectedGestionnaoreData();
            loadAssujettisAffecter(selectGestionnaireData.code_agent);
            gestionnaireModal.modal('hide');
        });
    });
    
    btnDesaffecterAssujetti.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        alertify.confirm('Etes-vous de vouloir désaffecter cet assujetti ?', function () {

            var assujettiLists = preparerDesaffectation();
            desaffecterAssujetti(assujettiLists);

        });
    });


});

function getSelectedGestionnaoreData() {

    lblNameGestionnaire.html(selectGestionnaireData.nomComplet);
    lblFonctionGestionnaire.html(selectGestionnaireData.fonction);
    codeGestionnaire = selectGestionnaireData.code_agent;
    odeFonction = selectGestionnaireData.codeFonction;

    lbl1.show();
    lbl2.show();

    gestionnaireObject = new Object();

    gestionnaireObject.code_agent = codeGestionnaire;
    gestionnaireObject.codeFonction = odeFonction;
}

function getCodeGestionnaire() {
    return codeGestionnaire;
}

function loadAssujettisAffecter(codeGestionnaire) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujettiAffecter',
            'codeGestionnaire': codeGestionnaire
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                tempPersonneList = JSON.parse(JSON.stringify(response));
                
                if (tempPersonneList.length > 0){
                    btnDesaffecterAssujetti.show();
                }else{
                    btnDesaffecterAssujetti.hide();
                }

                printAssujetti(tempPersonneList);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printAssujetti(tempPersonneList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%" > Nif </th>';
    header += '<th style="width:25%"> Assujetti </th>';
    header += '<th style="width:13%"> Catégorie </th>';
    header += '<th style="width:10%"> Téléphone </th>';
    header += '<th hidden="true" > Email </th>';
    header += '<th style="width:30%" > Adresse principale</th>';
    header += '<th style="width:10%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code personne </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempPersonneList.length; i++) {
        body += '<tr>';

        body += '<td>' + tempPersonneList[i].nif + '</td>';
        body += '<td>' + tempPersonneList[i].nomComplet + '</td>';
        body += '<td>' + tempPersonneList[i].libelleFormeJuridique + '</td>';
        body += '<td>' + tempPersonneList[i].telephone + '</td>';
        body += '<td hidden="true">' + tempPersonneList[i].email + '</td>';
        body += '<td>' + tempPersonneList[i].chaine + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxDesacffecterAssujetti' + tempPersonneList[i].codePersonne + '" name="checkBoxDesacffecterAssujetti' + tempPersonneList[i].codePersonne + '"></center></td>';
        body += '<td hidden="true">' + tempPersonneList[i].codePersonne + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableAssujetti.html(tableContent);

    tableAssujetti.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function preparerDesaffectation() {

    assujettiList = [];

    for (var i = 0; i < tempPersonneList.length; i++) {

        var desaffecterAssujetti = '#checkBoxDesacffecterAssujetti' + tempPersonneList[i].codePersonne;

        var select = $(desaffecterAssujetti);

        if (select.is(':checked')) {

            var assujetti = {};

            assujetti.codePersonne = tempPersonneList[i].codePersonne;
            assujetti.nif = tempPersonneList[i].nif;

            assujettiList.push(assujetti);

        }

    }

    assujettiList = JSON.stringify(assujettiList);

    return assujettiList;
}

function desaffecterAssujetti(assujettiList) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desaffecterAssujetti',
            'codeGestionnaire': selectGestionnaireData.code_agent,
            'assujettiList': assujettiList

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des assujettis en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('La désaffectation des assujettis s\'est effectué avec succès.');
                    loadAssujettisAffecter(codeGestionnaire);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désaffectation des assujettis.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

