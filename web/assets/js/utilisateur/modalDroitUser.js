/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalDroitsUser;

var lblNomComplet, lblFonction, lblMatricule;

var btnSearchDroit, btnAjouterDoit, btnRetirerDroit;

var cmbSearchTypeDroit;

var inputSearchDroit;

var tableDroitAffecter, tableIntituleDroit;

var codeResearchDroit, messageEmptyValueDroit, codeAgent;

var tempDroitList, tempDroitAffecterList;

var cmbModules, codeResearchModules;


$(function () {

    modalDroitsUser = $('#modalDroitsUser');

    tableDroitAffecter = $('#tableDroitAffecter');
    tableIntituleDroit = $('#tableIntituleDroit');

    lblNomComplet = $('#lblNomComplet');
    lblFonction = $('#lblFonction');
    lblMatricule = $('#lblMatricule');

    btnSearchDroit = $('#btnSearchDroit');
    btnRetirerDroit = $('#btnRetirerDroit');
    btnAjouterDoit = $('#btnAjouterDoit');

    cmbSearchTypeDroit = $('#cmbSearchTypeDroit');
    cmbModules = $('#cmbModules');

    inputSearchDroit = $('#inputSearchDroit');

    codeResearchDroit = cmbSearchTypeDroit.val();

    btnSearchDroit.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
//        if (inputSearchDroit.val().trim() === empty ||
//                inputSearchDroit.val().length < SEARCH_MIN_TEXT) {
//            showEmptySearchMessage();
//            return;
//        }
        loadDroits();
        loadDroitsAffecterUser();
    });

    cmbSearchTypeDroit.on('change', function (e) {
        codeResearchDroit = cmbSearchTypeDroit.val();
        inputSearchDroit.val('');

        if (codeResearchDroit === '0') {
            messageEmptyValueDroit = 'Veuillez saisir l\'intitulé.';
            inputSearchDroit.attr('placeholder', 'L\'intitulé du droit');
        } else {
            messageEmptyValueDroit = 'Veuillez saisir le code du droit.';
            inputSearchDroit.attr('placeholder', 'Code du droit');
        }

        printDroits('');
        printDroitAffecter('');
    });

    btnAjouterDoit.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        alertify.confirm('Etes-vous de vouloir affecter ce droit ?', function () {

            var droitLists = preparerAffecterDroit();
            affecterDroit(droitLists);

        });
    });

    btnRetirerDroit.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        alertify.confirm('Etes-vous de vouloir retirer ce droit ?', function () {

            var droitListsRetirer = preparerDesaffecterDroit();
            retirerDroit(droitListsRetirer);

        });
    });

    cmbModules.on('change', function (e) {
        codeResearchModules = cmbModules.val();
        
        loadDroits();
        loadDroitsAffecterUser();
    });

    loadModule();
//    printDroitAffecter('');
//    printDroits('');
//    resetFieldsDroit();

});

function afficherInfosUse(objetUser) {
    lblNomComplet.html(objetUser.nom_agent + " " + objetUser.prenom_agent);
    lblMatricule.html(objetUser.matricule);
    lblFonction.html(objetUser.libelleFonction);
    codeAgent = objetUser.code_agent;
}

function loadDroits() {


    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDroits',
            'typeSearch': codeResearchDroit,
            'idUser': userData.idUser,
            'code_agent': codeAgent,
            'codeModule': codeResearchModules,
            'libelle': inputSearchDroit.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var droitList = JSON.parse(JSON.stringify(response));

                tempDroitList = [];

                for (var i = 0; i < droitList.length; i++) {
                    var droit = new Object();
                    droit.code_droit = droitList[i].code_droit;
                    droit.intitule_droit = droitList[i].intitule_droit;
                    droit.etat = droitList[i].etat;

                    tempDroitList.push(droit);
                }

                printDroits(tempDroitList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadDroitsAffecterUser() {


    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDroitAffecterUser',
            'typeSearch': codeResearchDroit,
            'code_agent': codeAgent,
            'codeModule': codeResearchModules,
            'libelle': inputSearchDroit.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var droitAffecterList = JSON.parse(JSON.stringify(response));

                tempDroitAffecterList = [];

                for (var i = 0; i < droitAffecterList.length; i++) {
                    var droit = new Object();
                    droit.code_droit = droitAffecterList[i].code_droit;
                    droit.intitule_droit = droitAffecterList[i].intitule_droit;
                    droit.etat = droitAffecterList[i].etat;

                    tempDroitAffecterList.push(droit);
                }

                printDroitAffecter(tempDroitAffecterList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDroits(tempDroitsList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:87%" >INTITULE DROITS</th>';
    header += '<th style="width:3%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code droit </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyDroits">';

    for (var i = 0; i < tempDroitsList.length; i++) {

        var firstLineDroit = '';
        var intitule_droit = tempDroitsList[i].intitule_droit;
//        console.log(intitule_droit);
        if (intitule_droit.length > 75) {
            firstLineDroit = intitule_droit.substring(0, 72) + ' ...';
        } else {
            firstLineDroit = intitule_droit;
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;title="' + intitule_droit + '"><span style="font-weight:bold;"></span>' + firstLineDroit + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxSelectDroit' + tempDroitsList[i].code_droit + '" name="checkBoxSelectDroit' + tempDroitsList[i].code_droit + '"></center></td>';
        body += '<td hidden="true">' + tempDroitsList[i].code_droit + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableIntituleDroit.html(tableContent);

    tableIntituleDroit.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}


function printDroitAffecter(tempDroitAffecterList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:97%" >DROITS AFFECTES A L\'UTILISATEUR</th>';
    header += '<th style="width:3%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code droit </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyDroitsAffecter">';

    for (var i = 0; i < tempDroitAffecterList.length; i++) {

        var firstLineDroit = '';

        if (tempDroitAffecterList[i].intitule_droit.length > 75) {
            firstLineDroit = tempDroitAffecterList[i].intitule_droit.substring(0, 72) + ' ...';
        } else {
            firstLineDroit = tempDroitAffecterList[i].intitule_droit;
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;title="' + tempDroitAffecterList[i].intitule_droit + '"><span style="font-weight:bold;"></span>' + firstLineDroit + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxSelectDroitAffect' + tempDroitAffecterList[i].code_droit + '" name="checkBoxSelectDroitAffect' + tempDroitAffecterList[i].code_droit + '"></center></td>';
        body += '<td hidden="true">' + tempDroitAffecterList[i].code_droit + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableDroitAffecter.html(tableContent);

    tableDroitAffecter.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function resetFieldsDroit() {

    inputSearchDroit.val('');
//    lblFonction.html('');
    //    cmbFonction.val(0);
    printDroits('');
    printDroitAffecter('');
}

function preparerAffecterDroit() {

    droitList = [];

    for (var i = 0; i < tempDroitList.length; i++) {

        var affecterDroit = '#checkBoxSelectDroit' + tempDroitList[i].code_droit;

        var select = $(affecterDroit);

        if (select.is(':checked')) {

            var droit = {};

            droit.code_droit = tempDroitList[i].code_droit;
            droit.intitule_droit = tempDroitList[i].intitule_droit

            droitList.push(droit);

        }

    }

    droitList = JSON.stringify(droitList);

    return droitList;
}

function preparerDesaffecterDroit() {

    desaffecterDroitList = [];

    for (var i = 0; i < tempDroitAffecterList.length; i++) {

        var affecterDroit = '#checkBoxSelectDroitAffect' + tempDroitAffecterList[i].code_droit;

        var select = $(affecterDroit);

        if (select.is(':checked')) {

            var droit = {};

            droit.code_droit = tempDroitAffecterList[i].code_droit;

            desaffecterDroitList.push(droit);

        }

    }

    desaffecterDroitList = JSON.stringify(desaffecterDroitList);

    return desaffecterDroitList;
}

function affecterDroit(droitList) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterDroit',
            'code_agent': codeAgent,
            'idUser': userData.idUser,
            'droitList': droitList

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affectation des droits en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\affectation des droits s\'est effectué avec succès.');
                    loadDroits();
                    loadDroitsAffecterUser();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'affectation des droits.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function retirerDroit(droitListRetirer) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'retirerDroit',
            'code_agent': codeAgent,
            'droitList': droitListRetirer

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des assujettis en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('Le retrait des droits s\'est effectué avec succès.');
                    loadDroits();
                    loadDroitsAffecterUser();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors du retrait des droits.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadModule() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadModule'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            var moduleList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un module ---</option>';

            for (var i = 0; i < moduleList.length; i++) {
                data += '<option value="' + moduleList[i].codeModule + '">' + moduleList[i].intituleModule + '</option>';
            }
            cmbModules.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}