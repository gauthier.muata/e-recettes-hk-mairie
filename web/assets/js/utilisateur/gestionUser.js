/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbSearchTypeGestionUser;

var inputSearchGestionUser;

var btnSearchGestionUser, btnEditerUser, btnDisabledUser;

var btnCreateGestionUser, btnAffecterDroit;

var idRechercerAvancesGestionUser;

var tableGestionUser, modalDroitsUser;

var codeResearchGestionUser, messageEmptyValueGestionUser, messageAvanceeGestionUser;

var tempUserList;


$(function () {

    mainNavigationLabel.text('UTILISATEUR');
    secondNavigationLabel.text('Affectaion / Désaffectaion des droits à des utilisateurs');

    removeActiveMenu();
    linkSubMenuGestionUser.addClass('active');

    tableGestionUser = $('#tableGestionUser');
    modalDroitsUser = $('#modalDroitsUser');

//    idRechercerAvancesGestionUser = $('#idRechercerAvancesGestionUser');

    btnCreateGestionUser = $('#btnCreateGestionUser');
    btnSearchGestionUser = $('#btnSearchGestionUser');
    btnAffecterDroit = $('#btnAffecterDroit');
    btnEditerUser = $('#btnEditerUser');
    btnDisabledUser = $('#btnDisabledUser');

    inputSearchGestionUser = $('#inputSearchGestionUser');

    cmbSearchTypeGestionUser = $('#cmbSearchTypeGestionUser');

    codeResearchGestionUser = cmbSearchTypeGestionUser.val();

    btnSearchGestionUser.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadUsers();
    });

    btnEditerUser.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        var objetUser = preparerAffectationDroit();
        editUser(objetUser.code_agent);
    });

    btnDisabledUser.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        var objetUser = preparerAffectationDroit();
        disabledUser(objetUser.code_agent);
    });

    cmbSearchTypeGestionUser.on('change', function (e) {
        codeResearchGestionUser = cmbSearchTypeGestionUser.val();
        inputSearchGestionUser.val('');

        if (codeResearchGestionUser === '0') {
            messageEmptyValueGestionUser = 'Veuillez saisir le nom de l\'utilisateur.';
            inputSearchGestionUser.attr('placeholder', 'Nom de l\'utilisateur');
        } else {
            messageEmptyValueGestionUser = 'Veuillez saisir la fonction de l\'utilisateur.';
            inputSearchGestionUser.attr('placeholder', 'Fonction de l\'utilisateur');
        }

        printUsers('');
    });

    btnAffecterDroit.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var objetUser = preparerAffectationDroit();

        alertify.confirm('Voulez-vous affecter le(s) droit(s) à  ' + objetUser.nom_agent + ' ?', function () {
            resetFieldsDroit();
            afficherInfosUse(objetUser);
            loadDroits();
            loadDroitsAffecterUser();
            modalDroitsUser.modal('show');
        });

    });

    btnCreateGestionUser.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        window.location = 'gestion-utilisateur';
    });

    printUsers('');

});

function loadUsers() {

    if (inputSearchGestionUser.val().trim() === empty ||
            inputSearchGestionUser.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadUser',
            'typeSearch': codeResearchGestionUser,
            'codeFonction': userData.codeFonction,
            'userId': userData.idUser,
            'libelle': inputSearchGestionUser.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var userList = JSON.parse(JSON.stringify(response));

                tempUserList = [];
                for (var i = 0; i < userList.length; i++) {
                    var user = new Object();
                    user.code_agent = userList[i].code_agent;
                    user.nom_agent = userList[i].nom_agent;
                    user.prenom_agent = userList[i].prenom_agent;
                    user.libelleFonction = userList[i].libelleFonction;
                    user.codeFonction = userList[i].codeFonction;
                    user.codeService = userList[i].codeService;
                    user.libelleService = userList[i].libelleService;
                    user.libelleUa = userList[i].libelleUa;
                    user.codeUa = userList[i].codeUa;
                    user.login = userList[i].login;
                    user.matricule = userList[i].matricule;
                    user.codeSite = userList[i].codeSite;
                    user.libelleSite = userList[i].libelleSite;
                    user.codeDistrict = userList[i].codeDistrict;
                    user.libelleDistrinct = userList[i].libelleDistrinct;
                    user.grade = userList[i].grade;
                    tempUserList.push(user);
                }

                printUsers(tempUserList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printUsers(tempUserList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:23%" >NOM</th>';
    header += '<th style="width:17%">PRENOM</th>';
    header += '<th style="width:10%" >MATRICULE</th>';
    header += '<th style="width:10%">FONCTION</th>';
    header += '<th style="width:15%">SERVICE</th>';
    header += '<th style="width:20%" >GRADE</th>';
    header += '<th style="width:5%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code agent </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempUserList.length; i++) {
        body += '<tr>';
        body += '<td>' + tempUserList[i].nom_agent + '</td>';
        body += '<td>' + tempUserList[i].prenom_agent + '</td>';
        body += '<td>' + tempUserList[i].matricule + '</td>';
        body += '<td>' + tempUserList[i].libelleFonction + '</td>';
        body += '<td>' + tempUserList[i].libelleService + '</td>';
        body += '<td>' + tempUserList[i].grade + '</td>';
        body += '<td style="text-align:center"><center><input onclick="checkUser(\'' + tempUserList[i].code_agent + '\')" type="checkbox" id="checkBoxSelectUser' + tempUserList[i].code_agent + '" name="checkBoxSelectUser' + tempUserList[i].code_agent + '"></center></td>';
        body += '<td hidden="true">' + tempUserList[i].code_agent + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableGestionUser.html(tableContent);

    tableGestionUser.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function preparerAffectationDroit() {

    userList = [];

    for (var i = 0; i < tempUserList.length; i++) {

        var selectAssujetti = '#checkBoxSelectUser' + tempUserList[i].code_agent;

        var select = $(selectAssujetti);

        if (select.is(':checked')) {

            var userObjet = new Object();

            userObjet.code_agent = tempUserList[i].code_agent;
            userObjet.nom_agent = tempUserList[i].nom_agent;
            userObjet.prenom_agent = tempUserList[i].prenom_agent;
            userObjet.matricule = tempUserList[i].matricule;
            userObjet.libelleFonction = tempUserList[i].libelleFonction;

        }

    }

    return userObjet;
}

function checkUser(code_agent) {

    for (var i = 0; i < tempUserList.length; i++) {
        if (code_agent != tempUserList[i].code_agent) {
            var control = document.getElementById("checkBoxSelectUser" + tempUserList[i].code_agent);
            control.checked = false;
        }
    }
}

function editUser(codeAgent) {

    for (var i = 0; i < tempUserList.length; i++) {

        if (tempUserList[i].code_agent == codeAgent) {
            var nomComplet = tempUserList[i].nom_agent + " " + tempUserList[i].prenom_agent;
            alertify.confirm('Voulez-vous modifier les informations de ' + nomComplet + ' ?', function () {
                window.location = 'gestion-utilisateur?id=' + codeAgent;
            });
            break;
        }
    }
}

function disabledUser(codeAgent) {

    for (var i = 0; i < tempUserList.length; i++) {

        if (tempUserList[i].code_agent == codeAgent) {
            var nomComplet = tempUserList[i].nom_agent + " " + tempUserList[i].prenom_agent;
            alertify.confirm('Voulez-vous désactiver ' + nomComplet + ' ?', function () {
                validateDisabledUser(codeAgent);
            });
            break;
        }
    }
}

function validateDisabledUser(codeUser) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disabledUser',
            'code_agent': codeUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'assujetti ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'utilisateur est désactivé avec succès');
                    btnSearchGestionUser.trigger('click');

                } else if (response == '0') {
                    alertify.alert('Echec désactivation de l\'utilisateur.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}