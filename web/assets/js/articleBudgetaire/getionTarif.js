/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableResultTarifs;

var selectTarifData;

var cmbTypeValeutTarif, btnEnregistrerTarif, btnModifierTarif, btnInitialiser;

var typeValeur, codeTarifs;

var inputIntituleTarif, inputValeurTarif, cmbTypeTarif;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Edition des tarifs');

    tableResultTarifs = $('#tableResultTarifs');

    cmbTypeValeutTarif = $('#cmbTypeValeutTarif');

    btnModifierTarif = $('#btnModifierTarif');
    btnEnregistrerTarif = $('#btnEnregistrerTarif');
    btnInitialiser = $('#btnInitialiser');

    inputIntituleTarif = $('#inputIntituleTarif');
    inputValeurTarif = $('#inputValeurTarif');
    cmbTypeTarif = $('#cmbTypeTarif');

    typeValeur = cmbTypeValeutTarif.val();

    cmbTypeValeutTarif.on('change', function (e) {

        typeValeur = cmbTypeValeutTarif.val();
    });

    btnEnregistrerTarif.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntituleTarif.val() == '' || inputIntituleTarif.val() == 'null') {
            alertify.alert('Veuillez saisir l\'intitulé du tarif');
            return;
        }

        if (cmbTypeTarif.val() == '-1') {
            alertify.alert('Veuillez d\'abord sélectionner le type du tarif');
            return;
        }


        if (inputValeurTarif.val() == '' || inputValeurTarif.val() == 'null' || inputValeurTarif.val() == '0') {
            alertify.alert('Veuillez saisir la valeur du tarif');
            return;
        }
        if (typeValeur == '' || typeValeur == 'null' || typeValeur == '0') {
            alertify.alert('Veuillez selectionner un module.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce tarif ?', function () {

            saveTarif();

        });
    });

    btnInitialiser.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir réinitialiser ces champs de saisie de tarif ?', function () {

            inputIntituleTarif.val('');
            inputValeurTarif.val('');
            cmbTypeValeutTarif.val('0');
            typeValeur = '';
            codeTarifs = '';
            cmbTypeTarif.val('-1');
            btnEnregistrerTarif.attr('style', 'display:inline; right: 38%');
            btnModifierTarif.attr('style', 'display:none');

        });
    });

    btnModifierTarif.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntituleTarif.val() == '' || inputIntituleTarif.val() == 'null') {
            alertify.alert('Veuillez saisir l\'intitulé du tarif');
            return;
        }

        if (cmbTypeTarif.val() == '-1') {
            alertify.alert('Veuillez d\'abord sélectionner le type du tarif');
            return;
        }

        if (inputValeurTarif.val() == '' || inputValeurTarif.val() == 'null' || inputValeurTarif.val() == '0') {
            alertify.alert('Veuillez saisir la valeur du tarif');
            return;
        }
        if (typeValeur == '' || typeValeur == 'null' || typeValeur == '0') {
            alertify.alert('Veuillez selectionner un module.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir modifier ce tarif ?', function () {
            updateTarif();
        });
    });



    loadTarif();
});


function loadTarif() {


    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'userId': userData.idUser,
            'operation': 'loadTarifs'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var tarifList = JSON.parse(JSON.stringify(response));

                printTarif(tarifList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printTarif(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="width:50%">LIBELLE TARIF</th>';
    tableContent += '<th style="width:19%;text-align:center">VALEUR TARIF</th>';
    tableContent += '<th style="width:19%">TYPE TARIF</th>';
    tableContent += '<th style="width:5%; display:none"></th>';
    tableContent += '<th style="width:5%"></th>';
    tableContent += '<th style="width:2%; display:none"></th>';
    tableContent += '<th style="width:2%; display:none"></th>';
    tableContent += '<th style="width:2%; display:none"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:60%">' + data[i].intituleTarif.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center;width:15%">' + data[i].valeurBase + ' ' + data[i].TYPE_VALEUR_CODE + '</td>';
        tableContent += '<td style="text-align:left;width:20%">' + data[i].TYPE_TARIF_NAME.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].valeurType + '</td>';
        tableContent += '<td style="width:5%;text-align:center"><button type="button" onclick="removeTarif(\'' + data[i].codeTarif + '\',\'' + data[i].intituleTarif + '\')" class="btn btn-danger" title="Désactiver tarif"><i class="fa fa-trash"></i></button></td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].codeTarif + '</td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].valeurBase + '</td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].TYPE_TARIF_CODE + '</td>';


        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultTarifs.html(tableContent);

    var myDt = tableResultTarifs.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé tarif _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 30,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultTarifs tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectTarifData = new Object();
        selectTarifData.intituleTarif = myDt.row(this).data()[0].toUpperCase();
        selectTarifData.valeurBase = myDt.row(this).data()[1].toUpperCase();
        selectTarifData.typeValeur = myDt.row(this).data()[2].toUpperCase();
        selectTarifData.valeurType = myDt.row(this).data()[3].toUpperCase();
        selectTarifData.codeTarif = myDt.row(this).data()[5].toUpperCase();

        selectTarifData.base = myDt.row(this).data()[6];
        selectTarifData.estTarifPeage = myDt.row(this).data()[7];

        preparerModificationTarif();
    });
}

function preparerModificationTarif() {

    inputIntituleTarif.val(selectTarifData.intituleTarif);
    inputValeurTarif.val(selectTarifData.base);
    cmbTypeValeutTarif.val(selectTarifData.valeurType);
    typeValeur = selectTarifData.valeurType;
    codeTarifs = selectTarifData.codeTarif;
    cmbTypeTarif.val(selectTarifData.estTarifPeage);

    btnEnregistrerTarif.attr('style', 'display:none');
    btnModifierTarif.attr('style', 'display:inline; right: 40%');
}

function removeTarif(codeTarif, intitule) {
    alertify.confirm('Voulez-vous désactiver ' + intitule + ' ?', function () {
        validateDisableTarif(codeTarif);
    });
}

function saveTarif() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveTarif',
            'intituleTarif': inputIntituleTarif.val(),
            'valeurBase': inputValeurTarif.val(),
            'idUser': userData.idUser,
            'typeValeur': typeValeur,
            'typeTarif': cmbTypeTarif.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du tarif en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\enregistrement du tarif s\'est effectué avec succès.');
                    loadTarif()
                    inputIntituleTarif.val('');
                    inputValeurTarif.val('');
                    cmbTypeValeutTarif.val('0');
                    typeValeur = '';
                    cmbTypeTarif.val('-1');

                    btnEnregistrerTarif.attr('style', 'display:inline; right: 38%');
                    btnModifierTarif.attr('style', 'display:none');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement du tarif.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function updateTarif() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateTarif',
            'intituleTarif': inputIntituleTarif.val(),
            'valeurBase': inputValeurTarif.val(),
            'idUser': userData.idUser,
            'codeTarif': codeTarifs,
            'typeValeur': typeValeur,
            'typeTarif': cmbTypeTarif.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour du tarif en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La modification du tarif s\'est effectué avec succès.');
                    loadTarif();
                    inputIntituleTarif.val('');
                    inputValeurTarif.val('');
                    cmbTypeValeutTarif.val('0');
                    typeValeur = '';
                    codeTarifs = '';
                    cmbTypeTarif.val('-1');
                    btnEnregistrerTarif.attr('style', 'display:inline; right: 38%');
                    btnModifierTarif.attr('style', 'display:none');

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la modification du tarif.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function validateDisableTarif(codeTarif) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disableTarif',
            'codeTarif': codeTarif
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation du tarif en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La désactivation de tarif s\'est effectué avec succès.');
                    loadTarif()
                    inputIntituleTarif.val('');
                    inputValeurTarif.val('');
                    cmbTypeValeutTarif.val('');
                    typeValeur = '';
                    btnEnregistrerTarif.attr('style', 'display:inline; right: 38%');
                    btnModifierTarif.attr('style', 'display:none');

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désactivation du tarif.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}