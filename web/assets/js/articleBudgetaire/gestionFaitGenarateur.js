/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbMinistere, cmbServiceAssiette, cmbFormeJuridique;
var tableFaitGenerateur;
var DivNewFaitGenerateur;
var BtnNouveau, BtnEnregistrer, BtnDismiss;
var inputgenerateur;
var codeFaitGenerateur = '';

var faitGenerateurList;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('faits generateurs');

    removeActiveMenu();
    linkMenuGestionArticleBudgetaire.addClass('active');
    inputgenerateur = $('#inputgenerateur');
    cmbMinistere = $('#cmbMinistere');

    cmbMinistere.on('change', function () {
        var code = cmbMinistere.val();
        if (code == '0') {
            cmbServiceAssiette.html('');
            DivNewFaitGenerateur.hide();
//            printFaitsGenerateurs('');
            loadFaitGenerateursByService('');
            return;
        }
        loadServicesByMinistere(code);
    });

    cmbServiceAssiette = $('#cmbServiceAssiette');
    cmbServiceAssiette.on('change', function () {
        var code = cmbServiceAssiette.val();
        if (code == '0') {
            DivNewFaitGenerateur.hide();
            printFaitsGenerateurs('');
            return;
        }
        loadFaitGenerateursByService(code);
    });

    cmbFormeJuridique = $('#cmbFormeJuridique');
    cmbFormeJuridique.on('change', function () {
        var intitule = cmbFormeJuridique.val();
        inputgenerateur = $('# inputgenerateur');
    });

    BtnNouveau = $('#BtnNouveau');
    BtnNouveau.click(function (e) {
        e.preventDefault();
        if (cmbMinistere.val() == '0') {
            alertify.alert('Veuillez sélectionner un ministere.');
            return;
        }

        if (cmbServiceAssiette.val() == '' || cmbServiceAssiette.val() == 'null' || cmbServiceAssiette.val() == '0') {
            alertify.alert('Veuillez sélectionner un service.');
            return;
        }
        BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');
        codeFaitGenerateur = '';
        DivNewFaitGenerateur.show();
    });

    BtnEnregistrer = $('#BtnEnregistrer');

    BtnEnregistrer.click(function (e) {
        e.preventDefault();
        if (inputgenerateur.val() == '' || inputgenerateur.val() == 'null' || inputgenerateur.val() == '0') {
            alertify.alert('Veuillez saisir un fait generateur.');
            return;
        }
        if (cmbFormeJuridique.val() == '' || cmbFormeJuridique.val() == 'null' || cmbFormeJuridique.val() == '0') {
            alertify.alert('Veuillez selectionner une forme juridique.');
            return;
        }
        var action;
        if (codeFaitGenerateur == '') {

            if (checkDoublon()) {
                alertify.alert('ce fait generateur existe deja');
                return;
            }

            action = 'enregistrer ?';
        } else {
            action = 'modifier ?';
        }

        var message = 'Etes-vous sûrs de vouloir ' + action;

        alertify.confirm(message, function () {
            saveFaitGenerateur(1);
        });
    });

    BtnDismiss = $('#BtnDismiss');
    BtnDismiss.click(function (e) {
        e.preventDefault();
        DivNewFaitGenerateur.hide();

    });

    tableFaitGenerateur = $('#tableFaitGenerateur');
    DivNewFaitGenerateur = $('#DivNewFaitGenerateur');


    loadMinistere();
    loadformeJuridique();
//    printFaitsGenerateurs('');
    loadFaitGenerateursByService('');
});


function loadMinistere() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadMinistere',
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            var ministereList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un ministère ---</option>';

            for (var i = 0; i < ministereList.length; i++) {
                data += '<option value="' + ministereList[i].code + '">' + ministereList[i].intitule + '</option>';
            }
            cmbMinistere.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadServicesByMinistere(codeMinistere) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadServices',
            'code_ministere': codeMinistere,
            'code': '1'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            var serviceList = $.parseJSON(JSON.stringify(response));
            var data = '<option value="0">--- Sélectionner un service d\'assiètte ---</option>';

            for (var i = 0; i < serviceList.length; i++) {
                data += '<option value="' + serviceList[i].code + '">' + serviceList[i].intitule + '</option>';
            }
            cmbServiceAssiette.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function loadFaitGenerateursByService(codeService) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFaitGenerateurs',
            'code_service': codeService
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                printFaitsGenerateurs('');
                return;
            }

            faitGenerateurList = $.parseJSON(JSON.stringify(response));
            printFaitsGenerateurs(faitGenerateurList);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printFaitsGenerateurs(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> MINISTERE </th>';
    header += '<th scope="col"> SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col"> FAIT GENERATEUR </th>';
    header += '<th scope="col"> FORME JURIDIQUE </th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {
        body += '<tr>';
        body += '<td>' + result[i].ministere + '</td>';
        body += '<td>' + result[i].service + '</td>';
        body += '<td>' + result[i].intitule + '</td>';
        body += '<td>' + result[i].intituleForme + '</td>';

        body += '<td><button type="button" onclick="editFaitGenerateur(\'' + result[i].code + '\')" class="btn btn-success" title="Modification"><i class="fa fa-edit"></i></button></td>';
        body += '<td><button type="button" onclick="removeFaitGenerateur(\'' + result[i].code + '\')" class="btn btn-danger" title="Suppression"><i class="fa fa-trash"></i></button></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableFaitGenerateur.html(tableContent);
    tableFaitGenerateur.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function loadformeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            var formejuridiqueList = $.parseJSON(JSON.stringify(response));

            console.log(formejuridiqueList)
            var data = '<option value="0">--- Sélectionner une forme juridique ---</option>';

            for (var i = 0; i < formejuridiqueList.length; i++) {
                data += '<option value="' + formejuridiqueList[i].codeFormeJuridique + '">' + formejuridiqueList[i].libelleFormeJuridique + '</option>';
            }
            cmbFormeJuridique.html(data);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function editFaitGenerateur(code) {

    for (var i = 0; i < faitGenerateurList.length; i++) {
        if (faitGenerateurList[i].code == code) {
            inputgenerateur.val(faitGenerateurList[i].intitule);
            cmbFormeJuridique.val(faitGenerateurList[i].codeForme);
            BtnEnregistrer.html('<i class="fa fa-save"></i> Modifier');
            codeFaitGenerateur = code;
            DivNewFaitGenerateur.show();
            break;
        }
    }
}

function saveFaitGenerateur(etat) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveFaitGenerateur',
            'code': codeFaitGenerateur,
            'intitule': inputgenerateur.val(),
            'codeService': cmbServiceAssiette.val(),
            'codeForme': cmbFormeJuridique.val(),
            'etat': etat
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Opération en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucune données retrouvée.');
                return;
            }

            if (etat == 1) {
                var message;
                if (codeFaitGenerateur == '') {
                    message = 'Fait générateur enregistré avec succès!';
                } else {
                    message = 'Fait générateur modifié avec succès!';
                }

                inputgenerateur.val('');
                cmbFormeJuridique.val('0');

                alertify.alert(message);
                DivNewFaitGenerateur.hide();
            } else {
                alertify.alert('Fait générateur supprimé avec succès!');
            }

            codeFaitGenerateur = '';
            BtnEnregistrer.html('<i class="fa fa-save"></i> Enregister');

            loadFaitGenerateursByService(cmbServiceAssiette.val());
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeFaitGenerateur(code) {

    alertify.confirm('Etes-vous sûrs de vouloir supprimer ?', function () {
        codeFaitGenerateur = code;
        saveFaitGenerateur(0);
    });

}

function checkDoublon() {
    var checkVal = false;
    if (faitGenerateurList != undefined) {
        for (var i = 0; i < faitGenerateurList.length; i++) {
            if (faitGenerateurList[i].intitule == inputgenerateur.val()) {
                checkVal = true;
            }
        }
    }

    return checkVal;
}