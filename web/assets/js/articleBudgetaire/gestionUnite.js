/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var modalEditeUnite;

var tableResultUnites;

var btnEnregistrerUnite, btnModifierUnite;

var inputIntituleUnite;

var codeUnites = '';

$(function () {

    modalEditeUnite = $('#modalEditeUnite');

    tableResultUnites = $('#tableResultUnites');

    btnEnregistrerUnite = $('#btnEnregistrerUnite');
    btnModifierUnite = $('#btnModifierUnite');
    
    inputIntituleUnite = $('#inputIntituleUnite');

    btnEnregistrerUnite.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntituleUnite.val() === empty || inputIntituleUnite.val() == 'null') {
            alertify.alert('Veuillez d\'abord fournir l\'intutilé de l\'unité');
            return;
        }

        alertify.confirm('Etes-vous de vouloir enregistrer cette unité ?', function () {

            saveUnite();

        });
    });
    
    btnModifierUnite.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntituleUnite.val() === empty || inputIntituleUnite.val() == 'null') {
            alertify.alert('Veuillez d\'abord fournir la valeur à modifier');
            return;
        }

        alertify.confirm('Etes-vous de vouloir modifier cette unité ?', function () {

            updateUnite();

        });
    });

    loadUnite();
});

function loadUnite() {


    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadUnite'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var uniteList = JSON.parse(JSON.stringify(response));

                printUnite(uniteList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printUnite(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col" style="width:70%">INTITULE UNITE</th>';
    tableContent += '<th scope="col"style="width:5%"></th>';
    tableContent += '<th scope="col"style="width:5%; display:none"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:70%">' + data[i].intituleUnite + '</td>';
        tableContent += '<td style="width:5%"><button type="button" onclick="removeUnite(\'' + data[i].codeUnite + '\',\'' + data[i].intituleUnite + '\')" class="btn btn-danger" title="Désactiver tarif"><i class="fa fa-trash"></i></button></td>';
        tableContent += '<td style="text-align:left; display:none">' + data[i].codeUnite + '</td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultUnites.html(tableContent);

    var myDt = tableResultUnites.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par Intitulé tarif _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 9,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultUnites tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectUniteData = new Object();
        selectUniteData.intituleUnite = myDt.row(this).data()[0].toUpperCase();
        selectUniteData.codeUnite = myDt.row(this).data()[2].toUpperCase();
        preparerModificationUnite();
    });
}

function preparerModificationUnite() {
    inputIntituleUnite.val(selectUniteData.intituleUnite);
    codeUnites = selectUniteData.codeUnite;
    btnEnregistrerUnite.attr('style', 'display:none');
    btnModifierUnite.attr('style', 'display:inline; right: 9%');
}

function removeUnite(codeUnite, intitule) {
    alertify.confirm('Voulez-vous désactiver ' + intitule + ' ?', function () {
        validateDisableUnite(codeUnite);
    });
}

function saveUnite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveUnite',
            'intituleUnite': inputIntituleUnite.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement d\'une uinité en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\enregistrement de l\'unité s\'est effectué avec succès.');
                    loadUnite();
                    inputIntituleUnite.val('');
                    btnEnregistrerUnite.attr('style', 'display:inline; right: 7%');
                    btnModifierUnite.attr('style', 'display:none');
                    initDataArticle();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors d\'enregistrement de l\'unité.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function updateUnite() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateUnite',
            'intituleUnite': inputIntituleUnite.val(),
             'codeUnite': codeUnites
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour du tarif en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('La modification de l\'unité s\'est effectué avec succès.');
                    loadUnite();
                    codeUnites ='';
                    inputIntituleUnite.val('');
                    btnEnregistrerUnite.attr('style', 'display:inline; right: 7%');
                    btnModifierUnite.attr('style', 'display:none');
                    initDataArticle();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la modification de l\'unité.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function validateDisableUnite(codeUnite) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disableUnite',
            'codeUnite': codeUnite
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation de l\'unité en cours ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('La désactivation de l\'unité s\'est effectué avec succès.');
                    loadUnite();
                    codeUnites ='';
                    inputIntituleUnite.val('');
                    btnEnregistrerUnite.attr('style', 'display:inline; right: 7%');
                    btnModifierUnite.attr('style', 'display:none');
                    initDataArticle();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de la désactivation de l\'unité.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}