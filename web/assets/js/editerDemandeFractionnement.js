/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var codeResponsible = '';

var lblLegalForm, lblNifResponsible, lblNameResponsible, lblAddress;
var lbl1, lbl2, lbl3, lbl4;
var btnCallModalSearchResponsable;

var cmbTypeDocument;

var inputNumeroDocument, textReferenceCourrier;
var textObservation;
var btnSearchAndAddDocument;

var tableDemandeEchelonnement;

var codeTypeDoc = 'TD00000052';
var codeDoc = undefined;

var tempDocumentList = [];

var lblNoteDateExi;

var btnJoindreArchiveEchelonnement;
var lblNbDocumentEchelonnement;

var archivesEchelonnement = '';

var btnSaveDemandeEchelonnement;

$(function () {

    mainNavigationLabel.text('FRACTIONNEMENT');
    secondNavigationLabel.text('Edition d\'une demande de fractionnement');

    removeActiveMenu();
    linkMenuFractionnement.addClass('active');

//    if (!controlAccess('12001')) {
//        window.location = 'dashboard';
//    }

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    lblNoteDateExi = $('#lblNoteDateExi');

    cmbTypeDocument = $('#cmbTypeDocument');
    inputNumeroDocument = $('#inputNumeroDocument');
    textReferenceCourrier = $('#textReferenceCourrier');
    textObservation = $('#textObservation');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');

    tableDemandeEchelonnement = $('#tableDemandeEchelonnement');
    btnJoindreArchiveEchelonnement = $('#btnJoindreArchiveEchelonnement');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    btnSaveDemandeEchelonnement = $('#btnSaveDemandeEchelonnement');
    textReferenceCourrier = $('#textReferenceCourrier');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        assujettiModal.modal('show');

    });

    btnSearchAndAddDocument.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (inputNumeroDocument.val().trim() == '') {
            alertify.alert('Veuillez d\'abord saisir un titre de perception.');
            return;
        }

        if (!checkTitreExist(inputNumeroDocument.val().trim())) {
            searchtitre();
        } else {
            alertify.alert('Ce titre existe déjà dans la liste. Impossible de l\'ajouter plus d\'une fois.');
            return;
        }

    });

    btnJoindreArchiveEchelonnement.click(function (e) {

        e.preventDefault();

        if (archivesEchelonnement == '') {
            initUpload(codeDoc, codeTypeDoc);
        } else {
            initUpload(archivesEchelonnement, codeTypeDoc);
        }

    });

    lblNbDocumentEchelonnement.click(function (e) {

        initUpload(archivesEchelonnement, codeTypeDoc);

        e.preventDefault();

    });

    btnSaveDemandeEchelonnement.click(function (e) {

        if (tempDocumentList.length == 0) {

            alertify.alert('Veuillez ajouter au moins un titre dans la liste des titres à fractionner');
            return;
        }

        if (textReferenceCourrier.val() == empty) {

            alertify.alert('Veuillez founir la référence de la lettre de demande.');
            return;
        }

        if (archivesEchelonnement.length == 0 || archivesEchelonnement == '') {

            alertify.alert('Veuillez attacher les pièces jointes.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir validée cette demande ?', function () {

            saveDemandeEchelonnement();

        });

    });
});

function saveDemandeEchelonnement() {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': '1201',
            'titreList': JSON.stringify(tempDocumentList),
            'letterReference': textReferenceCourrier.val(),
            'codePersonne': codeResponsible,
            'observation': textObservation.val(),
            'archives': archivesEchelonnement,
            'userId': userData.idUser,
            'fkDocument': codeTypeDocumentSelected,
            'observationDocument': textObservDoc.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la demande ...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('La demande de fractionnement est effectuée avec succès.');

                setTimeout(function () {
                    window.location = 'editer-fractionnement';
                }, 1500);

            } else if (response == '0') {
                alertify.alert('Une erreur inattendue s\'est produite lors de la création de la demande');
            } else {
                showResponseError();
            }

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI()();
            showResponseError();
        }
    });
}

function getSelectedAssujetiData() {

    codeResponsible = selectAssujettiData.code;
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    lblNifResponsible.html(selectAssujettiData.nif);

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();
}

function searchtitre() {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTitreDetail',
            'codeAssujetti': codeResponsible,
            'numeroDocument': inputNumeroDocument.val().trim(),
            'typeSearchDocument': cmbTypeDocument.val(),
            'codeService': userData.allServiceList,
            'codeSite': userData.allSiteList,
            'codeEntite': userData.allEntiteList
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            switch (response) {
                case 400 :
                    alertify.alert('Le titre de perception fourni n\'a pas été trouvé.');
                    return;
                case 500 :
                    alertify.alert('Le titre de perception fourni n\'a pas un accusé de réception ou imprimé.');
                    return;
                case 600 :
                    alertify.alert('Le titre de perception fourni est un intercalaire');
                    return;
                case 700 :
                    alertify.alert('Le titre de perception fourni déjà fractionné');
                    return;
                case 800 :
                    alertify.alert('Le titre de perception est complement payé');
                    return;
                case 900 :
                    alertify.alert('Une demande existante contient le titre fourni. <br/> Vérifier dans le registre des demandes de fractionnement.');
                    return;
                case 901 :
                    alertify.alert('Le titre de perception fourni a déjà été enrôlé. <br/> Impossible d\'effectuer une demande de fractionnement.');
                    return;
                case 902 :
                    alertify.alert('Impossible de fractionner ce titre de perception fourni.');
                    return;
            }

            setTimeout(function () {

                tempDocumentList.push(response);

                printTitreList();

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printTitreList() {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> TYPE TITRE </th>';
    header += '<th scope="col"> NUM. TITRE </th>';
    header += '<th scope="col"> SECTEUR </th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col"> DATE ORD.</th>';
    header += '<th scope="col"> DATE EXI. </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody id="idBodyReclamation">';

    for (var i = 0; i < tempDocumentList.length; i++) {

        var firstLineAB = '';
        var colorEcheance = 'black';

        if (tempDocumentList[i].libelleArticleBudgetaire.length > 40) {
            firstLineAB = (tempDocumentList[i].codeBudgetaire != '')
                    ? tempDocumentList[i].codeBudgetaire + ' / ' + tempDocumentList[i].libelleArticleBudgetaire.substring(0, 40) + ' ...'
                    : tempDocumentList[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
        } else {
            firstLineAB = (tempDocumentList[i].codeBudgetaire != '')
                    ? tempDocumentList[i].codeBudgetaire + ' / ' + tempDocumentList[i].libelleArticleBudgetaire
                    : tempDocumentList[i].libelleArticleBudgetaire;
        }

        if (tempDocumentList[i].isEchanceEchus) {
            colorEcheance = 'red';
            lblNoteDateExi.show();
        }

        body += '<tr>';
        body += '<td style="width:10%">' + getTypeDocumentName(tempDocumentList[i].TYPE_DOCUMENT) + '</td>';
        body += '<td style="width:10%">' + tempDocumentList[i].referenceDocumentManuel + '</td>';
        body += '<td style="width:20%">' + tempDocumentList[i].SERVICE + '</td>';
        body += '<td style="text-align:left;width:25%;" title="' + tempDocumentList[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        body += '<td style="width:10%">' + tempDocumentList[i].DATECREAT + '</td>';
        body += '<td style="width:10%;color:' + colorEcheance + '">' + tempDocumentList[i].DATE_ECHEANCE_PAIEMENT + '</td>';
        body += '<td style="width:10%;text-align:right">' + formatNumber(tempDocumentList[i].MONTANTDU, tempDocumentList[i].DEVISE) + '</td>';
        body += '<td style="vertical-align:middle;text-align:center"><a onclick="removeTitre(\'' + tempDocumentList[i].referenceDocument + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableDemandeEchelonnement.html(tableContent);
    tableDemandeEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

}

function getTypeDocumentName(type) {
    var nameDocument = '';
    switch (type) {
        case 'NP':
            nameDocument = 'Note de perception';
            break;
        case 'BP':
            nameDocument = 'Bon à payer';
            break;
        case 'AMR1':
            nameDocument = 'Avis de mise en recouvrement 1';
            break;
        default:
            nameDocument = 'Avis de mise en recouvrement 2';
    }
    return nameDocument;
}

function removeTitre(idTitre) {

    alertify.confirm('Etes-vous sûre de vouloir retirer ce titre de la liste ?', function () {

        for (var i = 0; i < tempDocumentList.length; i++) {

            if (tempDocumentList[i].referenceDocument == idTitre) {

                tempDocumentList.splice(i, 1);
                printTitreList();
                break;
            }
        }

    });

}

function checkTitreExist(refTitre) {

    var exist = false;

    for (var i = 0; i < tempDocumentList.length; i++) {

        if (tempDocumentList[i].referenceDocument == refTitre) {

            exist = true;
            break;
        }
    }

    return exist;
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesEchelonnement = getUploadedData();

    nombre = JSON.parse(archivesEchelonnement).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}
