/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var
        codeResponsible = '',
        codeTypeAssjetti = '',
        nameResponsible = '',
        selectedBien = '',
        yes4Assujetti = true; //Pour afficher les biens non loués

var
        tabBiens,
        tabArticles;

var isTypeMobilier;

var
        tableBiens,
        tableArticles,
        tablePeriodeDeclaration;

var
        lblLegalForm,
        lblNameResponsible,
        lblAddress;

var
        btnCallModalSearchResponsable,
        btnNewBien,
        btnPasserAB, btnPasserAB2, btnPasserAB21,
        btnLocateBien, btnNewBienImmobilier, btnNewBienConcession,
        btnNewBienPanneauPublicitaire, btnPermuteBien, btnNewUniteEconomique;

var
        tempBienList = [],
        tempArticleList = [];

var bienList = [];

var modalPeriodeDeclaration;

var lbl1, lbl2, lbl3;

var typePage;

var modalDetailDeclaration, modalPermutationBien, modalCreateUniteEconomique;

var lblNameAssujettiModal, lblAdress,
        lblNoteCalcul, lblAgentTaxateur, lblAgentValidateur, lblAgentOrdonnateur, btnVoirDeclaration;

var archiveContent = '';

var redressementObj = null;

var modalMiseEnQuarantaine, textaeraObservation, btnMiseQuarantaine;

var periodeDeclarationId, periodeDeclarationName;
var codeAssujettiSave;

var modalInfoMiseEnQuarantaine, valuePeriode2, valueDate2, valueAgent2, valueObservation;
var valuePeriode1, valueAgent1, dateAcquisitionBien;

var btnIRL;
var btnIF;
var btnRL;
var btnVIGNETTE;
var btnICM;
var btnPUBLICITE;

var namePropriete, adressePropriete;

var checkOperation = false;

$(function () {
    tabBiens = $('#tabBiens');
    tabArticles = $('#tabArticles');
    assujettiModal = $('#assujettiModal');
    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    tableBiens = $('#tableBiens');
    tableArticles = $('#tableArticles');
    tablePeriodeDeclaration = $('#tablePeriodeDeclaration');
    modalPeriodeDeclaration = $('#modalPeriodeDeclaration');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');

    modalInfoMiseEnQuarantaine = $('#modalInfoMiseEnQuarantaine');
    modalPermutationBien = $('#modalPermutationBien');
    valuePeriode2 = $('#valuePeriode2');

    valueDate2 = $('#valueDate2');
    valueAgent2 = $('#valueAgent2');
    valueObservation = $('#valueObservation');
    valuePeriode1 = $('#valuePeriode1');
    valueAgent1 = $('#valueAgent1');

    modalMiseEnQuarantaine = $('#modalMiseEnQuarantaine');
    textaeraObservation = $('#textaeraObservation');
    btnMiseQuarantaine = $('#btnMiseQuarantaine');
    btnNewUniteEconomique = $('#btnNewUniteEconomique');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    modalCreateUniteEconomique = $('#modalCreateUniteEconomique');

    btnIRL = $("#btnIRL");
    btnIF = $("#btnIF");
    btnRL = $("#btnRL");
    btnVIGNETTE = $("#btnVIGNETTE");
    btnICM = $("#btnICM");
    btnPUBLICITE = $("#btnPUBLICITE");

    var urlCode = getUrlParameter('id');
    if (jQuery.type(urlCode) !== 'undefined') {
        codeResponsible = atob(urlCode);
        refreshDataAssujetti();
        loadBiens(codeResponsible, '0');
    }

    tabBiens.tab('show');

    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        yes4Assujetti = true;
        allSearchAssujetti = '1';
        resetSearchAssujetti();
        assujettiModal.modal('show');
    });

    btnLocateBien = $('#btnLocateBien');
    btnPasserAB = $('#btnPasserAB');
    btnPasserAB2 = $('#btnPasserAB2');
    btnPasserAB21 = $('#btnPasserAB21');
    btnPermuteBien = $('#btnPermuteBien');

    /*if (controlAccess('ASSUJETIR_BIEN')) {
     btnLocateBien.attr('style', 'display:block');
     }
     
     if (controlAccess('ASSUJETIR_BIEN')) {
     btnPasserAB.attr('style', 'display:block');
     //btnPasserAB2.attr('style', 'display:block;width:100%');
     //btnPasserAB21.attr('style', 'display:block;width:100%');
     }*/

    btnLocateBien.click(function (e) {
        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable');
            return;
        }

        codeLocataireBien = codeResponsible;

        yes4Assujetti = false;
        allSearchAssujetti = '1';

        resetSearchAssujetti();

        var messageLocation = 'Location d\'un bien :<br/>'
                + '<br/>- Rechercher et sélectionner le propiétaire'
                + '<br/>- Sélectionner le bien à louer'
                + '<br/>- Renseigner les informations du bien.'
                + '<br/><br/>Voulez-vous continuer ?';

        alertify.confirm(messageLocation, function () {
            assujettiModal.modal('show');
        });

    });

    btnPermuteBien.click(function (e) {
        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable');
            return;
        }

        codeLocataireBien = codeResponsible;
        codePermuteBien = codeResponsible;

        checkOperation = true;

        yes4Assujetti = false;
        allSearchAssujetti = '1';

        resetSearchAssujetti();

        var messageLocation = 'Location d\'un bien :<br/>'
                + '<br/>- Rechercher et sélectionner le propiétaire'
                + '<br/>- Sélectionner le bien à permuter'
                + '<br/>- Renseigner les informations du bien.'
                + '<br/><br/>Voulez-vous continuer ?';

        alertify.confirm(messageLocation, function () {
            assujettiModal.modal('show');
        });

    });

    btnNewBien = $('#btnNewBien');
    btnNewBienImmobilier = $('#btnNewBienImmobilier');
    btnNewBienConcession = $('#btnNewBienConcession');
    btnNewBienPanneauPublicitaire = $('#btnNewBienPanneauPublicitaire');

    btnNewBien.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-automobile?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewBienPanneauPublicitaire.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-panneau-publicitaire?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewBienImmobilier.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-immobilier?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewBienConcession.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }

        window.location = 'edition-bien-concession-mine?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);

    });

    btnNewUniteEconomique.click(function (e) {
        e.preventDefault();
        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un contribuable.');
            return;
        }
        getSelectedDataUnite();

        modalCreateUniteEconomique.modal('show');

    });

    btnMiseQuarantaine.click(function (e) {

        e.preventDefault();

        if (textaeraObservation.val() == empty) {

            alertify.alert('Veuillez d\'abord motiver la mise en quarantaine de cette période');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir mettre en quarantaine cette période de déclaration : ' + periodeDeclarationName + ' ?', function () {
            miseEnQuarantaine();
        });

    });

    //vérifier si le bien est de type immobilier
    isTypeMobilier = false;

    btnPasserAB.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un contribuable.');
            return;
        }

        if (isTypeMobilier) {
            $('#modalChooseAB').modal('show');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    btnPasserAB2.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    btnPasserAB21.click(function (e) {

        e.preventDefault();

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        if (!controlAccess('2004')) {
            alertify.alert('Vous n\'avez pas le droit de faire un assujettissement.');
            return;
        }

        setBienData(JSON.stringify(tempBienList));

        window.location = 'assujettissement-multiple?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien);
        selectedBien = '';

    });

    printBien('');
    printArticles('');

    var paramGuideValue = JSON.parse(getTourParam());

    if (paramGuideValue === 1) {

        Tour.run([
            {
                element: $('#btnCallModalSearchResponsable'),
                content: '<p style="font-size:17px;color:black">Commencer par rechercher un assujetti.</p>',
                language: 'fr',
                position: 'right',
                close: 'false'}
        ]);
    } else {
        Tour.run();
    }

    modalDetailDeclaration = $('#modalDetailDeclaration');
    btnVoirDeclaration = $('#btnVoirDeclaration');

    btnVoirDeclaration.click(function (e) {
        e.preventDefault();
        initPrintData(archiveContent);
    });

    btnIRL.on('click', function () {
        goToAssujettisement(AB_IRL, 'Impôt sur les revenus locatifs');
    });

    btnIF.on('click', function () {
        goToAssujettisement(AB_IF, 'Impôts fonciers (IF)');
    });

    btnRL.on('click', function () {
        goToAssujettisement(AB_RL, 'La retenue locative (RL)');
    });

    btnICM.on('click', function () {
        goToAssujettisement(AB_ICM, 'Impôts sur l\'ICM');
    });

    btnVIGNETTE.on('click', function () {
        goToAssujettisement(AB_VIGNETTE, 'Impôts sur la vignette Automobile');
    });

    btnPUBLICITE.on('click', function () {
        goToAssujettisement(AB_PUBLICITE, 'Taxes sur autorisation de dépôts des affiches et des panneaux publicitaires dans les lieux publics');
    });

    switch (userData.isSitePeage) {
        case true:

            btnNewBienImmobilier.attr('style', 'display:none');
            btnNewBienConcession.attr('style', 'display:none');
            btnNewBienPanneauPublicitaire.attr('style', 'display:none');
            btnPasserAB.attr('style', 'display:none');
            tabArticles.attr('style', 'display:none');

            break;
        case false:
            btnNewBienImmobilier.attr('style', 'display:inline');
            btnNewBienConcession.attr('style', 'display:inline');
            btnNewBienPanneauPublicitaire.attr('style', 'display:inline');
            btnPasserAB.attr('style', 'display:inline');
            tabArticles.attr('style', 'display:inline');
            break;

    }

    modalCreateUniteEconomique.on('shown.bs.modal', function () {
        jQuery(".chosen").chosen();
    });

});

function goToAssujettisement(ab, libelle) {

    setBienData(JSON.stringify(tempBienList));
    window.location = 'assujettissement?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&type=' + btoa(codeTypeAssjetti) + '&bien=' + btoa(selectedBien) + '&ab=' + btoa(ab) + '&libAb=' + btoa(libelle);
    selectedBien = '';
    $('#modalChooseAB').modal('hide');
}

function getSelectedAssujetiData() {

    if (yes4Assujetti == true) {

        codeResponsible = selectAssujettiData.code;
        codeTypeAssjetti = selectAssujettiData.codeForme;
        nameResponsible = selectAssujettiData.nomComplet;

        codeLocataireBien = codeResponsible;
        codePermuteBien = codeResponsible;
        codeAssujettiSave = codeResponsible;

        lblNameResponsible.html(nameResponsible);
        lblLegalForm.html(selectAssujettiData.categorie);
        lblAddress.html(selectAssujettiData.adresse);

        lbl1.show();
        lbl2.show();
        lbl3.show();

        setAssujettiData(JSON.stringify(selectAssujettiData));
        loadBiens(codeResponsible, '0');

    } else {

        if (codeResponsible == selectAssujettiData.code) {
            alertify.alert('Vous ne pouvez pas louer vos propres biens.');
            return;
        }

        codeProprietaireBien = selectAssujettiData.code;
        lblNameProprietaire.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietaire.html(selectAssujettiData.adresse);

        lblNameProprietairePerm.html(selectAssujettiData.nomComplet + (' (Propriétaire)'));
        lblAddressProprietairePerm.html(selectAssujettiData.adresse);

        loadBiens(codeProprietaireBien, '1');
    }

}

function refreshDataAssujetti() {

    var jsonAssujetti = getAssujettiData();
    var assujettiObj = JSON.parse(jsonAssujetti);

    codeResponsible = assujettiObj.code;
    nameResponsible = assujettiObj.nomComplet;

    lblNameResponsible.html(nameResponsible);
    lblLegalForm.html(assujettiObj.categorie);
    lblAddress.html(assujettiObj.adresse);

    lbl1.show();
    lbl2.show();
    lbl3.show();
}
function loadBiens(personneCode, forLocation) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne',
            'codePersonne': personneCode,
            'forLocation': forLocation,
            'codeService': userData.serviceCode,
            'cmbTarif': '*'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                var articleList = $.parseJSON(JSON.stringify(assujettissementList.listArticleBudgetaires));

                if (yes4Assujetti == true) {
                    printBien(bienList);
                    printArticles(articleList);
                } else {

                    var dataBiens = printOtherBien(bienList);

                    if (checkOperation) {
                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à permuter.');
                            return;
                        }
                        cmbPermutationBien.html(dataBiens);
                        modalPermutationBien.modal('show');

                    } else {

                        if (bienList.length === 0) {
                            alertify.alert('L\assujetti ' + selectAssujettiData.nomComplet + ' n\'a pas des biens à mettre en location.');
                            return;
                        }
                        cmbLocationBien.html(dataBiens);
                        modalLocationBien.modal('show');
                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printBien(bienList) {

    tempBienList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:40%"> UNITE ECONOMIQUE </th>';
//    header += '<th scope="col" style="width:30%"> DESCRIPTION </th>';
    header += '<th scope="col" style="width:45%"> ADRESSE </th>';
    header += '<th scope="col" style="width:10%"> PROPRIETAIRE </th>';
//    header += '<th ></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';
    header += '<th hidden="true" scope="col">Type</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        var proprietaire = bienList[i].proprietaire;

        var descriptionBien = empty;

        if (bienList[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + bienList[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + bienList[i].communeName + '</span>';

            var quartierInfo = 'Quartier : --';

            if (bienList[i].communeName !== empty) {
                quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + bienList[i].quartierName + '</span>';
            }

            var complementInfo = bienList[i].complement;

            descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo;


        } else {

            if (bienList[i].type == 1) {

                var genreInfo = 'Genre : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;

            } else if (bienList[i].type == 3) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;

            } else if (bienList[i].type == 4) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + bienList[i].complement;

            } else {

                descriptionBien = bienList[i].descriptionBien + '<hr/>' + bienList[i].complement;
            }
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;">' + bienList[i].intituleBien + '</td>';
//        body += '<td style="vertical-align:middle;">' + descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;">' + bienList[i].responsable.toUpperCase() + '</td>';
//        body += '<td style="vertical-align:middle;text-align:center">'
//                + '<a onclick="editBien(\'' + bienList[i].idBien + '\',\'' + bienList[i].codeTypeBien + '\',\'' + bienList[i].isImmobilier + '\')" class="btn btn-success" title="Modifier le bien"><i class="fa fa-edit"></i></a>&nbsp;'
//                + '<a onclick="disabledBien(\'' + bienList[i].idAcquisition + '\',\'' + proprietaire + '\')" class="btn btn-danger" title="Désactiver le bien"><i class="fa fa-trash-o"></i></a>&nbsp;'
//                + '<a style="display:none" onclick="storyBien(\'' + 0 + '\')" class="btn btn-warning" title="Voir l\'historique"><i class="fa fa-history"></i></a></td>';
        body += '<td hidden="true">' + bienList[i].idBien + '</td>';
        body += '<td hidden="true">' + bienList[i].type + '</td>';
        body += '</tr>';

        var bien = new Object();
        bien.idBien = bienList[i].idBien;
        bien.intituleBien = bienList[i].intituleBien;
        bien.chaineAdresse = bienList[i].chaineAdresse;
        bien.codeAP = bienList[i].codeAP;
        bien.dateAcquisition = bienList[i].dateAcquisition;
        tempBienList.push(bien);
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableBiens.html(tableContent);
    var dtBien = tableBiens.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 20
    });
    $('#tableBiens tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        //alert(JSON.stringify(data));
        selectedBien = data[5];
        switch (data[6]) {
            case 1:
            case 2:
                isTypeMobilier = false;
                break;

            default:
                isTypeMobilier = true;
                break;
        }
    });
}

function printArticles(articleList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" hidden="true"> Id </th>';
    header += '<th scope="col" hidden="true"> Code </th>';
    header += '<th scope="col" style="width:300px"> SERVICE D\'ASSIETE </th>';
    header += '<th scope="col" style="width:30px"> UNITE ECONOMIQUE </th>';
    header += '<th scope="col" style="width:400px"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" style="width:100px"> PERIODICITE </th>';
    header += '<th scope="col" style="width:100px"> ETAT </th>';
    header += '<th scope="col" style="width:150px"> </th>';
    header += '</tr></thead>';

    tempArticleList = [];

    var body = '<tbody id="tbodyArticles">';
    var firstLineAB = '';

    for (var i = 0; i < articleList.length; i++) {

        var article = {};

        firstLineAB = articleList[i].intituleAB;

        if (articleList[i].intituleTarif.toUpperCase() != 'TOUT' && articleList[i].intituleTarif.toUpperCase() != 'UNIQUE') {

            firstLineAB += ' : ' + articleList[i].intituleTarif;
        }

        if (firstLineAB.length > 340) {
            firstLineAB = firstLineAB.substring(0, 340) + ' ...';
        }

        article.idAssujettissement = articleList[i].idAssujettissement;
        article.codeAB = articleList[i].codeAB;
        article.intituleAB = articleList[i].intituleAB.toUpperCase();
        article.valeurBase = articleList[i].valeurBase;
        article.unite = articleList[i].unite;
        article.duree = articleList[i].duree;
        article.dateDebut = articleList[i].dateDebut;
        article.dateFin = articleList[i].dateFin;
        article.dateFin2 = articleList[i].dateFin2;
        article.reconduction = articleList[i].reconduction;
        article.nombreJour = articleList[i].nombreJour;
        article.nombreJourLimite = articleList[i].nombreJourLimite;
        article.nombreJourLimitePaiement = articleList[i].nbreJourLegalePaiement;
        article.echeanceLegale = articleList[i].echeanceLegale;
        article.periodeEcheance = articleList[i].periodeEcheance;
        article.codePeriodiciteAB = articleList[i].codePeriodiciteAB;
        article.listPeriodesDeclarations = articleList[i].listPeriodesDeclarations;
        article.montantDu = formatNumber(articleList[i].montant, articleList[i].devise);
        article.intituleTarif = articleList[i].intituleTarif;
        article.libellePeriodiciteAB = articleList[i].libellePeriodiciteAB;
        article.waittingClosing = articleList[i].waittingClosing;

        tempArticleList.push(article);

        var etat = 'VALIDE';
        var etatColor = 'black';

        if (articleList[i].etat == 0) {

            etat = 'ANNULE';
            etatColor = 'red';
        }


        body += '<tr>';
        body += '<td hidden="true">' + articleList[i].idAssujettissement + '</td>';
        body += '<td hidden="true">' + articleList[i].codeAB + '</td>';
        body += '<td style="width:20%;vertical-align:middle">' + articleList[i].secteurActivite.toUpperCase() + '</td>';
        body += '<td style="text-align:left;width:25%;vertical-align:middle"><span style="font-weight:bold">' + articleList[i].intituleBien.toUpperCase() + '</span><br/><br/>' + articleList[i].adresseBien.toUpperCase() + '</td>';

        if (articleList[i].isManyAB) {
            body += '<td style="width:30%;vertical-align:middle" title="Plusieurs"><a href="#3" style="font-weight:bold;color:blue;text-decoration:underline">Voir la liste</a></td>';
        } else {
            body += '<td style="width:30%;vertical-align:middle" title="' + articleList[i].intituleAB + '"><span style="font-weight:bold;">' + firstLineAB.toUpperCase() + '</td>';
        }

        body += '<td style="width:10%;vertical-align:middle"> ' + articleList[i].libellePeriodiciteAB + '</td>';
        body += '<td style="width:8%;vertical-align:middle;color:' + etatColor + '"> ' + etat + '</td>';

        var taxButton = '<a  onclick="gotoTaxation(\'' + articleList[i].idAssujettissement + '\',' + articleList[i].isManyAB + ')" class="btn btn-success" title="Passer à la taxation"><i class="fa fa-arrow-right"></i></a>&nbsp;';
        var generateButton = '<a  onclick="generatePeriode(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-success" title="Générer période"><i class="fa fa-plus-circle"></i></a>&nbsp;';
        var deleteButton = '<a onclick="removeAssujettissement(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-danger" title="Annuler cet assujettissement"><i class="fa fa-close"></i></a>&nbsp';

        if (!controlAccess('SET_TAXATION_FISCALE')) {
            taxButton = '';
        }
        if (!controlAccess('3011')) {
            generateButton = '';
        }
        if (!controlAccess('3012')) {
            deleteButton = '';
        }

        if (articleList[i].etat == 1) {

            body += '<td style="text-align:center;width:17%;vertical-align:middle">'
                    + '' + deleteButton + ''
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i></a>&nbsp;'
                    + '' + generateButton + ''
                    + '' + taxButton + '</td>';
        } else {

            body += '<td style="text-align:center;width:10%;vertical-align:middle">'
                    + '<a onclick="openPeriodes(\'' + articleList[i].idAssujettissement + '\')" class="btn btn-warning" title="Afficher les périodes"><i class="fa fa-list"></i></a></td>&nbsp;';
        }


        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableArticles.html(tableContent);
    var dtArticles = tableArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os', blurable: true
        },
        datalength: 3
    });
    $('#tableArticles tbody').on('click', 'tr', function () {
        var data = dtArticles.row(this).data();
    });
}

function printOtherBien(bienList) {

    var dataBien = '<option value ="0">-- Sélectionner --</option>';
    for (var i = 0; i < bienList.length; i++) {
        dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
    }

    return dataBien;
}

function generatePeriode(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir générer une période supplémentaire ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;

                break;
            }
        }

        createPeriodeDeclarationIfNotExist(1, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance);
    });
}

function gotoTaxation(idAssujettissement, isMany) {

    alertify.confirm('Etes-vous sûre de vouloir passer à la déclaration proprement dite ?', function () {

        var dateFin = '', nombreJour = '', nombreJourLimite = '', nombreJourLimitePaiement = '', echeanceLegale = '', codePeriodiciteAB = '', periodeEcheance = '';

        for (var i = 0; i < tempArticleList.length; i++) {

            if (tempArticleList[i].idAssujettissement == idAssujettissement) {

                dateFin = tempArticleList[i].dateFin2;
                nombreJour = tempArticleList[i].nombreJour;
                nombreJourLimite = tempArticleList[i].nombreJourLimite;
                echeanceLegale = tempArticleList[i].echeanceLegale;
                periodeEcheance = tempArticleList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleList[i].codePeriodiciteAB;
                nombreJourLimitePaiement = tempArticleList[i].nombreJourLimitePaiement;

                break;
            }
        }

        createPeriodeDeclarationIfNotExist(0, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany);
    });
}

var tempPeriodeDeclaration;

function openPeriodes(idAssujettissement) {

    for (var i = 0; i < tempArticleList.length; i++) {

        if (tempArticleList[i].idAssujettissement == idAssujettissement) {

            var header = empty, body = empty;

            tempPeriodeDeclaration = tempArticleList[i].listPeriodesDeclarations;

            header += '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
            header += '<th style="text-align:left">EXERCICE</th>';
            header += '<th style="text-aLign:left">ECHEANCE DEC.</th>';
            header += '<th style="text-aLign:left">ECHEANCE PAIE.</th>';
            header += '<th style="text-aLign:left">DATE TAXATION</th>';
            header += '<th style="text-aLign:left">NOTE TAXATION</th>';
            header += '<th style="text-aLign:left">NOTE PERCEPTION</th>';
            header += '<th style="text-aLign:right">MONTANT DU</th>';
            header += '<th style="text-aLign:right">PENALITE DU</th>';
            header += '<th style="text-aLign:right">TOTAL DU</th>';
            header += '<th style="text-aLign:right">TOTAL PAYE</th>';
            header += '<th style="text-aLign:right">RESTE A PAYER</th>';
            header += '<th></th>';
            header += '</tr></thead>';

            for (var j = 0; j < tempPeriodeDeclaration.length; j++) {

                var periodeDeclaration = tempPeriodeDeclaration[j];

                var btnMiseQuarantaine = '';

                if (controlAccess('3015')) {

                    if (periodeDeclaration.noteTaxation == '' && periodeDeclaration.statePeriodeDeclaration == 1) {
                        btnMiseQuarantaine = '&nbsp;<button type="button" class="btn btn-danger" title="Cliquez ici pour mettre en quarantaine cette période de déclaration" onclick="loadModalMiseEnQuarantaine(\'' + periodeDeclaration.periodeId + '\',\'' + periodeDeclaration.periode + '\')"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Annuler</button>';
                    } else if (periodeDeclaration.statePeriodeDeclaration == 0) {
                        btnMiseQuarantaine = '&nbsp;<button type="button" class="btn btn-default" title="Cliquez ici pour afficher les observations de la mise mettre en quarantaine" onclick="loadModalInfoMiseEnQuarantaine(\'' + periodeDeclaration.periode + '\',\'' + periodeDeclaration.observation + '\',\'' + periodeDeclaration.agentMaj + '\',\'' + periodeDeclaration.dateMaj + '\')"><i class="fa fa-list"></i>&nbsp;&nbsp;Afficher</button>';
                    } else {
                        btnMiseQuarantaine = '';
                    }
                }

                var montantDu = empty, penaliteDu = empty, totalDu = empty, montantPayer = empty, restePayer = empty;

                var noteTaxation = periodeDeclaration.noteTaxation;

                var colorEcheance = 'color: black';
                var colorEcheancePaiement = 'color: black';

                if (noteTaxation != empty) {

                    montantDu = formatNumber(periodeDeclaration.montantDu, periodeDeclaration.devise);
                    penaliteDu = formatNumber(periodeDeclaration.penaliteDu, periodeDeclaration.devise);
                    totalDu = formatNumber(periodeDeclaration.totalDu, periodeDeclaration.devise);
                    montantPayer = formatNumber(periodeDeclaration.montantPayer, periodeDeclaration.devise);
                    restePayer = formatNumber(periodeDeclaration.restePayer, periodeDeclaration.devise);

                }

                switch (periodeDeclaration.estPenalise) {
                    case "1":
                        colorEcheance = 'color: red';
                        break;
                    case "0":
                        colorEcheance = 'color: black';
                        break;
                }

                switch (periodeDeclaration.estPenalisePaiement) {
                    case "1":
                        colorEcheancePaiement = 'color: red';
                        break;
                    case "0":
                        colorEcheancePaiement = 'color: black';
                        break;
                }

                body += '<tr>';
                body += '<td style="text-align:left;width:9%;vertical-align:middle">' + periodeDeclaration.periode + '</td>';
                body += '<td style="text-align:left;width:6%;vertical-align:middle' + colorEcheance + '">' + periodeDeclaration.dateLimite + '</td>';
                body += '<td style="text-align:left;width:6%' + colorEcheancePaiement + '">' + periodeDeclaration.dateLimitePaiement + '</td>';
                body += '<td style="text-align:left;width:6%;vertical-align:middle">' + periodeDeclaration.dateDeclaration + '</td>';
                body += '<td style="text-align:left;width:9%;vertical-align:middle">' + noteTaxation + '</td>';
                body += '<td style="text-align:left;width:10%;vertical-align:middle">' + periodeDeclaration.notePerceptionManuel + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + montantDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + penaliteDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + totalDu + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + montantPayer + '</td>';
                body += '<td style="text-align:right;width:10%;vertical-align:middle">' + restePayer + '</td>';
                if (noteTaxation != empty) {
                    body += '<td style="text-align:center;width:9%;vertical-align:middle"><button type="button" class="btn btn-warning" onclick="showDeclarationDetail(\'' + noteTaxation + '\',\'' + periodeDeclaration.waittingClosing + '\',\'' + periodeDeclaration.periode + '\')"><i class="fa fa-list fa-1x"></i>&nbsp;&nbsp;Voir détails</button></td>';
                } else {
                    body += '<td style="text-align:center;width:9%;vertical-align:middle">' + btnMiseQuarantaine + '</td>';
                }
            }

            body += '</tr></tbody>';

            var tableContent = header + body;
            tablePeriodeDeclaration.html(tableContent);
            tablePeriodeDeclaration.DataTable({
                language: {
                    processing: "Traitement en cours...",
                    track: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                    search: "Rechercher par N° document _INPUT_  ",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                info: false,
                destroy: true,
                searching: false,
                paging: true,
                order: [[0, 'asc']],
                lengthChange: false,
                tracking: false,
                ordering: false,
                pageLength: 5,
                lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                select: {
                    style: 'os',
                    blurable: true
                },
                datalength: 3
            });

            modalPeriodeDeclaration.modal('show');
            break;
        }
    }
}

function removeAssujettissement(idAssujettissement) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet acte générateur du bien ?', function () {
        desactivateAssujettissement(idAssujettissement);
    });
}

function createPeriodeDeclarationIfNotExist(isGenerate, idAssujettissement, dateFin, nombreJour, nombreJourLimite, nombreJourLimitePaiement, echeanceLegale, codePeriodiciteAB, periodeEcheance, isMany) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'createPeriodeDeclaration',
            'isGenerate': isGenerate,
            'idAssujettissement': idAssujettissement,
            'dateFin': dateFin,
            'nombreJour': nombreJour,
            'nombreJourLimite': nombreJourLimite,
            'nbreJourLegalePaiement': nombreJourLimitePaiement,
            'echeanceLegale': echeanceLegale, 'codePeriodiciteAB': codePeriodiciteAB,
            'periodeEcheance': periodeEcheance
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Veuillez patientier...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1' && isGenerate == 0) {

                    if (isMany) {
                        setAssujettiData(JSON.stringify(selectAssujettiData));
                    }

                    window.location = isMany ? 'taxation-repetitive_multiple?assuj=' + btoa(idAssujettissement) : 'taxation-repetitive?id=' + btoa(idAssujettissement);

                } else if (response == '1' && isGenerate == 1) {

                    alertify.alert('Des périodes supplémnetaires sont générées avec succès');
                    loadBiens(codeResponsible, '0');

                } else if (response == '0') {

                    alertify.alert('Echec lors du passage à la déclaration');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function desactivateAssujettissement(idAssujettissement) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactivateAssujettissement',
            'idAssujettissement': idAssujettissement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation de l\'assujettissement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('l\'assujettissement est annulé avec succès.');

                    setTimeout(function () {

                        loadBiens(codeResponsible, '0');
                    }
                    , 1000);

                } else if (response == '0') {
                    alertify.alert('Echec lors de l\'annulation de l\'assujettissement');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetSearchAssujetti() {

    inputValueResearchAssujetti.val('');
    printAssujettiTable('');
}

function disabledBien(idAcquisition, proprietaire) {

    alertify.confirm('Etes-vous sûre de vouloir rétirer ce bien ?', function () {

        $.ajax({
            type: 'POST',
            url: 'assujetissement_servlet',
            dataType: 'JSON',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'desactivateBienAcquisition',
                'idAcquisition': idAcquisition
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Rétrait du bien en cours ...</h5>'});
            },
            success: function (response)
            {

                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                }

                setTimeout(function () {

                    if (response == '1') {
                        alertify.alert('Rétrait du bien effectuer avec succès.');
                        loadBiens(codeResponsible, '0');
                    } else if (response == '0') {
                        alertify.alert('Echec lors du rétrait du bien');
                    }
                }
                , 1);
            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}


function showDeclarationDetail(nc, state, periode) {

    alertify.confirm('Etes-vous sûre de vouloir visualiser les détails de cette taxation ?', function () {

        //setRegisterType('RNT');

        if (state === '0') {
            setRegisterType('RNCC');
            window.open('registre-notes-taxations-cloturees?nc=' + btoa(nc), '_blank');
        } else if (state === '1') {

            if (controlAccess('CLOTURE_TAXATION')) {
                setRegisterType('RNC')
                window.open('cloturer-taxation?nc=' + btoa(nc), '_blank');
            } else {
                alertify.alert('La taxation n° ' + '<span style="font-weight: bold">' + nc + '</span>' + ' relative à la période de déclaration ' + '<span style="font-weight: bold">' + periode + '</span>' + ' est en attente d\'une clôture.<br/><br/>Vous ne possedez pas le droit d\'accéder au registre des notes de ce type.');
                return;
            }

        }

    });
}

function loadModalMiseEnQuarantaine(periodeId, periodeName) {

    periodeDeclarationId = periodeId;
    periodeDeclarationName = periodeName;
    textaeraObservation.val(empty);
    valuePeriode1.html(periodeName);
    valueAgent1.html(userData.nomComplet);
    modalMiseEnQuarantaine.modal('show');
}

function miseEnQuarantaine() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'miseQuarantaine',
            'periodeId': periodeDeclarationId,
            'observation': textaeraObservation.val(),
            'userId': userData.idUser
        },
        beforeSend: function () {

            modalMiseEnQuarantaine.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>La mise en quarantaine en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                modalMiseEnQuarantaine.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalMiseEnQuarantaine.unblock();
                modalMiseEnQuarantaine.modal('hide');

                alertify.alert('La mise en quarantaine s\'est effectuée avec succès.');


                loadBiens(codeAssujettiSave, '1');

                modalPeriodeDeclaration.modal('hide');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalMiseEnQuarantaine.unblock();
            showResponseError();
        }
    });
}

function loadModalInfoMiseEnQuarantaine(periodeName, obervation, agent, date) {

    valuePeriode2.html(periodeName);
    valueDate2.html(date);
    valueAgent2.html(agent);
    valueObservation.val(obervation);

    modalInfoMiseEnQuarantaine.modal('show');
}


function editBien(idBien, codeTypeBien, isImmobilier) {

    var codeBien = idBien + ';' + codeTypeBien;

    switch (isImmobilier) {
        case '0':
            if (codeTypeBien == 'TB00000041') {
                window.location = 'edition-bien-concession-mine?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            } else if (codeTypeBien == 'TB00000042') {
                window.location = 'edition-bien-panneau-publicitaire?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            } else {
                window.location = 'edition-bien-automobile?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            }

            break;
        case '1':
            window.location = 'edition-bien-immobilier?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible) + '&codeBien=' + btoa(codeBien);
            break;
    }


}

function prepareDateAcquitionBien(idBien) {

    for (var i = 0; i < bienList.length; i++) {

        if (idBien == bienList[i].idBien) {
            dateAcquisitionBien = bienList[i].dateAcquisition;
            break;
        }

    }
    return dateAcquisitionBien;
}

function getBienPersonne(){
    loadBiens(codeResponsible, '0');
}
