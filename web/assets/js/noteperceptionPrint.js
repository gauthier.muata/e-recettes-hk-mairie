/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var selectBanque, selectCompteBancaire;
var labelNpAmount, labelTaux, labelNPConvertedAmout;
var lblNotePerception, lblNameAssujetti, lblAdress, labelReceptionniste;

var selectedAccountCode, selectedAccountDevise;

var taux;

var npNumero;

var modalNpDetails;

var npInitialAmout;
var npInitialDevise;
var npConvertedAmount;
var npPrintCount;
var npNoteCalcul;
var npPersonneReception;
var npBankAccount;
var npDevise;


var btnPrintNP;
var btnSearchReception;

var inputchoiceAddEchanceDate;

$(function () {

    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    labelNpAmount = $('#labelNpAmount');
    labelTaux = $('#labelTaux');
    labelNPConvertedAmout = $('#labelNPConvertedAmout');
    lblNotePerception = $('#lblNotePerception');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblAdress = $('#lblAdress');
    labelReceptionniste = $('#labelReceptionniste');

    btnPrintNP = $('#btnPrintNP');
    
    btnSearchReception = $('#btnSearchReception');
    modalNpDetails = $('#modalNotePerceptionDetails');

    inputchoiceAddEchanceDate = $('#inputchoiceAddEchanceDate');

    selectBanque.on('change', function (e) {

        e.preventDefault();

        addAccount(selectBanque.val());

        getSelectedAccountValue();
    });

    selectCompteBancaire.on('change', function (e) {

        e.preventDefault();

        getSelectedAccountValue();

    });

    btnPrintNP.click(function (e) {

        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir imprimer cette note de perception ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            printNP();
        });
    });

    btnSearchReception.on('click', function (e) {

        if (npPersonneReception === empty) {
            alertify.alert('Veuillez sélectionner l\'assujeti qui réceptionne la note de perception.');
            return;
        }
        e.preventDefault();
        assujettiModal.modal('show');
    });

});

function initNPDetails(npNumber) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    modalNpDetails.modal('show');
    npPrintCount = 0;
    btnSearchReception.attr('disabled', false);
    inputchoiceAddEchanceDate.show();

    for (var i = 0; i < NpList.length; i++) {

        if (NpList[i].NUMERO === npNumber) {

            npInitialDevise = NpList[i].INITIAL_DEVISE;
            npInitialAmout = NpList[i].MONTANT_INITIAL;
            npNumero = npNumber;
            npPrintCount = NpList[i].NBR_IMPRESSION;
            npNoteCalcul = NpList[i].NOTE_CALCUL;
            npPersonneReception = NpList[i].CODE;
            lblNotePerception.html(npNumber);
            lblNameAssujetti.html(NpList[i].ASSUJETTI);
            lblAdress.html(NpList[i].adresseAssujetti);
            labelReceptionniste.html(NpList[i].ASSUJETTI);
            npBankAccount = NpList[i].CompteBancaireCode;
            npDevise = NpList[i].DEVISE;

            if (npPrintCount > 0) {

                npPersonneReception = NpList[i].CODE_RECEPTIONNISTE;
                labelReceptionniste.html(NpList[i].RECEPTIONNISTE);
                btnSearchReception.attr('disabled', true);
                inputchoiceAddEchanceDate.hide();

            }

        }

    }

    addBanque();

    addAccount(selectBanque.val());

    getSelectedAccountValue();

    if (npPrintCount > 0) {
        var bankCode = getBankCodeByAccount(JSON.parse(userData.accountList), npBankAccount);
        selectBanque.html(fillSelect(bankCode));
        selectCompteBancaire.html(fillSelect(npBankAccount));
        calculNetPayer(npInitialAmout, npInitialDevise, npDevise);

    }
}

function fillSelect(val) {
    return '<option>' + val + '</option>';
}

function printNP() {

    var addEchance = 0;

    if (npPrintCount == 0) {

        var checkControl = document.getElementById("checkBoxEcheance");

        if (checkControl.checked) {
            addEchance = 1;
        } else {
            addEchance = 0;
        }
    }

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'NUMERO': npNumero,
            'NOTE_CALCUL': npNoteCalcul,
            'CompteBancaireCode': selectedAccountCode,
            'RECEPTIONNISTE': npPersonneReception,
            'TIMBRE': '',
            'PAPIER_SECURISE': '',
            'TAUX_APPLIQUE': taux,
            'INITIAL_DEVISE': npInitialDevise,
            'DEVISE': selectedAccountDevise,
            'netAPayer': npConvertedAmount,
            'MONTANT_INITIAL': npInitialAmout,
            'PRINT_COUNT': npPrintCount,
            'ADD_ECHEANCE': addEchance,
            'operation': 'PrintNotePerception'
        },
        beforeSend: function () {
            modalNpDetails.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {
                modalNpDetails.unblock();
                showResponseError();
                return;
            }
            if (response == '0') {
                modalNpDetails.unblock();
                showResponseError();
                return;
            }
            setTimeout(function () {

                modalNpDetails.modal('hide');
                modalNpDetails.unblock();
                setDocumentContent(response);

                setTimeout(function () {
                }, 2000);

                loadNotePerception(isAvancedSearch);

                window.open('visualisation-document-np', '_blank');
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalNpDetails.unblock();
            showResponseError();
        }

    });

}

function getSelectedAccountValue() {

    if (selectCompteBancaire.val() == null) {
        labelNpAmount.html(formatNumber(npInitialAmout, npInitialDevise));
        labelTaux.html('&nbsp;');
        labelNPConvertedAmout.html('&nbsp;');
        return;
    }

    var getAccountList = JSON.parse(userData.accountList);

    var accountList = getAccountByBanque(getAccountList, selectBanque.val());

    var selectedAccount = getAccountById(accountList, selectCompteBancaire.val());

    selectedAccountCode = selectCompteBancaire.val();
    selectedAccountDevise = selectedAccount.acountDevise;

    calculNetPayer(npInitialAmout, npInitialDevise, selectedAccountDevise);
}

function calculNetPayer(initialAmount, initialDevise, accountDevise) {

    npConvertedAmount = 0, taux = 0;
    labelNpAmount.html(formatNumber(npInitialAmout, initialDevise));

    if (initialDevise === accountDevise) {
        taux = 1;
        npConvertedAmount = initialAmount;
    } else {
        taux = getTaux(initialDevise.trim(), accountDevise.trim());
        npConvertedAmount = taux * initialAmount;
    }
    labelTaux.html('1 ' + initialDevise + ' ---> ' + taux + ' ' + accountDevise);
    labelNPConvertedAmout.html(formatNumber(npConvertedAmount, accountDevise));

}

function addBanque() {

    var BanqueSiteList = JSON.parse(userData.banqueUserList);

    var databanque = '';

    for (var i = 0; i < BanqueSiteList.length; i++) {

        databanque += '<option value="' + BanqueSiteList[i].banqueCode + '">' + BanqueSiteList[i].banqueName + '</option>';
    }

    selectBanque.html(databanque);
}

function addBanqueAccountAndDevise(banque, account) {


}

function addAccount(selectBanqueCode) {

    var getAccountList = JSON.parse(userData.accountList);

    var dataBankAccountList = '';

    var accountList = getAccountByBanque(getAccountList, selectBanqueCode);

    for (var i = 0; i < accountList.length; i++) {
        dataBankAccountList += '<option value="' + accountList[i].accountCode + '">' + accountList[i].accountCode + ' - ' + accountList[i].accountName + ' - ' + accountList[i].acountDevise + '</option>';
    }
    selectCompteBancaire.html(dataBankAccountList);
}

function getBankCodeByAccount(bankAccountList, account) {

    var banqueName;

    for (var i = 0; i < bankAccountList.length; i++) {

        if (account === bankAccountList[i].accountCode) {
            banqueName = bankAccountList[i].acountBanqueName;
            break;
        }

    }

    return banqueName;
}

function getAccountById(bankAccountList, account) {

    var selectAccount;

    for (var i = 0; i < bankAccountList.length; i++) {

        if (account === bankAccountList[i].accountCode) {
            selectAccount = bankAccountList[i];
            break;
        }

    }

    return selectAccount;
}


function getAccountByBanque(bankAccountList, selectBanque) {

    var accountList = [];

    for (var i = 0; i < bankAccountList.length; i++) {

        if (selectBanque === bankAccountList[i].acountBanqueCode) {

            accountList.push(bankAccountList[i]);
        }
        else if (selectBanque === '') {
            accountList.push(bankAccountList[i].acountBanqueCode);
        }

    }

    return accountList;
}


function getTaux(source, destination) {
    var taux = 1;
    var tauxList = JSON.parse(userData.tauxList);
    for (var i = 0; i < tauxList.length; i++) {

        if (tauxList[i].deviseSource === source && tauxList[i].deviseDestination === destination) {
            taux = tauxList[i].tauxValue;
        }
    }
    return taux;
}

function getSelectedAssujetiData() {
    npPersonneReception = selectAssujettiData.code;
    labelReceptionniste.html(selectAssujettiData.nomComplet);
}