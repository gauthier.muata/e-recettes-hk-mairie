
var
        PONC = 'PR0012015',
        JOUR = 'PR0022015',
        MENS = 'PR0032015',
        TRIME = 'PR0052015',
        SEMS = 'PR0072015',
        ANNEE = 'PR0042015';

var BIMEN = '';

var
        codeOfPersonne = '',
        codeTypeAssjetti = '',
        idOfBien = '',
        nombreJour = 0,
        nombreJourLimite = 0,
        nombreJourPaiement = '',
        echeanceLegale = '',
        periodeEcheance = '',
        codeAdressePersonne = '',
        idComplementBien = '',
        articleAssujettissableObj = null,
        codePeriodicite = '',
        selectTariftypePersonne = '';

var codeBienSelected;


var
        lblNameResponsible,
        lblComplement,
        lblADressePersonne,
        lblUniteBase;

var
        cmbBien,
        cmbArticleBudgetaire,
        cmbPeriodicite,
        cmbTarif,
        cmbMois,
        cmbAnnee;

var
        inputBase,
        inputUnite,
        inputEcheance,
        inputEcheancePaiement,
        inputValueResearchArticleAssujettissable,
        inputTaux,
        inputModalBaseCalcul;

var
        inputDayEcheance,
        inputMonthEcheance;

var
        btnSaveAssujettissement,
        btnCallAllAB,
        btnLauchSearchArticleAssujettissable,
        btnSearchProprietaire,
        btnSelectArticleAssujettissable,
        btnOtherArticleB,
        btnValidateInfoBase;

var
        btnEditELD,
        btnEditELP,
        btnValidateEcheance;

var
        tablePeriodes,
        tableSearchArticleAssujettissable;

var
        tempArticleBudgetaireList = [],
        tempPeriodiciteList = [],
        tempPeriodeDeclarationList = [],
        tempBienList,
        tempPaliers = [];

var
        articleAssujettissableModal,
        infosBaseCalculModal,
        editEcheancesLegalesModal;

var
        isPalierAB = 0,
        uniteAB,
        divBien;

var
        divCmbMois,
        divCmbAnnee,
        divPeriodEcheance,
        divCmbTarif,
        divInputTaux,
        divContenerEcheanceD,
        divContenerEcheanceP;

var
        selectMinistere,
        selectService;

var inputMontantLoyer, inputMontantLoyerAnnuel, inputSuperficieBatie;

var divML, divMLA, divSB;

var cbDeviseML, cbDeviseMLA, cbUniteSB;

var changeEcheance;
var tauxPalier2;

var articleB = '', libelleAB = '';

$(function () {

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Edition bien -> Nouvel assujettissement');
    removeActiveMenu();
    linkMenuRepertoire.addClass('active');
    inputBase = $('#inputBase');
    inputUnite = $('#inputUnite');
    inputEcheance = $('#inputEcheance');
    inputEcheancePaiement = $('#inputEcheancePaiement');
    inputValueResearchArticleAssujettissable = $('#inputValueResearchArticleAssujettissable');
    inputTaux = $('#inputTaux');
    inputModalBaseCalcul = $('#inputModalBaseCalcul');
    lblNameResponsible = $('#lblNameResponsible');
    lblComplement = $('#lblComplement');
    lblADressePersonne = $('#lblADressePersonne');
    lblUniteBase = $('#lblUniteBase');
    cmbBien = $('#cmbBien');
    cmbArticleBudgetaire = $('#cmbArticleBudgetaire');
    cmbPeriodicite = $('#cmbPeriodicite');
    cmbTarif = $('#cmbTarif');
    cmbMois = $('#cmbMois');
    cmbAnnee = $('#cmbAnnee');
    tablePeriodes = $('#tablePeriodes');
    tableSearchArticleAssujettissable = $('#tableSearchArticleAssujettissable');
    btnOtherArticleB = $('#btnOtherArticleB');
    btnValidateInfoBase = $('#btnValidateInfoBase');
    divBien = $('#divBien');
    articleAssujettissableModal = $('#articleAssujettissableModal');
    infosBaseCalculModal = $('#infosBaseCalculModal');
    editEcheancesLegalesModal = $('#editEcheancesLegalesModal');
    divCmbMois = $('#divCmbMois');
    divCmbAnnee = $('#divCmbAnnee');
    divPeriodEcheance = $('#divPeriodEcheance');
    divCmbTarif = $('#divCmbTarif');
    divInputTaux = $('#divInputTaux');
    divContenerEcheanceD = $('#divContenerEcheanceD');
    divContenerEcheanceP = $('#divContenerEcheanceP');
    divInfoAssujetissements = $('#divInfoAssujetissements');
    selectService = $('#selectService');
    selectMinistere = $('#selectMinistere');

    inputMontantLoyer = $("#inputMontantLoyer");
    inputMontantLoyerAnnuel = $("#inputMontantLoyerAnnuel");
    inputSuperficieBatie = $("#inputSuperficieBatie");

    cbDeviseML = $('#cbDeviseML');
    cbDeviseMLA = $('#cbDeviseMLA');
    cbUniteSB = $('#cbUniteSB');

    divML = $("#divML");
    divMLA = $("#divMLA");
    divSB = $("#divSB");

    divML.hide();
    divMLA.hide();
    divSB.hide();

    var urlId = getUrlParameter('id');
    var urlName = getUrlParameter('nom');
    var urlBien = getUrlParameter('bien');
    var typeCode = getUrlParameter('type');

    var ab = getUrlParameter('ab');
    var libAB = getUrlParameter('libAb');

    if (jQuery.type(urlId) !== 'undefined' && jQuery.type(urlName) !== 'undefined' && jQuery.type(urlName) !== 'undefined' && jQuery.type(typeCode) !== 'undefined') {
        codeOfPersonne = atob(urlId);
        codeTypeAssjetti = atob(typeCode);
        lblNameResponsible.html(atob(urlName));
        idOfBien = atob(urlBien);
        
        codeBienSelected = idOfBien;
        
    }

    if (jQuery.type(ab) !== 'undefined' && jQuery.type(libAB) !== 'undefined') {

        articleB = atob(ab);
        libelleAB = atob(libAB);
        btnOtherArticleB.hide();

        switch (articleB) {
            case AB_IRL:
                divMLA.show();
                break;
            case AB_IF:
            case AB_PUBLICITE:
            //case AB_VIGNETTE:
            case AB_ICM:
                divSB.show();
                break;
            case AB_RL:
                divML.show();
                break;
        }

    }

    if (idOfBien == '') {
        divBien.hide();
        btnOtherArticleB.attr('style', 'display:inline');
        $('#divAB').removeClass('selectABAll');
        $('#divAB').addClass('selectAB');
    } else {
        divBien.show();
        btnOtherArticleB.attr('style', 'display:none');
        $('#divAB').removeClass('selectAB');
        $('#divAB').addClass('selectABAll');
    }

    cmbAnnee.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbAnnee.val() == '0') {
            return;
        }

        if (codePeriodicite == '') {
            alertify.alert('Veuillez d\'abord sélectionner un article budgétaire.');
            return;
        }

        /*if (selectTariftypePersonne != '*') {

            if (selectTariftypePersonne != codeTypeAssjetti) {

                alertify.alert('Le type assujetti ne correspond pas à celui du tarif.');
                return;
            }
        }*/

        switch (codePeriodicite) {
            case PONC:
            case JOUR:
                break;
            case MENS:
            case TRIME:
            case SEMS:
                if (cmbMois.val() === '0') {
                    alertify.alert('Veuillez d\'abord choisir le mois.');
                    return;
                }
                break;
            case ANNEE:
                if (cmbAnnee.val() === '0') {
                    alertify.alert('Veuillez d\'abord choisir l\'année.');
                    return;
                }
        }

        generatePeriodesDeclarations(cmbMois.val(), cmbAnnee.val(), codePeriodicite);
    });

    cmbMois.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbMois.val() == '0') {
            return;
        }

        if (cmbAnnee.val() == '0') {
            return;
        }

        if (codePeriodicite == '') {
            alertify.alert('Veuillez d\'abord sélectionner un article budgétaire.');
            return;
        }

        /*if (selectTariftypePersonne != '*') {

            if (selectTariftypePersonne != codeTypeAssjetti) {

                alertify.alert('Le type assujetti ne correspond pas à celui du tarif.');
                return;
            }
        }*/

        generatePeriodesDeclarations(cmbMois.val(), cmbAnnee.val(), codePeriodicite);
    });

    articleAssujettissableModal.on('shown.bs.modal', function () {
        // jQuery(".chosen").chosen();
    });

    btnOtherArticleB.click(function (e) {
        e.preventDefault();
        printABAssujettissable('');
        articleAssujettissableModal.modal('show');
    });

    btnLauchSearchArticleAssujettissable = $('#btnLauchSearchArticleAssujettissable');
    btnLauchSearchArticleAssujettissable.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        loadArticlesBudgetairesAssujettissable();
    });

    btnSaveAssujettissement = $('#btnSaveAssujettissement');
    btnSaveAssujettissement.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();
    });

    btnSearchProprietaire = $('#btnSearchProprietaire');
    btnSearchProprietaire.click(function (e) {
        e.preventDefault();
        window.location = 'edition-bien?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);
    });

    btnSelectArticleAssujettissable = $('#btnSelectArticleAssujettissable');

    btnSelectArticleAssujettissable.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (articleAssujettissableObj == null) {
            alertify.alert('Veuillez d\'abord sélectionner un article budgétaire.');
            return;
        }

        refreshABAssujettissable();
    });

    cmbBien.on('change', function (e) {
        var idBien = cmbBien.val();
        refreshDataAB(idBien);
    });

    cmbArticleBudgetaire.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var codeAB = cmbArticleBudgetaire.val();

        if (codeAB == '0')
            return;

        var codePeriodiciteAB, codeTarif = '', codeTarifBien = '', paliers;
        resetDataAssujettissement();
        for (var i = 0; i < tempArticleBudgetaireList.length; i++) {

            if (tempArticleBudgetaireList[i].codeAB == codeAB) {

                idComplementBien = tempArticleBudgetaireList[i].idComplementBien;
                lblComplement.html(tempArticleBudgetaireList[i].libelleTypeComplement);
                uniteAB = tempArticleBudgetaireList[i].unite;
                inputBase.val(tempArticleBudgetaireList[i].valeur);
                inputUnite.val(tempArticleBudgetaireList[i].unite);
                nombreJourLimite = tempArticleBudgetaireList[i].nombreJourLimite;
                nombreJourPaiement = tempArticleBudgetaireList[i].nombreJourLimitePaiement;
                echeanceLegale = tempArticleBudgetaireList[i].echeanceLegale;
                periodeEcheance = tempArticleBudgetaireList[i].periodeEcheance;
                codePeriodiciteAB = tempArticleBudgetaireList[i].codePeriodiciteAB;
                codeTarif = tempArticleBudgetaireList[i].codeTarifAB;
                codeTarifBien = tempArticleBudgetaireList[i].codeTarifBien;
                paliers = tempArticleBudgetaireList[i].paliers;
                codePeriodicite = codePeriodiciteAB;
                if (tempArticleBudgetaireList[i].baseVariable == '0') {
                    inputBase.attr('readonly', 'true');
                } else {
                    inputBase.removeAttr('readonly');
                    lblUniteBase.html(tempArticleBudgetaireList[i].unite);
                    inputModalBaseCalcul.val(1);
                }

                adaptPanelAssujettissement(codePeriodicite);
                break;
            }
        }
        dataAssujettissement(codeAB, codePeriodiciteAB, codeTarifBien, paliers,codeBienSelected);
    });

    cmbPeriodicite.on('change', function (e) {
        var codePeriodiciteAB = cmbPeriodicite.val();
        nombreJour = getNombreJour(codePeriodiciteAB);
    });

    cmbTarif.on('change', function (e) {
        var codeTarif = cmbTarif.val();
        applyTauxTarif(codeTarif);
    });

    btnValidateInfoBase.click(function (e) {

        e.preventDefault();
        var baseCalcul = inputModalBaseCalcul.val();
        if (baseCalcul < 1) {
            baseCalcul = 1;
        }

        selectTauxByPalier(baseCalcul);
        var paramGuideValue = JSON.parse(getTourParam());
        if (paramGuideValue === 1) {
            Tour.run([
                {
                    element: $('#divInfoAssujetissements'),
                    content: '<p style="font-size:17px;color:black">Renseigner les informations sur l\'assujettissement puis enregistrer</p>',
                    language: 'fr',
                    position: 'top',
                    close: 'false'
                }
            ]);
        } else {
            Tour.run();
        }
    });

    printBien();
    printPeride('');
    loadMois();
    loadAnnee();
    getAllService(userData.allServiceList);

    if (controlAccess('2005')) {

        var data = '<div class="row"><div class="col-lg-11"><input readonly="true" type="text" class="form-control" id="inputEcheance"/></div>';
        data += '<div class="col-lg2-1"><a id="btnEditELD"  style="font-size: 15px"><i class="fa fa-edit"></i></a></div></div>';

        divContenerEcheanceD.html(data);

        inputEcheance = $('#inputEcheance');
        btnEditELD = $('#btnEditELD');
        btnEditELD.click(function (e) {
            e.preventDefault();
            editDefaultEcheances('DECLARATION');
        });

        data = '<div class="row"><div class="col-lg-11"><input readonly="true" type="text" class="form-control" id="inputEcheancePaiement"/></div>';
        data += '<div class="col-lg2-1"><a id="btnEditELP"  style="font-size: 15px"><i class="fa fa-edit"></i></a></div></div>';

        divContenerEcheanceP.html(data);
        inputEcheancePaiement = $('#inputEcheancePaiement');
        btnEditELP = $('#btnEditELP');
        btnEditELP.click(function (e) {
            e.preventDefault();
            editDefaultEcheances('PAIEMENT');
        });

        inputDayEcheance = $('#inputDayEcheance');
        setEventControl(inputDayEcheance, 'keyup', false);
        setEventControl(inputDayEcheance, 'change', false);

        inputMonthEcheance = $('#inputMonthEcheance');
        setEventControl(inputMonthEcheance, 'keyup', true);
        setEventControl(inputMonthEcheance, 'change', true);

        btnValidateEcheance = $('#btnValidateEcheance');
        btnValidateEcheance.click(function (e) {
            e.preventDefault();

            var type = $('#echeanceModalTitle').attr('name');

            var jour = (inputDayEcheance.val().length <= 1)
                    ? ('0' + inputDayEcheance.val())
                    : inputDayEcheance.val();

            if (codePeriodicite == 'ANNEE') {
                if (inputDayEcheance.val() == '' || inputMonthEcheance.val() == '') {
                    alertify.alert('Veuillez renseigner tous les champs.');
                    return;
                }

                var mois = (inputMonthEcheance.val().length <= 1)
                        ? ('0' + inputMonthEcheance.val())
                        : inputMonthEcheance.val();

                if (type == 'DECLARATION') {
                    echeanceLegale = '2000-' + mois + '-' + jour;
                } else {
                    nombreJourPaiement = '2000-' + mois + '-' + jour;
                }

            } else {
                if (inputDayEcheance.val() == '') {
                    alertify.alert('Veuillez renseigner le jour.');
                    return;
                }

                if (type == 'DECLARATION') {
                    echeanceLegale = '2000-01-' + jour;
                } else {
                    nombreJourPaiement = '2000-01-' + jour;
                }
            }

            var echeance;
            if (type == 'DECLARATION') {
                echeance = echeanceLegale.split('-');
                echeance = echeance[2] + '/' + echeance[1];
                inputEcheance.val(echeance + ' de chaque période');
            } else {
                echeance = nombreJourPaiement.split('-');
                echeance = echeance[2] + '/' + echeance[1];
                inputEcheancePaiement.val(echeance + ' de chaque période');
            }

            changeEcheance = '1';

            editEcheancesLegalesModal.modal('hide');
        });
    }

    changeEcheance = '0';
});

function setEventControl(control, event, forMonth) {

    control.on(event, function () {
        var data = control.val();
        control.val(getIntervalDayMonth(data, forMonth));
    });
}

function editDefaultEcheances(type) {

    if (cmbArticleBudgetaire.val() === '0' || cmbArticleBudgetaire.val() == null) {
        alertify.alert('Veuillez d\'abord sélectionner l\'article budgétaire.');
        return;
    }

    if (codePeriodicite == 'PONC' || codePeriodicite == 'JOUR') {
        alertify.alert('Echéance légale non disponible.');
        return;
    }

    if (type == 'DECLARATION') {

        if (inputEcheance.val() == defaultText) {
            alertify.alert('Echéance de déclaration non définie.');
            return;
        }

        $('#echeanceModalTitle').html('Edition de l\échéance - DECLARATION');
        $('#echeanceModalTitle').attr('name', 'DECLARATION');

    } else {

        if (inputEcheancePaiement.val() == defaultText) {
            alertify.alert('Echéance de paiement non définie.');
            return;
        }

        $('#echeanceModalTitle').html('Edition de l\échéance - PAIEMENT');
        $('#echeanceModalTitle').attr('name', 'PAIEMENT');
    }

    switch (codePeriodicite) {
        case ANNEE:
            inputMonthEcheance.show();
            $('#lblMonthEcheance').show();
            break;
        case MENS:
        case TRIME:
        case BIMEN:
        case SEMS:
            inputMonthEcheance.hide();
            $('#lblMonthEcheance').hide();
    }

    inputDayEcheance.val('');
    inputMonthEcheance.val('');
    editEcheancesLegalesModal.modal('show');
}

function getIntervalDayMonth(val, forMonth) {

    var month = inputMonthEcheance.val();
    var max = getDaysOfMonth(month);
    if (forMonth) {
        if (inputDayEcheance.val() != '') {
            var day = +(inputDayEcheance.val());
            if (day > max) {
                inputDayEcheance.val(max);
            }
        }
    }

    if (val > max)
        return max;
    if (val < 1)
        return 1;
    return val;
}

function resetDataAssujettissement() {

    lblComplement.text('');
    inputBase.val('1');
    inputUnite.val('');
    cmbPeriodicite.val(0);
    cmbTarif.val(0);
    cmbMois.val(0);
    cmbAnnee.val(0);
    inputEcheance.val('');
    codePeriodicite = '';
    printPeride('');
}

function resetAll() {

    cmbBien.val('0');
    cmbArticleBudgetaire.html('');
    lblComplement.text('');
    lblADressePersonne.text('');
    inputBase.val('1');
    inputUnite.val('');
    cmbPeriodicite.html('');
    inputEcheance.val('');
    inputEcheancePaiement.val();
    cmbTarif.html('');
    printPeride('');
    nombreJour = 0;
    nombreJourLimite = 0;
    nombreJourPaiement = '';
    echeanceLegale = '';
    periodeEcheance = '';
    codeAdressePersonne = '';
    idComplementBien = '';
    setTimeout(function () {
        location.reload(true);
    }, 1500);
}

/*function printBien() {
 
 var tempBienData = getBienData();
 var bienList = JSON.parse(tempBienData);
 tempBienList = bienList;
 var dataBien = '<option value ="0">-- Sélectionner --</option>';
 for (var i = 0; i < bienList.length; i++) {
 dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
 }
 
 cmbBien.html(dataBien);
 if (idOfBien !== '') {
 cmbBien.val(idOfBien);
 refreshDataAB(idOfBien);
 idOfBien = '';
 }
 }*/

function printBien() {

    var tempBienData = getBienData();
    var bienList = JSON.parse(tempBienData);
    tempBienList = bienList;
    var dataBien = '';

    if (articleB !== '') {
        for (var i = 0; i < bienList.length; i++) {
            if (idOfBien === bienList[i].idBien) {
                dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
            }
        }
    } else {
        dataBien += '<option value ="0">-- Sélectionner --</option>';
        for (var i = 0; i < bienList.length; i++) {
            dataBien += '<option value =' + bienList[i].idBien + '>' + bienList[i].intituleBien + '</option>';
        }
    }

    cmbBien.html(dataBien);

    if (idOfBien !== '') {
        cmbBien.val(idOfBien);
        refreshDataAB(idOfBien);
        idOfBien = '';
    }
}


function loadArticlesBudgetaires(idBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticlesBudgetaires',
            'idBien': idBien,
            'ab': articleB,
            'formeJuridique': codeTypeAssjetti
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var abList = $.parseJSON(JSON.stringify(response));
                var dataAB = '<option value ="0">-- Sélectionner --</option>';
                var select = '';
                tempArticleBudgetaireList = [];

                for (var i = 0; i < abList.length; i++) {

                    select = abList[i].codeAB == articleB ? 'selected' : '';
                    dataAB += '<option ' + select + ' value =' + abList[i].codeAB + '>' + abList[i].intituleAB + '</option>';

                    var article = {};
                    article.codeAB = abList[i].codeAB;
                    article.intituleAB = abList[i].intituleAB;
                    article.libelleTypeComplement = abList[i].libelleTypeComplement;
                    article.baseVariable = abList[i].baseVariable;
                    article.periodiciteVariable = abList[i].periodiciteVariable;
                    article.tarifVariable = abList[i].tarifVariable;
                    article.valeur = abList[i].valeur;
                    article.unite = abList[i].unite;
                    article.codePeriodiciteAB = abList[i].codePeriodiciteAB;
                    article.codeTarifAB = abList[i].codeTarifAB;
                    article.nombreJourLimite = abList[i].nombreJourLimite;
                    article.nombreJourLimitePaiement = abList[i].nbreJourLegalePaiement;
                    article.echeanceLegale = abList[i].echeanceLegale;
                    article.periodeEcheance = abList[i].periodeEcheance;
                    article.idComplementBien = abList[i].id;
                    article.paliers = abList[i].paliers;
                    article.codeTarifBien = abList[i].codeTarifBien;
                    article.intituleCOmplementIF = abList[i].intituleCOmplementIF;
                    article.ValeurCOmplementIF = abList[i].ValeurCOmplementIF;
                    article.codeUniteComplement = abList[i].codeUniteComplement;

                    tempArticleBudgetaireList.push(article);
                }

                cmbArticleBudgetaire.html(dataAB);
                cmbArticleBudgetaire.trigger('change');

                if (articleB !== '') {

                    $("#lblComplement").html(tempArticleBudgetaireList[0].intituleCOmplementIF);
                    inputSuperficieBatie.val(tempArticleBudgetaireList[0].ValeurCOmplementIF);
                    inputSuperficieBatie.attr('readOnly', true);
                    var optionComplement;
                    if(tempArticleBudgetaireList[0].codeUniteComplement === undefined) {
                        optionComplement = '<option value="" >Non défini</option>';
                        cbUniteSB.hide();
                    } else {
                        optionComplement = '<option value="' + tempArticleBudgetaireList[0].codeUniteComplement + '" >' + tempArticleBudgetaireList[0].codeUniteComplement + '</option>';
                    }
                    
                    cbUniteSB.html(optionComplement);

                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printPeride(periodeList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> PERIODE </th>';
    header += '<th scope="col"> ECHEANCE DECLARATION </th>';
    header += '<th scope="col"> ECHEANCE PAIEMENT </th>';
    header += '</tr></thead>';
    var body = '<tbody>';
    tempPeriodeDeclarationList = [];
    for (var i = 0; i < periodeList.length; i++) {

        var periode = new Object();
        periode.dateDebut = periodeList[i].dateDebut;
        periode.dateFin = periodeList[i].dateFin;
        periode.periode = periodeList[i].periode;
        periode.dateLimite = periodeList[i].dateLimite;
        periode.ordrePeriode = periodeList[i].ordrePeriode;
        periode.dateLimitePaiement = periodeList[i].dateLimitePaiement;
        tempPeriodeDeclarationList.push(periode);
        var color = periodeList[i].isEchus ? 'red' : 'black';
        var colorPaiement = periodeList[i].isEchusPaiement ? 'red' : 'black';
        body += '<tr>';
        body += '<td>' + periodeList[i].periode + '</td>';
        body += '<td style="color:' + color + '">' + periodeList[i].dateLimite + '</td>';
        body += '<td style="color:' + colorPaiement + '">' + periodeList[i].dateLimitePaiement + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tablePeriodes.html(tableContent);
    tablePeriodes.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function dataAssujettissement(codeAB, codePeriodiciteAB, codeTarifBien, paliers,codeBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDataAssujettissement',
            'formeJuridique': codeTypeAssjetti,
            'tarif': articleB === '' ? '' : codeTarifBien,
            'codeAB': codeAB,
            'codeBien': codeBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var dataAssujettissement = $.parseJSON(JSON.stringify(response));
                var periodicitesList = $.parseJSON(JSON.stringify(dataAssujettissement.listPeriodicites));
                var tarifsList = $.parseJSON(JSON.stringify(dataAssujettissement.listTarifs));
                tempPeriodiciteList = [];
                var dataPriodicite = '';
                for (var i = 0; i < periodicitesList.length; i++) {
                    var periodicite = new Object();
                    periodicite.codePeriodicite = periodicitesList[i].codePeriodicite;
                    periodicite.intitulePeriodicite = periodicitesList[i].intitulePeriodicite;
                    periodicite.nombreJour = periodicitesList[i].nombreJour;
                    tempPeriodiciteList.push(periodicite);
                    dataPriodicite += '<option value =' + periodicitesList[i].codePeriodicite + '>' + periodicitesList[i].intitulePeriodicite + '</option>';
                }
                cmbPeriodicite.html(dataPriodicite);
                cmbPeriodicite.val(codePeriodiciteAB);
                var listPalier = JSON.parse(JSON.stringify(dataAssujettissement.paliers));
                tempPaliers = [];
                for (var i = 0; i < listPalier.length; i++) {

                    var palierObj = {};
                    palierObj.codeTarif = listPalier[i].codeTarif;
                    palierObj.intituleTarif = listPalier[i].intituleTarif;
                    palierObj.tauxAB = listPalier[i].tauxAB;
                    palierObj.multiplierValeurBase = listPalier[i].multiplierValeurBase;
                    palierObj.palierTypeTaux = listPalier[i].palierTypeTaux;
                    palierObj.devise = listPalier[i].devise;
                    palierObj.unite = listPalier[i].unite;
                    palierObj.codeTypePersonne = listPalier[i].codeTypePersonne;
                    palierObj.borneInferieur = listPalier[i].borneInferieur;
                    palierObj.borneSuperieur = listPalier[i].borneSuperieur;
                    tempPaliers.push(palierObj);
                }

                nombreJour = getNombreJour(codePeriodiciteAB);

                if (isPalierAB == '1') {

                    lblUniteBase.html(uniteAB);
                    inputModalBaseCalcul.val(1);
                    infosBaseCalculModal.modal('show');
                    inputTaux.val(empty);

                } else {

                    var dataTarif = '';
                    for (var i = 0; i < tarifsList.length; i++) {
                        dataTarif += '<option value =' + tarifsList[i].codeTarif + '>' + tarifsList[i].intituleTarif + '</option>';
                    }

                    if (tempPaliers.length > 0) {
                        cmbTarif.html(dataTarif);
                        applyTauxTarif(cmbTarif.val());
                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function generatePeriodesDeclarations(mois, annee, codePeriodiciteAB) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'generatePeriodesDeclarations',
            'codePeriodiciteAB': codePeriodiciteAB,
            'mois': mois,
            'annee': annee,
            'nombreJour': nombreJour,
            'nombreJourLimite': nombreJourLimite,
            'nbreJourLegalePaiement': nombreJourPaiement,
            'echeanceLegale': echeanceLegale,
            'periodeEcheance': periodeEcheance
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var periodes = $.parseJSON(JSON.stringify(response));
                printPeride(periodes);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getNombreJour(codePeriodiciteAB) {

    var nbJour = 0;
    for (var i = 0; i < tempPeriodiciteList.length; i++) {
        if (tempPeriodiciteList[i].codePeriodicite == codePeriodiciteAB) {
            nbJour = tempPeriodiciteList[i].nombreJour;
            break;
        }
    }
    return nbJour;
}

function saveAssujettissement() {

    var periodesDeclarations = JSON.stringify(tempPeriodeDeclarationList);
    var debut = '', fin = '';
    var size = tempPeriodeDeclarationList.length;
    if (size > 0) {
        debut = getDatePeriode(1, true);
        fin = getDatePeriode(size, false);
    }

    var nvlEcheanceObj = '';
    if (changeEcheance == '1') {
        nvlEcheanceObj = {};
        nvlEcheanceObj.echeanceLegale = echeanceLegale;
        nvlEcheanceObj.dateLimitePaiement = nombreJourPaiement;
        nvlEcheanceObj = JSON.stringify(nvlEcheanceObj);
    }

    var value;

    switch (parseInt(inputBase.val())) {
        case 0:
        case 1:
            value = tauxPalier2;
            break;
        default:
            value = inputBase.val();
            break;
    }

    var uniteValeur = '';

    switch (articleB) {
        case AB_IRL:
            uniteValeur = cbDeviseMLA.val();
            value = inputMontantLoyerAnnuel.val();
            break;
        case AB_IF:
        case AB_ICM:
        case AB_PUBLICITE:
            uniteValeur = cbUniteSB.val();
            value = inputSuperficieBatie.val();
            break;
        case AB_RL:
            uniteValeur = cbDeviseML.val();
            value = inputMontantLoyer.val();
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': '204',
            'idBien': cmbBien.val(),
            'codeAB': cmbArticleBudgetaire.val(),
            'reconduction': 0,
            'dateDebut': debut,
            'dateFin': fin,
            'valeurBase': value,
            'codePersonne': codeOfPersonne,
            'nombreJourLimite': nombreJourLimite,
            'codePeriodicite': cmbPeriodicite.val(),
            'codeTarif': cmbTarif.val(),
            'codeGestionnaire': userData.idUser,
            'codeAP': codeAdressePersonne,
            'id': idComplementBien,
            'periodesDeclarations': periodesDeclarations,
            'idUser': userData.idUser,
            'nvlEcheance': nvlEcheanceObj,
            'uniteValeur': uniteValeur
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'assujettissement ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                if (response == '1') {
                    alertify.alert('L\'assujettissement est enregistré avec succès.');
                    setTimeout(function () {
                        window.location = 'gestion-bien?id=' + btoa(codeOfPersonne);
                    }, 1500);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de l\'assujettissement.');
                } else if (response == '2') {
                    var errorMessage = empty;
                    if (idOfBien == '')
                    {
                        errorMessage = 'Il existe déjà un assujettissement en cours avec le même article budgétaire et tarif';
                    } else {
                        errorMessage = 'Il existe déjà un assujettissement en cours pour ce bien et avec le même article budgétaire';
                    }
                    alertify.alert(errorMessage);
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkFields() {

    if (cmbArticleBudgetaire.val() === '0' || cmbArticleBudgetaire.val() == null) {
        alertify.alert('Veuillez d\'abord sélectionner l\'article budgétaire.');
        return;
    }

    if (cmbPeriodicite.val() === '0' || cmbPeriodicite.val() == null) {
        alertify.alert('Veuillez d\'abord sélectionner une périodicité pour cet article budgétaire.');
        return;
    }

    if (cmbTarif.val() === '0' || cmbTarif.val() == null) {
        alertify.alert('Veuillez d\'abord sélectionner un tarif.');
        return;
    }

    if (tempPeriodeDeclarationList.length == 0) {
        alertify.alert('Aucune période de déclaration pour la période sélectionnée.');
        return;
    }

    /*if (selectTariftypePersonne != '*') {

        if (selectTariftypePersonne != codeTypeAssjetti) {

            alertify.alert('Le type assujetti ne correspond pas à celui du tarif.');
            return;
        }
    }*/

    alertify.confirm('Etes-vous sûre de vouloir enregistrer cet assujettissement ?', function () {
        saveAssujettissement();
    });
}

function refreshDataAB(idBien) {

    resetDataAssujettissement();
    for (var i = 0; i < tempBienList.length; i++) {

        if (tempBienList[i].idBien == idBien) {
            var chaineAdresse = tempBienList[i].chaineAdresse;
            if (chaineAdresse === '') {
                lblADressePersonne.html('Aucune adresse du bien trouvée<br/><br/>');
            } else {
                lblADressePersonne.html('Adresse du bien : ' + chaineAdresse.toUpperCase() + '<br/><br/>');
            }
            codeAdressePersonne = tempBienList[i].codeAP;
        }
    }

    loadArticlesBudgetaires(idBien);
}

function getDatePeriode(ordre, debut) {

    var result = '';
    for (var i = 0; i < tempPeriodeDeclarationList.length; i++) {
        if (ordre == tempPeriodeDeclarationList[i].ordrePeriode) {
            if (debut == true) {
                result = tempPeriodeDeclarationList[i].dateDebut;
            } else {
                result = tempPeriodeDeclarationList[i].dateFin;
            }
            break;
        }
    }
    return result;
}

function loadArticlesBudgetairesAssujettissable() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadArticleBudgetaireAssujettissable',
            'codeService': selectService.val(),
            'codeSite': userData.SiteCode,
            'codeEntite': userData.entiteCode,
            'libelle': inputValueResearchArticleAssujettissable.val()
        },
        beforeSend: function () {

            articleAssujettissableModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {

                articleAssujettissableModal.unblock();
                showResponseError();
                return;
            }

            if (response == '0') {
                articleAssujettissableModal.unblock();
                printABAssujettissable(empty);
                alertify.alert('Aucun article budgétaire trouvé.');
                return;
            }

            setTimeout(function () {

                var abList = $.parseJSON(JSON.stringify(response));
                if (abList.length > 0) {

                    articleAssujettissableModal.unblock();
                    printABAssujettissable(abList);
                    btnSelectArticleAssujettissable.show();
                } else {
                    btnSelectArticleAssujettissable.hide();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            articleAssujettissableModal.unblock();
            showResponseError();
        }

    });
}

function printABAssujettissable(abList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" hidden="true">Code</th>';
    header += '<th scope="col">ARTICLE BUDGETAIRE</th>';
    header += '<th scope="col">FAIT GENERATEUR</th>';
    header += '<th scope="col">SERVICE D\'ASSIETTE</th>';
    header += '<th scope="col" hidden="true">Code periodicité</th>';
    header += '<th scope="col" hidden="true">Code tarif</th>';
    header += '<th scope="col" hidden="true">Périodicité variable</th>';
    header += '<th scope="col" hidden="true">Tarif variable</th>';
    header += '<th scope="col" hidden="true">Unité</th>';
    header += '<th scope="col" hidden="true">Nombre jour limite</th>';
    header += '<th scope="col" hidden="true">Echéance légale</th>';
    header += '<th scope="col" hidden="true">Période echéance</th>';
    header += '<th scope="col" hidden="true">Paliers</th>';
    header += '<th scope="col">PERIODICITE</th>';
    header += '<th scope="col" hidden="true"></th>';
    header += '<th scope="col" hidden="true"></th>';
    header += '<th scope="col" hidden="true"></th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyArticles">';

    var firstLineAB = '';

    for (var i = 0; i < abList.length; i++) {

        if (abList[i].intituleAB.length > 75) {
            firstLineAB = abList[i].intituleAB.substring(0, 75) + ' ...';
        } else {
            firstLineAB = abList[i].intituleAB;
        }

        body += '<tr>';
        body += '<td hidden="true">' + abList[i].codeAB + '</td>';
        body += '<td style="vertical-align:middle" title="' + abList[i].intituleAB + '"><span style="font-weight:bold;">' + abList[i].codeOfficiel + '</span><br/>' + firstLineAB + '</td>';
        body += '<td>' + abList[i].intituleArticleGenerique.toUpperCase() + '</td>';
        body += '<td>' + abList[i].ministereLibelle.toUpperCase() + ' / ' + abList[i].secteurActivite.toUpperCase() + '</td>';
        body += '<td hidden="true">' + abList[i].codePeriodiciteAB + '</td>';
        body += '<td hidden="true">' + abList[i].codeTarifAB + '</td>';
        body += '<td hidden="true">' + abList[i].periodiciteVariable + '</td>';
        body += '<td hidden="true">' + abList[i].tarifVariable + '</td>';
        body += '<td hidden="true">' + abList[i].unite + '</td>';
        body += '<td hidden="true">' + abList[i].nombreJourLimite + '</td>';
        body += '<td hidden="true">' + abList[i].echeanceLegale + '</td>';
        body += '<td hidden="true">' + abList[i].periodeEcheance + '</td>';
        body += '<td hidden="true">' + JSON.stringify(abList[i].paliers) + '</td>';
        body += '<td>' + abList[i].libellePeriodiciteAB.toUpperCase() + '</td>';
        body += '<td hidden="true">' + abList[i].codeOfficiel + ' / ' + abList[i].intituleAB + '</td>';
        body += '<td hidden="true">' + abList[i].nbreJourLegalePaiement + '</td>';
        body += '<td hidden="true">' + abList[i].isPalier + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableSearchArticleAssujettissable.html(tableContent);
    var dtArticleAssujettissable = tableSearchArticleAssujettissable.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        columnDefs: [
            {"visible": false, "targets": 3}
        ],
        order: [[3, 'asc']],
        pageLength: 5,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 5,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(3, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6;font-weight:bold"  class="group"><td colspan="4">' + group + '</td></tr>'
                            );
                    last = group;
                }
            });
        }
    });

    $('#tableSearchArticleAssujettissable tbody').on('click', 'tr', function () {
        var data = dtArticleAssujettissable.row(this).data();
        articleAssujettissableObj = {};
        articleAssujettissableObj.codeAB = data[0];
        articleAssujettissableObj.intituleAB = data[14];
        articleAssujettissableObj.intituleArticleGenerique = data[2];
        articleAssujettissableObj.codePeriodiciteAB = data[4];
        articleAssujettissableObj.codeTarifAB = data[5];
        articleAssujettissableObj.periodiciteVariable = data[6];
        articleAssujettissableObj.tarifVariable = data[7];
        articleAssujettissableObj.unite = data[8];
        articleAssujettissableObj.nombreJourLimite = data[9];
        articleAssujettissableObj.echeanceLegale = data[10];
        articleAssujettissableObj.periodeEcheance = data[11];
        articleAssujettissableObj.paliers = data[12];
        articleAssujettissableObj.nombreJourLimitePaiement = data[15];
        articleAssujettissableObj.isPalier = data[16];
        isPalierAB = data[16];
        uniteAB = articleAssujettissableObj.unite;
    });
}

function refreshABAssujettissable() {

    var codeAB = articleAssujettissableObj.codeAB;
    var codePeriodiciteAB = articleAssujettissableObj.codePeriodiciteAB;
    var codeTarifAB = articleAssujettissableObj.codeTarifAB;
    var paliers = articleAssujettissableObj.paliers;
    resetDataAssujettissement();
    codePeriodicite = codePeriodiciteAB;
    idComplementBien = '';
    lblComplement.text('');
    inputUnite.val(articleAssujettissableObj.unite);
    nombreJourLimite = articleAssujettissableObj.nombreJourLimite;
    echeanceLegale = articleAssujettissableObj.echeanceLegale;
    periodeEcheance = articleAssujettissableObj.periodeEcheance;
    nombreJourPaiement = articleAssujettissableObj.nombreJourLimitePaiement;
    adaptPanelAssujettissement(codePeriodicite);
    var dataAB = '<option value =' + articleAssujettissableObj.codeAB + '>' + articleAssujettissableObj.intituleAB + '</option>';
    cmbArticleBudgetaire.html(dataAB);
    dataAssujettissement(codeAB, codePeriodiciteAB, codeTarifAB, paliers,codeBienSelected);
    articleAssujettissableObj = null;
    articleAssujettissableModal.modal('hide');
}

function loadMois() {

    var dataMois = '<option value ="0">-- Mois --</option>';
    dataMois += '<option value =01> Janvier </option>';
    dataMois += '<option value =02> Février </option>';
    dataMois += '<option value =03> Mars </option>';
    dataMois += '<option value =04> Avril </option>';
    dataMois += '<option value =05> Mai </option>';
    dataMois += '<option value =06> Juin </option>';
    dataMois += '<option value =07> Juillet </option>';
    dataMois += '<option value =08> Aôut </option>';
    dataMois += '<option value =09> Septembre </option>';
    dataMois += '<option value =10> Octobre </option>';
    dataMois += '<option value =11> Novembre </option>';
    dataMois += '<option value =12> Décembre </option>';
    cmbMois.html(dataMois);
}

function loadAnnee() {

    var anneeDebut = 2000;
    var date = new Date();
    var anneeFin = date.getFullYear();
    var dataAnnee = '<option value ="0">-- Année --</option>';
    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }
    cmbAnnee.html(dataAnnee);
}

function adaptPanelAssujettissement(codePeriodicite) {

    var echeanceMonth = '0';
    var echeanceYear = '0';
    var paiementEcheanceMonth = '0';
    var paiementEcheanceyear = '0';
    defaultText = 'non défini';

    if (echeanceLegale != '') {
        var echeance = echeanceLegale.split('-');
        echeanceMonth = echeance[2];
        echeanceYear = echeance[2] + '/' + echeance[1];
    }

    if (nombreJourPaiement != '') {
        var echeance = nombreJourPaiement.split('-');
        paiementEcheanceMonth = echeance[2];
        paiementEcheanceyear = echeance[2] + '/' + echeance[1];
    }

    switch (codePeriodicite) {
        case PONC:
        case JOUR:
            divPeriodEcheance.hide();
            generatePeriodesDeclarations(cmbMois.val(), cmbAnnee.val(), codePeriodicite);
            break;
        case MENS:
        case TRIME:
        case BIMEN:
        case SEMS:
            inputEcheance.val(echeanceMonth + " de chaque période");
            inputEcheancePaiement.val(paiementEcheanceMonth + " de chaque période");
            divCmbAnnee.attr('class', 'col-lg-2');
            divCmbMois.show();
            divPeriodEcheance.show();
            break;
        case ANNEE:
            inputEcheance.val(echeanceYear + " de chaque année");
            inputEcheancePaiement.val(paiementEcheanceyear + " de chaque année");
            divCmbAnnee.attr('class', 'col-lg-4');
            divCmbMois.hide();
            divPeriodEcheance.show();
            break;
    }

    if (echeanceLegale == '') {
        inputEcheance.val(defaultText);
    }

    if (nombreJourPaiement == '') {
        inputEcheancePaiement.val(defaultText);
    }

}

function selectTauxByPalier(value) {

    var palier = null;
    for (var i = 0; i < tempPaliers.length; i++) {

        if ((value >= tempPaliers[i].borneInferieur && value <= tempPaliers[i].borneSuperieur)) {

            selectTariftypePersonne = tempPaliers[i].codeTypePersonne;
            var dataTarif = '<option value =' + tempPaliers[i].codeTarif + '>' + tempPaliers[i].intituleTarif + '</option>';
            cmbTarif.html(dataTarif);
            palier = tempPaliers[i];
            inputBase.val(value);
            break;
        }
    }

    if (palier == null) {
        alertify.alert('Aucun tarif trouvé. Veuillez configurer correctement l\'article budgétaire');
        return;
    } else {

        articleAssujettissableModal.modal('hide');
        infosBaseCalculModal.modal('hide');
        divInputTaux.attr('class', 'col-lg-4');
        divCmbTarif.hide();
        divInputTaux.show();
        var tauxPalier = tempPaliers[i].palierTypeTaux == '%' ? tempPaliers[i].tauxAB + ' %'
                : formatNumber(tempPaliers[i].tauxAB, tempPaliers[i].devise);
        inputTaux.val(tauxPalier);
        tauxPalier2 = parseFloat(tauxPalier);

        if (tempPaliers[i].multiplierValeurBase == '1') {

            var unite = tempPaliers[i].unite;
            if (tempPaliers[i].palierTypeTaux == '%') {
                inputTaux.val(tauxPalier + ' de ' + unite);
            } else {
                inputTaux.val(tauxPalier + ' / ' + unite);
            }

        }

    }

}

function applyTauxTarif(codeTarif) {

    if (tempPaliers.length == 1) {
        divInputTaux.attr('class', 'col-lg-4');
        divCmbTarif.hide();
        divInputTaux.show();
    } else {
        divInputTaux.attr('class', 'col-lg-2');
        divCmbTarif.show();
    }

    for (var i = 0; i < tempPaliers.length; i++) {

        if (tempPaliers[i].codeTarif == codeTarif) {

            selectTariftypePersonne = tempPaliers[i].codeTypePersonne;

            var libTarif = tempPaliers[i].intituleTarif;
            var tauxPalier = tempPaliers[i].palierTypeTaux == '%' ? tempPaliers[i].tauxAB + ' %'
                    : formatNumber(tempPaliers[i].tauxAB, tempPaliers[i].devise);

            tauxPalier2 = parseFloat(tauxPalier);

            inputTaux.val(libTarif + ' (' + tauxPalier + ')');

            if (tempPaliers[i].multiplierValeurBase == '1') {

                var unite = tempPaliers[i].unite;

                inputTaux.attr('readonly', 'true');

                if (tempPaliers[i].palierTypeTaux == '%') {
                    inputTaux.val(libTarif + ' (' + tauxPalier + ' de ' + unite + ')');
                } else {
                    inputTaux.val(libTarif + ' (' + tauxPalier + ' / ' + unite + ')');
                }

            } else {

                inputBase.val('1');
                inputBase.attr('readonly', 'true');
            }

            break;
        }
    }
}

function getAllService(AllServiceList) {

    var data = '';
    data += '<option value="' + userData.serviceCode + '">' + userData.serviceAssiette + '</option>';
    selectService.html(data);
    //selectService.trigger("chosen:updated");
}
