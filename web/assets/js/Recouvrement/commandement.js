/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var inputSearchValue;
var selectSearchCommandement;
var btnShowAdvancedCommandement, btnSimpleSearchCommandement,
        btnAdvencedSearchRole, btnImprimerCommandement;
var tableRegistreCommandement, tableRegistreCommandement2, tableNouveauTitrePerception;
var tempCommandementList;
var inputDateDebut, inputdateLast;
var selectService, selectSite;
var modalDetailCommandement;
var lblArticleRole, lblNameAssujetti;
var idCommandement, idAssujetti, solde, amountCmd = 0;
var btnGenerateATD, btnGenerateSaisie, btnShowAccuserReception;
var typeSearch = '1', isAdvanced;

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblProvince, lblEntite, lblAgent;
var isAdvance;

var advancedSearchParam;
var codeAssujetti = '';

$(function () {

    mainNavigationLabel.text('POURSUITE');
    secondNavigationLabel.text('Registre des commandements');

    removeActiveMenu();
    linkMenuPoursuite.addClass('active');

    if (!controlAccess('10003')) {
        window.location = 'dashboard';
    }

    inputSearchValue = $('#inputSearchValue');
    selectSearchCommandement = $('#selectSearchCommandement');
    btnShowAdvancedCommandement = $('#btnShowAdvancedCommandement');
    btnSimpleSearchCommandement = $('#btnSimpleSearchCommandement');
    btnAdvencedSearchRole = $('#btnAdvencedSearchRole');
    btnImprimerCommandement = $('#btnImprimerCommandement');
    tableRegistreCommandement = $('#tableRegistreCommandement');
    tableRegistreCommandement2 = $('#tableRegistreCommandement2');
    tableNouveauTitrePerception = $('#tableNouveauTitrePerception');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    selectService = $('#selectService');
    selectSite = $('#selectSite');
    modalDetailCommandement = $('#modalDetailCommandement');
    btnGenerateATD = $('#btnGenerateATD');
    btnGenerateSaisie = $('#btnGenerateSaisie');
    btnShowAccuserReception = $('#btnShowAccuserReception');
    lblArticleRole = $('#lblArticleRole');
    lblNameAssujetti = $('#lblNameAssujetti');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');
    lblAgent = $('#lblAgent');

    selectSearchCommandement.on('change', function () {
        var critere = selectSearchCommandement.val();
        if (critere == '1') {
            typeSearch = '1';
            inputSearchValue.attr('placeholder', 'Article du rôle');
            inputSearchValue.removeAttr('disabled');
            inputSearchValue.val('');
        } else {
            typeSearch = '2';
            inputSearchValue.attr('placeholder', 'Assujetti');
            inputSearchValue.attr('disabled', 'true');
            inputSearchValue.val('');
            assujettiModal.modal('show');
        }
    });

    btnShowAdvancedCommandement.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        modalRechercheAvanceeNC.modal('show');
    });

    btnSimpleSearchCommandement.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var critere = selectSearchCommandement.val();

        if (critere == '2') {
            typeSearch = '2';
            assujettiModal.modal('show');
        } else {
            typeSearch = '1';
            isAdvanced = '0';
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            loadCommandement();
        }
    });

    btnAdvencedSearch.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        isAdvanced = '1';
        modalRechercheAvanceeNC.modal('hide');
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        loadCommandement();
    });

    btnShowAccuserReception.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        initAccuseReceptionUI(idCommandement, 'CD');
    });

    btnGenerateATD.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        alertify.confirm('Etes-vous sûre de vouloir générer un avis à tiers détenteurs ?', function () {
            generateAtdLs(1);
        });
    });

    btnGenerateSaisie.click(function (e) {
        e.preventDefault();
        alertify.confirm('Etes-vous sûre de vouloir générer le procès verbal de saisie ?', function () {
            generateAtdLs(0);
        });
    });

    btnImprimerCommandement.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        imprimerCommandement();
    });

    setTimeout(function () {

        var paramDashboard = getDashboardParam();

        if (paramDashboard == 'null') {

            setTimeout(function () {
                btnAdvencedSearch.trigger('click');
            }, 1000);

        } else {

            modeResearch = '2';
            advancedSearchParam = JSON.parse(paramDashboard);

            setTimeout(function () {
                isAdvanced = '1';
                loadCommandement();
            }, 1000);

        }

    }, 1000);

});

function printCommandement(result) {

    var etatCommandement = "";
    tempCommandementList = [];

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
    header += '<th>ASUUJETI</th>';
    header += '<th>ARTICLE EXTRAIT DE RÔLE</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th>DATE RECEPTION</th>';
    header += '<th>DATE EXI.</th>';
    header += '<th>ETAT</th>';
    header += '<th style="text-align:right">MONTANT DÛ</th>';
    header += '<th style="text-align:right">FRAIS DE POURSUITE</th>';
    header += '<th style="text-align:right">MONTANT PAYE</th>';
    header += '<th style="text-align:right">RESTE A PAYER</th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var data = '';

    var sumMontantDuCDF = 0;
    var sumMontantPayerCDF = 0;
    var sumFraisPoursuite = 0;

    for (var i = 0; i < result.length; i++) {
       
        var montantDuExtrait = getSumMontantDuExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantDuCDF += montantDuExtrait - result[i].totalFraisPoursuite;
        var montantPayeExtrait = getSumMontantPayeExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantPayerCDF += montantPayeExtrait;
        sumFraisPoursuite += result[i].totalFraisPoursuite;
        var RestePayerExtrait = montantDuExtrait - montantPayeExtrait;

        var commandement = {};

        var restePayerNotPoursuite = (montantDuExtrait - result[i].totalFraisPoursuite) - (montantPayeExtrait - result[i].totalPayeFraisPoursuite);

        commandement.commandementId = result[i].commandementId;
        commandement.articleRole = result[i].articleRole;
        commandement.assujettiName = result[i].assujettiName;
        commandement.codeAssujetti = result[i].assujettiCode;
        commandement.titrePerceptionList = result[i].titrePerceptionList;
        commandement.dateCreateCommandement = result[i].dateCreateCommandement;
        commandement.dateReceptionCommandement = result[i].dateReceptionCommandement;
        commandement.echeanceCommandementDepasser = result[i].echeanceCommandementDepasser;
        commandement.restePayerCommandement = RestePayerExtrait
        commandement.amountCmd = restePayerNotPoursuite;
        commandement.etat = result[i].stateCommandement;
        commandement.commandementExistsAtd = result[i].commandementExistsAtd;
        commandement.commandementExistsSaisie = result[i].commandementExistsSaisie;
        commandement.codeOfficiel = result[i].codeOfficiel;
        tempCommandementList.push(commandement);

        var color = ';color: black';
        if (result[i].echeanceCommandementDepasser == 1 &&
                RestePayerExtrait > 0) {
            color = ';color: red';
        }

        if (result[i].niveauExtraitRole == 'N5') {
            etatCommandement = "(Niveau : ATD)";
        } else if (result[i].niveauExtraitRole == 'N4') {
            etatCommandement = "(Niveau : PV DE SAISIE)";
        } else if (result[i].niveauExtraitRole == 'N3') {
            etatCommandement = "(Niveau : COMMANDEMENT)";
        } else if (result[i].niveauExtraitRole == 'N2') {
            etatCommandement = "(Niveau : CONTRAINTE)";
        } else if (result[i].niveauExtraitRole == 'N1') {
            etatCommandement = "(Niveau : DERNIER AVERTISSEMENT)";
        } else if (result[i].niveauExtraitRole == 'N0') {

            if (result[i].stateCommandement == 1) {
                etatCommandement = "VALIDER";
            } else if (result[i].stateCommandement == 2) {
                etatCommandement = "EN ATTENTE DE VALIDATION";
            } else {
                etatCommandement = "";
            }

        }

        var firstLineASS = '';
        var firstLineAB = '';

        if (result[i].assujettiName.length > 30) {
            firstLineASS = result[i].assujettiName.substring(0, 30) + ' ...';
        } else {
            firstLineASS = result[i].assujettiName;
        }

        if (result[i].articleRole.length > 40) {
            firstLineAB = result[i].articleRole.substring(0, 40) + ' ...';
        } else {
            firstLineAB = result[i].articleRole;
        }

        data += '<tbody id="bodyTable">';
        data += '<tr>';
        data += '<td style="text-align:left;width:15%" title="' + result[i].assujettiName + '">' + firstLineASS + '</td>';
        data += '<td style="text-align:left;width:12%;" title="' + result[i].articleRole + '">' + firstLineAB + '</td>';
        data += '<td style="text-align:left">' + result[i].dateCreateCommandement + '</td>';
        data += '<td style="text-align:left">' + result[i].dateReceptionCommandement + '</td>';
        data += '<td style="text-align:left' + color + '">' + result[i].dateEcheanceCommandement + '</td>';
        data += '<td style="text-align:left">' + etatCommandement + '</td>';
        data += '<td style="text-align:right">' + formatNumber((montantDuExtrait - result[i].totalFraisPoursuite), 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(result[i].totalFraisPoursuite, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantPayeExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(RestePayerExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:center;width:6%;vertical-align:middle;"> <a  onclick="showModalDetailCommandement(\'' + result[i].commandementId + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreCommandement.html(TableContent);

    tableRegistreCommandement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF'));

            $(api.column(7).footer()).html(
                    formatNumber(sumFraisPoursuite, 'CDF'));

            $(api.column(8).footer()).html(
                    formatNumber(sumMontantPayerCDF, 'CDF'));

            $(api.column(9).footer()).html(
                    formatNumber((sumMontantDuCDF + sumFraisPoursuite) - sumMontantPayerCDF, 'CDF'));
        }
    });
}

function loadCommandement() {

    if (isAdvanced == '1') {

        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;

            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
            lblProvince.attr('title', province);

            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
            lblEntite.attr('title', entite);

            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
            lblService.attr('title', service);

            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
            lblSite.attr('title', site);

            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
            lblAgent.attr('title', agent);

            lblDateDebut.text(advancedSearchParam.dateDebut);
            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }
    }

    var valueSearch = inputSearchValue.val().trim();

    if (selectSearchCommandement.val() === '2') {
        valueSearch = codeAssujetti;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadCommandement',
            'typeSearch': typeSearch,
            'isAdvanced': isAdvanced,
            'dateDebut': advancedSearchParam.dateDebut,
            'dateFin': advancedSearchParam.dateFin,
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'valueSearch': valueSearch,
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeSite': JSON.stringify(advancedSearchParam.site),
            'isHuissier': userData.isHuissier,
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                printCommandement('');
                return;
            }

            setTimeout(function () {
                var commandementList = JSON.parse(JSON.stringify(response));
                printCommandement(commandementList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function showModalDetailCommandement(commandementId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    modalDetailCommandement.modal('show');

    for (var i = 0; i < tempCommandementList.length; i++) {

        if (tempCommandementList[i].commandementId == commandementId) {

            lblArticleRole.html(tempCommandementList[i].articleRole);
            lblNameAssujetti.html(tempCommandementList[i].assujettiName);
            idCommandement = commandementId;
            idAssujetti = tempCommandementList[i].codeAssujetti;

            amountCmd = tempCommandementList[i].amountCmd;

            var detailTitrePerception = JSON.parse(tempCommandementList[i].titrePerceptionList);
            printTitrePerception(detailTitrePerception);

            if (tempCommandementList[i].dateReceptionCommandement != empty) {
                
                btnShowAccuserReception.attr('style', 'display: none');
                
            } else {
                
                btnShowAccuserReception.removeAttr('style', 'display: inline');
            }

            if (tempCommandementList[i].etat == 4) {
                
                btnGenerateATD.attr('style', 'display: none');
                btnGenerateSaisie.attr('style', 'display: none');
                
            } else {
                
                if (tempCommandementList[i].echeanceCommandementDepasser == 1 &&
                        tempCommandementList[i].restePayerCommandement > 0) {

                    if (tempCommandementList[i].commandementExistsAtd == '1') { 
                        
                        btnGenerateATD.attr('style', 'display: none');
                        
                    } else {
                        
                        btnGenerateATD.removeAttr('style', 'display: inline');
                        btnGenerateATD.attr('style', 'margin-right:3px');
                    }

                    if (tempCommandementList[i].commandementExistsSaisie == '1') {
                        
                        btnGenerateSaisie.attr('style', 'display: none');
                        
                    } else {
                        btnGenerateSaisie.removeAttr('style', 'display: inline');
                    }

                } else {
                    
                    btnGenerateATD.attr('style', 'display: none');
                    btnGenerateSaisie.attr('style', 'display: none');
                }
            }

            if (tempCommandementList[i].restePayerCommandement == 0) {
                
                btnGenerateATD.attr('style', 'display: none');
                btnGenerateSaisie.attr('style', 'display: none');
            }

        }
    }
}

function printTitrePerception(listDetailRole) {

    tempExtraitRoleId = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col"> EXERCICE </th>';
    header += '<th scope="col"> SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col"> TYPE </th>';
    header += '<th scope="col"> TYPE DOCUMENT </th>';
    header += '<th scope="col"> TITRE DE PERCEPTION</th>';
    header += '<th scope="col"> DATE EXIGIBILITE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th scope="col" style="text-align:right"> PENALITE DÛ </th>';
    header += '<th scope="col" style="text-align:right"></th>';

    header += '</tr></thead>';

    var body = '<tbody>';
    var firstLineAB = '';
    var firstLineSA = '';

    for (var i = 0; i < listDetailRole.length; i++) {

        var extraitRole = {};

        extraitRole.documentReference = listDetailRole[i].documentReference;
        extraitRole.documentReferenceManuel = listDetailRole[i].documentReferenceManuel;
        extraitRole.typeDocument = listDetailRole[i].typeDocument;
        extraitRole.amountPenalite = 0;

        tempExtraitRoleId.push(extraitRole);

        var penaliteDu = listDetailRole[i].TOTAL_PENALITE;

        if (listDetailRole[i].codeOfficiel !== '') {
            if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            } else {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire;
            }
        } else {
            firstLineAB = '';
        }


        if (listDetailRole[i].SERVICE.length > 40) {
            firstLineSA = listDetailRole[i].SERVICE.substring(0, 40) + ' ...';
        } else {
            firstLineSA = listDetailRole[i].SERVICE;
        }

        var etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:green">PAYE </a></center>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">PAYE</a></center>';
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="width:5%">' + listDetailRole[i].exerciceFiscal + '</td>';

        body += '<td style="width:8%" title="' + listDetailRole[i].SERVICE + '">'
                + firstLineSA.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:20%;" title="' + listDetailRole[i].libelleArticleBudgetaire
                + '">' + firstLineAB + '</td>';

        body += '<td style="width:7%">' + listDetailRole[i].TYPE.toUpperCase() + '</td>';
        body += '<td style="width:7%">' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].documentReferenceManuel + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(listDetailRole[i].MONTANTDU, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';

        if (listDetailRole[i].typeDocument == 'BP' && (listDetailRole[i].TYPE_TITRE == 5 | listDetailRole[i].TYPE_TITRE == 6)) {
            body += '<td style="width:5%;vertical-align:middle;text-align:center;">'
                    + '<button  type="button" onclick="printBPFraisPoursuite(\'' + listDetailRole[i].documentReference + '\',\'' + listDetailRole[i].typeDocument + '\')" class="btn btn-warning"><i class="fa fa-print"></i></button>&nbsp;&nbsp;'
                    + '</td>';

            body += '</tr>';
        } else {
            body += '<td style="width:5%;vertical-align:middle;text-align:center;"></td>';
        }

    }

    body += '</tbody>';
    var tableContent = header + body;

    tableNouveauTitrePerception.html(tableContent);

    var myTableDetailRole = tableNouveauTitrePerception.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 4}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[4, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="10">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableNouveauTitrePerception tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = myTableDetailRole.row(tr);
        var dataDetail = myTableDetailRole.row(tr).data();
        var numeroTitre = dataDetail[6];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {


            var tableTitreDependant = '<center><h4>LES TITRES DES PERCEPTIONS DEPENDANTS AU TITRE N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TITRE DE PERCEPTION</td><td>TYPE TITRE DE PERCEPTION</td><td>ECHEANCE</td><td>MONTANT DÛ</td><td>ETAT PAIEMENT</td></tr></thead>';

            var titreList = '';

            for (var j = 0; j < listDetailRole.length; j++) {

                if (listDetailRole[j].documentReferenceManuel == numeroTitre) {

                    if (!isUndefined(listDetailRole[j].TITRE_PERCEPTION_DEPENDACY)) {
                        
                        titreList = JSON.parse(listDetailRole[j].TITRE_PERCEPTION_DEPENDACY);

                        for (var i = 0; i < titreList.length; i++) {

                            var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';

                            if (titreList[i].isApured == true && titreList[i].isPaid == true) {
                                etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                            } else if (titreList[i].isApured == false && titreList[i].isPaid == true) {
                                etatPaiement = '<span style="font-weight:bold;color:red">PAYE</span>';
                            }

                            tableTitreDependant += '<tr>';
                            tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].documentReferenceManuel + '</td>';
                            tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE + '</td>';

                            tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].echeance + '</td>';

                            tableTitreDependant += '<td style="text-align:right;width:25%;">' + formatNumber(titreList[i].MONTANTDU, 'CDF') + '</td>';
                            tableTitreDependant += '<td style="text-align:center:width:15%;">' + etatPaiement + '</td>';
                            tableTitreDependant += '</tr>';
                        }
                    }


                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();

            tr.addClass('shown');

        }

    });
}

function refrechDataAfterAccuserReception() {

    if (isAdvanced == '1') {
        btnAdvencedSearch.trigger('click');
    } else {
        btnSimpleSearchCommandement.trigger('click');
    }

    modalDetailCommandement.modal('hide');
}

function generateAtdLs(atd) {

    var operation = '1406';
    var textTraitement = 'Génération de l\'avis à tiers détenteurs...';
    var message = 'L\'avis à tiers détenteurs est généré avec succès.';

    if (atd === 0) {
        operation = '1407';
        message = 'Le procès verbal de saisie est généré avec succès.';
        textTraitement = 'Génération du procès verbal de saisie...';
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': operation,
            'idUser': userData.idUser,
            'codeAssujetti': idAssujetti,
            'commandementId': idCommandement,
            'MONTANTDU': amountCmd
        },
        beforeSend: function () {

            modalDetailCommandement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >' + textTraitement + '</h5>'});
        },
        success: function (response)
        {

            modalDetailCommandement.unblock();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                if (atd === 1) {
                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');
                } else {
                    //Impression du pv de saisie
                    //setDocumentContent(response);
                    //window.open('visualisation-document', '_blank');
                }
                alertify.alert(message);
                idAssujetti = '';
                idCommandement = '';
                solde = 0;
                refrechDataAfterAccuserReception()
                modalDetailCommandement.modal('hide');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalDetailCommandement.unblock();
            showResponseError();
        }

    });
}

function imprimerCommandement() {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': idCommandement,
            'operation': 'printDocumentRecouvrement'
        },
        beforeSend: function () {
            modalDetailCommandement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailCommandement.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucun document(Commandement) trouvé.');
                return;
            }
            setTimeout(function () {
                modalDetailCommandement.unblock();
                setDocumentContent(response);
                window.open('visualisation-documents', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailCommandement.unblock();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {
    isAdvanced = '0';
    isAdvance.attr('style', 'display: none');

    inputResearchValue.val(selectAssujettiData.nomComplet);
    codeAssujetti = selectAssujettiData.code;

    getCurrentSearchParam(0);
    loadCommandement();

}

function getSumMontantDuExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].MONTANTDU + listeDetailRole[i].TOTAL_PENALITE;
    }
    return sum;
}

function getSumMontantPayeExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].TOTAL_PAYEE_PENALITE + listeDetailRole[i].montantPaye;
    }

    return sum;
}

function printBPFraisPoursuite(numeroDocument, typeDoc) {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': numeroDocument,
            'typeDocument': typeDoc,
            'operation': 'fraisPoursuitPrinter'
        },
        beforeSend: function () {
            modalDetailCommandement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailCommandement.unblock();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            setTimeout(function () {
                modalDetailCommandement.unblock();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailCommandement.unblock();
            showResponseError();
        }

    });

}
