/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnValiderRole;
var formRadioButton;

var btnShowAdvancedSerachModalRole;
var btnPrintProjectRole;
var btnSimpleSearchRole, ResearchTypeRole, ResearchValueRole;
var tableRegistreRole;
var modalRole, tableDetailRole;
var tempDetailRole = [];
var lblnumeroRole;

var detailsRoleList = [];
var dataPublicList = [];

var textaeraObservation;
var btnValiderTraitementRole;
var roleId, divRadio, divDecision, labelDecision;
var modalTraitementRole;

var typeSearch;

var selectSite, selectMinistere, selectService,
        inputDateDebut, inputdateLast;
var modalRechercheAvancee, btnAdvencedSearchRole;

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblProvince, lblEntite;
var isAdvance;

var idRoleSelected;
var roleStateValue;

var modalObservationDisplay, btnDisplayObservation;

var observationListData;
var numberFolderSelected;
var lblNumberFolder, lblValueNumberFolder, divInfoObservationContent;

$(function () {

    idRoleSelected = '';

    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Registre des rôles');

    removeActiveMenu();
    linkMenuRecouvrement.addClass('active');

//    if (!controlAccess('14003')) {
//        window.location = 'dashboard';
//    }

    btnValiderRole = $('#btnValiderRole');
    btnPrintProjectRole = $('#btnPrintProjectRole');
    tableRegistreRole = $('#tableRegistreRole');
    modalRole = $('#modalRole');
    tableDetailRole = $('#tableDetailRole');
    lblnumeroRole = $('#lblnumeroRole');
    btnShowAdvancedSerachModalRole = $('#btnShowAdvancedSerachModalRole');
    btnSimpleSearchRole = $('#btnSimpleSearchRole');
    ResearchTypeRole = $('#ResearchTypeRole');
    ResearchValueRole = $('#ResearchValueRole');
    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');

    modalTraitementRole = $('#modalTraitementRole');
    btnValiderTraitementRole = $('#btnValiderTraitementRole');
    divRadio = $('#divRadio');
    labelDecision = $('#labelDecision');
    textaeraObservation = $('#textaeraObservation');
    divDecision = $('#divDecision');

    selectSite = $('#selectSite');
    selectService = $('#selectService');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    modalRechercheAvancee = $('#modalRechercheAvancee');
    btnAdvencedSearchRole = $('#btnAdvencedSearchRole');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');

    modalObservationDisplay = $('#modalObservationDisplay');
    btnDisplayObservation = $('#btnDisplayObservation');

    lblNumberFolder = $('#lblNumberFolder');
    lblValueNumberFolder = $('#lblValueNumberFolder');
    divInfoObservationContent = $('#divInfoObservationContent');

    btnSimpleSearchRole.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (ResearchValueRole.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir un critère de recherche');
        } else {
            typeSearch = 1;
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            searchRole(typeSearch);
        }
    });

    btnShowAdvancedSerachModalRole.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        modalRechercheAvanceeNC.modal('show');
    });

    btnDisplayObservation.click(function (e) {
        
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        displayObservation(observationListData);
    });

    btnAdvencedSearch.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        typeSearch = 2;
        modalRechercheAvanceeNC.modal('hide');
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        searchRole(typeSearch);
    });

    btnValiderTraitementRole.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir valider ce projet de rôle ?', function () {
            traitementRole();
        });

    });


    btnPrintProjectRole.click(function (e) {
        
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir imprimer ce projet de rôle ?', function () {
            printerProjetRole();
        });

    });



    btnValiderRole.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        modalTraitementRole.modal('show');
    });

    setTimeout(function () {

        var paramDashboard = getDashboardParam();

        if (paramDashboard == 'null') {

            setTimeout(function () {
                btnAdvencedSearchRole.trigger('click');
            }, 1000);

        } else {

            advancedSearchParam = JSON.parse(paramDashboard);
            setTimeout(function () {
                typeSearch = 2;
                isAdvance.attr('style', 'display: block;');
                searchRole(typeSearch);
            }, 1000);
        }



    }, 1000);


    loadRegistreRole('');

//    btnAdvencedSearch.trigger('click');

});

function searchRole(codeResearch) {

    if (typeSearch == 2) {

        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;
            

//            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
//            lblProvince.attr('title', province);
//
//            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
//            lblEntite.attr('title', entite);
//
//            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
//            lblService.attr('title', service);
//
//            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
//            lblSite.attr('title', site);
//
//            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
//            lblAgent.attr('title', agent);
//
//            lblDateDebut.text(advancedSearchParam.dateDebut);
//            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': codeResearch,
            'articleRole': ResearchValueRole.val().trim(),
            'operation': 'loadRole',
//            'codeService': JSON.stringify(advancedSearchParam.service),
//            'codeSite': JSON.stringify(advancedSearchParam.site),
//            'codeEntite': JSON.stringify(advancedSearchParam.entite),
//            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'dateDebut': $('#inputDateDebut').val(),
            'dateFin': $('#inputdateLast').val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadRegistreRole('');
                return;
            }

            setTimeout(function () {
                roleList = null;
                roleList = JSON.parse(JSON.stringify(response));
                dataPublicList = roleList;

                loadRegistreRole(roleList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadRegistreRole(result) {

    tempDetailRole = [];

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th>DATE CREATION</th>\n\
                <th>NUMERO RÔLE</th>\n\
                <th>ETAT</th>\n\\n\\n\\n\
                 <th></th>\n\\n\\n\
                <th style="text-align:right">MONTANT TOTAL DÛ</th>\n\\n\\n\\n\
                <th style="text-align:right">PENALITE</th>\n\\n\
                <th style="text-align:right">MONTANT TOTAL PAYE</th>\n\\n\\n\
                <th style="text-align:right">RESTE A PAYER </th>\n\\n\\n\
                <th></th></tr></thead>';

    var data = '';
    data += '<tbody id="bodyTable">';

    var sumMontantDuCDF = 0;
    var sumMontantPayeCDF = 0;
    var sumFraisPoursuiteCDF = 0;

    for (var i = 0; i < result.length; i++) {

        var role = new Object();
        role.roleId = result[i].roleId;
        role.roleState = result[i].roleState;
        role.articleRole = result[i].articleRole;
        role.detailRoleList = result[i].detailRoleList;
        role.fraisPoursuite = result[i].fraisPoursuiteGlobal;

        tempDetailRole.push(role);

        var montantDu = getRoleSumById(JSON.parse(role.detailRoleList));
        var montantPaye = result[i].montanTotalPayer;
        var amountPenalite = result[i].amountPenalite;
        var resteApayer = (montantDu + amountPenalite) - montantPaye;

        sumMontantDuCDF += montantDu;
        sumFraisPoursuiteCDF += fraisDePoursuite;
        sumMontantPayeCDF += montantPaye;

        var state = '';
        if (result[i].roleState == 1) {
            state = 'Projet de rôle validé';
        } else if (result[i].roleState == 2) {
            state = 'Projet de rôle en attente de la deuxième validation';
        } else if (result[i].roleState == 3) {
            state = 'Projet de rôle en attente de la première validation';
        }

        data += '<tr>';
        data += '<td style="text-align:left;width:10%">' + result[i].dateCreateRole + '</td>';
        data += '<td style="text-align:left;width:10%">' + result[i].articleRole + '</td>';
        data += '<td style="text-align:left;width:15%">' + state.toUpperCase() + '</td>';
        data += '<td style="text-align:center;width:15%"><label id="idLabelDetailExtraitRole" style="text-decoration:underline;color:maron" onclick="loadExtraitByRoleSelected(\'' + result[i].articleRole + '\',\'' + result[i].extraitRoleOrdonnance + '\')">Voir les extraits de rôle </label></td>';
        data += '<td style="text-align:right;width:15%">' + formatNumber(montantDu, result[i].devise) + '</td>';
        data += '<td style="text-align:right;width:15%">' + formatNumber(amountPenalite, result[i].devise) + '</td>';
        data += '<td style="text-align:right;width:15%">' + formatNumber(montantPaye, result[i].devise) + '</td>';
        data += '<td style="text-align:right;width:15%">' + formatNumber(resteApayer, result[i].devise) + '</td>';
        data += '<td style="text-align:center;width:8%"><a onclick="showModalDetailRole(\'' + result[i].roleId + '\',\'' + result[i].roleState + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></td>';
        data += '</tr>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="4" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreRole.html(TableContent);

    tableRegistreRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(4).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(0, 'USD'));

            $(api.column(5).footer()).html(
                    formatNumber(sumFraisPoursuiteCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(0, 'USD'));

            $(api.column(6).footer()).html(
                    formatNumber(sumMontantPayeCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(0, 'USD'));

            $(api.column(7).footer()).html(
                    formatNumber((sumMontantDuCDF + sumFraisPoursuiteCDF) - sumMontantPayeCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(0, 'USD'));
        }
    });
}

function printerProjetRole(roleId) {

    var detailRoleData = JSON.stringify(detailsRoleList);

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'roleId': idRoleSelected,
            'userId': userData.idUser,
            'detailRoleList': JSON.stringify(detailsRoleList),
            'operation': 'printProjectRole'
        },
        beforeSend: function () {
            modalTraitementRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Impression en cours...</h5>'});
        },
        success: function (response)
        {
            modalTraitementRole.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                showResponseError();
                return;
            }
            setTimeout(function () {
                setDocumentContent(response);
                window.open('document-paysage', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalTraitementRole.unblock();
            showResponseError();
        }

    });
}


function loadExtraitByRoleSelected(articleRole, extraitRoleOrdonnance) {

    if (extraitRoleOrdonnance == '1') {
        window.location = 'registre-extrait-role?id=' + btoa(articleRole.trim());
    } else if (extraitRoleOrdonnance == '0') {
        alertify.alert('Impossible d\'afficher les extraits de ce rôle car ils ne sont pas encore générer.');
    }

}

function showModalDetailRole(idRole, roleState) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    idRoleSelected = idRole;
    roleStateValue = roleState;

    for (var i = 0; i < tempDetailRole.length; i++) {

        if (tempDetailRole[i].roleId == idRole) {

            numberFolderSelected = tempDetailRole[i].articleRole;

            for (var j = 0; j < dataPublicList.length; j++) {

                if (dataPublicList[j].roleId == idRole) {
                    observationListData = dataPublicList[j].observationList;
                    continue;
                }
            }

            if (tempDetailRole[i].roleState == 3) {
                btnValiderRole.attr('style', 'display:inline');
            } else if (tempDetailRole[i].roleState == 2) {

//                if (controlAccess('14010')) {
                    btnValiderRole.attr('style', 'display:inline');
//                } else {
//                    btnValiderRole.attr('style', 'display:none');
//                }

            } else if (tempDetailRole[i].roleState == 1) {
                btnValiderRole.attr('style', 'display:none');
            }

            lblnumeroRole.html(tempDetailRole[i].articleRole);
            roleId = idRole;

            var listDetailRole = JSON.parse(tempDetailRole[i].detailRoleList);

            initPenalite(listDetailRole);

        }

        modalRole.modal('show');

    }
}

function getRoleSumById(listDetailRole) {

    var sumRole = 0;

    for (var i = 0; i < listDetailRole.length; i++) {

        var penalite = calculteInteretMoratoireV3(
                listDetailRole[i].netAPaye,
                listDetailRole[i].isRecidiviste,
                listDetailRole[i].nombreMois);

        sumRole += listDetailRole[i].netAPaye + penalite;
    }

    return sumRole;
}

function printDetailRole(listDetailRole) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th> EXERCICE </th>';
    header += '<th> SERVICE D\'ASSIETTE </th>';
    header += '<th> ARTICLE BUDGETAIRE </th>';
    header += '<th style="text-align:center"> TITRE DE PERCEPTION </th>';
    header += '<th hidden="true"> TYPE DOCUMENT </th>';
    header += '<th> DATE EXIGIBILITE </th>';
    header += '<th> ASSUJETTI </th>';
    header += '<th style="text-align:right"> MONTANT DÛ </th>';
    header += '<th style="text-align:right"> PENALITE DÛ</th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyAdresses">';

    for (var i = 0; i < listDetailRole.length; i++) {

        var penaliteDu = 0;//getSumByDoc(listDetailRole[i].documentReference);
        var firstLineSA = '';
        var firstLineAB = '';

        var detailsRole = {};
        detailsRole.documentReference = listDetailRole[i].documentReference;
        detailsRole.documentReferenceManuel = listDetailRole[i].documentReferenceManuel;
        detailsRole.typeDocument = listDetailRole[i].typeDocument;
        detailsRole.amountPenalite = penaliteDu;
        detailsRole.netAPaye = listDetailRole[i].netAPaye;
        detailsRole.codeOfficiel = listDetailRole[i].codeOfficiel;
        detailsRole.assujettiCode = listDetailRole[i].assujettiCode;
        detailsRole.codeTarif = listDetailRole[i].codeTarif;
        detailsRole.libelleArticleBudgetaire = listDetailRole[i].libelleArticleBudgetaire;
        detailsRole.devise = listDetailRole[i].devise;
        detailsRole.libelleService = listDetailRole[i].libelleService;
        detailsRole.dateCreate = listDetailRole[i].dateCreate;
        detailsRole.codeOfficiel = listDetailRole[i].codeOfficiel;
        detailsRole.roleId = listDetailRole[i].roleId;

        detailsRole.assujettiCode = listDetailRole[i].assujettiCode;

        if (roleStateValue == 1) {
            btnPrintProjectRole.attr('style', 'display: block');
        } else {
            btnPrintProjectRole.attr('style', 'display: none');
        }

        detailsRoleList.push(detailsRole);

//        if (listDetailRole[i].libelleService.length > 45) {
//            firstLineSA = listDetailRole[i].libelleService.substring(0, 45) + ' ...';
//        } else {
//            firstLineSA = listDetailRole[i].libelleService;
//        }

        if (listDetailRole[i].codeOfficiel !== '') {

            if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
                //firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
                firstLineAB += firstLineAB = '<br/>' + listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            } else {
                //firstLineAB = listDetailRole[i].libelleArticleBudgetaire;
                firstLineAB += firstLineAB = '<br/>' + listDetailRole[i].libelleArticleBudgetaire;
            }
        } else {
            firstLineAB = '';
        }


        var etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:green">PAYE</a></center>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">PAYE</a></center>';
        }

        var typeTitreDoc = '<br/><center><a href="#" style="font-weight:bold;color:red">' + '(' + listDetailRole[i].typeDocument + ')' + '</a></center>';

        body += '<tr>';
        body += '<td style="width:5%">' + listDetailRole[i].exerciceFiscal + '</td>';
        body += '<td style="width:11%" title=""></td>';

        body += '<td style="text-align:left;width:25%;" title="' + listDetailRole[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';

        body += '<td style="width:7%;text-align:center">' + listDetailRole[i].documentReference + ' ' + typeTitreDoc + '</td>';
        body += '<td style="width:6%" hidden="true">' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:8%">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="width:15%">' + listDetailRole[i].assujettiName + '</td>';
        body += '<td style="text-align:right;width:8%">' + formatNumber(listDetailRole[i].netAPaye, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:8%">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;

    tableDetailRole.html(tableContent);
    tableDetailRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[6, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        columnDefs: [
            {"visible": false, "targets": 6}
        ],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(6, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold" ><td colspan="9">' + group + '</td></tr>'
                            );
                    last = group;
                }
            });
        }
    });
}

function traitementRole() {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'roleId': roleId,
            'operation': 'validateRole',
            'description': textaeraObservation.val(),
            'roleState': roleStateValue,
            'idUser': userData.idUser
        },
        beforeSend: function () {
            modalTraitementRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Validation en cours...</h5>'});
        },
        success: function (response)
        {
            modalTraitementRole.unblock();

            if (response === '-1') {
                showResponseError();
                return;
            } else if (response === '0') {
                showResponseError();
                return;
            } else if (response === '1') {

                modalTraitementRole.modal('hide');

                setTimeout(function () {
                    alertify.alert('La validation du projet de rôle s\'est effectuée avec succès.');
                    modalRole.modal('hide');
                    searchRole(typeSearch);
                }
                , 1);
            } else {
                showResponseError();
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalTraitementRole.unblock();
            showResponseError();
        }
    });
}

function initPenalite(listDetailRole) {

    listePenalite.length = 0;

    for (var i = 0; i < listDetailRole.length; i++) {

        calculteInteretMoratoireV2ByDoc(
                listDetailRole[i].documentReference,
                listDetailRole[i].netAPaye,
                listDetailRole[i].isRecidiviste,
                listDetailRole[i].nombreMois,
                listDetailRole[i].devise
                );
    }

    printDetailRole(listDetailRole);

}

function displayObservation(observList) {


    observList = JSON.parse(observList);
    var table = '';

    if (observList[0] !== '0') {

        lblNumberFolder.text('ARTICLE ROLE :');
        lblValueNumberFolder.html(numberFolderSelected);

        table += '<table class="table table-bordered table-hover">';

        table += '<thead style="background-color:#0085c7;color:white">';
        table += '<tr>';
        table += '<td> TYPE </td>';
        table += '<td> CONTENU OBSERVATION </td>';
        table += '</tr>';
        table += '</thead>';

        table += '<tbody>';

        if (observList[0].obsCreateExists == '1') {

            table += '<tr>';

            table += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold"> Observation fournie à la création </td>';
            table += '<td style="text-align:left;width:80%;vertical-align:middle"> ' + observList[0].obsCreateValues + ' </td>';

            table += '</tr>';
        }

        if (observList[0].obsValidateExistsFirstLevel == '1') {

            table += '<tr>';

            table += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold"> Observation fournie à la première validation </td>';
            table += '<td style="text-align:left;width:80%;vertical-align:middle"> ' + observList[0].obsValidateFirstLevel + ' </td>';

            table += '</tr>';
        }

        if (observList[0].obsValidateExistsSecondLevel == '1') {

            table += '<tr>';

            table += '<td style="text-align:left;width:20%;vertical-align:middle;font-weight:bold"> Observation fournie à la deuxième validation </td>';
            table += '<td style="text-align:left;width:80%;vertical-align:middle"> ' + observList[0].obsValidateSecondLevel + ' </td>';


            table += '</tr>';
        }

        table += '</tbody>';
        table += '</table>';

        divInfoObservationContent.html(table);
        modalObservationDisplay.modal('show');
    } else {

        lblNumberFolder.text(empty);
        lblValueNumberFolder.html(empty);
        divInfoObservationContent.html(empty);

        alertify.alert('Ce dossier n\'a pas d\'observation.');

    }
}
