/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnRechercheDefaillant, btnAdvencedSearch, btnCreateRole, btnValidateCreateRole, btnCallModalSearchResponsable;
var selectMoisFin, selectMoisDebut, selectAnnee, selectSite,
        selectMinistere, selectService, cbxResearchType;
var tableDefaillants, tableDefaillants2;
var modalRechercheAvanceeDefaillantPaiement, modalCreateProjectRole;
var tempRecouvrementList = [];
var textArticleRole;
var monthDataDebut = '', monthDataFin = '', yearData = '';
var listRecouvrement;

var lblMinistere, lblService, lblMoisDebut, lblMoisFin, lblMois, lblSite, lblProvince, lblEntite;
var isAdvance;

var divRadio;
var inputDefaillantSelected;
var responsibleObject = new Object();

var codeAssujettiDefaillant;
var codeBdgetArticle;

var dateDebuts, dateFins, codeService,
        codeSite;

$(function () {
    codeAssujettiDefaillant = '';
    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Edition de rôle');

    removeActiveMenu();
    linkMenuRecouvrement.addClass('active');

//    if (!controlAccess('14002')) {
//        window.location = 'dashboard';
//    }

    selectMoisFin = $('#selectMoisFin');
    selectMoisDebut = $('#selectMoisDebut');
    selectAnnee = $('#selectAnnee');
    selectSite = $('#selectSite');
    selectMinistere = $('#selectMinistere');
    selectService = $('#selectService');
    tableDefaillants = $('#tableDefaillants');
    tableDefaillants2 = $('#tableDefaillants2');
    cbxResearchType = $('#cbxResearchType');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblAnnee = $('#lblAnnee');
    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');

    lblMoisDebut = $('#lblMoisDebut');
    lblMoisFin = $('#lblMoisFin');

    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    inputDefaillantSelected = $('#inputDefaillantSelected');

    textArticleRole = $('#textArticleRole');
    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    divRadio = $('#divRadio');

    modalRechercheAvanceeDefaillantPaiement = $('#modalRechercheAvanceeDefaillantPaiement');
    modalCreateProjectRole = $('#modalCreateProjectRole');

    btnRechercheDefaillant = $('#btnRechercheDefaillant');

    btnRechercheDefaillant.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        modalRechercheAvanceeDefaillantPaiement.modal('show');

    });

    btnCallModalSearchResponsable.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cbxResearchType.val() === '1') {
            allSearchAssujetti = '1';
            assujettiModal.modal('show');
        } else if (cbxResearchType.val() === '2') {
            budgetArticleModal.modal('show');
        }

    });


    cbxResearchType.on('change', function (e) {

        codeResearch = cbxResearchType.val();
        if (codeResearch === '1') {
            inputDefaillantSelected.attr('placeholder', 'Le nom de l\'assujetti');
            inputDefaillantSelected.val('');
            assujettiModal.modal('show');
        } else if (codeResearch === '2') {
            inputDefaillantSelected.attr('placeholder', 'Numéro de la note de perception');
            inputDefaillantSelected.val('');
        } else {
            inputDefaillantSelected.attr('placeholder', 'Numero de l\'article budgetaire');
            inputDefaillantSelected.val('');
            //budgetArticleModal.modal('show');
        }
    });


    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnAdvencedSearch.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        isAdvance.attr('style', 'display: block');
        modalRechercheAvanceeDefaillantPaiement.modal('hide');
        codeAssujettiDefaillant = '';
        getCurrentSearchParam(1);
        loadDefaillants();
    });

    btnCreateRole = $('#btnCreateRole');
    btnCreateRole.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (tempRecouvrementList.length == 0) {
            alertify.alert('Impossible de créer un projet de rôle pour ce(s) titre(s) de perception');
            return;
        }
        modalCreateProjectRole.modal('show');
    });

    btnValidateCreateRole = $('#btnValidateCreateRole');
    btnValidateCreateRole.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (textArticleRole.val() == '') {
            alertify.alert('Veuillez d\'abord saisir l\'article du rôle.');
            return;
        }

        if (tempRecouvrementList.length == 0) {
            alertify.alert('Impossible de créer un projet de rôle pour ce(s) titre(s) de perception');
            return;
        } else {
            alertify.confirm('Etes-vous sûre de vouloir créer ce projet de rôle ?', function () {
                var detailRoles = JSON.stringify(tempRecouvrementList);
                createProjectRole(detailRoles);
            });
        }

    });

    modalRechercheAvanceeDefaillantPaiement.on('shown.bs.modal', function () {
        jQuery(".chosen").chosen();
    });

    loadMois();
    loadAnnee();

    printDefaillants('');

    if (getTourParam() == 1) {
        Tour.run([
            {
                element: $('#btnRechercheDefaillant'),
                content: '<p style="font-size:17px;color:black">Commencer par rechercher un défaillant</p>',
                language: 'fr',
                position: 'right',
                close: 'false'
            }
        ]);
    }

    //btnAdvencedSearch.trigger('click');

});

function loadMois() {

    var date = new Date();
    var mois = date.getMonth() + 1;

    var dataMois = '';

    dataMois += '<option value =1> Janvier </option>';
    dataMois += '<option value =2> Février </option>';
    dataMois += '<option value =3> Mars </option>';
    dataMois += '<option value =4> Avril </option>';
    dataMois += '<option value =5> Mai </option>';
    dataMois += '<option value =6> Juin </option>';
    dataMois += '<option value =7> Juillet </option>';
    dataMois += '<option value =8> Aôut </option>';
    dataMois += '<option value =9> Septembre </option>';
    dataMois += '<option value =10> Octobre </option>';
    dataMois += '<option value =11> Novembre </option>';
    dataMois += '<option value =12> Décembre </option>';

    selectMoisFin.html(dataMois);
    selectMoisFin.val(mois);

    selectMoisDebut.html(dataMois);
    selectMoisDebut.val(mois);
}

function loadAnnee() {

    var anneeDebut = 2000;
    var date = new Date();
    var anneeFin = date.getFullYear();

    var dataAnnee = '';

    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }

    selectAnnee.html(dataAnnee);
    selectAnnee.val(anneeFin);
}

function printDefaillants(result) {

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>'
            + '<th style="text-align:left">EXERCICE</th>'
            + '<th style="text-aLign:left">SECTEUR</th>'
            + '<th style="text-aLign:left">ARTICLE BUDGETAIRE</th>'
            + '<th style="text-aLign:left">ASSUJETTI</th>'
            + '<th style="text-aLign:left" hidden="true">TYPE DOCUMENT</th>'
            + '<th style="text-aLign:left">TITRE DE PERCEPTION</th>'
            + '<th style="text-aLign:left">DATE EXI.</th>'
            + '<th style="text-aLign:left">MOIS DE RETARD</th>'
            + '<th style="text-align:left">ETAT PAIEMENT</th>'
            + '<th style="text-align:right">MONTANT DÛ</th>'
            + '<th style="text-align:right">PENALITE DÛ</th>'
            + '<th style="text-align:center" hidden="true">PEUT GENERER INT.MOR</th>'
            + '</tr></thead>';

    var data = '';
    data += '<tbody id="bodyTable">';

    tempRecouvrementList = [];

    var sumCDF = 0;
    var sumUSD = 0;
    var sumPenaliteCDF = 0;
    var sumPenaliteUSD = 0;

    if (result.length > 0) {
        btnCreateRole.attr('style', 'display: inline');
    } else {
        btnCreateRole.attr('style', 'display: none');
    }

    for (var i = 0; i < result.length; i++) {

        var devise = result[i].deviseNp;
        var amount = result[i].amountNp;

        var amountPenalite = result[i].amountPenalite;
//        calculteInteretMoratoireV3(
//                result[i].amountNp,
//                result[i].isRecidiviste,
//                result[i].nombreMois);

        if (devise === 'CDF') {
            sumCDF += amount;
            sumPenaliteCDF += amountPenalite;
        } else if (devise === 'USD') {
            sumUSD += amount;
            sumPenaliteUSD += amountPenalite;
        }

        var recouvrementObj = {};
        recouvrementObj.typeDocument = "NP"//result[i].typeDocument;
        recouvrementObj.documentReference = result[i].numeroNp;
        recouvrementObj.documentReferenceManuel = result[i].numeroNp;
        recouvrementObj.CODE = result[i].assujettiCode;



        //if (result[i].genererInteretMoratoire) {
        tempRecouvrementList.push(recouvrementObj);
        //}


        var penaliteDu = amountPenalite;

        var firstLineSA = '';
        var firstLineAB = '';
        var assujetti = result[i].assujettiName;

//        if (result[i].libelleService.length > 50) {
//            firstLineSA = result[i].libelleService.substring(0, 50) + ' ...';
//        } else {
//            firstLineSA = result[i].libelleService;
//        }

        if (result[i].articleBudgetaireName.length > 40) {
            firstLineAB = result[i].articleBudgetaireName.substring(0, 40) + ' ...';
        } else {
            firstLineAB = result[i].articleBudgetaireName;
        }

        var etatPaiement = '<span style="font-weight:bold;;color:red">NON PAYE</span>';

//        if (result[i].isApured == true && result[i].isPaid == true) {
//            etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
//        } else if (result[i].isApured == false && result[i].isPaid == true) {
//            etatPaiement = '<span style="font-weight:bold;;color:green">PAYE</span>';
//        } else {
//            etatPaiement = '<span style="font-weight:bold;;color:red">NON PAYE</span>';
//        }

        data += '<tr>';
        data += '<td style="text-align:left;width:4%">' + result[i].exerciceNp + '</td>';
        data += '<td style="text-align:left;width:15%" title=""></td>';
        data += '<td style="text-align:left;width:23%;" title="' + result[i].articleBudgetaireName + '">' + firstLineAB + '</td>';
        data += '<td style="text-align:left;width:16%;" title="' + assujetti + '">' + assujetti + '</td>';

        data += '<td style="text-align:left;width:6%" hidden="true">' + result[i].typeDocument + '</td>';

        data += '<td style="text-align:center;width:8%"><span style="font-weight:bold;">' + result[i].numeroNp + '</span></td>';

        data += '<td style="text-align:left;width:7%">' + result[i].dateEcheanceNp + '</td>';
        data += '<td style="text-align:center;width:6%">1</td>';

        if (result[i].isPaid == true) {
            data += '<td style="text-align:center;width:11%;vertical-align:middle;color:blue;font-style:italic;"><u><a href="#" onclick="openRP(\'' + result[i].documentReference + '\')">' + etatPaiement + '</a></u></td>';
        } else {
            data += '<td style="text-align:center;width:9%">' + etatPaiement + '</td>';
        }

        data += '<td style="text-align:right;width:8%">' + formatNumber(result[i].amountNp, result[i].deviseNp) + '</td>';
        data += '<td style="text-align:right;width:8%">' + formatNumber(penaliteDu, result[i].deviseNp) + '</td>';

//        if (!result[i].genererInteretMoratoire) {
//            data += '<td style="text-align:center;width:6%" hidden="true"><input type="checkbox" disabled  /></td>';
//        } else {
        data += '<td style="text-align:center;width:6%" hidden="true"><input type="checkbox" disabled checked /></td>';
//        }

        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="8" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableDefaillants.html(TableContent);

    tableDefaillants.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        order: [[1, 'asc']],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(8).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));

            $(api.column(9).footer()).html(
                    formatNumber(sumPenaliteCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumPenaliteUSD, 'USD')
                    );
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });
}

function loadDefaillants() {

//    try {
//
//        var annee = $('#selectAnnee option:selected').text();
//        var moisDebut = $('#selectMoisDebut option:selected').text();
//        var moisFin = $('#selectMoisFin option:selected').text();
//
//        var service = advancedSearchParam.serviceLibelle;
//        var site = advancedSearchParam.siteLibelle;
//        var entite = advancedSearchParam.entiteLibelle;
//        var province = advancedSearchParam.provinceLibelle;
//
//        lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
//        lblProvince.attr('title', province);
//
//        lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
//        lblEntite.attr('title', entite);
//
//        lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
//        lblService.attr('title', service);
//
//        lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
//        lblSite.attr('title', site);
//
//        lblAnnee.text(annee);
//        lblMoisDebut.text(moisDebut);
//        lblMoisFin.text(moisFin);
//
//    } catch (err) {
//        location.reload(true);
//    }

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;
    codeService = userData.serviceCode;
    codeSite = userData.SiteCode;

    monthDataDebut = selectMoisDebut.val();
    monthDataFin = selectMoisFin.val();
//
    yearData = selectAnnee.val();
//
//    var valueResearch = codeAssujettiDefaillant;
//
//    if (cbxResearchType.val() === '1') {
//        valueResearch = codeAssujettiDefaillant;
//    } else if (cbxResearchType.val() === '3') {
//        valueResearch = codeBdgetArticle;
//    }
//
//    if (valueResearch !== '') {
//        advancedSearchParam.site = JSON.parse(userData.allSiteList);
//        advancedSearchParam.service = JSON.parse(userData.allServiceList);
//        advancedSearchParam.entite = JSON.parse(userData.allEntiteList);
//    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeService': codeService,
            'codeSite': codeSite,
            'dateDebut': dateDebuts,
            'dateFin': dateFins,
            'assujettiCode': responsibleObject.codeResponsible,
            'advancedSearch': '0',
            'operation': 'loadDefaillantPaiementRole'
        },
        beforeSend: function () {
            modalRechercheAvanceeDefaillantPaiement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalRechercheAvanceeDefaillantPaiement.unblock();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                    printDefaillants('');
                } else {

                    if (codeAssujettiDefaillant == '') {
                        inputDefaillantSelected.val(empty);
                    }

                    var recouvrementList = JSON.parse(JSON.stringify(response));
                    modalRechercheAvanceeDefaillantPaiement.modal('hide');
                    initPenalite(recouvrementList);
                    listRecouvrement = recouvrementList;

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalRechercheAvanceeDefaillantPaiement.unblock();
            showResponseError();
        }

    });
}

function createProjectRole(detailRoles) {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'idUser': userData.idUser,
            'articleRole': textArticleRole.val(),
            'monthSearchStart': monthDataDebut,
            'monthSearchEnd': monthDataFin,
            'yearSearch': yearData,
            'detailRoles': detailRoles,
            'operation': '1401'
        },
        beforeSend: function () {
            modalCreateProjectRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création du projet en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalCreateProjectRole.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    alertify.alert('Erreur lors de la création du projet de rôle.');
                    return;
                } else if (response == '3') {
                    alertify.alert('Cette valeur de l\'article du rôle : ' + textArticleRole.val() + ' est déjà associée à un autre rôle.');
                    return;
                } else {
                    alertify.alert('Le projet de rôle est créé avec succès.');
                    modalCreateProjectRole.modal('hide');
                    printDefaillants('');
                    inputDefaillantSelected.val(empty);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalCreateProjectRole.unblock();
            showResponseError();
        }

    });

}

function initPenalite(tempRecouvrementList) {

    printDefaillants(tempRecouvrementList);

}

function getSelectedAssujetiData() {

    isAdvance.attr('style', 'display: none');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.nameResponsible = selectAssujettiData.nomComplet;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;

    if (selectAssujettiData.nif !== '') {
        inputDefaillantSelected.val('Nif : ' + selectAssujettiData.nif + ' / Noms complet : ' + responsibleObject.nameResponsible);
    } else {
        inputDefaillantSelected.val('Code : ' + selectAssujettiData.code + ' / Noms complet : ' + responsibleObject.nameResponsible);
    }

    codeAssujettiDefaillant = selectAssujettiData.code;

    //getCurrentSearchParam('0');
    loadDefaillants();
}

function getCurrentBudgetArticle() {

    isAdvance.attr('style', 'display: none');

    dataArrayBudgetArticles = infoBudgetArticle;
    inputDefaillantSelected.val(dataArrayBudgetArticles[5]);
    codeBdgetArticle = dataArrayBudgetArticles[3];

    if (codeBdgetArticle !== '') {
        inputDefaillantSelected.val(dataArrayBudgetArticles[5]);
    } else {
        inputDefaillantSelected.val(dataArrayBudgetArticles[5]);
    }

    advancedSearchParam.site = JSON.parse(userData.allSiteList);
    advancedSearchParam.service = JSON.parse(userData.allServiceList);

    budgetArticleModal.modal('hide');

    getCurrentSearchParam('0');
    loadDefaillants();
}


