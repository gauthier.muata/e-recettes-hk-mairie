/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global alertify */

var modalFichepriseEnCharge;
var modalFichepriseEnChargeValidate;

var ListPN = [];
var listPNSelected = [];

var selectPenalite, inputPenaliteDu, inputTaux, tablePenalite, btnAjouter, btnValiderDoc;
var lblReferenceLibelle, lblMontantDu, lblMoisRetard, lblRecidiviste;

var tablePenaliteValidate, lblSumPenaliteValidate;

var lblSumPenalite;

var CODE_PENALITE_INTERERT_MORATOIRE = 'P00012015';
var CODE_PENALITE_ASSIETE_RETARD_DECLARATION = 'P00022015';
var CODE_PENALITE_ASSIETE_ABSENCE_DECLARATION = 'P00072015';
var CODE_PENALITE_ASSIETE_FAUSSE_DECLARATION = 'P00082015';

var NATURE_AB = 'N002';

var selectedPenality = {};

var referenceInformation = {};

var clickAdd = 0;

$(function () {

    $.when(loadPenaliteData()).done(function (x) {
        setTimeout(function () {

        }, 1000);
    });

});

function initMyFields() {

    modalFichepriseEnCharge = $("#modalFichePriseEnCharge");
    modalFichepriseEnCharge.modal('show');

    tablePenalite = $('#tablePenalite');
    selectPenalite = $('#selectPenalite');
    inputPenaliteDu = $('#inputPenaliteDu');
    inputTaux = $('#inputTaux');
    btnAjouter = $('#btnAjouter');
    lblSumPenalite = $('#lblSumPenalite');

    lblReference = $('#lblReferenceLibelle');
    lblMontantDu = $('#lblMontantDu');
    lblMoisRetard = $('#lblMoisRetard');
    lblRecidiviste = $('#lblRecidiviste');
    btnValiderDoc = $('#btnValiderDoc');

}

function resetFields() {

    inputPenaliteDu.val(0);
    inputTaux.val(0);

}

function initUIPenality(obj) {

    referenceInformation = obj;

    initMyFields();

    resetFields();

    lblReference.html(obj.libelleReference);
    lblMontantDu.html(formatNumber(obj.amount, obj.currency));
    lblMoisRetard.html(obj.monthLate);
    lblRecidiviste.html(obj.isRecidiviste ? 'OUI' : 'NON');

    constitutePenaliteList();

    selectPenalite.change(function (e) {

        e.preventDefault();

        if (selectPenalite.val() === '0') {

            inputPenaliteDu.val(0);
            inputTaux.val(0);

            return;
        }

        calculatePenalie(selectPenalite.val());

    });

    btnValiderDoc.click(function (e) {
        e.preventDefault();
        validateFPC();
    });

    btnAjouter.click(function (e) {

        e.preventDefault();

        addPenality();

    });

    generatePenalieInRetraitDeclaration(obj.reference, obj.amount, obj.currency, obj.isRecidiviste, obj.monthLate, obj.case);
    loadTablePenality(listPNSelected);
}

function addPenality() {

    if (selectPenalite.val() === '0') {
        alertify.alert('Veuillez sélectionner une pénalité avant d\'ajouter dans le panier');
        return;
    }

    if (inputTaux.val() === '0') {
        alertify.alert('Vous ne pouvez pas ajouter cette pénalité. La pénalité dû invalide.');
        return;
    }

    for (var i = 0; i < listPNSelected.length; i++) {

        if (listPNSelected[i].id === selectPenalite.val() && listPNSelected[i].referenceNumber === referenceInformation.reference) {
            alertify.alert('Vous ne pouvez pas ajouter cette pénalité. Elle existe déjà dans le panier.');
            return;
        }
    }

    listPNSelected.push(selectedPenality);

    resetFields();

    loadTablePenality(listPNSelected);

    clickAdd = 0;
}

function loadTablePenality(data)
{
    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left" hidden="true">Code pénalité</th>\n\\n\\n\
                <th style="text-align:left">PENALITE</th>\n\\n\
                <th style="text-aLign:left">PRINCIPAL DU</th>\n\\n\
                <th style="text-aLign:left">TAUX</th>\n\\n\\n\
                <th style="text-align:center">MOIS RETARD</th>\n\\n\\n\
                <th style="text-align:right">PENALITE DU</th>\n\\n\\n\
                <th style="text-align:center"> </th>\n\\n\
                </tr></thead>';

    var table = '';
    table += '<tbody id="bodyTable">';

    var sumPenalities = 0;

    for (var i = 0; i < data.length; i++) {

        if (data[i].referenceNumber == referenceInformation.reference) {

            sumPenalities += data[i].value;

            table += '<tr>';
            table += '<td style="text-align:left;width:5%" hidden="true">' + data[i].id + '</td>';
            table += '<td style="text-align:left;width:30%">' + data[i].label + '</td>';
            table += '<td style="text-align:left;width:15%">' + formatNumber(data[i].principal, data[i].currency) + '</td>';
            table += '<td style="text-align:left;width:15%">' + data[i].rate + ' ' + data[i].type + '</td>';
            table += '<td style="text-align:center;width:15%">' + data[i].numberMonth + '</td>';
            table += '<td style="text-align:right;width:15%">' + formatNumber(data[i].value, data[i].currency) + '</td>';

            if (data[i].canDelete) {
                table += '<td style="text-align:center;width:5%"><button style="margin-top:5px" class="btn btn-danger" onclick="deletePenalite(\'' + data[i].id + '\',\'' + '' + '\')"><i class="fa fa-trash-o"></i></button></td>';
            } else {
                table += '<td style="text-align:center;width:5%"></td>';
            }

            table += '</tr>';

        }

    }
    table += '</tbody>';
    var TableContent = header + table;
    tablePenalite.html(TableContent);
    tablePenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune pénalité dans le panier",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        datalength: 5
    });

    lblSumPenalite.html(formatNumber(sumPenalities, referenceInformation.currency));

}

function loadPenaliteData() {

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'JSON',
        async: false,
        crossDomain: false,
        data: {
            'operation': 'getPenalite'
        },
        beforeSend: function () {

        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                ListPN = JSON.parse(JSON.stringify(response));

            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            showResponseError();
        }
    });
}

function constitutePenaliteList() {

    var dataPenalite = '<option value="0">--- S&eacute;lectionner une p&eacute;nalit&eacute --</option>';

    for (var i = 0; i < ListPN.length; i++) {

        if (ListPN[i].IS_VISIBLE_UTILISATEUR) {

            dataPenalite += '<option value="' + ListPN[i].CODE + '">' + ListPN[i].INTITULE + '</option>';

        }

    }

    selectPenalite.html(dataPenalite);
}

function calculatePenalie(codePenalite) {

    var objPenalite;
    var listTaux;
    var valuePenalite = 0;
    var taux, valTaux, nbrMois, libellePenalite;

    var appliquerMoisPenalite;

    for (var i = 0; i < ListPN.length; i++) {

        if (codePenalite === ListPN[i].CODE) {

            libellePenalite = ListPN[i].INTITULE;
            appliquerMoisPenalite = ListPN[i].APPLIQUER_MOIS_RETARD;
            objPenalite = ListPN[i];
            listTaux = JSON.parse(objPenalite.LIST_TAUX_PANALIE);
            nbrMois = 1;

            if (listTaux.length > 0) {

                taux = listTaux[0].VALEUR;

                if (taux === 0) {
                    valTaux = 1;
                } else {
                    valTaux = taux;
                }

                if (appliquerMoisPenalite) {
                    nbrMois = referenceInformation.monthLate;
                }

                switch (listTaux[0].EST_POURCENTAGE) {
                    case true:
                        valuePenalite = (((referenceInformation.amount) * valTaux) / 100) * nbrMois;
                        inputTaux.val(taux + ' ' + '%');
                        break;
                    case false:
                        valuePenalite = taux * nbrMois;
                        inputTaux.val(taux + ' ' + referenceInformation.currency);
                        break;
                    default :
                }


            } else {

                inputTaux.val(0);
                inputPenaliteDu.val(0);
                alertify.alert('Cette pénalité n\'a pas de taux. Veuillez la configurer correctement');
            }

            break;
        }
    }

    inputPenaliteDu.val(formatNumber(valuePenalite, referenceInformation.currency));

    selectedPenality = {};

    selectedPenality.value = valuePenalite;
    selectedPenality.rate = taux;
    selectedPenality.label = libellePenalite;
    selectedPenality.principal = referenceInformation.amount;
    selectedPenality.numberMonth = !appliquerMoisPenalite ? 0 : referenceInformation.monthLate;
    selectedPenality.currency = referenceInformation.currency;
    selectedPenality.referenceNumber = referenceInformation.reference;
    selectedPenality.referenceLabel = referenceInformation.libelleReference;
    selectedPenality.type = '%';
    selectedPenality.id = codePenalite;
    selectedPenality.canDelete = true;
}

function deletePenalite(idPenality) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cette pénalité dans le panier ?', function () {

        for (var i = 0; i < listPNSelected.length; i++) {

            if (listPNSelected[i].id === idPenality && listPNSelected[i].referenceNumber === referenceInformation.reference) {

                listPNSelected.splice(i, 1);
                loadTablePenality(listPNSelected);
                return;
            }
        }
    });

}

function generatePenalies(reference, amount, currency, isRecidiviste, numberMonth, Penalitycase) {

    switch (Penalitycase) {
        case 1:
            listPNSelected = [];
            break;
        default:
            listPNSelected = [];
            break;
    }


    if (ListPN.length === 0) {
        alertify.alert('Aucune pénalité configuré');
        return;
    }

    var selectedPenality = {};

    var codePenality = '';

    switch (Penalitycase) {

        case 1:
            codePenality = CODE_PENALITE_ASSIETE_RETARD_DECLARATION;
            break;
        case 2:
            codePenality = CODE_PENALITE_INTERERT_MORATOIRE;
            break;
        case 3:
            codePenality = CODE_PENALITE_ASSIETE_ABSENCE_DECLARATION;
            break;
        case 4:
            codePenality = CODE_PENALITE_ASSIETE_FAUSSE_DECLARATION;
            break;
    }

    var penaliteName = empty;

    for (var i = 0; i < ListPN.length; i++) {

        if (codePenality == ListPN[i].CODE) {
            selectedPenality = ListPN[i];
            penaliteName = ListPN[i].INTITULE;
            break;
        }
    }

    var ratePenality = getTaux(selectedPenality.LIST_TAUX_PANALIE, isRecidiviste);
    var valuePenalite = parseFloat(((parseFloat(amount) * parseFloat(ratePenality)) / 100) * numberMonth);

    selectedPenality = {};

    selectedPenality.value = valuePenalite;
    selectedPenality.rate = ratePenality;
    selectedPenality.label = penaliteName;
    selectedPenality.principal = amount;
    selectedPenality.numberMonth = numberMonth === 0 ? 1 : numberMonth;
    selectedPenality.currency = currency;
    selectedPenality.referenceNumber = reference;
    selectedPenality.referenceLabel = reference;
    selectedPenality.type = '%';
    selectedPenality.id = codePenality;
    selectedPenality.canDelete = false;

    listPNSelected.push(selectedPenality);

}

function generatePenalieInRetraitDeclaration(reference, amount, currency, isRecidiviste, numberMonth, Penalitycase) {

    switch (Penalitycase) {
        case 1:
        case '1':
            listPNSelected = [];
            break;
        default:
            listPNSelected = [];
            break;
    }


    if (ListPN.length === 0) {
        alertify.alert('Aucune pénalité configuré');
        return;
    }

    var selectedPenality = {};

    var codePenality = '';

    var isRetrait = false;

    switch (Penalitycase) {

        case 1:
        case '1':
            isRetrait = true;
            codePenality = CODE_PENALITE_ASSIETE_RETARD_DECLARATION;
            break;
        case 2:
        case '2':
            codePenality = CODE_PENALITE_INTERERT_MORATOIRE;
            break;
        case 3:
        case '3':
            codePenality = CODE_PENALITE_ASSIETE_ABSENCE_DECLARATION;
            break;
        case 4:
        case '4':
            codePenality = CODE_PENALITE_ASSIETE_FAUSSE_DECLARATION;
            break;
    }

    var penaliteName = empty;

    for (var i = 0; i < ListPN.length; i++) {

        if (codePenality == ListPN[i].CODE) {
            selectedPenality = ListPN[i];
            penaliteName = ListPN[i].INTITULE;
            break;
        }
    }

    var ratePenality = ratePenality = getTaux(selectedPenality.LIST_TAUX_PANALIE, isRecidiviste);
    var valuePenalite = 0;

    if (isRetrait === true) {
        var remainMonths = parseInt(numberMonth - 1);
        valuePenalite = parseFloat(((parseFloat(amount) * 25) / 100));
        valuePenalite += parseFloat(((parseFloat(amount) * 2) / 100) * remainMonths);
    } else {
        valuePenalite = parseFloat(((parseFloat(amount) * parseFloat(ratePenality)) / 100) * numberMonth);
    }

    selectedPenality = {};

    selectedPenality.value = valuePenalite;
    selectedPenality.rate = ratePenality;
    selectedPenality.label = penaliteName;//selectedPenality.INTITULE;
    selectedPenality.principal = amount;
    selectedPenality.numberMonth = numberMonth === 0 ? 1 : numberMonth;
    selectedPenality.currency = currency;
    selectedPenality.referenceNumber = reference;
    selectedPenality.referenceLabel = reference;
    selectedPenality.type = '%';
    selectedPenality.id = codePenality;
    selectedPenality.canDelete = false;

    listPNSelected.push(selectedPenality);

}

function getPenalitiesReference(reference) {

    var penalities = [];

    for (var j = 0; j < listPNSelected.length; j++) {

        if (listPNSelected[j].referenceNumber == reference) {
            penalities.push(listPNSelected[j]);
        }
    }

    return penalities;
}

function getSumByReference(reference) {

    var penalitySUM = 0;

    for (var j = 0; j < listPNSelected.length; j++) {

        if (listPNSelected[j].referenceNumber == reference) {
            penalitySUM += listPNSelected[j].value;
        }
    }

    return penalitySUM;
}

function getTaux(listTaux, isRecidiviste) {

    var tauxList = JSON.parse(listTaux);
    var taux = 0;

    for (var i = 0; i < tauxList.length; i++) {
        if (isRecidiviste == 1) {
            if (tauxList[i].RECIDIVE == true) {
                taux = tauxList[i].VALEUR;
                return taux;
            }
        } else {
            taux = tauxList[i].VALEUR;
            return taux;
        }
    }

    return taux;
}

function loadTablePenalityValidate(data)
{
    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>\n\\n\
                <th style="text-align:left" hidden="true">Code pénalité</th>\n\\n\\n\\n\
                <th style="text-align:left">REFERENCE</th>\n\\n\
                <th style="text-align:left">PENALITE</th>\n\\n\
                <th style="text-aLign:left">PRINCIPAL DU</th>\n\\n\
                <th style="text-aLign:left">TAUX</th>\n\\n\\n\
                <th style="text-align:center">MOIS RETARD</th>\n\\n\\n\
                <th style="text-align:right">PENALITE DU</th>\n\\n\\n\
                </tr></thead>';

    var table = '';
    table += '<tbody id="bodyTable">';

    var sumPenalities = 0;

    for (var i = 0; i < data.length; i++) {

        sumPenalities += data[i].value;

        table += '<tr>';
        table += '<td style="text-align:left;width:5%" hidden="true">' + data[i].id + '</td>';
        table += '<td style="text-align:left;width:30%">' + data[i].referenceLabel + '</td>';
        table += '<td style="text-align:left;width:30%">' + data[i].label + '</td>';
        table += '<td style="text-align:left;width:15%">' + formatNumber(data[i].principal, data[i].currency) + '</td>';
        table += '<td style="text-align:left;width:15%">' + data[i].rate + ' ' + data[i].type + '</td>';
        table += '<td style="text-align:center;width:15%">' + data[i].numberMonth + '</td>';
        table += '<td style="text-align:right;width:15%">' + formatNumber(data[i].value, data[i].currency) + '</td>';
        table += '</tr>';

    }

    table += '</tbody>';
    var TableContent = header + table;
    tablePenaliteValidate.html(TableContent);
    tablePenaliteValidate.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune pénalité dans le panier",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        datalength: 5
    });

    lblSumPenaliteValidate.html(formatNumber(sumPenalities, referenceInformation.currency));

}

/*function validateFichePriseEnCharge() {
 
 if (listPNSelected.length > 0) {
 
 modalFichepriseEnChargeValidate = $("#modal5");
 tablePenaliteValidate = $("#tablePenaliteValidate");
 lblSumPenaliteValidate = $("#lblSumPenaliteValidate");
 
 modalFichepriseEnChargeValidate.modal('show');
 loadTablePenalityValidate(listPNSelected);
 
 } else {
 
 alertify.alert("no penality");
 }
 
 }*/