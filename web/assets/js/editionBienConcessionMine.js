/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbTypeBien;

var
        divComplement,
        divPanelComplement;

var
        inputIntitule,
        inputDescription,
        inputDateAcquissition,
        cmbCategorieBien,
        inputAdresse;

var
        btnAjouterAdresse,
        btnEnregistrer, btnEnregistrer2, BtnEditerTypeBien;

var
        tempTypeBienList = [],
        tempComplementList = [],
        tempCategorieBienList = [];

var
        codeOfPersonne,
        nameOfPersonne,
        selectedCodeAP = '',
        idOfBien = '',
        codeOfTypeBien = '';

var labelAssujettiName;
 var codeTypeDoc = 'TD00000035';
 var codeDoc = undefined;
 var archivesEditionBienConcessionMinier = '', lblNbDocument;

$(function () {


    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Edition d\'un bien concession minière');

    inputIntitule = $('#inputIntitule');
    inputDescription = $('#inputDescription');
    inputDateAcquissition = $('#inputDateAcquissition');
    cmbCategorieBien = $('#cmbCategorieBien');
    inputAdresse = $('#inputAdresse');

    labelAssujettiName = $('#labelAssujettiName');

    modalAdressesPersonne = $('#modalAdressesPersonne');
    modalEditerTypeBien = $('#modalEditerTypeBien');


    BtnEditerTypeBien = $('#BtnEditerTypeBien');
    lblNbDocument = $('#lblNbDocument');

    inputDescription.val('');
    inputAdresse.val('');

    var urlId = getUrlParameter('id');
    var urlName = getUrlParameter('nom');

    if (jQuery.type(urlId) !== 'undefined' && jQuery.type(urlName) !== 'undefined') {

        codeOfPersonne = atob(urlId);
        nameOfPersonne = atob(urlName);

        var codeBien = getUrlParameter('codeBien');
        if (jQuery.type(codeBien) !== 'undefined') {
            codeBien = atob(codeBien);
            secondNavigationLabel.text('Gestion d\'assujettissement -> Modification d\'un bien');
            idOfBien = codeBien.split(';')[0];
            codeOfTypeBien = codeBien.split(';')[1];
        } else {
            idOfBien = '';
            codeOfTypeBien = '';
        }
    }

    btnAjouterAdresse = $('#btnAjouterAdresse');
    btnAjouterAdresse.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        codePersonne = codeOfPersonne;
        nomPersonne = nameOfPersonne;

        loadAdressesOfPersonne();

        modalAdressesPersonne.modal('show');

    });

    btnEnregistrer = $('#btnEnregistrer');
    btnEnregistrer2 = $('#btnEnregistrer2');

    btnEnregistrer.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnEnregistrer2.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();
    });

    cmbTypeBien = $('#cmbTypeBien');
    cmbTypeBien.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var codeTypeBien = cmbTypeBien.val();
        loadTypeComplementBiens(codeTypeBien);
    });
    
    var catBien = cmbCategorieBien.val();
    
    cmbCategorieBien.on('change', function (e) {

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

       /*if (catBien !== cmbCategorieBien.val()) {
            initUpload(codeDoc, codeTypeDoc);
        }*/ 
    });

    BtnEditerTypeBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        modalEditerTypeBien.modal('show');
    });

    divComplement = $('#divComplement');
    divPanelComplement = $('#divPanelComplement');

    labelAssujettiName.text(nameOfPersonne.toUpperCase());
    ID_TYPE = 3;
    loadTypeBien();

});

function loadTypeBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeBienByService',
            'codeService': userData.serviceCode,
            'type': 3
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }
            if (response == '0') {
                $.unblockUI();
                alertify.alert('Aucun type bien retrouvé.');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = $.parseJSON(JSON.stringify(response));

                var typeBienList = result.typeBienList;
                var categorieBienList = result.categorieBienList;

                tempTypeBienList = [];
                tempCategorieBienList = [];

                var dataTypeBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < typeBienList.length; i++) {

                    var typeBien = new Object();
                    typeBien.codeTypeBien = typeBienList[i].codeTypeBien;
                    typeBien.libelleTypeBien = typeBienList[i].libelleTypeBien;
                    typeBien.estContractuel = typeBienList[i].estContractuel;
                    tempTypeBienList.push(typeBien);

                    dataTypeBien += '<option value =' + typeBienList[i].codeTypeBien + '>' + typeBienList[i].libelleTypeBien + '</option>';
                }

                cmbTypeBien.html(dataTypeBien);

                if (idOfBien != '') {
                    cmbTypeBien.val(codeOfTypeBien);
                    loadTypeComplementBiens(codeOfTypeBien);
                }


                var dataCategorieBien = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < categorieBienList.length; i++) {

                    var categorieBien = new Object();
                    typeBien.tarifCode = categorieBienList[i].tarifCode;
                    typeBien.tarifName = categorieBienList[i].tarifName;

                    tempCategorieBienList.push(categorieBien);

                    dataCategorieBien += '<option value =' + categorieBienList[i].tarifCode + '>' + categorieBienList[i].tarifName + '</option>';
                }

                cmbCategorieBien.html(dataCategorieBien);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadTypeComplementBiens(codeTypeBien) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadTypeComplementBien',
            'codeTypeBien': codeTypeBien,
            'idBien': idOfBien
        },
        beforeSend: function () {
            divPanelComplement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                divPanelComplement.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                divPanelComplement.unblock();

                var result = $.parseJSON(JSON.stringify(response));

                var typeComplementBienList = result.typeComplementBienList;

                var dataTypeComplementBien = '';
                for (var i = 0; i < typeComplementBienList.length; i++) {
                    dataTypeComplementBien += typeComplementBienList[i].inputValue;
                }
                divComplement.html(dataTypeComplementBien);

                if (result.bien != '') {

                    var dataBien = result.bien;
                    var complementList = dataBien.complementList;

                    inputIntitule.val(dataBien.intituleBien);
                    inputDescription.val(dataBien.descriptionBien);
                    inputDateAcquissition.val(dataBien.dateAcquisition);
                    cmbCategorieBien.val(dataBien.codeCategorie);

                    selectedCodeAP = dataBien.codeAdressePersonne;
                    inputAdresse.val(dataBien.chaineAdressePersonne);

                    var length = typeComplementBienList.length;

                    for (var i = 0; i < length; i++) {
                        var idControl = "#IDENT_" + i;
                        var name = $(idControl).attr("name");
                        var splitName = name.split('-');
                        var code = splitName[1];
                        var requiered = splitName[2];
                        for (var j = 0; j < complementList.length; j++) {
                            if (complementList[j].codeTypeComplementBien == code) {
                                $(idControl).val(complementList[j].valeur);
                                $(idControl).attr('name', complementList[j].id + '-' + code + '-' + requiered);
                                var uniteDevise = complementList[j].uniteDevise;
                                if (uniteDevise != '') {
                                    if (uniteDevise.length > 5) {
                                        $(idControl + '-u').val(uniteDevise);
                                    } else {
                                        $(idControl + '-d').val(uniteDevise);
                                    }
                                }
                            }
                        }
                    }
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            divPanelComplement.unblock();
            showResponseError();
        }

    });
}

function getSelectedAdressePersonneData() {

    inputAdresse.val(selectedAdressePersonne.chaine);
    selectedCodeAP = selectedAdressePersonne.codeAP;
}

function checkFields() {

    if (cmbTypeBien.val() === '0') {
        alertify.alert('Veuillez d\'abord sélectionner le type du bien.');
        return;
    }

    if (cmbCategorieBien.val().trim() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner la catégorie du bien.');
        return;
    }
    
    /*if (inputIntitule.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir l\'intitulé du bien.');
        return;
    }*/

    /*if (inputDescription.val().trim() === '') {
     alertify.alert('Veuillez d\'abord saisir la description du bien.');
     return;
     }*/

    /*if (inputDateAcquissition.val().trim() === '') {
     alertify.alert('Veuillez d\'abord saisir la date d\'acquisition du bien.');
     return;
     }*/

    var passe = true;

    if ($("#divComplement") != null && $("#divComplement").length > 0) {

        var count = 0;
        tempComplementList = [];

        $("#divComplement").children().each(function () {

            var idControl = "#IDENT_" + count;
            var value = $(this).find(idControl).val();
            var name = $(this).find(idControl).attr("name");
            var placeholder = $(this).find(idControl).attr("placeholder");

            var splitName = name.split('-');
            var id = splitName[0];
            var code = splitName[1];
            var requiered = splitName[2];

            var deviseId = $(idControl + "-d");
            var uniteId = $(idControl + "-u");

            var hasDevise = (deviseId.val() == undefined) ? false : true;
            var hasUnite = (uniteId.val() == undefined) ? false : true;

            var uniteDevise = '';

            if (requiered === '1') {

                if (value.trim() === '' || value === '0') {
                    passe = false;
                    alertify.alert(placeholder + ' est obligatoire.');
                    return;

                } else {

                    if (hasDevise) {

                        uniteDevise = deviseId.val();
                        if (uniteDevise == '0') {
                            passe = false;
                            alertify.alert('La devise du complément ' + placeholder + ' est obligatoire.');
                            return;
                        }

                    } else {

                        if (hasUnite) {
                            uniteDevise = uniteId.val();
                            if (uniteDevise == '0') {
                                passe = false;
                                alertify.alert('L\'unité du complément ' + placeholder + ' est obligatoire.');
                                return;
                            }
                        }
                    }
                }
            } else {

                if (hasDevise) {
                    uniteDevise = deviseId.val();
                    if (uniteDevise == '0') {
                        uniteDevise = '';
                    }

                } else {

                    if (hasUnite) {
                        uniteDevise = uniteId.val();
                        if (uniteDevise == '0') {
                            uniteDevise = '';
                        }
                    }
                }
            }

            count++;
            var complement = new Object();
            complement.id = id;
            complement.code = code;
            complement.valeur = value;
            complement.uniteDevise = uniteDevise;
            tempComplementList.push(complement);
        });
    }

    if (passe) {
        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce bien ?', function () {
            saveBien();
        });
    }
}

function saveBien() {

    var complementsBien = JSON.stringify(tempComplementList);

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveBien',
            'idBien': idOfBien,
            'codeTypeBien': cmbTypeBien.val(),
            'codePersonne': codeOfPersonne,
            'intituleBien': inputIntitule.val(),
            'descriptionBien': inputDescription.val(),
            'dateAcquisition': inputDateAcquissition.val(),
            'codeAP': selectedCodeAP,
            'idUser': userData.idUser,
            'codeCategorie': cmbCategorieBien.val(),
            'complementBiens': complementsBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du bien ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('Le bien est enregistré avec succès.');

                    setTimeout(function () {
                        window.location = 'gestion-bien?id=' + btoa(codeOfPersonne);
                    }
                    , 1500);

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du bien.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesEditionBienConcessionMinier = getUploadedData();

    nombre = JSON.parse(archivesEditionBienConcessionMinier).length;

    switch (nombre) {
        case 0:
            lblNbDocument.text('');
            break;
        case 1:
            lblNbDocument.html('1 documents');
            break;
        default:
            lblNbDocument.html(nombre + ' documents');
    }
}

//function loadBienForEdition(idBien) {
//
//    $.ajax({
//        type: 'POST',
//        url: 'assujetissement_servlet',
//        dataType: 'JSON',
//        headers: {
//            'Access-Control-Allow-Origin': '*'
//        },
//        crossDomain: true,
//        data: {
//            'operation': 'getBienForEdition',
//            'idBien': idBien
//        },
//        beforeSend: function () {
//
//            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
//
//        },
//        success: function (response)
//        {
//
//            if (response == '-1') {
//                $.unblockUI();
//                showResponseError();
//                return;
//            }
//
//            setTimeout(function () {
//                $.unblockUI();
//
//                window.location = 'edition-bien?id=' + btoa(codeResponsible) + '&nom=' + btoa(nameResponsible);
//
//                alert(codeResponsible + ' - ' + nameResponsible);

//                var personneList = JSON.parse(JSON.stringify(response));
//                tempPersonneList = [];
//                for (var i = 0; i < personneList.length; i++) {
//                    var personne = new Object();
//                    personne.libelleFormeJuridique = personneList[i].libelleFormeJuridique;
//                    personne.nif = personneList[i].nif;
//                    personne.nom = personneList[i].nom;
//                    personne.postNom = personneList[i].postNom;
//                    personne.prenom = personneList[i].prenom;
//                    personne.telephone = personneList[i].telephone;
//                    personne.email = personneList[i].email;
//                    personne.chaine = personneList[i].chaine;
//                    personne.codePersonne = personneList[i].codePersonne;
//                    personne.nomComplet = personneList[i].nomComplet;
//                    tempPersonneList.push(personne);
//                }
//
//                printPersonnes(tempPersonneList);
//
//            }
//            , 1);
//        },
//        complete: function () {
//
//        },
//        error: function (xhr, status, error) {
//            alert(xhr.status);
//            $.unblockUI();
//            showResponseError();
//        }
//
//    });
//}
