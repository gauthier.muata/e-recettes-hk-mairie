/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeDocumentResearch, researchType, researchValue, btnSimpleSearch;
var advancedSearch;
var typeDocumentCode;

var tableJournal;
var journalList = [];
var dbList = [];

var typeDocumentResearchAdvanced;
var btnAdvencedSearch;

var inputDateDebut, inputdateLast;

var btnShowAdvancedSerachModal;
var modalRechercheAvancee;

var codeTypeDocument;
var idRecord;
var codeJrnalVolet2 = '';

var traitementPaiementJournalModal, cmbAvisTraitementPaiement, btnTraiterPaiement, txtObservationTraitementPaiement;

var divTableauDB, tableDB;
var divInfoTraitementPaiement;

var isAdvance, lblTypeDocument, lblAgent, lblDateDebut, lblDateFin;

var idValueAgentTraitement, idValueDateTraitement;
var divInfoTraitementPaiement2;

var lblStatePaiement;

var idSpan1, idSpan2, idSpan3, idSpan4;

$(function () {

    mainNavigationLabel.text('PAIEMENT');
    secondNavigationLabel.text('Registre des paiements');

    removeActiveMenu();
    linkMenuPaiement.addClass('active');

    typeDocumentResearch = $('#typeDocumentResearch');
    researchType = $('#researchType');
    researchValue = $('#researchValue');
    btnSimpleSearch = $('#btnSimpleSearch');
    tableJournal = $('#tableJournal');
    typeDocumentResearchAdvanced = $('#typeDocumentResearchAdvanced');
    btnAdvencedSearch = $('#btnAdvencedSearch');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');
    modalRechercheAvancee = $('#modalRechercheAvancee');

    idValueAgentTraitement = $('#idValueAgentTraitement');
    idValueDateTraitement = $('#idValueDateTraitement');

    traitementPaiementJournalModal = $('#traitementPaiementJournalModal');
    cmbAvisTraitementPaiement = $('#cmbAvisTraitementPaiement');
    btnTraiterPaiement = $('#btnTraiterPaiement');
    txtObservationTraitementPaiement = $('#txtObservationTraitementPaiement');
    divInfoTraitementPaiement2 = $('#divInfoTraitementPaiement2');

    divTableauDB = $('#divTableauDB');
    tableDB = $('#tableDB');
    divInfoTraitementPaiement = $('#divInfoTraitementPaiement');

    isAdvance = $('#isAdvance');
    lblTypeDocument = $('#lblTypeDocument');
    lblAgent = $('#lblAgent');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblStatePaiement = $('#lblStatePaiement');

    idSpan1 = $('#idSpan1');
    idSpan2 = $('#idSpan2');
    idSpan3 = $('#idSpan3');
    idSpan4 = $('#idSpan4');

    txtObservationTraitementPaiement.val(empty);
    var npMairie = typeDocumentResearch.val();

    typeDocumentResearch.on('change', function (e) {
        if (typeDocumentResearch.val() == '0') {
            alertify.alert('Veuillez sélectionner un type document valide');
            return;
        } else {
            typeDocumentCode = typeDocumentResearch.val();

        }
    });

    researchType.on('change', function (e) {
        if (researchType.val() == '1') {
            researchValue.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else if (researchType.val() == '2') {
            researchValue.attr('placeholder', 'Veuillez saisir la référence du document');
        }
    });

    btnSimpleSearch.click(function (e) {
        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (researchValue.val() === empty || researchValue.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
        } else {
            if (typeDocumentResearch.val() == '0') {
                alertify.alert('Veuillez sélectionner un document valide avant de rechercher un paiement');
                return;
            } else {
                advancedSearch = '0';
                getCurrentSearchParam(advancedSearch);
                loadPaiementByDocument(advancedSearch);

            }
        }
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (typeDocumentResearchAdvanced.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un type document valide avant de rechercher un paiement');
        } else {
            advancedSearch = '1';
            getCurrentSearchParam(advancedSearch);
            loadPaiementByDocument(advancedSearch)
        }
        ;

    });

    btnShowAdvancedSerachModal.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvancee.modal('show');
    });

    btnTraiterPaiement.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        checkValueTraitement();
    });


    //loadJournalTable('', 0);
    loadJournalInfos('');
    btnAdvencedSearch.trigger('click');
});



function loadPaiementByDocument(value) {

    var viewAllSite = 0;
    var agentSiteParam = '';
    var researchPaymentFromBank = '1';

    if (!controlAccess('VIEW_ALL_SITE')) {

        if (controlAccess('VIEW_PAIEMENTS_SITE')) {

            viewAllSite = 1;
            agentSiteParam = userData.SiteCode;

            if (researchPaymentFromBank == '0') {
                agentSiteParam = userData.SiteCode;
            }

        } else {
            agentSiteParam = userData.idUser;
        }
    }

    var startDateSearch;
    var endDateSearch;
    var operation;

    var typeDocumentName = advancedSearchParam.typeDocumentName;
    var agentName = advancedSearchParam.agentName;

    //lblTypeDocument.text(getsearchbarText(advancedSearchParam.typeDocumentCode.length, typeDocumentName));
    lblTypeDocument.text(typeDocumentName.toUpperCase());
    lblTypeDocument.attr('title', typeDocumentName);

    lblAgent.text(agentName.toUpperCase());
    lblAgent.attr('title', agentName.toUpperCase());

    if (value === '1') {
        idSpan1.attr('style', 'display: inline');
        idSpan2.attr('style', 'display: inline');
        idSpan3.attr('style', 'display: inline');
        idSpan4.attr('style', 'display: inline');

        switch (advancedSearchParam.statePaiementCode) {
            case '*':

                idSpan1.attr('style', 'font-size: 16px;color: black');
                idSpan2.text('TOUT');
                idSpan2.attr('title', 'TOUT');
                idSpan2.attr('style', 'font-weight: bold;font-size: 16px;font-style: italic');

                break;
            default:

                idSpan1.attr('style', 'font-size: 16px;color: black');
                idSpan2.text(advancedSearchParam.statePaiementName);
                idSpan2.attr('title', advancedSearchParam.statePaiementName);
                idSpan2.attr('style', 'font-weight: bold;font-size: 16px;font-style: italic');


                break;
        }

        switch (advancedSearchParam.accountBankCode) {
            case '*':
                idSpan3.attr('style', 'font-size: 16px;color: black');
                idSpan4.text('TOUT');
                idSpan4.attr('title', 'TOUT');
                idSpan4.attr('style', 'font-weight: bold;font-size: 16px;font-style: italic');
                break;
            default:
                idSpan3.attr('style', 'font-size: 16px;color: black');
                idSpan4.text(advancedSearchParam.accountBankName);
                idSpan4.attr('title', advancedSearchParam.accountBankName);
                idSpan4.attr('style', 'font-weight: bold;font-size: 16px;font-style: italic');
                break;
        }
    } else {
        idSpan1.attr('style', 'display: none');
        idSpan2.attr('style', 'display: none');
        idSpan3.attr('style', 'display: none');
        idSpan4.attr('style', 'display: none');
    }


    lblDateDebut.text(advancedSearchParam.dateDebut);
    lblDateFin.text(advancedSearchParam.dateFin);


    operation = "searchPaymentFromBank";

    if (value === '1') {
        startDateSearch = inputDateDebut.val();
        endDateSearch = inputdateLast.val();
        modalRechercheAvancee.modal('hide');
    }

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': researchType.val(),
            'valueSearch': researchValue.val().trim(),
            'typeDocument': value == '0' ? typeDocumentResearch.val().trim() : typeDocumentResearchAdvanced.val(),
            'operation': operation,
            'allSite': viewAllSite,
            'advancedSearch': value,
            'compteBancaire': selectCompteBancaire.val(),
            'idEtatPaiement': selectStatePaiement.val(),
            'agentOrSite': agentSiteParam,
            'fromResearch': researchPaymentFromBank,
            'periodeDebutValue': startDateSearch,
            'periodeFinValue': endDateSearch
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            if (response == '-1') {

                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {
                $.unblockUI();
                loadJournalInfos('');

                switch (value) {
                    case '0':
                        alertify.alert('Aucun paiement ne correspond à cette référence du document : ' + researchValue.val().trim());
                        break;
                    case '1':
                        alertify.alert('Aucun paiement ne correspond au critère de recherche fournis');
                        break;
                }

                return;
            } else {
                setTimeout(function () {
                    $.unblockUI();
                    journalList = JSON.parse(JSON.stringify(response));
                    loadJournalInfos(journalList);

                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function loadJournalInfos(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    tableContent += '<tr>';
    tableContent += '<th>DATE PAIEMENT</th>';
    tableContent += '<th>DOCUMENT</th>';
    tableContent += '<th>ASSUJETTI</th>';
    tableContent += '<th>REFERENCE DOCUMENT</th>';
    tableContent += '<th>REFERENCE PAIEMENT</th>';
    tableContent += '<th>BANQUE</th>';
    tableContent += '<th>COMPTE BANCAIRE</th>';
    tableContent += '<th>ETAT</th>';
    tableContent += '<th>AGENT PAIEMENT</th>';
    tableContent += '<th style="text-align:right">MONTANT VOLET 1</th>';
    tableContent += '<th style="text-align:right">MONTANT VOLET 2</th>';
    tableContent += '<th style="text-align:right">TOTAL MONTANT PAYE</th>';
    tableContent += '<th></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var buttonValue = '';

        tableContent += '<tr>';

        switch (result[i].typeDocumentCode) {
            case 'AMR':
            case 'BP':
            case 'NP':

                divTableauDB.attr('style', 'visibility: hidden');

                switch (result[i].idEtatPaiement) {
                    case 2:
                    case 3:
                    case 4:
                        buttonValue = '<button style="text-align:center;width:5% class="btn btn-warning" onclick="displayTraitementPaiement(\'' + result[i].codeJournal + '\',\'' + result[i].typeDocumentCode + '\',\'' + result[i].idEtatPaiement + '\',\'' + result[i].observation + '\',\'' + result[i].dateValidationPaiement + '\',\'' + result[i].agentValidationPaiement + '\',\'' + result[i].codeJournalVolet2 + '\',\'' + result[i].amount2 + '\')"><i class="fa fa-list"></i></button>';
                        break;

                }

                break;
            case 'DEC':

                divTableauDB.attr('style', 'visibility: visible');
                dbList = JSON.parse(result[i].detailBordereauList);

                switch (result[i].idEtatPaiement) {
                    case 2:
                    case 3:
                    case 4:
                        buttonValue = '<button style="text-align:center;width:5% class="btn btn-warning" onclick="displayTraitementPaiement(\'' + result[i].codeJournal + '\',\'' + result[i].typeDocumentCode + '\',\'' + result[i].idEtatPaiement + '\',\'' + result[i].observation + '\',\'' + result[i].dateValidationPaiement + '\',\'' + result[i].agentValidationPaiement + '\',\'' + result[i].codeJournalVolet2 + '\',\'' + result[i].amount2 + '\')"><i class="fa fa-list"></i></button>';
                        break;
                }
                break;
        }

        tableContent += '<td style="text-align:left;width:5%">' + result[i].datePaiement + '</td>';
        tableContent += '<td style="text-align:left;width:8%">' + result[i].typeDocument + '</td>';
        tableContent += '<td style="text-align:left;width:14%">' + result[i].assujettiName + '</td>';
        tableContent += '<td style="text-align:left;width:8%">' + result[i].documentApure + '</td>';
        tableContent += '<td style="text-align:left;width:8%">' + result[i].bordereau + '</td>';
        tableContent += '<td style="text-align:left;width:13%">' + result[i].banqueName + '</td>';
        tableContent += '<td style="text-align:left;width:8%">' + result[i].compteBancaire + '</td>';
        tableContent += '<td style="text-align:left;width:5%">' + result[i].etatPaiement + '</td>';
        tableContent += '<td style="text-align:left;width:9%">' + result[i].agentPaiement + '</td>';
        tableContent += '<td style="text-align:right;width:9%">' + formatNumber(result[i].montantPaye, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:right;width:9%">' + formatNumber(result[i].amount2, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:right;width:9%">' + formatNumber(result[i].montantPaye + result[i].amount2, result[i].devise) + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonValue + '</td>';
        tableContent += '</tr>';
    }


    tableContent += '</tbody>';
    tableJournal.html(tableContent);

    tableJournal.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        paging: true,
        pageLength: 6,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function displayTraitementPaiement(codeJournal, typeDocument, statePayment, observation, dateValidationPaiement, agentValidation, codeJournalVolet2, amuont2) {

    codeTypeDocument = typeDocument;
    idRecord = codeJournal;
    codeJrnalVolet2 = codeJournalVolet2;
    var value = parseFloat(amuont2);

    switch (typeDocument) {
        case 'AMR':
        case 'BP':
        case 'NP':

            if (statePayment == '3') {

                if (value > 0) {
                    btnTraiterPaiement.attr('style', 'visibility: visible');
                    divInfoTraitementPaiement2.attr('style', 'visibility: hidden');
                    divInfoTraitementPaiement.attr('style', 'visibility: visible');

                    cmbAvisTraitementPaiement.val('0');
                    txtObservationTraitementPaiement.val(empty);

                    cmbAvisTraitementPaiement.attr('disabled', false);
                    txtObservationTraitementPaiement.attr('readOnly', false);

                    idValueDateTraitement.html(empty);
                    idValueAgentTraitement.html(empty);
                } else {
                    divInfoTraitementPaiement2.attr('style', 'visibility: visible');
                    idValueDateTraitement.html(dateValidationPaiement);
                    idValueAgentTraitement.html(agentValidation);

                    cmbAvisTraitementPaiement.val(statePayment);
                    cmbAvisTraitementPaiement.attr('disabled', true);

                    txtObservationTraitementPaiement.val(observation);
                    txtObservationTraitementPaiement.attr('readOnly', true);
                    btnTraiterPaiement.attr('style', 'visibility: hidden');
                    alertify.alert('Veuillez payé le volet B');
                    break;
                }

            } else if (statePayment == '2') {

                divInfoTraitementPaiement2.attr('style', 'visibility: visible');
                idValueDateTraitement.html(dateValidationPaiement);
                idValueAgentTraitement.html(agentValidation);

                cmbAvisTraitementPaiement.val(statePayment);
                cmbAvisTraitementPaiement.attr('disabled', true);

                txtObservationTraitementPaiement.val(observation);
                txtObservationTraitementPaiement.attr('readOnly', true);
                //divInfoTraitementPaiement.attr('style', 'visibility: hidden');
                btnTraiterPaiement.attr('style', 'visibility: hidden');
            }

            traitementPaiementJournalModal.modal('show');
            break;
        case 'DEC':
            if (statePayment == '3') {
                btnTraiterPaiement.attr('style', 'visibility: visible');
                divInfoTraitementPaiement2.attr('style', 'visibility: hidden');
                divInfoTraitementPaiement.attr('style', 'visibility: visible');

                cmbAvisTraitementPaiement.val('0');
                txtObservationTraitementPaiement.val(empty);

                cmbAvisTraitementPaiement.attr('disabled', false);
                txtObservationTraitementPaiement.attr('readOnly', false);

                idValueDateTraitement.html(empty);
                idValueAgentTraitement.html(empty);

            } else {//if (statePayment == '2') {

                divInfoTraitementPaiement2.attr('style', 'visibility: visible');
                idValueDateTraitement.html(dateValidationPaiement);
                idValueAgentTraitement.html(agentValidation);

                cmbAvisTraitementPaiement.val(statePayment);
                cmbAvisTraitementPaiement.attr('disabled', true);

                txtObservationTraitementPaiement.val(observation);
                txtObservationTraitementPaiement.attr('readOnly', true);
                //divInfoTraitementPaiement.attr('style', 'visibility: hidden');
                btnTraiterPaiement.attr('style', 'visibility: hidden');
            }

            printDetailDB(dbList);
            break;
    }
}

function printDetailDB(dbList) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    tableContent += '<tr>';
    tableContent += '<th>PERIODE DECLARATION</th>';
    tableContent += '<th>ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:right">MONTANT PAYE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumMontantPayeUSD = 0;
    var sumMontantPayeCDF = 0;

    for (var i = 0; i < dbList.length; i++) {

        switch (dbList[i].devise) {
            case 'USD':
                sumMontantPayeUSD += parseFloat(dbList[i].montantPercu);
                break;
            case 'CDF':
                sumMontantPayeCDF += parseFloat(dbList[i].montantPercu);
                break;
        }

        tableContent += '<tr>';

        tableContent += '<td style="text-align:left;width:10%">' + dbList[i].periodeName + '</td>';
        tableContent += '<td style="text-align:left;width:15%">' + dbList[i].articleBudgetaireName + '</td>';
        tableContent += '<td style="text-align:right;width:10%">' + formatNumber(dbList[i].montantPercu, dbList[i].devise) + '</td>';
        tableContent += '</tr>';
    }


    tableContent += '</tbody>';
    /*tableContent += '<tfoot>';
     tableContent += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:green"></th><th></th></tr>';
     tableContent += '</tfoot>';*/
    tableDB.html(tableContent);

    tableDB.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        paging: false,
        pageLength: 10,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
                /*,footerCallback: function (row, data, start, end, display) {
                 var api = this.api(), data;
                 $(api.column(3).footer()).html(
                 formatNumber(sumMontantPayeCDF, 'CDF') +
                 '<hr/>' +
                 formatNumber(sumMontantPayeUSD, 'USD')
                 );
                 
                 }*/
    });

    traitementPaiementJournalModal.modal('show');
}

function checkValueTraitement() {

    if (cmbAvisTraitementPaiement.val() == '0') {
        alertify.alert('Veuillez d\'abord sélectionner un avis sur votre traitement');
        return;
    }

    if (txtObservationTraitementPaiement.val() == empty) {

        alertify.alert('Veuillez d\'abord fournir les observations sur votre traitement');
        return;

    }

    var typeTraitement = '';

    switch (cmbAvisTraitementPaiement.val()) {
        case '2':
            typeTraitement = 'Confirmer ce paiement ';
            break;
        case '4':
            typeTraitement = 'Rejeter ce paiement ';
            break;
    }

    alertify.confirm('Etes-vous sûre de vouloir ' + typeTraitement + ' ? ', function () {
        traiterPaiement(cmbAvisTraitementPaiement.val());
    });
}

function traiterPaiement(typeTraitement) {

    $.ajax({
        type: 'POST',
        url: 'paiement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'referenceRecord': idRecord,
            'referenceRecordVolet2': codeJrnalVolet2,
            'typeDocumentCode': codeTypeDocument,
            'userId': userData.idUser,
            'observation': txtObservationTraitementPaiement.val(),
            'valueTraitement': typeTraitement,
            'operation': 'traitementPaiement'
        },
        beforeSend: function () {

            switch (typeTraitement) {
                case '2':
                    traitementPaiementJournalModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >La confirmation du paiement est en cours...</h5>'});
                    break;
                case '4':
                    traitementPaiementJournalModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Le rejet du paiement est en cours...</h5>'});
                    break;
            }

        },
        success: function (response)
        {
            traitementPaiementJournalModal.unblock();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {
                setTimeout(function () {
                    //traitementPaiementJournalModal.modal('hide');

                    switch (typeTraitement) {
                        case '2':
                            traitementPaiementJournalModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >La confirmation du paiement s\'est effectuée avec succès</h5>'});
                            break;
                        case '4':
                            traitementPaiementJournalModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Le rejet du paiement s\'est effectué avec succès</h5>'});
                            break;
                    }

                    setTimeout(function () {

                        traitementPaiementJournalModal.unblock();
                        traitementPaiementJournalModal.modal('hide');

                        switch (advancedSearch) {
                            case '0':
                                btnSimpleSearch.trigger('click');
                                break;
                            case '1':
                                btnAdvencedSearch.trigger('click');
                                break;
                        }

                    }, 2000);
                }
                , 2000);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            traitementPaiementJournalModal.unblock();
            showResponseError();
        }
    });
}
