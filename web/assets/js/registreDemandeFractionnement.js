/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var btnSimpleSearch, btnAdvencedSearch, btnShowAdvancedSerachModal;

var inputDateDebut, inputdateLast;


var codeTypeDoc = 'TD00000052';
var codeDoc = undefined;

var tableEchelonnement, tableDemandeEchelonnement, ResearchType, ResearchValue;

var modalDetailEchelonnement, modalTraitementEchelonnement, tableDetailEchelonnement;
var tempEchelonnementList = [];

var lblNameAssujettiModalDetailEchelonnement,
        lblNumTitreModalTraitementEchelonnement,
        lblMontantDuModalTraitementEchelonnement, lblEtatModalTraitementEchelonnement, lblAmountInteretEchelonnement;

var inputNombreIntercalaire, txtObservalitionValidation;

var btnValiderTraitement, btnJoindreArchiveDecison, lblNbDocumentDecision;

var echelonnementDocumentAttach = '';

var selectedEchelonnementDecisonArchive = '';

var btnShowArchiveEchelonnement;

var isDecisionUpload, isDecisonProroger;

var selectedDecision = null;

var titrePrincipalAmount = 0;
var selectedTitre, selectedTitreForDisplay, selectedTitreType, selectedDemandeEchelonnementCode;

var modalProrogationEcheance, inputDateEcheanceProrogation, btnValidateDateProrogation;
var selectedEcheanceToProroge = '', selectedTitreNumeroToProroge = '', selectedTitreMereNumeroToProroge = '', selectedTypeTitreTitreToProroge = '';

var listNpFilleProrogation = [], listNpFilleProrogationValidate = [];

var typeSearch;

var selectedAssujettiCode;

var advancedSearchParam;
var dateLimiteEcheanceRevocationByIntercalaire = '';

var titreCheckingList = [];

var paramDashboard;

var INTERET_ECHELONNEMENT = 0.10;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;

$(function () {

    mainNavigationLabel.text('FRACTIONNEMENT');
    secondNavigationLabel.text('Registre des demandes de fractionnement');

    removeActiveMenu();
    linkMenuFractionnement.addClass('active');

//    if (!controlAccess('12002')) {
//        window.location = 'dashboard';
//    }

    modalProrogationEcheance = $('#modalProrogationEcheance');
    inputDateEcheanceProrogation = $('#inputDateEcheanceProrogation');
    btnValidateDateProrogation = $('#btnValidateDateProrogation');

    modalDetailEchelonnement = $('#modalDetailEchelonnement');
    modalTraitementEchelonnement = $('#modalTraitementEchelonnement');
    tableDetailEchelonnement = $('#tableDetailEchelonnement');
    tableDemandeEchelonnement = $('#tableDemandeEchelonnement');

    lblNameAssujettiModalDetailEchelonnement = $('#lblNameAssujettiModalDetailEchelonnement');
    lblNumTitreModalTraitementEchelonnement = $('#lblNumTitreModalTraitementEchelonnement');
    lblEtatModalTraitementEchelonnement = $('#lblEtatModalTraitementEchelonnement');
    lblMontantDuModalTraitementEchelonnement = $('#lblMontantDuModalTraitementEchelonnement');
    lblAmountInteretEchelonnement = $('#lblAmountInteretEchelonnement');
    btnShowArchiveEchelonnement = $('#btnShowArchiveEchelonnement');

    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');
    lblAgent = $('#lblAgent');

    btnValiderTraitement = $('#btnValiderTraitement');
    btnJoindreArchiveDecison = $('#btnJoindreArchiveDecison');
    lblNbDocumentDecision = $('#lblNbDocumentDecision');

    inputNombreIntercalaire = $('#inputNombreIntercalaire');
    txtObservalitionValidation = $('#txtObservalitionValidation');

    ResearchType = $('#ResearchType');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    ResearchValue = $('#ResearchValue');
    btnSimpleSearch = $('#btnSimpleSearch');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    selectService = $('#selectService');
    selectSite = $('#selectSite');

    tableEchelonnement = $('#tableEchelonnement');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    codeResearch = ResearchType.val();

    isAdvance.attr('style', 'display: none');

    ResearchType.on('change', function (e) {

        codeResearch = ResearchType.val();

        if (codeResearch === "1") {
            assujettiModal.modal('show');

        } else if (codeResearch == "2") {

            ResearchValue.attr('placeholder', 'Numéro de la note de perception');
            ResearchValue.val('');

        } else {
            ResearchValue.attr('placeholder', 'Référence de la lettre');
            ResearchValue.val('');
        }
    });

    btnSimpleSearch.click(function (e) {
        e.preventDefault();

        if (codeResearch == "1") {

            ResearchValue.val(empty);
            assujettiModal.modal('show');
            return;
        }

        if (ResearchValue.val() === empty || ResearchValue.val().length < SEARCH_MIN_TEXT) {

            if (codeResearch === "2") {

                showEmptyMessage('<b>Numéro du titre de perception<b/>');

            } else {

                showEmptyMessage('<b>Référence de la lettre<b>');

            }

        } else {
            getRegistreDemandeEchelonnement('1');
        }
    });

    btnShowAdvancedSerachModal.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('hide');
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        getRegistreDemandeEchelonnement('2');
    });

    btnShowArchiveEchelonnement.click(function (e) {

        e.preventDefault();
        initPrintData(echelonnementDocumentAttach);

    });

    btnJoindreArchiveDecison.click(function (e) {

        e.preventDefault();
        isDecisionUpload = 1;

        if (selectedEchelonnementDecisonArchive == '') {

            initUpload(codeDoc, codeTypeDoc);

        } else {

            initUpload(selectedEchelonnementDecisonArchive, codeTypeDoc);
        }

    });

    lblNbDocumentDecision.click(function (e) {

        initUpload(selectedEchelonnementDecisonArchive, codeTypeDoc);

        e.preventDefault();

    });

    $('input[name=radioValider]').on('change', function (e) {

        e.preventDefault();

        selectedDecision = $('input[name=radioValider]:checked').val();

    });

    inputNombreIntercalaire.on('change', function (e) {
        e.preventDefault();
        generateTitreINtercalaire();
    });

    btnValiderTraitement.click(function (e) {
        e.preventDefault();

        if (selectedDecision == null) {
            alertify.alert('Veuillez sélectionner une décision!');
            return;
        }

        if (selectedEchelonnementDecisonArchive == empty) {

            alertify.alert('Veuillez joindre la décison de l\'autorité!');
            return;


        } else {

            var nombre = JSON.parse(selectedEchelonnementDecisonArchive).length;

            if (nombre < 1) {

                alertify.alert('Veuillez joindre la décision de l\'autorité');
                return;
            }

        }

        if (selectedDecision == '1') {
            if (controlDate()) {
                checkFields();
            }
        } else {

            alertify.confirm('Etes-vous sûre de vouloir valider l\'échelonnement du titre de perception ' + selectedTitre + ' ?', function () {
                validerDemandeEchelonnement('');
            });

        }

    });

    btnValidateDateProrogation.click(function (e) {

        e.preventDefault();

        if (inputDateEcheanceProrogation.val() == empty) {

            alertify.alert('Veuillez fournir la nouvelle échéance de paiement.');
            return;
        }

        var currentDate = new Date(selectedEcheanceToProroge);
        var selectDate = new Date(inputDateEcheanceProrogation.val());

        var selectMonth = (selectDate.getMonth() + 1);
        var currentMonth = (currentDate.getMonth() + 1);

        if (selectMonth < 10) {
            selectMonth = '0' + selectMonth;
        }

        if (currentMonth < 10) {
            currentMonth = '0' + currentMonth;
        }

        var selectedDay = selectDate.getDate();
        var currentDay = currentDate.getDate();

        if (selectedDay < 10) {
            selectedDay = '0' + selectedDay;
        }

        if (currentDay < 10) {
            currentDay = '0' + currentDay;
        }

        var selectDate_fullDate_format = selectedDay + '/' + selectMonth + '/' + selectDate.getFullYear();
        var current_fullDate_format = currentDay + '/' + currentMonth + '/' + currentDate.getFullYear();

        selectDate = new Date(selectMonth + '/' + selectedDay + '/' + selectDate.getFullYear());

        if (current_fullDate_format == selectDate_fullDate_format) {

            alertify.alert('La nouvelle échéance de paiement n\'est doit pas être égale à l\'actuelle échéance');
            return;

        } else if (currentDate.getTime() > selectDate.getTime()) {

            alertify.alert('La nouvelle échéance de paiement est inférieure à l\'actuelle échéance');
            return;

        }

        $('#lblDateEcheanceIntercalaire_' + selectedTitreNumeroToProroge).html(selectDate_fullDate_format);

        var npIntercalaireProrogation = {};

        npIntercalaireProrogation.numeroIntercalaire = selectedTitreNumeroToProroge.trim();
        npIntercalaireProrogation.TitreMerenumero = selectedTitreMereNumeroToProroge.trim();
        npIntercalaireProrogation.echeanceNpFilleProrogee = selectDate_fullDate_format;


        if (listNpFilleProrogation.length > 0) {

            for (var i = 0; i < listNpFilleProrogation.length; i++) {

                if (selectedTitreNumeroToProroge.trim() == listNpFilleProrogation[i].numeroIntercalaire.trim()) {

                    listNpFilleProrogation.splice(i, 1);
                    break;
                }
            }

            listNpFilleProrogation.push(npIntercalaireProrogation);

        } else {
            listNpFilleProrogation.push(npIntercalaireProrogation);
        }

        modalProrogationEcheance.modal('hide');

    });

    setTimeout(function () {

        init();

    }, 1000);

    loadTableDemandeEchelonnement('');
});

function init() {

    paramDashboard = getDashboardParam();

    if (paramDashboard == 'null' || paramDashboard == null) {

        setTimeout(function () {

            var dashboardParam = {};
            dashboardParam.service = selectService.val();
            dashboardParam.site = selectSite.val();
            dashboardParam.dateDebut = inputDateDebut.val();
            dashboardParam.dateFin = inputdateLast.val();
            dashboardParam.serviceLibelle = $('#selectService option:selected').text();
            dashboardParam.siteLibelle = $('#selectSite option:selected').text();

            sessionStorage.removeItem('dashboardParam');
            setDashboardParam(JSON.stringify(dashboardParam));

            init();

        }, 1000);

    } else {

        btnAdvencedSearch.trigger('click');
    }

}

function controlDate() {

    var result = true;
    var nombre = inputNombreIntercalaire.val();

    var date = new Date();
    var year = date.getFullYear() + '';

    var month = date.getMonth() + 1 + '';
    if (month.length == 1) {
        month = '0' + month;
    }

    var day = date.getDate() + '';
    if (day.length == 1) {
        day = '0' + day;
    }

    var dateJour = parseInt(year + month + day);

    for (var i = 0; i < nombre; i++) {

        var idEcheance = "#inputDate_" + i;
        //var idTitreManuel = "#inputTitreManuel_" + i;
        tableEchelonnement.find(idEcheance).attr('style', 'color:black;width:100%');
        var dateEcheance = $('#tableEchelonnement').find(idEcheance).val();

        if (dateEcheance == '') {
            alertify.alert('L\'échéance de la note intercalaire est invalide');
            tableEchelonnement.find(idEcheance).attr('style', 'color:red;width:100%');
            result = false;
            break;
        }



        dateEcheance = dateEcheance.replace(/-/g, '');
        dateEcheance = parseInt(dateEcheance);

        if (dateEcheance < dateJour) {
            alertify.alert('L\'échéance ne peut pas être inférieure à la date du jour');
            tableEchelonnement.find(idEcheance).attr('style', 'color:red;width:100%');
            result = false;
            break;
        }

        if (i > 0) {

            var indicePreview = "#inputDate_" + (i - 1);
            var datePrecedente = tableEchelonnement.find(indicePreview).val();
            datePrecedente = datePrecedente.replace(/-/g, '');
            datePrecedente = parseInt(datePrecedente);

            if (dateEcheance <= datePrecedente) {
                alertify.alert('L\'échéance ne peut pas être inférieure ou égale à la date précédente');
                tableEchelonnement.find(idEcheance).attr('style', 'color:red;width:100%');
                result = false;
                break;
            }
        }
    }

    return result;

}

function generateTitreINtercalaire() {

    var nombre = inputNombreIntercalaire.val();

    if (parseInt(nombre) < 2) {

        alertify.alert('La valeur saisit est incorrect');
        printGenerateEchelonnement('');
        return;
    }

    //droit pour plus de six tranches
    var canGoUpToSix = false;

    if (parseInt(nombre) > 6 && !canGoUpToSix) {

        alertify.alert('Vous ne pouvez pas aller au-delà de 6 tranches');
        printGenerateEchelonnement('');
        return;
    }

    var montant = titrePrincipalAmount;
    var montantByNote = montant / nombre;

    var partieEntier = Math.trunc(montantByNote);
    var resteMontant = montant - (partieEntier * nombre);

    var echeanceList = [];

    for (var i = 0; i < nombre; i++) {

        var echeance = new Object();
        if (i == 0) {
            echeance.montant = (partieEntier + resteMontant);
        } else {
            echeance.montant = partieEntier;
        }
        echeance.date = '';
        echeanceList.push(echeance);
    }

    printGenerateEchelonnement(echeanceList);

}

function printGenerateEchelonnement(result) {

    var header = '';
    header += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    header += '<tr>';
    header += '<th style="text-align:center">Tranche</th>';
    header += '<th>Montant de la tranche</th>';
    header += '<th>Echéance paiement</th>';
    //header += '<th>Numéro du titre de perception</th>';
    header += '</tr>';
    header += '</thead>';

    var data = '';
    data += '<tbody>';
    for (var i = 0; i < result.length; i++) {

        var inputNumber = "inputNumber_" + i;
        var inputDate = "inputDate_" + i;
        //var inputTitreManuel = "inputTitreManuel_" + i;
        var counter = i + 1;
        data += '<tr>';
        data += '<td style="text-align:center"> Tranche (' + counter + ')</input></td>';
        data += '<td><input id="' + inputNumber + '" onchange="ajustMontant()" type="number" style="width : 100%" value="' + result[i].montant + '"></input></td>';
        data += '<td><input id="' + inputDate + '" type="date" style="width : 100%" value="' + result[i].date + '"></input></td>';
        // data += '<td><span style="font-weight: bold">' + provincePrefix + '&nbsp;&nbsp;</span><input id="' + inputTitreManuel + '" onchange="checkValidateTitre(' + inputTitreManuel + ')" type="number" style="width : 70%""></input></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    var TableContent = header + data;

    tableEchelonnement.html(TableContent);

    tableEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune tranche",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 6,
        datalength: 3
    });
}

function getRegistreDemandeEchelonnement(isAdvancedSearch) {

    codeResearch = ResearchType.val();
    operation = "getListDemandeEchelonnement";

    typeSearch = isAdvancedSearch;

    if (isAdvancedSearch == '2') {

        lblSite.html($('#selectSite option:selected').text());
        lblService.html($('#selectService option:selected').text());
        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());

        isAdvance.attr('style', 'display: block');
    } else {
        isAdvance.attr('style', 'display: none');
    }

    var dsbParam = JSON.parse(paramDashboard);

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': codeResearch,
            'valueSearch': codeResearch == 1 ? selectedAssujettiCode : (ResearchValue.val()).trim(),
            'operation': operation,
            'modeSearch': isAdvancedSearch,
            'codeSite': dsbParam.site,
            'codeService': dsbParam.service,
            'dateDebut': dsbParam.dateDebut, //advancedSearchParam.dateDebut,
            'dateFin': dsbParam.dateFin, //advancedSearchParam.dateFin,
            'stateCondition': ' IN (0,1,2,3,4) ',
            'typeRegistre': '1'

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                loadTableDemandeEchelonnement('');
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadTableDemandeEchelonnement('');
                return;
            }

            setTimeout(function () {

                result = JSON.parse(JSON.stringify(response));
                loadTableDemandeEchelonnement(result);
            }
            , 1);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadTableDemandeEchelonnement(result) {

    tempEchelonnementList = result;

    var header = '';
    header += '<thead style="background-color:#0085c7;color:white" id="headerTable">';
    header += '<tr>';
    header += '<th>N° REF LETTRE</th>';
    header += '<th>DATE DE LA DEMANDE</th>';
    header += '<th>NTD</th>';
    header += '<th>ASSUJETTI</th>';
    header += '<th>AGENT CREATION</th>';
    header += '<th>OBSERVATION</th>';
    header += '<th></th>';
    header += '</tr>';
    header += '</thead>';

    var data = '';
    data += '<tbody id="bodyTable">';

    for (var i = 0; i < result.length; i++) {

        var firstLineObservation = '';

        if (result[i].observation.length > 120) {
            firstLineObservation = result[i].observation.substring(0, 120) + ' ...';
        } else {
            firstLineObservation = result[i].observation;
        }

        data += '<tr>';
        data += '<td style="text-align:left;width:10%">' + result[i].letterReference + '</td>';
        data += '<td style="text-align:left;width:10%">' + result[i].dateCreateDe + '</td>';
        data += '<td style="text-align:left;width:10%">' + result[i].userName + '</td>';
        data += '<td style="text-align:left;width:20%;" title="' + result[i].assujettiName + '"><span style="font-weight:bold;">' + result[i].assujettiName + '</span></td>';
        data += '<td style="text-align:left;width:10%">' + result[i].agentCreate + '</td>';
        data += '<td style="text-align:left;width:20%;" title="' + result[i].observation + '"">' + firstLineObservation + '</td>';
        data += '<td style="text-align:center;width:2%;vertical-align:middle;"><button type="button" class="btn btn-warning" onclick="showDetailEchelonnementModal(\'' + result[i].demandeEchelonnementCode + '\')"><i class="fa fa-list"></i></button></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    var TableContent = header + data;

    tableDemandeEchelonnement.html(TableContent);

    tableDemandeEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthChange: false,
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
        }
    });
}

function showDetailEchelonnementModal(id) {


    for (i = 0; i < tempEchelonnementList.length; i++) {

        if (id == tempEchelonnementList[i].demandeEchelonnementCode) {

            lblNameAssujettiModalDetailEchelonnement.html(tempEchelonnementList[i].assujettiName);
            echelonnementDocumentAttach = tempEchelonnementList[i].declarationDocumentList;
            modalDetailEchelonnement.modal('show');
            selectedDemandeEchelonnementCode = id;
            dateLimiteEcheanceRevocationByIntercalaire = '';
            printTitreList(JSON.parse(tempEchelonnementList[i].detailEchelonnement), id);
            break;
        }
    }
}

var tempTitreList = [];

function printTitreList(titreList, codeDemandeEchelonnement) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';

    tempTitreList = titreList;

    header += '<th> </th>';
    header += '<th> TYPE TITRE </th>';
    header += '<th> NUM. TITRE </th>';
    header += '<th> SECTEUR </th>';
    header += '<th> ARTICLE BUDGETAIRE </th>';
    header += '<th> DATE ORD.</th>';
    header += '<th> DATE EXI. </th>';
    header += '<th style="text-align:center"> MONTANT DÛ </th>';
    header += '<th> ETAT </th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < titreList.length; i++) {

        var firstLineAB = '';
        var validation = 'VALIDE';
        var colorEcheance = 'black', colorEtat = 'black';

        var showValidateButton = true;


        if (titreList[i].libelleArticleBudgetaire.length > 40) {

            firstLineAB = titreList[i].codeBudgetaire + ' / ' + titreList[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';

        } else {

            firstLineAB = titreList[i].codeBudgetaire + ' / ' + titreList[i].libelleArticleBudgetaire;

        }

        if (titreList[i].isEchanceEchus) {
            colorEcheance = 'red';
        }

        if (titreList[i].etat == '0') {

            validation = 'REJETEE <br/> par (' + titreList[i].agentValidation + ') <br/> le ' + titreList[i].dateValidation;
            colorEtat = 'red';

        } else if (titreList[i].etat == '1' || titreList[i].etat == '4') {

            validation = 'VALIDEE <br/> par (' + titreList[i].agentValidation + ') <br/> le ' + titreList[i].dateValidation;
            colorEtat = 'green';

        } else if (titreList[i].etat == '2') {

//            showValidateButton = controlAccess('12007')

            validation = 'EN ATTENTE';

        } else if (titreList[i].etat == '3') {

            validation = 'REVOQUEE';
            colorEtat = 'red';
        }

        var etatPaiement = '';
        var displayValidateButton = 'display : none';

        if (showValidateButton) {
            displayValidateButton = 'display : inline';
        }


//        if (titreList[i].isApured) {
//            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:blue">PAYE et <br/> (APURE) </a></center>';
//            validation = 'PAYE';
//        } else if (!titreList[i].isApured && titreList[i].isPaid) {
//            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:blue">PAYE et <br/> (NON APURE) </a></center>';
//            validation = 'PAYE';
//        } else {
//            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';
//        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="width:10%">' + getTypeDocumentName(titreList[i].TYPE_DOCUMENT) + '</td>';
        body += '<td style="width:10%">' + titreList[i].referenceDocumentManuel + '</td>';
        body += '<td>' + titreList[i].SERVICE + '</td>';
        body += '<td style="text-align:left;width:30%;" title="' + titreList[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        body += '<td style="width:8%">' + titreList[i].DATECREAT + '</td>';
        body += '<td style="width:8%;color:' + colorEcheance + '">' + titreList[i].DATE_ECHEANCE_PAIEMENT + '</td>';
        body += '<td style="width:10%;text-align:center">' + formatNumber(titreList[i].MONTANTDU, titreList[i].DEVISE) + ' ' + etatPaiement + '</td>';
        body += '<td style="width:20%;text-align:left;color:' + colorEtat + '">' + validation + '</td>';

        if (titreList[i].etat != '2' || titreList[i].isPaid) 
{
            body += '<td style="text-align:center;width:10%;"><a style="color:#00acee;text-decoration: underline;font-style: italic" href="#" title="' + titreList[i].observation + '">Observation</a></td>';
        } else {
            body += '<td style="text-align:center;width:10%"><a title="Traiter" style="' + displayValidateButton + '" onclick="validationEchelonnementByTitre(\'' + titreList[i].referenceDocument + '\',\'' + titreList[i].MONTANTDU + '\',\'' + titreList[i].DEVISE + '\',\'' + titreList[i].TYPE_DOCUMENT + '\',\'' + titreList[i].referenceDocumentManuel + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>';
        }
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableDetailEchelonnement.html(tableContent);

    var tableDetail = tableDetailEchelonnement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        scroller: true,
        columnDefs: [
            {"visible": false, "targets": 3}
        ],
        order: [[3, 'asc']],
        scrollCollapse: true,
        pageLength: 2,
        datalength: 2,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(3, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="9">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableDetailEchelonnement tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');
        var row = tableDetail.row(tr);

        var dataDetail = tableDetail.row(tr).data();
        var typeDocument = dataDetail[1];

        var numeroTitre = dataDetail[2];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        }
        else {

            var detailTable = '<center><h4>LES NOTES DES FRACTIONS </h4><br/></center><table class="table table-bordered">';

            detailTable += '<thead><tr style="background-color:#e6ceac;color:black"><td style="text-align:center">NUMERO TITRE</td><td style="text-align:center">ECHEANCE</td><td style="text-align:center">ECHEANCE PROROGEE</td><td style="text-align:center">MONTANT DU</td><td style="text-align:center">ETAT PAIEMENT</td></tr></thead>';

            for (var i = 0; i < tempTitreList.length; i++) {

                if (tempTitreList[i].referenceDocumentManuel == numeroTitre) {

                    if (tempTitreList[i].etat == '2' || tempTitreList[i].etat == '0') {

                        row.child('<center><h5>Aucun titre intercalaire.</h5></center>').show();
                        tr.addClass('shown');

                        return;
                    }

                    var listIntercalaireDetail = JSON.parse(tempTitreList[i].listIntercalaireDetail);
                    var etat = tempTitreList[i].etat;
                    var allPayed;
                    var canRevok = false;
                    var canProrog = false;
                    var revokFract;
                    var showRevocaquationAction;
                    var showProrogationAction;

                    if (etat != '3') {
                        canProrog = controlAccess('12006');
                    }

                    detailTable += '<tbody>';

                    for (var j = 0; j < listIntercalaireDetail.length; j++) {

                        if (listIntercalaireDetail[j].IS_INTERET)
                            continue;

                        allPayed = false;

                        var showEditProrogationAction = canProrog ? 'inline' : 'none';

                        var colorEcheance = 'black';
                        var etatPaiement = 'NON PAYE';

                        if (listIntercalaireDetail[j].isEchanceEchus && !listIntercalaireDetail[j].isPaid && etat != '3') {

                            canRevok = true;
                            colorEcheance = 'red';

                            if (dateLimiteEcheanceRevocationByIntercalaire == '' && !listIntercalaireDetail[j].isPaid) {
                                dateLimiteEcheanceRevocationByIntercalaire = listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT;
                            }
                        }
                        if (listIntercalaireDetail[j].isEchanceEchus) {

                            revokFract = true;
                        }

                        if (listIntercalaireDetail[j].isPaid) {

                            allPayed = true;
                            showEditProrogationAction = 'none';
                            etatPaiement = 'PAYE';

                        }

                        if (listIntercalaireDetail[j].NUMERO_MANUEL == empty) {
                            showEditProrogationAction = 'none';
                            showProrogationAction = 'none';
                        }

                        detailTable += '<tr>';
                        detailTable += '<td style="text-align:center;width:30%;">' + listIntercalaireDetail[j].NUMERO_MANUEL + '</td>';
                        detailTable += '<td style="text-align:center;width:20%;color:' + colorEcheance + '""> ' + listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT + ' </td>';
                        detailTable += '<td style="text-align:center;width:20%"><span id="lblDateEcheanceIntercalaire_' + listIntercalaireDetail[j].NUMERO + '"> ' + getEcheanceProrogerByTitre(listIntercalaireDetail[j].NUMERO) + ' </span>&nbsp;&nbsp;<a title="Modifier la date d\'échéance" style="display:' + showEditProrogationAction + '" onclick="showProrogerEcheanceModal(\'' + listIntercalaireDetail[j].NUMERO + '\',\'' + numeroTitre + '\',\'' + listIntercalaireDetail[j].DATE_ECHEANCE_PAIEMENT_V2 + '\')"><i class="fa fa-edit"></i><a/></td>';
                        detailTable += '<td style="text-align:right;width:20%;">' + formatNumber(listIntercalaireDetail[j].MONTANTDU, listIntercalaireDetail[j].DEVISE) + '</td>';
                        detailTable += '<td style="text-align:center:width:10%;">' + etatPaiement + '</td>';
                        detailTable += '</tr>';

                    }

                    if (revokFract) {
                        detailTable += '</tbody><tfoot>';
                        detailTable += '<tr><th colspan="3" style="vertical-align:middle;text-align:right"><i class="fa fa-check-circle"></i><th colspan="2" style="vertical-align:middle;text-align:right"><button class="btn btn-danger" onclick="validateRevocation(\'' + numeroTitre + '\',\'' + typeDocument + '\',\'' + selectedDemandeEchelonnementCode + '\')" type="submit" id="btnRevocation"><i class="fa fa-check-circle"></i> Révoquer fractionnement</button></th></tr>';
                        detailTable += '</tfoot></table>';
                    }

                    detailTable += '</tbody></table>';
                    row.child(detailTable).show();

                    tr.addClass('shown');

                    break;
                }

            }
        }

    });

}

function validationEchelonnementByTitre(titreId, amount, devise, type, titreIdManuel) {
    selectedTitre = titreId;
    selectedTitreForDisplay = titreIdManuel;
    selectedTitreType = type;
    titrePrincipalAmount = amount;
    lblNumTitreModalTraitementEchelonnement.html(titreIdManuel + ' &nbsp;&nbsp; (' + getTypeDocumentName(type) + ')');
    lblMontantDuModalTraitementEchelonnement.html(formatNumber(amount, devise));
    lblEtatModalTraitementEchelonnement.html("EN ATTENTE");
    lblAmountInteretEchelonnement.html('Intérêt échelonnement : ' + formatNumber(amount * INTERET_ECHELONNEMENT, devise));
    modalDetailEchelonnement.modal('hide');
    modalTraitementEchelonnement.modal('show');
}

function getEtatDemandeEchelonnement(etat) {

    switch (etat) {
        case 0:
            labelEtatDemande.html('Etat : <b>Demande rejetée</b>');
            break;
        case 1:
            labelEtatDemande.html('Etat : <b>Demande en attente d\'ordonnancement</b>');
            break;
        case 2:
            labelEtatDemande.html('Etat : <b>Demande en attente de validation</b>');
            break;
        case 3:
            labelEtatDemande.html('Etat : <b>Demande revoquée</b>');
            break;
        case 4:
            labelEtatDemande.html('Etat : <b>Demande ordonnancée</b>');
    }
}

function checkFields() {

    var nombre = inputNombreIntercalaire.val();
    var total = titrePrincipalAmount;

    if (nombre < 2) {
        alertify.alert('Veuillez d\'abord fournir le nombre de notes intercalaires à créer.');
        return;
    }


    var tempListEchelonnement = [];

    var totalNotesIntercalaires = 0;

    for (var i = 0; i < nombre; i++) {

        var idMontant = "#inputNumber_" + i;
        var idEcheance = "#inputDate_" + i;
        var idTitreManuel = "#inputTitreManuel_" + i;
        var valueMontant = tableEchelonnement.find(idMontant).val();
        var valueEcheance = tableEchelonnement.find(idEcheance).val();
        var valueTitreManuel = tableEchelonnement.find(idTitreManuel).val();

        var montant = parseFloat(valueMontant);

        if (montant <= 0) {
            alertify.alert('Il y a au moins une note intercalaire qui contient un montant invalide (inférieur ou égal à zéro)');
            return;
        }

        totalNotesIntercalaires += montant;

        var echelonnement = new Object();

        if (i == 0) {
            var totalNote = parseFloat(valueMontant);
            echelonnement.amountNpIntercalaire = totalNote;
        } else {
            echelonnement.amountNpIntercalaire = valueMontant;
        }
        echelonnement.echeanceNpIntercalaire = valueEcheance;
        echelonnement.numeroNpIntercalaire = valueTitreManuel;
        tempListEchelonnement.push(echelonnement);
    }

    if (totalNotesIntercalaires == total) {

        alertify.confirm('Etes-vous sûre de vouloir valider l\'échelonnement du titre de perception ' + selectedTitreForDisplay + ' ?', function () {
            validerDemandeEchelonnement(tempListEchelonnement);
        });

    } else {
        alertify.alert('La somme des tranches ne correspond pas au montant de la titre initial');
    }

}

function getDocumentsToUpload() {

    var nombre = 0;

    if (isDecisionUpload == 1) {

        selectedEchelonnementDecisonArchive = getUploadedData();

        nombre = JSON.parse(selectedEchelonnementDecisonArchive).length;

        switch (nombre) {
            case 0:
                lblNbDocumentDecision.text('');
                break;
            case 1:
                lblNbDocumentDecision.html('1 document');
                break;
            default:
                lblNbDocumentDecision.html(nombre + ' documents');
        }
    }

    if (isDecisonProroger == 1) {

        var selectedEchelonnementProrogationArchive = getUploadedData();

        nombre = JSON.parse(selectedEchelonnementProrogationArchive).length;

        if (nombre == 0) {

            alertify.alert('Veuillez attacher la décision et d\'autres pièces pour proroger ');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir proroger l\'échéance des titres concernés ?', function () {

            validateProrogationEcheancePaiement(selectedEchelonnementProrogationArchive);

        }, function () {
        });
    }



}

function validerDemandeEchelonnement(intercalaireList) {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': '1202',
            'demandeEchelonnementCode': selectedDemandeEchelonnementCode,
            'numeroTitre': selectedTitre,
            'numeroTitreManuel': selectedTitreForDisplay,
            'typeTitre': selectedTitreType,
            'SiteCode': userData.SiteCode,
            'intercalaireList': intercalaireList != empty ? JSON.stringify(intercalaireList) : empty,
            'userId': userData.idUser,
            'etat': selectedDecision,
            'observation': txtObservalitionValidation.val(),
            'montantInteret': titrePrincipalAmount * INTERET_ECHELONNEMENT,
            'archives': selectedEchelonnementDecisonArchive,
            'fkDocument': codeTypeDocumentSelected,
            'observationDocument': textObservDoc.val()
        },
        beforeSend: function () {

            modalTraitementEchelonnement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation en cours ...</h5>'});

        },
        success: function (response)
        {

            modalTraitementEchelonnement.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                if (response == '0') {

                    alertify.alert('Echec lors de la validation de la demande d\'échelonnement du titre ' + selectedTitreForDisplay);

                } else if (response == '1') {

                    alertify.alert('La demande d\'échelonnement du titre ' + selectedTitreForDisplay + ' a été validé');

                    if (typeSearch == '1') {
                        getRegistreDemandeEchelonnement('1');
                    } else {
                        getRegistreDemandeEchelonnement('2');
                    }

                    modalTraitementEchelonnement.modal('hide');
                }
            }
            , 1);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalTraitementEchelonnement.unblock();
            showResponseError();
        }
    });
}

function showProrogerEcheanceModal(numTitre, titreMere, echeance) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    selectedEcheanceToProroge = echeance.replace('_', '/');
    selectedEcheanceToProroge = selectedEcheanceToProroge.replace('_', '/');

    var day = parseInt(selectedEcheanceToProroge.split("/")[0]);
    var month = parseInt(selectedEcheanceToProroge.split("/")[1]);
    var annee = parseInt(selectedEcheanceToProroge.split("/")[2]);

    selectedEcheanceToProroge = month + '/' + day + '/' + annee;

    var constituateDate = month + '/' + (day + 1) + '/' + annee;

    document.getElementById("inputDateEcheanceProrogation").valueAsDate = new Date(constituateDate);

    selectedTitreNumeroToProroge = numTitre;
    selectedTitreMereNumeroToProroge = titreMere;

    modalProrogationEcheance.modal('show');

}

function validateRevocation(numTitreMere, typeTitre, codeDemandeEchelonnement) {

    alertify.confirm('Etes-vous sûre de vouloir révoquer cette demande ?', function () {

        $.ajax({
            type: 'POST',
            url: 'fractionnement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'numeroTitre': numTitreMere,
                'userId': userData.idUser,
                'typeTitre': 'NP',
                'SiteCode': userData.SiteCode,
                'demandeEchelonnementCode': codeDemandeEchelonnement,
                'dateLimiteEcheance': dateLimiteEcheanceRevocationByIntercalaire,
                'operation': '1206'
            },
            beforeSend: function () {
                modalDetailEchelonnement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Révocation en cours ...</h5>'});
            },
            success: function (response)
            {
                modalDetailEchelonnement.unblock();

                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    showResponseError();
                    return;
                }
                if (response == '1') {

                    modalDetailEchelonnement.modal('hide');

                    alertify.alert('La révocation est éffectuée avec succès');

                    setTimeout(function () {

                        if (typeSearch == '1') {
                            getRegistreDemandeEchelonnement('1');
                        } else {
                            getRegistreDemandeEchelonnement('2');
                        }
                    },
                            1500);
                }

            },
            complete: function () {

            },
            error: function (xhr) {
                modalDetailEchelonnement.unblock();
                showResponseError();
            }

        });
    });
}

function validateProrogation(numTitreMere, TypeTitre) {

    if (listNpFilleProrogation.length == 0) {

        alertify.alert('Aucune date n\'a été modifié pour la prorogation');
        return;
    }

    listNpFilleProrogationValidate = [];

    for (var i = 0; i < listNpFilleProrogation.length; i++) {

        if (numTitreMere == listNpFilleProrogation[i].TitreMerenumero.trim()) {

            listNpFilleProrogationValidate.push(listNpFilleProrogation[i]);
        }
    }

    selectedTypeTitreTitreToProroge = TypeTitre;
    isDecisonProroger = 1;
    initUpload(codeDoc, codeTypeDoc);
}

function getEcheanceProrogerByTitre(numTitre) {

    var selectedEcheance = '-';

    for (var i = 0; i < listNpFilleProrogation.length; i++) {

        if (numTitre == listNpFilleProrogation[i].numeroIntercalaire.trim()) {

            selectedEcheance = listNpFilleProrogation[i].echeanceNpFilleProrogee;
            break;
        }
    }

    return selectedEcheance;
}

function validateProrogationEcheancePaiement(archiveDecisionProrogation) {

    $.ajax({
        type: 'POST',
        url: 'fractionnement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'listNpFilleProrogation': JSON.stringify(listNpFilleProrogationValidate),
            'demandeEchelonnementCode': selectedDemandeEchelonnementCode,
            'idUser': userData.idUser,
            'typeDocument': selectedTypeTitreTitreToProroge,
            'archives': archiveDecisionProrogation,
            'fkDocument': codeTypeDocumentSelected,
            'observation': textObservDoc.val(),
            'operation': '1207'
        },
        beforeSend: function () {
            modalDetailEchelonnement.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Prorogation des échéances en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailEchelonnement.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                showResponseError();
                return;
            }
            if (response == '1') {

                modalDetailEchelonnement.modal('hide');

                alertify.alert('La prorogation des échéances de paiement est enregistrée avec succès');

                setTimeout(function () {

                    listNpFilleProrogation = [];
                    listNpFilleProrogationValidate = [];

                    if (typeSearch == '1') {
                        getRegistreDemandeEchelonnement('1');
                    } else {
                        getRegistreDemandeEchelonnement('2');
                    }
                },
                        1500);


            }

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailEchelonnement.unblock();
            showResponseError();
        }

    });

}

function getSelectedAssujetiData() {

    ResearchValue.val(selectAssujettiData.nomComplet);
    selectedAssujettiCode = selectAssujettiData.code;
    getRegistreDemandeEchelonnement('1');

}

function getTypeDocumentName(type) {
    var nameDocument = '';
    switch (type) {
        case 'NP':
            nameDocument = 'Note de perception';
            break;
        case 'BP':
            nameDocument = 'Bon à payer';
            break;
        case 'AMR1':
            nameDocument = 'Avis de mise en recouvrement 1';
            break;
        default:
            nameDocument = 'Avis de mise en recouvrement 2';
    }
    return nameDocument;
}
