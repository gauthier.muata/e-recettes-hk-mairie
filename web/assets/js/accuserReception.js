/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAccuserReception,
        btnJoindreArchiveDocument;


var inputDateReception;

var idDocument, typeDocument;
var codeTypeDocumentSelected;
var textObservDoc;
var codeTypeDoc = '';
var codeDoc = undefined;

var modalAccuserReception;

var lblNbDocumentArchive;

var archivesAccuser = '';

var archiveList;

$(function () {
    inputDateReception = $('#inputDateReception');

    btnAccuserReception = $('#btnAccuserReception');
    btnJoindreArchiveDocument = $('#btnJoindreArchiveDocument');

    modalAccuserReception = $('#modalAccuserReception');
    textObservDoc = $('#textObservDoc');

    lblNbDocumentArchive = $('#lblNbDocumentArchive');
    lblNbDocumentArchive.text('');

    btnAccuserReception.on('click', function (e) {

        if (inputDateReception.val() == empty) {
            alertify.alert('Veuillez fournir une date.');
            return;
        }

        var today = new Date();
        var selectDate = new Date(inputDateReception.val());

        var today_fullDate = today.getDate() + '' + today.getMonth() + '' + today.getFullYear();
        var selectDate_fullDate = selectDate.getDate() + '' + selectDate.getMonth() + '' + selectDate.getFullYear();

        if (parseInt(selectDate_fullDate) < parseInt(today_fullDate)) {
            alertify.alert('La date sélectionnée est inférieure à la date du jour');
            return;
        }

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir enregistrer la date de l\'accusé de reception ?', function () {
            accuserRedception(inputDateReception.val());
            lblNbDocumentArchive.text('');
        });

    });

    btnJoindreArchiveDocument.on('click', function (e) {

        if (archivesAccuser == '') {
            initUpload(codeDoc, codeTypeDoc);
        } else {
            initUpload(archivesAccuser, codeTypeDoc);
        }

    });
});

function initAccuseReceptionUI(id, type) {
    idDocument = id;
    typeDocument = type;
    checkTypeDocument(type);

    switch (type) {
        case 'NP':
            modalAccuserReception.modal('show');
            break;

    }

}

function checkTypeDocument(typeDoc) {
    switch (typeDoc) {
        case  'AMR':
            codeTypeDoc = 'AMR1';
            break;
        case 'NP':
            codeTypeDoc = 'NP';
            break;
    }
}

function accuserRedception(dateEcheance) {

    $.ajax({
        type: 'POST',
        url: 'general_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'idDocument': idDocument,
            'dateAccuserReception': dateEcheance,
            'idUser': userData.idUser,
            'typeDocument': typeDocument,
            'archives': archivesAccuser,
            'fkDocument': codeTypeDocumentSelected,
            'observation': textObservDoc.val(),
            'operation': 'updateDateEcheance'
        },
        beforeSend: function () {
            modalAccuserReception.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Accuser de réception en cours ...</h5>'});
        },
        success: function (response)
        {
            modalAccuserReception.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                showResponseError();
                return;
            }
            if (response == '1') {
                alertify.alert('La date de l\'accusé de reception est enregistrée avec succès');
                modalAccuserReception.modal('hide');
                refrechDataAfterAccuserReception();
                archivesAccuser = '';
                inputDateReception.val(empty);
                return;
            }
            setTimeout(function () {
                modalAccuserReception.unblock();
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalNotePerceptionChild.unblock();
            showResponseError();
        }

    });

}
