/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var divisionList = [], abList = [];
var dateDebut,
        dateFin,
        datePick1,
        datePick2;

$(function () {
    
    dateDebut = $("#dateDebut"),
        dateFin = $("#dateFin");

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    $(".datePicker1").datepicker("setDate", new Date());
    $(".datePicker2").datepicker("setDate", new Date());

    $('.cmbDiviision').click(function (e) {
        e.preventDefault();
        setDataBureau($('.cmbDiviision').val());
    });

    setDataDivision();

    getArticleBudgetaireImpot();

    $('.cmbAnnee').html(getYears());
    $('.cmbMois').html(getAllMounths());

    setMonthVisibility();

    $('.cmbAB').click(function (e) {
        e.preventDefault();
        setMonthVisibility($('.cmbAB').val());
    });
});

function setDataDivision() {
    var data = JSON.parse(userData.utilisateurDivisionList);

    var option = '';
    if (data.length > 1) {
        option = '<option value="">Sélectionner une division</option>';
    }
    $.each(data, function (index, item, array) {
        if (!checkDivisionExists(item.codeDivision)) {
            option += '<option value="' + item.codeDivision + '">' + item.intituleDivision + '</option>';
            var division = {};
            division.code = item.codeDivision;
            divisionList.push(division);
        }
    });
    $('.cmbDiviision').append(option);
    $('.cmbBureau').html('<option value="">Sélectionner un bureau</option>');
    if (data.length == 1) {
        setDataBureau($('.cmbDiviision').val());
    }
}

function setDataBureau(codeDivision) {
    var data = JSON.parse(userData.utilisateurDivisionList);
    var option = '';
    if (data.length > 1) {
        option = '<option value="">Sélectionner un bureau</option>';
    }
    $.each(data, function (index, item, array) {
        if (item.codeDivision == codeDivision) {
            option += '<option value="' + item.codeBureau + '">' + item.intituleBureau + '</option>';
        }
    });
    $('.cmbBureau').html(option);
}

function getArticleBudgetaireImpot() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getArticleBudgetaireImpot',
            'codes': JSON.stringify(AB_IMPOT)
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                abList = response;
                var option = '<option value="" selected disabled>S&eacute;lectionner un article</optio>';
                $.each(response, function (index, item, array) {
                    option += '<option value="' + item.code + '">' + item.intitule + '</optio>';
                });
                $('.cmbAB').html(option);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getAllMounths() {
    var months = [
        'Janvier', 'Fevrier',
        'Mars', 'Avril', 'Mai',
        'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'
    ];
    var option = '<option value="" selected disabled>S&eacute;lectionner le mois</option>';
    var mois = '';
    $.each(months, function (index, item, array) {
        mois = index + 1;
        mois >= 10 ? mois : mois = '0' + mois;
        option += '<option value="' + mois + '">' + item + '</option>';
    });
    return option;
}

function getYears() {
    var year = [
        '2021', '2020',
        '2019', '2018', '2017',
        '2016'
    ];
    var option = '<option value="" selected disabled>S&eacute;lectionner l\'ann&eacute;e</option>';
    $.each(year, function (index, item, array) {
        option += '<option value="' + item + '">' + item + '</option>';
    });
    return option;
}

function checkDivisionExists(code) {
    for (var i = 0; i < divisionList.length; i++) {

        if (divisionList[i].code == code) {
            return true;
        }
    }
    return false;
}

function setMonthVisibility(ab) {
    abCode = ab;
//    $('.divAnnee').attr('class','col-lg-6 col-md-6');
    $.each(abList, function (index, item, array) {
        if (ab == item.code) {
            if (item.codePeriodicite == PERIODICITE_MENSUELLE) {
                isMensuel = true;
                $('.divAnnee').attr('class','col-lg-6 col-md-6');
                $('.month').removeClass('hidden');
            } else if (item.codePeriodicite == PERIODICITE_ANNUELLE) {
                isMensuel = false;
                $('.divAnnee').attr('class','col-lg-12 col-md-12');
                $('.month').addClass('hidden');
                
            }
        }
    });
    
    
}