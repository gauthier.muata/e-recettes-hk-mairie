/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalAdressesPersonne;

var tableAdressesPersonne;

var adressePersonneTitle;

var codePersonne, nomPersonne, selectedAdressePersonne;

$(function () {

    adressePersonneTitle = $('#adressePersonneTitle');
    modalAdressesPersonne = $('#modalAdressesPersonne');
    tableAdressesPersonne = $('#tableAdressesPersonne');

});

function loadAdressesOfPersonne() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAdressesPersonne',
            'codePersonne': codePersonne
        },
        beforeSend: function () {

            modalAdressesPersonne.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                modalAdressesPersonne.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAdressesPersonne.unblock();

                if (nomPersonne == '') {
                    adressePersonneTitle.text('ADRESSE(S) DU CONTRIBUABLE');
                } else {
                    //var nameAssujetti = '<span style="font-weight:bold">' + nomPersonne.toUpperCase() + '</span>';
                    adressePersonneTitle.text('ADRESSE(S) DU CONTRIBUABLE : ' + nomPersonne.toUpperCase());
                }

                var adressePersonneList = $.parseJSON(JSON.stringify(response));

                printAdressesPersonne(adressePersonneList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalAdressesPersonne.unblock();
            showResponseError();
        }

    });
}

function printAdressesPersonne(adressePersonneList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> LE(S) ADRESSE(S) DU CONTRIBUABLE </th>';
    header += '<th scope="col" style="width :5%"></th>';
    header += '<th hidden="true" scope="col"> Code AP </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyAdressesPersonne">';

    for (var i = 0; i < adressePersonneList.length; i++) {
        body += '<tr>';
        body += '<td>' + adressePersonneList[i].chaine.toUpperCase() + '</td>';
        body += '<td><a onclick="getAdressePersonne2(\'' + adressePersonneList[i].codeAP + '\',\'' + adressePersonneList[i].chaine + '\')" class="btn btn-success"><i class="fa fa-check-circle"></i></a></td>';
        body += '<td hidden="true">' + adressePersonneList[i].codeAP + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableAdressesPersonne.html(tableContent);

    tableAdressesPersonne.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par adresse ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function getAdressePersonne2(codeAP, chaine) {

    alertify.confirm('Etes-vous sûre de vouloir sélectionner cette adresse ?  <hr/>' + '<span style="font-weight:bold">' + chaine + '</span>', function () {

        selectedAdressePersonne = new Object();
        selectedAdressePersonne.codeAP = codeAP;
        selectedAdressePersonne.chaine = chaine;

        getSelectedAdressePersonneData();

        modalAdressesPersonne.modal('hide');

        codePersonne = '';
        selectedAdressePersonne = null;
        printAdressesPersonne('');

    });
}

