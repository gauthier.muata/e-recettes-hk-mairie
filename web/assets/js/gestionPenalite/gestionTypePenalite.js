/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAddTypePenalite;
var modalUpdateTypePenalite;
var tableTypePenalite;

var typePenaliteList = [];

var typePenaliteName, btnDeleteTypePenalite, btnSaveTypePenalite;

var codeTypePenalite;

var modalPenalite, idTypePenaliteName, tablePenalite;


$(function () {

    mainNavigationLabel.text('GESTION DE PENALITES');
    secondNavigationLabel.text('Edition d\'un type pénalité');
    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    codeTypePenalite = empty;

    btnAddTypePenalite = $('#btnAddTypePenalite');
    modalUpdateTypePenalite = $('#modalUpdateTypePenalite');
    tableTypePenalite = $('#tableTypePenalite');

    typePenaliteName = $('#typePenaliteName');
    btnDeleteTypePenalite = $('#btnDeleteTypePenalite');
    btnSaveTypePenalite = $('#btnSaveTypePenalite');

    modalPenalite = $('#modalPenalite');
    idTypePenaliteName = $('#idTypePenaliteName');
    tablePenalite = $('#tablePenalite');

    btnAddTypePenalite.on('click', function (e) {

        e.preventDefault();

        codeTypePenalite = empty;
        typePenaliteName.val(empty);


        btnDeleteTypePenalite.attr('style', 'display:none');
        modalUpdateTypePenalite.modal('show');

    });

    btnSaveTypePenalite.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnDeleteTypePenalite.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteTypePenalite();

    });

    loadTypePenalites();
});

function deleteTypePenalite() {

    alertify.confirm('Etes-vous sûr de vouloir supprimer le type pénalité : ' + typePenaliteName.val() + ' ?', function () {
        $.ajax({
            type: 'POST',
            url: 'gestionPenaliteBackendServlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'code': codeTypePenalite,
                'userId': userData.idUser,
                'operation': 'deleteTypePenalite'
            },
            beforeSend: function () {
                modalUpdateTypePenalite.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression type pénalité en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateTypePenalite.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression du type pénalité s\'est effectuée avec succès');
                        resetFields();
                        modalUpdateTypePenalite.modal('hide');
                        loadTypePenalites();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalUpdateTypePenalite.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    codeBank = empty;
    typePenaliteName.val(empty);

}

function checkFields() {

    if (typePenaliteName.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir l\'intitulé du type pénalité');
        typePenaliteName.focus()
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir enregistrer ce type pénalité ?', function () {
        saveTypePenalite();
    });
}

function saveTypePenalite() {

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'code': codeTypePenalite,
            'intitule': typePenaliteName.val(),
            'userId': userData.idUser,
            'operation': 'saveTypePenalite'
        },
        beforeSend: function () {
            modalUpdateTypePenalite.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement type pénalité en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalUpdateTypePenalite.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement du type pénalité s\'est effectué avec succès');
                    resetFields();
                    modalUpdateTypePenalite.modal('hide');
                    loadTypePenalites();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateTypePenalite.unblock();
            showResponseError();
        }
    });
}

function loadTypePenalites() {

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadTypePenalite'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des types pénalités en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    typePenaliteList = JSON.parse(JSON.stringify(response));
                    printTableTypePenalite(typePenaliteList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableTypePenalite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:60%">TYPE PENALITE</th>';
    tableContent += '<th style="text-align:center;width:15%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditTypePenalite = '<button class="btn btn-success" onclick="editTypePenalite(\'' + result[i].typePenaliteCode + '\')"><i class="fa fa-edit"></i></button>';
        var btnDisplayPenalite = '&nbsp;<button class="btn btn-warning" onclick="displayPenalite(\'' + result[i].typePenaliteCode + '\',\'' + result[i].typePenaliteName + '\',\'' + result[i].penalisteExist + '\')"><i class="fa fa-list"></i></button>';

        var btnAction = btnEditTypePenalite + btnDisplayPenalite;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:60%">' + result[i].typePenaliteName + '</td>';
        tableContent += '<td style="text-align:center;width:15%">' + btnAction + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableTypePenalite.html(tableContent);

    tableTypePenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par intitulé _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function editTypePenalite(code) {

    for (var i = 0; i < typePenaliteList.length; i++) {

        if (typePenaliteList[i].typePenaliteCode == code) {

            codeTypePenalite = code;

            typePenaliteName.val(typePenaliteList[i].typePenaliteName);
            btnDeleteTypePenalite.attr('style', 'display:inline');
            modalUpdateTypePenalite.modal('show');

            break;
        }
    }

}

function displayPenalite(code, name, exist) {

    if (exist == '1') {

        for (var i = 0; i < typePenaliteList.length; i++) {

            if (typePenaliteList[i].typePenaliteCode == code) {

                var penalisteList = JSON.parse(typePenaliteList[i].penalisteList);
                printTablePenalite(penalisteList, name);

                break;
            }
        }

    } else {

        alertify.alert('Ce type pénalité : ' + name.toUpperCase() + ', n\'a pas des pénalités associées');
    }


}

function printTablePenalite(result, typePenaliteName) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">PENALITE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    idTypePenaliteName.html(typePenaliteName.toUpperCase());

    for (var i = 0; i < result.length; i++) {


        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:100%;vertical-align:middle">' + result[i].penaliteName + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tablePenalite.html(tableContent);
    tablePenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

    modalPenalite.modal('show');
}