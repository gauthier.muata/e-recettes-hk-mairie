/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var inputPenalite, selectTypePenalite, btnSavePenalite, btnInitFields, checkBoxVisibleUser,
        checkBoxActivatePenalite,
        checkBoxVerifyTaux, checkBoxApplyNumberMonth, inputResearchPenalite,
        btnResearchPenalite, tablePenalite;

var penaliteList;
var penaliteSelected;

$(function () {

    mainNavigationLabel.text('GESTION DE PENALITES');
    secondNavigationLabel.text('Edition des pénalités');


    inputPenalite = $('#inputPenalite');
    selectTypePenalite = $('#selectTypePenalite');
    btnSavePenalite = $('#btnSavePenalite');
    btnInitFields = $('#btnInitFields');
    checkBoxVisibleUser = $('#checkBoxVisibleUser');
    checkBoxActivatePenalite = $('#checkBoxActivatePenalite');

    checkBoxVerifyTaux = $('#checkBoxVerifyTaux');
    checkBoxApplyNumberMonth = $('#checkBoxApplyNumberMonth');
    inputResearchPenalite = $('#inputResearchPenalite');
    btnResearchPenalite = $('#btnResearchPenalite');
    tablePenalite = $('#tablePenalite');

    inputResearchPenalite.val(empty);

    btnResearchPenalite.click(function (e) {

        e.preventDefault();

        if (inputResearchPenalite.val() === empty) {

            alertify.alert('Veuillez d\'abord saisir la pénalité à rechercher');
            return;

        } else {
            loadPenalites();
        }
    });

    btnInitFields.click(function (e) {
        e.preventDefault();
        resetFields();
    });

    btnSavePenalite.click(function (e) {
        e.preventDefault();
        checkFields();
    });

    loadTypePenalite();
    loadPenalites();

});

function loadTypePenalite() {

    typePenaliteList = JSON.parse(userData.typePenaliteList);

    var dataTypePenalite = '';

    dataTypePenalite += '<option value="">--</option>';

    for (var i = 0; i < typePenaliteList.length; i++) {
        dataTypePenalite += '<option value="' + typePenaliteList[i].typePenaliteCode + '">' + typePenaliteList[i].typePenaliteName + '</option>';
    }

    selectTypePenalite.html(dataTypePenalite);
}

function loadPenalites() {

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadPenalites',
            'valueSearch': inputResearchPenalite.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {

                printPenalite(empty);
                showResponseError();
                return;

            } else if (response == '0') {

                printPenalite(empty);
                alertify.alert('Aucune pénalite trouver !');
                return;

            } else {

                penaliteList = JSON.parse(JSON.stringify(response));
                printPenalite(penaliteList);

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            printPenalite(empty);
            showResponseError();
        }
    });

}

function printPenalite(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">PENALITE</th>';
    tableContent += '<th style="text-align:left">TYPE PENALITE</th>';
    tableContent += '<th style="text-align:center">VISIBILITE</th>';
    tableContent += '<th style="text-align:center">PALIER</th>';
    tableContent += '<th style="text-align:center">MOIS DE RETARD</th>';
    tableContent += '<th style="text-align:center">ETAT</th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {


        var valueCheckVisibility = 'checked';
        var valueCheckState = 'checked';
        var valueCheckPalier = 'checked';
        var valueCheckApplyMonth = 'checked';

        if (result[i].palier === false) {
            valueCheckPalier = empty;
        }

        if (result[i].visibility === false) {
            valueCheckVisibility = empty;
        }

        if (result[i].applyMonth === false) {
            valueCheckApplyMonth = empty;
        }

        if (result[i].state === 0) {
            valueCheckState = empty;
        }

        var btnDisplayPenalite = '<button class="btn btn-warning" onclick="displayPenalite(\'' + result[i].penaliteCode + '\')"><i class="fa fa-list"></i></button>';

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].penaliteName + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].penaliteTypeName + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckVisibility + '></td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckPalier + '></td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckApplyMonth + '></td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" disabled ' + valueCheckState + '></td>';
        tableContent += '<td style="text-align:center;width:5%";vertical-align:middle">' + btnDisplayPenalite + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tablePenalite.html(tableContent);
    tablePenalite.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function displayPenalite(code) {

    for (var i = 0; i < penaliteList.length; i++) {

        if (penaliteList[i].penaliteCode === code) {

            penaliteSelected = code;

            inputPenalite.val(penaliteList[i].penaliteName);
            selectTypePenalite.val(penaliteList[i].penaliteTypeCode);

            if (penaliteList[i].visibility === true) {
                document.getElementById('checkBoxVisibleUser').checked = true;
            } else {
                document.getElementById('checkBoxVisibleUser').checked = false;
            }

            if (penaliteList[i].applyMonth === true) {
                document.getElementById('checkBoxApplyNumberMonth').checked = true;
            } else {
                document.getElementById('checkBoxApplyNumberMonth').checked = false;
            }

            if (penaliteList[i].palier === true) {
                document.getElementById('checkBoxVerifyTaux').checked = true;
            } else {
                document.getElementById('checkBoxVerifyTaux').checked = false;
            }

            if (penaliteList[i].state === 1) {
                document.getElementById('checkBoxActivatePenalite').checked = true;
            } else {
                document.getElementById('checkBoxActivatePenalite').checked = false;
            }
        }
    }
}

function resetFields() {

    penaliteSelected = empty;

    inputPenalite.val(empty);
    selectTypePenalite.val(empty);

    document.getElementById('checkBoxVisibleUser').checked = false;
    document.getElementById('checkBoxApplyNumberMonth').checked = false;
    document.getElementById('checkBoxVerifyTaux').checked = false;
    document.getElementById('checkBoxActivatePenalite').checked = false;
}

function checkFields() {

    if (inputPenalite.val() == empty) {

        alertify.alert('Veuillez d\'abord saisir l\'intitulé de la pénalité');
        return;

    }

    if (selectTypePenalite.val() == empty) {

        alertify.alert('Veuillez d\'abord sélectionner le type de la pénalité');
        return;
    }

    if (penaliteSelected === empty) {

        alertify.confirm('Etes-vous sûre de vouloir enregistrer cette pénalité ?', function () {
            savePenalite();
        });

    } else {

        alertify.confirm('Etes-vous sûre de vouloir modifier cette pénalité ?', function () {
            savePenalite();
        });
    }

}

function savePenalite() {

    var valueChkVisibility = false;
    var valueChkActivate = false;
    var valueChkPalier = false;
    var valueChkApplyMonth = false;

    if (checkBoxVisibleUser.is(":checked")) {
        valueChkVisibility = true;
    }

    if (checkBoxActivatePenalite.is(":checked")) {
        valueChkActivate = true;
    }

    if (checkBoxVerifyTaux.is(":checked")) {
        valueChkPalier = true;
    }

    if (checkBoxApplyNumberMonth.is(":checked")) {
        valueChkApplyMonth = true;
    }

    $.ajax({
        type: 'POST',
        url: 'gestionPenaliteBackendServlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'savePenalite',
            'code': penaliteSelected,
            'inputPenalite': inputPenalite.val(),
            'selectTypePenalite': selectTypePenalite.val(),
            'valueChkVisibility': valueChkVisibility,
            'valueChkActivate': valueChkActivate,
            'valueChkPalier': valueChkPalier,
            'valueChkApplyMonth': valueChkApplyMonth,
            'userId': userData.idUser
        },
        beforeSend: function () {

            if (penaliteSelected === empty) {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
            } else {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Modification en cours ...</h5>'});
            }
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;

            } else {

                if (penaliteSelected === empty) {

                    alertify.alert('L\'enregistrement de la pénalité : ' + inputPenalite.val() + ', s\'est effectué avec succès');

                } else {
                    alertify.alert('La modification de la pénalité : ' + inputPenalite.val() + ', s\'est effectuée avec succès');
                }

                loadPenalites();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

