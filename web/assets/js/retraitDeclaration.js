/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResponsible = '';

var
        lblLegalForm,
        lblNameResponsible,
        lblAddress;

var cmbArticleBudgetaire;
var cmbDevise;
var tableBiensAB;

var dataAB = [], dataAssujettissement = [];

var dataRetrait = [];

var defaultDevise = 'CDF';
var totalSum = 0;
var valuePeriode;
var abSelected;

var btnEnregistrer, inputNumeroDeclaration, inputRequerant;

var cmdPeriodeDeclarationID, valuecmdPeriodeDeclaration;
var amountPenalite;
var dataPeriode;
var nbreMois, estPenalise, periodeName;
var checkBox;
var checkExist;
var assujettissementSelected, deviseSelected;
var penalitieList = [];
var modalFichePriseEnCharge;
var btnCancelPenalite = empty;
var lblTotalGeneral;

var cmbBanque, cmbCompteBancaire;
var modalReviewAmountPenalite, spnAmountPenaliteInit, cmbTauxRemise, spnAmountPenaliteRemise, btnConfirmeRemisePenalite, spnMonthLate, inputObservationRemiseTaux;
var amountRemise;

var retrait = {};

$(function () {

    amountPenalite = 0;
    amountRemise = 0;
    valuecmdPeriodeDeclaration = '0';

    checkExist = false;

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Retrait déclaration');

    removeActiveMenu();
    linkMenuDeclaration.addClass('active');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');

    cmbArticleBudgetaire = $("#cmbArticleBudgetaire");
    tableBiensAB = $('#tableBiensAB');
    cmbDevise = $('#cmbDevise');
    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');

    btnEnregistrer = $('#btnEnregistrer');
    inputNumeroDeclaration = $('#inputNumeroDeclaration');
    inputRequerant = $('#inputRequerant');
    lblTotalGeneral = $('#lblTotalGeneral');

    cmbBanque = $('#cmbBanque');
    cmbCompteBancaire = $('#cmbCompteBancaire');

    modalReviewAmountPenalite = $('#modalReviewAmountPenalite');
    spnAmountPenaliteInit = $('#spnAmountPenaliteInit');
    cmbTauxRemise = $('#cmbTauxRemise');
    spnAmountPenaliteRemise = $('#spnAmountPenaliteRemise');
    btnConfirmeRemisePenalite = $('#btnConfirmeRemisePenalite');
    spnMonthLate = $('#spnMonthLate');
    inputObservationRemiseTaux = $('#inputObservationRemiseTaux');

    btnCallModalSearchResponsable.click(function (e) {
        e.preventDefault();
        assujettiModal.modal('show');
    });

    printBienList('');

    cmbArticleBudgetaire.change(function (e) {

        var AbID = cmbArticleBudgetaire.val();
        printBienList(AbID);

        var lblTotalGeneral = $("#lblTotalGeneral");
        
        if(defaultDevise == ''){
            defaultDevise = cmbDevise.val();
        }
        
        lblTotalGeneral.html(formatNumber(0, defaultDevise));

    });

    cmbTauxRemise.change(function (e) {

        if (cmbTauxRemise.val() == '0') {
            spnAmountPenaliteRemise.html(formatNumber(0, deviseSelected));
            alertify.alert('Veuillez d\'abord sélectionner un taux de remsie valide');
            return;
        } else {
            reviewRatePenalite(cmbTauxRemise.val());
        }
    });


    cmbBanque.change(function (e) {

        e.preventDefault();

        if (cmbBanque.val() !== '0') {
            loadingAccountBankData(cmbBanque.val());
        } else {

            cmbCompteBancaire.val('0');
            cmbCompteBancaire.attr('disabled', true);
            alertify.alert('Veuillez d\'abord sélectionner une banque valide');
            return;
        }

    });

    cmbCompteBancaire.change(function (e) {
        e.preventDefault();

        if (cmbCompteBancaire.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire valide');
            return;
        }

    });

    btnConfirmeRemisePenalite.click(function (e) {

        e.preventDefault();

        if (cmbTauxRemise.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un taux de remsie valide');
            return;
        }

        if (inputObservationRemiseTaux.val() == empty) {
            alertify.alert('Veuillez d\'abord motiver la raison de cette remise');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir appliquer cette remise de pénalité ?', function () {
            applyReviewPenalite();
        });
    });

    btnEnregistrer.click(function (e) {

        e.preventDefault();

        if (inputNumeroDeclaration.val() == '') {

            alertify.alert('Veuillez renseigner le numéro de la déclaration');
            return;
        }

        if (inputRequerant.val() == '') {

            alertify.alert('Veuillez renseigner le nom du requérant de la déclaration');
            return;
        }

        if (cmbBanque.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner une banque avant de poursuivre');
            return;
        }

        if (cmbCompteBancaire.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un compte bancaire avant de poursuivre');
            return;
        }

        if (dataRetrait.length == 0) {

            alertify.alert('Aucun détail sélectionné pour la déclaration');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir confirmer ce retrait de la déclaration ?', function () {

            for (var k = 0; k < dataRetrait.length; k++) {

                dataRetrait[k].devise = defaultDevise;
            }

            saveDeclaration();

        });


    });


    cmbDevise.on('change', function (e) {
        e.preventDefault();
        var lblTotalGeneral = $("#lblTotalGeneral");
        defaultDevise = cmbDevise.val();
        lblTotalGeneral.html(formatNumber(totalSum, defaultDevise));
    });

});

function getSelectedAssujetiData() {

    codeResponsible = selectAssujettiData.code;
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);

    loadABAssujetti();
    loadingBankData();
}

function saveDeclaration() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveRetraitDeclaration',
            'codePersonne': codeResponsible,
            'listRetrait': JSON.stringify(dataRetrait),
            'requerant': inputRequerant.val(),
            'numDeclaration': inputNumeroDeclaration.val(),
            'idUser': userData.idUser,
            'codeAB': cmbArticleBudgetaire.val(),
            'estPenalise': estPenalise,
            'amountPenalite': amountPenalite,
            'codeBanque': cmbBanque.val(),
            'codeCompteBancaire': cmbCompteBancaire.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            }

            if (response == '2') {

                alertify.alert('Le numéro de la déclaration est déjà utilisé');
                return;
            }

            alertify.alert('Le retrait de la déclaration a été enregistré avec succès');

            var documentList = JSON.parse(JSON.stringify(response));

            setDocumentContent(documentList.noteTaxationPrincipal);
            window.open('visualisation-document', '_blank');

            if (estPenalise == '1') {
                setDocumentContent(documentList.noteTaxationPenalite);
                window.open('visualisation-document', '_blank');
            }

            initUI();

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function initUI() {

    dataAB = [];
    dataAssujettissement = [];
    dataRetrait = [];
    inputRequerant.val('');
    inputNumeroDeclaration.val('');
    printBienList('');
    checkExist = false;
    valuePeriode = empty;

    nbreMois = 0;
    periodeName = empty;

    //cmbBanque.va('0');
    //cmbCompteBancaire.va('0');

    penalitieList = [];
    estPenalise == '0';

    var idColPenalite = $("#penalite_" + assujettissementSelected);
    var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

    amountPenalite = 0;

    idColPenalite.html(formatNumber(amountPenalite, deviseSelected));
    idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

    idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

    totalSum = amountPenalite;

    var lblTotalGeneral = $("#lblTotalGeneral");
    lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

    setTimeout(function () {
        loadABAssujetti();
    }, 1500);
}

function loadABAssujetti() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getABAssujetti',
            'codePersonne': codeResponsible
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des données en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            constituateABData(response);
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function generatePeriode(id, data) {

    var periodeData = '<select id="selectPeriode_' + id + '" class="form-control" style="width:100%" onchange="getPeriodeDeclaration(\'' + id + '\')">';

    var defautlValue = '- Sélectionner la période -';
    periodeData += '<option value="0">' + defautlValue + '</option>';

    for (var i = 0; i < data.length; i++) {
        periodeData += '<option value="' + data[i].periodeID + '">' + data[i].periode + '</option>';
    }

    periodeData += '<select>';

    return periodeData;

}

function constituateABData(data) {

    var cbData = '<option value=""> --- </option>';

    dataAB = JSON.parse(data.dataAB);
    dataAssujettissement = JSON.parse(data.dataAssujettisement);

    //alert(JSON.stringify(dataAssujettissement));
    //return;

    for (var i = 0; i < dataAB.length; i++) {

        var ab = dataAB[i];
        cbData += '<option value="' + ab.codeAB + '">' + ab.codeOfficiel.toUpperCase() + ' - ' + ab.intituleAB.toUpperCase() + '</option>';

    }

    cmbArticleBudgetaire.html(cbData);
}

function printBienList(idAb) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:6%"></th>';
    tableContent += '<th style="text-align:left;width:20%">BIEN</th>';
    tableContent += '<th style="text-align:left;width:20%">DESCRIPTION</th>';
    tableContent += '<th style="text-align:left;width:12%">PERIODE DECLARATION</th>';
    tableContent += '<th style="text-align:right;width:12%">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:right;width:12%">PENALITE DÛ</th>';
    tableContent += '<th style="text-align:right;width:10%"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    totalSum = 0;

    for (var i = 0; i < dataAssujettissement.length; i++) {

        var row = dataAssujettissement[i];

        if (row.codeAB != idAb)
            continue;

        var descriptionBien = empty;
        var tauxCumulInfo = '';

        if (row.isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + row.usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + row.communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + row.quartierName + '</span>';

            var complementInfo = row.complement;

            var tauxInfo = '';
            var valeurBaseInfo = '';

            if (row.typeTaux == '%') {

                tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + row.taux + '%' + '</span>';

                if (row.valeurBase > 0) {

                    valeurBaseInfo = '<br/>' + 'BASE DE CALCUL : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.valeurBase, row.devise) + '</span>';

                }

            } else {
                tauxInfo = 'TAUX : ' + '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.taux, row.devise) + '</span>';
            }


            descriptionBien = natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + complementInfo + '<br/>' + tauxInfo + valeurBaseInfo;

            tauxCumulInfo = '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.tauxCumule, row.devise) + '</span>';
            defaultDevise = row.devise;

        } else {

            tauxCumulInfo = '<span style="font-weight:bold;color:green;font-size:18px">' + formatNumber(row.tauxCumule, row.devise) + '</span>';
            defaultDevise = row.devise;

            if (row.type == 1) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
                var categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + row.complement;

            } else if (row.type == 3) {

                var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + row.libelleTypeBien + '</span>';
                var categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + row.tarifName + '</span>';

                descriptionBien = genreInfo + '<br/>' + categorieInfo2 + '<hr/>' + row.complement;

            } else {

                descriptionBien = bienList[i].descriptionBien + '<hr/>' + row.complement;
            }

        }


        dataPeriode = JSON.parse(JSON.stringify(row.listPeriodesDeclarations));

        var periode = dataPeriode.length === 0 ? 'Aucune période disponible' : '<center>' + generatePeriode(row.idAssujettissement, dataPeriode) + '</center>';

        var readOnly = dataPeriode.length === 0 ? 'hidden' : 'checkbox';

        var bienInfo = '<span style="font-weight:bold">' + row.intituleBien + '</span>' + '<hr/>' + row.adresseBien;

        var amountPenaliteFormat = '<span id="spnPenalite_' + row.idAssujettissement + '" style="font-weight:bold;color:red;font-size:18px">' + formatNumber(amountPenalite, row.devise) + '</span>';

        btnCancelPenalite = empty;
        btnCancelPenalite = '<button type="button" class="btn btn-danger " title="Cliquez ici pour revoir le taux de la pénalité" onclick="callModalDisplayTauxPenalite(\'' + row.idAssujettissement + '\')"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Revoir la pénalité</button>';

        tableContent += '<tr id="row_' + row.idAssujettissement + '">';

        tableContent += '<td style="text-align:center;width:6%;vertical-align:middle"><input  type="' + readOnly + '" id="checkbox_' + row.idAssujettissement + '" onclick="Declarer(\'' + row.idAssujettissement + '\',\'' + row.devise + '\',\'' + amountPenalite + '\')" /></th>';
        tableContent += '<td style="text-align:left;width:18%;vertical-align:middle">' + bienInfo + '</td>';
        tableContent += '<td style="text-align:left;width:18%;vertical-align:middle" id="tarif_' + row.idAssujettissement + '">' + descriptionBien + '</td>';
        tableContent += '<td style="text-align:left;width:13%;vertical-align:middle" id="periode_' + row.idAssujettissement + '">' + periode + '</td>';
        tableContent += '<td style="text-align:right;width:11%;vertical-align:middle" id="montant_' + row.idAssujettissement + '">' + tauxCumulInfo + '</td>';
        tableContent += '<td style="text-align:right;width:11%;vertical-align:middle" id="penalite_' + row.idAssujettissement + '">' + amountPenaliteFormat + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle" id="btnCalcelPenalite_' + row.idAssujettissement + '">' + btnCancelPenalite + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="4" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th></tr>';

    tableContent += '</tfoot>';

    tableContent += '</tbody>';
    tableBiensAB.html(tableContent);

    var myDt = tableBiensAB.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée à afficher",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {

            var api = this.api(), data;

            $(api.column(5).footer()).html(
                    formatNumber(totalSum, defaultDevise));
        }
    });
}

function Declarer(idAssujettisement, devise, amountPenalite) {

    var checkBox = document.getElementById("checkbox_" + idAssujettisement);

    var row = $("#row_" + idAssujettisement);
    var lblTotalGeneral = $("#lblTotalGeneral");

    defaultDevise = devise;

    var amount = 0;
    var assuj;

    for (var i = 0; i < dataAssujettissement.length; i++) {

        assuj = dataAssujettissement[i];

        if (assuj.idAssujettissement == idAssujettisement) {

            amount = assuj.tauxCumule;
            abSelected = assuj.codeAB;

            break;
        }

    }

    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9');
        totalSum += amount;
        checkExist = true;

        retrait = {};

        retrait.assuj = idAssujettisement;
        retrait.montant = assuj.tauxCumule;
        retrait.devise = defaultDevise;
        retrait.periode = $("#selectPeriode_" + idAssujettisement).val();
        retrait.base = 0;
        retrait.icmParamId = 0;
        retrait.penalite = 0;
        retrait.estPenalise = 0;
        retrait.tauxRemise = 0;
        retrait.observationRemise = '';
        retrait.remise = 0;

        dataRetrait.push(retrait);

    } else {
        checkExist = false;
        totalSum -= amount;
        amountPenalite -= amountPenalite;

        row.removeAttr('style');

        for (var k = 0; k < dataRetrait.length; k++) {

            if (dataRetrait[k].assuj == idAssujettisement) {

                dataRetrait.splice(k, 1);
                break;
            }
        }

    }

    lblTotalGeneral.html(formatNumber((totalSum + parseFloat(amountPenalite)), defaultDevise));
}

function getPeriodeDeclaration(id) {

    for (var j = 0; j < dataAssujettissement.length; j++) {

        if (dataAssujettissement[j].idAssujettissement == id) {

            var amount = dataAssujettissement[j].tauxCumule;
            var devise = dataAssujettissement[j].devise;
            deviseSelected = devise;

            cmdPeriodeDeclarationID = "#selectPeriode_" + id;
            assujettissementSelected = id;

            valuecmdPeriodeDeclaration = $('#tableBiensAB').find(cmdPeriodeDeclarationID).val();

            if (valuecmdPeriodeDeclaration !== '0') {

                if (checkExist) {

                    if (checkPenalite(valuecmdPeriodeDeclaration) === '1') {


                        var assujList = dataAssujettissement[j].listPeriodesDeclarations;

                        for (var k = 0; k < assujList.length; k++) {

                            if (assujList[k].periodeID == valuecmdPeriodeDeclaration) {

                                var isRecidiviste = assujList[k].estRecidiviste;
                                var monthNumber = assujList[k].moisRetard;

                                penalitieList = [];
                                //listPNSelected = [];

                                valuePeriode = '<span style="font-weight:bold">' + periodeName.toUpperCase() + '</span>';

                                alertify.confirm('Cette période de déclaration : ' + valuePeriode + ' sera frappée de pénalité d\'assiette. <br/><br/> Etes-vous sûr de vouloir continier ?', function () {

                                    callModalApplyPenalite(amount, valuecmdPeriodeDeclaration,
                                            periodeName.toUpperCase(), devise, isRecidiviste, monthNumber);

                                });

                            }
                        }
                    } else {

                        for (var i = 0; i < dataRetrait.length; i++) {

                            if (dataRetrait[i].assuj == assujettissementSelected) {

                                dataRetrait[i].periode = valuecmdPeriodeDeclaration;
                                dataRetrait[i].estPenalise = '0';
                                dataRetrait[i].penalite = 0;
                                dataRetrait[i].remise = 0;
                                dataRetrait[i].observationRemise = empty;
                                dataRetrait[i].tauxRemise = 0;

                                if (abSelected == '00000000000002312021') {

                                    var assujList_ = dataAssujettissement[j].listPeriodesDeclarations;

                                    for (var j = 0; j < assujList_.length; j++) {

                                        if (assujList_[j].periodeID == valuecmdPeriodeDeclaration) {

                                            var idColMontantDu = $("#montant_" + assujettissementSelected);
                                            idColMontantDu.html(formatNumber(assujList_[j].tauxCumule, assujList_[j].devise));
                                            idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                                            retrait.montant = assujList_[j].tauxCumule;
                                            retrait.icmParamId = assujList_[j].icmParamId;

                                            var lblTotalGeneral = $("#lblTotalGeneral");
                                            lblTotalGeneral.html(formatNumber(assujList_[j].tauxCumule, assujList_[j].devise));

                                            break;
                                        }
                                    }

                                }
                            }
                        }

                    }

                } else {

                    alertify.alert('Veuillez d\'abord sélectionner un bien immobilier avant de poursuivre');
                    valuecmdPeriodeDeclaration = '0'
                    cmdPeriodeDeclarationID.val('0');
                }


            } else {

                var idColMontantDu = $("#montant_" + assujettissementSelected);
                idColMontantDu.html(formatNumber(0, defaultDevise));
                idColMontantDu.attr('style', 'font-weight:bold;color:green;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                var lblTotalGeneral = $("#lblTotalGeneral");
                lblTotalGeneral.html(formatNumber(0, defaultDevise));

                var idColPenalite = $("#penalite_" + assujettissementSelected);
                idColPenalite.html(formatNumber(0, defaultDevise));
                idColMontantDu.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

                alertify.alert('Veuillez d\'abord sélectionner une période de déclaration valide avant de poursuivre');

            }

            break;
        }
    }
}

function checkPenalite(periodeSelected) {

    for (var i = 0; i < dataPeriode.length; i++) {

        if (dataPeriode[i].periodeID == periodeSelected) {
            nbreMois = dataPeriode[i].moisRetard;
            estPenalise = dataPeriode[i].estPenalise;
            periodeName = dataPeriode[i].periode;
            return estPenalise;
        }
    }
}

function callModalDisplayTauxPenalite(id) {


    if (checkExist) {

        if (valuecmdPeriodeDeclaration !== '0') {

            if (estPenalise === '1') {

                for (var j = 0; j < dataAssujettissement.length; j++) {

                    if (dataAssujettissement[j].idAssujettissement == id) {
                        assujettissementSelected = id;

                        cmdPeriodeDeclarationID = "#selectPeriode_" + id;
                        valuecmdPeriodeDeclaration = $('#tableBiensAB').find(cmdPeriodeDeclarationID).val();

                        spnAmountPenaliteInit.html(formatNumber(amountPenalite, deviseSelected));
                        cmbTauxRemise.val('0');
                        spnMonthLate.html(nbreMois);
                        inputObservationRemiseTaux.val(empty);
                        inputObservationRemiseTaux.attr('placeholder', 'Veuillez motiver la raison de la remise ici');
                        spnAmountPenaliteRemise.html(formatNumber(0, deviseSelected));

                        modalReviewAmountPenalite.modal('show');

                    }
                }
            } else {
                //btnCancelPenalite = empty;
                alertify.alert('Impossible de revoir la pénalité, car cette période de déclaration : ' + valuePeriode + ' n\'est pas pénalisée');
            }

        } else {
            alertify.alert('Veuillez d\'abord sélectionner une période de déclaration valide avant de poursuivre');
        }

    } else {

        alertify.alert('Veuillez d\'abord sélectionner un bien immobilier avant de poursuivre');
        valuecmdPeriodeDeclaration = '0'
        cmdPeriodeDeclarationID.val('0');

    }

}

function callModalApplyPenalite(montant, periodeID, periodeName, devise, isRecidiviste, monthLate) {

    periodeIDSelected = periodeID
    deviseSelected = devise;

    var obj = {};

    obj.amount = parseFloat(montant);
    obj.reference = periodeID;
    obj.libelleReference = periodeName;
    obj.currency = devise;
    obj.isRecidiviste = isRecidiviste == 1 ? true : false;
    obj.monthLate = parseInt(monthLate);
    obj.case = 1;

    initUIPenality(obj);
}

function validateFPC() {

    alertify.confirm('Etes-vous sûre de vouloir valider prendre en compte ces pénalités ?', function () {

        penalitieList = getPenalitiesReference(periodeIDSelected);

        var idColPenalite = $("#penalite_" + assujettissementSelected);
        var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

        amountPenalite = getSumByReference(valuecmdPeriodeDeclaration);

        idColPenalite.html(formatNumber(amountPenalite, deviseSelected));
        idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

        idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

        totalSum += amountPenalite;

        var lblTotalGeneral = $("#lblTotalGeneral");
        lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

        for (var i = 0; i < dataRetrait.length; i++) {

            if (dataRetrait[i].assuj == assujettissementSelected) {

                dataRetrait[i].periode = valuecmdPeriodeDeclaration;
                dataRetrait[i].estPenalise = '1';
                dataRetrait[i].penalite = amountPenalite;
                dataRetrait[i].remise = 0;
                dataRetrait[i].observationRemise = empty;
                dataRetrait[i].tauxRemise = 0;
            }
        }

        modalFichePriseEnCharge.modal('hide');
    });


    return penalitieList;
}

function loadingBankData() {


    var dataBank = empty;
    var defautlValue = '--';
    dataBank += '<option value="0">' + defautlValue + '</option>';

    var banqueUserList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < banqueUserList.length; i++) {

        dataBank += '<option value="' + banqueUserList[i].banqueCode + '" >' + banqueUserList[i].banqueName + '</option>';

    }

    cmbBanque.html(dataBank);
}

function loadingAccountBankData(bankCode) {

    var dataAccountBank = empty;
    var defautlValue = '--';

    dataAccountBank += '<option value="0">' + defautlValue + '</option>';

    var accountBankList = JSON.parse(userData.accountList);

    for (var i = 0; i < accountBankList.length; i++) {
        if (bankCode == accountBankList[i].acountBanqueCode) {
            dataAccountBank += '<option value="' + accountBankList[i].accountCode + '" >' + accountBankList[i].accountName + '</option>';
        }
    }

    cmbCompteBancaire.html(dataAccountBank);
    cmbCompteBancaire.attr('disabled', false);
}

function reviewRatePenalite(taux) {

    var valueRemise = (amountPenalite * parseInt(taux) / 100);
    amountRemise = amountPenalite - valueRemise;
    spnAmountPenaliteRemise.html(formatNumber(amountRemise, deviseSelected));

}

function applyReviewPenalite() {

    for (var i = 0; i < dataRetrait.length; i++) {

        if (dataRetrait[i].assuj == assujettissementSelected) {

            dataRetrait[i].remise = amountRemise;
            dataRetrait[i].tauxRemise = cmbTauxRemise.val();
            dataRetrait[i].observationRemise = inputObservationRemiseTaux.val();


            var idColPenalite = $("#penalite_" + assujettissementSelected);
            var idColCalcelPenalite = $("#btnCalcelPenalite_" + assujettissementSelected);

            idColPenalite.html(formatNumber(amountRemise, deviseSelected));
            idColPenalite.attr('style', 'font-weight:bold;color:red;font-size:18px;text-align:right;width:11%;vertical-align:middle');

            idColCalcelPenalite.attr('style', 'text-align:center;width:11%;vertical-align:middle');

            totalSum -= amountPenalite;
            totalSum += amountRemise;

            var lblTotalGeneral = $("#lblTotalGeneral");
            lblTotalGeneral.html(formatNumber(totalSum, deviseSelected));

            modalReviewAmountPenalite.modal('hide');
        }
    }
}