/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var bonApayerList = [];
var responsibleObject = new Object();

var btnSimpleSearch;
var selectTypeSearch;
var selectTypeSearchID;
var valueTypeSearch;

var modalAccuserReception;

var assujettiModal;
var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var dateStartPickerAmr;
var dateEndPickerAmr;
var btnAdvencedSearchAmr;

var lblService, lblTypeContrainte, lblStateAmr, lblDateDebut, lblDateFin;
var selectServiceID;

var btnShowAdvancedSerachModal;
var tableBonApayer;

var codeBapCurrent;
var commandementDocumentPrint = '';
var bonApayerDocumentPrint = '';

var documentListModal, selectDocument, btnPrintDocument;

var documentHtmlPrint;

var labelType, lblStateAmr, selectStateAmr;
var isAdvance;
var fromModalCall;
var contrainteSelected;
var redevableId;

var lblTypeAmr;

$(function () {

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des bon à payer');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');


    btnSimpleSearch = $('#btnSimpleSearch');
    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');

    modalAccuserReception = $('#modalAccuserReception');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    assujettiModal = $('#assujettiModal');

    selectService = $('#selectService');
    selectTypeAmr = $('#selectTypeAmr');
    selectStateAmr = $('#selectStateAmr');
    inputDateStartAmr = $('#inputDateStartAmr');
    inputdateEndAmr = $('#inputdateEndAmr');

    dateStartPickerAmr = $('#dateStartPickerAmr');
    dateEndPickerAmr = $('#dateEndPickerAmr');
    btnAdvencedSearchAmr = $('#btnAdvencedSearchAmr');

    modalRechercheAvanceeAmr = $('#modalRechercheAvanceeAmr');
    lblTypeAmr = $('#lblTypeAmr');

    isAdvance = $('#isAdvance');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    lblService = $('#lblService');
    lblTypeContrainte = $('#lblTypeContrainte');
    lblStateAmr = $('#lblStateAmr');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    labelType = $('#labelType');
    lblStateAmr = $('#lblStateAmr');

    tableBonApayer = $('#tableBonApayer');

    documentListModal = $('#documentListModal');
    selectDocument = $('#selectDocument');
    btnPrintDocument = $('#btnPrintDocument');

    selectService.on('change', function (e) {
        selectServiceID = selectService.val();
    });

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    dateStartPickerAmr.datepicker("setDate", new Date());
    dateEndPickerAmr.datepicker("setDate", new Date());

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            
            case '0':

                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', '');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;

            case '1':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro du bon à payer');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '3':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de l\'AMR');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
        }


    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeAmr.modal('show');
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {

            case '1':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro du bon à payer');
                    return;
                } else {
                    loadBonApayer(isAdvanced);
                }

                break;
            case '2':
                fromModalCall = '1';
                assujettiModal.modal('show');
                break;
            case '3':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro de l\'AMR');
                    return;
                } else {
                    loadBonApayer(isAdvanced);
                }
                break;
            default:
                loadBonApayer(isAdvanced);
                break;
        }

    });

    btnAdvencedSearchAmr.on('click', function (e) {
        e.preventDefault();

        isAdvanced = '1';
        selectTypeSearch.val('1');
        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', false);
        valueTypeSearch.attr('placeholder', '');
        valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
        loadBonApayer(isAdvanced);
    });


    lblTypeAmr.attr('style', 'display:none');
    selectTypeAmr.attr('style', 'display:none');
    lblTypeContrainte.attr('style', 'display:none');

    lblStateAmr.text('Type :');

    var dataselectStateAmr = '';

    selectStateAmr.html(empty);

    dataselectStateAmr += '<option value="0">Tous</option>';
    dataselectStateAmr += '<option value="1">En attente de validation</option>';
    dataselectStateAmr += '<option value="2">Valider</option>';
    dataselectStateAmr += '<option value="3">Annuler</option>';

    selectStateAmr.html(dataselectStateAmr);

    selectTypeSearchID = selectTypeSearch.val();
    printBonApayer(empty);

});

function loadBonApayer(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            labelType.text($('#selectStateAmr option:selected').text().toUpperCase());
            labelType.attr('title', $('#selectStateAmr option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: block');
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectServiceID,
            'codeType': selectStateAmr.val(),
            'dateDebut': inputDateStartAmr.val(),
            'dateFin': inputdateEndAmr.val(),
            'valueSearch': selectTypeSearchID === '2' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadBonAPayer'
        },
        beforeSend: function () {
            
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '2':
                                printBonApayer(empty);
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas de bon à payer');
                                break;
                            default:
                                printBonApayer(empty);
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'un bon à payer');
                                break;
                        }
                        break;
                    case '1':
                        printBonApayer(empty);
                        alertify.alert('Aucun commandement ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                bonApayerList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }
                printBonApayer(bonApayerList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }
            showResponseError();
        }

    });
}


function getSelectedAssujetiData() {

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    switch (fromModalCall) {

        case '1':

            valueTypeSearch.val(selectAssujettiData.nomComplet);
            valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

            loadBonApayer(isAdvanced);

            break;

        case '2':

            if (selectAssujettiData.code === redevableId) {

                alertify.alert('Déoler, le redevable n\'est doit pas être lui-même tier détenteur.');
            } else {
                saveAtd(selectAssujettiData.code);
            }

            break;
    }


}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadBonApayer(isAdvanced);
}

function printBonApayer(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">ID</th>';
    tableContent += '<th style="text-align:left">DATE CREATION</th>';
    tableContent += '<th style="text-align:left">NTD</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">FAIT GENERATEUR</th>';

    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left">COMPTE BANCAIRE</th>';

    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';

    tableContent += '<th style="text-align:left">EST FRACTIONNE ?</th>';

    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amountDu = formatNumber(result[i].montantDu, result[i].devise);

        var amountDuComposite = amountDu;

        if (result[i].estPayer !== empty) {
            amountDuComposite = '';
            amountDuComposite = amountDu + '<br/><br/>' + result[i].estPayer;
        }

        var buttonPrintBap = '';
        var valueCheck = 'checked';

        if (result[i].estFractionner === '0') {
            valueCheck = '';
        }

        buttonPrintBap = '<button class="btn btn-warning" onclick="accuserReception(\'' + result[i].code + '\',\'' + result[i].dateEcheance + '\')"><i class="fa fa-print"></i></button>';

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].code + '</td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + result[i].articleBudgetaireName + '</td>';

        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountDuComposite + '</td>';
        tableContent += '<td style="text-align:left;width:15%;vertical-align:middle;font-weight:normal">' + result[i].compteBancaire + '</td>';

        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].dateEcheance + '</td>';

        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" id="checkboxBap" disabled ' + valueCheck + '></td>';

        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + buttonPrintBap + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableBonApayer.html(tableContent);
    tableBonApayer.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function accuserReception(bapID, dateEcheance) {

    codeTypeDoc = 'BP';
    codeBapCurrent = bapID;

    var documentBap = '';

    if (dateEcheance === empty) {

        initAccuseReceptionUI(codeBapCurrent, codeTypeDoc);
        modalAccuserReception.modal('show');

    } else {

        for (var i = 0; i < bonApayerList.length; i++) {

            if (bonApayerList[i].code === bapID) {

                documentBap = bonApayerList[i].bapDocument;

                setTimeout(function () {

                    setDocumentContent(documentBap);

                    setTimeout(function () {
                    }, 2000);
                    window.open('visualisation-document', '_blank');
                }
                , 1);

            }
        }
    }
}





