/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var atdList = [];
var responsibleObject = new Object();

var btnSimpleSearch;
var selectTypeSearch;
var selectTypeSearchID;
var valueTypeSearch;

var modalAccuserReception;

var assujettiModal;
var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var dateStartPickerAmr;
var dateEndPickerAmr;
var btnAdvencedSearchAmr;

var lblService, lblTypeContrainte, lblStateAmr, lblDateDebut, lblDateFin;
var selectServiceID;

var btnShowAdvancedSerachModal;
var tableAtd;

var codeCommandementCurrent;
var commandementDocumentPrint = '';
var bonApayerDocumentPrint = '';

var documentListModal, selectDocument, btnPrintDocument;

var documentHtmlPrint;

var labelType, lblStateAmr, selectStateAmr;
var isAdvance;
var fromModalCall;
var contrainteSelected;
var redevableId;

var lblTypeAmr;

$(function () {

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des avis des tiers detenteurs (ATD)');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');


    btnSimpleSearch = $('#btnSimpleSearch');
    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');

    modalAccuserReception = $('#modalAccuserReception');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    assujettiModal = $('#assujettiModal');

    selectService = $('#selectService');
    selectTypeAmr = $('#selectTypeAmr');
    selectStateAmr = $('#selectStateAmr');
    inputDateStartAmr = $('#inputDateStartAmr');
    inputdateEndAmr = $('#inputdateEndAmr');

    dateStartPickerAmr = $('#dateStartPickerAmr');
    dateEndPickerAmr = $('#dateEndPickerAmr');
    btnAdvencedSearchAmr = $('#btnAdvencedSearchAmr');

    modalRechercheAvanceeAmr = $('#modalRechercheAvanceeAmr');
    lblTypeAmr = $('#lblTypeAmr');

    isAdvance = $('#isAdvance');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    lblService = $('#lblService');
    lblTypeContrainte = $('#lblTypeContrainte');
    lblStateAmr = $('#lblStateAmr');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    labelType = $('#labelType');
    lblStateAmr = $('#lblStateAmr');

    tableAtd = $('#tableAtd');

    documentListModal = $('#documentListModal');
    selectDocument = $('#selectDocument');
    btnPrintDocument = $('#btnPrintDocument');

    selectService.on('change', function (e) {
        selectServiceID = selectService.val();
    });

    selectDocument.on('change', function (e) {
        documentHtmlPrint = '';

        switch (selectDocument.val()) {
            case '0':
                alertify.alert('Désoler, veuillez sélectionner un document valide');
                break;
            case '1':
                documentHtmlPrint = commandementDocumentPrint;
                break;
            case '2':
                documentHtmlPrint = bonApayerDocumentPrint;
                break;
        }
    });

    btnPrintDocument.on('click', function (e) {
        e.preventDefault();

        if (selectDocument.val() === '0') {

            alertify.alert('Veuillez sélectionner un document valide avant de pouvoir lancer son impression');
            return;

        } else {

            setTimeout(function () {
                setDocumentContent(documentHtmlPrint);

                setTimeout(function () {
                }, 2000);
                window.open('visualisation-document', '_blank');
            }
            , 1);
        }

    });

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    dateStartPickerAmr.datepicker("setDate", new Date());
    dateEndPickerAmr.datepicker("setDate", new Date());

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':

                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de l\'ATD');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
        }


    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeAmr.modal('show');
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {

            case '1':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro de l\'ATD');
                    return;
                } else {
                    loadAtd(isAdvanced);
                }

                break;
            case '2':
                fromModalCall = '1';
                assujettiModal.modal('show');
                break;

        }

    });

    btnAdvencedSearchAmr.on('click', function (e) {
        e.preventDefault();

        isAdvanced = '1';
        selectTypeSearch.val('1');
        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', false);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de l\'ATD');
        valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
        loadAtd(isAdvanced);
    });


    lblTypeAmr.attr('style', 'display:none');
    selectTypeAmr.attr('style', 'display:none');
    lblTypeContrainte.attr('style', 'display:none');

    lblStateAmr.text('Type :');

    var dataselectStateAmr = '';

    selectStateAmr.html(empty);

    dataselectStateAmr += '<option value="1">Tous</option>';
    dataselectStateAmr += '<option value="2">Echus</option>';
    dataselectStateAmr += '<option value="3">Non Echus</option>';

    selectStateAmr.html(dataselectStateAmr);

    selectTypeSearchID = selectTypeSearch.val();
    printAtd(empty);

});

function loadAtd(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            labelType.text($('#selectStateAmr option:selected').text().toUpperCase());
            labelType.attr('title', $('#selectStateAmr option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: block');
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectServiceID,
            'codeTypeContrainte': selectStateAmr.val(),
            'dateDebut': inputDateStartAmr.val(),
            'dateFin': inputdateEndAmr.val(),
            'valueSearch': selectTypeSearchID === '2' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadAtd'
        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '2':
                                printAtd(empty);
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas d\'ATD');
                                break;
                            case '1':
                                printAtd(empty);
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'un ATD');
                                break;
                        }
                        break;
                    case '1':
                        printAtd(empty);
                        alertify.alert('Aucun avis à tiers détenteur ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                atdList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }
                printAtd(atdList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }
            showResponseError();
        }

    });
}


function getSelectedAssujetiData() {

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    switch (fromModalCall) {

        case '1':

            valueTypeSearch.val(selectAssujettiData.nomComplet);
            valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

            loadAtd(isAdvanced);

            break;

        case '2':

            if (selectAssujettiData.code === redevableId) {

                alertify.alert('Déoler, le redevable n\'est doit pas être lui-même tier détenteur.');
            } else {
                saveAtd(selectAssujettiData.code);
            }

            break;
    }


}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadAtd(isAdvanced);
}

function printAtd(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';
    tableContent += '<th style="text-align:left">N° COMMANDEMENT</th>';
    tableContent += '<th style="text-align:left">DATE CREATION</th>';
    tableContent += '<th style="text-align:left">NTD</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left">DETENTEUR</th>';

    tableContent += '<th style="text-align:right">MONTANT GLOBAL</th>';
    tableContent += '<th style="text-align:right">MONTANT PENALITE</th>';
    tableContent += '<th style="text-align:right">MONTANT FONCTIONNEMENT</th>';
    tableContent += '<th style="text-align:right">MONTANT PRINCIPAL</th>';

    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amountGlobal = formatNumber(result[i].amountGlobal, result[i].devise);
        var amountPenalite = formatNumber(result[i].amountPenalite, result[i].devise);
        var amountFonctionnement = formatNumber(result[i].amountFontionnement, result[i].devise);
        var amountPrincipal = formatNumber(result[i].amountPrincipal, result[i].devise);

        var buttonPrintAtd = '<button class="btn btn-warning" onclick="printDocumentAtd(\'' + result[i].atdID + '\',\'' + result[i].printExist + '\')"><i class="fa fa-print"></i></button>';

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:7%;vertical-align:middle">' + result[i].numeroDocument + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].commandementID + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:center;width:7%;vertical-align:middle">' + result[i].dateEmission + '</td>';
        tableContent += '<td style="text-align:left;width:22%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:22%;vertical-align:middle">' + result[i].detenteurNameComposite + '</td>';

        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountGlobal + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountPenalite + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountFonctionnement + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountPrincipal + '</td>';

        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonPrintAtd + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableAtd.html(tableContent);
    tableAtd.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function printDocumentAtd(id, print) {

    for (var i = 0; i < atdList.length; i++) {

        if (atdList[i].atdID === id) {

            alertify.confirm('Etes-vous sûre de vouloir imprimer l\'ATD ?', function () {

                if (print === '1') {
                    
                    if(atdList.length === 1){
                        
                        i = 0;
                    }
                    
                    setDocumentContent(atdList[i].documentPrint);
                    window.open('visualisation-document', '_blank');

                } else if (print === '0') {

                    $.ajax({
                        type: 'POST',
                        url: 'poursuites_servlet',
                        dataType: 'text',
                        headers: {
                            'Access-Control-Allow-Origin': '*'
                        },
                        crossDomain: false,
                        data: {
                            'atdID': id,
                            'userId': userData.idUser,
                            'operation': 'printAtd'

                        },
                        beforeSend: function () {
                            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours de l\'Atd...</h5>'});

                        },
                        success: function (response)
                        {

                            $.unblockUI();

                            if (response == '-1' || response == '0') {
                                showResponseError();
                                return;
                            } else {
                                loadAtd(isAdvanced);
                                setDocumentContent(response);
                                window.open('visualisation-document', '_blank');

                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $.unblockUI();
                            showResponseError();
                        }
                    });
                }
            });
        }
    }
}

function accuserReception(commandementID) {

    var dateEcheance = '';

    for (var i = 0; i < atdList.length; i++) {

        if (atdList[i].numeroCommandement === commandementID) {
            codeCommandementCurrent = commandementID;
            commandementDocumentPrint = atdList[i].commandementHtmlDocument;
            bonApayerDocumentPrint = atdList[i].bonApayerHtmlDocument;
            codeTypeDoc = 'ATD';
            dateEcheance = atdList[i].dateEcheanceCommandement;
        }
    }

    if (dateEcheance === empty) {
        initAccuseReceptionUI(codeCommandementCurrent, codeTypeDoc);
        modalAccuserReception.modal('show');

    } else {

        alertify.confirm('Etes-vous sûre de vouloir imprimer l\'ATD ?', function () {

            documentListModal.modal('show');

        });
    }
}

function saveAtd(tierDetenteur) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'numeroContrainte': contrainteSelected,
            'userId': userData.idUser,
            'tierDetenteur': tierDetenteur,
            'operation': 'generateAtd'

        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Génération en cours de l\'Atd...</h5>'});

        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                loadAtd(isAdvanced);

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function callGenerateAtd(contrainteID, assujettiCode) {

    contrainteSelected = contrainteID;
    redevableId = assujettiCode;

    alertify.confirm('Etes-vous sûre de vouloir générer l\'Atd pour ce commandement ?', function () {

        fromModalCall = '2';
        assujettiModal.modal('show');
    });
}

