/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalBienTaxation;

var tableBienTaxation2;

var btnSelectAssujetiBien;

var selectAssujettiBien;

var lblNbreBienTaxed;
var bienList = [];
var numeroTaxation;

var ncSelected;

$(function () {

    modalBienTaxation = $('#modalBienTaxation');
    tableBienTaxation2 = $('#tableBienTaxation2');
    btnSelectAssujetiBien = $('#btnSelectAssujetiBien');
    lblNbreBienTaxed = $('#lblNbreBienTaxed');
    numeroTaxation = $('#numeroTaxation');

});

function loadBiensTaxation(nc) {

    ncSelected = nc;

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensTaxation',
            'numeroTaxation': nc
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Affichage de biens en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var result = JSON.parse(JSON.stringify(response));

                if (result.length > 0) {

                    printBienTaxation(result);


                } else {

                    printBienTaxation('');

                    var valueNc = '<span style="font-weight:bold">' + nc + '</span>';
                    alertify.alert('Aucun bien n\'est associé à la taxation numéro : ' + valueNc);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printBienTaxation(bienList) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">BIEN </th>';
    tableContent += '<th scope="col">ADRESSE</th>';
    tableContent += '<th scope="col">MESSAGE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var count = 0;

    for (var i = 0; i < bienList.length; i++) {

        count++;

        var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
        var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';


        var estLocation = '';

        if (bienList[i].isMaster == false) {
            estLocation = '<br/>' + bienList[i].estLocation;
        }

        var nameInfo = 'Dénomination : ' + '<span style="font-weight:bold">' + bienList[i].intituleBien + '</span>' + estLocation;

        var descriptionBien = nameInfo + '<br/>' + genreInfo + '<br/>' + categorieInfo + '<hr/>' + bienList[i].complement;


        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:50px">' + descriptionBien + '</td>';
        tableContent += '<td style="vertical-align:middle;width:25px">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;width:25px">' + bienList[i].messageAnnonceur + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';

    tableBienTaxation2.html(tableContent);

    var myDataTable = tableBienTaxation2.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher bien  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 25,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    lblNbreBienTaxed.html(count);
    numeroTaxation.html(ncSelected);
    modalBienTaxation.modal('show');
}

function getBienTaxation(id) {

    var row = $("#row_" + id);
    var checkBox = document.getElementById("checkbox_" + id);



    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9;color:black');

        checkExist = true;

        for (var j = 0; j < bienList.length; j++) {

            if (bienList[j].idBien == id) {

                bienTaxation = {};

                bienTaxation.bien = id;
                bienTaxation.libelleTypeBien = bienList[j].libelleTypeBien;
                bienTaxation.tarifName = bienList[j].tarifName;
                bienTaxation.chaineAdresse = bienList[j].chaineAdresse;
                bienTaxation.complement = bienList[j].complement;
                bienTaxation.intituleBien = bienList[j].intituleBien;

                dataBienTaxation.push(bienTaxation);
            }
        }

    } else {

        row.removeAttr('style');

        for (var i = 0; i < dataBienTaxation.length; i++) {

            if (dataBienTaxation[i].bien == id) {

                dataBienTaxation.splice(i, 1);
                break;
            }
        }

        if (dataBienTaxation.length == 0) {
            checkExist = false;
        } else {
            checkExist = true;
        }
    }

    lblNbreBienTaxed.html(dataBienTaxation.length);
    //alert(checkExist);

}