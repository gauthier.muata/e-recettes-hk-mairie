/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResearch,
        messageEmptyValue,
        infoPrint,
        advanced;

var tableNC;

var
        inputSearch,
        inputNumeroPapier,
        inputNumeroTimbre,
        inputDateDebut,
        inputdateLast;

var
        lblNotePerception,
        lblNameAssujetti,
        lblAdress,
        lblDebit,
        lblCredit,
        lblSolde,
        lblReceptionniste;

var
        btnSearch,
        btnValidateAcquitLiberatoire;

var
        cmbSearchType,
        selectSite,
        selectService;

var modalAcquitLiberatoireDetails;

var tempNCList;

var
        datePickerDebut,
        datePickerFin;

var fpcCode;
var amountPenalite;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;
var modalRechercheAvanceeModelTwo;

var idDivNumeroVignette, inputNumeroVignette;

var isVignette;

$(function () {

    mainNavigationLabel.text('ACQUIT LIBERATOIRE');
    secondNavigationLabel.text('Edition d\'un acquit libératoire');

    removeActiveMenu();
    linkMenuAcquitLiberatoire.addClass('active');

    inputSearch = $('#inputSearch');
    inputNumeroPapier = $('#inputNumeroPapier');
    inputNumeroTimbre = $('#inputNumeroTimbre');

    lblNotePerception = $('#lblNotePerception');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblAdress = $('#lblAdress');
    lblDebit = $('#lblDebit');
    lblCredit = $('#lblCredit');
    lblSolde = $('#lblSolde');
    lblReceptionniste = $('#lblReceptionniste');

    modalAcquitLiberatoireDetails = $('#modalAcquitLiberatoireDetails');

    tableNC = $('#tableNC');

    isVignette = false;

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    idDivNumeroVignette = $('#idDivNumeroVignette');
    inputNumeroVignette = $('#inputNumeroVignette');


    btnSearch = $('#btnSearch');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');

    btnSearch.click(function (e) {
        e.preventDefault();
        advanced = '0';
        loadNotesCalcul();
    });

    btnValidateAcquitLiberatoire = $('#btnPrintAcquitLiberatoire');

    btnValidateAcquitLiberatoire.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (isVignette) {

            if (inputNumeroVignette.val() === '') {
                alertify.alert('Veuillez d\'abord fournir le numéro de VIGNETTE');
                return;
            }
        }

        alertify.confirm('Etes-vous sûre de vouloir imprimer l\'acquit libératoire ?', function () {
            validatePrintingAcquitLberatoire();
        });

    });

    cmbSearchType = $('#cmbSearchType');
    codeResearch = cmbSearchType.val();
    messageEmptyValue = 'Veuillez d\'abord saisir le nom de l\'assujetti.';

    cmbSearchType.on('change', function (e) {

        codeResearch = cmbSearchType.val();
        inputSearch.val('');

        if (codeResearch === '0') {
            messageEmptyValue = 'Veuillez d\'abord saisir le nom du l\'assujetti.';
            inputSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else {
            messageEmptyValue = 'Veuillez d\'abord saisir le numéro de la note de perception.';
            inputSearch.attr('placeholder', 'Veuillez saisir le titre de perception');
        }
    });

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    selectSite = $('#selectSite');
    selectService = $('#selectService');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    btnRechercheAvancee = $('#btnRechercheAvancee');

    btnRechercheAvancee.click(function (e) {
        e.preventDefault();

        selectSite.val(userData.SiteCode);
        selectService.val(userData.serviceCode);
        datePickerDebut.datepicker("setDate", new Date());
        datePickerFin.datepicker("setDate", new Date());

        $('#modalRechercheAvanceeModelTwo').modal('show');
    });

    btnAdvencedSearch = $('#btnAdvencedSearch');

    btnAdvencedSearch.click(function (e) {

        e.preventDefault();

        advanced = '1';
        inputSearch.val('');
        loadNotesCalcul();

    });

    printEditingNote('');

    btnAdvencedSearch.trigger('click');

});

function loadNotesCalcul() {

    var
            allSite = '0',
            allService = '0',
            serviceCode,
            siteCode;

    if (advanced == '0') {

        if (inputSearch.val().trim() === empty || inputSearch.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }

        serviceCode = userData.serviceCode;
        siteCode = userData.SiteCode;

    } else {
        serviceCode = selectService.val();
        siteCode = selectSite.val();
    }

    if (controlAccess('VIEW_ALL_SITES_NC')) {
        allSite = '1';
    }
    if (controlAccess('VIEW_ALL_SERVICES_NC')) {
        allService = '1';
    }

    if (advanced === '1') {

        if ($('#selectSite option:selected').text().toUpperCase() == '*') {
            lblSite.html('TOUS LES BUREAUX');
        } else {
            lblSite.html($('#selectSite option:selected').text().toUpperCase());
        }

        if ($('#selectService option:selected').text().toUpperCase() == '*') {
            lblService.html('TOUS LES SERVICES');
        } else {
            lblService.html($('#selectService option:selected').text().toUpperCase());
        }

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());

        isAdvance.attr('style', 'display: block');

    } else {
        isAdvance.attr('style', 'display: none');
    }

    $.ajax({
        type: 'POST',
        url: 'acquitLiberatoire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadNotesCalcul',
            'advanced': advanced,
            'typeSearch': codeResearch,
            'libelle': inputSearch.val(),
            'allSite': allSite,
            'allService': allService,
            'idUser': userData.idUser,
            'codeService': serviceCode,
            'codeSite': siteCode,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val()
        },
        beforeSend: function () {

            switch (advanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeModelTwo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
                    break;
            }
        },
        success: function (response)
        {

            switch (advanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeModelTwo.unblock();
                    break;
            }

            if (response == '-1') {

                printEditingNote(empty);
                showResponseError();
                return;

            } else if (response == '0') {

                switch (advanced) {

                    case '0':

                        if (codeResearch == '0') {
                            alertify.alert('Cet assujetti n\'est possède pas de paiement en attente d\'édition du CPI');
                            return;
                        } else {
                            alertify.alert('Ce titre de perception ne pas lié à un paiement en attente d\'édition du CPI');
                            return;
                        }

                        break;

                    case '1':
                        alertify.alert('Aucune ne disponible pour cette période');
                        break;
                }

                printEditingNote(empty);

            } else {

                setTimeout(function () {

                    var noteCalculList = JSON.parse(JSON.stringify(response));


                    tempNCList = [];

                    for (var i = 0; i < noteCalculList.length; i++) {

                        var note = new Object();
                        note.serviceAssiette = noteCalculList[i].serviceAssiette;
                        note.date = noteCalculList[i].date;
                        note.articleBudgetaire = noteCalculList[i].articleBudgetaire;
                        note.periode = noteCalculList[i].periode;
                        note.notePerception = noteCalculList[i].notePerception;
                        note.requerant = noteCalculList[i].requerant;
                        note.formeJuridique = noteCalculList[i].formeJuridique;
                        note.montantDu = noteCalculList[i].montantDu;
                        note.paye = noteCalculList[i].paye;
                        note.penaliteMontantDu = noteCalculList[i].PenaliteMontantDu;
                        note.penalitePaye = noteCalculList[i].Penalitepaye;
                        note.defaultAdress = noteCalculList[i].defaultAdress;
                        note.receptionniste = noteCalculList[i].receptionniste;
                        note.noteCalcul = noteCalculList[i].noteCalcul;
                        note.devise = noteCalculList[i].devise;
                        note.codeArticleBudgetaire = noteCalculList[i].codeArticleBudgetaire;
                        tempNCList.push(note);
                    }

                    printEditingNote(tempNCList);

                }
                , 1);

                $('#modalRechercheAvanceeModelTwo').modal('hide');
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {

            switch (advanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeModelTwo.unblock();
                    break;
            }
            showResponseError();
        }

    });
}

function printEditingNote(tempNCList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col" style="width:50px"> EXERCICE </th>';
    header += '<th scope="col"> SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col" style="width:140px"> NOTE DE PERCEPTION </th>';
    header += '<th scope="col"  style="width:70px"> DATE </th>';
    header += '<th scope="col"> ASSUJETTI </th>';
    header += '<th scope="col"> TYPE ASSUJETTI </th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" style="text-align:left"> MONTANT DÛ </th>';
    header += '<th scope="col" style="text-align:left"> PENALITE DÛ </th>';
    header += '<th scope="col" style="text-align:left"> MONTANT PAYE </th>';
    header += '<th scope="col" style="text-align:left"> RESTE A PAYER </th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < tempNCList.length; i++) {

        var firstLineAB = '';

        if (tempNCList[i].articleBudgetaire.length > 250) {
            firstLineAB = tempNCList[i].articleBudgetaire.substring(0, 250).toUpperCase() + ' ...';
        } else {
            firstLineAB = tempNCList[i].articleBudgetaire.toUpperCase();
        }

        var amountPaid = tempNCList[i].paye + tempNCList[i].penalitePaye;
        var amountPaidColor = 'color:black';

        if (amountPaid > 0) {
            amountPaidColor = 'color:green';
        }

        var reste = ((tempNCList[i].montantDu + tempNCList[i].penaliteMontantDu) - (tempNCList[i].paye + tempNCList[i].penalitePaye));
        var resteColor = 'color:red';

        if (tempNCList[i].reste === 0) {
            resteColor = 'color:black';
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].periode + '</td>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].serviceAssiette.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].notePerception.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].date + '</td>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].requerant.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;">' + tempNCList[i].formeJuridique.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;title="' + tempNCList[i].articleBudgetaire + '"><span style="font-weight:bold;"></span>' + firstLineAB + '</td>';
        body += '<td style="text-align:right;width:10%;vertical-align:middle;">' + formatNumber(tempNCList[i].montantDu, tempNCList[i].devise) + '</td>';
        body += '<td style="text-align:right;width:10%;vertical-align:middle;">' + formatNumber(tempNCList[i].penaliteMontantDu, tempNCList[i].devise) + '</td>';
        body += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;' + amountPaidColor + '">' + formatNumber(amountPaid, tempNCList[i].devise) + '</td>';
        body += '<td style="text-align:right;width:10%;vertical-align:middle;font-weight:bold;' + resteColor + '">' + formatNumber(reste, tempNCList[i].devise) + '</td>';
        body += '<td style="vertical-align:middle;"><a title="Cliquer pour imprimer" style="margin-right:3px" onclick="print(\''
                + tempNCList[i].notePerception
                + '\')" class="btn btn-primary"><i class="fa fa-print"></i></a></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableNC.html(tableContent);

    var dtNoteCalcul = tableNC.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 6}
        ],
        order: [[6, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(6, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableNC tbody').on('click', 'tr', function () {
        var data = dtNoteCalcul.row(this).data();
    });
}

function validatePrintingAcquitLberatoire() {

    $.ajax({
        type: 'POST',
        url: 'acquitLiberatoire_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'printAcquitLiberatoire',
            'notePerception': infoPrint.numeroNP,
            'resteAPayer': infoPrint.solde,
            'paye': infoPrint.paye,
            'numeroPapier': inputNumeroPapier.val(),
            'numeroTimbre': inputNumeroTimbre.val(),
            'idUser': userData.idUser,
            'codeSite': userData.SiteCode,
            'inputNumeroVignette': isVignette == true ? inputNumeroVignette.val() : '',
            'nameUser': userData.nomComplet
        },
        beforeSend: function () {

            modalAcquitLiberatoireDetails.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression de l\'acquit libératoire en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                modalAcquitLiberatoireDetails.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAcquitLiberatoireDetails.unblock();

                switch (response) {
                    case '0':
                        alertify.alert('Echec lors de la création de l\'acquit libératoire.');
                        break;
                    case '2':
                        alertify.alert('Ce titre de perception n\'a pas été payé totalement.');
                        break;
                    case '3':
                        alertify.alert('Le paiement de ce titre de perception ou ses dépendances sont en attente d\'apurement administratif.');
                        break;
                    case '4':
                        alertify.alert('Le paiement de ce titre de perception ou ses dépendances ne sont pas encore apuré par le comptable.');
                        break;
                    case '500':
                        alertify.alert('Echec lors de la génération du code QR.');
                        break;
                    default:

                        modalAcquitLiberatoireDetails.modal('hide');

                        setDocumentContent(response);
                        window.open('visualisation-document', '_blank');

                        tempNCList.splice(infoPrint.indice, 1);
                        infoPrint = null;
                        printEditingNote(tempNCList);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function print(numeroNP) {

    btnValidateAcquitLiberatoire.show();

    for (var i = 0; i < tempNCList.length; i++) {

        if (tempNCList[i].notePerception == numeroNP) {


            lblNotePerception.text(tempNCList[i].notePerception);
            lblNameAssujetti.text(tempNCList[i].requerant);
            lblAdress.text(tempNCList[i].defaultAdress);
            lblReceptionniste.text(tempNCList[i].receptionniste);

            var amount = (tempNCList[i].montantDu + tempNCList[i].penaliteMontantDu);
            var amountPaid = tempNCList[i].paye + tempNCList[i].penalitePaye;
            var reste = (amount - amountPaid);

            lblDebit.text(formatNumber(amount, tempNCList[i].devise));
            lblCredit.text(formatNumber(amountPaid, tempNCList[i].devise));
            lblSolde.text(formatNumber(reste, tempNCList[i].devise));

            infoPrint = {};

            if (reste > 0) {
                btnValidateAcquitLiberatoire.hide();
            }

            infoPrint.solde = reste;
            infoPrint.paye = amountPaid;
            infoPrint.numeroNP = tempNCList[i].notePerception;

            lblReceptionniste.text(tempNCList[i].receptionniste);

            if (tempNCList[i].codeArticleBudgetaire == '00000000000002302020') {
                isVignette = true;
                inputNumeroVignette.val('');
                //inputNumeroVignette.val(tempNCList[i].vignette);
                idDivNumeroVignette.attr('style', 'display:inline');
            } else {
                isVignette = false;
                idDivNumeroVignette.attr('style', 'display:none');
                inputNumeroVignette.val('');
            }

            modalAcquitLiberatoireDetails.modal('show');

            break;
        }
    }

}
