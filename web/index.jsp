<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : moussa.toure
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>
    
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Tableau de bord</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css">  
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            #content2 .hidden{display: none;}
            #content2 #loader{display: none;}
        </style>
        
    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div id="content2"></div>

            </div>
                
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>      
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/index.js"></script>
        
    </body>
    
</html>