<%-- 
    Document   : taxation
    Created on : 16 juin 2018, 08:03:05
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edition de la carte</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/css/cropper.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div style="width: 100%">

                    <div id="contentSearchCPI" style=" width: 33.3%; margin: auto;  position: absolute; top: 35%; left: 55%; transform: translate(-50%); ">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h4 class="panel-title"> Recherche un reçu du paiement
                                </h4>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 

                                    <form>
                                        <div class="form-group">
                                            <label style="margin-bottom: 10px">NUMERO DU RECU</label>
                                            <input type="text" class="form-control" style="" id="ResearchCPI" placeholder="EXP: REC20AA49170">
                                        </div>
                                        <br/>
                                    </form>


                                </div>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary" id="btnSearchCPI" >  <i class="fa fa-search"></i>  Rechercher</button>
                                    <!--a title="Annuler" class="btn btn-secondary" data-dismiss="modal" >Fermer</a-->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


                <div style="display: none" id="contentBody">


                    <div class="row">
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations du propriétaire (Etape 1) 
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 

                                        <div class="row">
                                            <div class="col-lg-9">
                                                <span id="lbl1P" style="font-size: 16px;display: none">Type assujetti :</span>
                                                <span >
                                                    <p id="lblLegalForm"
                                                       style="font-weight: bold;color: black">&nbsp;</p>
                                                </span><br/>
                                                <span id="lbl2" style="font-size: 16px;display: none">Nif :</span>
                                                <span >
                                                    <p id="lblNifResponsible"
                                                       style="font-weight: bold;color: black">&nbsp;</p>
                                                </span><br/>
                                                <span id="lbl3P" style="font-size: 16px;display: none">Noms :</span>
                                                <span  >
                                                    <b id="lblNameResponsible" >&nbsp;</b>
                                                </span><br/><br/>
                                                <span id="lbl4P" style="font-size: 15px;display: none">Adresse :</span>
                                                <span  >
                                                    <p id="lblAddress"
                                                       style="font-size: 12px;
                                                       font-weight: bold;color: black">&nbsp;</p>
                                                </span>
                                                <br/><br/>
                                            </div>
                                            <div class="col-lg-3">
                                                <p>
                                                    Photo
                                                </p>

                                                <div style="border: 1px solid; height: 130px; width: 90%;">
                                                    <img width="100%" height="100%" src="" alt=""/>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="panel-footer">
                                        <button style="visibility:  hidden" 
                                                id="btnCallModalSearchResponsable"
                                                class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un proprietaire
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations du conducteur de la moto (Etape 2)
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body"> 
                                        <div class="row">
                                            <div class="col-lg-9">
                                                <span id="lbl1C" style="font-size: 16px;display: none">Type assujetti :</span>
                                                <span >
                                                    <p id="lblLegalForm2"
                                                       style="font-weight: bold;color: black">&nbsp;</p>
                                                </span><br/>
                                                <span id="lbl2" style="font-size: 16px;display: none">Nif :</span>
                                                <span >
                                                    <p id="lblNifResponsible2"
                                                       style="font-weight: bold;color: black">&nbsp;</p>
                                                </span><br/>
                                                <span id="lbl3C" style="font-size: 16px;display: none">Noms :</span>
                                                <span  >
                                                    <b id="lblNameResponsible2" >&nbsp;</b>
                                                </span><br/><br/>
                                                <span id="lbl4C" style="font-size: 15px;display: none">Adresse :</span>
                                                <span  >
                                                    <p id="lblAddress2"
                                                       style="font-size: 12px;
                                                       font-weight: bold;color: black">&nbsp;</p>
                                                </span>
                                                <br/><br/>
                                            </div>

                                            <div class="col-lg-3">


                                                <div > 
                                                    <input style="display: none" type="file" id="input" name="file" /> 
                                                    <button onclick="pick()" id="btnSearchPhoto" style="width: 90%" class="btn btn-secondary"><i class="fa fa-picture-o"></i>
                                                        Photo
                                                    </button>
                                                    <script>
                                                        function pick() {
                                                            document.getElementById("input").click();
                                                        }
                                                    </script>
                                                </div> 
                                                <!--input type="text" class="form-control" style="display: none" id="textPieceJointe" -->
                                                <br/>
                                                <div style="border: 1px solid; height: 130px; width: 90%;">
                                                    <img id="avatar" width="100%" height="100%" src="assets/images/user.jpg" alt=""/>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="panel-footer">
                                        <button  
                                            id="btnCallModalSearchConducteur"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un conducteur
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div  class="col-lg-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Autres informations de la carte (Etape 3)
                                    </h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" >
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <span class="control-label col-sm-4" for="inputAutocollant">
                                                        Numero autocollant <span style="color:red"> *</span></span>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="inputAutocollant" placeholder="Numéro autocollant"/>
                                                        <br/>
                                                    </div>
                                                </div>


                                                <!--div class="form-group">
                                                    <span class="control-label col-sm-4" for="inputCPI">
                                                        Numéro CPI </span>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="inputCPI" placeholder="Numéro CPI"/>
                                                        <br/>
                                                    </div>
                                                </div-->

                                                <div class="form-group">
                                                    <p class="control-label col-sm-4" for="inputNumAutorisation">Numéro autorisation<span style="color:red"> *</span></p>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="inputNumAutorisation" placeholder="Numéro autorisation"/>
                                                        <br/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-4" for="inputDistrict">District</p>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="inputDistrict" placeholder="District"/>
                                                        <br/>
                                                    </div>
                                                </div>

                                                <!--div class="form-group">
                                                    <p class="control-label col-sm-4" for="inputDateCreation">Date création</p>
                                                    <div class="col-sm-8">
                                                        <input type="date" class="form-control" id="inputDateCreation" />
                                                        <br/>
                                                    </div>
                                                </div-->

                                                <!--div class="form-group">
                                                    <p class="control-label col-sm-4" for="inputDateExpiration">Date expiration</p>
                                                    <div class="col-sm-8">
                                                        <input type="date" class="form-control" id="inputDateExpiration" />
                                                        <br/>
                                                    </div>
                                                </div-->

                                            </div>


                                            <div class="col-lg-6">

                                                <button  
                                                    id="btnAssociateBien"
                                                    class="btn btn-warning ">
                                                    <i class="fa fa-motorcycle"></i>  
                                                    Associer une moto
                                                </button>

                                                <hr/>

                                                <div>

                                                    <span id="lbl1" style="font-size: 16px;">Intitulé de la moto :</span>
                                                    <span >
                                                        <p id="lblIntituleBien"
                                                           style="font-weight: bold;color: black">&nbsp;</p>
                                                    </span><br/>
                                                    <span id="lbl2" style="font-size: 16px;">Description :</span>
                                                    <span >
                                                        <p id="lblDescriptionBien"
                                                           style="font-weight: bold;color: black">&nbsp;</p>
                                                    </span><br/>
                                                    <span id="lbl2" style="font-size: 16px;">Adresse :</span>
                                                    <span >
                                                        <p id="lblAdresserBien"
                                                           style="font-weight: bold;color: black">&nbsp;</p>
                                                    </span><br/>
                                                </div>

                                            </div>



                                        </div>

                                    </div>

                                    <div class="panel-footer" style="margin-top: 15px">
                                        <div class="row">

                                            <div class="col-lg-12 " style="margin-top: 1%">
                                                <button  
                                                    id="btnSaveCarte"
                                                    class="btn btn-success pull-right">
                                                    <i class="fa fa-save"></i>  
                                                    Enregistrer
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="modal fade" id="budgetArticleModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" style="width: 865px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Registre des articles budgétaires</h5>
                    </div>
                    <div class="modal-body" id="divBudgetArticle">
                        <div class="navbar-form" role="search">
                            <div class="input-group">
                                <input 
                                    type="text" class="form-control" 
                                    id="inputValueResearchBudgetArticle" 
                                    placeholder="Critère de recherche">
                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-primary" 
                                        id="btnResearchBudgetArticle">
                                        <i class="fa fa-search"></i>
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table width="100%" 
                               class="table table-bordered table-hover" 
                               id="tableBudgetArticles">
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" 
                                id="btnSelectedBudgetArticle"
                                style="display: none">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCheckCPI" 
             role="dialog"
             data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-dialog-centered" style="margin-top: 100px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Vérification CPI de la fiche</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <label class="control-label col-sm-12" >Veuillez saisir le CPI de la fiche  : </label>

                            <div class="col-sm-12" style="margin-top: 10px">
                                <input class="form-control" id="inputFicheCPI" 
                                       placeholder="Numéro CPI"
                                       type="text" >
                                </input>
                            </div>
                            <p id="lblCPIResult"
                               style="color: red;margin-left: 3px;margin-top: 15px;font-size: 15px"
                               class="control-label col-sm-12" >
                            </p>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="btnValidateCPI">
                            <i class="fa fa-check-circle"></i>
                            Valider
                        </button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalverifyCPI"
             data-backdrop="static" data-keyboard="false"
             tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width: 35%;margin-top: 200px" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Recherche CPI</h5>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label style="margin-bottom: 10px">NUMERO DU CPI</label>
                                <input type="text" class="form-control" style="" id="ResearchCPI" placeholder="EXP: CPI20AA49170">
                            </div>
                            <br/>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnSearchCPI" >  <i class="fa fa-search"></i>  Rechercher</button>
                        <a title="Annuler" class="btn btn-secondary" data-dismiss="modal" >Fermer</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalCrop"
             data-backdrop="static" data-keyboard="false"
             tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width: 35%; height: 50%" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Rognage image</h5>
                    </div>
                    <div class="modal-body">
                        <div id="crop-select"></div>
                    </div>
                    <div class="modal-footer">

                        <a title="Annuler" class="btn btn-secondary" data-dismiss="modal" >Fermer</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalLabel">Rognage image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <img width="100%" id="image" src="https://avatars0.githubusercontent.com/u/3456749">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="button" class="btn btn-primary" id="crop">Rogner</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>        

<%@include file="assets/include/modal/header.html" %>
<%@include file="assets/include/modal/rechercheAssujetti.html" %>
<%@include file="assets/include/modal/rechercheBienPersonne.html" %>


<script>

        window.addEventListener('DOMContentLoaded', function () {
        var avatar = document.getElementById('avatar');
        var image = document.getElementById('image');
        var input = document.getElementById('input');
        var $progress = $('.progress');
        var $progressBar = $('.progress-bar');
        var $alert = $('.alert');
        var $modal = $('#modal');
        var cropper;
        
        bonheur = '0000';
        $('[data-toggle="tooltip"]').tooltip();

            input.addEventListener('change', function (e) {
            var files = e.target.files;
                var done = function (url) {
                input.value = '';
                image.src = url;
                $alert.hide();
            $modal.modal('show');
            };
            var reader;
            var file;
            var url;

                if (files && files.length > 0) {
                    file = files[0];

                if (URL) {
                done(URL.createObjectURL(file));
                    } else if (FileReader) {                     reader = new FileReader();
                        reader.onload = function (e) {
                    done(reader.result);
                    };
        reader.readAsDataURL(file);
        }
            }
        });

            $modal.on('shown.bs.modal', function () {
                cropper = new Cropper(image, {
                    ready: function () {
                    var cropper = this.cropper;
                    var containerData = cropper.getContainerData();
                    var cropBoxData = cropper.getCropBoxData();
                    var aspectRatio = cropBoxData.width / cropBoxData.height;
                    var newCropBoxWidth;

                        if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
                        newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);

                            cropper.setCropBoxData({
                            left: (containerData.width - newCropBoxWidth) / 2,
                    width: newCropBoxWidth
                });
                }
                },
                    cropmove: function () {
                    var cropper = this.cropper;
                    var cropBoxData = cropper.getCropBoxData();
                    var aspectRatio = cropBoxData.width / cropBoxData.height;

                        if (aspectRatio < minAspectRatio) {
                            cropper.setCropBoxData({
                    width: cropBoxData.height * minAspectRatio
                        });
                        } else if (aspectRatio > maxAspectRatio) {
                            cropper.setCropBoxData({
                    width: cropBoxData.height * maxAspectRatio
                });
            }
        },
            });         }).on('hidden.bs.modal', function () {
        cropper.destroy();
        cropper = null;
        });

            document.getElementById('crop').addEventListener('click', function () {
            var initialAvatarURL;
            var canvas;

                $modal.modal('hide');

            if (cropper) {
                    canvas = cropper.getCroppedCanvas({
                    width: 160,
                height: 160,
                });
                //alert('XX');
                photoConducteurExist = true;                 initialAvatarURL = avatar.src;
                avatar.src = canvas.toDataURL();
                console.log(canvas.toDataURL());
                $progress.show();
                $alert.removeClass('alert-success alert-warning');
                    canvas.toBlob(function (blob) {                     var formData = new FormData();

                formData.append('avatar', blob, 'avatar.jpg');

            

    });
            }
        });
    });
</script>


<script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
<script src="assets/lib/js/datatables.min.js" type="text/javascript"></script>
<script src="assets/lib/js/dataTables.select.min.js" type="text/javascript"></script>
<script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
<script src="assets/lib/js/cropper.js" type="text/javascript"></script>
<!--script src="assets/lib/js/crop-select-js.min.js" type="text/javascript"></script-->

<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script>

<script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
<script type="text/javascript" src="assets/js/rechercheBienPersonne.js"></script>
<script src="assets/js/contractMotar.js" type="text/javascript"></script>

</body>
</html>
