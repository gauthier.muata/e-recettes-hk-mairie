<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des extraits de rôle</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/> 

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>

        <link href="assets/css/progressbar.css" rel="stylesheet" type="text/css"/>

        <style>
            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="ResearchTypeRole">
                                <option value="1" >Assujetti</option>
                                <option value="2" >Extrait de rôle</option>
                            </select>
                            <div class="input-group" >
                                <input disabled="true" type="text" class="form-control" 
                                       style="width: 500px" id="ResearchValueRole" placeholder="Nom de l'assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSimpleSearchRole"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnShowAdvancedSerachModalRole" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="row" id="isAdvance">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">

                                        <span id="labelProvince" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Province : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblProvince" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="labelEntite" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service :&nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblEntite" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="lbl3" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service d'assiete :&nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="lbl1" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Centre :
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblSite" >&nbsp;</b>
                                        </span> 

                                        <span style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Agent : 
                                        </span>

                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblAgent" >&nbsp;</b>
                                        </span> 

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Du :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Au :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblDateFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="journal" >
                    <table id="tableRegistreExtraitRole" class="table table-bordered">
                    </table>
                    <table id="tableRegistreExtraitRole2" class="table table-bordered">
                    </table>
                </div>


            </div>
        </div>


        <div class="modal fade" id="modalDetailExtraitRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width: 67%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Registre des extraits des rôles --> Les titres de perception</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group" style="font-size: 20px">
                            <label for="lblArticleRole" style="font-weight: normal" >
                                ARTICLE DE L'EXTRAIT DE ROLE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                            </label>
                            <b id="lblArticleRole">&nbsp;</b><br/>
                            <label for="lblNameAssujetti" style="font-weight: normal" >
                                NOM DE L'ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                            </label>
                            <b id="lblNameAssujetti">&nbsp;</b>

                            <button type="button" class="btn btn-warning pull-right" id="btnShowAccuserReceptionExtr">
                                <i class="fa fa-check-circle"></i> Accuser réception
                            </button>
                        </div>

                        <hr style="margin-top: -10px"/>

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <!--<h4 class="panel-title">Détail de l'extrait de rôle</h4>-->
                            </div>
                            <div class="panel-wrapper collapse in">

                                <ul class="nav nav-tabs">
                                    <!--li id="tab1" class="active"><a data-toggle="tab" href="#contentTab1" >Nouveaux titres de perception</a></li-->
                                    <li id="tab1" class="active"><a data-toggle="tab" href="#contentTab1" >Les titres de perception</a></li>
                                </ul>

                                <div class="tab-content">

                                    <div id="contentTab2" class="tab-pane fade"><!--div id="contentTab2" class="tab-pane fade"-->
                                        <div class="table-responsive">
                                            <form id="formNvxTP">
                                                <table 
                                                    class="table table-bordered table-hover" 
                                                    id="tableNouveauTitrePerception">                                                       
                                                </table>

                                            </form>
                                        </div>
                                    </div>

                                    <div id="contentTab1" class="tab-pane fade in active">
                                        <div class="table-responsive">
                                            <form id="formFicheCompte">
                                                <table 
                                                    class="table table-bordered table-hover" 
                                                    id="tableDetailRole">                                                       
                                                </table>

                                            </form>
                                        </div>

                                    </div>

                                </div>
                                <div class="panel-footer"  style="text-align: right">
                                    <button type="button" class="btn btn-warning pull-left" id="btnImprimerEtraitRole" >
                                        <i class="fa fa-print"></i> Imprimer l'extrait de rôle
                                    </button>
                                    <button type="button" class="btn btn-success" id="btnDernierAvertissement" >
                                        <i class="fa fa-plus-circle"></i> Générer dernier avertissement
                                    </button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalTrackerLineExtraitRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document" style="width: 67%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Evolution de l'extrait de rôle dans le temps</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group" style="font-size: 20px">
                            <label for="lblArticleRoleExtrait" style="font-weight: normal" >
                                ARTICLE DE L'EXTRAIT DE ROLE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                            </label>
                            <b id="lblArticleRoleExtrait">&nbsp;</b><br/>
                            <label for="lblNameAssujettiExtrait" style="font-weight: normal" >
                                NOM DE L'ASSUJETTI &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                            </label>
                            <b id="lblNameAssujettiExtrait">&nbsp;</b>

                        </div>

                        <hr style="margin-top: -10px"/><br/><br/><br/><br/>

                        <div class="wizard" >
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li id="idLevelZeroLi">
                                        <a  title="Etape 1 : Extrait de rôle">
                                            <span class="round-tab" >
                                                1
                                            </span><br/><br/>
                                            <span id="idLevelZero" 
                                                  style="margin-top: 20px;width: 300px;font-weight: bold;margin-left: -65px">
                                            </span>

                                        </a>
                                    </li>
                                    <li id="idLevelOneLi">
                                        <a title="Etape 2 : Dernier avertissement">
                                            <span class="round-tab" >
                                                2
                                            </span><br/><br/>
                                            <span id="idLevelOne" 
                                                  style="margin-top: 20px;width: 300px;font-weight: bold;margin-left: -65px">
                                            </span>
                                        </a>
                                    </li>
                                    <li id="idLevelTwoLi">
                                        <a  title="Etape 3 : Contrainte">
                                            <span class="round-tab" >
                                                3
                                            </span><br/><br/>
                                            <span id="idLevelTwo" 
                                                  style="margin-top: 20px;width: 300px;font-weight: bold;margin-left: -65px">
                                            </span>
                                        </a>
                                    </li>
                                    <li id="idLevelThreeLi">
                                        <a title="Etape 4 : Commandement">
                                            <span class="round-tab" >
                                                4
                                            </span><br/><br/>
                                            <span id="idLevelThree" 
                                                  style="margin-top: 20px;width: 300px;font-weight: bold;margin-left: -65px">
                                            </span>
                                        </a>
                                    </li>

                                    <li id="idLevelFourLi">
                                        <a title="Etape 5 : PV de saisie /  ATD">
                                            <span class="round-tab" >
                                                5
                                            </span><br/><br/>
                                            <span id="idLevelFour" 
                                                  style="margin-top: 20px;width: 300px;font-weight: bold;margin-left: -65px">
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>


                        </div>

                        <br/><br/><br/><br/><br/><br/>

                        <div class="panel-footer"  style="text-align: right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>       

    <%@include file="assets/include/modal/header.html" %>

    <%@include file="assets/include/modal/rechercheAvanceeNC.html" %>


    <%@include file="assets/include/modal/accuserReception.html" %>
    <%@include file="assets/include/modal/modalUpload.html" %>

    <%@include file="assets/include/modal/notePerceptionChild.xhtml" %>
    <%@include file="assets/include/modal/rechercheAssujetti.html" %>

    <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
    <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>

    <script type="text/javascript" src="assets/js/utils.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>

    <script src="assets/js/accuserReception.js" type="text/javascript"></script>
    <script src="assets/js/rechercheAvancee_1.js" type="text/javascript"></script>
    <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
    <script src="assets/js/paiement.js" type="text/javascript"></script>
    <script src="assets/js/Recouvrement/extraitRole.js" type="text/javascript"></script>
    <script src="assets/js/modalAddPenalite.js" type="text/javascript"></script>
    <script src="assets/js/uploading.js" type="text/javascript"></script>

</body>
</html>
