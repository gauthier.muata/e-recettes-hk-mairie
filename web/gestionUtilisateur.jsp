<%-- 
    Document   : parametrage
    Created on : 27 janv. 2020, 12:01:40
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition d'un utilisateur</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-lg-12">                         
                            <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>
                            <button class="btn btn-success" id="btnEnregistrer" style="float: right">
                                <i class="fa fa-save"></i> Enregistrer
                            </button>
                        </div>
                    </div>



                    <!--div class="col-lg-12"-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title" > INFORMATIONS DE L'UTILISATEUR</h4>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="col-lg-6" >
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">

                                                <div class="form-group">    
                                                    <p class="control-label col-sm-2" for="inputMatricule">
                                                        Matricule <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputMatricule" placeholder="Matricule"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputNom">
                                                        Nom <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputNom" placeholder="Nom de l'utilisateur"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputPreNom">
                                                        Prénom <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputPrenom" placeholder="Prénom de l'utilisateur"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="cmbFonction">
                                                        Fonction <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" id="cmbFonction" placeholder="Fonction de l'utilisateur"></select>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputGrade">
                                                        Grade</p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputGrade" placeholder="Grade de l'utilisateur"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputEmail">E-mail</p>
                                                    <div class="col-sm-10">
                                                        <input type="email" class="form-control" id="inputEmail" placeholder="E-mail"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputTelephone">
                                                        Téléphone</p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputTelephone" placeholder="Téléphone de l'utilisateur"/>
                                                    </div>
                                                </div>        

                                            </div>
                                        </div>

                                    </div>
                                </div> 

                                <div class="col-lg-6">
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="cmbDivisions">
                                                        Division <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" id="cmbDivisions" placeholder="Division de l'utilisateur"></select>
                                                    </div>
                                                </div> 
                                                
                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="cmbSite">
                                                        Site <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" id="cmbSite" placeholder="Site de l'utilisateur"></select>
                                                    </div>
                                                </div> 
                                                
                                                <div class="form-group" id="idDivSitePeage" style="display: none">
                                                    <p class="control-label col-sm-2" for="cmbSitePeage">
                                                        P&eacute;age <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" id="cmbSitePeage" placeholder="Site p&eacute;age"></select>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="cmbService">
                                                        Service <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <select class="form-control" id="cmbService" placeholder="Service de l'utilisateur"></select>
                                                    </div>
                                                </div> 

                                                <div class="form-group">
                                                    <p class="control-label col-sm-2" for="inputLogin">
                                                        Login <span style="color:red"> *</span></p>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="inputLogin" placeholder="Login de l'utilisateur"/>
                                                    </div>
                                                </div>

                                                <fieldset class="fieldset" >

                                                    <legend style="font-size: 16px;color: #9e9e9e">
                                                        Adresse(s) <span style="color:red"> *</span></legend>

                                                    <div >
                                                        <table 
                                                            width="100%" 
                                                            class="table table-bordered table-hover"
                                                            id="tableAdressesPersonne">
                                                        </table>

                                                        <label id="labelInfo"
                                                               style="font-weight: normal;font-size: 13px;color: #449d44;">  
                                                            <br/>
                                                        </label>

                                                        <button class="btn btn-warning" id="btnRéinitialiserMotPasse" type="button"
                                                                style="float: right;display: none">
                                                            <i class="fa fa-edit"></i>  
                                                            Réinitialiser mot passe
                                                        </button> &nbsp;&nbsp;
                                                        <button class="btn btn-success" id="btnAjouterAdresse"
                                                                style="float: right">
                                                            <i class="fa fa-plus-circle"></i>  
                                                            Ajouter une adresse
                                                        </button>

                                                    </div>

                                                </fieldset>

                                            </div>
                                        </div>

                                    </div>
                                </div>  
                            </div>
                        </div>


                    </div>                           
                    <!--/div-->

                    <div class="modal fade" id="modalAdresses" role="dialog" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog modal-dialog-centered" style="width: 865px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title" id="exampleModalLongTitle">Registre des adresses</h5>
                                </div>
                                <div class="modal-body" id="divAdresses">
                                    <div class="navbar-form" role="search">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="inputLibelle" 
                                                   placeholder="Veuillez saisir l'adresse">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary" id="btnRechercherAdresses">
                                                    <i class="fa fa-search-plus"></i>
                                                    Rechecher 
                                                </button>
                                            </div>
                                        </div>
                                        <a id="btnAddEntite"
                                           class="btn btn-warning pull-right">
                                            <i class="fa fa-plus-circle"></i>  
                                            Nouvelle adresse
                                        </a>
                                    </div>

                                    <table 
                                        width="100%" 
                                        class="table table-bordered table-hover" 
                                        id="tableAdresses">                                                       
                                    </table>

                                </div>
                                <div class="modal-footer">
                                    <div class="navbar-form" >
                                        <label id="labelSelectedAdresse" style="float:left;font-weight: normal"></label>
                                        <div class="input-group" >
                                            <input type="text" 
                                                   class="form-control" 
                                                   id="inputNumber" 
                                                   placeholder="Numéro" 
                                                   size="5"
                                                   style="visibility: hidden">

                                            <div class="input-group-btn">
                                                <button 
                                                    class="btn btn-success" 
                                                    id="btnValiderAdresse"
                                                    style="visibility: hidden">
                                                    <i class="fa fa-check-circle"></i>
                                                    Valider
                                                </button>
                                                <button class="btn btn-default" 
                                                        id="btnFermerAdresse"
                                                        data-dismiss="modal">
                                                    Fermer
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>                    
        </form>
    </div>
</div>


<%@include file="assets/include/modal/header.html" %>
<%@include file="assets/include/modal/addEntiteAdministrative.xhtml" %>

<script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
<script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
<script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="assets/js/main.js"></script> 
<script type="text/javascript" src="assets/js/utils.js"></script>
<script type="text/javascript" src="assets/js/utilisateur/gestionUtilisateur.js"></script>
<script type="text/javascript" src="assets/js/entityAdministrative.js"></script>
</body>
</html>
