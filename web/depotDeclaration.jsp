<%-- 
    Document   : depotDeclaration
    Created on : 4 févr. 2020, 10:08:22
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition d'un depôt de déclaration</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row" style="margin-bottom: 5px">
                        <div class="col-lg-12">
                            <button class="btn btn-success" id="btnEnregistrerDeclaration" style="display: none;float: right">
                                <i class="fa fa-save"></i>  Enregistrer dépôt déclaration
                            </button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DU CONTRIBUABLE</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">

                                        <span id="lblFormeJurid" style="font-size: 18px">Type :</span>
                                        <span  >
                                            <p id="lblFormeJuridique"
                                               style="font-size: 16px;
                                               font-weight: bold;color: black">&nbsp;</p>
                                        </span><br/>

                                        <span id="lblNomAssuj" style="font-size: 18px">Noms :</span>
                                        <span  >
                                            <p id="lblNomAssujetti"
                                               style="font-size: 16px;
                                               font-weight: bold;color: black">&nbsp;</p>
                                        </span><br/>

                                        <span id="lbllblAddr" style="font-size: 18px">Adresse :</span>
                                        <span  >
                                            <p id="lblAddresse"
                                               style="font-size: 16px;
                                               font-weight: bold;color: black">&nbsp;</p>
                                        </span>
                                    </div> 
                                </div>                              
                            </div>

                        </div>               
                    </div> 

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS PAIEMENT</h4>
                                </div>

                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <br/>
                                        <div class="col-lg-3">
                                            <span id="lblNumeroBor" style="font-size: 18px">N° Bordereau :</span>
                                            <span  >
                                                <p id="lblNumeroBordereau"
                                                   style="font-size: 16px;
                                                   font-weight: bold;color: black">&nbsp;</p>
                                            </span><br/>
                                        </div>
                                        <div class="col-lg-3">
                                            <span id="lblNumeroDecl" style="font-size: 18px">N° Note de taxation :</span>
                                            <span  >
                                                <p id="lblNumeroDeclaration"
                                                   style="font-size: 16px;
                                                   font-weight: bold;color: black">&nbsp;</p>
                                            </span><br/>
                                        </div>
                                        <div class="col-lg-3">
                                            <span id="lblMontantPe" style="font-size: 18px">Montant payé :</span>
                                            <span  >
                                                <p id="lblMontantPercu"
                                                   style="font-size: 16px;
                                                   font-weight: bold;color: black">&nbsp;</p>
                                            </span><br/>
                                        </div>

                                        <div class="col-lg-3">
                                            <span id="lblBanq" style="font-size: 18px">Banque :</span>
                                            <span  >
                                                <p id="lblBanque"
                                                   style="font-size: 16px;
                                                   font-weight: bold;color: black">&nbsp;</p>
                                            </span>
                                        </div>

                                    </div> 
                                </div>

                                <hr/>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body" >
                                        <table width="100%" 
                                               class="table table-responsive table-bordered table-hover" 
                                               id="tableDetailBordereau">
                                        </table>
                                    </div>
                                </div>
                            </div> 
                        </div> 

                    </div> 

                    <div class="modal-footer">

                        <a 
                            id="lblNbDocumentDepotDeclation"
                            class="pull-left"
                            style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                        </a>

                        <button   type="button" class="btn btn-warning pull-right" 
                                  id="btnJoindreDocument">
                            <i class="fa fa-download"></i>  
                            Joindre les documents
                        </button>

                    </div>   

                </form>
            </div>             
        </div>


        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalSerchDocumentDepotDeclaration.xhtml" %>
        <%@include file="assets/include/modal/modalUpload.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>   
        <script type="text/javascript" src="assets/js/declaration/depotDeclaration.js"></script>
        <script type="text/javascript" src="assets/js/uploading.js"></script>        
    </body>
</html>
