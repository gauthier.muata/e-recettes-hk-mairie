<%-- 
    Document   : gestionFaitsGenerateurs
    Created on : 29 janv. 2020, 12:26:15
    Author     : enochbukasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des paiements</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>


                <div class="row">
                    <div class="col-lg-5">
                        <select style="width: 100%" class="form-control" id="cmbMinistere">
                        </select>
                    </div>
                    <div class="col-lg-5">
                        <select style="width: 100%" class="form-control" id="cmbServiceAssiette">
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-primary" 
                           id="BtnNouveau"
                           href="edition-paiement"
                           style="width: 100%">
                            <i class="fa fa-plus"></i> 
                            Nouveau</a>
                    </div>

                    <hr/><br/>
                    <div class="col-lg-12" id="DivNewFaitGenerateur" style="display: none">
                        <div class="panel panel-success">
                            <div class="panel-heading" style="display: flex;justify-content: space-between;cursor: pointer">
                                <h4 class="panel-title"> Cr&eacute;&eacute;r un nouveau fait g&eacute;n&eacute;rateur</h4>
                                <a id="BtnDismiss" type="button" class="pull-right"><i class="fa fa-close"></i></a>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input class="form-control" placeholder="Fait générateur" style="width: 100%" id="inputgenerateur"/>
                                        </div>
                                        <div class="col-lg-5">
                                            <select style="width: 100%" class="form-control" id="cmbFormeJuridique">
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <a class="btn btn-success"
                                               id="BtnEnregistrer"
                                               href="edition-paiement"
                                               style="width: 100%">
                                                <i class="fa fa-save"></i> 
                                                Enregistrer</a>
                                        </div>
                                    </div>                               
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>

                <div >
                    <table id="tableFaitGenerateur" class="table table-bordered">
                    </table>
                </div>

            </div>

            <%@include file="assets/include/modal/header.html" %>
            <%@include file="assets/include/modal/rechercheAvanceePaiement.html" %>
            <%@include file="assets/include/modal/rechercheAssujetti.html" %>

            <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
            <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

            <script type="text/javascript" src="assets/js/utils.js"></script>
            <script type="text/javascript" src="assets/js/main.js"></script>
            <script src="assets/js/rechercheAvancee.js" type="text/javascript"></script>
            <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
            <script src="assets/js/modalSearchArticleBudgetaire.js" type="text/javascript"></script>
            <script src="assets/js/paiement.js" type="text/javascript"></script>
            <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>
            <script src="assets/js/articleBudgetaire/gestionFaitGenarateur.js" type="text/javascript"></script>


    </body>
</html>