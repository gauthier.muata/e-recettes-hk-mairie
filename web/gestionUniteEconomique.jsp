<%-- 
    Document   : gestionUniteEconomique
    Created on : 23 févr. 2021, 13:36:34
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des unités économiques</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <!--link rel="stylesheet" href="assets/lib/css/font-awesome.css"--> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }
        </style>
    </head>
    <body>
        <div class="wrapper">
            
        </div>
    </body>
</html>
