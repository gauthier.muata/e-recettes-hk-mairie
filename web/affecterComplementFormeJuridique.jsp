<%-- 
    Document   : affecterComplementFormeJuridique
    Created on : 14 déc. 2020, 12:02:49
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Affection d'un complément à une forme juridique</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">

                        <div class="col-lg-12">

                            <div class="form-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> Affectation d'un complément à une forme juridique</h4>
                                    </div>
                                </div> 

                                <div class="col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body">

                                                <div class="form-group">

                                                    <p class="control-label col-sm-2" for="cmbFormeJuridique">
                                                        Forme juridique
                                                    <div class="col-sm-7">
                                                        <select class="form-control" id="cmbFormeJuridique"></select>
                                                    </div>

                                                    <button class="btn btn-default" 
                                                            id="btnAffecterFormeJuridique"
                                                            style="float:right;margin-right: 50px">
                                                        <i class="fa fa-plus"></i> 
                                                        Affecter complément forme
                                                    </button>

                                                </div>  


                                            </div> 
                                        </div> 
                                    </div> 
                                </div> 

                                <div class="row">
                                    <div  class="col-lg-12" style="margin-top: -1%">

                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body" >
                                                <table width="100%" 
                                                       class="table table-responsive table-bordered table-hover" 
                                                       id="tableComplementFormeJuridique">
                                                </table>                             
                                            </div>
                                                                
                                        </div>                      
                                    </div>  

                                </div>

                            </div> 
                        </div> 
                    </div> 
                </form>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalComplementFormeJuridique.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script src="assets/js/complement/gestionComplementFormeJuridique.js" type="text/javascript"></script>
    </body>
</html>
