<%-- 
    Document   : trackingCarte
    Created on : 14 avr. 2020, 10:07:52
    Author     : juslin.tshiamua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tra&ccedil;age de la carte</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            
            <div id="content" style="width: 100%">
                <div class="row" style="display: none">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearch">
                                <option selected value="1">Num&eacute;ro carte</option>
                                <option value="2">Noms du conducteur</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 340px;" id="inputResearchFilter">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnResearchCarte">
                                        <i class="fa fa-search"></i>
                                        Rechercher
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr/>
                <div class="journal" >
                    <div  id="detailCarteContainer">

                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/carte/trackingCard.js"></script>

    </body>
</html>
