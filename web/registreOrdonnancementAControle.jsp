<%-- 
    Document   : registreOrdonnancementAControle
    Created on : 30 nov. 2020, 10:21:27
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des ordonnancements à contrôler</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css" />
        <link href="assets/lib/css/datatables.min.css"/>

        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        
        <style>
            #inputResearchTaxation{width:340px}
            @media (max-width: 768px) {
                #spanSearch{display: none}
                #inputResearchTaxation{width:100%}
                #divInputGroupSearch{margin-top: 2px}
                #divTableTaxation{overflow: auto}
                .btnPerso{background-color: transparent;color: blue;cursor: pointer;border-color: transparent;text-decoration: underline}
                .btnPerso:focus{background-color: transparent;border-color: transparent}
            }
        </style>
        
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html"%>

                <div class="row">

                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="typeResearchControle">
                                <option selected value="1">Assujetti</option>
                                <option value="2">Note de calcul</option>
                            </select>
                            <div class="input-group" id="divInputGroupSearchControle">
                                <input type="text" class="form-control inputRT" id="inputResearchTaxationControle">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit"id="btnResearchTaxationControle">
                                        <i class="fa fa-search"></i> <span id="spanSearchControle">Rechercher</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>

                     <button 
                        id="btnCallModalSearchAvanded"
                        style="margin-right: 20px" 
                        class="btn btn-warning pull-right btnPerso">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée
                    </button>  
                    
                      <hr/>
                      
                      <div class="row" id="isAdvance" style="display: none">
                          <div  class="col-lg-12">
                               <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour :</label>
                               
                          </div>
                      </div>

                </div>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>

    </body>
</html>
