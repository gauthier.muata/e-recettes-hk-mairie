<%-- 
    Document   : editerReclamation
    Created on : 20 juin 2019, 11:07:24
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition de la réclamation</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
         <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div  class="col-lg-12">

                        <div id="divRadio" class="form-check">
                            <input class="form-check-input" type="radio" name="radioTypeReclamation" style="display: none" id="radioTypeReclamationT" value="1" checked="false">                        
                            <label class="form-check-label" for="radioTypeReclamationT" style="margin-right: 100px;font-weight: normal; display: none"> Réclamation en taxation</label>
                            <input class="form-check-input" type="radio" name="radioTypeReclamation" id="radioTypeReclamationOR" value="2" checked="true">
                            <label class="form-check-label" for="radioTypeReclamationOR" style="margin-right: 80px;font-weight: normal">Réclamation </label>
                        </div> 

                    </div>
                </div>
                <br/>
                <div class="row">

                    <div  class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <span id="lbl1" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Noms de l'assujetti &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNameResponsible" >&nbsp;</b>
                                </span>

                                <span id="lbl2" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Type&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblLegalForm" >&nbsp;</b>
                                </span>

                                <span id="lbl3" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Nif &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblNifResponsible" >&nbsp;</b>
                                </span>
                                <br/>
                                <span id="lbl4" style="font-size: 16px; color: black; display: none">
                                    &nbsp;&nbsp;&nbsp;&nbsp;Adresse principale &nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;
                                </span>
                                <span style="font-size: 16px; color: black;font-weight: bold">
                                    <b id="lblAddress" >&nbsp;</b>
                                </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <span style="float: right;">
                                    <button id="btnCallModalSearchResponsable" class="btn btn-primary">
                                        <i class="fa fa-search"></i> 
                                        Rechercher un assujetti
                                    </button>&nbsp;&nbsp;&nbsp;&nbsp;
                                </span>
                                <br/><br/>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">

                            <div class="panel-heading">
                                <h4 class="panel-title"> Informations du dossier de réclamation
                                </h4>
                            </div>

                            <div class="panel-wrapper collapse in">
                                <div class="panel-body"> 

                                    <div class="row" id="divRecAddDoc">
                                        <div class="col-lg-1">
                                            <label style="font-weight: normal"> N° document</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <input class="form-control" id="inputNumeroDocument" >
                                        </div>
                                        <div class="col-lg-1">
                                            <label style="font-weight: normal"> Type document</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <select class="form-control" id="cmbTypeDocument" >
                                                <!--<option value ="0">-- Sélectionner --</option>-->
                                                <option value ="AMR">Avis de mise en recouvrement</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2">
                                            <button  style="width: 100%"
                                                     id="btnSearchAndAddDocument"
                                                     class="btn btn-warning">
                                                <i class="fa fa-plus-circle"></i>
                                                Rechercher / ajouter document
                                            </button>
                                        </div>

                                    </div>

                                    <br/>

                                    <div id="divEntiteTableRefObs">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="responsive">
                                                    <table class="table table-bordered table-hover" 
                                                           id="tableReclammation"
                                                           style="margin: auto">                                                       
                                                    </table>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6"></div>

                                            <div class="col-lg-6 pull-right">
                                                <label for="textReferenceCourrier" style="font-weight: normal">
                                                    Référence du courrier de réclamation <span style="color: red"> *</span>
                                                </label>
                                                <input id="textReferenceCourrier" style="width: 100%">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <label for="textObservation" style="font-weight: normal">Observation</label>
                                                <textarea id="textObservation" rows="7" style="width: 100%"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="panel-footer" style="text-align: right">

                                    <button                    
                                        type="button" 
                                        class="btn btn-warning pull-left" 
                                        id="btnJoindreArchiveReclamation">
                                        <i class="fa fa-download"></i>  
                                        Joindre les pi&egrave;ces jointes
                                    </button>
                                    <a 
                                        id="lblNbDocumentReclamation"
                                        class="pull-left"
                                        style="font-style: italic;margin-top: 8px;margin-left: 8px;text-decoration: underline;color: blue">
                                    </a>

                                    <button 
                                        id="btnEnregistrerReclammation"
                                        class="btn btn-success">
                                        <i class="fa fa-save"></i> Enregistrer la réclamation
                                    </button>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="modal fade" id="modalDetailExtraitRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 67%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Les d&eacute;tails de l'extrait de r&ocirc;le</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <!--<h4 class="panel-title">Détail de l'extrait de rôle</h4>-->
                                    </div>
                                    <div class="panel-wrapper collapse in">

                                        <div class="panel-body">
                                            <form >
                                                <div class="responsive">
                                                    <table 
                                                        class="table table-bordered table-hover" 
                                                        id="tableTitrePerception">                                                       
                                                    </table>
                                                </div>   

                                            </form>
                                        </div>

                                        <div class="panel-footer"  style="text-align: right">
                                            <button type="button" class="btn btn-primary" id="btnAjouterDocumentRole" >
                                                <i class="fa fa-plus-circle"></i> Ajouter document
                                            </button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" >
                                                Fermer
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalUpload.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheAdressePersonne.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>
        <script  type="text/javascript" src="assets/js/uploading.js"></script>
        <script type="text/javascript" src="assets/js/editerReclamation.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
    </body>
</html>
